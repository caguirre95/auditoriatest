﻿using DevExpress.Web;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administracion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Linea();
            op();
        }
        combo();
    }

    void Linea()
    {
        string line = "  select id_linea as Id, numero as Linea from Linea where idcliente = 11 and numero not like '%-%' order by numero asc";
        DataTable li = DataAccess.Get_DataTable(line);

        linea.DataSource = li;
        linea.DataTextField = "Linea";
        linea.DataValueField = "Id";
        linea.DataBind();
        linea.Items.Insert(0, "Select...");

        cmbline.DataSource = li;
        cmbline.TextField = "Linea";
        cmbline.ValueField = "Id";
        cmbline.DataBind();
    }

    void sec()
    {
        string section = " select s.id_seccion as Ids, m.Modulo as Modulo from Seccion s"
                      + " join Linea l on l.id_linea = s.id_linea"
                      + " join Modulo m on m.id_modulo = s.idModulo"
                      + " where l.id_linea =" + linea.SelectedItem.Value;
        DataTable linsec = DataAccess.Get_DataTable(section);

        seccion.DataSource = linsec;
        seccion.DataTextField = "Modulo";
        seccion.DataValueField = "Ids";
        seccion.DataBind();
        seccion.Items.Insert(0, "Select...");
    }

    void s()
    {
        string section = " select s.id_seccion as Ids, m.Modulo as Modulo from Seccion s"
                      + " join Linea l on l.id_linea = s.id_linea"
                      + " join Modulo m on m.id_modulo = s.idModulo"
                      + " where l.id_linea =" + cmbline.SelectedItem.Value;
        DataTable linsec = DataAccess.Get_DataTable(section);

        cmbseccion.DataSource = linsec;
        cmbseccion.TextField = "Modulo";
        cmbseccion.ValueField = "Ids";
        cmbseccion.DataBind();
    }

    void op()
    {
        try
        {
            string oper = " select ope.id_operario as id1, ope.nombre as Nombre1 from OperacionOperario op"
                       + " join Operario ope on ope.id_operario = op.id_operario"
                       + " join Operacion opr on opr.id_operacion = op.id_operacion"
                       + " join Seccion s on s.id_seccion = op.id_seccion"
                       + " join Modulo m on m.id_modulo = s.idModulo"
                       + " join Linea l on l.id_linea = s.id_linea";
            //+ " where l.id_linea = " + linea.SelectedValue + "  and s.id_seccion = " + seccion.SelectedValue;
            DataTable operario_t = DataAccess.Get_DataTable(oper);
            cmboperario.DataSource = operario_t;
            cmboperario.TextField = "Nombre1";
            cmboperario.ValueField = "id1";
            cmboperario.DataBind();
        }
        catch (Exception ex)
        {
            string a = ex.Message;
        }
    }

    void carga_oper()
    {
        if (Convert.ToInt32(linea.SelectedItem.Value) > 0 && Convert.ToInt32(seccion.SelectedItem.Value) > 0)
        {
            string oper = " select op.idoperarioOpe as id, m.Modulo as Seccion, opr.descripcion_completa as Operacion, ope.nombre as Nombre, op.estado as Estado from OperacionOperario op"
                      + " join Operario ope on ope.id_operario = op.id_operario"
                      + " join Operacion opr on opr.id_operacion = op.id_operacion"
                      + " join Seccion s on s.id_seccion = op.id_seccion"
                      + " join Modulo m on m.id_modulo = s.idModulo"
                      + " join Linea l on l.id_linea = s.id_linea"
                      + " where l.id_linea = " + linea.SelectedItem.Value + "and s.id_seccion = " + seccion.SelectedItem.Value;

            DataTable oper_q = DataAccess.Get_DataTable(oper);
            grid_operario.DataSource = oper_q;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "gridcampo();", true);
        }
    }

    void combo()
    {
        string comb = " select id_operacion as id, descripcion_completa as Operacion from Operacion order by descripcion_completa asc";

        DataTable cm = DataAccess.Get_DataTable(comb);
        combooper.DataSource = cm;
        combooper.ValueField = "id";
        combooper.TextField = "Operacion";
        combooper.DataBind();
    }

    void limpiarcampos()
    {
        txtcodigo.Text = "";
        txtnombre.Text = "";
        txtdescrip.Text = "";
        chkactiv.Checked = false;
        chkstad.Checked = false;
        combooper.Text = "";
        txtdescripcion.Text = "";
        txtnombre1.Text = "";
        chkstado2.Checked = false;
        combooper.Text = "";
        chkstad.Checked = true;
    }

    void habilitar()
    {
        txtcodigo.Text = "";
        txtnombre.Text = "";
        txtdescrip.Text = "";
        chkactiv.Checked = false;
        chkstad.Checked = false;
        combooper.Text = "";
        txtdescripcion.Enabled = false;
        txtnombre1.Enabled = false;
        chkstado2.Enabled = false;
        combooper.Enabled = true;
        chkstad.Enabled = true;
    }

    protected void btnaceptar_Click(object sender, EventArgs e)
    {
        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
        string userId = mu.ProviderUserKey.ToString();

        try
        {
            if (txtcodigo.Text != string.Empty && txtnombre.Text != string.Empty && combooper.Text != string.Empty && chkactiv.Checked != false)
            {
                if (seccion.SelectedItem.Value == "Select..." || Convert.ToInt32(seccion.SelectedItem.Value) == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "seccion_choice();", true);
                    limpiarcampos();
                }
                else
                {
                    int ab = DataAccess.SaveOperario(txtcodigo.Text, txtnombre.Text, chkactiv.Checked, txtdescrip.Text);
                    if (ab > 0)
                    {
                        DataAccess.SaveOperarioOperacion(Convert.ToInt32(combooper.SelectedItem.Value), Convert.ToInt32(seccion.SelectedItem.Value), ab, chkstad.Checked);
                        limpiarcampos();                                    
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                    }
                    else
                    if (ab <= 0)
                    {
                        limpiarcampos();                        
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "menor1();", true);
                    }
                }
            }
            else
            if (txtcodigo.Text != string.Empty && txtnombre.Text != string.Empty && txtnombre1.Text != string.Empty && chkactiv.Checked != false)
            {
                if (seccion.SelectedItem.Value == "Select..." || Convert.ToInt32(seccion.SelectedItem.Value) == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "seccion_choice();", true);
                }
                else
                {
                    int ab = DataAccess.SaveOperario(txtcodigo.Text, txtnombre.Text, chkactiv.Checked, txtdescrip.Text);
                    int abc = DataAccess.SaveOperacion(txtdescripcion.Text, txtnombre1.Text);
                    if (ab > 0 && abc > 0)
                    {
                        DataAccess.SaveOperarioOperacion(abc, Convert.ToInt32(seccion.SelectedItem.Value), ab, chkstado2.Checked);
                        habilitar();                      
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                    }
                    else
                    if (ab <= 0 || abc <= 0)
                    {
                        habilitar();                        
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "menor1();", true);
                    }
                }
            }
            else
            {
                limpiarcampos();
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalertelim1();", true);
            }
        }
        catch (Exception ex)
        {
            MailMessage mailObj = new MailMessage("soporte@rocedes.com.ni", "dmartinez@rocedes.com.ni", "Sistema Auditoria", ex.ToString() + "Usuario : " + mu.UserName);
            SmtpClient SMTPServer = new SmtpClient("rocedes.com.ni");
            SMTPServer.Send(mailObj);
        }
    }

    protected void linea_SelectedIndexChanged(object sender, EventArgs e)
    {
        sec();
    }

    protected void seccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        grid_operario.DataBind();        
    }

    protected void grid_operario_DataBinding1(object sender, EventArgs e)
    {
        if (linea.SelectedItem.Value.Equals("Select...") || seccion.SelectedItem.Value.Equals("Select..."))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "gridcampo();", true);
        }
        else
        if (Convert.ToInt32(linea.SelectedItem.Value) > 0 && Convert.ToInt32(seccion.SelectedItem.Value) > 0)
        {
            string oper = " select op.idoperarioOpe as id, m.Modulo as Seccion, opr.descripcion_completa as Operacion, ope.nombre as Nombre, op.estado as Estado from OperacionOperario op"
                      + " join Operario ope on ope.id_operario = op.id_operario"
                      + " join Operacion opr on opr.id_operacion = op.id_operacion"
                      + " join Seccion s on s.id_seccion = op.id_seccion"
                      + " join Modulo m on m.id_modulo = s.idModulo"
                      + " join Linea l on l.id_linea = s.id_linea"
                      + " where l.id_linea = " + linea.SelectedItem.Value + " and s.id_seccion = " + seccion.SelectedItem.Value;

            DataTable oper_q = DataAccess.Get_DataTable(oper);
            grid_operario.DataSource = oper_q;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "gridcampo();", true);
        }
    }

    protected void grid_operario_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grid_operario.DataBind();
    }

    protected void btnaddoper_Click(object sender, EventArgs e)
    {
        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
        string userId = mu.ProviderUserKey.ToString();
        ASPxCheckBox chkactive = new ASPxCheckBox();
        string ok = string.Empty;

        for (int i = 0; i <= grid_operario.VisibleRowCount; i++)
        {
            int id = Convert.ToInt32(grid_operario.GetRowValues(i, "id"));

            if (grid_operario.FindRowCellTemplateControl(i, (GridViewDataColumn)grid_operario.Columns["Estado"], "chkactive") != null)
            {
                chkactive = (ASPxCheckBox)grid_operario.FindRowCellTemplateControl(i, (GridViewDataColumn)grid_operario.Columns["Estado"], "chkactive");
                ((IPostBackDataHandler)chkactive).LoadPostData("chkactive", Request.Form);

                try
                {
                    ok = DataAccess.UpdateEstado(id, chkactive.Checked);
                    if (ok == "OK")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki_bad();", true);
                    }
                }
                catch (Exception ex)
                {
                    MailMessage mailObj = new MailMessage("soporte@rocedes.com.ni", "dmartinez@rocedes.com.ni", "Sistema Auditoria", ex.ToString() + "Usuario : " + mu.UserName);
                    SmtpClient SMTPServer = new SmtpClient("rocedes.com.ni");
                    SMTPServer.Send(mailObj);
                }
            }
        }
    }

    protected void cmbline_SelectedIndexChanged(object sender, EventArgs e)
    {
        s();
    }

    protected void btnedit_Click(object sender, EventArgs e)
    {
        try
        {
            string o = " select ope.id_operario, ope.nombre as Nombre from OperacionOperario op"
                     + " join Operario ope on ope.id_operario = op.id_operario"
                     + " join Operacion opr on opr.id_operacion = op.id_operacion"
                     + " join Seccion s on s.id_seccion = op.id_seccion"
                     + " join Modulo m on m.id_modulo = s.idModulo"
                     + " join Linea l on l.id_linea = s.id_linea"
                     + " where ope.id_operario = " + cmboperario.SelectedItem.Value;
            DataTable a = DataAccess.Get_DataTable(o);
            for (int i = 0; i < a.Rows.Count; i++)
            {
                string ok = DataAccess.UpdateLineaOperario(Convert.ToInt32(cmboperario.SelectedItem.Value), Convert.ToInt32(cmbseccion.SelectedItem.Value), chkestado.Checked);
                if (ok == "OK")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki_bad();", true);                   
                }
            }
            cmboperario.Text = "";
            cmbline.Text = "";
            cmbseccion.Text = "";
            chkestado.Checked = false;
        }
        catch (Exception ex)
        {
            string a = ex.Message;
        }
    }
}