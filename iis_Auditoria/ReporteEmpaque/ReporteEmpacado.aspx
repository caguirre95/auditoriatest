﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteEmpacado.aspx.cs" Inherits="ReporteEmpaque_ReporteEmpacado" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

     <script lang="javascript" type="text/javascript">

             function auto() {
                 $('#<%=txtCorte.ClientID%>').autocomplete({
                    source: function (request, response) {

                        $.ajax({
                            url: "ReporteEmpacado.aspx/GetPorder",
                            data: "{'pre' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        porder: item.porder,
                                        idcliente: item.idcliente,
                                        json: item
                                    }
                                }))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $('#<%=txtCorte.ClientID%>').val(ui.item.porder);

                        return false;
                    },
                    select: function (event, ui) {
                        $('#<%=hdnCorte.ClientID%>').val(ui.item.idcliente);

                        const orden = ui.item.porder.trim();
                        const idcliente = ui.item.idcliente;
                        const caracter = '-';
                        var bandera = true;
                        var ordenF = "";
                        var arreglo = [];
                        var cont = 0;

                        for (var i = 0; i < orden.length; i++) {
                       
                            if (orden[i] === caracter) {
                                arreglo.push(i);
                                cont++;
                                bandera = false;
                            }

                            if (bandera && cont === 0) {
                                ordenF = ordenF + orden[i];
                                console.log(ordenF);
                            }
                        }

                        if (cont === 0) $('#<%=txtCorte.ClientID%>').val(orden);
                        else if (cont === 1) $('#<%=txtCorte.ClientID%>').val(ordenF);
                        else if (cont === 2) {
                            if (ordenF === '25') {
                                ordenF = ordenF + '000';
                            }
                            if (ordenF === '27') {
                                ordenF = ordenF + '00000';
                            }

                            var corte = orden.slice(arreglo[0]+1, arreglo[1])

                            ordenF = ordenF + corte;

                            $('#<%=txtCorte.ClientID%>').val(ordenF);
                        }

                        document.getElementById("<%= drpcliente.ClientID %>").value = idcliente === 11 ? 1 : idcliente === 14 ? 2 : idcliente === 17 ? 3 : 0;

                        return false;
                    }
                 }).data("ui-autocomplete")._renderItem = function (ul, item) {
                     return $("<li  >").append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
                    };
             }
     
     </script>


    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

     <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

     <script type="text/javascript">
         function alertSeleccion() {
             swal({
                 title: '!Sin Selección!',
                 text: 'Debe seleccionar un cliente de la lista desplegable',
                 type: 'info'
             });
         }
     </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <style>
        .sp {
            margin-top: 15px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <br />
        <div class="panel panel-primary">
            <div class="panel-heading">Reporte Empacado Por Cliente</div>
            <div class="panel-body">

                <div class="col-lg-12">

                    <div class="col-lg-4">
                        <%-- <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="rd1" runat="server">
                                    <asp:ListItem Value="1">Empacado por Fecha</asp:ListItem>
                                    <asp:ListItem Value="2">Empacado General</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>--%>
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>


                            <div class="panel-body">
                                <div class="form-group">
                                    Corte
                                 <asp:TextBox ID="txtCorte" runat="server" placeholder="Corte" onkeypress="auto();" CssClass="form-control"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdnCorte" />
                                </div>

                                <div class="form-group">
                                    Cliente 
                                <asp:DropDownList ID="drpcliente" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">Select...</asp:ListItem>
                                    <asp:ListItem Value="1">Carhartt</asp:ListItem>
                                    <asp:ListItem Value="2">Dennis</asp:ListItem>
                                    <asp:ListItem Value="3">Dickies</asp:ListItem>
                                </asp:DropDownList>
                                </div>

                                <div class="form-group">
                                    Fecha Inicial
                                    <asp:TextBox ID="txtfecha1" runat="server" CssClass="form-control"></asp:TextBox>
                                    Fecha Final
                                    <asp:TextBox ID="txtfecha2" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <asp:Button ID="btnbuscar" runat="server" Text="Buscar" CssClass="btn btn-primary form-control" OnClick="btnbuscar_Click" />

                        <asp:Button ID="btnlimpiar" runat="server" Text="Limpiar" CssClass="btn btn-success form-control sp" OnClick="btnlimpiar_Click" />
                    </div>

                    <div class="col-lg-4">
                    </div>

                    <div class="col-lg-12">
                        <hr />
                        <asp:Panel ID="paneldickies" runat="server">
                            <dx:ASPxGridView ID="grdempaque1" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdempaque1_DataBinding" OnCustomCallback="grdempaque1_CustomCallback">
                                <Columns>
                                    <dx:GridViewBandColumn Caption="Reporte Empacado Dickies" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="Cliente" Caption="Cliente" Settings-AllowCellMerge="True"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="WorkOrderID" Caption="Corte"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Style" Caption="Estilo"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Empacado" Caption="Unidades Empacadas"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha"></dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                </Columns>
                                <Settings ShowFooter="true"  />
                                <TotalSummary>
                                    <dx:ASPxSummaryItem FieldName="Empacado" SummaryType="Sum" DisplayFormat="total={0}" />
                                </TotalSummary>
                            </dx:ASPxGridView>
                        </asp:Panel>

                        <asp:Panel ID="paneldennis" runat="server">
                            <dx:ASPxGridView ID="grdempaque2" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdempaque2_DataBinding" OnCustomCallback="grdempaque2_CustomCallback">
                                <Columns>
                                    <dx:GridViewBandColumn Caption="Reporte Empacado Dennis" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="Cliente" Caption="Cliente" Settings-AllowCellMerge="True"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Corte" Caption="Corte"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Empacado" Caption="Unidades Empacadas"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha"></dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                </Columns>
                                <Settings ShowFooter="true"  />
                                <TotalSummary>
                                    <dx:ASPxSummaryItem FieldName="Empacado" SummaryType="Sum" DisplayFormat="total={0}" />
                                </TotalSummary>
                            </dx:ASPxGridView>
                        </asp:Panel>

                        <asp:Panel ID="panelcarhartt" runat="server">
                            <dx:ASPxGridView ID="grdempaque3" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdempaque3_DataBinding" OnCustomCallback="grdempaque3_CustomCallback">
                                <Columns>
                                    <dx:GridViewBandColumn Caption="Reporte Empacado Carhartt" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="BuyerCode" Caption="Cliente" Settings-AllowCellMerge="True"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="OrderNumber" Caption="Corte"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Style" Caption="Estilo"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="UnidadesEmpacadas" Caption="Unidades Empacadas"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha"></dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                </Columns>
                                <Settings ShowFooter="true"  />
                                <TotalSummary>
                                    <dx:ASPxSummaryItem FieldName="UnidadesEmpacadas" SummaryType="Sum" DisplayFormat="total={0}" />
                                </TotalSummary>
                            </dx:ASPxGridView>
                        </asp:Panel>

                        <hr />
                        <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control sp" Text="Export to Excel" OnClick="btnexcel_Click" />
                        <dx:ASPxGridViewExporter ID="export" runat="server"></dx:ASPxGridViewExporter>
                    </div>

                </div>

            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

