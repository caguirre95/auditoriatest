﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteBihorarioReproceso.aspx.cs" Inherits="ReporteEmpaque_ReporteBihorarioReproceso" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script>
        function vacio(fecha) {
            swal({
                title: 'Por Favor!',
                text: "No hay datos para mostrar en esta fecha : " + fecha,
                type: 'info'
            });
        }

        function vacio1() {
            swal({
                title: 'Por Favor!',
                text: " Ingrese los campos de manera correcta",
                type: 'info'
            });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtdate.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });

        function exito() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
    </script>

    <style>
        .m {
            margin-top: 10px;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Reporte Bihorario Produccion</strong></div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <strong>Fecha</strong>
                                <asp:TextBox ID="txtdate" runat="server" CssClass="form-control">
                                </asp:TextBox>
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <div class="panel panel-default">
                            <div class="panel-heading">Comandos</div>
                            <div class="panel-body">
                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control" Text="Generar" OnClick="btngenerar_Click" />
                                <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control m" Text="Pass to excel" OnClick="btnexcel_Click" />
                                <asp:Button ID="btnLimpiar" runat="server" CssClass="btn btn-primary form-control m" Text="Limpiar" OnClick="btnLimpiar_Click" />
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <dx:ASPxGridView ID="grdbiho" runat="server" KeyFieldName="id" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="true"
                            OnDataBinding="grdbiho_DataBinding" OnCustomCallback="grdbiho_CustomCallback">
                            <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                            <Columns>

                                <dx:GridViewBandColumn Caption="CONTROL INTERNO DE REPROCESOS" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Responsable:">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Responsable" Caption="Responsable" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Reproceso" Caption="Reproceso" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Operario" Caption="Operario" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Firma:">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="9.00AM" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="11.00AM" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="1.00PM" VisibleIndex="5">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="3.00PM" VisibleIndex="6">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="4.50PM" VisibleIndex="7">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="6.00PM" VisibleIndex="8">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>

                                    </Columns>
                                </dx:GridViewBandColumn>

                                <dx:GridViewBandColumn Caption="ROC 01 REG PRO 0020" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Fecha de Creacion: Junio de 2022" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Total" UnboundType="Integer" VisibleIndex="8">
                                                    <PropertiesTextEdit DisplayFormatString="{0}" />
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>

                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                            <Settings ShowFooter="true" />
                            <SettingsPager PageSize="50" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="Total" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <GroupSummary>
                                <dx:ASPxSummaryItem FieldName="9.00AM" ShowInGroupFooterColumn="9.00AM" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="11.00AM" ShowInGroupFooterColumn="11.00AM" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="1.00PM" ShowInGroupFooterColumn="1.00PM" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="3.00PM" ShowInGroupFooterColumn="3.00PM" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="4.50PM" ShowInGroupFooterColumn="4.50PM" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="6.00PM" ShowInGroupFooterColumn="6.00PM" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Total" ShowInGroupFooterColumn="Total" SummaryType="Sum" DisplayFormat="{0}" />
                            </GroupSummary>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar" runat="server"></dx:ASPxGridViewExporter>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

