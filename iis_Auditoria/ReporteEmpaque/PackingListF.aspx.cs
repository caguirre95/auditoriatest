﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReporteEmpaque_PackingListF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["Packing"] = null;
        }
    }

    public class porderClass
    {
        public int idorder { get; set; }
        public string porder { get; set; }
        public string style { get; set; }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("spdBuscarPoCliente", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@porder", SqlDbType.NChar,15).Value=pre;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

               // obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][0].ToString();
                obj.style = dt.Rows[i][2].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    void getgrid()
    {
        if (txtPorder.Text != "" && hdfcorte.Value != "")
        {
            Session["Packing"] = null;

            var dt = OrderDetailDA.ReporteEmpaque(hdfcorte.Value);

            Session["Packing"] = dt;
            grdp.DataBind();
            grdp.ExpandAll();

        }

    }

    public class POCont
    {
        public int Id_Order { get; set; }
        public string POrder { get; set; }
        public string CodigoEstilo { get; set; }
    }

    protected void grdp_DataBinding(object sender, EventArgs e)
    {
        grdp.DataSource = Session["Packing"];
        grdp.GroupBy(grdp.Columns["Corte"]);
    }

    protected void grdp_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdp.DataBind();
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            getgrid();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnexpprt_Click(object sender, EventArgs e)
    {
        try
        {
            exportar1.GridViewID = "grdp";

            PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = exportar1;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();

                options.ExportMode = XlsxExportMode.SingleFile;
                options.SheetName = "Empaque";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Packing-List.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }

            ps.Dispose();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnlimp_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/PackingListF.aspx");
    }
}