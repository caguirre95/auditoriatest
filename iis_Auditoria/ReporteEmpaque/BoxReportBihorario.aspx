﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BoxReportBihorario.aspx.cs" Inherits="ReporteEmpaque_BoxReportBihorario" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>    

    <script type="text/javascript">
        function alertSeleccion() {
            swal({
                title: '!Sin Selección!',
                text: 'Debe seleccionar un cliente de la lista desplegable',
                type: 'info'
            });
        }
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <br />
    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px">
            <div class="panel-heading"><strong>Reporte de Empaque</strong></div>
            <div class="panel-body">
                <div>
                    Fecha 
                    <asp:TextBox ID="txtfecha1" runat="server" CssClass="form-control"></asp:TextBox>                    
                </div>

                <div>
                    <hr />
                    <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-primary" Text="Generar Reporte" OnClick="btngenerar_Click" />

                    <asp:Button ID="btnexportar" runat="server" CssClass="btn btn-success" Text="Exportar Reporte" OnClick="btnexportar_Click" />

                    <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-warning" Text="Limpiar Formulario" OnClick="btnlimpiar_Click" />
                </div>

                <div>
                    <br />
                    <dx:ASPxGridView ID="grdbox" runat="server" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="true" OnDataBinding="grdbox_DataBinding" OnCustomCallback="grdbox_CustomCallback">
                        <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="corte" Caption="Corte">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Style" Caption="Estilo">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="08:00AM" Caption="08:00AM">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="10:00AM" Caption="10:00AM">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="12:00PM" Caption="12:00PM">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="02:00PM" Caption="02:00PM">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="04:00PM" Caption="04:00PM">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="06:00PM" Caption="06:00PM">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Total" Caption="Total">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                        <Settings ShowFooter="true" />
                        <SettingsPager PageSize="50" />
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="corte" SummaryType="Count" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="08:00AM" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="10:00AM" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="12:00PM" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="02:00PM" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="04:00PM" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="06:00PM" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="Total" SummaryType="Sum" DisplayFormat="{0}" />                            
                        </TotalSummary>                        
                    </dx:ASPxGridView>
                    <dx:ASPxGridViewExporter ID="exportar" runat="server"></dx:ASPxGridViewExporter>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

