﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GraficoEmpacado.aspx.cs" Inherits="ReporteEmpaque_GraficoEmpacado" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx1" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <style>
        .espacio {
            margin: 7px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <br />
    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px">
            <div class="panel-heading"><strong>Reporte Grafico de Empaque</strong></div>
            <div class="panel-body">
                <div class="col-lg-12">

                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                Fecha Inicial
                                <asp:TextBox ID="txtfecha1" runat="server" CssClass="form-control"></asp:TextBox>

                                Fecha Final
                                <asp:TextBox ID="txtfecha2" runat="server" CssClass="form-control"></asp:TextBox>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-primary form-control espacio" Text="Generar Reporte" OnClick="btngenerar_Click" />

                                <asp:Button ID="btnexportar" runat="server" CssClass="btn btn-success form-control espacio" Text="Exportar Reporte" OnClick="btnexportar_Click" />

                                <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-warning form-control espacio" Text="Limpiar Formulario" OnClick="btnlimpiar_Click" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <hr />
                    <div class="col-lg-12">
                        <dx1:WebChartControl ID="wcClienteEmp" runat="server" Height="400px" Width="704px" RenderFormat="Svg" OnDataBinding="wcClienteEmp_DataBinding"
                            ClientInstanceName="chart" OnCustomCallback="wcClienteEmp_CustomCallback" CrosshairEnabled="True">
                            <SeriesSerializable>
                                <cc1:Series Name="Empaque" ArgumentDataMember="Cliente" ValueDataMembersSerializable="Empacado" SeriesPointsSorting="Descending" 
                                    SeriesPointsSortingKey="Value_1" LegendTextPattern="{A:F2}" LabelsVisibility="True" CrosshairLabelPattern="{V} - Piezas">
                                    <ViewSerializable>
                                        <cc1:SideBySideBarSeriesView ColorEach="True" FillStyle-FillMode="Gradient"></cc1:SideBySideBarSeriesView>
                                    </ViewSerializable>
                                </cc1:Series>
                            </SeriesSerializable>
                            <BorderOptions Visibility="False" />
                            <Titles>
                                <cc1:ChartTitle Text="Empacado Rocedes Por Cliente"></cc1:ChartTitle>
                                <cc1:ChartTitle Dock="Bottom" Alignment="Far" Text="From Rocedes Database Client" Font="Tahoma, 8pt" TextColor="Gray"></cc1:ChartTitle>
                            </Titles>
                            <DiagramSerializable>
                                <cc1:XYDiagram>
                                    <AxisX VisibleInPanesSerializable="-1">
                                    </AxisX>
                                    <AxisY VisibleInPanesSerializable="-1" Title-Text="Empaque" Title-Visibility="True">
                                    </AxisY>
                                </cc1:XYDiagram>
                            </DiagramSerializable>
                        </dx1:WebChartControl> 
                        <br />                        
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

