﻿using DevExpress.Web;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReporteEmpaque_EmpaqueUsuario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                string user = HttpContext.Current.User.Identity.Name.ToLower();
                var dt = biohorarioEmpaqueXUsuario(user);
                Session["BioEmpacado"] = dt;
                grdempaqueUser.DataBind();
                grdempaqueUser.ExpandAll();
            }
        }
        catch (Exception ex)
        {
            string res = ex.Message;

        }
    }

    DataTable biohorarioEmpaqueXUsuario(string user)
    {
        string query = " select pv.BuyerCode,pv.CreatedBy,pv.OrderNumber,pv.Style,pv.Fecha,isnull([1],0) as '08:00AM',isnull([2],0) as '10:00AM',isnull([3],0) as '12:00PM',isnull([4],0) as '02:00PM',isnull([5],0) as '04:00PM',isnull([6],0) as '06:00PM'"
                    + "	, isnull([1],0)+isnull([2],0)+isnull([3],0)+isnull([4],0)+isnull([5],0)+isnull([6],0) as Total, a.Unidades, a.Deficit from"
                    + " (select pkt.BuyerCode, op.OrderNumber, oi.Style, convert(date, pkt.CreateDate) as Fecha, sum(pkt.Qty) as UnidadesEmpacadas, pm.CreatedBy,"
                    + " case when CONVERT(time, pkt.CreateDate) < '08:01:01.1000000' then 1 else"
                    + "	case when CONVERT(time, pkt.CreateDate) < '10:01:01.1000000' then 2 else"
                    + "	case when CONVERT(time, pkt.CreateDate) < '12:01:01.1000000' then 3 else"
                    + "	case when CONVERT(time, pkt.CreateDate) < '14:01:01.1000000' then 4 else"
                    + "	case when CONVERT(time, pkt.CreateDate) < '16:01:01.1000000' then 5 else"
                    + "	case when CONVERT(time, pkt.CreateDate) < '23:50:01.1000000' then 6 end end end end end end  as Bihorario"
                    + "	from PackMaster pm join OrderPackItemTracking pkt on pm.SsccNumber = pkt.SsccNumber join OrderPackItem op on op.SsccNumber = pkt.SsccNumber"
                    + "	join OrderItemInfo oi on op.OrderNumber = oi.OrderNumber and oi.version = 1 and oi.ItemId = 1 and oi.FactoryId = '904731'"
                    + "	where convert(date, pkt.CreateDate) = convert(date, getdate())"//and pm.CreatedBy = '"+ user +"'
                    + " 	group by pkt.BuyerCode,convert(date, pkt.CreateDate),oi.Style,op.OrderNumber,pm.CreatedBy,"
                    + " case when CONVERT(time, pkt.CreateDate) < '08:01:01.1000000' then 1 else"
                    + "	case when CONVERT(time, pkt.CreateDate) < '10:01:01.1000000' then 2 else"
                    + "	case when CONVERT(time, pkt.CreateDate) < '12:01:01.1000000' then 3 else"
                    + "	case when CONVERT(time, pkt.CreateDate) < '14:01:01.1000000' then 4 else"
                    + "	case when CONVERT(time, pkt.CreateDate) < '16:01:01.1000000' then 5 else"
                    + "	case when CONVERT(time, pkt.CreateDate) < '23:50:01.1000000' then 6 end end end end end end"
                    + " ) pvt"
                    + " pivot"
                    + " (sum(pvt.unidadesempacadas) for pvt.bihorario  in ([1],[2],[3],[4],[5],[6]) ) as pv"
                    + " join (select a.OrderNumber, a.Unidades, SUM(b.UnidadesEmpacadas) Empacadas, (SUM(ISNULL(b.UnidadesEmpacadas, 0)) - a.Unidades) Deficit"
                    + " from (SELECT OrderNumber, SUM([OrderQty]) Unidades FROM PackOne2.dbo.OrderItem where FactoryId = '904731' AND CtnCapInstruct != 0 AND Version = 1 group by OrderNumber) a join"
                    + " (select op.OrderNumber, sum(pkt.Qty) as UnidadesEmpacadas from PackMaster pm join OrderPackItemTracking pkt on pm.SsccNumber = pkt.SsccNumber join OrderPackItem op on op.SsccNumber = pkt.SsccNumber"
                    + " join OrderItemInfo oi on op.OrderNumber = oi.OrderNumber and oi.version = 1 and oi.ItemId = 1 and oi.FactoryId = '904731' where convert(date, pkt.CreateDate) <= convert(date, getdate())"
                    + " group by pkt.BuyerCode, op.OrderNumber) b on a.OrderNumber = b.OrderNumber group by a.OrderNumber, a.Unidades) a on a.OrderNumber = pv.OrderNumber"
                    + " order by(isnull([1],0)+isnull([2],0)+isnull([3],0)+isnull([4],0)+isnull([5],0)+isnull([6],0)) desc";

        DataTable dt = new DataTable();
        SqlConnection cn1 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CN1"].ConnectionString);//Carhartt
        cn1.Open();
        SqlCommand cmd = new SqlCommand(query, cn1);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        cn1.Close();        

        for (int i = dt.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = dt.Rows[i];
            if (dr["CreatedBy"].ToString() != user.Trim())
                dr.Delete();
        }
        dt.AcceptChanges();

        return dt;

    }

    protected void grdempaqueUser_DataBinding(object sender, EventArgs e)
    {
        grdempaqueUser.DataSource = Session["BioEmpacado"];
    }

    protected void grdempaqueUser_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdempaqueUser.DataBind();
        grdempaqueUser.ExpandAll();

    }

    protected void btnGenerar_Click(object sender, EventArgs e)
    {
        try
        {
            string user = HttpContext.Current.User.Identity.Name.ToLower();
            var dt = biohorarioEmpaqueXUsuario(user);
            Session["BioEmpacado"] = dt;
            grdempaqueUser.DataBind();
            grdempaqueUser.ExpandAll();
        }
        catch (Exception)
        {

            // throw;
        }

    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/EmpaqueUsuario.aspx");
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);


            export.GridViewID = "grdempaqueUser";

            link3.Component = export;


            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link3 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Empacado";
                options.RawDataMode = true;
                options.ShowGridLines = true;
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Empacado" + DateTime.Now.ToShortDateString() + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {
            //throw;
        }
    }
}