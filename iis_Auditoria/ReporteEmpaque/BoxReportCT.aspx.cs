﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReporteEmpaque_BoxReportCT : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["BCT"] = null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top 15 corteCompleto from tbBultosCodigosBarra where corteCompleto like '%" + pre + "%' group by corteCompleto", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porder = dt.Rows[i][0].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porder { get; set; }
    }

    void Carga2()
    {
        var query = "";

        DataTable dt = new DataTable();

        query = " select corteCompleto, Style, talla, Cantidad, Cantidad -Restante Escaneado, (Cantidad - Restante) - Cantidad Deficit"
              + " from tbBultosCodigosBarra where corteCompleto = '" + txtcorte.Text.Trim().ToUpper() + "'";

        SqlConnection cn1 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);
        cn1.Open();
        SqlCommand cmd = new SqlCommand(query, cn1);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        Session["BCT"] = dt;
        cn1.Close();
        grdCorteT.DataSource = Session["BCT"];
        grdCorteT.DataBind();
    }

    protected void grdCorteT_DataBinding(object sender, EventArgs e)
    {
        grdCorteT.DataSource = Session["BCT"];
        grdCorteT.GroupBy(grdCorteT.Columns["corteCompleto"]);
    }

    protected void grdCorteT_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdCorteT.DataBind();
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            Carga2();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnexportar_Click(object sender, EventArgs e)
    {
        try
        {
            exportar2.GridViewID = "grdCorteT";
            exportar2.FileName = "Reporte_Box-" + DateTime.Now;
            exportar2.WriteXlsxToResponse();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/BoxReportCT.aspx");
    }

    protected void btnreg_Click(object sender, EventArgs e)
    {
        Response.Redirect("../ReporteEmpaque/BoxReport.aspx");
    }
}