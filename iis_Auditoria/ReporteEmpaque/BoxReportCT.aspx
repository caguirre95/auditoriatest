﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BoxReportCT.aspx.cs" Inherits="ReporteEmpaque_BoxReportCT" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "BoxReport.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porder);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>    

    <script type="text/javascript">
        function alertSeleccion() {
            swal({
                title: '!Sin Selección!',
                text: 'Debe seleccionar un cliente de la lista desplegable',
                type: 'info'
            });
        }
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <style>
        .espacio {
            margin: 10px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <br />
    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px">
            <div class="panel-heading"><strong>Reporte de Empaque</strong></div>
            <div class="panel-body">
                <div class="col-lg-12">                   

                    <div class="col-lg-5">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                Corte
                                <asp:TextBox ID="txtcorte" runat="server" CssClass="form-control" placeholder="Digite el corte" AutoCompleteType="Disabled"></asp:TextBox>                               
                                <hr />
                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-primary form-control espacio" Text="Generar Reporte" OnClick="btngenerar_Click" />

                                <asp:Button ID="btnexportar" runat="server" CssClass="btn btn-success form-control espacio" Text="Exportar Reporte" OnClick="btnexportar_Click" />
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-7">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">                               

                                <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-warning form-control espacio" Text="Limpiar Formulario" OnClick="btnlimpiar_Click" />

                                <asp:Button ID="btnreg" runat="server" CssClass="btn btn-info form-control espacio" Text="Regresar Formulario Anterior" OnClick="btnreg_Click" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <hr />                   

                    <asp:Panel ID="panel2" runat="server" Visible="true">
                        <dx:ASPxGridView ID="grdCorteT" runat="server" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="true" OnDataBinding="grdCorteT_DataBinding" OnCustomCallback="grdCorteT_CustomCallback">
                            <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="corteCompleto" Caption="Corte" VisibleIndex="0" Settings-AllowCellMerge="True">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Style" Caption="Estilo" VisibleIndex="1" Settings-AllowCellMerge="True">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="talla" Caption="Talla" VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Cantidad" Caption="Cantidad" VisibleIndex="3">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Escaneado" Caption="Escaneado" VisibleIndex="4">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Deficit" Caption="Deficit" VisibleIndex="5">
                                </dx:GridViewDataTextColumn>                                
                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                            <Settings ShowFooter="true" />
                            <SettingsPager PageSize="50" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="talla" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Cantidad" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Escaneado" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>                            
                        </dx:ASPxGridView>
                    </asp:Panel>                    
                    <dx:ASPxGridViewExporter ID="exportar2" runat="server"></dx:ASPxGridViewExporter>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

