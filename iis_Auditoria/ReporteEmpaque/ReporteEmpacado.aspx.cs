﻿using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReporteEmpaque_ReporteEmpacado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtfecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["empacado"] = null;
            panelcarhartt.Visible = false;
            paneldennis.Visible = false;
            paneldickies.Visible = false;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/ReporteEmpacado.aspx");
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            if (drpcliente.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccion();", true);
                return;
            }

            var query = "";
            DataTable dt = new DataTable();
            if (drpcliente.SelectedValue == "1")
            {
                panelcarhartt.Visible = true;
                paneldennis.Visible = false;
                paneldickies.Visible = false;


                if (txtCorte.Text == "")
                {
                    query = " select pkt.BuyerCode ,op.OrderNumber,oi.Style,convert(date, CreateDate) as Fecha ,sum(pkt.Qty) as UnidadesEmpacadas"
                           + " from OrderPackItemTracking pkt join OrderPackItem op on op.SsccNumber = pkt.SsccNumber"
                           + " join OrderItemInfo oi on op.OrderNumber = oi.OrderNumber and oi.version = 1 and oi.ItemId = 1 and oi.FactoryId = '904731'"
                           + " where convert(date, pkt.CreateDate) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by pkt.BuyerCode,convert(date, pkt.CreateDate),oi.Style,op.OrderNumber"
                           + " order by sum(pkt.Qty) desc";
                }
                else
                {
                    query = " select pkt.BuyerCode ,op.OrderNumber,oi.Style,convert(date, CreateDate) as Fecha ,sum(pkt.Qty) as UnidadesEmpacadas"
                           + " from OrderPackItemTracking pkt join OrderPackItem op on op.SsccNumber = pkt.SsccNumber"
                           + " join OrderItemInfo oi on op.OrderNumber = oi.OrderNumber and oi.version = 1 and oi.ItemId = 1 and oi.FactoryId = '904731'"
                           + " where op.OrderNumber = '" + txtCorte.Text + "'"
                           + " group by pkt.BuyerCode,convert(date, pkt.CreateDate),oi.Style,op.OrderNumber"
                           + " order by convert(date, pkt.CreateDate)";
                }

                SqlConnection cn1 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CN1"].ConnectionString);//Carhart
                cn1.Open();
                SqlCommand cmd = new SqlCommand(query, cn1);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                Session["empacado"] = dt;
                cn1.Close();
                grdempaque3.DataSource = Session["empacado"];
                grdempaque3.DataBind();

            }
            else if (drpcliente.SelectedValue == "2")
            {
                panelcarhartt.Visible = false;
                paneldennis.Visible = true;
                paneldickies.Visible = false;

                if (txtCorte.Text == "")
                {
                    query = " select o.buyer_code Cliente, o.order_number Corte, o.style_number Estilo, sum(c.packed_qty) Empacado, convert(nvarchar,c.pack_date,23) Fecha from order_head o "
                                              + " join carton c on c.order_id = o.order_id where convert(nvarchar,c.pack_date,23) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by o.buyer_code, o.order_number, o.style_number,convert(nvarchar,c.pack_date,23)";
                }
                else
                {
                    query = " select o.buyer_code Cliente, o.order_number Corte, o.style_number Estilo, sum(c.packed_qty) Empacado, convert(nvarchar,c.pack_date,23) Fecha"
                           + " from order_head o"
                           + " join carton c on c.order_id = o.order_id"
                           + " where o.order_number = '" + txtCorte.Text + "'"
                           + " group by o.buyer_code, o.order_number, o.style_number,convert(nvarchar, c.pack_date, 23)"
                           + " order by o.order_number, o.style_number,convert(nvarchar, c.pack_date, 23)";
                }

                SqlConnection cn2 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CN2"].ConnectionString);//Dennis
                cn2.Open();
                SqlCommand cmd = new SqlCommand(query, cn2);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                Session["empacado"] = dt;
                cn2.Close();
                grdempaque2.DataSource = Session["empacado"];
                grdempaque2.DataBind();
            }
            else if (drpcliente.SelectedValue == "3")
            {
                panelcarhartt.Visible = false;
                paneldennis.Visible = false;
                paneldickies.Visible = true;

                if (txtCorte.Text == "")
                {
                    query = " select 'Dickies' Cliente, bl.WorkOrderID, w.Style, SUM(bl.PackQty) Empacado,  CONVERT(date,b.PackDate) Fecha"
                           + " from WorkOrder w"
                           + " join BaleLineItem bl on bl.WorkOrderID = w.WorkOrderId join Bale b on b.BaleID = bl.BaleID"
                           + " where CONVERT(date, b.PackDate) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by bl.WorkOrderID, w.Style, CONVERT(date,b.PackDate)";
                }
                else
                {
                    query = " select 'Dickies' Cliente, bl.WorkOrderID, w.Style, SUM(bl.PackQty) Empacado,  CONVERT(date,b.PackDate) Fecha from WorkOrder w"
                           + " join BaleLineItem bl on bl.WorkOrderID = w.WorkOrderId join Bale b on b.BaleID = bl.BaleID"
                           + " where bl.WorkOrderID = '" + txtCorte.Text + "'"
                           + " group by bl.WorkOrderID, w.Style,CONVERT(date, b.PackDate)";
                }

                SqlConnection cn3 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CN3"].ConnectionString);//Dickies
                cn3.Open();
                SqlCommand cmd = new SqlCommand(query, cn3);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                Session["empacado"] = dt;
                cn3.Close();
                grdempaque1.DataSource = Session["empacado"];
                grdempaque1.DataBind();
            }

        }
        catch (Exception ex)
        {
            string m = ex.Message;
            //throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

            if (drpcliente.SelectedValue == "1")
            {
                export.GridViewID = "grdempaque3";

                link3.Component = export;
            }
            else if (drpcliente.SelectedValue == "2")
            {
                export.GridViewID = "grdempaque2";

                link3.Component = export;
            }
            else if (drpcliente.SelectedValue == "3")
            {
                export.GridViewID = "grdempaque1";

                link3.Component = export;
            }

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link3 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Empacado";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Empacado" + DateTime.Now.ToShortDateString() + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {
            //throw;
        }
    }

    protected void grdempaque1_DataBinding(object sender, EventArgs e)
    {
        grdempaque1.DataSource = Session["empacado"];
    }

    protected void grdempaque1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdempaque1.DataBind();
    }

    protected void grdempaque2_DataBinding(object sender, EventArgs e)
    {
        grdempaque2.DataSource = Session["empacado"];
    }

    protected void grdempaque2_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdempaque2.DataBind();
    }

    protected void grdempaque3_DataBinding(object sender, EventArgs e)
    {
        grdempaque3.DataSource = Session["empacado"];
    }

    protected void grdempaque3_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdempaque3.DataBind();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top 20 p.Id_cliente,p.POrder from POrder p where p.POrder like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idcliente = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][1].ToString();


                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public int idcliente { get; set; }
        public string porder { get; set; }

    }

}