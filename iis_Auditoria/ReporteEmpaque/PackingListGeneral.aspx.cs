﻿using DevExpress.Web;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReporteEmpaque_PackingListGeneral : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Session["dt"] = null;

                txtfecha1.Text = DateTime.Now.ToShortDateString();
                txtfecha2.Text = DateTime.Now.ToShortDateString();

                //System.Globalization.CultureInfo norwCulture = System.Globalization.CultureInfo.CreateSpecificCulture("es");
                //System.Globalization.Calendar cal = norwCulture.Calendar;
                //int x = cal.GetWeekOfYear(DateTime.Now, norwCulture.DateTimeFormat.CalendarWeekRule, norwCulture.DateTimeFormat.FirstDayOfWeek);

                //var list = new List<ClassArrm>();
                //for (int i = 1; i <= x; i++)
                //{
                //    ClassArrm obj = new ClassArrm();
                //    obj.id = i;
                //    obj.semana = "Semana " + i;
                //    list.Add(obj);
                //}

                //drpSemana.DataSource = list;
                //drpSemana.DataTextField = "semana";
                //drpSemana.DataValueField = "id";
                //drpSemana.DataBind();

                //drpSemana.SelectedValue = (x).ToString();

                getgrid();
            }

            gridpack.SettingsDetail.ExportMode = GridViewDetailExportMode.Expanded;// Enum.Parse(typeof(GridViewDetailExportMode), ddlExportMode.Text);
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void gridpack_DataBinding(object sender, EventArgs e)
    {
        try
        {
            gridpack.DataSource = Session["dtHG"];
            gridpack.GroupBy(gridpack.Columns["Planta"]);
            gridpack.GroupBy(gridpack.Columns["Linea"]);

        }
        catch (Exception ex)
        {
            string j = ex.Message;
        }
    }

    protected void gridpack_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridpack.DataBind();
    }

    void getgrid()
    {
        if (txtfecha1.Text != "" && txtfecha2.Text != "")
        {
            Session["dtHG"] = null;

            Session["f1"] = DateTime.Parse(txtfecha1.Text);

            Session["f2"] = DateTime.Parse(txtfecha2.Text);

            var dt = OrderDetailDA.ReporteEmpaqueGeneralXFecha(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text));

            Session["dtHG"] = dt;
            gridpack.DataBind();
            gridpack.ExpandAll();

        }

    }

    protected void btnExport_Click(object sender, EventArgs e)
    {

        ASPxGridViewExporter1.GridViewID = "gridpack";

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link1.Component = ASPxGridViewExporter1;


        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link1 });

        compositeLink.CreatePageForEachLink();

        using (MemoryStream stream = new MemoryStream())
        {

            XlsxExportOptions options = new XlsxExportOptions();

            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
            options.SheetName = "ReporteEmpaque";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=Reporte Empaque.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        ps.Dispose();

    }

    protected void grid_Load(object sender, EventArgs e)
    {

    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            getgrid();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void gridtallas_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        string corte = (string)detailGrid.GetMasterRowKeyValue();

        var dt = OrderDetailDA.ReporteEmpaquetallasXcorte(corte);

        detailGrid.DataSource = dt;
    }

    protected void gridBox_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        string corte = (string)detailGrid.GetMasterRowKeyValue();

        var dt = OrderDetailDA.ReporteEmpaqueCajasXcorte(corte);

        detailGrid.DataSource = dt;

    }

    protected void ASPxGridView1_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        string corte = (string)detailGrid.GetMasterRowKeyValue();

        var f1 = (DateTime)Session["f1"];
        var f2 = (DateTime)Session["f2"];

        var dt = OrderDetailDA.ReporteEmpaquePacklistXcorte(corte, f1, f2);

        detailGrid.DataSource = dt;

    }


}