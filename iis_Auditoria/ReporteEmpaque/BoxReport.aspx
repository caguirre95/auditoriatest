﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BoxReport.aspx.cs" Inherits="ReporteEmpaque_BoxReport" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "BoxReport.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porder);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script type="text/javascript">
        function alertSeleccion() {
            swal({
                title: '!Sin Selección!',
                text: 'Debe seleccionar un cliente de la lista desplegable',
                type: 'info'
            });
        }
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <style>
        .espacio {
            margin: 10px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <br />
    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px">
            <div class="panel-heading"><strong>Reporte de Empaque</strong></div>
            <div class="panel-body">
                <div class="col-lg-12">

                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="rd" runat="server">
                                    <asp:ListItem Value="1">Por Corte</asp:ListItem>
                                    <asp:ListItem Value="2">Rango de Fecha</asp:ListItem>                                    
                                </asp:RadioButtonList>
                                <hr />
                                <asp:Button ID="btnCT" runat="server" Text=" Reporte Corte - Talla" CssClass="btn btn-primary" OnClick="btnCT_Click" />
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                Corte
                                <asp:TextBox ID="txtcorte" runat="server" CssClass="form-control" placeholder="Digite el corte" AutoCompleteType="Disabled"></asp:TextBox>

                                <hr />

                                Fecha Inicial
                                <asp:TextBox ID="txtfecha1" runat="server" CssClass="form-control"></asp:TextBox>

                                Fecha Final
                                <asp:TextBox ID="txtfecha2" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-primary form-control espacio" Text="Generar Reporte" OnClick="btngenerar_Click" />

                                <asp:Button ID="btnexportar" runat="server" CssClass="btn btn-success form-control espacio" Text="Exportar Reporte" OnClick="btnexportar_Click" />

                                <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-warning form-control espacio" Text="Limpiar Formulario" OnClick="btnlimpiar_Click" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <hr />
                    <asp:Panel ID="panel1" runat="server" Visible="true">
                        <dx:ASPxGridView ID="grdbox" runat="server" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="true" OnDataBinding="grdbox_DataBinding" OnCustomCallback="grdbox_CustomCallback">
                            <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="corte" Caption="Corte" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Style" Caption="Estilo" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="talla" Caption="Talla" VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Box" Caption="Box" VisibleIndex="3">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Cantidad" VisibleIndex="4">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Fecha" VisibleIndex="5">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Hora" VisibleIndex="6">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                            <Settings ShowFooter="true" />
                            <SettingsPager PageSize="50" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="Cantidad" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <GroupSummary>
                                <dx:ASPxSummaryItem FieldName="Cantidad" ShowInGroupFooterColumn="Cantidad" SummaryType="Sum" DisplayFormat="{0}" />
                            </GroupSummary>
                        </dx:ASPxGridView>
                    </asp:Panel>   
                    <dx:ASPxGridViewExporter ID="exportar" runat="server"></dx:ASPxGridViewExporter>                    
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

