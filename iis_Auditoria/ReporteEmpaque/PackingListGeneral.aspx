﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PackingListGeneral.aspx.cs" Inherits="ReporteEmpaque_PackingListGeneral" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Histórico de Corte</strong></div>
            <div class="panel-body">

                <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                   
                    Start Date
                            <div class="input-group">

                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>

                            </div>
                                End Date
                          
                                <div class="input-group">
                                    <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                    <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                   <%-- <asp:DropDownList ID="drpSemana" CssClass="form-control" Visible="false" runat="server" Width="200px" />--%>
                    <br />
                    <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control" Text="Generar" Width="200px" OnClick="btngenerar_Click" />

                    <asp:Button ID="btnExport" OnClick="btnExport_Click" CssClass="btn btn-success form-control a" runat="server" Text="Export Excel" Width="200px" />
                    <hr />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow-x: auto">
                    <dx:ASPxGridView ID="gridpack" runat="server" KeyFieldName="Corte" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnDataBinding="gridpack_DataBinding" OnCustomCallback="gridpack_CustomCallback">
                        <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                        <Columns>                                            
                            <dx:GridViewDataTextColumn FieldName="Linea" Caption="Linea" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Corte" Caption="Corte" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Color" Caption="Color" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Cantidad" Caption="Cantidad" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="deficitLinea" Caption="Deficit Empaque" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="cajas" Caption="Cajas" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                           <%-- <dx:GridViewDataTextColumn FieldName="unidadesAnterior" Caption="Unidades Acumuladas" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>--%>
                            <dx:GridViewDataTextColumn FieldName="unidadesEnFecha" Caption="Unidades Empaquadas" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="primeras" Caption="Primeras" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="PorcentajePrimera" Caption="% de Primeras" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="segundas" Caption="Segundas" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="PorcentajeSegundas" Caption="% de Segundas" VisibleIndex="12">
                            </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn FieldName="pedientes" Caption="Pendientes" VisibleIndex="13">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="PorcentajePendiente" Caption="% de Pendientes" VisibleIndex="14">
                            </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn FieldName="TotalEmpaque" Caption="Total Empaque" VisibleIndex="15">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="PorcentajeEmpaque" Caption="% de Empaque" VisibleIndex="16">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <Templates>
                            <DetailRow>
                                <div style="padding: 3px 3px 2px 3px">
                                    <dx:ASPxPageControl runat="server" ID="pageControl" Width="100%" EnableCallBacks="true">
                                        <TabPages>
                                             
                                            <dx:TabPage Text="Packing List" Visible="true">
                                                <ContentCollection>
                                                    <dx:ContentControl runat="server">
                                                        <dx:ASPxGridView ID="ASPxGridView1" runat="server" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnBeforePerformDataSelect="ASPxGridView1_BeforePerformDataSelect"
                                                            KeyFieldName="codigobox" Width="100%">
                                                            <Columns>                                                              
                                                                <dx:GridViewDataColumn FieldName="codigobox" Caption="Carton ID" Settings-AllowCellMerge="true"/>
                                                                <dx:GridViewDataColumn FieldName="estilo" Caption="Item ID" Settings-AllowCellMerge="false"/>
                                                                <dx:GridViewDataColumn FieldName="color" Caption="Color" Settings-AllowCellMerge="false"/>
                                                                <dx:GridViewDataColumn FieldName="talla" Caption="Size" Settings-AllowCellMerge="false"/>
                                                                <dx:GridViewDataColumn FieldName="total" Caption="Carton Qty" Settings-AllowCellMerge="false"/>
                                                                <dx:GridViewDataColumn FieldName="calidad" Caption="Deficit" Settings-AllowCellMerge="False"/>
                                                            </Columns>
                                                            <SettingsBehavior AllowCellMerge="true" />
                                                            <SettingsPager EnableAdaptivity="true" />
                                                            <Settings ShowFooter="true"  />
                                                            <TotalSummary>
                                                                
                                                                <dx:ASPxSummaryItem FieldName="codigobox" SummaryType="Count" DisplayFormat="{0}" />
                                                                <dx:ASPxSummaryItem FieldName="total" SummaryType="Sum" DisplayFormat="{0}" />
                                                            </TotalSummary>
                                                        </dx:ASPxGridView>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>

                                            <dx:TabPage Text="Tallas" Visible="true">
                                                <ContentCollection>
                                                    <dx:ContentControl runat="server">
                                                        <dx:ASPxGridView ID="gridtallas" runat="server" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnBeforePerformDataSelect="gridtallas_BeforePerformDataSelect"
                                                             Width="100%">
                                                            <Columns>
                                                                <dx:GridViewDataColumn FieldName="talla" Caption="Tallas" />
                                                                <dx:GridViewDataColumn FieldName="Cantidad" Caption="Unidades" />
                                                                <dx:GridViewDataColumn FieldName="Escaneado" Caption="Unidades Escaneadas" />
                                                                <dx:GridViewDataColumn FieldName="deficitXtalla" Caption="Deficit" />
                                                            </Columns>
                                                            <SettingsPager EnableAdaptivity="true" />
                                                            <Settings ShowFooter="true"  />
                                                            <TotalSummary>
                                                                <dx:ASPxSummaryItem FieldName="cantidadagregada" SummaryType="Sum" DisplayFormat="{0}" />

                                                            </TotalSummary>
                                                        </dx:ASPxGridView>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>

                                            <dx:TabPage Text="Box" Visible="true">
                                                <ContentCollection>
                                                    <dx:ContentControl runat="server">
                                                        <dx:ASPxGridView ID="gridBox" runat="server" Theme="Metropolis" SettingsBehavior-AllowSort="false"  OnBeforePerformDataSelect="gridBox_BeforePerformDataSelect"
                                                            Width="100%">
                                                            <Columns>
                                                                <dx:GridViewDataColumn FieldName="secBoxXCorte" Caption="Sec" Settings-AllowCellMerge="False" />
                                                                <dx:GridViewDataColumn FieldName="codigoBox" Caption="Codigo Box" Settings-AllowCellMerge="False" />
                                                                <dx:GridViewDataColumn FieldName="unidades" Caption="Unidades" Settings-AllowCellMerge="False" /> 
                                                                <dx:GridViewDataColumn FieldName="estatus" Caption="Calidad" Settings-AllowCellMerge="False" />            
                                                            </Columns>
                                                            <SettingsBehavior AllowCellMerge="true" />
                                                            <SettingsPager EnableAdaptivity="true" />
                                                            <Settings ShowFooter="true"  />
                                                            <TotalSummary>
                                                                <dx:ASPxSummaryItem FieldName="Unidades" SummaryType="Sum" DisplayFormat="{0}" />
                                                            </TotalSummary>
                                                        </dx:ASPxGridView>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                        </TabPages>
                                    </dx:ASPxPageControl>
                                </div>
                            </DetailRow>
                        </Templates>
                        <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                        <Settings ShowFooter="true" HorizontalScrollBarMode="Visible" />
                        <SettingsPager PageSize="100" />
                        <SettingsBehavior EnableCustomizationWindow="true" />
                        <SettingsDetail ShowDetailRow="true" />
                    </dx:ASPxGridView>
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                </div>
            </div>

        </div>

    </div>
    
    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

