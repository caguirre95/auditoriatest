﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdministracionEmpaque.aspx.cs" Inherits="ReporteEmpaque_AdministracionEmpaque" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "AdministracionEmpaque.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    idorder: item.idorder,
                                    porder: item.porder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);
                    $('#<%=txtestilo.ClientID%>').val(ui.item.style);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <style>
        .sp {
            margin-top: 15px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <br />
        <div class="panel panel-primary">
            <div class="panel-heading">Administracion Corte Empaque</div>
            <div class="panel-body">

                <div class="col-lg-12">

                    <div class="col-lg-4">
                        <asp:RadioButtonList ID="rd1" runat="server">
                            <asp:ListItem Value="1">Anular Box</asp:ListItem>
                            <asp:ListItem Value="2">Modificar Cantidad Box</asp:ListItem>
                            <asp:ListItem Value="3">Eliminar Registros del Corte</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>

                    <div class="col-lg-4">
                        Corte
                        <div class="input-group">
                            <span class="input-group-btn">
                                <asp:LinkButton Text="" CssClass="btn btn-default" OnClick="LinkButton1_Click" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                            </span>
                            <div class="ui-widget" style="text-align: left;">
                                <asp:TextBox ID="txtcorte" runat="server" placeholder="Corte..." AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <asp:HiddenField ID="hfidPorder" runat="server" />
                        Talla
                        <asp:DropDownList ID="drptallas" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                        Estilo
                        <asp:TextBox ID="txtestilo" runat="server" placeholder="Estilo..." AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>
                    </div>

                    <div class="col-lg-4">
                        <asp:Button ID="btnbuscar" runat="server" Text="Buscar" CssClass="btn btn-primary form-control sp" OnClick="btnbuscar_Click" />

                        <asp:Button ID="btnlimpiar" runat="server" Text="Limpiar" CssClass="btn btn-success form-control sp" OnClick="btnlimpiar_Click" />

                        <asp:Button ID="btneliinar" runat="server" Text="Eliminar Registro del Corte General" CssClass="btn btn-danger form-control sp" OnClick="btneliinar_Click" />
                    </div>

                    <div class="col-lg-12">
                        <hr />
                        <asp:GridView ID="grdboxn" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" CssClass="table table-hover table-striped w" AutoGenerateColumns="false" GridLines="None">
                            <Columns>
                                <asp:TemplateField Visible="true">
                                    <HeaderTemplate>
                                        Box 
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblbox" runat="server" Font-Size="Medium" CssClass="mar_t label label-default" Text='<%#Bind("codigoBox") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false" ItemStyle-CssClass="padd_t">
                                    <HeaderTemplate>
                                        Estilo 
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblestilo" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("estilo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                    <HeaderTemplate>
                                        Generado
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblgenerado" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("Generado") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                    <HeaderTemplate>
                                        Impresiones 
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblnimpresion" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("numeroImpresion") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Impreso
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblimpreso" runat="server" Font-Size="Medium" CssClass="mar_t label label-success" Text='<%#Bind("Impreso") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Clasificacion
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblclasi" runat="server" Font-Size="Medium" CssClass="mar_t label label-info" Text='<%#Bind("clasificacion") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>                                        
                                        <asp:CheckBox ID="checkanu" Text="Anular" Checked="true" CssClass="btn btn-default" runat="server" />
                                        <asp:LinkButton Text="" ID="lnkanu" OnClick="lnkanu_Click" CssClass="btn btn-warning" runat="server"><i class="glyphicon glyphicon-check"> </i> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>

                </div>

            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

