﻿using DevExpress.XtraCharts;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReporteEmpaque_GraficoEmpacado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtfecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["princ"] = null;
        }
    }

    void CargarDatos()
    {
        var queryC = "";
        var queryD = "";
        var queryV = "";

        DataTable dtC = new DataTable();
        DataTable dtD = new DataTable();
        DataTable dtV = new DataTable();
        DataTable dtPrincp = new DataTable();

        dtC.Columns.Add("Cliente", typeof(string));
        dtC.Columns.Add("Empacado", typeof(int));

        dtD.Columns.Add("Cliente", typeof(string));
        dtD.Columns.Add("Empacado", typeof(int));

        dtV.Columns.Add("Cliente", typeof(string));
        dtV.Columns.Add("Empacado", typeof(int));

        dtPrincp.Columns.Add("Cliente", typeof(string));
        dtPrincp.Columns.Add("Empacado", typeof(int));

        queryC = " select pkt.BuyerCode Cliente, sum(pkt.Qty) as Empacado from OrderPackItemTracking pkt join OrderPackItem op on op.SsccNumber = pkt.SsccNumber"
               + " join OrderItemInfo oi on op.OrderNumber = oi.OrderNumber and oi.version = 1 and oi.ItemId = 1 and oi.FactoryId = '904731'"
               + " where convert(date, pkt.CreateDate) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by pkt.BuyerCode,oi.Style";

        SqlConnection cn1 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CN1"].ConnectionString);//Carhart
        cn1.Open();
        SqlCommand cmd = new SqlCommand(queryC, cn1);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dtC);
        cn1.Close();

        var Empaqueprod = (
                       from p in dtC.AsEnumerable()
                       group p by p.Field<string>("Cliente") into g
                       select new
                       {
                           Cliente = g.Key,
                           Empacado = g.Sum(x => x.Field<int>("Empacado"))
                       }).ToList();

        dtC.Rows.Clear();

        foreach( var item in Empaqueprod)
        {
            DataRow r = dtC.NewRow();
            r["Cliente"] = item.Cliente;
            r["Empacado"] = item.Empacado;
            dtC.Rows.Add(r);
        }

        queryD = " select o.buyer_code Cliente, sum(c.packed_qty) Empacado from order_head o join carton c on c.order_id = o.order_id"
               + " where convert(nvarchar, c.pack_date,23) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by o.buyer_code";

        SqlConnection cn2 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CN2"].ConnectionString);//Dennis
        cn2.Open();
        SqlCommand cmd1 = new SqlCommand(queryD, cn2);
        SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
        da1.Fill(dtD);
        cn2.Close();

        queryV = " select 'VF - Dickies' Cliente, count(cbs.codigoBox) Empacado from tbCodigosBarraScan cbs"
               + " where CAST(cbs.fechaEscaneado AS DATE) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'";

        SqlConnection cn3 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);
        cn3.Open();
        SqlCommand cmd3 = new SqlCommand(queryV, cn3);
        SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
        da3.Fill(dtV);
        cn3.Close();

        dtPrincp.Merge(dtC);
        dtPrincp.Merge(dtD);
        dtPrincp.Merge(dtV);

        Session["princ"] = dtPrincp;

        wcClienteEmp.DataBind();
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            CargarDatos();
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void btnexportar_Click(object sender, EventArgs e)
    {
        try
        {
            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

            wcClienteEmp.DataSource = Session["princ"];
            wcClienteEmp.DataBind();
            link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)wcClienteEmp).Chart;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link2 });

            compositeLink.CreatePageForEachLink();
            //compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Report_Empaque";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=ReporteEmpaque.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/GraficoEmpacado.aspx");
    }

    protected void wcClienteEmp_DataBinding(object sender, EventArgs e)
    {
        wcClienteEmp.DataSource = Session["princ"];        
    }

    protected void wcClienteEmp_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    {      
        wcClienteEmp.DataBind();
    }    
}