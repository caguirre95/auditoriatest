﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReporteEmpaque_AdministracionEmpaque : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top 20 corte, estilo, unidades from tbPorderSinGuion where corte like '%" + pre + "%' order by LEN(corte) asc", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                //obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][0].ToString();
                obj.style = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public int idorder { get; set; }
        public string porder { get; set; }
        public string style { get; set; }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            if (hfidPorder.Value != string.Empty)
            {
                int idporder = int.Parse(hfidPorder.Value);

                string queryBundle = "select codigoBarra,talla from tbBultosCodigosBarra where corteCompleto = '" + txtcorte.Text.Trim() + "' order by talla asc";

                DataTable dt_Bundle = DataAccess.Get_DataTable(queryBundle);

                drptallas.DataSource = dt_Bundle;
                drptallas.DataTextField = "talla";
                drptallas.DataValueField = "codigoBarra";
                drptallas.DataBind();
                drptallas.Items.Insert(0, "Select...");
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/AdministracionEmpaque.aspx");
    }

    protected void btneliinar_Click(object sender, EventArgs e)
    {
        try
        {

        }
        catch (Exception)
        {

            throw;
        }
    }

    void BoxG()
    {
        try
        {
            DataTable dt;

            string query = " select codigoBox, estilo, CONVERT(date, fechaGenerado) Generado, numeroImpresion, CONVERT(date, fechaImpresion) Impreso, clasificacion"
                         + " from tbcodigosCajas where corteCompleto = '" + txtcorte.Text.Trim() + "' and estado = 1 order by secBoxXCorte";

            dt = DataAccess.Get_DataTable(query);

            grdboxn.DataSource = dt;
            grdboxn.DataBind();

        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void lnkanu_Click(object sender, EventArgs e)
    {
        try
        {

        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            string op1 = rd1.SelectedValue;

            switch (op1)
            {
                case "1":
                    if (txtcorte.Text != string.Empty)
                    {
                        BoxG();
                    }
                    break;
            }
        }
        catch (Exception)
        {
            //throw;
        }
    }
}