﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PackingListF.aspx.cs" Inherits="ReporteEmpaque_PackingListF" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <script lang="javascript" type="text/javascript">

        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "PackingListF.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,                               
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hdfcorte.ClientID%>').val(ui.item.porder);
                   <%-- $('#<%=txtStyle.ClientID%>').val(ui.item.idorder);--%>
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        })
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <br />
    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px">
            <div class="panel-heading"><strong>Reporte de Empaque</strong></div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <asp:LinkButton Text="" OnClick="LinkButton1_Click" CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                            </span>
                            <div class="ui-widget" style="text-align: left;">
                                <asp:TextBox ID="txtPorder" placeholder="Codigo de Corte" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="hdfcorte" />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:Button ID="btnexpprt" runat="server" CssClass="btn btn-success form-control" Text="Exportar" OnClick="btnexpprt_Click" />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:Button ID="btnlimp" runat="server" CssClass="btn btn-info form-control" Text="Limpiar" OnClick="btnlimp_Click" />
                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <hr />
                    <dx:ASPxGridView ID="grdp" runat="server" KeyFieldName="Id_Order" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnDataBinding="grdp_DataBinding" OnCustomCallback="grdp_CustomCallback">
                        <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                        <Columns>
                            <dx:GridViewBandColumn Caption="Packing List" HeaderStyle-HorizontalAlign="Center">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="Planta" Caption="Planta" VisibleIndex="1" Settings-AllowCellMerge="True">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Linea" Caption="Linea" VisibleIndex="2" Settings-AllowCellMerge="True">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo" VisibleIndex="3" Settings-AllowCellMerge="True">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Corte" Caption="Corte" VisibleIndex="4" Settings-AllowCellMerge="True">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Cantidad" VisibleIndex="5" Settings-AllowCellMerge="True">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Porcentajecorte" Caption="Empaque por Corte" VisibleIndex="6" Settings-AllowCellMerge="True">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Size" Caption="Talla" VisibleIndex="7" Settings-AllowCellMerge="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TotalTalla" Caption="Total por Talla" VisibleIndex="8" Settings-AllowCellMerge="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Escaneado" Caption="Escaneado" VisibleIndex="9" Settings-AllowCellMerge="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Balance" Caption="Balance" VisibleIndex="10" Settings-AllowCellMerge="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TotalBox" Caption="Cantidad de Box" VisibleIndex="11" Settings-AllowCellMerge="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Porcentaje" Caption="Empaque por Talla" VisibleIndex="12" Settings-AllowCellMerge="False">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:GridViewBandColumn>
                        </Columns>
                        <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                        <Settings ShowFooter="true" />
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="TotalTalla" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="Escaneado" SummaryType="Sum" DisplayFormat="{0}" />
                             <dx:ASPxSummaryItem FieldName="TotalBox" SummaryType="Sum" DisplayFormat="{0}" />
                        </TotalSummary>
                    </dx:ASPxGridView>
                     <dx:ASPxGridViewExporter ID="exportar1" runat="server"></dx:ASPxGridViewExporter>
                </div>

            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

