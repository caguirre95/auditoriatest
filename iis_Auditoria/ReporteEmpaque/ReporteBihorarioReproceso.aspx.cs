﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReporteEmpaque_ReporteBihorarioReproceso : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtdate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["dtR"] = null;
        }
    }

    void bioIng()
    {
        try
        {
            var bihor1 = OrderDetailDA.Bihorario_ReprocesoEmpaque(Convert.ToDateTime(txtdate.Text));

            if (bihor1.Rows.Count > 0)
            {
                Session["dtR"] = bihor1;
                grdbiho.DataSource = Session["dtR"];
                grdbiho.DataBind();
                grdbiho.GroupBy(grdbiho.Columns["Responsable"]);
                grdbiho.GroupBy(grdbiho.Columns["Reproceso"]);
            }            
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            bioIng();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            bioIng();

            exportar.GridViewID = "grdbiho";
            exportar.FileName = "Reporte de Bihorario " + DateTime.Now;
            exportar.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/ReporteBihorarioReproceso.aspx");
    }

    protected void grdbiho_DataBinding(object sender, EventArgs e)
    {
        grdbiho.DataSource = Session["dtR"];
    }

    protected void grdbiho_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdbiho.DataBind();
    }
}