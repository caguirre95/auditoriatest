﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EmpaqueSupervisor.aspx.cs" Inherits="ReporteEmpaque_EmpaqueSupervisor" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtFecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
                $('#<%=txtFecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>


    <script type="text/javascript">
        function alertSeleccion() {
            swal({
                title: '!Sin Selección!',
                text: 'Debe seleccionar un Usuario de la lista desplegable',
                type: 'info'
            });
        }

        function alertError(msg) {
            swal({
                title: 'Lo sentimos ha ocurrido un error',
                text: msg,
                type: 'error'
            });
        }
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <style>
        .sp {
            margin-top: 15px;
        }

        .mr {
            margin: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <br />
        <div class="panel panel-primary">
            <div class="panel-heading">Reporte Empacado Por Usuario</div>
            <div class="panel-body">

                <div class="col-lg-12">
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                    <asp:ListItem Value="1">Usuario</asp:ListItem>
                                    <asp:ListItem Value="2">Global</asp:ListItem>

                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    Usuarios 
                                    <asp:DropDownList ID="drpUsuarios" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    Fecha Inicio
                                    <asp:TextBox ID="txtFecha1" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    Fecha Final
                                    <asp:TextBox ID="txtFecha2" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <asp:Button ID="btnGenerar" runat="server" Text="Generar Informe" CssClass="btn btn-primary form-control mr" OnClick="btnGenerar_Click" />

                        <asp:Button ID="btnlimpiar" runat="server" Text="Limpiar" CssClass="btn btn-success form-control sp mr" OnClick="btnlimpiar_Click" />

                        <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control sp mr" Text="Export to Excel" OnClick="btnexcel_Click" />



                    </div>



                    <div class="col-lg-12">
                        <hr />
                        <%-- Styles-Header-Font-Bold="true"--%>
                        <asp:Panel ID="panelcarhartt" runat="server">
                            <dx:ASPxGridView ID="grdempaqueUser" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdempaqueUser_DataBinding" OnCustomCallback="grdempaqueUser_CustomCallback">
                                <Columns>
                                    <dx:GridViewBandColumn Caption="Reporte Empacado Carhartt" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="CreatedBy" Caption="Usuario" GroupIndex="0"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="OrderNumber" Caption="Corte"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Style" Caption="Estilo"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Unidades" Caption="Unidades"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha" PropertiesTextEdit-DisplayFormatString="dd-MM-yyyy"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="08:00AM" Caption="08:00AM"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="10:00AM" Caption="10:00am"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="12:00PM" Caption="12:00pm"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="02:00PM" Caption="02:00pm"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="04:00PM" Caption="04:00pm"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="06:00PM" Caption="06:00PM"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Total" Caption="Total"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="Deficit" Caption="Balance"></dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                </Columns>
                                <Settings VerticalScrollBarMode="Visible" />
                                <SettingsPager PageSize="100"></SettingsPager>
                                <Settings ShowFooter="true" />
                                <TotalSummary>
                                    <dx:ASPxSummaryItem FieldName="OrderNumber" SummaryType="Count" DisplayFormat="{0}" />
                                    <dx:ASPxSummaryItem FieldName="08:00AM" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx:ASPxSummaryItem FieldName="10:00AM" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx:ASPxSummaryItem FieldName="12:00PM" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx:ASPxSummaryItem FieldName="02:00PM" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx:ASPxSummaryItem FieldName="04:00PM" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx:ASPxSummaryItem FieldName="06:00PM" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx:ASPxSummaryItem FieldName="Total" SummaryType="Sum" DisplayFormat="total={0}" />
                                </TotalSummary>
                            </dx:ASPxGridView>
                        </asp:Panel>

                        <hr />

                        <dx:ASPxGridViewExporter ID="export" runat="server">
                            <Styles>
                                <Header Font-Bold="True">
                                </Header>
                                <Footer Font-Bold="True">
                                </Footer>
                            </Styles>
                        </dx:ASPxGridViewExporter>
                    </div>

                </div>

            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

