﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReporteEmpaque_BoxReportBihorario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["boxbiho"] = null;
        }
    }

    void Carga()
    {
        var query = "";

        DataTable dt = new DataTable();

        query = " select pv.corte, pv.Style, ISNULL(pv.[1], 0) '08:00AM', ISNULL(pv.[2], 0) '10:00AM', ISNULL(pv.[3], 0) '12:00PM', ISNULL(pv.[4], 0) '02:00PM', ISNULL(pv.[5], 0) '04:00PM',"
              + " ISNULL(pv.[6], 0) '06:00PM' ,isnull([1],0)+isnull([2],0)+isnull([3],0)+isnull([4],0)+isnull([5],0)+isnull([6],0) as Total from ("
              + " SELECT T.corteCompleto corte, T.Style, COUNT(CB.codigoBarra) AS Cantidad, CAST(CB.fechaEscaneado AS DATE) AS Fecha,"
              + " case when CONVERT(time, CB.fechaEscaneado) < '08:01:01.1000000' then 1 else case when CONVERT(time, CB.fechaEscaneado) < '10:01:01.1000000' then 2 else"
              + " case when CONVERT(time, CB.fechaEscaneado) < '12:01:01.1000000' then 3 else case when CONVERT(time, CB.fechaEscaneado) < '14:01:01.1000000' then 4 else"
              + " case when CONVERT(time, CB.fechaEscaneado) < '16:01:01.1000000' then 5 else case when CONVERT(time, CB.fechaEscaneado) < '23:50:01.1000000' then 6 end end end end end end Bihorario"
              + " FROM tbBultosCodigosBarra AS T"
              + " INNER JOIN tbcodigosCajas AS CC ON CC.corteCompleto = T.corteCompleto INNER JOIN tbCodigosBarraScan AS CB ON CB.codigoBarra = T.codigoBarra AND CB.codigoBox = CC.codigoBox"
              + " WHERE CAST(CB.fechaEscaneado AS DATE) = '" + txtfecha1.Text + "'"
              + " GROUP BY T.corteCompleto, T.Style, CAST(CB.fechaEscaneado AS DATE),"
              + " case when CONVERT(time, CB.fechaEscaneado) < '08:01:01.1000000' then 1 else case when CONVERT(time, CB.fechaEscaneado) < '10:01:01.1000000' then 2 else"
              + " case when CONVERT(time, CB.fechaEscaneado) < '12:01:01.1000000' then 3 else case when CONVERT(time, CB.fechaEscaneado) < '14:01:01.1000000' then 4 else"
              + " case when CONVERT(time, CB.fechaEscaneado) < '16:01:01.1000000' then 5 else case when CONVERT(time, CB.fechaEscaneado) < '23:50:01.1000000' then 6"
              + " end end end end end end) pvt pivot(sum(pvt.Cantidad) for pvt.bihorario  in ([1],[2],[3],[4],[5],[6]) ) as pv";

        SqlConnection cn1 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);
        cn1.Open();
        SqlCommand cmd = new SqlCommand(query, cn1);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        Session["boxbiho"] = dt;
        cn1.Close();
        grdbox.DataSource = Session["boxbiho"];
        grdbox.DataBind();
    }

    protected void grdbox_DataBinding(object sender, EventArgs e)
    {
        grdbox.DataSource = Session["boxbiho"];
    }

    protected void grdbox_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdbox.DataBind();
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            Carga();
        }
        catch (Exception)
        {
            //throw;
        }
    }

    protected void btnexportar_Click(object sender, EventArgs e)
    {
        try
        {
            exportar.GridViewID = "grdbox";
            exportar.FileName = "Reporte_Box_Biho-" + DateTime.Now;
            exportar.WriteXlsxToResponse();
        }
        catch (Exception)
        {
            //throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/BoxReportBihorario.aspx");
    }
}