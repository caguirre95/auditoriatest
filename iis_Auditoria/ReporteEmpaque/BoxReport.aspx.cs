﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReporteEmpaque_BoxReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtfecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["box"] = null;
            Session["BCT"] = null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top 15 corteCompleto from tbBultosCodigosBarra where corteCompleto like '%" + pre + "%' group by corteCompleto", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porder = dt.Rows[i][0].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porder { get; set; }
    }

    void Carga()
    {       
        var query = "";

        DataTable dt = new DataTable();

        string opcion = rd.SelectedValue;

        switch (opcion)
        {
            case "1":

                panel1.Visible = true;
                query = " SELECT T.corteCompleto corte, T.Style, T.talla, SUBSTRING(CB.codigoBox, LEN(CB.codigoBox) - 2, 3) AS Box, COUNT(CB.codigoBarra) AS Cantidad,"
              + " CAST(CB.fechaEscaneado AS DATE) AS Fecha, max(right(cb.fechaEscaneado, 8)) Hora FROM tbBultosCodigosBarra AS T INNER JOIN tbcodigosCajas AS CC ON CC.corteCompleto = T.corteCompleto"
              + " INNER JOIN tbCodigosBarraScan AS CB ON CB.codigoBarra = T.codigoBarra AND CB.codigoBox = CC.codigoBox"
              + " WHERE T.corteCompleto = '" + txtcorte.Text.Trim().ToUpper() + "' GROUP BY T.corteCompleto, T.Style, T.talla, CB.codigoBarra, CB.codigoBox, CAST(CB.fechaEscaneado AS DATE)"
              + " ORDER BY T.corteCompleto, T.Style, T.talla, CAST(SUBSTRING(CB.codigoBox, LEN(CB.codigoBox) - 2, 3) AS INT) asc";

                break;

            case "2":

                panel1.Visible = true;
                query = " SELECT T.corteCompleto corte, T.Style, T.talla, SUBSTRING(CB.codigoBox, LEN(CB.codigoBox) - 2, 3) AS Box, COUNT(CB.codigoBarra) AS Cantidad,"
              + " CAST(CB.fechaEscaneado AS DATE) AS Fecha, max(right(cb.fechaEscaneado, 8)) Hora FROM tbBultosCodigosBarra AS T INNER JOIN tbcodigosCajas AS CC ON CC.corteCompleto = T.corteCompleto"
              + " INNER JOIN tbCodigosBarraScan AS CB ON CB.codigoBarra = T.codigoBarra AND CB.codigoBox = CC.codigoBox"
              + " WHERE CAST(CB.fechaEscaneado AS DATE) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' GROUP BY T.corteCompleto, T.Style, T.talla, CB.codigoBarra, CB.codigoBox, CAST(CB.fechaEscaneado AS DATE)"
              + " ORDER BY T.corteCompleto, T.Style, T.talla, CAST(SUBSTRING(CB.codigoBox, LEN(CB.codigoBox) - 2, 3) AS INT) asc";

                break;

            default:
                break;
        }

        SqlConnection cn1 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);
        cn1.Open();
        SqlCommand cmd = new SqlCommand(query, cn1);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        Session["box"] = dt;
        cn1.Close();
        grdbox.DataSource = Session["box"];
        grdbox.DataBind();
    }    

    protected void grdbox_DataBinding(object sender, EventArgs e)
    {
        grdbox.DataSource = Session["box"];
        grdbox.GroupBy(grdbox.Columns["corte"]);
    }

    protected void grdbox_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdbox.DataBind();
    }   

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            Carga();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnexportar_Click(object sender, EventArgs e)
    {
        try
        {
            exportar.GridViewID = "grdbox";
            exportar.FileName = "Reporte_Box-" + DateTime.Now;
            exportar.WriteXlsxToResponse();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/BoxReport.aspx");
    }

    protected void btnCT_Click(object sender, EventArgs e)
    {
        Response.Redirect("../ReporteEmpaque/BoxReportCT.aspx");
    }
}