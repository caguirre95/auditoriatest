﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Threading.Tasks;
using System.Drawing;

public partial class InspecEmbarqueIntex : System.Web.UI.Page
{
    class listas
    {
        public string id { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (User.Identity.IsAuthenticated == true)
            {
                //string Id_Order, Id_Seccion;

                Session["tablaaql"] = null;
                Session["auditorias"] = null;

                Session["aqluser"] = null;

                Session["defectos"] = null;
                Session["posicion"] = null;

                string idorder, idauditoria;

                idorder = (string)Request.QueryString["idOrder"];
                idauditoria = (string)Request.QueryString["idAuditoria"];

                if (idorder == "")
                    idorder = "0";


                carga();
                cargagridAuditorias();
                cargagrid();
                Load_Data(idorder);
                cargaTabla();
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

    }

    void cargaTabla()
    {
        if (drpniveles.SelectedItem.Text != "Select..." && drpnivelaql.SelectedItem.Text != "Select...")
        {
            DataTable dttablaaql = new DataTable();
            dttablaaql = DataAccess.Get_DataTable("select m.rangoInicio, m.rangoFinal, m.letra, m.muestra, a.Aceptado, a.Rechazado " +
                                                  "from tbNivel n join tbMuestra m on n.id = m.idnivel join tbaql a on m.id = a.idMuestra " +
                                                  "join tbNivelAQL na on a.idNivelAQL = na.id where n.id = " + drpniveles.SelectedValue + " and na.id =" + drpnivelaql.SelectedValue);

            Session["tablaaql"] = dttablaaql;
        }

    }

    void cargagridAuditorias()
    {
        var corte = (string)Request.QueryString["idOrder"];

        DataTable dtAuditorias = new DataTable();
        dtAuditorias = DataAccess.Get_DataTable("select id, p.POrder,codigoLote,lote,case when estadoAudit = 0 then 'Rechazado' else 'Aprobado' end estadoAudit,secuencia from tbAuditEmbarqueIntex ae join POrder p on p.Id_Order = ae.idcorte where p.Id_Order =" + corte);
        Session["auditorias"] = dtAuditorias;

        GridViewAuditado.DataSource = dtAuditorias;
        GridViewAuditado.DataBind();
    }

    void carga()
    {
        DataTable dtDefecto = new DataTable();
        dtDefecto = DataAccess.Get_DataTable("select idDefecto as idDefecto,Defecto as Defecto from tbDefectos order by Defecto  asc");

        DataTable dtPosicion = new DataTable();
        dtPosicion = DataAccess.Get_DataTable("select idPosicion,Posicion as Codigo from tbPosicion where Estado = 1 order by Codigo asc");

        DataTable dtNiveles = new DataTable();
        dtNiveles = DataAccess.Get_DataTable("select id, nivel from tbNivel");

        DataTable dtNivelAql = new DataTable();
        dtNivelAql = DataAccess.Get_DataTable("select id,CAST(nivelAQL as decimal(3, 1)) as aql from tbNivelAQL");

        Session["defectos"] = dtDefecto;
        Session["posicion"] = dtPosicion;

        drpniveles.DataSource = dtNiveles;
        drpniveles.DataTextField = "nivel";
        drpniveles.DataValueField = "id";
        drpniveles.DataBind();
        drpniveles.Items.Insert(0, "Select...");

        drpnivelaql.DataSource = dtNivelAql;
        drpnivelaql.DataTextField = "aql";
        drpnivelaql.DataValueField = "id";
        drpnivelaql.DataBind();
        drpnivelaql.Items.Insert(0, "Select...");

        DataTable dtusuarioaql = new DataTable();
        dtusuarioaql = DataAccess.Get_DataTable("select idnivel,idNivelAQL as naql from tbusuarioNivel_AQL where usuario = '" + HttpContext.Current.User.Identity.Name + "'");

        Session["aqluser"] = dtusuarioaql;

        drpniveles.SelectedValue = dtusuarioaql.Rows[0][0].ToString();
        drpnivelaql.SelectedValue = dtusuarioaql.Rows[0][1].ToString();

    }

    public int Verificacion(string idpo, int lote, string cantidad)
    {
        DataTable dt = new DataTable();

        int def = 0;

        dt = DataAccess.Get_DataTable(" select ae.idcorte, p.POrder, p.Quantity, SUM(lote) TotalLote, (p.Quantity - ISNULL(SUM(lote), 0)) Def" +
                                      " from tbAuditEmbarqueIntex ae join tbNumeroAuditoriaIntex na on na.idAuditoriaEmbarque = ae.id left join POrder p on p.Id_Order = ae.idcorte" +
                                      " where na.secuencia = 1 and ae.idcorte = '" + idpo + "' group by ae.idcorte, p.POrder, p.Quantity");

        if (dt.Rows.Count > 0)
        {
            def = Convert.ToInt32(dt.Rows[0][4]) - Convert.ToInt32(txtlote.Text);
        }
        else
        {
            def = Convert.ToInt32(cantidad) - Convert.ToInt32(txtlote.Text);
        }

        if (def < 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }

    void Load_Data(string idporder)
    {

        DataTable dt = new DataTable();
        dt = DataAccess.Get_DataTable(" select po.Id_Order,po.POrder,po.Description,po.Quantity,po.Bundles,c.Cliente,s.Style,min(b.Color)as color"
                                     + " from POrder po join Cliente c on po.Id_Cliente=c.Id_Cliente join Style s on po.Id_Style=s.Id_Style"
                                     + " join Bundle  b on b.Id_Order = po.Id_Order where po.Id_Order = " + idporder
                                     + " group by po.Id_Order, po.POrder, po.Description, po.Quantity, po.Bundles, c.Cliente, s.Style");

        DataTable dt1 = new DataTable();
        dt1 = DataAccess.Get_DataTable(" select ae.idcorte, p.POrder, p.Quantity, SUM(lote) TotalLote from tbAuditEmbarqueIntex ae join tbNumeroAuditoriaIntex na on na.idAuditoriaEmbarque = ae.id"
                                     + " left join POrder p on p.Id_Order = ae.idcorte where na.secuencia = 1 and ae.idcorte = '" + idporder + "' group by ae.idcorte, p.POrder, p.Quantity");

        //dt = DataAccess.Get_DataTable(" select po.POrderClient,sum(po.Quantity) Quantity,SUM(po.Bundles) Bundles,c.Cliente,MIN(s.Style) Style"
        //                            + " from POrder po join Cliente c on po.Id_Cliente = c.Id_Cliente join Style s on po.Id_Style = s.Id_Style"
        //                            + " where po.POrderClient = '" + idporder + "' group by po.POrderClient, c.Cliente, s.Style");

        if (dt == null)
        {
            Response.Redirect("Default.aspx");
        }

        if (dt.Rows.Count > 0)
        {
            lblorder1.Text = Convert.ToString(dt.Rows[0][1]); //extraemos numero de Porder
            lblorder2.Text = lblorder1.Text;
            lblcustomer.Text = dt.Rows[0][5].ToString();

            txtporder.Text = dt.Rows[0][1].ToString();
            txtstyle.Text = dt.Rows[0][6].ToString();
            txtdescrip.Text = dt.Rows[0][2].ToString();
            txtcantidad.Text = dt.Rows[0][3].ToString();
            txtcolor.Text = dt.Rows[0][7].ToString();

            if (dt1.Rows.Count > 0)
            {
                txtdef.Text = (Convert.ToInt32(dt.Rows[0][3]) - Convert.ToInt32(dt1.Rows[0][3])).ToString() + " Piezas Sin Auditar";
            }
            else
            {
                txtdef.Text = dt.Rows[0][3].ToString() + " Piezas Sin Auditar";
            }
        }
        else
        {
            var mess = "No se encontraron registros para esta busqueda!";
            string tipo = "info";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
            return;
        }


    }

    void cargagrid()
    {
        List<listas> lista = new List<listas>();

        for (int i = 1; i <= 10; i++)
        {
            var obj = new listas();
            obj.id = i.ToString();
            lista.Add(obj);
        }

        gridAddDefectos.DataSource = lista;
        gridAddDefectos.DataBind();
    }

    void cargamuestra()
    {
        if (Session["tablaaql"] == null)
        {
            return;
        }

        if (txtlote.Text != string.Empty && txtlote.Text != "0")
        {
            var lote = Convert.ToInt32(txtlote.Text);


            var dt = (DataTable)Session["tablaaql"];

            IEnumerable<DataRow> resp = from a in dt.AsEnumerable()
                                        where lote >= a.Field<int>("rangoInicio") && lote <= a.Field<int>("rangoFinal")
                                        select a;

            foreach (var item in resp)
            {
                txtmuestra.Text = (string)item[3].ToString();
                txtacepta.Text = (string)item[4].ToString();
                txtrechaza.Text = (string)item[5].ToString();
            }
        }
        else
        {
            string message = "Ingrese Primero La cantidad del Lote!!!";
            string tipo = "warning";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
            txtlote.Text = string.Empty;
        }       

    }

    void limpiar()
    {
        txtmuestra.Text = string.Empty;
        txtacepta.Text = string.Empty;
        txtrechaza.Text = string.Empty;
        txtpiezasdefect.Text = string.Empty;

        txtlote.Text = string.Empty;
        txthoraI.Text = string.Empty;
        txthoraF.Text = string.Empty;

        txtcomentario.Text = string.Empty;
        txtcodigo.Text = string.Empty;

        //FileUpload1.Dispose();

        cargagrid();

        cargagridAuditorias();
    }

    protected void lnkNew_Click(object sender, EventArgs e)
    {
        string idauditoria = (string)Request.QueryString["idAuditoria"];
        int idlinea = Convert.ToInt16(Request.QueryString["idLinea"]);
        Response.Redirect("SearchIL.aspx?idAuditoria=" + idauditoria + "&idLinea=" + idlinea + "&idSeccion=0");
    }

    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string idauditoria = (string)Request.QueryString["idAuditoria"];
            int idlinea = Convert.ToInt16(Request.QueryString["idLinea"]);
            Response.Redirect("SearchIL.aspx?idAuditoria=" + idauditoria + "&idLinea=" + idlinea + "&idSeccion=0");
        }
        catch (Exception)
        {


        }
    }

    protected void gridAddDefectos_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddldef = (DropDownList)e.Row.FindControl("ddldefectos");
                DropDownList ddlpos = (DropDownList)e.Row.FindControl("ddlposicion");

                var dt1 = (DataTable)Session["defectos"];
                var dt2 = (DataTable)Session["posicion"];

                ddldef.DataSource = dt1;
                ddldef.DataTextField = "Defecto";
                ddldef.DataValueField = "idDefecto";
                ddldef.DataBind();
                ddldef.Items.Insert(0, "");

                ddlpos.DataSource = dt2;
                ddlpos.DataTextField = "Codigo";
                ddlpos.DataValueField = "idPosicion";
                ddlpos.DataBind();
                ddlpos.Items.Insert(0, "");

            }
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + ex.Message + "','" + tipo + "');", true);

        }
    }

    protected void drpniveles_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            cargaTabla();
            cargamuestra();
        }
        catch (Exception)
        {

            throw;
        }

    }

    protected void drpnivelaql_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            cargaTabla();

            cargamuestra();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void txtlote_TextChanged(object sender, EventArgs e)
    {
        try
        {
            if (txtlote.Text != "")
            {

                cargamuestra();
            }
            else
            {
                //mensaje
            }

        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void GridViewAuditado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Reauditar")
        {
            GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

            Label lblidaudit = (Label)row.FindControl("lblno");


            DataTable dt = DataAccess.Get_DataTable("select * from tbAuditEmbarqueIntex where id =" + lblidaudit.Text);

            txtcodigo.Text = dt.Rows[0][2].ToString();
            txtlote.Text = dt.Rows[0][3].ToString();
            txtmuestra.Text = dt.Rows[0][6].ToString();

            drpniveles.SelectedValue = dt.Rows[0][8].ToString();
            drpnivelaql.SelectedValue = dt.Rows[0][9].ToString();

            cargaTabla();

            cargamuestra();
            //txtlote.TextChanged +=new EventHandler

        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtlote.Text == string.Empty || txtrechaza.Text == string.Empty || txtpiezasdefect.Text == string.Empty
                || txtmuestra.Text == string.Empty || drpniveles.SelectedItem.Text == "Select..." || drpnivelaql.SelectedItem.Text == "Select..." || txtpiezasdefect.Text == string.Empty || txtaudit.Text == string.Empty)//Validar Nombre Auditor
            {
                string message = "Debe Cargar Correctamente los campos!!!";
                string tipo = "warning";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);

                return;
            }
            //1 aceptado
            //0 rechazado
            var idcorte = Request.QueryString["idOrder"] == "" ? 0 : Convert.ToInt32(Request.QueryString["idOrder"]);
            var codigo = txtcodigo.Text == "" ? "0" : txtcodigo.Text;
            var lote = Convert.ToInt32(txtlote.Text);
            var estado = Convert.ToInt32(txtrechaza.Text) > Convert.ToInt32(txtpiezasdefect.Text) ? true : false;
            var muestra = Convert.ToInt32(txtmuestra.Text);
            var comentario = txtcomentario.Text;
            var idnivel = Convert.ToInt32(drpniveles.SelectedValue);
            var idnivelaql = Convert.ToInt32(drpnivelaql.SelectedValue);
            var unidadesDef = Convert.ToInt32(txtpiezasdefect.Text);

            var horaI = Convert.ToDateTime(txthoraI.Text);
            var horaF = Convert.ToDateTime(txthoraF.Text);

            int retorno = Verificacion(idcorte.ToString(), lote, txtcantidad.Text);

            if (retorno == 0 && codigo == "0")
            {
                string message = "El deficit de lo Auditado no puede ser negativo!!!";
                string tipo = "warning";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);

                return;
            }

            int respp = 0;

            //  var aql calculado
            //extrae id de tabla tbnumeroauditoria
            var resp = OrderDetailDA.spdSaveAEIntex(idcorte, codigo, lote, estado, horaI, horaF, 1, muestra, unidadesDef, comentario, idnivel, idnivelaql, HttpContext.Current.User.Identity.Name, txtaudit.Text);//Agregar Nombre Auditor

            if (resp > 0)
            {
                foreach (GridViewRow item in gridAddDefectos.Rows)
                {
                    DropDownList ddldef = (DropDownList)item.FindControl("ddldefectos");
                    DropDownList ddlpos = (DropDownList)item.FindControl("ddlposicion");
                    TextBox txtcant = (TextBox)item.FindControl("txtcantidaddef");
                    if (ddldef.SelectedItem.Text != "" && ddlpos.SelectedItem.Text != "")
                    {
                        var resp1 = OrderDetailDA.spdSaveAEDefectosDetIntex(resp, Convert.ToInt32(ddldef.SelectedValue), Convert.ToInt32(ddlpos.SelectedValue), txtcant.Text == string.Empty ? 1 : Convert.ToInt16(txtcant.Text));

                        if (resp1 > 0)
                        {
                            respp++;
                        }
                    }
                }

                //var pathCarpetaDestino = System.IO.Path.Combine(Server.MapPath("~/"), "imgAuditorias");
                //var carpetaDestino = new System.IO.DirectoryInfo(pathCarpetaDestino);
                //// verificamos si existe
                //if (!carpetaDestino.Exists)
                //    carpetaDestino.Create();


                //var archivos = FileUpload1.PostedFiles;

                //int respp = 0;
                //foreach (var archivo in archivos)
                //{
                //    var extencion = System.IO.Path.GetExtension(archivo.FileName);

                //    // bool band = false;
                //    if (extencion.ToLower() == ".jpg" || extencion.ToLower() == ".png" || extencion.ToLower() == ".jpeg")
                //    {
                //        var nombreArchivo = System.IO.Path.GetFileName(archivo.FileName);

                //        var dir = new DirectoryInfo(Server.MapPath("~/imgAuditorias/" + HttpContext.Current.User.Identity.Name.Replace(" ", "").ToUpper()));

                //        if (!dir.Exists)
                //            dir.Create();

                //        var pathArchivoDestino = Path.Combine(dir.ToString(), nombreArchivo);

                //        archivo.SaveAs(pathArchivoDestino);
                //        var resp2 = OrderDetailDA.spdSaveAEImagen(resp, pathArchivoDestino);

                //        if (!resp2.Equals("OK"))
                //        {
                //            respp++;
                //        }

                //    }
                //}

                if (respp > 0)
                {

                    DataTable dt = DataAccess.Get_DataTable("select codigoLote from tbAuditEmbarqueIntex ae join tbNumeroAuditoriaIntex na on ae.id=na.idAuditoriaEmbarque where na.id=" + resp);

                    string message = "Guardado Correctamente!!!";

                    if (dt != null)
                    {
                        message = string.Concat(message, ", Con codigo lote: ", dt.Rows[0][0].ToString());
                    }

                    //string message = "Guardado Correctamente!!!";
                    string tipo = "success";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);

                    limpiar();
                    Load_Data(idcorte.ToString());
                }
                else
                {
                    string message = "Guardado Correctamente!!!";
                    string tipo = "success";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);

                    limpiar();
                    //string message = "Error al Guardar imagen!!!";
                    //string tipo = "warning";
                    //ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
                }
            }

        }
        catch (Exception ex)
        {
            string message = ex.Message;
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);

        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        try
        {
            limpiar();
        }
        catch (Exception)
        {

            throw;
        }
    }


    protected void GridViewAuditado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblestado = (Label)e.Row.FindControl("lblestado");

                Button btnReauditar = (Button)e.Row.FindControl("btnReauditar");

                if (lblestado.Text == "Aprobado")
                {
                    btnReauditar.Visible = false;
                    e.Row.BackColor = Color.SkyBlue;
                    e.Row.CssClass = "label-grid-inv";
                }
                else
                {
                    e.Row.BackColor = Color.IndianRed;
                    e.Row.CssClass = "label-grid";
                    btnReauditar.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + ex.Message + "','" + tipo + "');", true);

        }
    }
}