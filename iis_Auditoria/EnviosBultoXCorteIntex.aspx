﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EnviosBultoXCorteIntex.aspx.cs" Inherits="EnviosBultoXCorteIntex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    var user = document.getElementById("<%= HiddenFielduser.ClientID %>").value;
                    $.ajax({
                        url: "EnviosBultoXCorteIntex.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "','user':'" + user + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);


                    if (ui.item.porder != "" && ui.item.idorder != "") {

                        //var currentURL = window.location.href + '&';
                        //alert(currentURL);

                        cadena = "EnviosBultoXCorteIntex.aspx?idorder=" + ui.item.idorder + "&porder=" + ui.item.porder;

                        // alert(cadena);
                        window.history.replaceState('', '', cadena);
                    }

                    //var change = new RegExp('('+param+')=(.*)&', 'g'); 
                    //var newURL = currentURL.replace(change, '$1='+value+'&'); 

                    //if (getURLParameter(param) !== null){ 
                    //        try { 
                    //                window.history.replaceState('', '', newURL.slice(0, - 1) ); 
                    //        } catch (e) { 
                    //                console.log(e); 
                    //        } 
                    //} else { 
                    //        var currURL = window.location.href; 
                    //        if (currURL.indexOf("?") !== -1){ 
                    //                window.history.replaceState('', '', currentURL.slice(0, - 1) + '&' + param + '=' + value); 
                    //        } else { 
                    //                window.history.replaceState('', '', currentURL.slice(0, - 1) + '?' + param + '=' + value); 
                    //        } 
                    //} 

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Algunos Bultos No Se Guardaron Correctamente !',
                type: 'info'
            });
        }

        function limpiar() {
            //alert();
            $('#<%=txtPorder.ClientID%>').val("");
            $('#<%=hfidPorder.ClientID%>').val("");
        }
    </script>

    <style>
        .mar_t {
            margin: 10px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>

    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="container">
        <div class="">
            <div class="panel panel-primary" style="margin-top: 10px">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Measurement suggestion </strong></h3>
                </div>
                <div class="panel-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="HiddenFielduser" runat="server" />


                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                Corte:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:HiddenField ID="hfidPorder" runat="server" />
                                            <asp:LinkButton Text="" OnClick="lnkMostrarBultosIncompletos_Click" CssClass="btn btn-default" ID="lnkMostrarBultosIncompletos" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" onfocus="limpiar();" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                Envio Dia
                                    <asp:DropDownList ID="drpEnvioDia" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Select..." Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 Envio" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 Envio" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 Envio" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 Envio" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5 Envio" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="6 Envio" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="7 Envio" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="8 Envio" Value="8"></asp:ListItem>
                                    </asp:DropDownList>


                                <%--   <asp:Button runat="server" ID="btnguardar" OnClick="btnguardar_Click" Text="Guardar Unidades" CssClass="btn btn-primary pull-right mar_t" />--%>
                                <asp:LinkButton Text="" ID="lnkEnvio" OnClick="lnkEnvio_Click" CssClass="btn btn-info mar_t" runat="server"><i class="glyphicon glyphicon-arrow-left"> </i>Regresar </asp:LinkButton>
                                <asp:Button runat="server" ID="btnver" OnClick="btnver_Click" Text="Guardar Todos Seleccionados" CssClass="btn btn-primary pull-right mar_t" />
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            </div>
                            <style>
                                .w {
                                    width: 100%;
                                    min-width: 1000px;
                                }

                                .vis {
                                    display: none
                                }
                            </style>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="overflow-x: auto">
                                <asp:GridView ID="gridEnvios" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" DataKeyNames="Id_Bundle" OnRowCommand="gridEnvios_RowCommand" CssClass="table table-hover table-striped w" ShowFooter="true" AutoGenerateColumns="false" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField ControlStyle-CssClass="vis">
                                            <ItemTemplate>
                                                <asp:Label ID="lblidbundle" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("Id_Bundle") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Linea 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbllinea" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("Linea") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Porder
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPorder" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("POrder") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Secuencia
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblseq" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("NSeq") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Talla 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblsize" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Size") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Cantidad 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblquantity" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Quantity") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Acumulado
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblUni" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Cantidad") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Envio Corte
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtenvio" Width="60px" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Envio") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                               
                                                <asp:Button Text="Save" CommandName="enviar" CssClass="btn btn-default" runat="server" />
                                                <asp:CheckBox ID="checkEnvio" Text="Enviar" Checked="true" CssClass="btn btn-default" runat="server" />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                  
                </div>
            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

