﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InspecInternaAprobados : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (User.Identity.IsAuthenticated == true)
            {
                //string Id_Order, Id_Seccion;

                string idorder, idauditoria;

                idorder = (string)Request.QueryString["idOrder"];
                idauditoria = (string)Request.QueryString["idAuditoria"];

                if (idorder == "")
                    idorder = "0";

                Load_Data(idorder, idauditoria);


            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

    }

    public void Load_Data(string idporder, string idauditoria)
    {

        DataTable dt = new DataTable();
        dt = OrderDetailDA.Get_OrderbyId(idporder);

        if (dt == null)
        {
            Response.Redirect("Default.aspx");
        }

        if (dt.Rows.Count > 0)
        {
            lblorder1.Text = Convert.ToString(dt.Rows[0][1]); //extraemos numero de Porder
            lblorder2.Text = lblorder1.Text;
            lblcustomer.Text = dt.Rows[0][5].ToString();

            txtporder.Text = dt.Rows[0][1].ToString();
            txtstyle.Text = dt.Rows[0][6].ToString();
            txtdescrip.Text = dt.Rows[0][2].ToString();
            txtcantidad.Text = dt.Rows[0][3].ToString();
            txtbultos.Text = dt.Rows[0][4].ToString();
        }
        else
        {
            lblnoRows.Text = "No se encontraron registros para esta busqueda!";
            string tipo = "info";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + lblnoRows.Text + "','" + tipo + "');", true);
            return;
        }

        MembershipUser user = Membership.GetUser(Page.User.Identity.Name);
        string UserId = user.ProviderUserKey.ToString();

        DataTable dt_Inspector = DataAccess.Get_DataTable("select id_inspector from Inspector where UserId='" + UserId + "'");
        int idInspector = Convert.ToInt16(dt_Inspector.Rows[0][0]);

        DataTable dt_ODetails = new DataTable();
        dt_ODetails = OrderDetailDA.GetBultoByOrder(Convert.ToInt32(idporder), Convert.ToInt16(idauditoria), idInspector);

        GridBultos.DataSource = dt_ODetails;
        GridBultos.DataBind();

    }

    protected void GridBultos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label idbulto = (Label)e.Row.FindControl("lblidbundle");

                string query = "select idBBRechazado from tbAuditInterRechazado  where  idBulto = " + idbulto.Text;

                DataTable bundle_rejected = DataAccess.Get_DataTable(query);

                LinkButton lk = (LinkButton)e.Row.FindControl("comando");
                LinkButton lk1 = (LinkButton)e.Row.FindControl("comandoRep");

                if (bundle_rejected.Rows.Count > 0)
                {
                    RadioButtonList rbl_aprob1 = (RadioButtonList)e.Row.FindControl("RadioButtonList1");
                    rbl_aprob1.SelectedValue = "0";//Rechazado
                    lk.CssClass = "btn btn-danger";
                    lk1.Visible = true;
                }
                else
                {
                    lk1.Visible = false;
                    lk.CssClass = "btn btn-default";
                }

            }
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
            return;

        }

    }

    protected void GridBultos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string idorder = (string)Request.QueryString["idOrder"];
            string idauditoria = (string)Request.QueryString["idAuditoria"];
            int idlinea = Convert.ToInt16(Request.QueryString["idLinea"]);

            //pendiente de revision
            if (idorder == "")
            {
                Response.Redirect("Default.aspx");
                //  Response.Redirect("Secciones.aspx");
            }

            if (e.CommandName == "Editar")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int indice = row.RowIndex;
                string idbulto = GridBultos.DataKeys[indice].Value.ToString();

                RadioButtonList rbl_aprob = (RadioButtonList)row.FindControl("RadioButtonList1");
                int status = Convert.ToInt32(rbl_aprob.SelectedValue);

                if (status == 0)
                {
                    Response.Redirect("InspecInternaRechazo.aspx?idBundle=" + idbulto + "&idOrder=" + idorder + "&idAuditoria=" + idauditoria + "&idLinea=" + idlinea + "&style=" + txtstyle.Text + "&customer=" + lblcustomer.Text + "&cut=" + lblorder1.Text, false);
                }
                else
                {
                    try
                    {
                        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
                        string userId = mu.ProviderUserKey.ToString();

                        string query = "select idBBRechazado from tbAuditInterRechazado ba where IdBulto= " + idbulto;
                        DataTable dt_rechazos = DataAccess.Get_DataTable(query);

                        if (dt_rechazos.Rows.Count > 0)
                        {
                            string mess = "Por favor ingrese la cantidad o seleccione un defecto u operacion!";
                            string tipo = "info";
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
                            return;
                        }
                        else
                        {
                            DataTable dtporder = new DataTable();
                            dtporder = DataAccess.Get_DataTable("select b.Quantity from Bundle b  where b.Id_Bundle=" + idbulto);
                            int muestra = 0;
                            var quantity = int.Parse(dtporder.Rows[0][0].ToString());
                            if (quantity < 25)
                            {
                                muestra = 5;
                            }
                            else if (quantity > 24)
                            {
                                muestra = 8;
                            }

                            OrderDetailDA.SaveAIApproved(int.Parse(idbulto), userId, idlinea, muestra);
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                            Load_Data(idorder, idauditoria);

                        }
                    }
                    catch (Exception ex)
                    {
                        string tipo = "info";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
                    }

                }

            }
            if (e.CommandName == "Reparar")
            {
                try
                {

                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    string Id_Bundle = GridBultos.DataKeys[indice].Value.ToString();

                    OrderDetailDA.spdSaveAIRepair(Convert.ToInt32(Id_Bundle));
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                    Load_Data(idorder, idauditoria);

                }
                catch (Exception ex)
                {
                    //  string mess = "Por favor ingrese la cantidad o seleccione un defecto u operacion!";
                    string tipo = "info";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
                }
            }

        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        //btnsave.Enabled = false;
        //foreach (GridViewRow row in grv_defectos.Rows)
        //{
        //    TextBox txt = row.FindControl("txtfirma") as TextBox;
        //    if (txt != null)
        //    {
        //        string value = txt.Text;
        //    }
        //    else
        //    {
        //        string error = "alert('Ingrese su firma!');";
        //        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
        //    }
        //}
        //this.AsignarDefectos.ShowOnPageLoad = false;
    }

    protected void lnkNew_Click(object sender, EventArgs e)
    {
        string idauditoria = (string)Request.QueryString["idAuditoria"];
        int idlinea = Convert.ToInt16(Request.QueryString["idLinea"]);
        Response.Redirect("SearchIL.aspx?idAuditoria=" + idauditoria + "&idLinea=" + idlinea + "&idSeccion=0");
    }

    protected void btnGuardarTodos_Click(object sender, EventArgs e)
    {
        try
        {
            string idorder = (string)Request.QueryString["idOrder"];
            string idauditoria = (string)Request.QueryString["idAuditoria"];
            int idlinea = Convert.ToInt16(Request.QueryString["idLinea"]);

            string resp = "true";
            int cont = 0;
            //revisar ***
            if (idorder == "")
            {
              //  return;
                //Response.Redirect("Secciones.aspx");
            }

            foreach (GridViewRow Row in GridBultos.Rows)
            {
                RadioButtonList rd = (RadioButtonList)Row.FindControl("RadioButtonList1");

                if (rd.SelectedValue == "1")
                {
                    Label Id_OrderDetail = (Label)Row.FindControl("lblidbundle");
                    int Id_Bundle = Convert.ToInt32(Id_OrderDetail.Text);

                    string query = "select idBBRechazado from tbAuditInterRechazado ba where IdBulto= " + Convert.ToString(Id_Bundle);

                    DataTable dt_rechazos = DataAccess.Get_DataTable(query);

                    if (dt_rechazos.Rows.Count <= 0)
                    {
                        //  ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "bultomalo();", true);
                        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
                        string UserId = mu.ProviderUserKey.ToString();


                        DataTable dtporder = new DataTable();
                        dtporder = DataAccess.Get_DataTable("select p.POrder,b.Quantity from Bundle b join POrder p on b.Id_Order=p.Id_Order where b.Id_Bundle=" + Id_Bundle);
                        int muestra = 0;
                        var quantity = int.Parse(dtporder.Rows[0][1].ToString());
                        if (quantity < 25)
                        {
                            muestra = 5;
                        }
                        else if (quantity > 24)
                        {
                            muestra = 8;
                        }

                        OrderDetailDA.SaveAIApproved(Id_Bundle, UserId, idlinea, muestra);
                        cont++;
                    }
                    else
                    {
                        resp = "false";
                    }
                }
                else
                {
                    resp = "false";
                }

            }

            Load_Data(idorder, idauditoria);

            if (resp == "true")
            {
                string message = "Exito! " + " Aprobados: " + cont;
                string tipo = "success";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
            }
            else
            {
                string message = "Aprobados: " + cont + ", No ha sido posible la aprobacion de algunos bultos!";
                string tipo = "info";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
            }


        }
        catch (Exception)
        {
            string message = "Ha Ocurrido un Error, Reintente nuevamente!";
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);


        }

    }


    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string idauditoria = (string)Request.QueryString["idAuditoria"];
            int idlinea = Convert.ToInt16(Request.QueryString["idLinea"]);
            Response.Redirect("SearchIL.aspx?idAuditoria=" + idauditoria + "&idLinea=" + idlinea+ "&idSeccion=0");
        }
        catch (Exception)
        {


        }
    }
}