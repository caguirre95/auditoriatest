﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EnviosBulto : System.Web.UI.Page
{

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/BundleView.aspx");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.User.Identity.IsAuthenticated == true)
        {
            if (Page.User.IsInRole("PackAdmon"))
            {
                if (!IsPostBack)
                {
                    multiview.ActiveViewIndex = 0;
                    cantidadLiberada();
                    extraerlinea();

                    string u = Page.User.Identity.Name.ToString().ToLower();
                    if (u.Equals("adminsnr"))
                    {
                        verB.Visible = true;
                        Estado();
                    }
                        
                }
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }

        }
        else
        {
            Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
        }
    }

    void cantidadLiberada()
    {
        try
        {
            DataTable dt;
            string user = HttpContext.Current.User.Identity.Name;

            string queryBundle = " select vc.* from vwcantidadliberado vc"
                               + " join Linea l on l.id_linea = vc.id_linea"
                               + " join UserPlanta u on l.id_planta = u.Id_Planta"
                               + " where u.nombreUsuario = '" + user + "'";

            dt = DataAccess.Get_DataTable(queryBundle);

            gridEnvios.DataSource = dt;
            gridEnvios.DataBind();

        }
        catch (Exception)
        {
            throw;
        }
    }

    void Estado()
    {
        string query = " select Estado from tbBloqueo where UserName = 'adminsnr'";

        DataTable dt = DataAccess.Get_DataTable(query);
        
        string est = dt.Rows[0][0].ToString();

        if (est == "False")
        {
            lbld.Text = "Form Bloq";
        }
        else
        {
            lbld.Text = "Form Desbloq";
        }
    }

    void extraerlinea()
    {
        try
        {
            string user = HttpContext.Current.User.Identity.Name.ToUpper();

            string query = " select l.id_linea,l.numero"
                          + " from linea l"
                          + " where numero not like '%-%' and numero not like '%B%' and id_planta = (select Id_Planta from UserPlanta u where u.nombreUsuario = '" + user + "')"
                          + " order by convert(float, numero) asc";

            DataTable dt = DataAccess.Get_DataTable(query);

            drpLinea.DataSource = dt;
            drpLinea.DataTextField = "numero";
            drpLinea.DataValueField = "id_linea";
            drpLinea.DataBind();
            drpLinea.Items.Insert(0, "Select...");


        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnguardarEnviar_Click(object sender, EventArgs e)
    {
        try
        {
            if (drpEnvioDia.SelectedItem.Text != "Select...")
            {
                int rows = 0;// gridEnvios.Rows.Count;
                int cont = 0;
                foreach (GridViewRow item in gridEnvios.Rows)
                {
                    string resp = "";
                    var lblidorder = (Label)item.FindControl("lblidorder");
                    var chk = (CheckBox)item.FindControl("checkEnvio");
                    var txtenvio = (TextBox)item.FindControl("txtenvio");

                    if (chk.Checked)
                    {
                        rows++; //cambiar nombre procedimiento
                        resp = OrderDetailDA.enviarguardarBihorario(int.Parse(lblidorder.Text), int.Parse(txtenvio.Text),
                                                                    int.Parse(drpEnvioDia.SelectedValue), Context.User.Identity.Name);
                    }

                    if (resp == "OK")
                    {
                        cont++;
                    }
                }

                if (rows == cont)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
                }

                cantidadLiberada();
                limpiar();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    void limpiar()
    {
        // gridEnvios.DataSource = null;
        // gridEnvios.DataBind();

        //drpBihorario.SelectedIndex = 0;
        drpEnvioDia.SelectedIndex = 0;
    }

    protected void drpLinea_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (drpLinea.SelectedItem.Text != "Select...")
            {
                string queryBundle = "select * from vwcantidadliberado where id_linea=" + drpLinea.SelectedValue;
                DataTable dt = DataAccess.Get_DataTable(queryBundle);

                gridEnvios.DataSource = dt;
                gridEnvios.DataBind();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnVertodos_Click(object sender, EventArgs e)
    {
        try
        {
            cantidadLiberada();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void lnkEnvio_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnkred = (LinkButton)sender;
            GridViewRow row = (GridViewRow)lnkred.NamingContainer;

            var lblidorder = (Label)row.FindControl("lblidorder");
            var lblporder = (Label)row.FindControl("lblPorder");

            if (lblidorder.Text != string.Empty && lblporder.Text != string.Empty)
            {
                Response.Redirect("EnviosBultoXCorte.aspx?idorder=" + lblidorder.Text + "&porder=" + lblporder.Text);
            }


        }
        catch (Exception)
        {

            throw;
        }

    }

    protected void lnkseccionado_Click(object sender, EventArgs e)
    {

        try
        {
            LinkButton lnkred = (LinkButton)sender;
            GridViewRow row = (GridViewRow)lnkred.NamingContainer;

            var lblidorder = (Label)row.FindControl("lblidorder");
            var lblporder = (Label)row.FindControl("lblPorder");

            if (lblidorder.Text != string.Empty && lblporder.Text != string.Empty)
            {
                Response.Redirect("enviosBultosSeccionados.aspx?idorder=" + lblidorder.Text + "&porder=" + lblporder.Text);
            }


        }
        catch (Exception)
        {

            throw;
        }


    }

    protected void btnbloq_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtblo.Text.Equals(string.Empty))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
            }
            else
            {
                string clave = txtblo.Text;

                if (clave == "86589707")
                {
                    OrderDetailDA.spdbloqueo();

                    txtblo.Text = "";

                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "ClaveCorrect();", true);

                    Estado();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave();", true);
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }
}