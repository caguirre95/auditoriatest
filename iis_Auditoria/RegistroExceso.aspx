﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RegistroExceso.aspx.cs" Inherits="RegistroExceso" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=idporderAuxiliar.ClientID%>').val(ui.item.idorder);
                    $('#<%=txtstyle.ClientID%>').val(ui.item.style);
                    $('#<%=lblmensajepo.ClientID%>').html(ui.item.porder);

                    $('#myModal').modal('toggle')

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });

        $(document).ready(function () {

           <%--  $(function () {
                $('#<%=fecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })--%>


            $('#<%=Button1.ClientID%>').click(function (e) {
                e.preventDefault();
                $('#<%=txtPorder.ClientID%>').val('');
                $('#<%=hfidPorder.ClientID%>').val('');
                $('#<%=txtstyle.ClientID%>').val('');
                $('#<%=lblmensajepo.ClientID%>').html('');
                $('#myModal').modal('hide')
            })

            $('#<%=Button2.ClientID%>').click(function (e) {
                e.preventDefault();
                var idporder = $('#<%=idporderAuxiliar.ClientID%>').val();
                $('#<%=hfidPorder.ClientID%>').val(idporder);
                $('#<%=lblmensajepo.ClientID%>').html('');
                $('#myModal').modal('hide')
            })

        })



    </script>

    <script type="text/javascript">

        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function alertexeso() {
            swal({
                title: 'El Acumulado Sobrepasa Las Unidades Del Bulto Se Ha Corregido Automaticamente!',
                type: 'info'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Algunos Bultos No Se Guardaron Correctamente !',
                type: 'info'
            });
        }

    </script>

    <script>
        function key_up(objRef) {

            var row = objRef.parentNode.parentNode;

            if (row.hasChildNodes()) {
                var children = row.childNodes;

                alert(row.children[0].nodoName);

                for (var i = 0; i < children.length; i++) {
                    alert(children[i].childNodes[0].val);
                }
            }

           //  txtunidades
           //  lblquantity
           //  row.cell[0].text;
           //  if (objRef.checked) {
           //  var unidades = row.getElementById("<= txtunidades.ClientID %>").value;
           //  var quantity = row.getElementById("<= lblquantity.ClientID %>").value;
           //  var deficit = row.getElementById("<= txtdeficit.ClientID %>").value;

           //  var num = document.getElementById("<%= HiddenFielduser.ClientID %>").value;

            //  row.style.backgroundColor = "#0090CB";
            //  row.style.color = "white";
            //  }
            //  else {
            //  If not checked change back to original color
            //  if (row.rowIndex % 2 == 0) {
            //  Alternating Row Color
            //  row.style.backgroundColor = "white";
            //  row.style.color = "black";
            //  }
            //  else {
            //  row.style.backgroundColor = "white";
            //  row.style.color = "black";
            //  }
            //  }
        }
    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function alertMasterE(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }
    </script>


    <style>
        .mar_t {
            margin-top: 20px;
            margin-bottom: 10px;
            margin-left: 5px;
            margin-right: 5px;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>


    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdnusuario" runat="server" />

    <div class="container">
        <div class="">
            <div class="panel panel-primary" style="margin-top: 10px;">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Registro de Exceso </strong></h3>
                </div>
                <div class="panel-body">

                    <asp:HiddenField ID="HiddenFielduser" runat="server" />
                    <asp:HiddenField ID="hfidPorder" runat="server" />
                    <%--    <div class="col-lg-12">--%>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                Corte:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" required="true" AutoCompleteType="Disabled" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                            </div>
                            <div class="form-group">
                                Estilo:
                                     <asp:TextBox runat="server" CssClass="form-control" placeholder="Estilo"  Font-Size="14px" ID="txtstyle" />

                            </div>
                            <div class="form-group">
                                Talla:
                                     <asp:TextBox runat="server" CssClass="form-control" placeholder="Talla" Font-Size="14px" ID="txttalla" />

                            </div>
                            <div class="form-group">
                                Unidades:
                                     <asp:TextBox runat="server" onkeypress="return num(event);" CssClass="form-control" placeholder="Unidades" Font-Size="14px" ID="txtunidades" />

                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                Comentario:
                                     <asp:TextBox runat="server" CssClass="form-control" placeholder="Comentario" Font-Size="14px" ID="txtcomentario" />
                            </div>
                            <div class="form-group">
                                Envio Dia:
                                         <asp:DropDownList ID="drpEnvio" CssClass="form-control" runat="server">
                                             <asp:ListItem Text="Select..." Value="0"></asp:ListItem>
                                             <asp:ListItem Text="1 Envio" Value="1"></asp:ListItem>
                                             <asp:ListItem Text="2 Envio" Value="2"></asp:ListItem>
                                             <asp:ListItem Text="3 Envio" Value="3"></asp:ListItem>
                                             <asp:ListItem Text="4 Envio" Value="4"></asp:ListItem>
                                             <asp:ListItem Text="5 Envio" Value="5"></asp:ListItem>
                                             <asp:ListItem Text="6 Envio" Value="6"></asp:ListItem>
                                             <asp:ListItem Text="7 Envio" Value="7"></asp:ListItem>
                                             <asp:ListItem Text="8 Envio" Value="8"></asp:ListItem>
                                         </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <asp:Button runat="server" ID="btnguardar" Text="Guardar Unidades" OnClick="btnguardar_Click" CssClass="btn btn-primary pull-right mar_t" />
                                <asp:Button runat="server" ID="btnvertodos" Text="Ver Todos" OnClick="btnvertodos_Click" CssClass="btn btn-info pull-right mar_t" />
                                <asp:Button runat="server" ID="btnlimpiar" Text="Limpiar" OnClick="btnlimpiar_Click" CssClass="btn btn-success  pull-right mar_t" />

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <h1>Exceso del Corte</h1>
                        <div  style="overflow-x: auto">

                            <style>
                                .w {
                                    width: 100%;
                                }
                            </style>

                            <asp:GridView ID="gridbultos" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" DataKeyNames="idExceso" OnRowCommand="gridbultos_RowCommand"
                                CssClass="table w table-hover table-striped "
                                GridLines="None" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <HeaderTemplate>
                                            Bulto 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblidbulto" runat="server" Font-Size="Medium" Text='<%#Bind("idExceso") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                        <HeaderTemplate>
                                            Talla 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSeq" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("tallaExceso") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="True">
                                        <HeaderTemplate>
                                            Unidades 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtcantidad" CssClass="form-control" Text='<%#Bind("cantidadExceso") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Comando
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="btn" ForeColor="Black" CommandName="agregar" Text="Reiniciar" CssClass="btn btn-default" runat="server">
                                                      <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>

                    </div>


                </div>

            </div>

        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
        <div class="modal-dialog " role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Verificacion de Corte</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="display: block !important">
                        <asp:HiddenField ID="idporderAuxiliar" runat="server" />
                        <div class="col-lg-12 center-block">
                            <h1>
                                <asp:Label Text="text" ID="lblmensajepo" CssClass="label label-info center-block " runat="server" />
                            </h1>
                        </div>
                        <div class="col-lg-6 center-block">
                            <asp:Button ID="Button2" CssClass="btn btn-primary center-block" runat="server" Text="Aceptar" />
                        </div>

                        <div class="col-lg-6 center-block">
                            <asp:Button ID="Button1" CssClass="btn btn-default center-block" runat="server" Text="Cancelar" />
                        </div>

                    </div>

                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

