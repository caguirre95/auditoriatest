﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using SistemaAuditores.DataAccess;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using DevExpress.Web;

public partial class Production : System.Web.UI.Page
{
    int seccion = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Load_Data();
        }
    }

    public void Load_Data()
    {
        string hora_actual = DateTime.Now.Hour.ToString();
        string minutos_actual = DateTime.Now.Minute.ToString();
        int minutos = Convert.ToInt16(minutos_actual);
        if (minutos < 10)
        {
            minutos_actual = "0" + minutos_actual;
        }

        string date_now = hora_actual + '.' + minutos_actual;
        DateTime fecha_actual = DateTime.Now;
        string sqlfecha_actual = fecha_actual.ToString("yyyy-MM-dd");
        int Id_Biohorario = 0;
        DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio, bihorario FROM Biohorario WHERE @input between rango1 and rango2");
        if (Bihorarios.Rows.Count > 0)
        {
            Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);
            
        }
        int Id_Linea = Convert.ToInt16(Request.QueryString["id_linea"]);
        int Id_Seccion = Convert.ToInt16(Request.QueryString["id_seccion"]);///se quito comentario        
        Session["idSeccion"] = Id_Seccion;

        string query=  " SELECT row_number() OVER(ORDER BY Op.id_operario) AS id, Op.codigo AS Codigo,Op.nombre as Nombre, Oper.descripcion_completa as Operacion"
                      +" FROM Operario Op"
                      +" inner join Catalogo c on op.id_operario = c.Id_operario"
                      +" inner join Operacion Oper ON oper.id_operacion = c.id_operacion"
                      + " where c.Id_Linea=" + Id_Linea + " and c.Id_Seccion= " + seccion + " and Op.id_operario not in (select id_operario from Ausencia where id_biohorario = " + Id_Biohorario + " and fecha = '" + sqlfecha_actual + "')";


        //string query = "SELECT row_number() OVER (ORDER BY Op.id_operario) AS id, Op.codigo AS Codigo,"
        //+"Op.nombre as Nombre, Oper.descripcion as Operacion FROM Operario Op inner join Operacion Oper "
        //+ "ON op.id_operacion=Oper.id_operacion where Op.Id_Linea = " + Id_Linea + " and Op.Id_Seccion= " + seccion + "" 
        //+"and Op.id_operario not in (select id_operario from Ausencia where id_biohorario=" + Id_Biohorario + " and fecha='" + sqlfecha_actual + "')";
        DataTable oper = DataAccess.Get_DataTable(query);
        GridView1.DataSource = oper;
        GridView1.DataBind();
    }
    protected void imgBack_Click(object sender, ImageClickEventArgs e)
    {
        seccion = 1;
        Validar();  

    }
    protected void lnkback_Click(object sender, EventArgs e)
    {
        seccion = 1;
        Validar();  
    }
    protected void imgFront_Click(object sender, ImageClickEventArgs e)
    {
        seccion = 2;
        Validar();  
    }
    protected void lnkFront_Click(object sender, EventArgs e)
    {
        seccion = 2;
        Validar(); 
    }
    protected void imgE1_Click(object sender, ImageClickEventArgs e)
    {
        seccion = 3;
        Validar(); 
    }
    protected void lnkE1_Click(object sender, EventArgs e)
    {
        seccion = 3;
        Validar(); 
    }
    protected void imgE2_Click(object sender, ImageClickEventArgs e)
    {
        seccion = (int)Session["idSeccion"];
        Validar(); 
    }
    protected void lnkE2_Click(object sender, EventArgs e)
    {

        seccion = (int)Session["idSeccion"];
        Validar(); 
            
    }
    
    public void Validar()
    {
        string inspectorUN = Page.User.Identity.Name;
        DataTable dt_UserName = DataAccess.Get_DataTable("select UserId from aspnet_Users where UserName='" + inspectorUN + "'");
        string UserId = Convert.ToString(dt_UserName.Rows[0][0]);
        DataTable dt_Inspector = DataAccess.Get_DataTable("select Id_Supervisor from Supervisor where UserId='" + UserId + "' and Id_Seccion=" + seccion);
        if (dt_Inspector.Rows.Count > 0)
        {
            Load_Data();
            CantidadProduccion.ShowOnPageLoad = true;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
        }
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        //for (int i = 0; i <= GridView1.Rows.Count - 1; i++)
        //{
        //    for (int j = 0; j <= GridView1.Columns.Count - 1; j++)
        //    {
        //        if (j == 0)
        //        {
        //            CheckBox check = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkdefectos");
        //            if (check.Checked == true)
        //            {
        //                Label lbldefecto = (Label)GridView1.Rows[i].Cells[1].FindControl("LabelDefect");
        //                DataTable dt_IdDefectos = new DataTable();
        //                dt_IdDefectos = DataAccess.Get_DataTable("select id_defectos from Defectos where Nombre='" + lbldefecto.Text + "'");

        //                DropDownList cbxArea = (DropDownList)GridView1.Rows[i].Cells[2].FindControl("ddlAreaDefecto");
        //                DataTable dt_IdAreaDef = new DataTable();
        //                dt_IdAreaDef = DataAccess.Get_DataTable("select Id_DefxArea from DefectosxArea where Id_defectos=" + Convert.ToInt16(dt_IdDefectos.Rows[0][0]) + " and Id_Area=" + cbxArea.SelectedValue);
        //                int IdAreaxDef = Convert.ToInt16(dt_IdAreaDef.Rows[0][0]);
        //                TextBox txtcantidad = (TextBox)GridView1.Rows[i].Cells[1].FindControl("txtcantidad");
        //                if (txtcantidad.Text != "")
        //                {
        //                    try
        //                    {
        //                        OrderDetailDA.SaveBundlesRejected(Convert.ToInt16(IdBundle), Convert.ToInt16(IdOrder), Convert.ToInt16(Id_Biohorario), Id_Seccion, Id_Linea, IdAreaxDef, Convert.ToInt16(id_inspector), IdSupervisor, Idoperario, Id_Muestreo, Convert.ToDateTime(lbldaterechazo.Text), Convert.ToString(lblhorarechazo.Text), Convert.ToInt16(txtcantidad.Text));
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
        //                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        //                        return;
        //                    }
        //                }
        //                else
        //                {
        //                    string message = "alert('Por favor ingrese la cantidad!');";
        //                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        //                    return;
        //                }
        //            }
        //        }
        //    }
        //}
    }
}