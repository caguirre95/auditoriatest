﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.Configuration;
using Pervasive.Data.SqlClient;
using System.Configuration;

namespace SistemaAuditores.DataAccess
{
    public class DataAccessPervasive
    {

        public static DataTable GetDataTable(string query)
        {
            var cnn = ConfigurationManager.AppSettings["PervasiveSQLClient"];
            PsqlConnection DBConn = new PsqlConnection(cnn.ToString());
            try
            {
                DataTable dt = new DataTable();
                // Open the connection
                DBConn.Open();
                PsqlDataAdapter da = new PsqlDataAdapter();
                da.SelectCommand = new PsqlCommand(query, DBConn);
                da.Fill(dt);
                //Console.WriteLine("Connection Successful!");

                return dt;

            }
            catch (PsqlException ex)
            {
                // Connection failed
                Console.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                DBConn.Close();
            }
        }


        public static string Get_ConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = WebConfigurationManager.AppSettings["DataSource"];
            builder.InitialCatalog = WebConfigurationManager.AppSettings["DataBase"];
            builder.UserID = WebConfigurationManager.AppSettings["User"];
            builder.Password = WebConfigurationManager.AppSettings["Password"];
            return builder.ConnectionString;
        }

        public static DataSet Get_DataSet(string query)
        {
            string CoString = @"Provider=PervasiveOLEDB;" + WebConfigurationManager.AppSettings["PervasiveSQL"];
            OleDbConnection con = new OleDbConnection(CoString);
            try
            {
            con.Open();
            OleDbDataAdapter dadapter = new OleDbDataAdapter();
            dadapter.SelectCommand = new OleDbCommand(query, con);
            DataSet dset = new DataSet();
            dadapter.Fill(dset, "POrder");
            return dset;
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
            }        
        }



        public static DataTable Get_DataTablePOrder(string prodno, string style)
        {
            DataTable dt = new DataTable();
            string con = @"Provider=PervasiveOLEDB;" + WebConfigurationManager.AppSettings["PervasiveSQL"];
            OleDbConnection conn = new OleDbConnection(con);
            try
            {
                conn.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                string query = "SELECT customer as Cliente, style as Estilo, porder as POrder, description as Description, quantity as Quantity, bundles as Bundles FROM Prod_Order where porder='" + prodno + "' and style='" + style + "'";
                da.SelectCommand = new OleDbCommand(query, conn);
                da.Fill(dt);
                return dt;
            }
            catch
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

       /* public static DataTable Get_DTPOrderbydate(string fecha, string customer)
        {
            DataTable dt = new DataTable();
            string con = @"Provider=PervasiveOLEDB;" + WebConfigurationManager.AppSettings["PervasiveSQL"];
            OleDbConnection conn = new OleDbConnection(con);
            try
            {
                conn.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                string query = "SELECT customer as Cliente, style as Estilo, porder as POrder, description as Description, quantity as Quantity, bundles as Bundles FROM Prod_Order";
                da.SelectCommand = new OleDbCommand(query, conn);
                da.Fill(dt);
                return dt;
            }
            catch
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }*/

        public static DataTable Get_DataTableNewOrdersV10()
        {
            DataTable dt = new DataTable();
            string con = @"Provider=PervasiveOLEDB;" + WebConfigurationManager.AppSettings["PervasiveSQL"];
            OleDbConnection conn = new OleDbConnection(con);
            try
            {
                conn.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                string query = "SELECT customer as Cliente, style as Estilo, porder as POrder, description as Description, quantity as Quantity, bundles as Bundles FROM Prod_Order";
                da.SelectCommand = new OleDbCommand(query, conn);
                da.Fill(dt);
                return dt;
            }
            catch
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Get_DataTableNewBundlesV10()
        {
            DataTable dt = new DataTable();
            string con = @"Provider=PervasiveOLEDB;" + WebConfigurationManager.AppSettings["PervasiveSQL"];
            OleDbConnection conn = new OleDbConnection(con);
            try
            {
                conn.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                string query = "select Bdl as Seq, Bdl, Size_Color, Quantity, prodno from Bundle";
                da.SelectCommand = new OleDbCommand(query, conn);
                da.Fill(dt);
                return dt;
            }
            catch
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

       /* public static DataTable Get_DataTableBundlesbyDate(string fecha)
        {
            DataTable dt = new DataTable();
            string con = @"Provider=PervasiveOLEDB;" + WebConfigurationManager.AppSettings["PervasiveSQL"];
            OleDbConnection conn = new OleDbConnection(con);
            try
            {
                conn.Open();
                OleDbDataAdapter da = new OleDbDataAdapter();
                string query = "select Bdl as Seq, Bdl, Size_Color, Quantity from Bundle where Bundle.reldate>'" + fecha + "'";
                da.SelectCommand = new OleDbCommand(query, conn);
                da.Fill(dt);
                return dt;
            }
            catch
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }*/

        public static void Execute_Query(string query)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand(query, conn);
            try
            {
                conn.Open();
                comm.ExecuteNonQuery();
            }
            catch
            {
            }
            finally
            {
                conn.Close();
            }
        }

        public static void Execute_StoredProcedure(string sp, SqlParameterCollection spc)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand(sp, conn);
            comm.CommandType = CommandType.StoredProcedure;
            int index = 0;

            for (int i = 0; i < spc.Count; i++)
            {
                comm.Parameters.Add(spc[i]);
                if (spc[i].Direction == ParameterDirection.InputOutput)
                {
                    index = i;
                    comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.InputOutput;
                }

            }

        }


    }
}
