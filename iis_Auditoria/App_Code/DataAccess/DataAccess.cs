﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace SistemaAuditores.DataAccess
{
    public class DataAccess
    {
        public static string Get_ConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = WebConfigurationManager.AppSettings["DataSource"];
            builder.InitialCatalog = WebConfigurationManager.AppSettings["DataBase"];
            builder.UserID = WebConfigurationManager.AppSettings["User"];
            builder.Password = WebConfigurationManager.AppSettings["Password"];
            return builder.ConnectionString;
        }

        public static DataTable Get_DataTable(string query)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            try
            {
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                return dt;
            }
            catch
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string Get_UserbyUserName(string username)
        {
            string query = "select IdUsuario from tblusuario where username='" + username.ToString() + "'";
            DataTable dt = Get_DataTable(query);
            string IdUsuario = Convert.ToString(dt.Rows[0]["IdUsuario"]);
            return IdUsuario;
        }

        public static void Execute_Query(string query)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand(query, conn);
            try
            {
                conn.Open();
                comm.ExecuteNonQuery();
            }
            catch
            {
            }
            finally
            {
                conn.Close();
            }
        }

        public static void Execute_StoredProcedure(string sp, SqlParameterCollection spc)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand(sp, conn);
            comm.CommandType = CommandType.StoredProcedure;
            int index = 0;

            for (int i = 0; i < spc.Count; i++)
            {
                comm.Parameters.Add(spc[i]);
                if (spc[i].Direction == ParameterDirection.InputOutput)
                {
                    index = i;
                    comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.InputOutput;
                }

            }

        }

        public static string Save_Bundles(int Id_Order, int NSeq, int Bld, string Size, string color, int Quantity)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveBundles";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Order", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Order;

                comm.Parameters.Add("@NSeq", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = NSeq;

                comm.Parameters.Add("@Bdl", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Bld;

                comm.Parameters.Add("@Size", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Size;

                comm.Parameters.Add("@Color", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = color;

                comm.Parameters.Add("@Quantity", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Quantity;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string Update_Oper(int id_oper, int id_sec)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "Update_Oper";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id_oper", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_oper;

                comm.Parameters.Add("@Id_Seccion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_sec;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string DeleteBundleApro(int id_bund)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "EliminarBundleApr";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_bund;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string DeleteBundleRejec(int id_bundrej)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "EliminarBundleRej";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_BundleRejected", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_bundrej;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdatePOrder(int Id_Cliente, int IdProd)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_UpdatePOrderPervasive";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Cliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Cliente;

                comm.Parameters.Add("@IdProd", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = IdProd;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdatePOrderStyles(int Id_Estilo, int IdProd)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_UpdatePOrderStyles";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Estilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Estilo;

                comm.Parameters.Add("@IdProd", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = IdProd;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string Update_UserLine(string userms, int linea)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "UpdateLineUser";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@userms", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = userms;

                comm.Parameters.Add("@linea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = linea;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int SaveOperario(string codigoB, string nombre, bool activo, string descrip)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveOperario_new";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@codigoB", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = codigoB;

                comm.Parameters.Add("@descripcion", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = descrip;

                comm.Parameters.Add("@nombre", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = nombre;

                comm.Parameters.Add("@activo", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = activo;

                SqlParameter idp = new SqlParameter();
                idp.SqlDbType = SqlDbType.Int;
                idp.Direction = ParameterDirection.Output;
                idp.ParameterName = "@id";  
                comm.Parameters.Add(idp);

                comm.ExecuteNonQuery();
                return Convert.ToInt32(comm.Parameters["@id"].Value);
            }
            catch (Exception)
            {
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int SaveOperacion(string descripcion, string descripcion_completa)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveOperacion_new";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@descripcion", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = descripcion;

                comm.Parameters.Add("@descripcion_completa", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = descripcion_completa;

                SqlParameter ida = new SqlParameter();
                ida.SqlDbType = SqlDbType.Int;
                ida.Direction = ParameterDirection.Output;
                ida.ParameterName = "@ido";
                comm.Parameters.Add(ida);

                comm.ExecuteNonQuery();
                return Convert.ToInt32(comm.Parameters["@ido"].Value);
            }
            catch (Exception)
            {
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveOperarioOperacion(int id_operacion, int id_seccion, int id_operario, bool estado)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveOperarioOperacion_new";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id_operacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_operacion;

                comm.Parameters.Add("@id_seccion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_seccion;

                comm.Parameters.Add("@id_operario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_operario;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateEstado(int id_operarioOpe, bool estado)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_UpdateEstado_New";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idoperarioOpe", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_operarioOpe;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateStyleName(int id, string style)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_UpdateStyle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Style", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@Style", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = style;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string Update_Style_All(int id, string sty, float p, bool w)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_ActualizarStyle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Style", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@Style", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = sty;

                comm.Parameters.Add("@Wash", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = w;

                comm.Parameters.Add("@Price", SqlDbType.Money);
                comm.Parameters[comm.Parameters.Count - 1].Value = p;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateEstadoLinea(int id_linea, bool estado1)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_UpdateEstadoLinea_New";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id_linea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_linea;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado1;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string updateStyleWash(int id_Style, bool wash)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_StateWash";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Style", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_Style;

                comm.Parameters.Add("@wash", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = wash;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateLinea(int id_linea, string numero, int idcliente, bool estado2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_UpdateLine_New";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id_linea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_linea;

                comm.Parameters.Add("@numero", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = numero;

                comm.Parameters.Add("@idcliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcliente;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado2;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int SaveLinea(int id_planta, string numero, int idcliente, bool estado)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveLine_new";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id_planta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_planta;

                comm.Parameters.Add("@numero", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = numero;


                comm.Parameters.Add("@idcliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcliente;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                SqlParameter idll = new SqlParameter();
                idll.SqlDbType = SqlDbType.Int;
                idll.Direction = ParameterDirection.Output;
                idll.ParameterName = "@idl";
                comm.Parameters.Add(idll);

                comm.ExecuteNonQuery();
                return Convert.ToInt32(comm.Parameters["@idl"].Value);
            }
            catch (Exception)
            {
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveStyle(string style, decimal price, bool wash)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_CreateStyle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Style", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = style;

                comm.Parameters.Add("@Price", SqlDbType.Money);
                comm.Parameters[comm.Parameters.Count - 1].Value = price;

                comm.Parameters.Add("@Wash", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = wash;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveSeccion(int id_linea, int idModulo)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveSeccion_new";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id_linea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_linea;

                comm.Parameters.Add("@id_modulo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idModulo;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateLineaOperario(int idoperario, int idseccion, bool estado)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_ChangeLineOperator_New";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idoperario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idoperario;

                comm.Parameters.Add("@idseccion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idseccion;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}

