﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using SistemaAuditores.Business;
using System.Web;

namespace SistemaAuditores.DataAccess
{
    public class OrderDetailDA : DataAccess
    {
        public static DataTable Get_OrderbyId(string id_order)
        {
            string query = "select po.Id_Order,po.POrder,po.Description,po.Quantity,po.Bundles,c.Cliente,s.Style from POrder po join Cliente c on po.Id_Cliente=c.Id_Cliente join Style s on po.Id_Style=s.Id_Style where po.Id_Order=" + id_order;
            DataTable dt = Get_DataTable(query);
            return dt;
        }

        #region  save IP Audit        

        public static DataTable GetBultoByOrder(int idOrder)
        {

            string query = "select b.* from Bundle b where b.Id_Order =" + idOrder + " order by b.NSeq asc";

            DataTable dt = Get_DataTable(query);
            return dt;

        }

        public static DataTable GetBultoByOrder(int idOrder, int idAudi, int idInspector, int idmodulo)
        {

            string query = "select b.* from Bundle b"
                  + " where b.Id_Order = " + idOrder
                  + " except"
                  + " select b.* from Bundle b left"
                  + " join tbInspecProcAprobados ba on b.Id_Bundle = ba.IdBulto"
                  + " where b.Id_Order =" + idOrder + "  and ba.idModulo =" + idmodulo
                  + " except"
                  + " select b.* from Bundle b join tbInspecProcRechazado br on b.Id_Bundle = br.idBulto"
                  + " where Id_Order = " + idOrder + " and br.Estado = 0"
                  + " and br.idModulo = " + idmodulo
                  + " except"
                  + " select b.* from Bundle b join tbInspecProcRechazado br on b.Id_Bundle = br.idBulto"
                  + " where Id_Order =" + idOrder + "  and br.Estado = 1 and br.idModulo = " + idmodulo + " and br.idInspector!=" + idInspector
                  + " order by b.NSeq asc";

            DataTable dt = Get_DataTable(query);
            return dt;

        }

        public static string SaveIPApproved(int idbulto, string idinspector, int idlinea, int idmodulo, int muestra)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveIPApproved";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@idmodulo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmodulo;

                comm.Parameters.Add("@muestra", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestra;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveIPApprovedSem(int idbulto, string idinspector, int idlinea, int idmodulo, int muestra, string codoperario, string operario, string operacion)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveIPApprovedSem";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@idmodulo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmodulo;

                comm.Parameters.Add("@muestra", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestra;

                comm.Parameters.Add("@codoperario", SqlDbType.NChar, 7);
                comm.Parameters[comm.Parameters.Count - 1].Value = codoperario;

                comm.Parameters.Add("@operario", SqlDbType.VarChar, 80);
                comm.Parameters[comm.Parameters.Count - 1].Value = operario;

                comm.Parameters.Add("@operacion", SqlDbType.VarChar, 50);
                comm.Parameters[comm.Parameters.Count - 1].Value = operacion;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveIPRepair(int Id_Bundle, int idmodulo)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveIPRepair";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

                comm.Parameters.Add("@idmodulo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmodulo;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int SaveIPRejectedSem(int idbulto, int idinspector, int idlinea, int idmodulo, int muestreo, string guid, string codoperario, string operario, string operacion)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {


                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveIPRejectedSem";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@idinspector", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@idmodulo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmodulo;

                comm.Parameters.Add("@muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestreo;

                comm.Parameters.Add("@guid", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = guid;

                comm.Parameters.Add("@codoperario", SqlDbType.NChar, 7);
                comm.Parameters[comm.Parameters.Count - 1].Value = codoperario;

                comm.Parameters.Add("@operario", SqlDbType.VarChar, 80);
                comm.Parameters[comm.Parameters.Count - 1].Value = operario;

                comm.Parameters.Add("@operacion", SqlDbType.VarChar, 50);
                comm.Parameters[comm.Parameters.Count - 1].Value = operacion;

                comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int SaveIPRejected(int idbulto, int idinspector, int idlinea, int idmodulo, int muestreo, string guid)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {


                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveIPRejected";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@idinspector", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@idmodulo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmodulo;

                comm.Parameters.Add("@muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestreo;

                comm.Parameters.Add("@guid", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = guid;

                comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveIPRejectedDetail(int idbbrechazo, int iddefecto, int idposicion, int idoperarioope, int idoperario, int idoperacion, int cantidad, int numaudi)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveIPRejectedDetail";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbbrechazo;

                comm.Parameters.Add("@iddefecto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = iddefecto;

                comm.Parameters.Add("@idposicion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idposicion;

                comm.Parameters.Add("@idoperarioope", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idoperarioope;

                comm.Parameters.Add("@idoperario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idoperario;

                comm.Parameters.Add("@idoperacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idoperacion;

                comm.Parameters.Add("@cantidadDef", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@numAudi", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = numaudi;

                comm.ExecuteNonQuery();
                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveIPRejectedDetailSem(int idbbrechazo, int iddefecto, int cantidad, int numaudi)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveIPRejectedDetailSem";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbbrechazo;

                comm.Parameters.Add("@iddefecto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = iddefecto;

                comm.Parameters.Add("@cantidadDef", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@numAudi", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = numaudi;

                comm.ExecuteNonQuery();
                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region  save AI Audit
        public static string SaveAIApproved(int idbulto, string idinspector, int idlinea, int muestra)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAIApproved";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@muestra", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestra;


                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string spdSaveAIRepair(int Id_Bundle)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAIRepair";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int spdSaveAIRejected(int idbulto, int idinspector, int idlinea, int muestreo, string guid)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAIRejected";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@idinspector", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestreo;

                comm.Parameters.Add("@guid", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = guid;

                comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveAIRejectedDetail(int idbbrechazo, int iddefecto, int idposicion, int cantidad, bool reauditoria)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAIRejectedDetail";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbbrechazo;

                comm.Parameters.Add("@iddefecto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = iddefecto;

                comm.Parameters.Add("@idposicion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idposicion;

                comm.Parameters.Add("@cantidadDef", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@reauditoria", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = reauditoria;

                comm.ExecuteNonQuery();
                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region  save bb audit

        public static DataTable GetBultoByOrder(int idOrder, int idAudi, int idInspector)
        {
            string query = "";
            switch (idAudi)
            {

                case 2:
                    query = " select b.* from Bundle b left join tbBultoaBultoAprovados ba on b.Id_Bundle=ba.IdBulto where b.Id_Order=" + idOrder + "  and isnull(idBBAprovado,0)=0"
                          + " except"
                          + " select b.* from Bundle b join tbBultoaBultoRechazado br on b.Id_Bundle = br.idBulto  where Id_Order = " + idOrder + " and (br.Estado=0 or br.idInspector !=" + idInspector + ")"
                          + " order by b.NSeq asc";

                    break;
                case 3:
                    query = " select b.* from Bundle b left join tbAuditInterAprovados ba on b.Id_Bundle=ba.IdBulto where b.Id_Order=" + idOrder + "  and isnull(idBBAprovado,0)=0"
                          + " except"
                          + " select b.* from Bundle b join tbAuditInterRechazado br on b.Id_Bundle = br.idBulto  where Id_Order = " + idOrder + " and (br.Estado=0 or br.idInspector !=" + idInspector + ")"
                          + " order by b.NSeq asc";

                    break;
                case 4:
                    //tablas pendientes
                    query = " select b.* from Bundle b left join tbAuditInterAprovados ba on b.Id_Bundle=ba.IdBulto where b.Id_Order=" + idOrder + "  and isnull(idBBAprovado,0)=0"
                          + " except"
                          + " select b.* from Bundle b join tbAuditInterRechazado br on b.Id_Bundle = br.idBulto  where Id_Order = " + idOrder + " and (br.Estado=0 or br.idInspector !=" + idInspector + ")"
                          + " order by b.NSeq asc";

                    break;

                case 9:
                    query = " select b.*from Bundle b left join BultoAprobadoIF ba on b.Id_Bundle = ba.IdBulto where b.Id_Order = " + idOrder + " and isnull(idBultoAprobadoIF,0)= 0"
                          + " except"
                          + " select b.* from Bundle b join BultoRechazadoIF br on b.Id_Bundle = br.idBulto  where Id_Order = " + idOrder + " and(br.Estado = 0 or br.idInspector != " + idInspector + ")"
                          + " order by b.NSeq asc";

                    break;

                default:
                    break;
            }

            DataTable dt = Get_DataTable(query);
            return dt;

        }

        public static string SaveBBApproved(int idbulto, string idinspector, int idlinea, int muestra)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveBBApproved";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@muestra", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestra;


                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveBBRepair(int Id_Bundle)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveBBRepair";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int SaveBBRejected(int idbulto, int idinspector, int idlinea, int muestreo, int piezasrej, string guid)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveBBRejected";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@idinspector", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestreo;

                comm.Parameters.Add("@guid", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = guid;

                comm.Parameters.Add("@Piezas", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = piezasrej;

                comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveBBRejectedDetail(int idbbrechazo, int iddefecto, int idposicion, int cantidad, bool reauditoria)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveBBRejectedDetail";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbbrechazo;

                comm.Parameters.Add("@iddefecto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = iddefecto;

                comm.Parameters.Add("@idposicion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idposicion;

                comm.Parameters.Add("@cantidadDef", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@reauditoria", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = reauditoria;

                comm.ExecuteNonQuery();
                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region Auditoria bulto a bulto en Inspeccion Final
        public static string GuardarInspeccionFAprobada(int idbulto, string idinspector, int idlinea, int muestra)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarInpsFinalAprobada";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@muestra", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestra;


                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int GuardarRechazoInsFinal(int idbulto, int idinspector, int idlinea, int muestreo, int piezasrej, string guid)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarRechazoInspFinal";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@idinspector", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestreo;

                comm.Parameters.Add("@guid", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = guid;

                comm.Parameters.Add("@Piezas", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = piezasrej;

                comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string GuardarDetRechazoInspFinal(int idbbrechazo, int iddefecto, int idposicion, int cantidad, bool reauditoria)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarDetRechazoInspFinal";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbbrechazo;

                comm.Parameters.Add("@iddefecto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = iddefecto;

                comm.Parameters.Add("@idposicion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idposicion;

                comm.Parameters.Add("@cantidadDef", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@reauditoria", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = reauditoria;

                comm.ExecuteNonQuery();
                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string GuardarReparacionBulto(int Id_Bundle)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarReparacionBulto";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region  save AE Audit
        public static string SaveAEApproved(int idbulto, string idinspector, int idlinea, int muestra)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAEApproved";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@muestra", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestra;


                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string spdSaveAERepair(int Id_Bundle)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAERepair";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int spdSaveAERejected(int idbulto, int idinspector, int idlinea, int muestreo, string guid)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAERejected";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@idinspector", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestreo;

                comm.Parameters.Add("@guid", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = guid;

                comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveAERejectedDetail(int idbbrechazo, int iddefecto, int idposicion, int cantidad, bool reauditoria)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAERejectedDetail";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbbrechazo;

                comm.Parameters.Add("@iddefecto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = iddefecto;

                comm.Parameters.Add("@idposicion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idposicion;

                comm.Parameters.Add("@cantidadDef", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@reauditoria", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = reauditoria;

                comm.ExecuteNonQuery();
                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }


        #endregion

        #region reportesCalidadIP

        public static DataTable spdRepIPTablaDefectosAll(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepIPTablaDefectosAll";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepIPTablaDefectosXCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepIPTablaDefectosXCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idCorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepIPTablaDefectosXEstilo(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepIPTablaDefectosXEstilo";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idEstilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepIPTablaDefectosXLinea(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepIPTablaDefectosXLinea";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idLinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepIPTablaDefectosXLineaStyle(DateTime fecha1, DateTime fecha2, int id, int id2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepIPTablaDefectosXLineaStyle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idLinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@idStyle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id2;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepIPTablaDefectosXPlanta(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepIPTablaDefectosXPlanta";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idPlanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region reportesCalidadBB
        public static DataTable spdRepBBAqlOqlAll(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepBBAqlOqlAll";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepBBAqlOqlXCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepBBAqlOqlXCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idCorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepBBAqlOqlXEstilo(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepBBAqlOqlXEstilo";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idEstilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepBBAqlOqlXLinea(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepBBAqlOqlXLinea";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idLinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepBBAqlOqlXLineaStyle(DateTime fecha1, DateTime fecha2, int id, int id2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepBBAqlOqlXLineaStyle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idLinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@idStyle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id2;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepBBAqlOqlXPlanta(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepBBAqlOqlXPlanta";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idPlanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepBBTablaDefectosAll(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepBBTablaDefectosAll";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepBBTablaDefectosXCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepBBTablaDefectosXCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idCorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepBBTablaDefectosXEstilo(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepBBTablaDefectosXEstilo";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idEstilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepBBTablaDefectosXLinea(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepBBTablaDefectosXLinea";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idLinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepBBTablaDefectosXLineaStyle(DateTime fecha1, DateTime fecha2, int id, int id2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepBBTablaDefectosXLineaStyle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idLinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@idStyle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id2;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepBBTablaDefectosXPlanta(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepBBTablaDefectosXPlanta";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idPlanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region reportesCalidadAI
        public static DataTable spdRepAIAqlOqlAll(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAIAqlOqlAll";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAIAqlOqlXCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAIAqlOqlXCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idCorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAIAqlOqlXEstilo(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAIAqlOqlXEstilo";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idEstilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAIAqlOqlXLinea(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAIAqlOqlXLinea";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idLinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAIAqlOqlXPlanta(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAIAqlOqlXPlanta";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idPlanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAITablaDefectosAll(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAITablaDefectosAll";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAITablaDefectosXCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAITablaDefectosXCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idCorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAITablaDefectosXEstilo(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAITablaDefectosXEstilo";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idEstilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAITablaDefectosXLinea(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAITablaDefectosXLinea";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idLinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAITablaDefectosXLineaStyle(DateTime fecha1, DateTime fecha2, int id, int id2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAITablaDefectosXLineaStyle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idLinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@idStyle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id2;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAITablaDefectosXPlanta(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAITablaDefectosXPlanta";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idPlanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region reportesCalidadAE

        public static DataTable spdRepAETablaDefectosAll(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAETablaDefectosAll";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAETablaDefectosXCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAETablaDefectosXCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idCorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAETablaDefectosXEstilo(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAETablaDefectosXEstilo";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idEstilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAETablaDefectosXLinea(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAETablaDefectosXLinea";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idLinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAETablaDefectosXLineaStyle(DateTime fecha1, DateTime fecha2, int id, int id2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAETablaDefectosXLineaStyle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idLinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@idStyle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id2;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable spdRepAETablaDefectosXPlanta(DateTime fecha1, DateTime fecha2, int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepAETablaDefectosXPlanta";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idPlanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@date2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Get_DataTable
        public static DataTable Get_OrderDetail()
        {
            string query = "select * from Bundle";
            DataTable dt = Get_DataTable(query);
            return dt;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <param name="id_order"></param>
        /// <param name="id_sec"></param>
        /// <param name="idinspector"></param>
        /// <returns></returns>
        public static DataTable Get_OrderDetailbyIdOrderBulto(int id_order, int id_sec, string idinspector)
        {
            string query = " Select * from Bundle where Id_Order=" + id_order + " and Id_Bundle not in"
                          + " (select ap.Id_Bundle from BundleApprovedBulto ap join Bundle b on ap.Id_Bundle = b.Id_Bundle where b.Id_Order = " + id_order
                          + " and ap.idseccion =" + id_sec
                          + " union"
                          + " select ap.Id_Bundle"
                          + " from BundleRejectedBulto ap"
                          + " join Bundle b on ap.Id_Bundle = b.Id_Bundle"
                          + " where b.Id_Order = " + id_order + "  and ap.idseccionInsp != " + idinspector + ") order by NSeq asc";
            DataTable dt = Get_DataTable(query);
            return dt;
        }

        public static DataTable Get_OrderDetailbyIdOrder(int id_order, int id_sec, string idinspector)
        {
            //string query = "Select * from Bundle where Id_Order=" + id_order + " and Id_Bundle not in (select Id_Bundle from BundleApproved b join OperacionOperario op on b.id_operarioOpe=op.id_operarioOpe where op.id_seccion =" + id_sec + ")  order by NSeq asc";
            string query = " Select * from Bundle where Id_Order = " + id_order + " and Id_Bundle not in"
                           + " (select ap.Id_Bundle from BundleApproved ap join Bundle b on ap.Id_Bundle = b.Id_Bundle where b.Id_Order = " + id_order
                           + "  and ap.idseccion =" + id_sec
                           + " union"
                           + " select ap.Id_Bundle"
                           + " from BundleRejected ap"
                           + " join Bundle b on ap.Id_Bundle = b.Id_Bundle"
                           + " join seccionInspector si on ap.idseccionInspector=si.idseccionInspector"
                           + " where b.Id_Order = " + id_order + "  and ap.idseccionInspector != " + idinspector + " and idseccion=" + id_sec + " )  order by NSeq asc";

            DataTable dt = Get_DataTable(query);
            return dt;
        }

        public static DataTable Get_BundleRejected(string operario, string fecha, int biohorario)
        {
            string query = "select row_number() OVER (ORDER BY O.nombre) AS id, B.Bld as Bulto, PO.POrder as NumeroCorte, (CONVERT(VARCHAR, BR.FechaRecibido, 103) + ' '  + BR.HoraRecibido) as FechaRecibido, DF.Id_Area as Id_Area, DF.Id_defectos as Id_defectos from bundlerejected BR"
                            + " inner join Bundle B on BR.Id_Bundle=B.Id_Bundle"
                            + " inner join POrder PO on BR.Id_Order = PO.Id_Order"
                            + " inner join DefectosxArea DF on BR.Id_DefxArea= DF.Id_DefxArea"
                            + " inner join Operario O on BR.Id_operario = o.id_operario"
                            + " where  O.nombre = '" + operario + "' and br.FechaRecibido='" + fecha + "' and BR.id_bio=" + biohorario;
            DataTable dt = Get_DataTable(query);
            return dt;
        }

        public static DataTable Get_BundleRejected_ALL_bio_corte(string id_order, string id_bundle)
        {
            string query = "select row_number() OVER (ORDER BY O.nombre) AS id, B.Bld as Bulto, PO.POrder as NumeroCorte, (CONVERT(VARCHAR, BR.FechaRecibido, 103) + ' '  + BR.HoraRecibido) as FechaRecibido, DF.Id_Area as Id_Area, DF.Id_defectos as Id_defectos, O.nombre as Nombre, br.cantidad_defectos as Defectos from bundlerejected BR"
                            + " inner join Bundle B on BR.Id_Bundle=B.Id_Bundle"
                            + " inner join POrder PO on BR.Id_Order = PO.Id_Order"
                            + " inner join DefectosxArea DF on BR.Id_DefxArea= DF.Id_DefxArea"
                            + " inner join Operario O on BR.Id_operario = o.id_operario"
                            + " where  br.Id_Order =" + id_order + " and br.Id_Bundle=" + id_bundle;
            DataTable dt = Get_DataTable(query);
            return dt;
        }

        public static DataTable Get_BundleRejected_ALL_bio(string id_bundle, int idseccionInspector)
        {
            string query = "select row_number() OVER (ORDER BY O.nombre) AS id, B.Bld as Bulto, PO.POrder as NumeroCorte, (CONVERT(VARCHAR, BR.FechaRecibido, 103) + ' '  + BR.HoraRecibido) as FechaRecibido, DF.Id_Area as Id_Area, DF.Id_defectos as Id_defectos, O.nombre as Nombre, br.cantidad_defectos as Defectos from bundlerejected BR"
                            + " inner join Bundle B on BR.Id_Bundle=B.Id_Bundle"
                            + " inner join POrder PO on BR.Id_Order = PO.Id_Order"
                            + " inner join DefectosxArea DF on BR.Id_DefxArea= DF.Id_DefxArea"
                            + " inner join Operario O on BR.Id_operario = o.id_operario"
                            + " where br.Id_Bundle=" + id_bundle + " and br.Id_Inspector = " + idseccionInspector;
            DataTable dt = Get_DataTable(query);
            return dt;
        }

        public static DataTable Get_BundleRejected_ALL_bio_Bulto(string id_bundle, int Id_Inspector)
        {
            string query = " select row_number() OVER (ORDER BY O.nombre) AS id, B.Bld as Bulto, PO.POrder as NumeroCorte, (CONVERT(VARCHAR, BR.FechaRecibido, 103) + ' '  + BR.HoraRecibido) as FechaRecibido, DF.Id_defectos as Id_defectos, O.nombre as Nombre, br.cantidad_defectos as Defectos"
                            + " from BundleRejectedBulto BR"
                            + " join Bundle B on BR.Id_Bundle = B.Id_Bundle"
                            + " join porder po on b.id_order = po.id_order"
                            + " join Defectos DF on BR.Id_DefxArea = DF.id_defectos"
                            + " join OperacionOperario op on br.id_operarioOpe = op.id_operarioOpe"
                            + " inner join Operario O on op.id_operario = o.id_operario"
                            + " where  br.Id_Bundle=" + id_bundle + " and br.idseccionInsp = " + Id_Inspector;

            DataTable dt = Get_DataTable(query);
            return dt;
        }

        public static DataTable Get_BundlesApproved(int id_order, int id_bundle, string id_seccion)
        {
            string query = "Select * from BundleApproved where Id_Order=" + id_order + " and Id_Bundle=" + id_bundle + " and Id_Seccion=" + id_seccion;
            DataTable dt = Get_DataTable(query);
            return dt;
        }

        public static DataTable Get_BundlesRejected(int id_order, int id_bundle, string id_seccion)
        {
            string query = "Select * from BundleRejected where Id_Order=" + id_order + " and Id_Bundle=" + id_bundle + " and Id_Seccion=" + id_seccion;
            DataTable dt = Get_DataTable(query);
            return dt;
        }



        #endregion

        #region proc_Aprob_Rejec_bundle

        public static string SaveBundlesApproved(int Id_Bundle, int id_bio, int Id_Seccion, string Id_Inspector, int id_muestreo, DateTime FechaAprobado, string HoraAprobado, int cantidad_defectos)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveBundlesApproved";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

                comm.Parameters.Add("@id_bio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_bio;

                comm.Parameters.Add("@Id_Seccion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Seccion;

                comm.Parameters.Add("@Id_Inspector", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Inspector;

                comm.Parameters.Add("@id_muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_muestreo;

                comm.Parameters.Add("@FechaAprobado", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = FechaAprobado.Date;

                comm.Parameters.Add("@HoraAprobado", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = HoraAprobado;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveBundlesApprovedBulto(int Id_Bundle, int id_bio, int Id_Seccion, string Id_Inspector, int id_muestreo, DateTime FechaAprobado, string HoraAprobado, int cantidad_defectos)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveBundlesApprovedBulto";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

                comm.Parameters.Add("@id_bio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_bio;

                comm.Parameters.Add("@Id_Seccion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Seccion;

                comm.Parameters.Add("@Id_Inspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Inspector;

                comm.Parameters.Add("@id_muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_muestreo;

                comm.Parameters.Add("@FechaAprobado", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = FechaAprobado.Date;

                comm.Parameters.Add("@HoraAprobado", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = HoraAprobado;



                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveBundlesRepair(int Id_Bundle, int id_bio, int Id_Seccion, string Id_Inspector, int id_muestreo, DateTime FechaAprobado, string HoraAprobado)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveBundlesRepair";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

                comm.Parameters.Add("@id_bio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_bio;

                comm.Parameters.Add("@Id_Seccion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Seccion;

                comm.Parameters.Add("@Id_Inspector", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Inspector;

                comm.Parameters.Add("@id_muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_muestreo;

                comm.Parameters.Add("@FechaAprobado", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = FechaAprobado.Date;

                comm.Parameters.Add("@HoraAprobado", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = HoraAprobado;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveBundlesRepairBulto(int Id_Bundle, int id_bio, int Id_Seccion, string Id_Inspector, int id_muestreo, DateTime FechaAprobado, string HoraAprobado)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveBundlesRepairBulto";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

                comm.Parameters.Add("@id_bio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_bio;

                comm.Parameters.Add("@Id_Seccion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Seccion;

                comm.Parameters.Add("@Id_Inspector", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Inspector;

                comm.Parameters.Add("@id_muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_muestreo;

                comm.Parameters.Add("@FechaAprobado", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = FechaAprobado.Date;

                comm.Parameters.Add("@HoraAprobado", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = HoraAprobado;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveBundlesRejected(int Id_Bundle, int id_bio, int Id_DefxArea, int Id_operario, int idseccionInsp, int id_muestreo, DateTime FechaRecibido, string HoraRecibido, int cantidad_defectos, bool ayuda)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveBundlesRejected";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

                comm.Parameters.Add("@id_bio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_bio;

                comm.Parameters.Add("@Id_DefxArea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_DefxArea;

                comm.Parameters.Add("@Id_operario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_operario;

                comm.Parameters.Add("@idseccionisp", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idseccionInsp;

                comm.Parameters.Add("@id_muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_muestreo;

                comm.Parameters.Add("@FechaRecibido", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = FechaRecibido.Date;

                comm.Parameters.Add("@HoraRecibido", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = HoraRecibido;

                comm.Parameters.Add("@cantidad_defectos", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad_defectos;

                comm.Parameters.Add("@ayuda", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = ayuda;

                comm.ExecuteNonQuery();
                return "OK";
            }

            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveBundlesRejectedBulto(int Id_Bundle, int id_bio, int Id_DefxArea, int Id_operario, int idseccionInsp, int id_muestreo, DateTime FechaRecibido, string HoraRecibido, int cantidad_defectos, bool ayuda)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveBundlesRejectedBundle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

                comm.Parameters.Add("@id_bio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_bio;

                comm.Parameters.Add("@Id_DefxArea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_DefxArea;

                comm.Parameters.Add("@Id_operario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_operario;

                comm.Parameters.Add("@idseccionisp", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idseccionInsp;

                comm.Parameters.Add("@id_muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_muestreo;

                comm.Parameters.Add("@FechaRecibido", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = FechaRecibido.Date;

                comm.Parameters.Add("@HoraRecibido", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = HoraRecibido;

                comm.Parameters.Add("@cantidad_defectos", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad_defectos;

                comm.Parameters.Add("@ayuda", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = ayuda;

                comm.ExecuteNonQuery();
                return "OK";
            }

            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        //public static string SaveBundlesRejectedHelp(int Id_Bundle, int id_bio, int Id_DefxArea, int Id_operario, int idseccionInsp, int id_muestreo, DateTime FechaRecibido, string HoraRecibido, int cantidad_defectos,bool ayuda)
        //{
        //    SqlConnection conn = new SqlConnection(Get_ConnectionString());
        //    SqlCommand comm = new SqlCommand();

        //    try
        //    {
        //        conn.Open();
        //        comm.Connection = conn;
        //        comm.CommandText = "usp_SaveBundlesRejected";
        //        comm.CommandType = CommandType.StoredProcedure;
        //        comm.Parameters.Clear();

        //        comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

        //        comm.Parameters.Add("@id_bio", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = id_bio;

        //        comm.Parameters.Add("@Id_DefxArea", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = Id_DefxArea;

        //        comm.Parameters.Add("@Id_operario", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = Id_operario;

        //        comm.Parameters.Add("@idseccionisp", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = idseccionInsp;

        //        comm.Parameters.Add("@id_muestreo", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = id_muestreo;

        //        comm.Parameters.Add("@FechaRecibido", SqlDbType.DateTime);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = FechaRecibido.Date;

        //        comm.Parameters.Add("@HoraRecibido", SqlDbType.NVarChar);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = HoraRecibido;

        //        comm.Parameters.Add("@cantidad_defectos", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = cantidad_defectos;

        //        comm.ExecuteNonQuery();
        //        return "OK";
        //    }

        //    catch (Exception ex)
        //    {
        //        return ex.Message;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}

        //public static string SaveBundlesRejectedBultoHelp(int Id_Bundle, int id_bio, int Id_DefxArea, int Id_operario, int idseccionInsp, int id_muestreo, DateTime FechaRecibido, string HoraRecibido, int cantidad_defectos,bool ayuda)
        //{
        //    SqlConnection conn = new SqlConnection(Get_ConnectionString());
        //    SqlCommand comm = new SqlCommand();

        //    try
        //    {
        //        conn.Open();
        //        comm.Connection = conn;
        //        comm.CommandText = "usp_SaveBundlesRejectedBundle";
        //        comm.CommandType = CommandType.StoredProcedure;
        //        comm.Parameters.Clear();

        //        comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

        //        comm.Parameters.Add("@id_bio", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = id_bio;

        //        comm.Parameters.Add("@Id_DefxArea", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = Id_DefxArea;

        //        comm.Parameters.Add("@Id_operario", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = Id_operario;

        //        comm.Parameters.Add("@idseccionisp", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = idseccionInsp;

        //        comm.Parameters.Add("@id_muestreo", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = id_muestreo;

        //        comm.Parameters.Add("@FechaRecibido", SqlDbType.DateTime);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = FechaRecibido.Date;

        //        comm.Parameters.Add("@HoraRecibido", SqlDbType.NVarChar);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = HoraRecibido;

        //        comm.Parameters.Add("@cantidad_defectos", SqlDbType.Int);
        //        comm.Parameters[comm.Parameters.Count - 1].Value = cantidad_defectos;

        //        comm.ExecuteNonQuery();
        //        return "OK";
        //    }

        //    catch (Exception ex)
        //    {
        //        return ex.Message;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}

        #endregion

        #region Procedimientos no usados

        public static string SaveAusencia(int Id_biohorario, int Id_Operario, DateTime fecha, string Hora)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveAusencia";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id_biohorario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_biohorario;

                comm.Parameters.Add("@id_operario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Operario;

                comm.Parameters.Add("@fecha", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha.Date;

                comm.Parameters.Add("@hora", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Hora;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveBundlesNoAudit(int Id_Bundle, int Id_Order, int id_bio, int Id_Linea, int Id_Seccion, int Id_inspector, int id_muestreo, DateTime FechaRegistro, string HoraRegistro)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveBundlesNoAudit";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Bundle", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Bundle;

                comm.Parameters.Add("@Id_Order", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Order;

                comm.Parameters.Add("@id_bio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_bio;

                comm.Parameters.Add("@Id_Linea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Linea;

                comm.Parameters.Add("@Id_Seccion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Seccion;

                comm.Parameters.Add("@Id_inspector", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_inspector;

                comm.Parameters.Add("@id_muestreo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_muestreo;

                comm.Parameters.Add("@FechaRegistro", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = FechaRegistro;

                comm.Parameters.Add("@HoraRegistro", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = HoraRegistro;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }


        public static string SaveAllBundles_LocalBD(string Id_Order, int NSeq, int Bdl, string Size, string Color, int Quantity, int Approved, string Comments)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveBundles";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Order", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Order;

                comm.Parameters.Add("@NSeq", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = NSeq;

                comm.Parameters.Add("@Bdl", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Bdl;

                comm.Parameters.Add("@Size", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Size;

                comm.Parameters.Add("@Color", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Color;

                comm.Parameters.Add("@Quantity", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Quantity;

                comm.Parameters.Add("@Approved", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Approved;

                comm.Parameters.Add("@Comments", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Comments;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string Update(int Id_OrderDetail, string IdUsuario, DateTime CreatedDate, DateTime ModifiedDate, int aprobado, string comments)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_UpdateOrderDetail";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_OrderDetail", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_OrderDetail;

                comm.Parameters.Add("@IdUsuario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = IdUsuario;

                comm.Parameters.Add("@CreatedDate", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = CreatedDate;

                comm.Parameters.Add("@ModifiedDate", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = ModifiedDate;

                comm.Parameters.Add("@Approved", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = aprobado;

                comm.Parameters.Add("@Comments", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = comments;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateModifiedDate(int Id_OrderDetail, string IdUsuario, DateTime ModifiedDate, int aprobado, string comments)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_UpdateODetailsModifiedDat";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_OrderDetail", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_OrderDetail;

                comm.Parameters.Add("@IdUsuario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = IdUsuario;

                comm.Parameters.Add("@ModifiedDate", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = ModifiedDate;

                comm.Parameters.Add("@Approved", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = aprobado;

                comm.Parameters.Add("@Comments", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = comments;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region procedimientos registros de medidas
        public static string SaveMedida(int idpuntomedida, int idbulto, int unidadesT, int unidadesR, string idinspector)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {


                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarMedidas";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idpuntomedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntomedida;

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@unidadesTotal", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesT;

                comm.Parameters.Add("@unidadesrestantes", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesR;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveMedidaBulto(int idMaster, int idmedida, string insp)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_GuardarMedidasBulto";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idMaster;

                comm.Parameters.Add("@idMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.Parameters.Add("@insp", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = insp;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateMedida(int idpuntomaster, int unidadesres)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {


                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidas";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idpuntomaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntomaster;

                comm.Parameters.Add("@unidadesrestantes", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesres;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateMedidaBulto(int idmedidamaster, int idmedida, string insp)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidaBulto";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMedidamaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedidamaster;

                comm.Parameters.Add("@idmedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.Parameters.Add("@insp", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = insp;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string Asignacion(int idporder, int idlinea, string d, int wash)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_asignacionLine";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idporder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idporder;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@d", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = d;

                comm.Parameters.Add("@wash", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = wash;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string Asignacion1(int idporder, int idlinea, string d, int wash)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_asignacionLine1";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idporder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idporder;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@d", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = d;

                comm.Parameters.Add("@wash", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = wash;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region procedimientos registros de medidasIntex
        public static string SaveMedidaIntex(int idpuntomedida, int idbulto, int unidadesT, int unidadesR, string idinspector)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarMedidasIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idpuntomedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntomedida;

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@unidadesTotal", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesT;

                comm.Parameters.Add("@unidadesrestantes", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesR;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveMedidaBultoIntex(int idMaster, int idmedida, string inspector)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_GuardarMedidasBultoIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idMaster;

                comm.Parameters.Add("@idMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.Parameters.Add("@insp", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = inspector;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateMedidaIntex(int idpuntomaster, int unidadesres)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidasIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idpuntomaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntomaster;

                comm.Parameters.Add("@unidadesrestantes", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesres;



                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateMedidaBultoIntex(int idmedidamaster, int idmedida, string inspector)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidaBultoIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMedidamaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedidamaster;

                comm.Parameters.Add("@idmedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.Parameters.Add("@insp", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = inspector;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region procedimientos registros de medidasAL
        public static string SaveMedidaAL(int idpuntomedida, int idbulto, int unidadesT, int unidadesR, string idinspector)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarMedidasAL";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idpuntomedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntomedida;

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@unidadesTotal", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesT;

                comm.Parameters.Add("@unidadesrestantes", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesR;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveMedidaBultoAL(int idMaster, int idmedida, string insp)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_GuardarMedidasBultoAL";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idMaster;

                comm.Parameters.Add("@idMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.Parameters.Add("@insp", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = insp;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateMedidaAL(int idpuntomaster, int unidadesres)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidasAL";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idpuntomaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntomaster;

                comm.Parameters.Add("@unidadesrestantes", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesres;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateMedidaBultoAL(int idmedidamaster, int idmedida, string insp)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidaBultoAL";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMedidamaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedidamaster;

                comm.Parameters.Add("@idmedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.Parameters.Add("@insp", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = insp;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region procedimientos registros de medidasEmb
        public static string SaveMedidaEmb(int idpuntomedida, int idbulto, int unidadesT, int unidadesR, string idinspector)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarMedidasEmb";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idpuntomedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntomedida;

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@unidadesTotal", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesT;

                comm.Parameters.Add("@unidadesrestantes", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesR;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveMedidaBultoEmb(int idMaster, int idmedida)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_GuardarMedidasBultoEmb";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idMaster;

                comm.Parameters.Add("@idMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateMedidaEmb(int idpuntomaster, int unidadesres)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidasEmb";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idpuntomaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntomaster;

                comm.Parameters.Add("@unidadesrestantes", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesres;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateMedidaBultoEmb(int idmedidamaster, int idmedida)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidaBultoEmb";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMedidamaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedidamaster;

                comm.Parameters.Add("@idmedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }



        #endregion

        #region procedimientos registros de embarque
        public static string SaveEmbarque(int idporder, string idinspector, string planM, int unidadesAudi, int numeroDef, float defectuoso, bool approved, DateTime date, string responsable)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarShipping";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idOrder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idporder;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.Parameters.Add("@planM", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = planM;

                comm.Parameters.Add("@unidadesAudi", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesAudi;

                comm.Parameters.Add("@numeroMDefec", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = numeroDef;

                comm.Parameters.Add("@defectuoso", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = defectuoso;

                comm.Parameters.Add("@approved", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = approved;

                comm.Parameters.Add("@date1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = date;

                comm.Parameters.Add("@auditor", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = responsable;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string updateEmbarque(int idMaster, int idmedida)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_GuardarMedidasBultoEmb";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idMaster;

                comm.Parameters.Add("@idMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveDetalleEMbarque(int idshippingAudi, string iddefXArea, int cantidad, int defMayor, int defMEnor, int total)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardardetalleShipping";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idshippingAudi", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idshippingAudi;

                comm.Parameters.Add("@iddefXArea ", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = iddefXArea;

                comm.Parameters.Add("@cantidad", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@defMayor", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = defMayor;

                comm.Parameters.Add("@defMEnor", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = defMEnor;

                comm.Parameters.Add("@total", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = total;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateDetalleEMbarque(int idmedidamaster, int idmedida)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidaBultoEmb";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMedidamaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedidamaster;

                comm.Parameters.Add("@idmedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }



        #endregion

        #region procedimientos registro especial1
        public static string SaveMedidaEspecial1(int idpuntomedida, int idbulto, int unidadesT, int unidadesR, string idinspector)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {


                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarMedidasEspecial1";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idpuntomedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntomedida;

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@unidadesTotal", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesT;

                comm.Parameters.Add("@unidadesrestantes", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesR;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveMedidaBultoEspecial1(int idMaster, int idmedida, string insp)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_GuardarMedidasBultoEspecial1";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idMaster;

                comm.Parameters.Add("@idMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.Parameters.Add("@insp", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = insp;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateMedidaEspecial1(int idpuntomaster, int unidadesres)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {


                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidasEspecial1";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idpuntomaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntomaster;

                comm.Parameters.Add("@unidadesrestantes", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesres;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateMedidaBultoEspecial1(int idmedidamaster, int idmedida, string insp)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidaBultoEspecial1";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMedidamaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedidamaster;

                comm.Parameters.Add("@idmedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.Parameters.Add("@insp", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = insp;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region procedimientos registro especial2
        public static string SaveMedidaEspecial2(int idpuntomedida, int idbulto, int unidadesT, int unidadesR, string idinspector)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {


                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarMedidasEspecial2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idpuntomedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntomedida;

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@unidadesTotal", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesT;

                comm.Parameters.Add("@unidadesrestantes", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesR;

                comm.Parameters.Add("@idinspector", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idinspector;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string SaveMedidaBultoEspecial2(int idMaster, int idmedida, string insp)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_GuardarMedidasBultoEspecial2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idMaster;

                comm.Parameters.Add("@idMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.Parameters.Add("@insp", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = insp;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateMedidaEspecial2(int idpuntomaster, int unidadesres)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {


                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidasEspecial2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idpuntomaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntomaster;

                comm.Parameters.Add("@unidadesrestantes", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesres;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateMedidaBultoEspecial2(int idmedidamaster, int idmedida, string insp)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_updateMedidaBultoEspecial2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idMedidamaster", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedidamaster;

                comm.Parameters.Add("@idmedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idmedida;

                comm.Parameters.Add("@insp", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = insp;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Reporte Multinivel Por Corte
        public static DataTable ReportqueryMultiXCorte(DateTime fecha1, DateTime fecha2, string rol, int idcorte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "reportMultinivelXCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@rol", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = rol;

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportqueryMultiIntexXcorte(DateTime fecha1, DateTime fecha2, int idcorte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "reportMultinivelIntexXCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportqueryMultiMod4XCorte(DateTime fecha1, DateTime fecha2, int idcorte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "reportMultinivelMod4XCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;



                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportqueryMultiEspecialIXCorte(DateTime fecha1, DateTime fecha2, int idcorte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "reportMultinivelEspecialIXCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;



                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        public static DataTable ReportqueryMulti(DateTime fecha1, DateTime fecha2, string rol)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "reportMultinivel";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@rol", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = rol;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportqueryMultiIntex(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "reportMultinivelIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;


                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportqueryMultiMod4(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "reportMultinivelMod4";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;


                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string asignacionlineaUser(int idporder, int idlinea, string users)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "asignacionlineaUser";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@userMs", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = users;

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idporder;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        #region Correccion de envios
        /// <summary>
        /// regresa la informacon de envio de un corte para aplicar correcciones
        /// </summary>
        /// <param name="corte"></param>
        /// <returns></returns>
        public static DataTable ListarCorteaCorreccion(int corte, DateTime fecha, int envio, DateTime fechaconteo)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdListarCorteaCorreccion";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@fecha", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@envio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envio;

                comm.Parameters.Add("@fechaConteo", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fechaconteo;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                string m = ex.Message;
                return null;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string CorreccionFechaEnvio(int idhistorico, int idenvio, string fechaenvio, int envioCorte, int envioDia, string fechaconteo, int contado, int corte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Update_Fecha_Envio";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idhistoricoP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idhistorico;

                comm.Parameters.Add("@idenvioP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idenvio;

                comm.Parameters.Add("@numEnvioCorteP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envioCorte;

                comm.Parameters.Add("@enviodiaP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envioDia;

                comm.Parameters.Add("@fechaP", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = fechaenvio;

                comm.Parameters.Add("@fechaconteoP", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = fechaconteo;

                comm.Parameters.Add("@contadoP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = contado;

                comm.Parameters.Add("@corteP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@parSalida", SqlDbType.NVarChar, 150);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var resp = comm.Parameters["@parSalida"].Value.ToString();

                return resp;

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string AnularEnvio(int idhistorico, int idenvio, int corte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdAnularEnvios";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idhistorico", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idhistorico;

                comm.Parameters.Add("@idenvio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idenvio;

                comm.Parameters.Add("@corteP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string EliminarConteoEnvio(int idhistorico, int idenvio, int corte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdEliminarConteoEnvio";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idhistorico", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idhistorico;

                comm.Parameters.Add("@idenvio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idenvio;

                comm.Parameters.Add("@corteP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Correccion de envios intex

        /// <summary>
        /// regresa la informacon de envio de un corte para aplicar correcciones
        /// </summary>
        /// <param name="corte"></param>
        /// <returns></returns>
        public static DataTable ListarCorteaCorreccionIntex(int corte, DateTime fecha, int envio, DateTime fechaconteo)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdListarCorteaCorreccionIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@fecha", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@envio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envio;

                comm.Parameters.Add("@fechaConteo", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fechaconteo;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                string m = ex.Message;
                return null;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string CorreccionFechaEnvioIntex(int idhistorico, int idenvio, string fechaenvio, int envioCorte, int envioDia, string fechaconteo, int contado, int corte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Update_Fecha_EnvioIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idhistoricoP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idhistorico;

                comm.Parameters.Add("@idenvioP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idenvio;

                comm.Parameters.Add("@numEnvioCorteP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envioCorte;

                comm.Parameters.Add("@enviodiaP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envioDia;

                comm.Parameters.Add("@fechaP", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = fechaenvio;

                comm.Parameters.Add("@fechaconteoP", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = fechaconteo;

                comm.Parameters.Add("@contadoP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = contado;

                comm.Parameters.Add("@corteP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@parSalida", SqlDbType.NVarChar, 150);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var resp = comm.Parameters["@parSalida"].Value.ToString();

                return resp;

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string AnularEnvioIntex(int idhistorico, int idenvio, int corte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdAnularEnviosIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idhistorico", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idhistorico;

                comm.Parameters.Add("@idenvio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idenvio;

                comm.Parameters.Add("@corteP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string EliminarConteoEnvioIntex(int idhistorico, int idenvio, int corte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdEliminarConteoEnvioIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idhistorico", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idhistorico;

                comm.Parameters.Add("@idenvio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idenvio;

                comm.Parameters.Add("@corteP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Metodos save envios y bihorarios en lineas
        /// <summary>
        /// metodo para envio de bultos seccionados: envia una parte de las unidades contadas del bulto 
        /// </summary>
        /// <param name="idenvio"></param>
        /// <param name="enviocorte"></param>
        /// <param name="enviodia"></param>
        /// <param name="user"></param>
        /// <param name="justificacion"></param>
        /// <param name="unidadesenvidas"></param>
        /// <returns></returns>
        public static string guardarenvioBultosSeccionados(int idenvio, int enviocorte, int enviodia, string user, string justificacion, int unidadesenvidas)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarenvioBultosSeccionados";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idenvio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idenvio;

                comm.Parameters.Add("@enviocorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviocorte;

                comm.Parameters.Add("@envioDia", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviodia;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = user;

                comm.Parameters.Add("@justificacion", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = justificacion;

                comm.Parameters.Add("@unidadesenviadas", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesenvidas;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string guardarenvioBultosSeccionadosIntex(int idenvio, int enviocorte, int enviodia, string user, string justificacion, int unidadesenvidas)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarenvioBultosSeccionadosIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idenvio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idenvio;

                comm.Parameters.Add("@enviocorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviocorte;

                comm.Parameters.Add("@envioDia", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviodia;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = user;

                comm.Parameters.Add("@justificacion", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = justificacion;

                comm.Parameters.Add("@unidadesenviadas", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesenvidas;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }

        }



        /// <summary> ************************
        /// El metodo guarda la produccion del dia dividido en bihorarios 
        /// </summary>
        /// <param name="idbulto"></param>
        /// <param name="quantity"></param>
        /// <param name="acumulado"></param>
        /// <param name="size"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string saveEnviosbultos(int idbulto, int quantity, int acumulado, string size, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_saveEnviosbultos";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@quantity", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = quantity;

                comm.Parameters.Add("@acumulado", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = acumulado;

                comm.Parameters.Add("@size", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = size;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                SqlParameter parametroSalida = new SqlParameter("@idenvio", -1);
                parametroSalida.Direction = ParameterDirection.Output;
                comm.Parameters.Add(parametroSalida);

                comm.ExecuteNonQuery();

                int resp = Int32.Parse(comm.Parameters["@idenvio"].Value.ToString());

                return resp.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string saveEnviosbultosProd(int idbulto, int quantity, int acumulado, string size, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_saveEnviosbultosProd";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@quantity", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = quantity;

                comm.Parameters.Add("@acumulado", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = acumulado;

                comm.Parameters.Add("@size", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = size;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                SqlParameter parametroSalida = new SqlParameter("@idenvio", -1);
                parametroSalida.Direction = ParameterDirection.Output;
                comm.Parameters.Add(parametroSalida);

                comm.ExecuteNonQuery();

                int resp = Int32.Parse(comm.Parameters["@idenvio"].Value.ToString());

                return resp.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string guardarirregulares(int idorder, int cantidad, string comentario, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarUnidadesIrregulares";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idCorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idorder;

                comm.Parameters.Add("@cantidadIrreg", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@comentario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = comentario;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                SqlParameter parametroSalida = new SqlParameter("@resp", -1);
                parametroSalida.Direction = ParameterDirection.Output;
                comm.Parameters.Add(parametroSalida);

                comm.ExecuteNonQuery();

                int resp = int.Parse(comm.Parameters["@resp"].Value.ToString());

                return resp.ToString();

                //return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary> ********************************
        /// el metodo guardar el envio del
        /// producto a otro modulo (intex ò mod2)
        /// </summary>
        /// <param name="idorder"></param>
        /// <param name="idbihorario"></param>
        /// <param name="envioCorte"></param>
        /// <param name="enviodia"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string enviarguardarBihorario(int idorder, int envioCorte, int enviodia, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_enviarguardarBihorario";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idorder;

                comm.Parameters.Add("@enviocorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envioCorte;

                comm.Parameters.Add("@envioDia", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviodia;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string spdbloqueo()
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_bloqueo";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string enviarguardarComen(int idorder, int unidades, string comentario, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_enviarguardarComentario";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idorder;

                comm.Parameters.Add("@unidades", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidades;

                comm.Parameters.Add("@comentario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = comentario;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string enviarguardarComenIndi(int idorder, int unidades, string comentario, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_enviarguardarComentarioIndi";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idorder;

                comm.Parameters.Add("@unidades", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidades;

                comm.Parameters.Add("@comentario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = comentario;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary> ********************************
        /// el metodo guardar el envio del
        /// producto a otro modulo (intex ò mod2)
        /// </summary>
        /// <param name="idbulto"></param>
        /// <param name="idbihorario"></param>
        /// <param name="envioCorte"></param>
        /// <param name="enviodia"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string enviarguardarBihorarioXBulto(int idbulto, int envioCorte, int enviodia, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_enviarguardarBihorarioXBulto";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@enviocorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envioCorte;

                comm.Parameters.Add("@envioDia", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviodia;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string enviarguardarBihorarioXBultoIntex(int idbulto, int envioCorte, int enviodia, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_enviarguardarBihorarioXBultoIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@enviocorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envioCorte;

                comm.Parameters.Add("@envioDia", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviodia;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>******************************
        /// Envios Exepcion es cuando se envian bultos incompletos 
        /// </summary>
        /// <param name="idbulto"></param>
        /// <param name="enviocorte"></param>
        /// <param name="enviodia"></param>
        /// <param name="mess"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string enviarguardarBihorarioexeption(int idbulto, int enviocorte, int enviodia, string mess, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_enviarguardarBihorarioexeption";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@enviocorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviocorte;

                comm.Parameters.Add("@envioDia", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviodia;

                comm.Parameters.Add("@mess", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = mess;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                comm.ExecuteNonQuery();


                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// No esta en uso
        /// </summary>
        /// <param name="idorder"></param>
        /// <param name="idbihorario"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string guardarBihorarioPOinco(int idorder, int idbihorario, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarBihorarioPOinco";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idorder;

                comm.Parameters.Add("@idbihorario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbihorario;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// registro de exceso de talla o de corte en general 
        /// </summary>
        /// <param name="idCorte"></param>
        /// <param name="talla"></param>
        /// <param name="quantity"></param>
        /// <param name="usuario"></param>
        /// <param name="envioDia"></param>
        /// <param name="comentario"></param>
        /// <param name="idControlEnvio"></param>
        /// <returns> </returns>
        public static string GuardarExcesoTallaCorte(int idCorte, string talla, int quantity, string usuario, int envioDia, string comentario, string idControlEnvio)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarExcesoTallaCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idCorte;

                comm.Parameters.Add("@talla", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = talla;

                comm.Parameters.Add("@cantidad", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = quantity;

                comm.Parameters.Add("@usuarioIngreso", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@enviodia", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envioDia;

                comm.Parameters.Add("@comentario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = comentario;

                comm.Parameters.Add("@usuarioEnvio", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@idcontrolevento", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idControlEnvio;

                comm.Parameters.Add("@salida", SqlDbType.NChar, 10);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                string resp = comm.Parameters["@salida"].Value.ToString().Trim();

                return resp.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idExceso"></param>
        /// <param name="quantity"></param>
        /// <param name="usuario"></param>
        /// <param name="envioDia"></param>
        /// <param name="idControlEnvio"></param>
        /// <returns></returns>
        public static string ActualizarExcesoTallaCorte(int idExceso, int quantity, string usuario, int envioDia, string idControlEnvio)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdActualizarExcesoTallaCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idexceso", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idExceso;

                comm.Parameters.Add("@cantidad", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = quantity;

                comm.Parameters.Add("@enviodia", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envioDia;

                comm.Parameters.Add("@usuarioEnvio", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@idcontrolevento", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idControlEnvio;

                comm.Parameters.Add("@salida", SqlDbType.NChar, 10);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                string resp = comm.Parameters["@salida"].Value.ToString().Trim();

                return resp;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }

        }

        #endregion

        #region Metodos save envios y bihorarios en Intex
        public static string saveEnviosbultosIntex(int idbulto, int quantity, int acumulado, string size, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_saveEnviosbultosIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                comm.Parameters.Add("@quantity", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = quantity;

                comm.Parameters.Add("@acumulado", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = acumulado;

                comm.Parameters.Add("@size", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = size;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                SqlParameter parametroSalida = new SqlParameter("@idenvio", -1);
                parametroSalida.Direction = ParameterDirection.Output;
                comm.Parameters.Add(parametroSalida);

                comm.ExecuteNonQuery();

                int resp = Int32.Parse(comm.Parameters["@idenvio"].Value.ToString());

                return resp.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string savehistoricoEnvioIntex(int idenvio, int quantityenvio, DateTime fecha, string users)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_savehistoricoEnvioIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idenvio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idenvio;

                comm.Parameters.Add("@quantityenvio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = quantityenvio;

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@username", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = users;

                SqlParameter idhistorico = new SqlParameter("@idhistorico", -1);
                idhistorico.Direction = ParameterDirection.Output;
                comm.Parameters.Add(idhistorico);

                comm.ExecuteNonQuery();
                int resp = Int32.Parse(comm.Parameters["@idhistorico"].Value.ToString());

                return resp.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string enviarguardarBihorarioIntex(int idorder, int envioCorte, int enviodia, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_enviarguardarBihorarioIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idorder;

                comm.Parameters.Add("@enviocorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envioCorte;

                comm.Parameters.Add("@envioDia", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviodia;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string guardarBihorarioIntex(int idorder, int idbihorario, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarBihorarioIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idorder;

                comm.Parameters.Add("@idbihorario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbihorario;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string enviarguardarBihorarioexeptionIntex(int idbulto, int enviocorte, int enviodia, string mess, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_enviarguardarBihorarioexeptionIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idbulto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbulto;

                //comm.Parameters.Add("@idbihorario", SqlDbType.Int);
                //comm.Parameters[comm.Parameters.Count - 1].Value = idbihorario;

                comm.Parameters.Add("@enviocorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviocorte;

                comm.Parameters.Add("@envioDia", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviodia;

                comm.Parameters.Add("@mess", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = mess;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string guardarBihorarioPOincoIntex(int idorder, int idbihorario, string name)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarBihorarioPOincoIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idorder;

                comm.Parameters.Add("@idbihorario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idbihorario;

                comm.Parameters.Add("@username", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = name;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdatePorderAfterIntex(int idorder, int planta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_UpdatePorderAfterIntexSent";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idorder;

                comm.Parameters.Add("@plantasent", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = planta;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region Reportes de Envio

        public static DataTable GetExcesoTallaCorteXidCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGetExcesoTallaCorteXidCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;


                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ListarBultosSeccionadosXcorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_listarbultosXcorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;


                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ListarBultosSeccionadosXcorteIntex(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_listarbultosXcorteIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;


                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable PackingList(DateTime fecha, int idPlanta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_packingList";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPlanta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable PackingListgnral(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_packingListGeneral";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable PackingListgnralLa_NoLa(DateTime fecha, int was, int planta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_PLGeneralLa-NoLa";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@wa", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = was;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = planta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable PackingListgnralLa_NoLa2(DateTime fecha, int was)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_PLGeneralLa-NoLa2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@wa", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = was;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable PackingListgnralLa_NoLa3(DateTime fecha, int planta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_PLGeneralLa-NoLa3";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = planta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable PackingListXEnvio(DateTime fecha, int idPlanta, int enviodia)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_PackingListXenvioDelDia";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPlanta;

                comm.Parameters.Add("@enviodia", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = enviodia;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Bihorario(DateTime fecha, int planta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Bihorario";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@planta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = planta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable BihorarioCliente(DateTime fecha, DateTime fecha2, int cliente)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_BihorarioPorCliente";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@cliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cliente;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable BihorarioEntreFechas(DateTime fecha, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_BihorarioPorFecha";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable BihorarioProd(DateTime fecha, int planta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_BihorarioProd";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@planta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = planta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Bihorario_allplants(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Bihorario_allPlants";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                //comm.Parameters.Add("@planta", SqlDbType.Int);
                //comm.Parameters[comm.Parameters.Count - 1].Value = planta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 1800;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Bihorario_allplantsProd(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Bihorario_allPlantsProd";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 1800;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }


        public static DataTable BihorarioDeficit(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_deficit";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable BihorarioIntex(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_BihorarioIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable BihorarioDeficitIntex(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_deficitIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable PackingListIntex(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_packingListIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable PackingListIntexXenvio(DateTime fecha, int envio)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_packingListIntexXenvio";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@envio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envio;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable getreportBioEnvio(int semana, int planta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_reportbihorarioEnvio";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@semana", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = semana;

                comm.Parameters.Add("@planta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = planta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable getreportBioEnvioIntex(int semana)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_reportbihorarioEnvioIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@semana", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = semana;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable sent_not_sent(int idPlanta, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_Sent_NotSent";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                //comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPlanta;

                comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable sent_not_sent_close_Cut(int idPlanta, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_Sent_NotSent_CloseCut";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                //comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPlanta;

                comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable sent_not_sent_Open_Cut(int idPlanta, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_Sent_NotSent_openCut";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                //comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPlanta;

                comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable sent_not_sent2(int idPlanta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_Sent_NotSent2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                //comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPlanta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable sent_not_sent_close_Cut2(int idPlanta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_Sent_NotSent_CloseCut2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                //comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPlanta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable sent_not_sent_Open_Cut2(int idPlanta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_Sent_NotSent_openCut2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                //comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPlanta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable CorteAbiertoIntex(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_CorteAbiertoIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable CorteCerradoIntex(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_CorteCerradoIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable GeneralIntex(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_GeneralIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable CorteAbiertoIntex2()
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_CorteAbiertoIntex2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable InventarioIntex()
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_Inventario";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable CorteCerradoIntex2()
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_CorteCerradoIntex2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable GeneralIntex2()
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Report_GeneralIntex2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Contado_Envi_NoEnvi(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Reporte_Contadas_Enviadas_NoEnviadas";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                //comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Contado_Envi_NoEnvi_plan(int idplanta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Reporte_Contadas_Enviadas_NoEnviadas_Planta";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idplanta;

                //comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                //comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Contado_Envi_NoEnvi_Linea(int idlinea)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Reporte_Contadas_Enviadas_NoEnviadas_Linea";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                //comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                //comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Contado_Envi_NoEnvi_cliente(int cliente)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Reporte_Contadas_Enviadas_NoEnviadas_Cliente";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idCliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cliente;

                //comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                //comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                //comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Contado_Envi_NoEnvi_Corte(int idcorte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_Reporte_Contadas_Enviadas_NoEnviadas_Corte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Reporte_Invplanta(int idplanta, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_ReporteInvPlanta";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idplanta;

                comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Reporte_Invlinea(int idlinea, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_ReporteInvLinea";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idlinea;

                comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Reporte_InvCliente(int idcliente, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_ReporteInvCliente";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcliente;

                comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Reporte_InvFecha(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_ReporteInvFecha";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable PackingListnowash(DateTime fecha, int idPlanta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_packingListNoWashed";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPlanta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable PackingListnowashenvio(DateTime fecha, int idPlanta, int envio)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_packingListNoWashedenvio";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPlanta;

                comm.Parameters.Add("@envio", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envio;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable getreportWeekSNS(int semana, int planta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_ReportWeekSNS";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@week", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = semana;

                //comm.Parameters.Add("@planta", SqlDbType.Int);
                //comm.Parameters[comm.Parameters.Count - 1].Value = planta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportProcesoDeCorte(int idplanta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReporteHistoricoL";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idplanta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportProcesoDeCorteIntex(int idplanta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReporteHistoricoLIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idplanta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportProcesoDeCorte(string idcortes)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReporteHistoricoXListadoDeCortes";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcortes", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcortes;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportProcesoDeCorteIntex(string idcortes)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReporteHistoricoXListadoDeCortesIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcortes", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcortes;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportHistoricoConteoXidCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRep_HistoricoDeConteoxIdCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportHistoricoConteoXidCorteIntex(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRep_HistoricoDeConteoxIdCorteIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportHistoricoEnviadoXidCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRep_HistoricoDeEnvioxIdCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportHistoricoEnviadoXidCorteIntex(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRep_HistoricoDeEnvioxIdCorteIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportHistoricoSeccionadoxIdCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRep_HistoricoSeccionadoxIdCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }

            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportHistoricoExcesoXidCorte(int id)/*Reporte Nuevo*/
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRep_HistoricoExcesoxIdCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }

            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportHistoricoIrregularesXcorte(int id)/*Reporte Nuevo*/
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRep_HistoricoIrregularesXcorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }

            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportHistoricoTallaxIdCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_HistoricoXTalla";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }

            finally
            {
                conn.Close();
            }
        }

        #region
        /*Devolucion*/
        public static DataTable ReportDevolucionDeCorte(string idcortes)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReporteDevolucionXListadoDeCortes";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcortes", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcortes;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable ReportHistDevReparXidCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRep_HistDevReparacionXidCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportHistDevReproXidCorte(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRep_HistDevReprocesoXidCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportHistDevReparXidCorteDetalle(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRep_HistDevReparacionXidCorteDetalle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportHistDevReproXidCorteDetalle(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRep_HistDevReprocesoXidCorteDetalle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion


        public static int GuardarContenedor(string Contenedor, string usuario)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarContenedor";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@contenedor", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Contenedor;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@idcontenedor", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = Convert.ToInt32(comm.Parameters["@idcontenedor"].Value);

                return id;
            }
            catch (Exception ex)
            {
                string m = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string GuardarNameCont(int idcontenedor, string contenedor)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_CambiarNameContenedor";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcontenedor", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcontenedor;

                comm.Parameters.Add("@contenedor", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = contenedor;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int CerrarContenedor(int idcontenedor)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_CerrarContenedor";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcontenedor", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcontenedor;

                SqlParameter parmOUT = new SqlParameter("@retorno", SqlDbType.Int);
                parmOUT.Direction = ParameterDirection.Output;
                comm.Parameters.Add(parmOUT);

                comm.ExecuteNonQuery();

                int id = (int)comm.Parameters["@retorno"].Value;

                return id;

            }
            catch (Exception ex)
            {
                string a = ex.Message;
                return -1;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string GuardarDetalleCont(int idcontenedor, int idorder)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_guardarDetalleContenedor";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcontenedor", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcontenedor;

                comm.Parameters.Add("@idorder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idorder;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string EliminarCorteContenedor(int idcontenedor, int idcorte, string user)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_EliminarCorteContenedor";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcontenedor", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcontenedor;

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                comm.Parameters.Add("@user", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = user;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Reporte_ContenedorEmbarque(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_ReporteContenedeorEmbarque";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcontenedor", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Reporte_ContenedorEmbarqueCortesCompletos(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_ReporteContenedeorEmbarqueCortesCompletos";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcontenedor", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }



        //public static DataTable ReportProcesoDeCorte(int i)
        //{
        //    SqlConnection conn = new SqlConnection(Get_ConnectionString());
        //    SqlCommand comm = new SqlCommand();
        //    DataTable a = new DataTable();
        //    try
        //    {
        //        conn.Open();
        //        comm.Connection = conn;
        //        comm.CommandText = "spd_PLGeneralLa-NoLa";
        //        comm.CommandType = CommandType.StoredProcedure;
        //        comm.Parameters.Clear();

        //        //comm.Parameters.Add("@fecha", SqlDbType.DateTime);
        //        //comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

        //        //comm.Parameters.Add("@wa", SqlDbType.Int);
        //        //comm.Parameters[comm.Parameters.Count - 1].Value = was;

        //        SqlDataAdapter da = new SqlDataAdapter(comm);
        //        da.Fill(a);

        //        return a;

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.Write(ex.Message);
        //        return null;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}
        #endregion

        #region medidasNew

        public static string saveObjMaster(int idestacion, int idestilotalla, int idporder, bool estado)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                // string idcpompuesto = string.Concat(idestacion.ToString(), idestilotalla.ToString(), idporder.ToString(), DateTime.Now.ToString().Replace('/',' ').TrimEnd());
                string idcpompuesto = string.Concat(idestacion.ToString(), idestilotalla.ToString(), idporder.ToString(), DateTime.Now.ToShortDateString().Replace('/', '.'), DateTime.Now.ToLongTimeString());

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarObjMaster";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idestacion;

                comm.Parameters.Add("@idestilotalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idestilotalla;

                comm.Parameters.Add("@idporder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idporder;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@id", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcpompuesto;


                //comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                //comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                //var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return idcpompuesto;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "";
            }
            finally
            {
                conn.Close();
            }
        }

        public static string saveObjDetail(int idespecificacion, float valor, float valorspeck, string estado, string id)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarObjDetail";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idespecificacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idespecificacion;

                comm.Parameters.Add("@valor", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valor;

                comm.Parameters.Add("@valorspeck", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valorspeck;

                comm.Parameters.Add("@estado", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@id", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;


                //comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                //comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                //var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return "true";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "";
            }
            finally
            {
                conn.Close();
            }
        }

        public static string saveEspecificacion(int idestilotalla, int idpuntoMedida, double valor, double valorMax, double valorMin, string user)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveEspecificacion";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestilotalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idestilotalla;

                comm.Parameters.Add("@idpuntoMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntoMedida;

                comm.Parameters.Add("@valor", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valor;

                comm.Parameters.Add("@valorMax", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valorMax;

                comm.Parameters.Add("@valormin", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valorMin;

                comm.Parameters.Add("@usuario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = user;

                //comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                //comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                //var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "ERROR";
            }
            finally
            {
                conn.Close();
            }
        }

        public static string updateEspecificacion(int idespecificacion, double valor, double valorMax, double valorMin, string user)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdUpdateEspecificacion";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idespecificacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idespecificacion;

                comm.Parameters.Add("@valor", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valor;

                comm.Parameters.Add("@valorMax", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valorMax;

                comm.Parameters.Add("@valormin", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valorMin;

                comm.Parameters.Add("@usuario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = user;

                //comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                //comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                //var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "ERROR";
            }
            finally
            {
                conn.Close();
            }
        }

        public static string saveAsignacionTallas(int idestilo, int idtalla)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveEstiloTalla";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idestilo;

                comm.Parameters.Add("@idtalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idtalla;


                comm.ExecuteNonQuery();

                //var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "ERROR";
            }
            finally
            {
                conn.Close();
            }
        }

        public static string saveAsignacionPuntoM(int idestilo, int idtalla, int idpuntom)//Agregar Parte Spec
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdAgregarPuntoMSpec";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idestilo;

                comm.Parameters.Add("@idtalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idtalla;

                comm.Parameters.Add("@idPuntom", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntom;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "ERROR";
            }
            finally
            {
                conn.Close();
            }
        }

        public static string IngresarSpec(int idespecificacion, float valor, float tmax, float tmin)//Agregar Parte Spec
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdAgregarEspecificacion";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idespecificacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idespecificacion;

                comm.Parameters.Add("@valor", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valor;

                comm.Parameters.Add("@tmax", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = tmax;

                comm.Parameters.Add("@tmin", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = tmin;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "ERROR";
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepDashboard1(int id, int semana, int idcliente)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepDashboard1";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@semana", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = semana;

                comm.Parameters.Add("@cliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcliente;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepDashboard2(int id, int idcliente)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepDashboard2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@cliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcliente;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepDashboard3(int id, int idcliente)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepDashboard3";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@cliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcliente;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepDashboard4(int id, int semana, int idcliente)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepDashboard4";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@semana", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = semana;

                comm.Parameters.Add("@cliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcliente;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int SaveTallaParametroSalida(string talla)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveTallaParametroSalida";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@talla", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = talla;

                comm.Parameters.Add("@idtalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();
                conn.Close();

                var idtalla = Convert.ToInt32(comm.Parameters["@idtalla"].Value.ToString());

                return idtalla;

            }
            catch (Exception ex)
            {
                string m = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }

        }

        public static int SavePuntoMedidaParametroSalida(string puntomedida)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSavePuntoMedidaParametroSalida";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@puntomedida", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = puntomedida;

                comm.Parameters.Add("@idpuntoM", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();
                conn.Close();

                var idpuntoM = Convert.ToInt32(comm.Parameters["@idpuntoM"].Value.ToString());

                return idpuntoM;

            }
            catch (Exception ex)
            {
                string m = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string saveEspecificacionNew(int idestilo, int idtalla, int idpuntoMedida, double valor, double valorMax, double valorMin, string user, int rango)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveEspecificacionNew";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idestilo;

                comm.Parameters.Add("@idtalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idtalla;

                comm.Parameters.Add("@idpuntoMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idpuntoMedida;

                comm.Parameters.Add("@valor", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valor;

                comm.Parameters.Add("@valorMax", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valorMax;

                comm.Parameters.Add("@valormin", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valorMin;

                comm.Parameters.Add("@usuario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = user;

                comm.Parameters.Add("@rango", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = rango;

                comm.ExecuteNonQuery();


                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "ERROR";
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region medidasMuestra

        public static string saveObjMasterMuestra(int idestacion, int idestilotalla, int idporder, bool estado)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                // string idcpompuesto = string.Concat(idestacion.ToString(), idestilotalla.ToString(), idporder.ToString(), DateTime.Now.ToString().Replace('/',' ').TrimEnd());
                string idcpompuesto = string.Concat(idestacion.ToString(), idestilotalla.ToString(), idporder.ToString(), DateTime.Now.ToShortDateString().Replace('/', '.'), DateTime.Now.ToLongTimeString());

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarObjMasterMuestra";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idestacion;

                comm.Parameters.Add("@idestilotalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idestilotalla;

                comm.Parameters.Add("@idporder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idporder;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@id", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcpompuesto;

                //comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                //comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                //var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return idcpompuesto;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "";
            }
            finally
            {
                conn.Close();
            }
        }

        public static string saveObjDetailMuestra(int idespecificacion, float valor, float valorspeck, string estado, string id)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarObjDetailMuestra";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idespecificacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idespecificacion;

                comm.Parameters.Add("@valor", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valor;

                comm.Parameters.Add("@valorspeck", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = valorspeck;

                comm.Parameters.Add("@estado", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@id", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;


                //comm.Parameters.Add("@idbbrechazo", SqlDbType.Int);
                //comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                //var id = Convert.ToInt32(comm.Parameters["@idbbrechazo"].Value);

                return "true";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "";
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepDashboard1Muestra(int id, int semana, int cliente)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepDashboard1Muestra";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@semana", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = semana;

                comm.Parameters.Add("@cliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cliente;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepDashboard2Muestra(int id, int cliente)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepDashboard2Muestra";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@cliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cliente;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepDashboard5Muestra(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepDashboard5Muestra";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                //comm.Parameters.Add("@cliente", SqlDbType.Int);
                //comm.Parameters[comm.Parameters.Count - 1].Value = cliente;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepDashboard5StyleMuestra(int id, string style, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepDashboard5StyleMuestra";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@style", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = style;

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepDashboard5ClienteMuestra(int id, int idcliente, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepDashboard5ClienteMuestra";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@idcliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcliente;

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepDashboard5StyleClienteMuestra(int id, int idcliente, string style, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepDashboard5StyleClienteMuestra";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@idcliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcliente;

                comm.Parameters.Add("@style", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = style;

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepDashboard3Muestra(int id, int cliente)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepDashboard3Muestra";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@cliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cliente;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepDashboard4Muestra(int id, int semana, int cliente)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepDashboard4Muestra";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idestacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@semana", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = semana;

                comm.Parameters.Add("@cliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cliente;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }



        #endregion

        #region Defectos
        public static DataTable defectoOper(int Corte, string estilo, int linea, int planta, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdDefectosOperario";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                if (Corte > 0)
                {
                    comm.Parameters.Add("@corte", SqlDbType.Int);
                    comm.Parameters[comm.Parameters.Count - 1].Value = Corte;
                }
                else
                {
                    int corte1 = 0;
                    comm.Parameters.Add("@corte", SqlDbType.Int);
                    comm.Parameters[comm.Parameters.Count - 1].Value = corte1;
                }

                if (estilo == "")
                {
                    string estilo1 = "vacio";
                    comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                    comm.Parameters[comm.Parameters.Count - 1].Value = estilo1;
                }
                else
                {
                    comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                    comm.Parameters[comm.Parameters.Count - 1].Value = estilo;
                }

                if (linea > 0)
                {
                    comm.Parameters.Add("@linea", SqlDbType.Int);
                    comm.Parameters[comm.Parameters.Count - 1].Value = linea;
                }
                else
                {
                    int linea1 = 0;
                    comm.Parameters.Add("@linea", SqlDbType.Int);
                    comm.Parameters[comm.Parameters.Count - 1].Value = linea1;
                }

                if (planta > 0)
                {
                    comm.Parameters.Add("@planta", SqlDbType.Int);
                    comm.Parameters[comm.Parameters.Count - 1].Value = planta;
                }
                else
                {
                    int planta1 = 0;
                    comm.Parameters.Add("@planta", SqlDbType.Int);
                    comm.Parameters[comm.Parameters.Count - 1].Value = planta1;
                }

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable defectoOperrec(int Corte, string estilo, int linea, int planta, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdDefOperario_Re";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                if (Corte > 0)
                {
                    comm.Parameters.Add("@corte", SqlDbType.Int);
                    comm.Parameters[comm.Parameters.Count - 1].Value = Corte;
                }
                else
                {
                    int corte1 = 0;
                    comm.Parameters.Add("@corte", SqlDbType.Int);
                    comm.Parameters[comm.Parameters.Count - 1].Value = corte1;
                }

                if (estilo == "")
                {
                    string estilo1 = "vacio";
                    comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                    comm.Parameters[comm.Parameters.Count - 1].Value = estilo1;
                }
                else
                {
                    comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                    comm.Parameters[comm.Parameters.Count - 1].Value = estilo;
                }

                if (linea > 0)
                {
                    comm.Parameters.Add("@linea", SqlDbType.Int);
                    comm.Parameters[comm.Parameters.Count - 1].Value = linea;
                }
                else
                {
                    int linea1 = 0;
                    comm.Parameters.Add("@linea", SqlDbType.Int);
                    comm.Parameters[comm.Parameters.Count - 1].Value = linea1;
                }

                if (planta > 0)
                {
                    comm.Parameters.Add("@planta", SqlDbType.Int);
                    comm.Parameters[comm.Parameters.Count - 1].Value = planta;
                }
                else
                {
                    int planta1 = 0;
                    comm.Parameters.Add("@planta", SqlDbType.Int);
                    comm.Parameters[comm.Parameters.Count - 1].Value = planta1;
                }

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region Registros Segundas

        public static string GuadarSegundas(string corte, string estilo, int cantidad, int idoperacion, int idseccion, string responsable)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuadarSegundas";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estilo;

                comm.Parameters.Add("@cantidad", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@idoperacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idoperacion;

                comm.Parameters.Add("@idseccion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idseccion;

                comm.Parameters.Add("@responsable", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = responsable;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string EliminarSegundas(string corte, int idoperacion, int idseccion, string responsable)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdEliminarSegundas";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@idoperacion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idoperacion;

                comm.Parameters.Add("@idseccion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idseccion;

                comm.Parameters.Add("@responsable", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = responsable;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Segundaporcorte(string id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "ReporteSegundaporCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepSegSeccionCorte(string id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "ReportSegSeccionCut";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable SegundaporcorteGene(string id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "ReporteSegundaCorteGeneral";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable Segundaporcortefecha(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "ReporteSegundaporCorteFecha";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable RepSegSeccionFecha(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "ReportSegSeccionfecha";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable SegundaCorteGeneralfecha(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "ReporteSegundaCorteGeneralfecha";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region auditoria embarque defectos

        public static int spdSaveAE(int idcorte, string codigo, int lote, bool estado, DateTime horaI, DateTime horaF, decimal aql, int muestra, int unidef, string coment, int idnivel, int idnivelaql, string usuario, string nombreaudit)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAE";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                comm.Parameters.Add("@codigoLote", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = codigo;

                comm.Parameters.Add("@lote", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = lote;

                comm.Parameters.Add("@estadoAudit", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@horaInicial", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = horaI;

                comm.Parameters.Add("@horaFinal", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = horaF;

                comm.Parameters.Add("@aql", SqlDbType.Decimal);
                comm.Parameters[comm.Parameters.Count - 1].Value = aql;

                comm.Parameters.Add("@muestra", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestra;

                comm.Parameters.Add("@unidadesDefect", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidef;

                comm.Parameters.Add("@comentario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = coment;

                comm.Parameters.Add("@idnivel", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idnivel;

                comm.Parameters.Add("@idnivelaql", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idnivelaql;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@nombreAuditori", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = nombreaudit;

                comm.Parameters.Add("@idembarque", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var r = comm.Parameters["@idembarque"].Value.ToString();
                var id = Convert.ToInt32(comm.Parameters["@idembarque"].Value.ToString());

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int spdSaveAEDefectosDet(int idnumeroAudi, int iddefectos, int idposicion, int cantidad)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAEDetail";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idNumAudi", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idnumeroAudi;

                comm.Parameters.Add("@iddefecto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = iddefectos;

                comm.Parameters.Add("@idposicion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idposicion;

                comm.Parameters.Add("@cantidad", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@idDetail", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var r = comm.Parameters["@idDetail"].Value.ToString();
                var id = Convert.ToInt32(comm.Parameters["@idDetail"].Value.ToString());

                return id;
                //return "OK";
            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string spdSaveAEImagen(int idnumAudi, string ruta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAEImagen";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idNumAudi", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idnumAudi;

                comm.Parameters.Add("@ruta", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = ruta;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteDefectocorteProceso(int idplanta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdHistoricoDefectoEmbarque";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idplanta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteDefectoHistorico(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReporteDefecto";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteDefectoImagen(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReporteDefectoImagen";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteDefectoLote(int id)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReporteDefectoLote";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        #region Guardar Defecto Empaque (Maestro, Detalle)

        public static int spdGuardarDefectoEmp(string idcorte, int piezasdef, string InspectorEmp)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarDefectoEmp";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                comm.Parameters.Add("@piezasdef", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = piezasdef;

                comm.Parameters.Add("@InspectorEmp", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = InspectorEmp;

                comm.Parameters.Add("@iddefEmp", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var r = comm.Parameters["@iddefEmp"].Value.ToString();

                var id = Convert.ToInt32(comm.Parameters["@iddefEmp"].Value.ToString());

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string spdGuardarDefectoEmpDet(int idaudit, int iddefecto, int idposicion, int cantidad)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarDefectoEmpDet";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idaudit", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idaudit;

                comm.Parameters.Add("@iddefecto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = iddefecto;

                comm.Parameters.Add("@idposicion", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idposicion;

                comm.Parameters.Add("@CantDef", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.ExecuteNonQuery();
                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #endregion

        #region Reporte Empaque 
        public static DataTable GetEmpaqueSemana(int semana)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepPackingWeek";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@semana", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = semana;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteEmpaqueDeCorte(string idcortes)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdRepPackingXListadoDeCortes";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcortes", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcortes;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteEmpaqueDeCorteDetalle(int idcorte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdpakingXcorteDetalle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteEmpaque(string corte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_ReporteEmpaque";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NChar, 20);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteEmpaqueGeneralXFecha(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_ReporteEmpaqueGeneralXFecha";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteEmpaquetallasXcorte(string corte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_ReporteEmpaquetallasXcorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NChar, 20);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;



                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteEmpaqueCajasXcorte(string corte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_ReporteEmpaqueCajasXcorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NChar, 20);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;



                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteEmpaquePacklistXcorte(string corte, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_ReporteEmpaquePacklistXcorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NChar, 20);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;


                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Medidas new Bulto a bulto

        public static string saveObjMasterBLB(int idPorder, int idEstilo, string size, bool estado, string usuario, int unidadesXtalla, string color, string rol, string xmlstring)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarObjMasterBLB";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idporder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPorder;

                comm.Parameters.Add("@idestilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idEstilo;

                comm.Parameters.Add("@size", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = size;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@unidadesXtalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesXtalla;

                comm.Parameters.Add("@color", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = color;

                comm.Parameters.Add("@rol", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = rol;

                comm.Parameters.Add("@xmlstring", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = xmlstring;

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = comm.Parameters["@id"].Value.ToString();

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "";
            }
            finally
            {
                conn.Close();
            }
        }

        public static int saveObjDetailBLB(int idPuntoMedida, float medidaFlotante, string medidaTexto, string estado, string estado2, string id)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarObjDetailBLB";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@idPuntoMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPuntoMedida;

                comm.Parameters.Add("@medidaFlotante", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = medidaFlotante;

                comm.Parameters.Add("@medidaTexto", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = medidaTexto;

                comm.Parameters.Add("@estado", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@estado2", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado2;

                comm.Parameters.Add("@unidades", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var unidades = Convert.ToInt32(comm.Parameters["@unidades"].Value);

                return unidades;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Medidas new en Final

        public static string saveObjMasterFinal(int idPorder, int idEstilo, string size, bool estado, string usuario, int unidadesXtalla, string color, string xmlstring)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarObjMasterFinal";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idporder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPorder;

                comm.Parameters.Add("@idestilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idEstilo;

                comm.Parameters.Add("@size", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = size;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@unidadesXtalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesXtalla;

                comm.Parameters.Add("@color", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = color;

                comm.Parameters.Add("@xmlstring", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = xmlstring;

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = comm.Parameters["@id"].Value.ToString();

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "";
            }
            finally
            {
                conn.Close();
            }
        }

        public static int saveObjDetailFinal(int idPuntoMedida, float medidaFlotante, string medidaTexto, string estado, string estado2, string id)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarObjDetailFinal";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@idPuntoMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPuntoMedida;

                comm.Parameters.Add("@medidaFlotante", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = medidaFlotante;

                comm.Parameters.Add("@medidaTexto", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = medidaTexto;

                comm.Parameters.Add("@estado", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@estado2", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado2;

                comm.Parameters.Add("@unidades", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var unidades = Convert.ToInt32(comm.Parameters["@unidades"].Value);

                return unidades;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Medidas new en embarque

        public static string saveObjMasterEmbarque(int idPorder, int idEstilo, string size, bool estado, string usuario, int unidadesXtalla, string color)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarObjMasterEmbarque";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idporder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPorder;

                comm.Parameters.Add("@idestilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idEstilo;

                comm.Parameters.Add("@size", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = size;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@unidadesXtalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesXtalla;

                comm.Parameters.Add("@color", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = color;

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = comm.Parameters["@id"].Value.ToString();

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "";
            }
            finally
            {
                conn.Close();
            }
        }

        public static int saveObjDetailEmbarque(int idPuntoMedida, float medidaFlotante, string medidaTexto, string estado, string estado2, string id)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarObjDetailEmbarque";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@idPuntoMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPuntoMedida;

                comm.Parameters.Add("@medidaFlotante", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = medidaFlotante;

                comm.Parameters.Add("@medidaTexto", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = medidaTexto;

                comm.Parameters.Add("@estado", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@estado2", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado2;

                comm.Parameters.Add("@unidades", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var unidades = Convert.ToInt32(comm.Parameters["@unidades"].Value);

                return unidades;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Reporte Dinamicos de medidas antes de lavado (Final, Bulto a Bulto)

        public static DataTable ReporteFinalDinamicplan(DateTime fecha1, DateTime fecha2, int planta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReportDFinalPlanta";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = planta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteFinalDinamicplanAll(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReportDFinalPlantaAll";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteFinalDinamicLine(DateTime fecha1, DateTime fecha2, int linea)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReportDFinalLinea";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = linea;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteFinalDinamicStyle(DateTime fecha1, DateTime fecha2, int estilo)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReportDFinalstyle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@idestilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = estilo;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteFinalDinamicCorte(DateTime fecha1, DateTime fecha2, int corte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReportDFinalCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteBaBDinamicplan(DateTime fecha1, DateTime fecha2, int planta)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReportDBaBPlanta";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@idplanta", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = planta;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteBaBDinamicplanAll(DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReportDBaBPlantaAll";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteBaBDinamicLine(DateTime fecha1, DateTime fecha2, int linea)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReportDBaBLinea";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@idlinea", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = linea;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteBaBDinamicStyle(DateTime fecha1, DateTime fecha2, int estilo)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReportDBaBstyle";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@idestilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = estilo;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteBaBDinamicCorte(DateTime fecha1, DateTime fecha2, int corte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdReportDBaBCorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }


        #endregion

        #region Medidas en intex

        public static string saveObjMasterIntex(int idPorder, int idEstilo, string size, bool estado, string usuario, int unidadesXtalla, string color, string xmlstring)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarObjMasterIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idporder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPorder;

                comm.Parameters.Add("@idestilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idEstilo;

                comm.Parameters.Add("@size", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = size;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@unidadesXtalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesXtalla;

                comm.Parameters.Add("@color", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = color;

                comm.Parameters.Add("@xmlstring", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = xmlstring;

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = comm.Parameters["@id"].Value.ToString();

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "";
            }
            finally
            {
                conn.Close();
            }
        }

        public static int saveObjDetailIntex(int idPuntoMedida, float medidaFlotante, string medidaTexto, string estado, string estado2, string id)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarObjDetailIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@idPuntoMedida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPuntoMedida;

                comm.Parameters.Add("@medidaFlotante", SqlDbType.Float);
                comm.Parameters[comm.Parameters.Count - 1].Value = medidaFlotante;

                comm.Parameters.Add("@medidaTexto", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = medidaTexto;

                comm.Parameters.Add("@estado", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@estado2", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado2;

                comm.Parameters.Add("@unidades", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var unidades = Convert.ToInt32(comm.Parameters["@unidades"].Value);

                return unidades;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Reporte Medidas Nuevo Formato Intex 
        public static DataTable ExtraeMedidasIntexPrincipal(string cortes)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdExtraeMedidasIntexPrincipal";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcortes", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = cortes;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ExtraeMedidasIntexXTallas(int idcorte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdExtraeMedidasIntexXTallas";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ExtraeMedidasIntexFueraDRango(int idcorte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdExtraeMedidasIntexFueraDRango";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ExtraeMedidasIntexSpec(int idcorte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdExtraeMedidasIntexSpec";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ExtraeFrecuenciasMedidasPVTDinamico(int idcorte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdExtraeFrecuenciasMedidasPVTDinamico";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }


        public static DataTable ExtraeMedidasIntexXTallasXEstilo(int idStyle, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdExtraeMedidasIntexXTallasXEstilo";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idStyle;

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ExtraeMedidasIntexFueraDRangoXEstilo(int idStyle, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdExtraeMedidasIntexFueraDRangoXEstilo";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idStyle;

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ExtraeMedidasIntexSpecXEstilo(int idStyle, DateTime fecha1, DateTime fecha2)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdExtraeMedidasIntexSpecXEstilo";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idStyle;

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }


        public static DataTable ExtraeMedidasIntexXTallasXEstilo(int idStyle, DateTime fecha1, DateTime fecha2, string color)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdExtraeMedidasIntexXTallasXEstiloColor";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idStyle;

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@color", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = color;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ExtraeMedidasIntexFueraDRangoXEstilo(int idStyle, DateTime fecha1, DateTime fecha2, string color)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdExtraeMedidasIntexFueraDRangoXEstiloColor";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idStyle;

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@color", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = color;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ExtraeMedidasIntexSpecXEstilo(int idStyle, DateTime fecha1, DateTime fecha2, string color)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdExtraeMedidasIntexSpecXEstiloColor";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idStyle;

                comm.Parameters.Add("@fecha1", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha1;

                comm.Parameters.Add("@fecha2", SqlDbType.Date);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha2;

                comm.Parameters.Add("@color", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = color;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Nuevos Proc Inventario Intex Heat Transfer

        public static string saveObjMasterIntexML(int idPorder, int idEstilo, string size, bool estado, string usuario, int unidadesXtalla, string color, string xmlstring)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdIntexGuardarObjDetailReproML";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idporder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPorder;

                comm.Parameters.Add("@idestilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idEstilo;

                comm.Parameters.Add("@size", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = size;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@unidadesXtalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesXtalla;

                comm.Parameters.Add("@color", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = color;

                comm.Parameters.Add("@xmlstring", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = xmlstring;

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = comm.Parameters["@id"].Value.ToString();

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "";
            }
            finally
            {
                conn.Close();
            }
        }

        public static string saveObjMasterIntexPl(int idPorder, int idEstilo, string size, bool estado, string usuario, int unidadesXtalla, string color, string xmlstring)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdIntexGuardarObjDetailReproMPI";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idporder", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idPorder;

                comm.Parameters.Add("@idestilo", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idEstilo;

                comm.Parameters.Add("@size", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = size;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@unidadesXtalla", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesXtalla;

                comm.Parameters.Add("@color", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = color;

                comm.Parameters.Add("@xmlstring", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = xmlstring;

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = comm.Parameters["@id"].Value.ToString();

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "";
            }
            finally
            {
                conn.Close();
            }
        }

        public static string RecibidoHT(string corte, string estilo, int cantidad, DateTime fecha, string usuario, bool estado)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarRecibidoHT";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estilo;

                comm.Parameters.Add("@cantidad", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@idHT", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = comm.Parameters["@idHT"].Value.ToString();

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "";
            }
            finally
            {
                conn.Close();
            }
        }

        public static string RecibidoDetailHT(int id, int cantidad, DateTime fecharec, string usuario, bool estadorec)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarDetailHT";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@cantidad", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@fecharec", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecharec;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@estadorec", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estadorec;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return mes;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string RecibidoDetailHT2(string corte, int cantidad, DateTime fecharec, string usuario, bool estadorec)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarDetailHT2";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@cantidad", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@fecharec", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecharec;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@estadorec", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estadorec;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return mes;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string EnvioHT(string corte, string estilo, int unidades, DateTime fechaenv, string usuarioenv, bool estadoenv)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarEnvioHT";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estilo;

                comm.Parameters.Add("@unidades", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidades;

                comm.Parameters.Add("@fechaenv", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fechaenv;

                comm.Parameters.Add("@usuarioenv", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuarioenv;

                comm.Parameters.Add("@estadoenv", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estadoenv;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return mes;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string ReprocesoPlanchaRecibido(string corte, string estilo, bool liberado, DateTime fecha, string usuario)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarReprocesoPlancha";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estilo;

                comm.Parameters.Add("@liberado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = liberado;

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@idrp", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var id = comm.Parameters["@idrp"].Value.ToString();

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return "";
            }
            finally
            {
                conn.Close();
            }
        }

        public static string ReprocesoPlanchaRecibidoDet(string corte, int cantidad, DateTime fecharec, string usuario, bool estadorec)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarReprocesoPlanchaDet";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@cantidad", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecharec;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estadorec;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return mes;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string ReprocesoPlanchaRecibidoDet1(int id, int cantidad, DateTime fecharec, string usuario, bool estadorec)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarReprocesoPlanchaDet1";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@cantidad", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecharec;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@estado", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estadorec;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return mes;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string EnvioReprocesoPlancha(string corte, string estilo, int unidades, DateTime fechaenv, string usuarioenv, bool estadoenv)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarEnvioReprocPlancha";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estilo;

                comm.Parameters.Add("@unidades", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidades;

                comm.Parameters.Add("@fechaenv", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fechaenv;

                comm.Parameters.Add("@usuarioenv", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuarioenv;

                comm.Parameters.Add("@estadoenv", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estadoenv;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return mes;
            }
            finally
            {
                conn.Close();
            }

        }

        public static string HeatTransferIrregulares(string corte, int cantidad, DateTime fechaI, string comentario, string usuarioI)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdGuardarIrregularesHT";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corte", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@cantidadI", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fechaI;

                comm.Parameters.Add("@comentario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = comentario;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuarioI;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return mes;
            }
            finally
            {
                conn.Close();
            }

        }
        #endregion

        #region Vincular de pervasive Operario y Operacion

        public static string VincularOperario(string codigo, string nombre, string barcode)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "AddUpdOperario";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@codigo", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = codigo;

                comm.Parameters.Add("@nombre", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = nombre;

                comm.Parameters.Add("@barcode", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = barcode;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string VincularOperacion(string desc, string descrcml)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "AddUpdOperacion";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@descripcion", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = desc;

                comm.Parameters.Add("@descripCompl", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = descrcml;

                comm.ExecuteNonQuery();

                return "OK";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region Procesos Intex
        public static string enviarRecepcionIntex(int idcorte, string corte, string estilo, int cantidadenv, int totalc, string cortecompleto, string usuario)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spd_enviarRecepcionIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                comm.Parameters.Add("@corte", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estilo;

                comm.Parameters.Add("@cantenv", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidadenv;

                comm.Parameters.Add("@totalcant", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = totalc;

                comm.Parameters.Add("@corteCompleto", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = cortecompleto;

                comm.Parameters.Add("@usuario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string AgregarCorteSecado(string corteCompleto, string estilo, int unidadesT)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "Intexdbo.spd_agregarCorteSecado";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();                

                comm.Parameters.Add("@cortecompleto", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corteCompleto;

                comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estilo;                

                comm.Parameters.Add("@totalcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesT;                

                comm.ExecuteNonQuery();

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string AgregarCantidadBultoSecado(string corteCompleto, int unidadesSecadas, string estilo, int cantidadRecibida, string usuario)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "Intexdbo.spd_AgregarCantidadBultoSecadoIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@cortecompleto", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corteCompleto;                

                comm.Parameters.Add("@cantidadSecado", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesSecadas;

                comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estilo;

                comm.Parameters.Add("@cantidadRecibida", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidadRecibida;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@parSalida", SqlDbType.NVarChar, 150);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var resp = comm.Parameters["@parSalida"].Value.ToString();

                return resp;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string RepararBultoSecadoIntex(int id, string corteCompleto, int unidadesSecadas, string usuario, string parametroTarea)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "Intexdbo.spd_RepararUnidadesIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idSecadoDet", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@corteCompleto", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corteCompleto;

                comm.Parameters.Add("@unidadesUpd", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesSecadas;

                comm.Parameters.Add("@usuario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@parametroTarea", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = parametroTarea;

                comm.Parameters.Add("@paramSalida", SqlDbType.NVarChar, 200);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var resp = comm.Parameters["@paramSalida"].Value.ToString();

                return resp;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable BihorarioSecadoIntex(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "Intexdbo.spd_BihorarioSecadoIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;                

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string AgregarCantidadAuditoriaSecado(string corteCompleto, string estilo, int unidadesSecadas, int unidadesAuditadasDia, int unidadesSecadasAuditadas, bool checkaudit, string observacion, string usuario)            
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "Intexdbo.spd_AgregarCantidadAuditadaSecadoIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@cortecompleto", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corteCompleto;

                comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estilo;

                comm.Parameters.Add("@unidadesSecadas", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesSecadas;

                comm.Parameters.Add("@unidadesAuditDia", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesAuditadasDia;

                comm.Parameters.Add("@unidadesAuditadas", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesSecadasAuditadas;

                comm.Parameters.Add("@check", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = checkaudit;

                comm.Parameters.Add("@observacion", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = observacion;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@parSalida", SqlDbType.NVarChar, 150);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var resp = comm.Parameters["@parSalida"].Value.ToString();

                return resp;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string AgregarCantidadAuditoriaSecadoReparado(string corteCompleto, string estilo, int unidadesAuditadas, bool checkaudit, string observacion, string usuario, DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "[Intexdbo].[spd_AgregarCantAuditRepSecadoIntex]";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@cortecompleto", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corteCompleto;

                comm.Parameters.Add("@estilo", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = estilo;

                comm.Parameters.Add("@unidades", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesAuditadas;                

                comm.Parameters.Add("@estAudit", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = checkaudit;

                comm.Parameters.Add("@coment", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = observacion;

                comm.Parameters.Add("@userAudit", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@fechaAudit", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                comm.Parameters.Add("@parSalida", SqlDbType.NVarChar, 250);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var resp = comm.Parameters["@parSalida"].Value.ToString();

                return resp;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string RepararAuditoriaIntex(int id, string corteCompleto, int unidadesAuditadas, string usuario)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "Intexdbo.spd_RepararUnidadesIntexAuditoria";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idAuditoriaSecado", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@corteCompleto", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corteCompleto;

                comm.Parameters.Add("@unidadesUpd", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidadesAuditadas;

                comm.Parameters.Add("@usuario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;                

                comm.Parameters.Add("@paramSalida", SqlDbType.NVarChar, 250);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var resp = comm.Parameters["@paramSalida"].Value.ToString();

                return resp;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string RepararFechaAuditoriaIntex(int id, string corteCompleto, DateTime Fecha, string usuario)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "Intexdbo.spd_RepararFechaIntexAuditoria";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idAuditoriaSecado", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id;

                comm.Parameters.Add("@corteCompleto", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corteCompleto;

                comm.Parameters.Add("@fechaUpd", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = Fecha;

                comm.Parameters.Add("@usuario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@paramSalida", SqlDbType.NVarChar, 250);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var resp = comm.Parameters["@paramSalida"].Value.ToString();

                return resp;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteAuditoriaSecadoIntex(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "[Intexdbo].[spd_ReporteAuditoriaSecadoIntex]";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string enviarProcesoSecado(string corteCompleto, int numEnvioSec, int envioDiaSec, string usuario)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "Intexdbo.spd_EnviosProcesoSecado";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corteCompleto", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corteCompleto;

                comm.Parameters.Add("@numEnvioSec", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = numEnvioSec;

                comm.Parameters.Add("@envioDiaSec", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envioDiaSec;              

                comm.Parameters.Add("@userEnvioSec", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@parSalida", SqlDbType.NVarChar, 150);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var resp = comm.Parameters["@parSalida"].Value.ToString();

                return resp;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string enviarProcesoSecadoXcorte(string corteCompleto, int idSecadoDet, int numEnvioSec, int envioDiaSec, string usuario)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "Intexdbo.spd_EnviosProcesoSecadoXcorte";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corteCompleto", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corteCompleto;

                comm.Parameters.Add("@idsec", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idSecadoDet;

                comm.Parameters.Add("@numEnvioSec", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = numEnvioSec;

                comm.Parameters.Add("@envioDiaSec", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = envioDiaSec;

                comm.Parameters.Add("@userEnvioSec", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@parSalida", SqlDbType.NVarChar, 150);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var resp = comm.Parameters["@parSalida"].Value.ToString();

                return resp;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteProcesosIntex(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "[Intexdbo].[spd_ReportePorProcesoIntex]";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteBihorarioSecadoEnviado(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "[Intexdbo].[spd_ReporteBihorarioSecadoEnviadoIntex]";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReportePackingListoEnvioMedida(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "[Intexdbo].[spd_ReportePackingListEnvioSecadoaMedida]";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteInventarioIntexProc()
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "[spdInventarioIntexPorProceso]";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();               

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ReporteInventarioIntexProcPorCorte(string corte)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "[spdInventarioIntexPorProcesoPorCorte]";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@corteCompleto", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = corte;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        #region Reproceso Empaque
        public static string AgregarPlanReprocesoEmpaque(string tipo, string descripcion, string usuario)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "[RP].[spd_agregarPlanReproceso]";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@tipoR", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = tipo;

                comm.Parameters.Add("@descripcionR", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = descripcion;

                comm.Parameters.Add("@usuarioR", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@paramSalida", SqlDbType.NVarChar, 100);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var resp = comm.Parameters["@paramSalida"].Value.ToString();

                return resp;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string AgregarUnidadesReprocesoEmpaque(int planr, int idoper, string nombreoper, int unidades, string usuario, int tipop, string obser)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "[RP].[spd_agregarUnidadesReprocesoEmpaque]";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@planR", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = planr;

                comm.Parameters.Add("@idOper", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idoper;

                comm.Parameters.Add("@nombreOper", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = nombreoper;

                comm.Parameters.Add("@unidades", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidades;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@tipoP", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = tipop;

                comm.Parameters.Add("@observacionR", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = obser;

                comm.Parameters.Add("@paramSalida", SqlDbType.NVarChar, 150);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var resp = comm.Parameters["@paramSalida"].Value.ToString();

                return resp;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string AgregarUnidadesReparaciones(
            int idOrder, 
            string corte, 
            int idDefecto, 
            int idPosicion, 
            string talla, 
            string area, 
            string usuario, 
            int unidades, 
            string observ, 
            string estilo, 
            string color = "")
        {
            SqlConnection cnn = new SqlConnection(Get_ConnectionString());
            SqlCommand cmd = new SqlCommand();

            try
            {
                cnn.Open();
                cmd.Connection = cnn;
                cmd.CommandText = "[dbo].[spSaveReparacionesCorteCliente]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();

                cmd.Parameters.Add("@idOrder", SqlDbType.Int);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = idOrder;

                cmd.Parameters.Add("@corte", SqlDbType.NChar, 50);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = corte;

                cmd.Parameters.Add("@idDefecto", SqlDbType.Int);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = idDefecto;

                cmd.Parameters.Add("@idPosicion", SqlDbType.Int);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = idPosicion;

                cmd.Parameters.Add("@talla", SqlDbType.NChar, 15);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = talla;

                cmd.Parameters.Add("@color", SqlDbType.NChar, 15);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = color;

                cmd.Parameters.Add("@unidades", SqlDbType.Int);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = unidades;

                cmd.Parameters.Add("@area", SqlDbType.NChar, 2);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = area;

                cmd.Parameters.Add("@usuario", SqlDbType.NChar);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = usuario;

                cmd.Parameters.Add("@observaciones", SqlDbType.NVarChar, 250);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = observ;

                cmd.Parameters.Add("@style", SqlDbType.NVarChar, 100);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = estilo;

                cmd.Parameters.Add("@paramSalida", SqlDbType.NVarChar, 150);
                cmd.Parameters[cmd.Parameters.Count - 1].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                string resp = cmd.Parameters["@paramSalida"].Value.ToString();

                return resp;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                cnn.Close();
            }
        }

        public static string EliminarReparacion(int IdReparacion)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spEliminarReparacion";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@pId", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = IdReparacion;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        #region HEAT TRANSFER
        public static string GuardarProduccionDelDia(Produccion model)
        {
            SqlConnection cnn = new SqlConnection(Get_ConnectionString());
            SqlCommand cmd = new SqlCommand();

            try
            {
                cnn.Open();
                cmd.Connection = cnn;
                cmd.CommandText = "[HT].[spGuardarProduccion]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();

                cmd.Parameters.Add("@pOperacion", SqlDbType.Int);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = model.IdOperacion;

                cmd.Parameters.Add("@pColaborador", SqlDbType.Int);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = model.IdColaborador;

                cmd.Parameters.Add("@pResponsable", SqlDbType.Int);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = model.IdResponsable;

                cmd.Parameters.Add("@pOrder", SqlDbType.Int);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = model.IdOrder;

                cmd.Parameters.Add("@pCorte", SqlDbType.NVarChar, 50);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = model.IdCorte;

                cmd.Parameters.Add("@pStyle", SqlDbType.NVarChar, 100);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = model.IdStyle;

                cmd.Parameters.Add("@pUnidades", SqlDbType.Int);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = model.Unidades;

                cmd.Parameters.Add("@pUsuario", SqlDbType.NVarChar, 50);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = model.Usuario;

                cmd.Parameters.Add("@paramSalida", SqlDbType.NVarChar, 150);
                cmd.Parameters[cmd.Parameters.Count - 1].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                return cmd.Parameters["@paramSalida"].Value.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                cnn.Close();
            }
        }

        public static string EliminarLineaProduccion(int IdProduccion)
        {
            SqlConnection cnn = new SqlConnection(Get_ConnectionString());
            SqlCommand cmd = new SqlCommand();

            try
            {
                cnn.Open();
                cmd.Connection = cnn;
                cmd.CommandText = "HT.spEliminarLineaProduccion";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();

                cmd.Parameters.Add("@pId", SqlDbType.Int);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = IdProduccion;

                cmd.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                cnn.Close();
            }
        }

        public static string CrearColaborador(int idResponsable, string nombreColaborador)
        {
            SqlConnection cnn = new SqlConnection(Get_ConnectionString());
            SqlCommand cmd = new SqlCommand();

            try
            {
                cnn.Open();
                cmd.Connection = cnn;
                cmd.CommandText = "[HT].[spCrearColaborador]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();

                cmd.Parameters.Add("@pResponsable", SqlDbType.Int);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = idResponsable;

                cmd.Parameters.Add("@pNombreColaborador", SqlDbType.NVarChar, 200);
                cmd.Parameters[cmd.Parameters.Count - 1].Value = nombreColaborador;

                cmd.Parameters.Add("@paramSalida", SqlDbType.NVarChar, 150);
                cmd.Parameters[cmd.Parameters.Count - 1].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                return cmd.Parameters["@paramSalida"].Value.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                cnn.Close();
            }
        }
        #endregion


        public static DataTable Bihorario_ReprocesoEmpaque(DateTime fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();
            DataTable a = new DataTable();
            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "[RP].[spd_ReporteBihorarioEmpaqueReproceso]";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@fecha", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;

                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(a);

                comm.CommandTimeout = 600;

                return a;

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region auditoria embarque defectos Intex
        public static int spdSaveAEIntex(int idcorte, string codigo, int lote, bool estado, DateTime horaI, DateTime horaF, decimal aql, int muestra, int unidef, string coment, int idnivel, int idnivelaql, string usuario, string nombreaudit)
        {

            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAEIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idcorte", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idcorte;

                comm.Parameters.Add("@codigoLote", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = codigo;

                comm.Parameters.Add("@lote", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = lote;

                comm.Parameters.Add("@estadoAudit", SqlDbType.Bit);
                comm.Parameters[comm.Parameters.Count - 1].Value = estado;

                comm.Parameters.Add("@horaInicial", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = horaI;

                comm.Parameters.Add("@horaFinal", SqlDbType.DateTime);
                comm.Parameters[comm.Parameters.Count - 1].Value = horaF;

                comm.Parameters.Add("@aql", SqlDbType.Decimal);
                comm.Parameters[comm.Parameters.Count - 1].Value = aql;

                comm.Parameters.Add("@muestra", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = muestra;

                comm.Parameters.Add("@unidadesDefect", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unidef;

                comm.Parameters.Add("@comentario", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = coment;

                comm.Parameters.Add("@idnivel", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idnivel;

                comm.Parameters.Add("@idnivelaql", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idnivelaql;

                comm.Parameters.Add("@usuario", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = usuario;

                comm.Parameters.Add("@nombreAuditori", SqlDbType.NChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = nombreaudit;

                comm.Parameters.Add("@idembarque", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var r = comm.Parameters["@idembarque"].Value.ToString();
                var id = Convert.ToInt32(comm.Parameters["@idembarque"].Value.ToString());

                return id;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int spdSaveAEDefectosDetIntex(int idnumeroAudi, int iddefectos, int idposicion, int cantidad)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {

                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "spdSaveAEDetailIntex";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@idNumAudi", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idnumeroAudi;

                comm.Parameters.Add("@iddefecto", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = iddefectos;

                comm.Parameters.Add("@idposicion", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = idposicion;

                comm.Parameters.Add("@cantidad", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = cantidad;

                comm.Parameters.Add("@idDetail", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Direction = ParameterDirection.Output;

                comm.ExecuteNonQuery();

                var r = comm.Parameters["@idDetail"].Value.ToString();
                var id = Convert.ToInt32(comm.Parameters["@idDetail"].Value.ToString());

                return id;
                //return "OK";
            }
            catch (Exception ex)
            {
                string mes = ex.Message;
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

    }
}


