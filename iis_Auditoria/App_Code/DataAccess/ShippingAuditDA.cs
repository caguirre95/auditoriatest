﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using SistemaAuditores.Business;
using System.Data;

namespace SistemaAuditores.DataAccess
{    
    public class ShippingAuditDA : DataAccess
    {
        
        public static string SaveShippingAudit(int id_defectos, int Id_Order, int id_inspect, int qtyMayor, int qtymenor, int unid_audit, int def_mayor, int Approved, string fecha)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "Insert_Shipping_Audit";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@id_defectos", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_defectos;

                comm.Parameters.Add("@Id_Order", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Order;

                comm.Parameters.Add("@id_inspect", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_inspect;

                comm.Parameters.Add("@qtyMayor", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = qtyMayor;

                comm.Parameters.Add("@qtymenor", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = qtymenor;

                comm.Parameters.Add("@unid_audit", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = unid_audit;

                comm.Parameters.Add("@def_mayor", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = def_mayor;

                comm.Parameters.Add("@Approved", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Approved;

                comm.Parameters.Add("@fecha", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = fecha;                

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}