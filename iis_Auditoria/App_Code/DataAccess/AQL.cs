﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

/// <summary>
/// Devuelve las unidades a auditar y si el bulto esta aceptado o rechazado
/// </summary>


namespace SistemaAuditores.DataAccess
{
    public class AQL
    {
        public static string Get_ConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = WebConfigurationManager.AppSettings["DataSource"];
            builder.InitialCatalog = WebConfigurationManager.AppSettings["DataBase"];
            builder.UserID = WebConfigurationManager.AppSettings["User"];
            builder.Password = WebConfigurationManager.AppSettings["Password"];
            return builder.ConnectionString;
        }

        public static DataTable Get_DataTable(string query)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            try
            {
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                return dt;
            }
            catch
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable DataAQL(int cantidad)
        {
            int cant=0;
            DataTable aql = new DataTable();
            string query = "select levelaql from AQL";
            aql = DataAccess.Get_DataTable(query);
            DataTable muestreo = new DataTable();
            string m = "select sample_size, acep_quantity,rech_quantity from Muestreo where nivel_insp='" + aql.Rows[0][0] + "' and '" 
                + cantidad + "'between [rango_incial] and rango_final";
            muestreo = DataAccess.Get_DataTable(m);
            cant = Convert.ToInt32(muestreo.Rows[0][0]);
            return muestreo;
        }

        public static string SaveInspectLevel(int id_defectos)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveLevelAql";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@levelaql", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = id_defectos;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}