﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using SistemaAuditores.Business;

namespace SistemaAuditores.DataAccess
{
    public class POrderDA : DataAccess
    {
        public static DataTable Get_Order()
        {
            string query = "select * from POrder";
            DataTable dt = Get_DataTable(query);
            return dt;
        }

        public static DataTable Get_OrderbyPOrder(string po)
        {

            string query = "select * from POrder where POrder='" + po + "'";
            DataTable dt = Get_DataTable(query);
            return dt;
        }

        public static DataTable Get_IDOrderbyPOrder(string po)
        {
            string query = "select Id_Order from POrder where POrder='" + po + "'";
            DataTable dt = Get_DataTable(query);
            return dt;
        }

        public static DataTable Get_Date(string id_order)
        {
            string query = "select CreatedDate, ModifiedDate from Bundle where Id_Order=" + id_order.ToString();
            DataTable dt = Get_DataTable(query);
            return dt;
        }

        public static DataTable Get_OrderbyId(string id_order)
        {
            string query = "select * from POrder where Id_Order=" + id_order;
            DataTable dt = Get_DataTable(query);
            return dt;
        }

        public static string Save_POrder(int Id_Cliente, int Id_Style, string POrder, string Description, int Quantity, int Bundles)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveOrder";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Cliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Cliente;

                comm.Parameters.Add("@Id_Style", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Style;

                comm.Parameters.Add("@POrder", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = POrder;

                comm.Parameters.Add("@Description", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Description;

                comm.Parameters.Add("@Quantity", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Quantity;

                comm.Parameters.Add("@Bundles", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Bundles;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }
        
        public static string SavePOrders(int Id_Cliente, int Id_Style, string POrder, string Description, int Quantity, int Bundles)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_SaveProdOrder";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_Cliente", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Cliente;

                comm.Parameters.Add("@Id_Style", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_Style;

                comm.Parameters.Add("@POrder", SqlDbType.NVarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = POrder;

                comm.Parameters.Add("@Description", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Description;

                comm.Parameters.Add("@Quantity", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Quantity;

                comm.Parameters.Add("@Bundles", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Bundles;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string Update(int Id_OrderDetail, int aprobado, string comments)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "usp_UpdateOrder";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@Id_OrderDetail", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Id_OrderDetail;

                comm.Parameters.Add("@Approved", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = aprobado;

                comm.Parameters.Add("@Comments", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = comments;             

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }    
    }
}
