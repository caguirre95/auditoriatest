﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using SistemaAuditores.Business;

namespace SistemaAuditores.DataAccess
{
    public class UsersAcess : DataAccess
    {
        public static string CreateAdminUser(string Nombre, string Email, string UserName, string Pass, int Permisos, string Rol, string Acceso)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "CreateAdmin_Usuario";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();
                               
                comm.Parameters.Add("@Nombre", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Nombre;

                comm.Parameters.Add("@Email", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Email;

                comm.Parameters.Add("@Username", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = UserName;

                comm.Parameters.Add("@Password", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Pass;

                comm.Parameters.Add("@Permisos", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = Permisos;

                comm.Parameters.Add("@Rol", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Rol;

                comm.Parameters.Add("@Acceso", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Acceso;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string UpdateUser(int UserId, string Nombre, string UserName, string Pass, string Rol)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "UpadteAdmin_Usuario";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@IdUsuario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = UserId;

                comm.Parameters.Add("@Nombre", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Nombre;

                comm.Parameters.Add("@Username", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = UserName;

                comm.Parameters.Add("@Password", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Pass;

                comm.Parameters.Add("@Rol", SqlDbType.VarChar);
                comm.Parameters[comm.Parameters.Count - 1].Value = Rol;

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public static string DeleteUser(int IdUsuario)
        {
            SqlConnection conn = new SqlConnection(Get_ConnectionString());
            SqlCommand comm = new SqlCommand();

            try
            {
                conn.Open();
                comm.Connection = conn;
                comm.CommandText = "DeleteAdminUsuario";
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Clear();

                comm.Parameters.Add("@IdUsuario", SqlDbType.Int);
                comm.Parameters[comm.Parameters.Count - 1].Value = IdUsuario;             

                comm.ExecuteNonQuery();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}