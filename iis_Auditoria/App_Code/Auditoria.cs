﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

public partial class Linea
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public Linea()
    {
        this.Seccion = new HashSet<Seccion>();
    }

    public int id_linea { get; set; }
    public Nullable<int> id_planta { get; set; }
    public string numero { get; set; }
    public Nullable<int> idcliente { get; set; }
    public Nullable<bool> Estado { get; set; }

    public virtual Planta Planta { get; set; }
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
    public virtual ICollection<Seccion> Seccion { get; set; }
}

public partial class Modulo
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public Modulo()
    {
        this.Seccion = new HashSet<Seccion>();
    }

    public int Id_Modulo { get; set; }
    public string Modulo1 { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
    public virtual ICollection<Seccion> Seccion { get; set; }
}

public partial class Operacion
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public Operacion()
    {
        this.OperacionOperario = new HashSet<OperacionOperario>();
    }

    public int id_operacion { get; set; }
    public string descripcion { get; set; }
    public string descripcion_completa { get; set; }
    public Nullable<double> SAM { get; set; }
    public string codigo { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
    public virtual ICollection<OperacionOperario> OperacionOperario { get; set; }
}

public partial class OperacionOperario
{
    public int idoperarioOpe { get; set; }
    public Nullable<int> id_operacion { get; set; }
    public Nullable<int> id_seccion { get; set; }
    public Nullable<int> id_operario { get; set; }
    public Nullable<bool> estado { get; set; }

    public virtual Operacion Operacion { get; set; }
    public virtual Operario Operario { get; set; }
    public virtual Seccion Seccion { get; set; }
}

public partial class Operario
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public Operario()
    {
        this.OperacionOperario = new HashSet<OperacionOperario>();
    }

    public int id_operario { get; set; }
    public string codigo { get; set; }
    public string nombre { get; set; }
    public string CodigoBarra { get; set; }
    public Nullable<bool> activo { get; set; }
    public string codigoBarra { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
    public virtual ICollection<OperacionOperario> OperacionOperario { get; set; }
}

public partial class Planta
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public Planta()
    {
        this.Linea = new HashSet<Linea>();
    }

    public int id_planta { get; set; }
    public string descripcion { get; set; }
    public Nullable<bool> estado { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
    public virtual ICollection<Linea> Linea { get; set; }
}

public partial class Seccion
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public Seccion()
    {
        this.OperacionOperario = new HashSet<OperacionOperario>();
    }

    public int id_seccion { get; set; }
    public Nullable<int> id_linea { get; set; }
    public Nullable<int> idModulo { get; set; }

    public virtual Linea Linea { get; set; }
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
    public virtual ICollection<OperacionOperario> OperacionOperario { get; set; }
    public virtual Modulo Modulo { get; set; }
}

public partial class spd_ReporteAuditoriaSecadoIntex_Result
{
    public string estilo { get; set; }
    public string corteCompleto { get; set; }
    public Nullable<int> TotalDia { get; set; }
    public int TotalAudit { get; set; }
    public string Porcentaje { get; set; }
}
