﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de DatosRoster
/// </summary>
public class DatosRoster
{
    public DatosRoster()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public static string guardarRoster(List<Roster> list)
    {
        try
        {
            using (AuditoriaEntities au = new AuditoriaEntities())
            {
                using (var transaction = au.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var item in list)
                        {
                            string line = item.Linea.ToString();
                            string modu = item.ModuloS;
                            string codemp1 = item.CodigoEmp;
                            string codbar1 = item.CodigoBarra;
                            string operd1 = item.DescripcionOper;

                            var l = au.Linea.Where(a => a.numero.Equals(line)).SingleOrDefault();
                            var m = au.Modulo.Where(a => a.Modulo1.Equals(modu)).SingleOrDefault();

                            int l1 = l.id_linea;
                            int m1 = m.Id_Modulo;

                            int idsec = 0;
                            int idoper = 0;
                            int idoperac = 0;

                            /*Seccion Inicio*/
                            var seccion = au.Seccion.Where(a => a.id_linea == l1 && a.idModulo == m1).SingleOrDefault();

                            if (seccion == null)
                            {
                                au.Seccion.Add(new Seccion
                                {
                                    id_linea = l1,
                                    idModulo = m1
                                });

                                au.SaveChanges();

                                var seccion1 = au.Seccion.Where(a => a.id_linea == l1 && a.idModulo == m1).SingleOrDefault();

                                idsec = seccion1.id_seccion;
                            }
                            else
                            {
                                idsec = seccion.id_seccion;
                            }
                            /*Seccion Fin*/
                            /*-----------------------------------------------------------*/
                            /*Operario Inicio*/
                            var operario = au.Operario.Where(a => a.codigo.Equals(codemp1.Trim()) && a.CodigoBarra.Equals(codbar1.Trim())).SingleOrDefault();

                            if (operario == null)
                            {
                                //No hacer nada
                            }
                            else
                            {
                                idoper = operario.id_operario;
                            }
                            /*Operario Fin*/
                            /*-----------------------------------------------------------*/
                            /*Operacion Inicio*/
                            var operacion = au.Operacion.Where(a => a.descripcion_completa.Equals(operd1.Trim())).SingleOrDefault();

                            if (operacion == null)
                            {
                                //No hacer nada
                            }
                            else
                            {
                                idoperac = operacion.id_operacion;
                            }
                            /*Operacion Fin*/

                            var operoper = au.OperacionOperario.Where(a => a.id_operacion == idoperac && a.id_seccion == idsec && a.id_operario == idoper).SingleOrDefault();

                            if (operoper != null)
                            {
                                operoper.id_operacion = idoperac;
                                operoper.id_seccion = idsec;
                                operoper.id_operario = idoper;
                                operoper.estado = true;
                            }
                            else
                            {
                                au.OperacionOperario.Add(new OperacionOperario
                                {
                                    id_operacion = idoperac,
                                    id_seccion = idsec,
                                    id_operario = idoper,
                                    estado = true
                                });
                            }
                            au.SaveChanges();                            
                        }

                        transaction.Commit();

                        return "Exito";
                    }
                    catch (Exception ex)
                    {
                        string m = ex.Message;
                        transaction.Rollback();
                        return "Error";
                    }
                }
            }            
        }
        catch (Exception)
        {

            throw;
        }
    }
}