﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SistemaAuditores.Business
{
    public class Production_Order
    {
        private int _id_OrderDetail;
        private int _id_Style;
        private int _porder;
        private string _description;
        private int _quantity;
        private int _bundles;

        public int Id_OrderDetail
        {
            get { return _id_OrderDetail; }
            set { _id_OrderDetail = value; }
        }

        public int Id_Style
        {
            get { return _id_Style; }
            set { _id_Style = value; }
        }

        public int POrder
        {
            get { return _porder; }
            set { _porder = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        public int Bundles
        {
            get { return _bundles; }
            set { _bundles = value; }
        }
    }
}
