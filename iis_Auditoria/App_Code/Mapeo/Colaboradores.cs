﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Colaboradores
/// </summary>
public class Colaboradores
{
    public int idresponsable { get; set; }
    public string responsable { get; set; }
    public int idcolaborador { get; set; }
    public string colaborador { get; set; }
}