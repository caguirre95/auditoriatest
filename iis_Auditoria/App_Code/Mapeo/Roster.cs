﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Roster
/// </summary>
public class Roster
{
    public Roster()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public int Linea { get; set; }
    public string ModuloS { get; set; }
    public string CodigoEmp { get; set; }
    public string CodigoBarra { get; set; }
    public string DescripcionOper { get; set; }

}