﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de pOrderClass
/// </summary>
public class pOrder
{    
    public int idorder { get; set; }
    public string porder { get; set; }
    public string style { get; set; }   
}