﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Produccion
/// </summary>
public class Produccion
{
    public int IdOperacion { get; set; }
    public int IdColaborador { get; set; }
    public int IdResponsable { get; set; }
    public int IdOrder { get; set; }
    public string IdCorte { get; set; }
    public string IdStyle { get; set; }
    public int Unidades { get; set; }
    public string Usuario { get; set; }
}