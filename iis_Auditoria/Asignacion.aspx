﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Asignacion.aspx.cs" Inherits="Asignacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);


                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });

    </script>

    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>

    <script type="text/javascript">
        function correctoG() {
            swal({
                title: '!Exito!',

                type: 'success'
            });
        }

        function errorG() {
            swal({
                title: '!Error!',

                type: 'error'
            });
        }

        function errorUser() {
            swal({
                title: '!Seleccione Correctamente el Corte!',

                type: 'Info'
            });
        }
        
        function restrincion() {
            swal({
                title: '!Utilice un Usuario Autorizado!',
                type: 'Info'
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:10px;">
            <div class="panel-heading"><strong>Asignacion de Corte a Linea </strong></div>
            <div class="panel-body">
                <div class="col-lg-12">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        
                                <div class="col-lg-12">
                                    Corte:
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    Linea:
                                     <asp:DropDownList ID="DropDownListlinea" CssClass="form-control" runat="server"></asp:DropDownList>
                                    <asp:HiddenField ID="hfidPorder" runat="server" />

                                    Description:
                                    <asp:DropDownList ID="drpdesc" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">Select...</asp:ListItem>
                                        <asp:ListItem Value="O">Overall</asp:ListItem>
                                        <asp:ListItem Value="P">Pant</asp:ListItem>
                                        <asp:ListItem Value="S">Short</asp:ListItem>
				                        <asp:ListItem Value="SHI">Shirt</asp:ListItem>
                                        <asp:ListItem Value="A">Apromt</asp:ListItem>
                                        <asp:ListItem Value="CC">Chore Coat</asp:ListItem>
                                          <asp:ListItem Value="DNA">Duck Nail Apron</asp:ListItem>
                                          <asp:ListItem Value="XS">Xmas Stockeing</asp:ListItem>
                                          <asp:ListItem Value="QLH">Quilt Line Hood</asp:ListItem>
                                          <asp:ListItem Value="DB">Dog Bed</asp:ListItem>
                                          <asp:ListItem Value="NP">Napper Pad</asp:ListItem>
                                          <asp:ListItem Value="B">Blanket</asp:ListItem>
                                         <asp:ListItem Value="J">Jacket</asp:ListItem>
                                         <asp:ListItem Value="T">Tactical</asp:ListItem>
                                         <asp:ListItem Value="CFR">Carhartt FR</asp:ListItem>
                                        <asp:ListItem Value="FSW">Front Sweatshirt</asp:ListItem>
                                        <asp:ListItem Value="HFR">Hooded FR</asp:ListItem>  
                                        <asp:ListItem Value="KG">KIMONO GOWN</asp:ListItem>
                                       <asp:ListItem Value="KG">FACE MASK</asp:ListItem>  
                                        <asp:ListItem Value="FRB">FR BALACLAVA</asp:ListItem>  
                                        
                                       
                                    </asp:DropDownList>

                                    Wash
                                    <asp:DropDownList runat="server" ID="drpwash" CssClass="form-control">
                                        <asp:ListItem Value="S">Select...</asp:ListItem>
                                        <asp:ListItem Value="0">No Se Lava</asp:ListItem>
                                        <asp:ListItem Value="1">Se Lava</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-12" style="padding-top:10px">
                                    <asp:Button ID="btnGuadar" runat="server" Text="Guardar" CssClass="btn btn-primary form-control " OnClick="btnGuadar_Click" />
                                </div>
                           
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

