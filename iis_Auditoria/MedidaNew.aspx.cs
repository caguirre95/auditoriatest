﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MedidaNew : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {

                if (User.Identity.IsAuthenticated == true)
                {
                    cargaestacion();
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Response.Redirect("Login.aspx");
                }
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    void cargaestacion()
    {
        try
        {
            DataTable dt = DataAccess.Get_DataTable("select idEstacion from tbEstacionesTrabajo where UserName = '" + User.Identity.Name + "'");

            if (dt.Rows.Count > 0)
            {
                hdnidestacion.Value = dt.Rows[0][0].ToString();
            }

        }
        catch (Exception)
        {

            //throw;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top 10 p.Id_Order,p.POrder,s.Style,s.id_style from POrder p join Style s on p.Id_Style=s.Id_Style where p.POrder like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][1].ToString();
                obj.style = dt.Rows[i][2].ToString();
                obj.idstyle = int.Parse(dt.Rows[i][3].ToString());

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetTallaXEstilo(string pre,int idestilo)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top(20) t.Talla,et.idEstiloTalla from tbTalla t join tbEstiloTalla et on et.idTalla=t.idTalla where t.Talla like '%" + pre + "%'  and et.idEstilo=" + idestilo + " order by t.Talla", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();
                //talla
                obj.porder = dt.Rows[i][0].ToString().Trim();
                obj.idorder =Convert.ToInt32(dt.Rows[i][1].ToString());

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetTallasXIdCorte(string id)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = "spdGetTallasXIdCorte";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@id", SqlDbType.Int);
            cmd.Parameters[cmd.Parameters.Count - 1].Value = Convert.ToInt32(id);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][1].ToString());
                obj.porder = dt.Rows[i][0].ToString();
                //obj.style = dt.Rows[i][2].ToString();

                list.Add(obj);
            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetTallasXIdStyle(string id)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = "spdGetTallasXIdStyle";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@id", SqlDbType.Int);
            cmd.Parameters[cmd.Parameters.Count - 1].Value = Convert.ToInt32(id);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][1].ToString());
                obj.porder = dt.Rows[i][0].ToString();
                //obj.style = dt.Rows[i][2].ToString();

                list.Add(obj);
            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetEspecificaciones(string id)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select e.idespecificacion,e.Valor,e.TolMax,e.Tolmin,pm.NombreMedida from tbEspecificacion e join tblPuntosMedida pm on e.idPuntoDeMedida=pm.idPuntosM where e.idEstiloTalla=" + id, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.value1 = float.Parse(dt.Rows[i][1].ToString());
                obj.value2 = float.Parse(dt.Rows[i][2].ToString());
                obj.value3 = float.Parse(dt.Rows[i][3].ToString());
                obj.porder = dt.Rows[i][4].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetEspecificaciones1(string id)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select e.idespecificacion,e.Valor,e.TolMax,e.Tolmin,pm.NombreMedida from tbEspecificacion e join tblPuntosMedida pm on e.idPuntoDeMedida=pm.idPuntosM where e.idEstiloTalla=" + id, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.value1 = float.Parse(dt.Rows[i][1].ToString());
                obj.value2 = float.Parse(dt.Rows[i][2].ToString());
                obj.value3 = float.Parse(dt.Rows[i][3].ToString());
                obj.porder = dt.Rows[i][4].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    // [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GuardarMaster(string idestacion, string idestilotalla, string idporder, string estado)
    {
        try
        {
            //var obj = new porderClass();

            

            return OrderDetailDA.saveObjMaster(Convert.ToInt32(idestacion==""?"0":idestacion), Convert.ToInt32(idestilotalla), Convert.ToInt32(idporder), Convert.ToBoolean(estado));

            //return "test";

        }
        catch (Exception)
        {
            //var obj = new porderClass();
            //obj.porder = "";
            return "";
        }

    }

    [WebMethod]
    // [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GuardarDetail(int idespecificacion, float valormedido, float valorspeck, string status, string id)
    {
        try
        {
            return OrderDetailDA.saveObjDetail(idespecificacion, valormedido, valorspeck, status, id);
        }
        catch (Exception)
        {
            return "";
        }

    }

    public class porderClass
    {
        public int idorder { get; set; }
        public int idstyle { get; set; }
        public string porder { get; set; }
        public string style { get; set; }
        public float value1 { get; set; }
        public float value2 { get; set; }
        public float value3 { get; set; }
    }

    /*
     
select top(20) t.Talla from tbTalla t join tbEstiloTalla et on et.idTalla=t.idTalla 
where t.Talla like '%30%' and et.idEstilo=2503
     */


}