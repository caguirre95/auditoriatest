﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class MedidaEspecial1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.User.Identity.IsAuthenticated == true)
        {
            if (!IsPostBack)
            {
                //DataTable dtIdLinea = DataAccess.Get_DataTable("select linea from tblUserMedida where userms = '"+ Page.User.Identity.Name.ToString()+"'");

                //HiddenFielduser.Value = dtIdLinea.Rows[0][0].ToString();

                cargachekbox();
                Session["cont"] = null;
                lblunidades.Text = "0";

                msg.Text = "";
            }
        }
        else
        {

            Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");

        }
    }

    public string guardar(string name)
    {
        try
        {
            string resp = "false";

            List<l> listC = new List<l>();
            //sirve para saber que secciones de media se van a usar
            ////**inicio**////
            if (Session["cont"] == null)
            {
                for (int i = 0; i < CheckBoxList1.Items.Count; i++)
                {
                    if (CheckBoxList1.Items[i].Selected)
                    {
                        l obj = new l
                        {
                            id = CheckBoxList1.Items[i].Value,
                            nombre = CheckBoxList1.Items[i].Text
                        };
                        listC.Add(obj);
                    }
                }
            }
            else
            {
                listC = (List<l>)Session["cont"];
            }
            ////**Fin**/////

            ////  bool resp = false;
            if (listC.Count > 0)
            {
                if (DropDownList2.Items.Count > 0)
                {
                    int n1, n2, n3, n4 = 0;
                    string n5;

                    string[] separators = { "-" };
                    string[] words = DropDownList2.SelectedValue.Split(separators, StringSplitOptions.RemoveEmptyEntries);

                    //verifica si ya guardo un registro mediante id bulto e id de seccion de medida y cuantas unidades le faltan al bulto
                    string query = " select pm.idPuntoMaster,unidadesFalt from tblPuntoMasterEspecial1 pm "
                                  + " join tblPuntosMedida m on pm.idpuntosM=m.idPuntosM where pm.idbulto="
                                  + words.First() + " and pm.idpuntosM=" + listC.First().id;

                    DataTable dt_Bundle = DataAccess.Get_DataTable(query);

                    //extrae el id de medida asignada 
                    string query2 = " select idMedida from tblMedidas m where m.Medida = '" + name + "'";
                    DataTable dt_idm = DataAccess.Get_DataTable(query2);

                    //valida si es la primer vez que se guardara una unidad del bulto
                    if (dt_Bundle.Rows.Count > 0)
                    {

                        n2 = Convert.ToInt32(dt_Bundle.Rows[0][1]);//unidades faltantes
                        //n3 = Convert.ToInt32(words.Last());
                        if (n2 > 0)
                        {
                            n2--;
                            //restamos la unidad que se resgistrara
                            n1 = Convert.ToInt32(dt_Bundle.Rows[0][0]);
                            //id de punto master

                            if (OrderDetailDA.UpdateMedidaEspecial1(n1, n2) == "OK")
                            {
                                n3 = Convert.ToInt32(dt_idm.Rows[0][0]);//id medida

                                if (OrderDetailDA.SaveMedidaBultoEspecial1(n1, n3, HttpContext.Current.User.Identity.Name) == "OK")
                                {
                                    lblseccion.Text = listC.First().nombre;
                                    lblmedida.Text = name;
                                    listC.RemoveAt(0);
                                    resp = "true";
                                }
                            }
                            //else
                            //{

                            //}
                        }
                        else
                        {
                            //debe elegir otro bulto
                            resp = "agotado";
                        }

                    }
                    else
                    {
                        n1 = Convert.ToInt32(listC.First().id);
                        n2 = Convert.ToInt32(words.First());
                        n3 = Convert.ToInt32(words.Last());
                        n4 = n3 - 1;
                        n5 = HttpContext.Current.User.Identity.Name;// id inspector

                        if (OrderDetailDA.SaveMedidaEspecial1(n1, n2, n3, n4, n5) == "OK")
                        {
                            string query3 = "select idPuntoMaster from tblPuntoMasterEspecial1 pm where pm.idbulto=" + words.First() + " and pm.idpuntosM=" + listC.First().id;
                            DataTable dt_Bundle1 = DataAccess.Get_DataTable(query3);

                            n1 = Convert.ToInt32(dt_Bundle1.Rows[0][0]);
                            n2 = Convert.ToInt32(dt_idm.Rows[0][0]);

                            if (OrderDetailDA.SaveMedidaBultoEspecial1(n1, n2, n5) == "OK")
                            {
                                lblseccion.Text = listC.First().nombre;
                                lblmedida.Text = name;

                                listC.RemoveAt(0);

                                resp = "true";
                            }
                        }
                        //else
                        //{

                        //}
                    }


                    if (listC.Count == 0)
                    {
                        if (dt_Bundle.Rows.Count > 0)
                            lblunidades.Text = (Convert.ToInt16(words.Last()) - (Convert.ToInt16(dt_Bundle.Rows[0][1]) - 1)).ToString();
                        else
                            lblunidades.Text = (Convert.ToInt16(words.Last()) - n4).ToString();

                        Session["cont"] = null;
                    }
                    else
                    {
                        Session["cont"] = listC;
                    }
                }
                else
                {
                    resp = "selectB";
                    //debe seleccionar  un bulto si no se muestran debe buscar primeramente un corte                 
                }
            }
            else
            {
                resp = "selectM";
                // no ha seleccionado ninguna seccion de medida             
            }
            return resp;
        }
        catch (Exception)
        {
            return "false";
        }

    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            if (hfidPorder.Value != string.Empty)
            {
                string queryBundle = "Select b.Id_Bundle,b.Size,b.Quantity,b.NSeq from POrder p join Bundle b on p.Id_Order = b.Id_Order where p.Id_Order = " + hfidPorder.Value + " order by NSeq asc";
                DataTable dt_Bundle = DataAccess.Get_DataTable(queryBundle);

                List<p> l = new List<p>();
                List<p> l1 = new List<p>();
                for (int i = 0; i < dt_Bundle.Rows.Count; i++)
                {
                    p obj = new p();

                    obj.nom1 = dt_Bundle.Rows[i][1].ToString() + " - " + dt_Bundle.Rows[i][3].ToString();
                    obj.nom2 = dt_Bundle.Rows[i][0].ToString() + "-" + dt_Bundle.Rows[i][2].ToString();

                    l.Add(obj);

                    p obj1 = new p();

                    obj1.nom1 = dt_Bundle.Rows[i][1].ToString() + " - " + dt_Bundle.Rows[i][3].ToString();
                    obj1.nom2 = dt_Bundle.Rows[i][0].ToString();/* + "-" + dt_Bundle.Rows[i][2].ToString();*/

                    l1.Add(obj1);
                }

                DropDownList2.DataSource = l;
                DropDownList2.DataTextField = "nom1";
                DropDownList2.DataValueField = "nom2";
                DropDownList2.DataBind();
                //  DropDownList2.Items.Insert(0, "Select...");

                DropDownListbultos.DataSource = l1;
                DropDownListbultos.DataTextField = "nom1";
                DropDownListbultos.DataValueField = "nom2";
                DropDownListbultos.DataBind();
                //  DropDownListbultos.Items.Insert(0, "Select...");

            }
        }
        catch (Exception)
        {
            throw;
        }

        #region MyRegion
        // hfiunidades.Value = dt_Bundle.Rows[i][0].ToString();

        //string queryMedidas = "select * from tblMedidas m";

        //DataTable dt_Medidas = DataAccess.Get_DataTable(queryMedidas);

        //GVMedida.DataSource = dt_Medidas;
        //GVMedida.DataBind();
        #endregion

    }

    void cargachekbox()
    {
        string us = HttpContext.Current.User.Identity.Name;
        DataTable dt_orden = DataAccess.Get_DataTable("select pm.idPuntosM as id ,pm.NombreMedida as Medida from tblPuntosMedida pm join tblsecuenciaPMedida sm on pm.idPuntosM=sm.idpuntoM where sm.rol= (select rol from tbluserMedida where userms='" + us + "') order by sm.sec asc");

        CheckBoxList1.DataSource = dt_orden;
        CheckBoxList1.DataTextField = "Medida";
        CheckBoxList1.DataValueField = "id";
        CheckBoxList1.DataBind();

        DropDownListSecciones.DataSource = dt_orden;
        DropDownListSecciones.DataTextField = "Medida";
        DropDownListSecciones.DataValueField = "id";
        DropDownListSecciones.DataBind();
        DropDownListSecciones.Items.Insert(0, "Seleccione...");

    }

    //[WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public static List<porderClass> GetPorder(string pre)
    //{
    //    try
    //    {
    //        List<porderClass> list = new List<porderClass>();

    //        DataTable dt = new DataTable();
    //        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

    //        cn.Open();
    //        SqlCommand cmd = new SqlCommand("select top 10 p.Id_Order,p.POrder,s.Style from POrder p join Style s on p.Id_Style=s.Id_Style where p.POrder like '%" + pre + "%'", cn);
    //        SqlDataAdapter da = new SqlDataAdapter(cmd);
    //        da.Fill(dt);
    //        cn.Close();

    //        for (int i = 0; i < dt.Rows.Count; i++)
    //        {
    //            porderClass obj = new porderClass();

    //            obj.idorder = int.Parse(dt.Rows[i][0].ToString());
    //            obj.porder = dt.Rows[i][1].ToString();
    //            obj.style = dt.Rows[i][2].ToString();

    //            list.Add(obj);

    //        }

    //        return list;

    //    }
    //    catch (Exception ex)
    //    {
    //        Console.Write(ex);
    //        return null;
    //    }
    //}

    //public class porderClass
    //{
    //    public int idorder { get; set; }
    //    public string porder { get; set; }
    //    public string style { get; set; }
    //}

    public class p
    {
        public string nom1 { get; set; }
        public string nom2 { get; set; }
    }

    public class l
    {
        public string id { get; set; }
        public string nombre { get; set; }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Button clickedButton = (Button)sender;
        string nombre = clickedButton.Text;

        nombre = nombre.Substring(0, nombre.Length - 1);
        string resp = guardar(nombre);

        switch (resp)
        {
            case "agotado":
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertElegirOtroBultoXS();", true);
                break;
            case "false":
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                break;
            case "selectM":
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida();", true);
                break;
            case "selectB":
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertseleccionarBultoOTalla();", true);
                break;
            default:
                break;
        }


    }

    protected void Button34_Click(object sender, EventArgs e)
    {
        string url = HttpContext.Current.Request.Url.AbsoluteUri;
        Response.Redirect(url);
        Session["cont"] = null;
    }

    protected void DropDownListSecciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            msg.Text = "";
            cargagrid();
        }
        catch (Exception)
        {
            //throw;
        }

    }

    void cargagrid()
    {

        if (DropDownListSecciones.SelectedItem.Text != "Seleccione...")
        {
            string query = " select mb.idMedidaMaster,m.idMedida,m.Medida,b.Size + ' - ' + CONVERT(varchar, b.Bld) as bulto_Secuencia"
                             + " from tblMedidaBultoEspecial1 mb join tblMedidas m on mb.idMedida = m.idMedida"
                             + " join tblPuntoMasterEspecial1 pm on mb.idMaster = pm.idPuntoMaster"
                             + " join tblPuntosMedida pme on pm.idpuntosM = pme.idPuntosM"
                             + " join Bundle b on pm.idbulto = b.Id_Bundle"
                             + " where pm.idbulto = " + DropDownListbultos.SelectedValue + " and pme.idPuntosM = " + DropDownListSecciones.SelectedValue;
            //                             + " order by m.idMedida";

            DataTable dt_unidades = DataAccess.Get_DataTable(query);

            GridViewunidades.DataSource = dt_unidades;
            GridViewunidades.DataBind();
        }

    }

    protected void GridViewunidades_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList drp = (DropDownList)e.Row.FindControl("DropDownListNuevaMedida");

                string queryBundle = "SELECT idMedida,Medida FROM tblMedidas";
                DataTable dt_Bundle = DataAccess.Get_DataTable(queryBundle);

                drp.DataSource = dt_Bundle;
                drp.DataTextField = "Medida";
                drp.DataValueField = "idMedida";
                drp.DataBind();


                Label l1 = (Label)e.Row.FindControl("lblidmedida");
                int estado = int.Parse(l1.Text);

                string test = e.Row.Cells[1].Text;
                //   int tot = e.Row.RowIndex;

                if (estado <= 8)
                {
                    e.Row.ForeColor = Color.FromName("#E74C3C");
                    // e.Row.ForeColor = Color.FromName("#2ECC71");
                }
                else if (estado > 8 && estado <= 16)
                {
                    e.Row.ForeColor = Color.FromName("#2ECC71");
                    //  e.Row.ForeColor = Color.FromName("#E74C3C");
                }
                else if (estado >= 18 && estado <= 25)
                {
                    e.Row.ForeColor = Color.FromName("#E74C3C");
                }
                else if (estado > 25)
                {
                    e.Row.ForeColor = Color.FromName("#2ECC71");
                }
                else if (estado == 17)
                {
                    e.Row.ForeColor = Color.FromName("#5DADE2");
                    // e.Row.BackColor = Color.FromArgb(69, 178, 15, 1);               
                }
                //else if (estado > 17)
                //{
                //    e.Row.ForeColor = Color.FromName("#E74C3C");
                //}

            }
        }
        catch (Exception ex)
        {
            Console.Write(ex.Message);
            //throw;
        }


    }

    protected void GridViewunidades_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {

            GridViewunidades.PageIndex = e.NewPageIndex;
            cargagrid();

        }
        catch (Exception)
        {

            // throw;
        }

    }

    protected void GridViewunidades_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "editar")
            {
                GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int indice = row.RowIndex;
                GridViewRow row2 = GridViewunidades.Rows[indice];

                string id = GridViewunidades.DataKeys[indice].Value.ToString();

                DropDownList drp = (DropDownList)row2.FindControl("DropDownListNuevaMedida");

                int i = int.Parse(drp.SelectedValue);

                if (OrderDetailDA.UpdateMedidaBultoEspecial1(Convert.ToInt32(id), i, HttpContext.Current.User.Identity.Name) == "OK")
                {
                    cargagrid();
                    msg.Text = "Cambio Exitoso!!!";
                    msg.ForeColor = Color.Green;
                    DropDownListSecciones.SelectedIndex = 0;
                    // ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                }
                else
                {
                    msg.Text = "No fue Posible el cambio!!!";
                    msg.ForeColor = Color.Red;
                    // ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }

                //literalcomando.Text = "Restrincion";

                //obtener control anidado en evento rowComand asp.net
            }
            else if (e.CommandName == "Clear")
            {
                GridViewunidades.DataSource = null;
                GridViewunidades.DataBind();
                DropDownListSecciones.SelectedIndex = 0;
                msg.Text = "";
            }
        }
        catch (Exception)
        {
            throw;
        }
    }
}