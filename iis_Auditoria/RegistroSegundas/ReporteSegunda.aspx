﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReporteSegunda.aspx.cs" Inherits="RegistroSegundas_ReporteSegunda" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="../bootstrap/js/jquery.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>

    <link href="../jquery-ui.css" rel="stylesheet" />
    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>

    <script src="../sweetalert-master/dist/sweetalert.min.js"></script>
    <link href="../sweetalert-master/dist/sweetalert.css" rel="stylesheet" />

    <script>
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "RegistrosSegundas.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        function erroralert() {
            swal({
                title: 'Falló al guardar!',
                text: "No se ha Guardado el resgistro",
                type: 'error'
            });
        }

        function camposVacios() {
            swal({
                title: 'Campos Vacios!',
                text: "Cargar correctamente el Corte",
                type: 'info'
            });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <style>
        .border-shadow {
            -webkit-box-shadow: 0px 0px 9px 5px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 0px 9px 5px rgba(0,0,0,0.75);
            box-shadow: 0px 0px 9px 5px rgba(0,0,0,0.75);
        }

        .padb {
            padding-top: 5px;
        }

        .margin-b {
            margin-bottom: 8px;
        }

        @media (max-width: 630px) {
            .col-xs-6, .col-xs-12 {
                padding-left: 5px !important;
                padding-right: 5px !important;
            }

            .anchoBtn {
                Width: 180px !important;
            }
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="container border-shadow" style="padding-top: 20px">

            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-default" style="margin-bottom: 5px !important">

                        <ol class="breadcrumb" style="margin-bottom: 8px !important">

                            <li class="active">Reportes</li>
                            <li><a href="RegistrosSegundas.aspx">Registro de Segundas</a></li>
                        </ol>

                    </nav>
                </div>

            </div>
            <div class="row">
                <h1 class="col-xs-12 col-sm-10 col-md-10 col-lg-8 col-lg-offset-2 col-md-offset-1 col-sm-offset-1 panel panel-success" style="text-align: center; background: #006699; color: white">Reporte Defectos Segunda</h1>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">Reportes</div>
                <div class="panel-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">Filtros</div>
                                <div class="panel-body">
                                    <asp:RadioButtonList ID="rd1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rd1_SelectedIndexChanged">
                                        <asp:ListItem Value="1">Corte</asp:ListItem>
                                        <asp:ListItem Value="2">Fecha</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">Busqueda</div>
                                <div class="panel-body">
                                    <asp:Panel ID="panelcut" runat="server">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Corte</label>
                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtPorder" placeholder="Corte"></asp:TextBox>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="panelcf" runat="server">
                                        Fecha Inicial
                                            <dx1:ASPxDateEdit runat="server" ID="txtfecha1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                        Fecha Final
                                            <dx1:ASPxDateEdit runat="server" ID="txtfecha2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">Opciones</div>
                                <div class="panel-body">
                                    <div>
                                        <asp:Button ID="btnbuscar" runat="server" Text="Buscar" CssClass="btn btn-primary form-control" OnClick="btnbuscar_Click" />
                                    </div>
                                    <div style="padding-top: 10px">
                                        <asp:Button ID="btnlimpiar" runat="server" Text="Limpiar" CssClass="btn btn-info form-control" OnClick="btnlimpiar_Click" />
                                    </div>
                                    <div style="padding-top: 10px">
                                        <asp:Button ID="btnexport" runat="server" Text="Exportar" CssClass="btn btn-success form-control" OnClick="btnexport_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <dx1:ASPxGridView ID="grDefectS" runat="server" AutoGenerateColumns="false" Width="100%" Theme="Material" SettingsBehavior-AllowSort="false" OnDataBinding="grDefectS_DataBinding" OnCustomCallback="grDefectS_CustomCallback">
                                <Columns>
                                    <dx1:GridViewBandColumn Caption="Reporte de Defectos de Segunda">
                                        <Columns>
                                            <dx1:GridViewDataTextColumn FieldName="POrder"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Style"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Quantity"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Defecto"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Cantidad"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Segunda"></dx1:GridViewDataTextColumn>
                                        </Columns>
                                    </dx1:GridViewBandColumn>
                                </Columns>
                                <Settings VerticalScrollableHeight="300" VerticalScrollBarMode="Visible" />
                                <SettingsPager PageSize="500">
                                    <PageSizeItemSettings Visible="true" ShowAllItem="true" />
                                </SettingsPager>
                                <TotalSummary>
                                    <dx1:ASPxSummaryItem FieldName="Segunda" SummaryType="Sum" DisplayFormat="p" />
                                </TotalSummary>
                                <Styles>
                                    <Header HorizontalAlign="Center" />
                                    <Row HorizontalAlign="Center"></Row>
                                </Styles>
                            </dx1:ASPxGridView>
                            <dx1:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx1:ASPxGridViewExporter>
                            <hr />
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <dx1:ASPxGridView ID="grdefsec" runat="server" AutoGenerateColumns="false" Width="100%" Theme="Material" SettingsBehavior-AllowSort="false" OnDataBinding="grdefsec_DataBinding" OnCustomCallback="grdefsec_CustomCallback">
                                <Columns>
                                    <dx1:GridViewBandColumn Caption="Defectos por Seccion">
                                        <Columns>
                                            <dx1:GridViewDataTextColumn FieldName="POrder"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Style"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Quantity"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Seccion"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Operacion"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Total"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Segunda"></dx1:GridViewDataTextColumn>
                                        </Columns>
                                    </dx1:GridViewBandColumn>
                                </Columns>
                                <Settings VerticalScrollableHeight="300" VerticalScrollBarMode="Visible" />
                                <SettingsPager PageSize="500">
                                    <PageSizeItemSettings Visible="true" ShowAllItem="true" />
                                </SettingsPager>                                
                                <Styles>
                                    <Header HorizontalAlign="Center" />
                                    <Row HorizontalAlign="Center"></Row>
                                </Styles>
                            </dx1:ASPxGridView>
                            <dx1:ASPxGridViewExporter ID="ASPxGridViewExporter3" runat="server"></dx1:ASPxGridViewExporter>
                            <hr />
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <dx1:ASPxGridView ID="griD" runat="server" AutoGenerateColumns="false" Width="100%" Theme="MaTerial" SettingsBehavior-AllowSort="false" OnDataBinding="griD_DataBinding" OnCustomCallback="griD_CustomCallback">
                                <Columns>
                                    <dx1:GridViewBandColumn Caption="Reporte General de Segunda">
                                        <Columns>
                                            <dx1:GridViewDataTextColumn FieldName="POrder"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Style"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Quantity"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Total"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Segunda"></dx1:GridViewDataTextColumn>
                                        </Columns>
                                    </dx1:GridViewBandColumn>
                                </Columns>
                                <Settings VerticalScrollableHeight="300" VerticalScrollBarMode="Visible" />
                                <SettingsPager PageSize="500">
                                    <PageSizeItemSettings Visible="true" ShowAllItem="true" />
                                </SettingsPager>
                                <Styles>
                                    <Header HorizontalAlign="Center" />
                                    <Row HorizontalAlign="Center"></Row>
                                </Styles>
                            </dx1:ASPxGridView>
                            <dx1:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server"></dx1:ASPxGridViewExporter>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
