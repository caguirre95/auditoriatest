﻿using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SistemaAuditores.DataAccess;
using System.Data;

public partial class RegistroSegundas_ReporteSegunda : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();
            Session["SegundaReport"] = null;
            Session["ReporteSegunda"] = null;
            Session["repSegSeccion"] = null;
            panelcut.Visible = false;
            panelcf.Visible = false;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/RegistroSegundas/ReporteSegunda.aspx");
    }

    void carga()
    {
        if (txtPorder.Text != "")
        {
            Session["SegundaReport"] = OrderDetailDA.Segundaporcorte(txtPorder.Text);
            grDefectS.DataSource = Session["SegundaReport"];
            grDefectS.DataBind();

            Session["ReporteSegunda"] = OrderDetailDA.SegundaporcorteGene(txtPorder.Text);
            griD.DataSource = Session["ReporteSegunda"];
            griD.DataBind();

            Session["repSegSeccion"] = OrderDetailDA.RepSegSeccionCorte(txtPorder.Text);
            grdefsec.DataSource = Session["repSegSeccion"];
            grdefsec.DataBind();
        }
    }

    void carga2()
    {
        if (txtfecha1.Text != "" && txtfecha2.Text != "")
        {
            Session["SegundaReport"] = OrderDetailDA.Segundaporcortefecha(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));
            grDefectS.DataSource = Session["SegundaReport"];
            grDefectS.DataBind();

            Session["ReporteSegunda"] = OrderDetailDA.SegundaCorteGeneralfecha(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));
            griD.DataSource = Session["ReporteSegunda"];
            griD.DataBind();

            Session["repSegSeccion"] = OrderDetailDA.RepSegSeccionFecha(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));
            grdefsec.DataSource = Session["repSegSeccion"];
            grdefsec.DataBind();
        }
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            string id = rd1.SelectedValue;

            switch (id)
            {
                case "1":
                    carga();
                    break;

                case "2":
                    carga2();
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        try
        {
            grDefectS.DataBind();
            griD.DataBind();

            ASPxGridViewExporter1.GridViewID = "grDefectS";
            ASPxGridViewExporter2.GridViewID = "griD";
            ASPxGridViewExporter3.GridViewID = "grdefsec";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link2.Component = ASPxGridViewExporter2;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = ASPxGridViewExporter3;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1, link2, link3 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Defectos de Segunda";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=DefectosSegunda.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void grDefectS_DataBinding(object sender, EventArgs e)
    {
        grDefectS.DataSource = (DataTable)Session["SegundaReport"];
        grDefectS.GroupBy(grDefectS.Columns["POrder"]);
        grDefectS.GroupBy(grDefectS.Columns["Style"]);
        grDefectS.ExpandAll();
    }

    protected void grDefectS_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grDefectS.DataBind();
    }

    protected void griD_DataBinding(object sender, EventArgs e)
    {
        griD.DataSource = (DataTable)Session["ReporteSegunda"];
    }

    protected void griD_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        griD.DataBind();
    }

    protected void rd1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string id = rd1.SelectedValue;

            switch (id)
            {
                case "1":
                    panelcut.Visible = true;
                    panelcf.Visible = false;
                    txtPorder.Text = "";
                    break;

                case "2":
                    panelcut.Visible = false;
                    panelcf.Visible = true;
                    txtPorder.Text = "";
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void grdefsec_DataBinding(object sender, EventArgs e)
    {
        grdefsec.DataSource = (DataTable)Session["repSegSeccion"];
        grdefsec.GroupBy(grdefsec.Columns["POrder"]);
        grdefsec.GroupBy(grdefsec.Columns["Seccion"]);
        grdefsec.ExpandAll();
    }

    protected void grdefsec_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdefsec.DataBind();
    }
}