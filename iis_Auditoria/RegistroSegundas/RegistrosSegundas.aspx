﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistrosSegundas.aspx.cs" Inherits="RegistroSegundas_RegistrosSegundas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width" />

    <title></title>

    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="../bootstrap/js/jquery.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>

    <link href="../jquery-ui.css" rel="stylesheet" />
    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>

    <script src="../sweetalert-master/dist/sweetalert.min.js"></script>
    <link href="../sweetalert-master/dist/sweetalert.css" rel="stylesheet" />
    <script>

            $(function () {
                $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "RegistrosSegundas.aspx/GetPorder", 
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    cantidad: item.cantidad,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                   $('#<%=txtstyle.ClientID%>').val(ui.item.style);
                    $('#<%=txtcantidad.ClientID%>').val(ui.item.cantidad);

                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li  >")
                        .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
                   };
            });
       
    </script>

    <script type="text/javascript">
        function erroralert() {
            swal({
                title: 'Falló al guardar!',
                text: "No se ha Guardado el resgistro",
                type: 'error'
            });
        }

        function camposVacios() {
            swal({
                title: 'Campos Vacios!',
                text: "Cargar correctamente el Corte",
                type: 'info'
            });
        }
    </script>

    <style>
        .border-shadow {
            -webkit-box-shadow: 0px 0px 9px 5px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 0px 9px 5px rgba(0,0,0,0.75);
            box-shadow: 0px 0px 9px 5px rgba(0,0,0,0.75);
        }

        .padb {
            padding-top: 5px;
        }

        .margin-b {
            margin-bottom: 8px;
        }

        .btnB {
            padding: 6px 1px !important;
            Width: 30px !important;
        }

        .btnD {
            width: 150px !important;
        }

        .anchoBtn {
            Width: 150px !important;
        }

        @media (max-width: 630px) {
            .col-xs-6, .col-xs-12 {
                padding-left: 5px !important;
                padding-right: 5px !important;
            }

            .btnD {
                width: 140px !important;
            }

            .anchoBtn {
                Width: 140px !important;
            }
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="container border-shadow">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-default" style="margin-bottom: 5px !important; margin-top: 20px !important">

                        <ol class="breadcrumb" style="margin-bottom: 8px !important">

                            <li><a href="ReporteSegunda.aspx">Reportes</a></li>
                            <li class="active">Registro de Segundas</li>
                        </ol>

                    </nav>
                </div>

            </div>
            <div class="row">

                <h1 class="col-xs-12 col-sm-10 col-md-10 col-lg-8 col-lg-offset-2 col-md-offset-1 col-sm-offset-1 panel panel-success" style="text-align: center; background: #006699; color: white">Registros Defectos Segunda</h1>
            </div>
            <aside class="row" style="margin-top: 1%">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Datos del corte
                        </div>
                        <div class="panel-body">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Corte</label>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtPorder" ></asp:TextBox>
                                    <%--<asp:HiddenField runat="server" ID="hfidPorder" />--%>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Estilo</label>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtstyle" Enabled="false"></asp:TextBox>

                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Cantidad Del Corte</label>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtcantidad" Enabled="false"></asp:TextBox>

                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nombre:</label>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtnombre" Enabled="false"></asp:TextBox>

                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Fecha</label>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtfecha" Enabled="false"></asp:TextBox>

                                </div>
                                <div class="form-group">
                                    <asp:Button Text="Limpiar Pantalla" ID="BtnLimpiar" OnClick="BtnLimpiar_Click" CssClass="btn btn-success" runat="server" />
                                </div>
                                <div class=" form-inline">
                                    <div class="form-group">

                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                            <ContentTemplate>

                                                <asp:Button Text="Verificar Defectos" ID="VerificarDefetos" OnClick="VerificarDefetos_Click" CssClass="btn btn-primary margin-b" runat="server" />
                                                <div class="btn btn-warning margin-b">
                                                    Defectos <span class="badge">
                                                        <asp:Label runat="server" ID="lbltotalDefectos"></asp:Label></span>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            CONTROL DE SEGUNDAS (CARHARTT)
                        </div>
                        <div class="panel-body">

                            <div role="tabpanel">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active text-uppercase"><a aria-controls="tab1" data-toggle="tab" href="#tab1">Hoyos</a></li>
                                    <li role="presentation"><a aria-controls="tab2" class="text-uppercase" data-toggle="tab" href="#tab2">Manchas</a></li>
                                    <li role="presentation"><a aria-controls="tab3" class="text-uppercase" data-toggle="tab" href="#tab3">Tonos</a></li>
                                    <li role="presentation"><a aria-controls="tab4" class="text-uppercase" data-toggle="tab" href="#tab4">Rep</a></li>

                                </ul>
                                <div class="tab-content padb">
                                    <!-- aml model5000 -->
                                    <div role="tabpanel" class="tab-pane active padtb" id="tab1">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="gridDefH" runat="server" DataKeyNames="idoperseg" CssClass="table table-hover table-striped" GridLines="None" OnRowCommand="gridDefH_RowCommand"
                                                    Width="100%" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="idoperseg" Visible="false" HeaderText="Codigo" />
                                                        <asp:TemplateField HeaderText="Operación" ItemStyle-Width="200">
                                                            <ItemTemplate>
                                                                <asp:Button ID="BtnDefecto" runat="server" Text='<%#Bind("Operacion") %>' CssClass="btn btn-primary btnD" CommandName="guardar" />
                                                                <asp:LinkButton CommandName="eliminar" ID="comandoRep" CssClass="btn btn-danger btnB" runat="server">
                                                                     <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Defecto">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" Text='<%#Eval("Cantidad") %>' CssClass="form-control" Enabled="false" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        <h4 style="text-align: center">Sin datos para mostrar</h4>
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div role="tabpanel" class="tab-pane padtb " id="tab2">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="gridDefM" runat="server" DataKeyNames="idoperseg" CssClass="table table-hover table-striped" GridLines="None" OnRowCommand="gridDefM_RowCommand"
                                                    Width="100%" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="idoperseg" Visible="false" HeaderText="Codigo" />
                                                        <asp:TemplateField HeaderText="Operación" ItemStyle-Width="200">
                                                            <ItemTemplate>
                                                                <asp:Button ID="BtnDefecto" runat="server" Text='<%#Bind("Operacion") %>' CssClass="btn btn-success btnD" CommandName="guardar" />
                                                                <asp:LinkButton CommandName="eliminar" ID="comandoRep" CssClass="btn btn-danger btnB" runat="server">
                                                                     <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Defecto">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" Text='<%#Eval("Cantidad") %>' Enabled="false" CssClass="form-control" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div role="tabpanel" class="tab-pane padtb " id="tab3">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="gridDefT" runat="server" DataKeyNames="idoperseg" CssClass="table table-hover table-striped" GridLines="None" OnRowCommand="gridDefT_RowCommand"
                                                    Width="100%" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="idoperseg" Visible="false" HeaderText="Codigo" />
                                                        <asp:TemplateField HeaderText="Operación" ItemStyle-Width="200">
                                                            <ItemTemplate>
                                                                <asp:Button ID="BtnDefecto" runat="server" Text='<%#Bind("Operacion") %>' CssClass="btn btn-info btnD" CommandName="guardar" />
                                                                <asp:LinkButton CommandName="eliminar" ID="comandoRep" CssClass="btn btn-danger btnB" runat="server">
                                                                     <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Defecto">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" Text='<%#Eval("Cantidad") %>' Enabled="false" CssClass="form-control" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div role="tabpanel" class="tab-pane padtb " id="tab4">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="gridDefRep" runat="server" DataKeyNames="idoperseg" CssClass="table table-hover table-striped" GridLines="None" OnRowCommand="gridDefRep_RowCommand"
                                                    Width="100%" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="idoperseg" Visible="false" HeaderText="Codigo" />
                                                        <asp:TemplateField HeaderText="Operación" ItemStyle-Width="200">
                                                            <ItemTemplate>
                                                                <asp:Button ID="BtnDefecto" runat="server" Text='<%#Bind("Operacion") %>' CssClass="btn btn-warning btnD " CommandName="guardar" />
                                                                <asp:LinkButton CommandName="eliminar" ID="comandoRep" CssClass="btn btn-danger btnB" runat="server">
                                                                     <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Defecto">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" Text='<%#Eval("Cantidad") %>' Enabled="false" CssClass="form-control" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            REPORTE DE LAVADO (CARHARTT)
                        </div>
                        <div class="panel-body">
                            <div role="tabpanel">
                                <ul class="nav nav-tabs" role="tablist">

                                    <li role="presentation" class="active "><a aria-controls="tab5" class=" text-primary text-uppercase" data-toggle="tab" href="#tab5">Abrasion</a></li>
                                    <li role="presentation"><a aria-controls="tab6" class="text-uppercase" data-toggle="tab" href="#tab6">Manc. Lavado</a></li>
                                    <li role="presentation"><a aria-controls="tab7" class="text-uppercase" data-toggle="tab" href="#tab7">Manc. Cloro</a></li>
                                    <li role="presentation"><a aria-controls="tab8" class="text-uppercase" data-toggle="tab" href="#tab8">Mill Flaw</a></li>
                                </ul>
                                <div class="tab-content padb">
                                    <div role="tabpanel" class="tab-pane active padtb " id="tab5">
                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="gridDefA" runat="server" DataKeyNames="idoperseg" CssClass="table table-hover table-striped" GridLines="None" OnRowCommand="gridDefA_RowCommand"
                                                    Width="100%" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="idoperseg" Visible="false" HeaderText="Codigo" />
                                                        <asp:TemplateField HeaderText="Operación" ItemStyle-Width="200">
                                                            <ItemTemplate>
                                                                <asp:Button ID="BtnDefecto" runat="server" Text='<%#Bind("Operacion") %>' CssClass="btn btn-primary btnD" CommandName="guardar" />
                                                                <asp:LinkButton CommandName="eliminar" ID="comandoRep" CssClass="btn btn-danger btnB" runat="server">
                                                                     <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Defecto">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" Text='<%#Eval("Cantidad") %>' Enabled="false" CssClass="form-control" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div role="tabpanel" class="tab-pane padtb " id="tab6">
                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="gridDefML" runat="server" DataKeyNames="idoperseg" CssClass="table table-hover table-striped" GridLines="None" OnRowCommand="gridDefML_RowCommand"
                                                    Width="100%" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="idoperseg" Visible="false" HeaderText="Codigo" />
                                                        <asp:TemplateField HeaderText="Operación" ItemStyle-Width="200">
                                                            <ItemTemplate>
                                                                <asp:Button ID="BtnDefecto" runat="server" Text='<%#Bind("Operacion") %>' CssClass="btn btn-success btnD" CommandName="guardar" />
                                                                <asp:LinkButton CommandName="eliminar" ID="comandoRep" CssClass="btn btn-danger btnB" runat="server">
                                                                     <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Defecto">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" Text='<%#Eval("Cantidad") %>' Enabled="false" CssClass="form-control" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div role="tabpanel" class="tab-pane padtb " id="tab7">
                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="gridDefMC" runat="server" DataKeyNames="idoperseg" CssClass="table table-hover table-striped" GridLines="None" OnRowCommand="gridDefMC_RowCommand"
                                                    Width="100%" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="idoperseg" Visible="false" HeaderText="Codigo" />
                                                        <asp:TemplateField HeaderText="Operación" ItemStyle-Width="200">
                                                            <ItemTemplate>
                                                                <asp:Button ID="BtnDefecto" runat="server" Text='<%#Bind("Operacion") %>' CssClass="btn btn-info btnD" CommandName="guardar" />
                                                                <asp:LinkButton CommandName="eliminar" ID="comandoRep" CssClass="btn btn-danger btnB" runat="server">
                                                                     <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Defecto">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" Text='<%#Eval("Cantidad") %>' Enabled="false" CssClass="form-control" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div role="tabpanel" class="tab-pane padtb " id="tab8">
                                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="gridDefMF" runat="server" DataKeyNames="idoperseg" CssClass="table table-hover table-striped" GridLines="None" OnRowCommand="gridDefMF_RowCommand"
                                                    Width="100%" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="idoperseg" Visible="false" HeaderText="Codigo" />
                                                        <asp:TemplateField HeaderText="Operación" ItemStyle-Width="200">
                                                            <ItemTemplate>
                                                                <asp:Button ID="BtnDefecto" runat="server" Text='<%#Bind("Operacion") %>' CssClass="btn btn-warning anchoBtn" Font-Size="11px" CommandName="guardar" />
                                                                <asp:LinkButton CommandName="eliminar" ID="comandoRep" CssClass="btn btn-danger btnB" runat="server">
                                                                     <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Defecto">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" Text='<%#Eval("Cantidad") %>' Enabled="false" CssClass="form-control" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </aside>
        </div>
    </form>
</body>
</html>
