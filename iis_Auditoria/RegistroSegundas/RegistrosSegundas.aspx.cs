﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroSegundas_RegistrosSegundas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (User.IsInRole("Segundas"))
                {
                    lbltotalDefectos.Text = "0";
                    txtfecha.Text = DateTime.Now.ToShortDateString();
                    txtnombre.Text =ExtraeNombre(User.Identity.Name.ToString());
                    CargaOperacionH();
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Response.Redirect("../Login.aspx");
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {

            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);
            SqlCommand cmd = new SqlCommand();
            cn.Open();

            cmd.Connection = cn;
            cmd.CommandText = "spdConstainPorder";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            cmd.Parameters.Add("@corte", SqlDbType.NVarChar);
            cmd.Parameters[cmd.Parameters.Count - 1].Value = pre;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porder = dt.Rows[i][0].ToString();
                obj.style = dt.Rows[i][2].ToString();
                obj.cantidad = int.Parse(dt.Rows[i][1].ToString());

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {     
        public string porder { get; set; }
        public string style { get; set; }
        public int cantidad { get; set; }
    }

    string ExtraeNombre (string userms)
    {
        var dt=DataAccess.Get_DataTable("select * from tbUsuarioSegunda where userms='" + userms + "'");

        if (dt.Rows.Count>0)
        {
            return dt.Rows[0][1].ToString();
        }
        else
        {
            return userms;
        }

       
    }

    DataTable cargaGridDef(string filtro)
    {
        return DataAccess.Get_DataTable("select idoperseg,Operacion,0 as Cantidad from tbOperacionSegunda where " + filtro + " order by Operacion");
    }

    void CargaOperacionH()
    {
        gridDefH.DataSource = cargaGridDef("Hoyos=1");
        gridDefH.DataBind();

        gridDefM.DataSource = cargaGridDef("Manchas=2");
        gridDefM.DataBind();

        gridDefT.DataSource = cargaGridDef("Tonos=3");
        gridDefT.DataBind();

        gridDefRep.DataSource = cargaGridDef("Apariencia=4");
        gridDefRep.DataBind();

        gridDefA.DataSource = cargaGridDef("Abrasion=5");
        gridDefA.DataBind();

        gridDefML.DataSource = cargaGridDef("ManchasL=6");
        gridDefML.DataBind();

        gridDefMC.DataSource = cargaGridDef("ManchasClo=7");
        gridDefMC.DataBind();

        gridDefMF.DataSource = cargaGridDef("MillFlaw=8");
        gridDefMF.DataBind();
    }

    DataTable cargaoperacionCorte(string corte, int idseccion, string seccion)
    {
        var dt = DataAccess.Get_DataTable(" select c1.idoperseg, c1.Operacion, isnull(c2.Cantidad, 0) as Cantidad from"
                                        + " (select idoperseg, Operacion from tbOperacionSegunda os where " + seccion + " = " + idseccion + ") as c1"
                                        + " left join"
                                        + " (select cs.idoperacion, sum(cs.Cantidad) as Cantidad from tbControlSegunda cs where corte = '" + corte + "' and cs.idseccion =" + idseccion
                                        + " group by cs.idoperacion) as c2"
                                        + " on c1.idoperseg = c2.idoperacion"
                                        + " order by c1.Operacion");

        return dt;
    }

    int cargaTotal(string corte)
    {
        var dt = DataAccess.Get_DataTable("select  sum(cantidad) as Total from tbControlSegunda  where corte = '" + corte + "'");

        if (dt.Rows.Count > 0)
        {
            return int.Parse(dt.Rows[0][0].ToString()==""? "0": dt.Rows[0][0].ToString());
        }
        else
        {
            return 0;
        }


    }

    protected void gridDefH_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (txtcantidad.Text == "" && txtPorder.Text == "" && txtstyle.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                return;
            }
            else
            {
                var def = 1;
                var corte = txtPorder.Text.TrimEnd();
                var respon = "False";

                if (string.IsNullOrEmpty(corte) == true)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                    return;
                }
                if (e.CommandName == "guardar")
                {
                    GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefH.DataKeys[indice].Value.ToString());

                    var estilo = txtstyle.Text.TrimEnd();
                    var cantidad = int.Parse(txtcantidad.Text);
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.GuadarSegundas(corte, estilo, cantidad, idoperacion, def, user);

                }
                if (e.CommandName == "eliminar")
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefH.DataKeys[indice].Value.ToString());
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.EliminarSegundas(corte,idoperacion, def,user);

                }

                if (respon == "OK")
                {
                    gridDefH.DataSource = cargaoperacionCorte(corte, def, "Hoyos");
                    gridDefH.DataBind();

                    lbltotalDefectos.Text = cargaTotal(txtPorder.Text.TrimEnd()).ToString();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
        }

    }

    protected void gridDefM_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (txtcantidad.Text == "" && txtPorder.Text == "" && txtstyle.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                return;
            }
            else
            {
                var def = 2;
                var corte = txtPorder.Text.TrimEnd();
                var respon = "False";

                if (string.IsNullOrEmpty(corte) == true)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                    return;
                }
                if (e.CommandName == "guardar")
                {
                    GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefM.DataKeys[indice].Value.ToString());

                    var estilo = txtstyle.Text.TrimEnd();
                    var cantidad = int.Parse(txtcantidad.Text);               
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.GuadarSegundas(corte, estilo, cantidad, idoperacion, def, user);
                   
                }
                if (e.CommandName == "eliminar")
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefM.DataKeys[indice].Value.ToString());
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.EliminarSegundas(corte, idoperacion, def, user);

                }

                if (respon == "OK")
                {
                    gridDefM.DataSource = cargaoperacionCorte(corte, def, "Manchas");
                    gridDefM.DataBind();

                    lbltotalDefectos.Text = cargaTotal(txtPorder.Text.TrimEnd()).ToString();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
        }
    }

    protected void gridDefT_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (txtcantidad.Text == "" && txtPorder.Text == "" && txtstyle.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                return;
            }
            else
            {
                var def = 3;
                var corte = txtPorder.Text.TrimEnd();
                var respon = "False";

                if (string.IsNullOrEmpty(corte) == true)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                    return;
                }
                if (e.CommandName == "guardar")
                {
                    GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefT.DataKeys[indice].Value.ToString());
 
                    var estilo = txtstyle.Text.TrimEnd();
                    var cantidad = int.Parse(txtcantidad.Text);
                   
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.GuadarSegundas(corte, estilo, cantidad, idoperacion, def, user);

                }
                if (e.CommandName == "eliminar")
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefT.DataKeys[indice].Value.ToString());
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.EliminarSegundas(corte, idoperacion, def, user);

                }

                if (respon == "OK")
                {
                    gridDefT.DataSource = cargaoperacionCorte(corte, def, "Tonos");
                    gridDefT.DataBind();

                    lbltotalDefectos.Text = cargaTotal(txtPorder.Text.TrimEnd()).ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
        }
    }

    protected void gridDefRep_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (txtcantidad.Text == "" && txtPorder.Text == "" && txtstyle.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                return;
            }
            else
            {
                var def = 4;
                var corte = txtPorder.Text.TrimEnd();
                var respon = "False";

                if (string.IsNullOrEmpty(corte) == true)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                    return;
                }
                if (e.CommandName == "guardar")
                {
                    GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefRep.DataKeys[indice].Value.ToString());

                    var estilo = txtstyle.Text.TrimEnd();
                    var cantidad = int.Parse(txtcantidad.Text);               
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.GuadarSegundas(corte, estilo, cantidad, idoperacion, def, user);

                }
                if (e.CommandName == "eliminar")
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefRep.DataKeys[indice].Value.ToString());
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.EliminarSegundas(corte, idoperacion, def, user);

                }

                if (respon == "OK")
                {
                    gridDefRep.DataSource = cargaoperacionCorte(corte, def, "Apariencia");
                    gridDefRep.DataBind();

                    lbltotalDefectos.Text = cargaTotal(txtPorder.Text.TrimEnd()).ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }

            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
        }
    }

    protected void gridDefA_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (txtcantidad.Text == "" && txtPorder.Text == "" && txtstyle.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                return;
            }
            else
            {
                var def = 5;
                var corte = txtPorder.Text.TrimEnd();
                var respon = "False";

                if (string.IsNullOrEmpty(corte) == true)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                    return;
                }
                if (e.CommandName == "guardar")
                {
                    GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefA.DataKeys[indice].Value.ToString());

                    var estilo = txtstyle.Text.TrimEnd();
                    var cantidad = int.Parse(txtcantidad.Text);
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.GuadarSegundas(corte, estilo, cantidad, idoperacion, def, user);

                }
                if (e.CommandName == "eliminar")
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefA.DataKeys[indice].Value.ToString());

                    var user = txtnombre.Text;

                    respon = OrderDetailDA.EliminarSegundas(corte, idoperacion, def, user);

                }

                if (respon == "OK")
                {
                    gridDefA.DataSource = cargaoperacionCorte(corte, def, "Abrasion");
                    gridDefA.DataBind();

                    lbltotalDefectos.Text = cargaTotal(txtPorder.Text.TrimEnd()).ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
        }
    }

    protected void gridDefML_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (txtcantidad.Text == "" && txtPorder.Text == "" && txtstyle.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                return;
            }
            else
            {
                var def = 6;
                var corte = txtPorder.Text.TrimEnd();
                var respon = "False";

                if (string.IsNullOrEmpty(corte) == true)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                    return;
                }
                if (e.CommandName == "guardar")
                {
                    GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefML.DataKeys[indice].Value.ToString());

                    var estilo = txtstyle.Text.TrimEnd();
                    var cantidad = int.Parse(txtcantidad.Text);
                 
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.GuadarSegundas(corte, estilo, cantidad, idoperacion, def, user);
        
                }
                if (e.CommandName == "eliminar")
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefML.DataKeys[indice].Value.ToString());

                    var user = txtnombre.Text;

                    respon = OrderDetailDA.EliminarSegundas(corte, idoperacion, def, user);

                }

                if (respon == "OK")
                {
                    gridDefML.DataSource = cargaoperacionCorte(corte, def, "ManchasL");
                    gridDefML.DataBind();

                    lbltotalDefectos.Text = cargaTotal(txtPorder.Text.TrimEnd()).ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }

            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
        }
    }

    protected void gridDefMC_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (txtcantidad.Text == "" && txtPorder.Text == "" && txtstyle.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                return;
            }
            else
            {
                var def = 7;
                var corte = txtPorder.Text.TrimEnd();
                var respon = "False";

                if (string.IsNullOrEmpty(corte) == true)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                    return;
                }
                if (e.CommandName == "guardar")
                {
                    GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefMC.DataKeys[indice].Value.ToString());

                    var estilo = txtstyle.Text.TrimEnd();
                    var cantidad = int.Parse(txtcantidad.Text);
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.GuadarSegundas(corte, estilo, cantidad, idoperacion, def, user);
                   
                }
                if (e.CommandName == "eliminar")
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefMC.DataKeys[indice].Value.ToString());
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.EliminarSegundas(corte, idoperacion, def, user);


                }

                if (respon == "OK")
                {
                    gridDefMC.DataSource = cargaoperacionCorte(corte, def, "ManchasClo");
                    gridDefMC.DataBind();

                    lbltotalDefectos.Text = cargaTotal(txtPorder.Text.TrimEnd()).ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
        }
    }

    protected void gridDefMF_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (txtcantidad.Text == "" && txtPorder.Text == "" && txtstyle.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                return;
            }
            else
            {
                var def = 8;
                var corte = txtPorder.Text.TrimEnd();
                var respon = "False";

                if (string.IsNullOrEmpty(corte)==true)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                    return;
                }
                if (e.CommandName == "guardar")
                {
                    GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefMF.DataKeys[indice].Value.ToString());
                   
                    var estilo = txtstyle.Text.TrimEnd();
                    var cantidad = int.Parse(txtcantidad.Text);
                   
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.GuadarSegundas(corte, estilo, cantidad, idoperacion, def, user);

                }
                if (e.CommandName == "eliminar")
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    int idoperacion = int.Parse(gridDefMF.DataKeys[indice].Value.ToString());
                    var user = txtnombre.Text;

                    respon = OrderDetailDA.EliminarSegundas(corte, idoperacion, def, user);

                }

                if (respon == "OK")
                {
                    gridDefMF.DataSource = cargaoperacionCorte(corte, def, "MillFlaw");
                    gridDefMF.DataBind();

                    lbltotalDefectos.Text = cargaTotal(txtPorder.Text.TrimEnd()).ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
        }
    }

    protected void BtnLimpiar_Click(object sender, EventArgs e)
    {
        try
        {
            txtcantidad.Text = ""; txtPorder.Text = ""; txtstyle.Text = "";lbltotalDefectos.Text = "0";
            CargaOperacionH();
        }
        catch (Exception)
        {

        }
    }

    protected void VerificarDefetos_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtcantidad.Text == "" && txtPorder.Text == "" && txtstyle.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "camposVacios();", true);
                return;
            }
            else
            {
                string corte = txtPorder.Text.TrimEnd();

                gridDefH.DataSource = cargaoperacionCorte(corte, 1, "Hoyos");
                gridDefH.DataBind();

                gridDefM.DataSource = cargaoperacionCorte(corte, 2, "Manchas");
                gridDefM.DataBind();

                gridDefT.DataSource = cargaoperacionCorte(corte, 3, "Tonos");
                gridDefT.DataBind();

                gridDefRep.DataSource = cargaoperacionCorte(corte, 4, "Apariencia");
                gridDefRep.DataBind();

                gridDefA.DataSource = cargaoperacionCorte(corte, 5, "Abrasion");
                gridDefA.DataBind();

                gridDefML.DataSource = cargaoperacionCorte(corte, 6, "ManchasL");
                gridDefML.DataBind();

                gridDefMC.DataSource = cargaoperacionCorte(corte, 7, "ManchasClo");
                gridDefMC.DataBind();

                gridDefMF.DataSource = cargaoperacionCorte(corte, 8, " MillFlaw");
                gridDefMF.DataBind();

                lbltotalDefectos.Text = cargaTotal(txtPorder.Text.TrimEnd()).ToString();


            }
        }
        catch (Exception)
        {

        }
    }
}