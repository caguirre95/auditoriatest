﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CS : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FruitName");
            dt.Rows.Add("Apple");
            dt.Rows.Add("Mango");
            dt.Rows.Add("Banana");
            dt.Rows.Add("Orange");
            dt.Rows.Add("Pineapple");
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        this.RegisterPostBackControl();
    }

    private void RegisterPostBackControl()
    {
        foreach (GridViewRow row in GridView1.Rows)
        {
            LinkButton lnkFull = row.FindControl("lnkFull") as LinkButton;
            ScriptManager.GetCurrent(this).RegisterPostBackControl(lnkFull);
        }
    }

    protected void PartialPostBack(object sender, EventArgs e)
    {
        string fruitName = ((sender as LinkButton).NamingContainer as GridViewRow).Cells[0].Text;
        string message = "alert('Partial PostBack: You clicked " + fruitName + "');";
        ScriptManager.RegisterClientScriptBlock(sender as Control, this.GetType(), "alert", message, true);
    }

    protected void FullPostBack(object sender, EventArgs e)
    {
        string fruitName = ((sender as LinkButton).NamingContainer as GridViewRow).Cells[0].Text;
        string message = "alert('Full PostBack: You clicked " + fruitName + "');";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", message, true);
    }
}