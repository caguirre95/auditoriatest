﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MedidaEspecial1.aspx.cs" Inherits="MedidaEspecial1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

       <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                 
                    $.ajax({
                        url: "Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);
                    $('#<%=txtstyle.ClientID%>').val(ui.item.style);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });

        //window.onload = function() {
        //    imprimirValor();
        //}

        //function imprimirValor(){
        //    var select = document.getElementById("feedingHay");
        //    var options=document.getElementsByTagName("option");
        //    console.log(select.value);
        //    console.log(options[select.value-1].innerHTML)
        //}
    </script>

    <script type="text/javascript">
        function alertseleccionarBultoOTalla() {
            swal({
                title: 'Debe seleccionar una talla del combo!',
                type: 'info'
            });
        }
        function alertSeleccionarMedida() {
            swal({
                title: 'Debe seleccionar una seccion de medida!',
                type: 'info'
            });
        }
        function alertElegirOtroBultoXS() {
            swal({
                title: 'Elija la misma talla con una secuencia mayor!',
                type: 'info'
            });
        }
    </script>

    <link href="jquery-ui.css" rel="stylesheet" />
   <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>
     <link href="css/pagination.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <br />
    <div class="container">
        <div class="">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Measurement suggestion </strong></h3>
                </div>
                <div class="panel-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                             <%-- <asp:HiddenField ID="HiddenFielduser" runat="server" />--%>
                            <div class="col-lg-12">
                                <div class="col-lg-8">
                                    <div class="col-lg-6">
                                        Corte:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" OnClick="btnbuscar_Click" CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" required class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                        Bultos:
                                     <asp:DropDownList ID="DropDownList2" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </div>
                                    <div class="col-lg-6">
                                        Estilo:
                                     <asp:TextBox runat="server" CssClass="form-control" placeholder="Estilo" Font-Size="14px" ID="txtstyle" />
                                        <asp:HiddenField ID="hfidPorder" runat="server" />
                                        <asp:HiddenField ID="hfiunidades" runat="server" />
                                    </div>
                                    <div class="col-lg-12" style="padding-top: 15px;">
                                        <a href="#ventanapro" class="btn btn-warning" data-toggle="modal" style="text-decoration: none">Reparar</a>
                                        <asp:Button ID="Button34" OnClick="Button34_Click" runat="server" CssClass="btn btn-info" Text="Limpiar" />
                                    </div>
                                    <div class="col-lg-12" style="padding-top: 15px;">
                                        <div class="btn btn-primary">
                                            Unidades Medidas:
                                            <asp:Label CssClass="badge" ID="lblunidades" Text="text" runat="server" />
                                        </div>
                                        <div class="btn btn-info">
                                            Asignaste:
                                            <asp:Label CssClass="badge" ID="lblseccion" Text="text" runat="server" />
                                            <asp:Label CssClass="badge" ID="lblmedida" Text="text" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Secciones de Medida
                                        </div>
                                        <div class="panel-body">
                                            <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="col-lg-12">
                                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text='+2"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button2" runat="server" OnClick="Button1_Click" Text='+1 7/8"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button3" runat="server" OnClick="Button1_Click" Text='+1 3/4"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button4" runat="server" OnClick="Button1_Click" Text='+1 5/8"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button5" runat="server" OnClick="Button1_Click" Text='+1 1/2"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button6" runat="server" OnClick="Button1_Click" Text='+1 3/8"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button7" runat="server" OnClick="Button1_Click" Text='+1 1/4"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button8" runat="server" OnClick="Button1_Click" Text='+1 1/8"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button16" runat="server" OnClick="Button1_Click" Text='+1/8"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button15" runat="server" OnClick="Button1_Click" Text='+1/4"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button14" runat="server" OnClick="Button1_Click" Text='+3/8"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button13" runat="server" OnClick="Button1_Click" Text='+1/2"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button12" runat="server" OnClick="Button1_Click" Text='+5/8"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button11" runat="server" OnClick="Button1_Click" Text='+3/4"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button10" runat="server" OnClick="Button1_Click" Text='+7/8"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button9" runat="server" OnClick="Button1_Click" Text='+1"' CssClass="btn btn-success withB" />
                                    </div>
                                    <div class="col-lg-12">
                                        <asp:Button ID="Button17" runat="server" OnClick="Button1_Click" Text='+/-0"' CssClass="btn btn-primary withB" />
                                    </div>
                                    <div class="col-lg-12">
                                        <asp:Button ID="Button33" runat="server" OnClick="Button1_Click" Text='-1/8"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button32" runat="server" OnClick="Button1_Click" Text='-1/4"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button31" runat="server" OnClick="Button1_Click" Text='-3/8"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button30" runat="server" OnClick="Button1_Click" Text='-1/2"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button29" runat="server" OnClick="Button1_Click" Text='-5/8"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button28" runat="server" OnClick="Button1_Click" Text='-3/4"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button27" runat="server" OnClick="Button1_Click" Text='-7/8"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button26" runat="server" OnClick="Button1_Click" Text='-1"' CssClass="btn btn-success withB" />
                                        <asp:Button ID="Button18" runat="server" OnClick="Button1_Click" Text='-2"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button19" runat="server" OnClick="Button1_Click" Text='-1 7/8"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button20" runat="server" OnClick="Button1_Click" Text='-1 3/4"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button21" runat="server" OnClick="Button1_Click" Text='-1 5/8"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button22" runat="server" OnClick="Button1_Click" Text='-1 1/2"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button23" runat="server" OnClick="Button1_Click" Text='-1 3/8"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button24" runat="server" OnClick="Button1_Click" Text='-1 1/4"' CssClass="btn btn-danger withB" />
                                        <asp:Button ID="Button25" runat="server" OnClick="Button1_Click" Text='-1 1/8"' CssClass="btn btn-danger withB" />
                                    </div>

                                    <%-- <div class="col-lg-12">
                                        <input type="button" value='+2"' name='+2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 7/8"' name='+1 7/8"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 3/4"' name='+1 3/4"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 5/8"' name='+1 5/8"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 1/2"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 3/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 1/4"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    </div>
                                    <div class="col-lg-12">
                                        <input type="button" value='+/- 0"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-primary withB" />
                                        <input type="button" value='Next' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-warning withB2" />
                                        <input type="button" value='Previus' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-warning withB2" />
                                    </div>
                                    <div class="col-lg-12">
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                        <input type="button" value='+1 2/8"' name='+1 1/2"' onclick="changeText(this.name)" class="btn btn-danger withB" />
                                    </div>--%>
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>

    <style>
        .withB {
            width: 70px !important;
            height: 70px !important;
            display: inline-block;
            margin: 3px;
        }

        .withB2 {
            width: 130px !important;
            height: 70px !important;
            display: inline-block;
            margin: 3px;
        }

      
    </style>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="ventanapro">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h1>Correccion de Medidas</h1>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanelproveedor" runat="server">
                        <ContentTemplate>
                            <div style="color: #006699; font-size: 14px">

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Seleccione el bulto:</label>
                                    <asp:DropDownList ID="DropDownListbultos" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Seleccione la Seccion de Medida:</label>
                                    <asp:DropDownList ID="DropDownListSecciones" CssClass="form-control" OnSelectedIndexChanged="DropDownListSecciones_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                                </div>

                                <div style="text-align: center">
                                    <asp:Label ID="msg" runat="server"></asp:Label>
                                </div>
                                <hr />

                                <div>
                                    <asp:GridView ID="GridViewunidades" DataKeyNames="idMedidaMaster" runat="server" OnRowCommand="GridViewunidades_RowCommand" OnRowDataBound="GridViewunidades_RowDataBound"
                                        CssClass="table table-hover table-striped" GridLines="None" Width="100%" OnPageIndexChanging="GridViewunidades_PageIndexChanging" AutoGenerateColumns="False" AllowPaging="true" PageSize="10">
                                        <Columns>
                                           
                                            <asp:TemplateField Visible="false">
                                                <HeaderTemplate>
                                                    idMM
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblmedidaMaster" runat="server" Text='<%#Bind("idMedidaMaster") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false">
                                                <HeaderTemplate>
                                                    idM
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblidmedida" runat="server" Text='<%#Bind("idMedida") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="bulto_Secuencia" HeaderText=" Bulto " />
                                            <asp:BoundField DataField="Medida" HeaderText="Medida Asignada" />
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Nueva Medida
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DropDownListNuevaMedida" CssClass="form-control" runat="server"></asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-primary" CommandName="Clear" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Button ID="btneditar" runat="server" Text="Editar" CssClass="btn btn-success" CommandName="editar" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="Primero" LastPageText="Ultimo" />
                                        <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <%--<aside class="modal fade" id="ventanapro">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="col-lg-12 panel">
                    <article class="modal-header">
                    </article>
                    <article class="modal-body">
                        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11 panel panel-primary panel-footer " style="margin-left: 2.5%">
                            <button id="cerrar" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                            <aside class=" panel-body">
                                <div style="text-align: center" class="page-header">
                                    <h1>Correccion de Medidas</h1>
                                </div>


                            </aside>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </aside>--%>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

