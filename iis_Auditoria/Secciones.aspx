﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Secciones.aspx.cs" Inherits="SelectAudit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function result() {
            swal({
                title: 'Ups!',
                text: "Acceso Denegado!!",
                type: 'info'
            });
        }
    </script>

    <%--<script type="text/javascript">



readCookie();

function readCookie() {
     if (document.cookie == "") {
	writeCookie();
        alertMessage();
     } else {
	var the_cookie = document.cookie;
	the_cookie = unescape(the_cookie);
	the_cookie_split = the_cookie.split(";");
	for (loop=0;loop<the_cookie_split.length;loop++) {
		var part_of_split = the_cookie_split[loop];
		var find_name = part_of_split.indexOf("nfti_date")
		if (find_name!=-1) {
			break;
		} // Close if
	} // Close for
	if (find_name==-1) {
		writeCookie();
	} else {
		var date_split = part_of_split.split("=");
		var last = date_split[1];
		last=fixTheDate(last);
		alert (" - Bienvenido - Tu última visita a esta página fue: "+last);
		writeCookie();
	} // Close if (find_name==-1)
      }
} // Close function readCookie()


function writeCookie() {
     var today = new Date();
     var the_date = new Date("December 31, 2023");
     var the_cookie_date = the_date.toGMTString();
     var the_cookie = "nfti_date="+escape(today);
     var the_cookie = the_cookie + ";expires=" + the_cookie_date;
     document.cookie=the_cookie
}

function alertMessage(){
     alert ("Bienvenido\aquí agregas un mensaje de bienvenida.")
}

function fixTheDate(date) {
     var split = date.split(" ");
     var fix_the_time = split[3].split(":")
     var hours = fix_the_time[0]
     if (hours>=12) {
	var ampm="PM"
     } else {
	var ampm="AM"
     }
     if (hours > 12) {
	hours = hours-12
     }
     var new_time = hours+":"+fix_the_time[1]+" "+ampm
     var new_date = split[0]+" "+split[1]+", "+split[2]+" at "+new_time+", "+split[5]
     return new_date;
}

</script>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2 class="quitPadH">
        <asp:Label ID="lbl" runat="server" Text="Audit System"></asp:Label></h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <style type="text/css">
        .box_effect {
            height: 250px;
            width: 180px;
            padding-top: 15px;         
            text-align: center;
            padding: 5px;
            border-width: 160px;        
            margin: auto;
            opacity: 0.8;
            border: 1px solid rgba(0,0,0,0.2);         
        }
           
        .te {
            text-decoration: none;
        }

        .im {
            z-index: -1;
        }

        .mt {
            margin-left: 60px;
            margin-top: 15px;
            height: 130px;
            width: 130px;
        }

        .te:hover {
            color: white !important;
            text-decoration: none;
            outline-style: none;
        }
     
    </style>


    <div class="container" style="padding: 0">
       <asp:HiddenField ID="HiddenFieldidAuditoria" runat="server" />
        <asp:HiddenField ID="HiddenFieldidSeccion" runat="server" />
        <div class="col-lg-3" id="tr" runat="server">
            <div class="box_effect">
                <asp:LinkButton ID="lnksec1" runat="server" CssClass="te" OnClick="lnksec1_Click">
                    <div>
                        <img src="img/Trasero_New.png" alt="Trasero" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight:bold;color:rgba(0,0,0,0.6);">TRASERO</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>
        <div class="col-lg-3" id="de" runat="server">
            <div class="box_effect">
                <asp:LinkButton ID="lnksec2" runat="server" CssClass="te" OnClick="lnksec2_Click">
                    <div>
                        <img src="img/Delantero_new.png" alt="Delantero" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight:bold;color:rgba(0,0,0,0.6);">DELANTERO</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>
         <div class="col-lg-3" id="e1" runat="server">
            <div class="box_effect">
                <asp:LinkButton ID="lnksec3" runat="server" CssClass="te" OnClick="lnksec3_Click">
                    <div>
                        <img src="img/Ensamble_1_new.png" alt="Ensamble I" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight:bold;color:rgba(0,0,0,0.6);">ENSAMBLE I</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>
         <div class="col-lg-3" id="e2" runat="server">
            <div class="box_effect">
                <asp:LinkButton ID="LNKSEC4" runat="server" CssClass="te" OnClick="LNKSEC4_Click">
                    <div>
                        <img src="img/Ensamble_2_new.png" alt="Ensamble II" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight:bold;color:rgba(0,0,0,0.6);">ENSAMBLE II</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>
         <div class="col-lg-3" id="en" runat="server">
            <div class="box_effect">
                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="te" OnClick="LNKSEC5_Click">
                    <div>
                        <img src="img/pant_Chaq.jpg" alt="Ensamble" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight:bold;color:rgba(0,0,0,0.6);">ENSAMBLE</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>
         <div class="col-lg-3" id="fe" runat="server">
            <div class="box_effect">
                <asp:LinkButton ID="LinkButton2" runat="server" CssClass="te" OnClick="LNKSEC6_Click">
                    <div>
                        <img src="img/Chaqueta.jpg" alt="Ensamble" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight:bold;color:rgba(0,0,0,0.6);text-transform: uppercase;" >Frente y Espalda</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>
         <div class="col-lg-3" id="pf" runat="server">
            <div class="box_effect">
                <asp:LinkButton ID="LinkButton3" runat="server" CssClass="te" OnClick="LNKSEC7_Click">
                    <div>
                        <img src="img/Chaqueta.jpg" alt="Ensamble" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight:bold;color:rgba(0,0,0,0.6);text-transform: uppercase;">Preliminar Frente</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>
         <div class="col-lg-3" id="pfr" runat="server">
            <div class="box_effect">
                <asp:LinkButton ID="LinkButton4" runat="server" CssClass="te" OnClick="LNKSEC8_Click">
                    <div>
                        <img src="img/Chaqueta.jpg" alt="Ensamble" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight:bold;color:rgba(0,0,0,0.6);text-transform: uppercase;">Preliminar Forro</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>

           <div class="col-lg-3" id="pre" runat="server">
            <div class="box_effect">
                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="te" OnClick="LNKSEC9_Click">
                    <div>
                        <img src="img/Ensamble_2_new.png" alt="Preliminar" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight:bold;color:rgba(0,0,0,0.6);text-transform: uppercase;">Preliminar</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>
    </div>
    
    <style>
        /*Ejemplo 5*/
        .ejemplo-5 img {
            transform: scaleY(1);
            transition: all 0.7s ease-in-out;
        }

        .ejemplo-5 .mascara {
            background-color: rgba(119, 80, 21, 0.9);
            transition: all 0.5s linear;
            opacity: 0;
        }

        .ejemplo-5 h2 {
            border-bottom: 1px solid rgba(255, 255, 255, 0.6);
            background: transparent;
            margin: 5px 5px 0px 5px;
            transform: scale(0);
            color: #fff;
            transition: all 0.5s linear;
            opacity: 0;
        }

        .ejemplo-5 p {
            color: #fff;
            opacity: 0;
            transform: scale(0);
            transition: all 0.5s linear;
        }

        .ejemplo-5 a.link {
            opacity: 0;
            transform: scale(0);
            transition: all 0.5s linear;
            background-color: rgba(119, 80, 21, 0.9);
        }

        .ejemplo-5:hover img {
            transform: scale(10);
            opacity: 0;
        }

        .ejemplo-5:hover .mascara {
            opacity: 1;
        }

        .ejemplo-5:hover h2,
        .ejemplo-5:hover p,
        .ejemplo-5:hover a.link {
            transform: scale(1);
            opacity: 1;
        }
    </style>
</asp:Content>

