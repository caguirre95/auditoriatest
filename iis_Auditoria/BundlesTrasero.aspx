﻿<%@ Page Title="Sistema de Auditores" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BundlesTrasero.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="scripts/footable.min.js"></script>
    <script type="text/javascript">
        function successalert() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
    </script>

    <style type="text/css">
        .centergrid {
            margin: 0 auto;
        }
    </style>

    <script type="text/javascript" src="scripts/footable.min.js"></script>

    <script type="text/javascript">
        function successalert() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
    </script>

    <script type="text/javascript">
        var timeout;
        function scheduleGridUpdate(ASPxGridView1) {
            window.clearTimeout(timeout);
            timeout = window.setTimeout(
                function () { ASPxGridView1.Refresh(); },
                2000
            );
        }
        function grid_Init(s, e) {
            scheduleGridUpdate(s);
        }
        function grid_BeginCallback(s, e) {
            window.clearTimeout(timeout);
        }
        function grid_EndCallback(s, e) {
            scheduleGridUpdate(s);
        }
    </script>

    <script type="text/javascript">
        var timeout;
        function scheduleGridUpdate(ASPxGridView2) {
            window.clearTimeout(timeout);
            timeout = window.setTimeout(
                function () { ASPxGridView2.Refresh(); },
                2000
            );
        }
        function grid_Init(s, e) {
            scheduleGridUpdate(s);
        }
        function grid_BeginCallback(s, e) {
            window.clearTimeout(timeout);
        }
        function grid_EndCallback(s, e) {
            scheduleGridUpdate(s);
        }
    </script>

    <script type="text/javascript">
        //function pageLoad() { // this gets fired when the UpdatePanel.Update() completes
        //    ReBindMyStuff();
        //}
        //function ReBindMyStuff() { // create the rebinding logic in here
        //    $('[id*=Repeater]').footable();
        //}
    </script>


    <%-- <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="font-awesome/fonts.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2 class="quitPadH">
        <asp:Label ID="lbl" runat="server" Text="Bundles Check Off"></asp:Label></h2>
    <br />
    <div class="boton_search">
        <%-- <div class="col-md-12">--%>
        <div>
        </div>
        <%-- </div>--%>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="margin-left: 10px;">
        <%-- <label style="">From Production Order:</label>--%>
        <label id="frprododr">
            <asp:Label ID="lblorder1" Visible="false" runat="server" Text=""></asp:Label></label>
        <%-- <label>To </label>--%>
        <%-- <label id="lbtoprod">Production Order:</label>--%>
        <label id="toprododr">
            <asp:Label ID="lblorder2" runat="server" Visible="false" Text=""></asp:Label></label>
        <%-- <strong>All Dates</strong>--%>
        <br />


    </div>

    <div class="container-fluid">
        <div class="col-lg-12 centrar">
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/img/system_search.png" OnClick="lnkNew_Click" /><asp:LinkButton ID="lnkNew" runat="server" OnClick="lnkNew_Click">Nueva Busqueda</asp:LinkButton>
        </div>
    </div>

    <div class="container " style="padding-bottom: 5px;">
        <div class="col-lg-2">
            <strong>Customer:</strong>
            <asp:Label ID="lbclient" runat="server" CssClass="main">
                <asp:Label ID="lblcustomer" runat="server" Text=""></asp:Label></asp:Label>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container-fluid info_ord">
        <div class="container " style="padding-bottom: 5px;">
            <div class="col-lg-2 col-lg-offset-1">
                <asp:Label ID="Label5" runat="server" Text="Corte#:"></asp:Label>
                <asp:TextBox ID="txtporder" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label6" runat="server" Text="Estilo"></asp:Label>
                <asp:TextBox ID="txtstyle" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label7" runat="server" Text="Descripcion"></asp:Label>
                <asp:TextBox ID="txtdescrip" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label8" runat="server" Text="Cantidad"></asp:Label>
                <asp:TextBox ID="txtcantidad" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label9" runat="server" Text="Bultos"></asp:Label>
                <asp:TextBox ID="txtbultos" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
        </div>
    </div>

    <div class="">
        <div class="container">

            <%--   <div id="Ausencias" runat="server">
                 <a href="#ventanapro" class="btn btn-success center-block" data-toggle="modal" style="text-decoration: none; display:none">Registrar Ausencias</a>
        </div>--%>

            <div class="col-lg-12 centrar" style="height: 35px">
                <h4>
                    <asp:Label ID="lblbio" runat="server" Text="" CssClass="label label-primary"></asp:Label>
                </h4>
            </div>
            <%--  <asp:Label ID="lblbio" runat="server" Text="" Style="margin-top: 10px; text-align: center; font-size: 18px;" Width="100%" BackColor="#5D7B9D" ForeColor="White"></asp:Label>--%>

            <asp:Label ID="lblidbio" runat="server" Text="" Visible="false"></asp:Label>
            <div id="Grid" runat="server">
                <asp:GridView ID="GridBultos" OnRowCommand="GridBultos_RowCommand" OnRowDataBound="GridBultos_RowDataBound" AutoGenerateColumns="false" DataKeyNames="Id_Bundle"
                    CssClass="table table-hover table-striped" GridLines="None" Width="100%" runat="server">
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblidbundle" runat="server" Text='<%#Bind("Id_Bundle") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NSeq" HeaderText="Seq" />
                        <asp:BoundField DataField="Bld" HeaderText="Bulto" />
                        <asp:BoundField DataField="Size" HeaderText="Talla" />
                        <asp:BoundField DataField="Color" HeaderText="Color" />
                        <asp:BoundField DataField="Quantity" HeaderText="Cantidad" />
                        <asp:BoundField DataField="Muestreo" HeaderText="Muestreo" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                    <asp:ListItem Value="0" Text="Rechazado"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Aprobado"></asp:ListItem>
                                </asp:RadioButtonList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Button ID="btnGuardarTodos" OnClick="btnGuardarTodos_Click" CssClass="btn btn-primary" runat="server" Text="Aprobar Todos" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton CommandName="Editar" ID="comando" runat="server">
                                     <i class="fa fa-check-circle marApRe" aria-hidden="true"></i>         
                                     <i class="fa fa-times-circle" aria-hidden="true"></i>
                                </asp:LinkButton>
                                <asp:LinkButton CommandName="Reparar" ID="comandoRep" CssClass="btn btn-success" runat="server">
                                     <i class="glyphicon glyphicon-wrench" aria-hidden="true"></i> Reparar
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <h4 style="text-align: center">Sin datos para mostrar</h4>
                    </EmptyDataTemplate>
                </asp:GridView>

                <asp:Label ID="lblnoRows" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>

     
    <%-- <div class="modal fade" tabindex="-1" role="dialog" id="ventanapro">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h1>Control Asistencia</h1>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanelproveedor" runat="server">
                        <ContentTemplate>
                            <div style="color: #006699; font-size: 14px">
                                <div class="form-group">                                
                                    <asp:Button ID="btnguardar" runat="server" Text="Registrar" CssClass="belize-hole-flat-button"
                                        OnClick="btnguardar_Click" />
                                </div>

                                <div style="text-align: center">
                                    <asp:Label ID="msg" runat="server"></asp:Label>
                                </div>
                                <hr />

                                <div>
                                    <asp:GridView ID="ControlAsistencia" runat="server" CssClass="table table-hover table-striped" GridLines="None" Width="100%"
                                        AutoGenerateColumns="False" >
                                        <Columns>
                                            <asp:TemplateField HeaderText="No">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblid" Width="50px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "id")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="id" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblidoperario" Width="50px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "id_operario")%>'></asp:Label>
                                                      <asp:Label ID="lblidopOper" Width="50px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "id_operarioOpe")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Operario">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbloperario" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Nombre")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Asistencia">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="check_asistencia" Checked='<%#DataBinder.Eval(Container.DataItem, "Estado")%>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="Primero" LastPageText="Ultimo" />
                                        <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>--%>


    <%--  <dx:ASPxPopupControl ID="EditarAcciones" runat="server" EnableTheming="True" ClientInstanceName="EditarAcciones"
        HeaderText="Historial" Modal="True" Theme="MetropolisBlue"
        AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides"
        Width="950px" CloseAction="CloseButton" ScrollBars="Auto" MaxHeight="500px">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <dx:ASPxPanel ID="ASPxPanel4" runat="server">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div class="col-md-12" style="margin-top: 5px">
                                <div class="col-md-6">
                                    <div style="margin-left: 250px">
                                        <dx:ASPxButton ID="ASPxbtnap" runat="server" Theme="Mulberry" Text="Ver Aprobados" AutoPostBack="false" ClientInstanceName="verApr">
                                            <ClientSideEvents Click="function(s, e) { GridAprobado.SetVisible(true);
                                     verrech.SetClientVisible(false);
                                     verApr.SetClientVisible(false);
                                     regres.SetClientVisible(true);
                                     save.SetClientVisible(true);
                                      }" />
                                        </dx:ASPxButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <dx:ASPxButton ID="ASPxbtnre" runat="server" Theme="Mulberry" Text="Ver Rechazados" AutoPostBack="false" ClientInstanceName="verrech">
                                            <ClientSideEvents Click="function(s, e) { GridRechazados.SetVisible(true);
                                     verrech.SetClientVisible(false);
                                     verApr.SetClientVisible(false);
                                     regres.SetClientVisible(true);
                                     save.SetClientVisible(true);
                                      }" />
                                        </dx:ASPxButton>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: 5px">
                                <div style="margin-left: 400px">
                                    <dx:ASPxButton ID="btnRegresar" runat="server" Theme="Mulberry" Text="Regresar" AutoPostBack="false" ClientVisible="false" ClientInstanceName="regres">
                                        <ClientSideEvents Click="function(s, e) { 
                                    GridRechazados.SetVisible(false);
                                    GridAprobado.SetVisible(false);
                                    verApr.SetVisible(true);
                                    verrech.SetVisible(true);
                                    regres.SetVisible(false);
                                    save.SetClientVisible(false);
                                     }" />
                                    </dx:ASPxButton>
                                </div>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
                <dx:ASPxPanel ID="ASPxPanel3" runat="server" ClientVisible="false" ClientInstanceName="GridAprobado">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent2" runat="server">
                            <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" CssClass="centergrid" Width="100%"
                                EnableTheming="True" KeyFieldName="Id_Bundle" Theme="Moderno" ClientInstanceName="ASPxGridView1">
                                <SettingsSearchPanel Visible="True" />
                                <Columns>
                                    <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0" Width="10px">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Visible="false" FieldName="Id_Bundle" VisibleIndex="1" Width="30px">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="NSeq" FieldName="NSeq" VisibleIndex="3" Width="70px">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="FechaAprobado" FieldName="FechaAprobado" VisibleIndex="4" Width="70px">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="HoraAprobado" FieldName="HoraAprobado" VisibleIndex="5" Width="70px">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsLoadingPanel Mode="ShowOnStatusBar" />
                            </dx:ASPxGridView>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
                <dx:ASPxPanel ID="ASPxPanel1" runat="server" ClientVisible="false" ClientInstanceName="GridRechazados">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent3" runat="server">
                            <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" CssClass="centergrid" Width="100%"
                                EnableTheming="True" KeyFieldName="Id_BundleRejected" ClientInstanceName="ASPxGridView2"
                                Theme="Moderno">
                                <SettingsSearchPanel Visible="True" />
                                <Columns>
                                    <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0" Width="10px">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="Id_BundleRejected" Visible="false" Width="30px">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="NSeq" FieldName="NSeq" VisibleIndex="1" Width="30px">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Area" FieldName="Area" VisibleIndex="3" Width="70px">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Defecto" FieldName="Defecto" VisibleIndex="3" Width="70px">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Operario" FieldName="nombre" VisibleIndex="4" Width="70px">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="FechaRecibido" FieldName="FechaRecibido" VisibleIndex="5" Width="70px">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="HoraRecibido" FieldName="HoraRecibido" VisibleIndex="6" Width="70px">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
                <div style="text-align: center">
                   
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>--%>
</asp:Content>
