﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Data.Sql;
using SistemaAuditores.DataAccess;

public partial class SelectAudit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MESS.Visible = false;
            ip.Visible = false;
            bb.Visible = false;
            ai.Visible = false;
            ae.Visible = false;
            ac.Visible = false;
            mn.Visible = false;
            aem.Visible = false;
            af.Visible = false;
            emp.Visible = false;
            insf.Visible = false;
            aei.Visible = false;

            if (Page.User.Identity.IsAuthenticated == true)
            {
                if (Page.User.IsInRole("Administrator") || Page.User.IsInRole("Reports"))
                {
                    try
                    {
                        Response.Redirect("/admin/SelectReportType.aspx", false);
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                    }
                }

                if (Page.User.IsInRole("Packer"))
                {
                    try
                    {
                        string user = HttpContext.Current.User.Identity.Name;
                        string query = "select rtrim(modulo) from UserPlanta where nameMemberShip='" + user + "'";
                        DataTable acceso = DataAccess.Get_DataTable(query);
                        string a = acceso.Rows[0][0].ToString();

                        if (a == "pack")
                        {
                            Response.Redirect("/ReporteEmpaque/EmpaqueUsuario.aspx", false);
                           
                        }
                        else if (a == "packS")
                        {
                            Response.Redirect("/ReporteEmpaque/EmpaqueSupervisor.aspx", false);
                        }

                       
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                    }
                }

                if (Page.User.IsInRole("PackList") || Page.User.IsInRole("PackAdmon"))
                {
                    try
                    {
                        Response.Redirect("/Handler.ashx", false);
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                    }
                }

                if (Page.User.IsInRole("Inspector"))
                {
                    try
                    {
                        acceso();
                    }
                    catch (Exception ex)
                    {
                        string mess = ex.Message;
                    }
                }

                if (Page.User.IsInRole("Segundas"))
                {
                    try
                    {
                        string nombre = User.Identity.Name.ToString().ToLower().Trim();
                        if (nombre.Equals("empaque1"))
                        {
                            Response.Redirect("/RegistroEmpaqueDef/DefectosEmpaque.aspx", false);
                        }
                        else
                        Response.Redirect("/RegistroSegundas/RegistrosSegundas.aspx", false);
                        
                    }
                    catch (Exception ex)
                    {
                        string mess = ex.Message;
                    }
                }
            }
            else
            {
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }

        }

    }

    void acceso()
    {

        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
        string userId = mu.ProviderUserKey.ToString();

        DataTable dt_Inspector = DataAccess.Get_DataTable(" select ia.idAuditoria,i.id_inspector"
                                                        + " from Inspector i"
                                                        + " join tbInspectorAuditoria ia on i.id_inspector = ia.idInspector"
                                                        + " where i.UserId = '" + userId + "' and ia.Estado = 1 ");

        if (dt_Inspector.Rows.Count >= 1)
        {
            var medidas = (LinkButton)this.Master.FindControl("LinkButton6");
            medidas.Visible = false;

            hdnidinspector.Value = dt_Inspector.Rows[0][1].ToString();

            for (int i = 0; i < dt_Inspector.Rows.Count; i++)
            {
                int idaudi = int.Parse(dt_Inspector.Rows[i][0].ToString());

                switch (idaudi)
                {
                    case 1:
                        ip.Visible = true;
                        break;

                    case 2:
                        bb.Visible = true;
                        break;

                    case 3:
                        ai.Visible = true;
                        break;

                    case 4:
                        ae.Visible = true;
                        break;

                    case 5:
                        ac.Visible = true;
                        break;

                    case 6:
                        aem.Visible = true;
                        mn.Visible = true;
                        break;

                    case 7:
                        emp.Visible = true;
                        break;

                    case 8:
                        af.Visible = true;
                        mn.Visible = true;
                        break;

                    case 9:
                        insf.Visible = true;
                        break;

                    case 11:
                        aei.Visible = true;
                        break;

                    default:
                        break;
                }

            }

        }
        else
        {
            MESS.Visible = true;

            string user = HttpContext.Current.User.Identity.Name;

            string query = "select rol from tblusermedida where userms ='" + user + "'";

            DataTable dt_rol = DataAccess.Get_DataTable(query);

            if (dt_rol.Rows.Count > 0)
            {
                if (dt_rol.Rows[0][0].ToString().Contains("modulo4"))
                {
                    Response.Redirect("/Medida.aspx");
                }
                else if (dt_rol.Rows[0][0].ToString().Contains("especial1"))
                {
                    Response.Redirect("/MedidaEspecial1.aspx");
                }
                else if (dt_rol.Rows[0][0].ToString().Contains("especial2"))
                {
                    Response.Redirect("/MedidaNew.aspx", false);
                }
                else if (dt_rol.Rows[0][0].ToString().Contains("intex"))
                {
                    Response.Redirect("/MedidasNewIntex1.aspx");
                }
                else if (dt_rol.Rows[0][0].ToString().Contains("muestra"))
                {
                    Response.Redirect("/MedidaMuestra.aspx");
                }
                else if (dt_rol.Rows[0][0].ToString().Contains("bultoabulto"))
                {
                    Response.Redirect("/MedidasNewBultoaBulto.aspx");
                }
                else if (dt_rol.Rows[0][0].ToString().Contains("final"))
                {
                    Response.Redirect("/MedidasNewFinal.aspx");
                }
                else if (dt_rol.Rows[0][0].ToString().Contains("embarque"))
                {
                    Response.Redirect("/MedidaNewEmbarque.aspx");
                }
                else if (dt_rol.Rows[0][0].ToString().Contains("iSecado"))
                {
                    Response.Redirect("/SalidaSecadora.aspx");
                } // Acceso a reparaciones
                else if (dt_rol.Rows[0][0].ToString().Contains("Reproceso"))
                {
                    Response.Redirect("/Procesos/ReprocesoEmpaque.aspx");
                } // Acceso a HeatTransfer
                else if (dt_rol.Rows[0][0].ToString().Contains("HeatTransfer"))
                {
                    Response.Redirect("/Procesos/HeatTransfer2.aspx");
                }
                else if (dt_rol.Rows[0][0].ToString().Contains("AuditoriaS"))
                {
                    Response.Redirect("/Intex/AuditoriaSecadoIntex.aspx");
                }
                else if (dt_rol.Rows[0][0].ToString().Contains("embIn"))
                {
                    Response.Redirect("/InspecEmbarqueIntex.aspx");
                }
            }
            else
            {
                //  Response.Redirect("/Default.aspx");
            }
        }

    }

    protected void lnk_Click(object sender, EventArgs e)
    {

        Response.Redirect("Secciones.aspx?idAuditoria=1&idLinea=0&idInspector=" + hdnidinspector.Value);

    }

    protected void lnnkbundle_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchIL.aspx?idAuditoria=2&idLinea=0&idSeccion=0");

    }

    protected void lnkship_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchIL.aspx?idAuditoria=3&idLinea=0&idSeccion=0");

    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchIL.aspx?idAuditoria=4&idLinea=0&idSeccion=0");
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchIL.aspx?idAuditoria=5&idLinea=0&idSeccion=0");
    }

    protected void lnkmedidas_Click(object sender, EventArgs e)
    {

        string sql = " select distinct ta.idAuditoria from Inspector i join tbInspectorAuditoria ia on i.id_inspector=ia.idInspector join tbTipoAuditoria ta on ta.idAuditoria=ia.idAuditoria"
                   + " join tbInspectorLinea il on i.id_inspector=il.idInspector join Linea l on il.idLinea=l.id_linea"
                   + " where i.id_inspector=" + hdnidinspector.Value;// + " and ta.idAuditoria = 6";

        DataTable dt_Inspector = DataAccess.Get_DataTable(sql);

        if (dt_Inspector.Rows.Count > 0)
        {
            for (int i = 0; i < dt_Inspector.Rows.Count; i++)
            {
                var idaudi = Convert.ToInt16(dt_Inspector.Rows[i][0].ToString());
                if (idaudi == 6)

                    Response.Redirect("MedidasNewEmbarque.aspx"); //otros clientes
                    
                else if (idaudi == 8)

                    Response.Redirect("MedidasNewFinal.aspx"); //otros clientes
            }
          
        }         
        else
            Response.Redirect("MedidaNew.aspx");//cliente FEchheimer
    }

    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchIL.aspx?idAuditoria=6&idLinea=0&idSeccion=0");
    }

    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchIL.aspx?idAuditoria=8&idLinea=0&idSeccion=0");
    }

    protected void lnkemp_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchIL.aspx?idAuditoria=7&idLinea=0&idSeccion=0");
    }

    protected void lnkinspf_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchIL.aspx?idAuditoria=9&idLinea=0&idSeccion=0");
    }

    protected void lnkaein_Click(object sender, EventArgs e)
    {
        Response.Redirect("SearchIL.aspx?idAuditoria=11&idLinea=0&idSeccion=0");
    }
}