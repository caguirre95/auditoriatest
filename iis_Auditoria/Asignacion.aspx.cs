﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Asignacion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Page.User.IsInRole("Administrator") || Page.User.IsInRole("PackAdmon"))
                {
                    cargaLinea();
                }
                else
                {
                    //FormsAuthentication.SignOut();
                    Response.Redirect("/NotFound.aspx");
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    void cargaLinea()
    {
        string queryBundle = "  select id_linea as Id, numero as Linea from Linea where numero not like '%-%' and Estado = 1 order by CONVERT(decimal,numero) asc";
        DataTable dt_Bundle = DataAccess.Get_DataTable(queryBundle);

        DropDownListlinea.DataSource = dt_Bundle;
        DropDownListlinea.DataTextField = "Linea";
        DropDownListlinea.DataValueField = "Id";
        DropDownListlinea.DataBind();
        DropDownListlinea.Items.Insert(0, "Select...");

    }

    protected void btnGuadar_Click(object sender, EventArgs e)
    {
        try
        {
            bool resp = false;
            string NOMBRE = HttpContext.Current.User.Identity.Name.ToLower();

            if (NOMBRE == "makeup" || NOMBRE == "planta1" || NOMBRE == "planta03" || NOMBRE == "roc6" || NOMBRE == "administrator" || NOMBRE == "admon1" || NOMBRE == "admon2" || NOMBRE == "admon3" || NOMBRE == "admon4" || NOMBRE == "adminsnr")
            {

                if (hfidPorder.Value != "" && DropDownListlinea.SelectedItem.Text != "Select..." && drpdesc.SelectedItem.Text != "Select..." && drpwash.SelectedItem.Text != "Select...")
                {
                    string r = "";
                    if (NOMBRE == "administrator" || NOMBRE == "planta1" || NOMBRE == "roc6")
                    {
                        r = OrderDetailDA.Asignacion1(Convert.ToInt32(hfidPorder.Value), Convert.ToInt32(DropDownListlinea.SelectedValue),drpdesc.SelectedValue, Convert.ToInt16(drpwash.SelectedValue));
                    }
                    else
                    {
                        r = OrderDetailDA.Asignacion(Convert.ToInt32(hfidPorder.Value), Convert.ToInt32(DropDownListlinea.SelectedValue), drpdesc.SelectedValue, Convert.ToInt16(drpwash.SelectedValue));
                    }

                    if (r == "OK")
                    {
                        string res = OrderDetailDA.asignacionlineaUser(Convert.ToInt32(hfidPorder.Value), Convert.ToInt32(DropDownListlinea.SelectedValue), NOMBRE);

                        if (res == "OK")
                        {
                            resp = true;
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "errorUser();", true);
                    return;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "restrincion();", true);
                return;
            }

            if (resp)
            {
                txtPorder.Text = "";
                cargaLinea();
                hfidPorder.Value = "";
                drpdesc.SelectedValue = 0.ToString();
                drpwash.SelectedValue = "S";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "correctoG();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "errorG();", true);
            }

        }
        catch (Exception ex)
        {
            string a = ex.Message;
          //  throw;
        }

    }
}