﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MedidasNewBultoaBulto : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {

                if (User.Identity.IsAuthenticated == true)
                {
                    hdnusuario.Value = Context.User.Identity.Name.ToLower();

                    var dt = DataAccess.Get_DataTable("select rtrim(ltrim(rol)) as rol from tblUserMedida where userms='" + hdnusuario.Value.TrimEnd() + "' and Estado=1");

                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            hdnrol.Value = dt.Rows[0][0].ToString();
                        }

                    }
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Response.Redirect("Login.aspx");
                }
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre, string user)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select top 20 p.Id_Order,p.POrder,s.Style,s.id_style,p.id_linea2 from POrder p join Style s on p.Id_Style=s.Id_Style"
                                           +" join( select IdLinea from tblUserLineaMedida where nameMemberShip = '" + user + "') as c1 on p.Id_Linea2 = c1.IdLinea"
                                           +" where p.POrder like '%" + pre + "%'", cn);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][1].ToString();
                obj.style = dt.Rows[i][2].ToString();
                obj.idstyle = int.Parse(dt.Rows[i][3].ToString());
                obj.idlinea = int.Parse(dt.Rows[i][4].ToString());

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetTallasXIdCorte(string id)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = "spdGetTallasUniXIdCorte";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@id", SqlDbType.Int);
            cmd.Parameters[cmd.Parameters.Count - 1].Value = Convert.ToInt32(id);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][1].ToString());
                obj.porder = dt.Rows[i][0].ToString();
                //obj.style = dt.Rows[i][2].ToString();

                list.Add(obj);
            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetEspecificaciones(string id, string rol)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            //SqlCommand cmd = new SqlCommand(" select pmr.id,pmr.idPuntoMedida,pm.NombreMedida " +
            //    "from tbPuntosMedidasRolLinea pmr join tblPuntosMedida pm on pmr.idPuntoMedida=pm.idPuntosM "
            //                               + " where pm.activo = 1 and pmr.idlinea=" + id + " and pmr.rol='" + rol + "' order by pmr.secuencia asc", cn);

            SqlCommand cmd = new SqlCommand(" select " +
                "sp.id," +
                "sp.idPuntoMedida," +
                "sp.tolMaxValor," +
                "sp.tolMinValor," +
                "pm.NombreMedida "
                                           + " FROM EstiloPuntoMedidaTolBLB sp"
                                          + " join Style s on sp.idEstilo = s.Id_Style"
                                          + " join tblPuntosMedida pm on sp.idPuntoMedida = pm.idPuntosM"
                                          + " where pm.activo = 1 and s.Id_Style =" + id, cn);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.idpmedida = int.Parse(dt.Rows[i][1].ToString());
                obj.value2 = float.Parse(dt.Rows[i][2].ToString());
                obj.value3 = float.Parse(dt.Rows[i][3].ToString());
                obj.porder = dt.Rows[i][4].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    public static int GetTotalCorteXTalla(int idporder, string talla)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select isnull(sum(cantidad),0) as unidades from tbMedidasBultoaBultoAL where idPorder=" + idporder + " and  rtrim(size)=rtrim('" + talla + "')", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();


            return dt == null ? 0 : Convert.ToInt16(dt.Rows[0][0].ToString());

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return 0;
        }
    }

    [WebMethod]
    public static string GuardarMasterBLB(string idPorder, string idEstilo, string size, string estado, string usuario, int unidadesXtalla, string color, string rol,string xmlstring)
    {
        try
        {

            return OrderDetailDA.saveObjMasterBLB(Convert.ToInt32(idPorder), Convert.ToInt32(idEstilo), size, Convert.ToBoolean(estado), usuario, unidadesXtalla, color,rol, xmlstring);

        }
        catch (Exception)
        {

            return "";
        }

    }

    [WebMethod]
    public static int GuardarDetailBLB(int idPuntoMedida, float medidaFlotante, string medidaTexto, string estado, string estado2, string id)
    {
        try
        {
            return OrderDetailDA.saveObjDetailBLB(idPuntoMedida, medidaFlotante, medidaTexto, estado, estado2, id);
        }
        catch (Exception)
        {
            return 0;
        }

    }

    public class porderClass
    {
        public int idorder { get; set; }
        public int idstyle { get; set; }
        public string porder { get; set; }
        public string style { get; set; }
        public int idpmedida { get; set; }
        public float value1 { get; set; }
        public float value2 { get; set; }
        public float value3 { get; set; }
        public int idlinea { get; set; }
    }

}