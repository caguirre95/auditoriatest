﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BultoaBultoRechazoInspFinal : System.Web.UI.Page
{
    class listas
    {
        public string id { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (User.Identity.IsAuthenticated == true)
            {
                if (!IsPostBack)
                {

                    var customer = (string)Request.QueryString["customer"];
                    var style = (string)Request.QueryString["style"];
                    var idlinea = (string)Request.QueryString["idLinea"];
                    var idbulto = (string)Request.QueryString["idBundle"];

                    MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
                    string userId = mu.ProviderUserKey.ToString();

                    DataTable dtlinea = new DataTable();
                    dtlinea = DataAccess.Get_DataTable("select numero from Linea where id_linea=" + idlinea);

                    DataTable dtinspector = new DataTable();
                    dtinspector = DataAccess.Get_DataTable("select id_inspector, nombre from Inspector where UserId = '" + userId + "'");

                    DataTable dtporder = new DataTable();
                    dtporder = DataAccess.Get_DataTable("select p.POrder,b.Quantity from Bundle b join POrder p on b.Id_Order=p.Id_Order where b.Id_Bundle=" + idbulto);

                    var guid = Guid.NewGuid();

                    lblcustomerval.Text = customer;
                    lblestiloval.Text = style;
                    lineaval.Text = dtlinea.Rows[0][0].ToString();
                    lblinspectorval.Text = dtinspector.Rows[0][1].ToString();
                    lblfechaval.Text = DateTime.Now.ToShortDateString();
                    lblCorteval.Text = dtporder.Rows[0][0].ToString();

                    //Agregar Muestreo
                    var quantity = int.Parse(dtporder.Rows[0][1].ToString());
                    txtMuestreo.Text = quantity.ToString(); 

                    hdnguid.Value = guid.ToString();
                    Session["guid"] = guid.ToString();
                    hdfidinspector.Value = dtinspector.Rows[0][0].ToString();

                    carga();

                    cargagrid();

                }
            }
            else
            {
                FormsAuthentication.SignOut();
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + ex.Message + "','" + tipo + "');", true);
        }
    }

    void carga()
    {

        DataTable dtDefecto = new DataTable();
        dtDefecto = DataAccess.Get_DataTable("select idDefecto, Defecto from tbDefectos where Estado = 1 order by Defecto asc");

        DataTable dtPosicion = new DataTable();
        dtPosicion = DataAccess.Get_DataTable("select idPosicion, Posicion as Codigo from tbPosicion where Estado = 1 order by Codigo asc");

        Session["defectos"] = dtDefecto;
        Session["posicion"] = dtPosicion;

    }

    void cargagrid()
    {
        List<listas> lista = new List<listas>();

        for (int i = 1; i <= 12; i++)
        {
            var obj = new listas();
            obj.id = i.ToString();
            lista.Add(obj);
        }

        gridAddDefectos.DataSource = lista;
        gridAddDefectos.DataBind();
    }

    protected void btnreparar_Click(object sender, EventArgs e)
    {
        try
        {
            var idbulto = (string)Request.QueryString["idBundle"];
            var resp = OrderDetailDA.GuardarReparacionBulto(Convert.ToInt32(idbulto));

            if (resp == "OK")
            {
                string message = "Exito!!!";
                string tipo = "success";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + message + "','" + tipo + "');", true);
                retornar();
            }
            else
            {
                string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
                string tipo = "error";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + message + "','" + tipo + "');", true);
            }

        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + ex.Message + "','" + tipo + "');", true);
        }
    }

    protected void gridAddDefectos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddldef = (DropDownList)e.Row.FindControl("ddldefectos");
                DropDownList ddlpos = (DropDownList)e.Row.FindControl("ddlposicion");

                var dt1 = (DataTable)Session["defectos"];
                var dt2 = (DataTable)Session["posicion"];

                ddldef.DataSource = dt1;
                ddldef.DataTextField = "Defecto";
                ddldef.DataValueField = "idDefecto";
                ddldef.DataBind();
                ddldef.Items.Insert(0, "");

                ddlpos.DataSource = dt2;
                ddlpos.DataTextField = "Codigo";
                ddlpos.DataValueField = "idPosicion";
                ddlpos.DataBind();
                ddlpos.Items.Insert(0, "");

            }
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + ex.Message + "','" + tipo + "');", true);

        }
    }

    protected void gridAddDefectos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            var guid = (string)Session["guid"];
            if (hdnguid.Value == guid)
            {
                bool errorBan = false;
                if (e.CommandName == "Guardar")
                {
                    GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                    DropDownList ddldef = (DropDownList)row.FindControl("ddldefectos");
                    DropDownList ddlpos = (DropDownList)row.FindControl("ddlposicion");
                    TextBox txtcantidad = (TextBox)row.FindControl("txtcantidaDefectos");
                    var idlinea = (string)Request.QueryString["idLinea"];
                    var idbulto = (string)Request.QueryString["idBundle"];

                    var iddef = ddldef.SelectedValue;
                    var idpos = ddlpos.SelectedValue;

                    if (txtcantidad.Text != "" && ddldef.SelectedValue != "0" && ddlpos.SelectedValue != "0" && hdfidinspector.Value != "" && hdnguid.Value != "" && txtMuestreo.Text != string.Empty && txtrechazo.Text != string.Empty)
                    {
                        try
                        {
                            //muestreo,guid
                            var resp = OrderDetailDA.GuardarRechazoInsFinal(Convert.ToInt32(idbulto), Convert.ToInt16(hdfidinspector.Value), Convert.ToInt16(idlinea), Convert.ToInt16(txtMuestreo.Text), Convert.ToInt16(txtrechazo.Text), hdnguid.Value);

                            if (resp > 0)
                            {
                                OrderDetailDA.GuardarDetRechazoInspFinal(resp, Convert.ToInt16(iddef), Convert.ToInt16(idpos), Convert.ToInt16(txtcantidad.Text), CheckBox1.Checked);
                                txtrechazo.Text = "";
                            }
                            else
                            {
                                errorBan = true;
                                string mess = "Error En Base de Datos";
                                string tipo = "error";
                                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + mess + "','" + tipo + "');", true);
                                //error en base de datos
                            }

                        }
                        catch (Exception ex)
                        {
                            errorBan = true;
                            // errorBan = true;
                            //string mess = "Error En Base de Datos";
                            string tipo = "error";
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + ex.Message + "','" + tipo + "');", true);

                            //string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
                            //ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
                            return;
                        }
                    }
                    else
                    {
                        errorBan = true;
                        string mess = "Por favor ingrese la cantidad o seleccione un defecto u operacion!";
                        string tipo = "info";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
                        //  ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertaMess(" + mess + ");", true);

                        return;
                    }

                }
                else if (e.CommandName == "GuardarTodos")
                {
                    var idlinea = (string)Request.QueryString["idLinea"];
                    var idbulto = (string)Request.QueryString["idBundle"];

                    if (hdfidinspector.Value != "" && hdnguid.Value != "" && txtrechazo.Text != string.Empty)
                    {
                        var resp = OrderDetailDA.GuardarRechazoInsFinal(Convert.ToInt32(idbulto), Convert.ToInt16(hdfidinspector.Value), Convert.ToInt16(idlinea), Convert.ToInt16(txtMuestreo.Text), Convert.ToInt16(txtrechazo.Text), hdnguid.Value);

                        if (resp > 0)
                        {
                            txtrechazo.Text = "";
                            for (int i = 0; i < gridAddDefectos.Rows.Count; i++)
                            {
                                try
                                {
                                    var row = gridAddDefectos.Rows[i];

                                    DropDownList ddldef = (DropDownList)row.FindControl("ddldefectos");
                                    DropDownList ddlpos = (DropDownList)row.FindControl("ddlposicion");
                                    TextBox txtcantidad = (TextBox)row.FindControl("txtcantidaDefectos");

                                    if (txtcantidad.Text != "" && ddldef.SelectedValue != "0" && ddlpos.SelectedValue != "0")
                                    {
                                        var iddef = ddldef.SelectedValue;
                                        var idpos = ddlpos.SelectedValue;

                                        OrderDetailDA.GuardarDetRechazoInspFinal(resp, Convert.ToInt16(iddef), Convert.ToInt16(idpos), Convert.ToInt16(txtcantidad.Text), CheckBox1.Checked);
                                    }


                                }
                                catch (Exception ex)
                                {
                                    errorBan = true;

                                }

                            }
                        }
                        else
                        {
                            errorBan = true;
                            string message = "Ha Ocurrido un Error, Reintente nuevamente!";
                            string tipo = "error";
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + message + "','" + tipo + "');", true);
                        }
                    }
                    else
                    {
                        errorBan = true;
                        string mess = "Por favor ingrese la cantidad o seleccione un defecto u operacion!";
                        string tipo = "info";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
                    }
                }
                

                cargagrid();


                if (errorBan)
                {

                    string message = "Ha Ocurrido un Error, Reintente nuevamente!";
                    string tipo = "error";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + message + "','" + tipo + "');", true);
                }
                else
                {
                    string mess = "Exito!";
                    string tipo = "success";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);


                    var _guid = Guid.NewGuid();
                    Session["guid"] = _guid.ToString();
                }
            }
        }
        catch (Exception ex)
        {

            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);


        }
    }

    void retornar()
    {
        var idOrder = (string)Request.QueryString["idOrder"];
        var idAuditoria = (string)Request.QueryString["idAuditoria"];
        var idLinea = (string)Request.QueryString["idLinea"];


        Response.Redirect("BultoaBultoInspFinal.aspx?idOrder=" + idOrder + "&idAuditoria=" + idAuditoria + "&idLinea=" + idLinea, false);
    }

    void nuevaBusqueda()
    {
        var idorder = (string)Request.QueryString["idAuditoria"];
        Response.Redirect("SearchIL.aspx?idAuditoria=1&idLinea=0");
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            retornar();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            retornar();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void lnkNew_Click(object sender, EventArgs e)
    {
        try
        {
            nuevaBusqueda();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            nuevaBusqueda();
        }
        catch (Exception)
        {

           // throw;
        }
    }
}