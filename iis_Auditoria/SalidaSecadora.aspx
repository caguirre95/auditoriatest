﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SalidaSecadora.aspx.cs" Inherits="SalidaSecadora" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "SalidaSecadora.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porderclient: item.porderclient,
                                    estilo: item.estilo,
                                    totalcorte: item.totalcorte,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porderclient);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porderclient);
                    $('#<%=hfestilo.ClientID%>').val(ui.item.estilo);
                    $('#<%=hftotalc.ClientID%>').val(ui.item.totalcorte);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porderclient + "</a>").appendTo(ul);
            };
        })
    </script>

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                timer: 500,
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Campos Vacios, llenelos Correctamente !',
                timer: 800,
                type: 'info'
            });
        }
        function alertClave() {
            swal({
                title: 'Ocurrio un evento Inesperado, intentelo Nuevamente !',
                timer: 1000,
                type: 'warning'
            });
        }
        function ClaveCorrect() {
            swal({
                title: 'Clave Correcta, Accion Realizada !',
                type: 'info'
            });
        }

        function LimpiarFocus() {
            limpiar();
        }

        function limpiar() {

            $('#<%=txtPorder.ClientID%>').val('');
            $('#<%=hfestilo.ClientID%>').val('');
            $('#<%=hftotalc.ClientID%>').val('');

        }

    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function alertMasterE(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }
    </script>

    <style>
        .mar_t {
            margin: 10px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="">
            <div class="panel panel-primary" style="margin-top: 10px;">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Unidades Salientes de Secadora</strong></h3>
                </div>
                <div class="panel-body">
                    Corte
                    <asp:TextBox ID="txtPorder" runat="server" onfocus="LimpiarFocus()" CssClass="form-control" placeholder="Buscar Corte"></asp:TextBox>

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:MultiView runat="server" ID="multiview">
                                <asp:View runat="server">

                                    <asp:HiddenField ID="HiddenFielduser" runat="server" />
                                    <asp:HiddenField ID="hfestilo" runat="server" />
                                    <asp:HiddenField ID="hftotalc" runat="server" />

                                    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12" style="margin-top: 20px">
                                        <asp:Button ID="btnagregar" runat="server" Text="Agregar Corte" CssClass="form-control btn btn-info" OnClick="btnagregar_Click" />
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12" style="margin-top: 20px">
                                        <asp:Button runat="server" ID="btnvercorte" Text="Ver Corte" CssClass="form-control btn btn-primary" OnClick="btnvercorte_Click" />
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12" style="margin-top: 20px">
                                        <asp:Button runat="server" ID="btnver" Text="Ver Todos" CssClass="form-control btn btn-default" OnClick="btnver_Click" />
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="overflow-x: auto">
                                        <hr />
                                        <style>
                                            .w {
                                                width: 100%;
                                                min-width: 700px;
                                            }
                                        </style>
                                        <asp:GridView ID="gridEnvios" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" DataKeyNames="POrderClient" CssClass="table table-hover table-responsive table-striped"
                                            AutoGenerateColumns="false" GridLines="None" OnRowCommand="gridEnvios_RowCommand" AllowPaging="true" OnPageIndexChanging="gridEnvios_PageIndexChanging" PageSize="20" PagerSettings-Mode="NumericFirstLast">
                                            <Columns>
                                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        CORTE
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPorder" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("POrderClient") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        ESTILO 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblestilo" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("Estilo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField Visible="false" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        CANTIDADCORTE 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcantidad" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Totalcorte") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        CANTIDADCORTE RECIBIDA 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcantidadrecb" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("TotalEnviado") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        UNIDADES SECADAS
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtunidades" Enabled="false" Width="100px" Font-Size="Medium" CssClass="form-control" Text='<%#Bind("Secadas") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        UNIDADES
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtunits" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" MaxLength="4" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>                                                    
                                                    <ItemTemplate>                                                                                                                
                                                        <asp:Label ID="lbltexto1" runat="server" Text="Digitar" Font-Bold="true"></asp:Label>
                                                        <asp:LinkButton Text="" ID="lnkEnvio" CssClass="btn btn-info" runat="server" CommandName="AgregarCT"><i class="glyphicon glyphicon-indent-left"> </i> </asp:LinkButton>                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>                                                    
                                                    <ItemTemplate>                                                                                                                                                                        
                                                        <asp:Label ID="lbltexto" runat="server" Text="Bulto" Font-Bold="true"></asp:Label>
                                                        <asp:LinkButton Text="" ID="lnkenviotxt" CssClass="btn btn-success" runat="server" CommandName="AgregarCE"><i class="glyphicon glyphicon-object-align-bottom"> </i> </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                            </Columns>
                                        </asp:GridView>

                                        <%-- </div>--%>
                                    </div>

                                </asp:View>
                                <asp:View runat="server">
                                </asp:View>
                            </asp:MultiView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

