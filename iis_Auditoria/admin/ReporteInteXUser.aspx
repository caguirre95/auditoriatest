﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteInteXUser.aspx.cs" Inherits="admin_ReporteInteXUser"  %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraReports.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">    
    <div class="container">  
            <div class="col-lg-10 col-lg-6 col-lg-4 col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Select Date</h3>
                    </div>
                    <div class="panel-body">
                        <dx:ASPxDateEdit ID="txtfecha1" Theme="Metropolis"  Height="35px" runat="server" EditFormat="Custom" Width="100%">
                           
                        </dx:ASPxDateEdit>
                       
                        <br />
                        <dx:ASPxDateEdit ID="txtfecha2" Theme="Metropolis" Height="35px" runat="server" EditFormat="Custom" Width="100%">
                            
                        </dx:ASPxDateEdit>
                        <br />
                        <asp:DropDownList ID="DropDownListUser" CssClass="form-control" runat="server"></asp:DropDownList>
                        <%-- <br />
                        <asp:DropDownList ID="DropDownListCorte" runat="server"></asp:DropDownList>--%>
                        <hr />
                        <asp:Button ID="btnGenerar" OnClick="btnGenerar_Click" runat="server" Text="Generar" CssClass="btn btn-primary form-control" />
                    </div>
                </div>
            </div>
     
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Report</h3>
                </div>
                <div class="panel-body">
                    <dx:ASPxWebDocumentViewer ID="ASPxWebDocumentViewer1" Visible="true" runat="server">
                    </dx:ASPxWebDocumentViewer>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

