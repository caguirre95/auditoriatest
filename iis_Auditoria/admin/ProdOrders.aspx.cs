﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SistemaAuditores.DataAccess;
using System.Data;
using Hex_Converter;

public partial class admin_ProdOrders : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Save_POrders()
    {
        try
        {
            DataTable dt = new DataTable();
            string query = "select * from ProductionPervasive";
            dt = DataAccess.Get_DataTable(query);
            int i = 0;
            for (i = 0; i <= dt.Rows.Count - 1; i++)
            {
                POrderDA.SavePOrders(Convert.ToInt16(dt.Rows[i].ItemArray[1]), Convert.ToInt16(dt.Rows[i].ItemArray[3]), Convert.ToString(dt.Rows[i].ItemArray[5]), Convert.ToString(dt.Rows[i].ItemArray[6]), Convert.ToInt16(dt.Rows[i].ItemArray[7]), Convert.ToInt16(dt.Rows[i].ItemArray[8]));
            }

            string message = "alert('Actualizacion exitosa');";
            ScriptManager.RegisterClientScriptBlock(this.Panel, this.GetType(), "alert", message, true);
        }
        catch (Exception ex)
        {
            string message = "alert('" + ex.Message + "!');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        }
    }

    protected void img_actualizarpo_Click(object sender, ImageClickEventArgs e)
    {
        //Save_POrders();
    }
    protected void lnk_UpdateAllPO_Click(object sender, EventArgs e)
    {
        //Save_POrders();
    }
    protected void img_addnewpo_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("/admin/AddPOrder.aspx");
    }
    protected void lnk_UpdateNewpo_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin/AddPOrder.aspx");
    }
    protected void img_showallpo_Click(object sender, ImageClickEventArgs e)
    {
        Load_Data_Local();
    }
    protected void lnkshowpo_Click(object sender, EventArgs e)
    {
        Load_Data_Local();
    }

    public void Load_Data_Local()
    {
        DataTable dt_POrders = new DataTable();
        dt_POrders = DataAccess.Get_DataTable("select Id_Order, Id_Cliente, Id_Style, POrder, Description, Quantity, Bundles from POrder");

        GridView1.DataSource = dt_POrders;
        GridView1.DataBind();
    }

    public void Load_Data_LocalPV()
    {
        DataTable dt_POrders = new DataTable();
        dt_POrders = DataAccess.Get_DataTable("select IdProd, Id_Cliente, Id_Estilo, POrder, Description, Quantity, Bundles from ProductionPervasive");

        GridView1.DataSource = dt_POrders;
        GridView1.DataBind();
    }

    protected void lnkshowPV_Click(object sender, EventArgs e)
    {
        Load_Data_LocalPV();
    }
    protected void img_showallPV_Click(object sender, ImageClickEventArgs e)
    {
        Load_Data_LocalPV();
    }
    protected void imgupdIdCliente_Click(object sender, ImageClickEventArgs e)
    {
        UpdateIdCliente();
    }
    
    public void UpdateIdCliente()
    {
        try
        {
            DataTable dt_production_pervasive = new DataTable();
            dt_production_pervasive = DataAccess.Get_DataTable("select * from ProductionPervasive");
            DataTable dt_customer = new DataTable();
            dt_customer = DataAccess.Get_DataTable("select * from Production_Customer");
            for (int i = 0; i <= dt_production_pervasive.Rows.Count - 1; i++)
            {
                for (int j = 0; j <= dt_customer.Rows.Count - 1; j++)
                {
                    if (Convert.ToString(dt_production_pervasive.Rows[i].ItemArray[2]).Trim() == Convert.ToString(dt_customer.Rows[j].ItemArray[1]).Trim())
                    {
                        dt_production_pervasive.Rows[i]["Id_Cliente"] = dt_customer.Rows[j].ItemArray[0];
                        break;
                    }
                }
            }

            // dt_production_pervasive.Columns["Id_Cliente"].SetOrdinal(1);
            GridView1.DataSource = dt_production_pervasive;
            GridView1.DataBind();

            for (int i = 0; i <= dt_production_pervasive.Rows.Count - 1; i++)
            {
                DataAccess.UpdatePOrder(Convert.ToInt16(dt_production_pervasive.Rows[i]["Id_Cliente"]), Convert.ToInt16(dt_production_pervasive.Rows[i][0]));
            }
            string message = "alert('Actualizacion exitosa!');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        }
        catch (Exception ex)
        {
            string message = "alert('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        }
    }

    public void Update_IdStyle()
    {
        try
        {
            DataTable dt_production_pervasive = new DataTable();
            dt_production_pervasive = DataAccess.Get_DataTable("select * from ProductionPervasive");
            DataTable dt_styles = new DataTable();
            dt_styles = DataAccess.Get_DataTable("select * from Production_Styles");
            for (int i = 0; i <= dt_production_pervasive.Rows.Count - 1; i++)
            {
                for (int j = 0; j <= dt_styles.Rows.Count - 1; j++)
                {
                    if (Convert.ToString(dt_production_pervasive.Rows[i].ItemArray[4]).Trim() == Convert.ToString(dt_styles.Rows[j].ItemArray[1]).Trim())
                    {
                        dt_production_pervasive.Rows[i]["Id_Estilo"] = dt_styles.Rows[j].ItemArray[0];
                        break;
                    }
                }
            }

            GridView1.DataSource = dt_production_pervasive;
            GridView1.DataBind();

            for (int i = 0; i <= dt_production_pervasive.Rows.Count - 1; i++)
            {
                DataAccess.UpdatePOrderStyles(Convert.ToInt16(dt_production_pervasive.Rows[i]["Id_Estilo"]), Convert.ToInt16(dt_production_pervasive.Rows[i][0]));
            }

            string message = "alert('Actualizacion exitosa!');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        }
        catch (Exception ex)
        {
            string message = "alert('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        }
    }

    protected void imgupdIdStyle_Click(object sender, ImageClickEventArgs e)
    {
        Update_IdStyle();
    }
    protected void lnk_IdStyle_Click(object sender, EventArgs e)
    {
        Update_IdStyle();
    }
    protected void lnk_IdCliente_Click(object sender, EventArgs e)
    {
        UpdateIdCliente();
    }
    protected void lnk_importdata_Click(object sender, EventArgs e)
    {
        Importar_Datos();
    }
    
    public void Importar_Datos()
    {
        DataTable Datos_Pervasive = new DataTable();
        Datos_Pervasive = DataAccessPervasive.Get_DataTableNewOrdersV10();
            //.Get_DTPOrderbydate(fecha, customer);
        string query = "select * from Cliente"; 
        DataTable dt_customer = DataAccess.Get_DataTable(query);
        string query2 = "select * from Style";
        DataTable dt_styles = DataAccess.Get_DataTable(query2);
        int id_cliente=0, id_style = 0;
        for(int i=0;i<=Datos_Pervasive.Rows.Count-1;i++) 
        {
            for(int j=0;j<=dt_customer.Rows.Count-1;j++)
            {
                if (Convert.ToString(Datos_Pervasive.Rows[i][0]).Trim() == Convert.ToString(dt_customer.Rows[j][1]).Trim())
                {
                    id_cliente = Convert.ToInt16(dt_customer.Rows[j][0]);
                    break;
                }
            }
            for (int k = 0; k <= dt_styles.Rows.Count - 1; k++)
            {
                if (Convert.ToString(Datos_Pervasive.Rows[i][1]).Trim() == Convert.ToString(dt_styles.Rows[k][1]).Trim())
                {
                    id_style = Convert.ToInt16(dt_styles.Rows[k][0]);
                    break;
                }
            }
            POrderDA.Save_POrder(id_cliente, id_style, Datos_Pervasive.Rows[i][2].ToString(), Datos_Pervasive.Rows[i][3].ToString(), Convert.ToInt16(Datos_Pervasive.Rows[i][4].ToString()), Convert.ToInt16(Datos_Pervasive.Rows[i][5].ToString()));
        }

        DataTable dt_bundles = new DataTable();
        dt_bundles = DataAccessPervasive.Get_DataTableNewBundlesV10();
        //se agregan las columnas size y color y se convierte el campo bundle de hex a string
        dt_bundles.Columns.Add("Size");
        dt_bundles.Columns.Add("Color");
        for (int i = 0; i <= dt_bundles.Rows.Count - 1; i++)
        {
            string data = dt_bundles.Rows[i][2].ToString();
            data = data.Substring(2, data.Length - 2);
            string y = HexConverter.HexStringToString(data);
            y = y.TrimEnd();

            if (y.Contains('ý'))
            {
                y = y.Replace('ý', ' ');
            }
            if (y.Contains('Ý'))
            {
                y = y.Replace('Ý', ' ');
            }
            y = y.Replace("\0", " ");
            y = y.Replace("    ", "|");
            y = y.Replace("||", "|");

            string Size = y.Split('|')[0];
            string Color = y.Split('|')[1];
            Color = Color.Trim();
            dt_bundles.Rows[i]["Size"] = Size;
            dt_bundles.Rows[i]["Color"] = Color;
        }
        
        DataTable dt_Id_POrderLocal = new DataTable();
        for(int i = 0; i<= dt_bundles.Rows.Count-1;i++)
        {
            dt_Id_POrderLocal = DataAccess.Get_DataTable("select Id_Order, POrder from POrder where porder='" + dt_bundles.Rows[i][4] + "'");
            if (Convert.ToString(dt_Id_POrderLocal.Rows[0][1]) == Convert.ToString(dt_bundles.Rows[i][4]))
            {
                POrderDA.Save_Bundles(Convert.ToInt16(dt_Id_POrderLocal.Rows[0][0]), Convert.ToInt16(dt_bundles.Rows[i][0]), Convert.ToInt16(dt_bundles.Rows[i][1]), Convert.ToString(dt_bundles.Rows[i][5]), Convert.ToString(dt_bundles.Rows[i][6]), Convert.ToInt16(dt_bundles.Rows[i][3]));
            }
        }
        
        /*
        DataTable Datos_LocalDB = new DataTable();
        string sql = "select Cliente, Estilo, POrder, Description, Quantity, Bundles from ProductionPervasive";
        Datos_LocalDB = DataAccess.Get_DataTable(sql);        
        DataTable Datos_Nuevos = new DataTable();
        int contador = 0;

        for (int i = 0; i <= Datos_Pervasive.Rows.Count - 1; i++)
        {
            for (int j = 0; j <= Datos_LocalDB.Rows.Count - 1; j++)
            {             
                if(Datos_Pervasive.Rows[i][2] == Datos_LocalDB.Rows[j][2])
                {
                    contador=1;
                    break;
                }
                else
                {
                    contador = 0;
                }
            }
            if (contador == 0)
            { 
            
            }
        }
        */
    }


    protected void img_importdata_Click(object sender, ImageClickEventArgs e)
    {
        Importar_Datos();


        DataTable dt_localPOrder = new DataTable();
        dt_localPOrder = DataAccess.Get_DataTable("select * from POrder");

        GridView1.DataSource = dt_localPOrder;
        GridView1.DataBind();  
    }
}