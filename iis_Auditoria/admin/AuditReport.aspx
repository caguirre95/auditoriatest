﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AuditReport.aspx.cs" Inherits="AuditReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style>
  #ContentPlaceHolder2_GV_Defects
  {
  margin-top:-20px;
  }
  
  #reporte_aql tr td, #reporte_oql tr td,   #ContentPlaceHolder2_reporte tr td
  {
  padding:15px;
  border-top:1px solid #ccc; 
  }
  </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h3>
        <asp:Label ID="Label1" runat="server" Text="Control de Auditoria x Bihorario"></asp:Label></h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
<br />
    <div class="busqueda" style="width:100%;">
    <center>
    <div style="margin-bottom:10px;">
    <div style="display:table-cell;padding:5px;border-left:1px solid #ccc;border-top:1px solid #ccc;border-bottom:1px solid #ccc;">
    <asp:Label ID="Label3" runat="server" Text="Linea:"></asp:Label>&nbsp;&nbsp;
    </div>
    <div style="display:table-cell;padding:5px;border-top:1px solid #ccc;border-bottom:1px solid #ccc;">
     <asp:DropDownList ID="DropDownList1" runat="server">
               <%-- <asp:ListItem Value="0">Select a Value</asp:ListItem>
                <asp:ListItem Value="1">1</asp:ListItem>
                <asp:ListItem Value="2">2</asp:ListItem>
                <asp:ListItem Value="3">3</asp:ListItem>
                <asp:ListItem Value="4">4</asp:ListItem>
				<asp:ListItem Value="5">5</asp:ListItem>
				<asp:ListItem Value="6">6</asp:ListItem>
				<asp:ListItem Value="7">7</asp:ListItem>
				<asp:ListItem Value="8">8</asp:ListItem>
				<asp:ListItem Selected="True" Value="9">9</asp:ListItem>--%>
    </asp:DropDownList>
    </div>
    <div style="display:table-cell;padding:5px;border-left:1px solid #ccc;border-bottom:1px solid #ccc;border-top:1px solid #ccc;">
    <asp:Label ID="Label2" runat="server" Text="Fecha de Reporte: "></asp:Label>&nbsp;&nbsp;
    </div>
    <div style="display:table-cell;padding:5px;border-bottom:1px solid #ccc;border-top:1px solid #ccc;">
        <dx:ASPxDateEdit ID="txtfecha1" runat="server" AnimationType="Fade" width="100px" Theme="Metropolis">
        </dx:ASPxDateEdit>   
    </div>
    <div style="display:table-cell;padding:5px;border-top:1px solid #ccc;border-right:1px solid #ccc;border-bottom:1px solid #ccc;">
        <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Buscar" 
            onclick="ASPxButton1_Click" Theme="MetropolisBlue">
        </dx:ASPxButton>    
    </div>
    </div>    
    </center>
    </div>
<br />
<center>
<div width="80%">
	       <dx:ASPxLabel ID="txtmensaje" runat="server" Text="" Visible="False">
    </dx:ASPxLabel>
</div>
</center>
<center>
    <div class="col-md-10 col-md-offset-1" style="margin-bottom:40px;">
    <div id="contenedor" runat="server"></div>        
    </div>    
</center>

</asp:Content>

