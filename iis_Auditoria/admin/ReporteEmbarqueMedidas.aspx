﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteEmbarqueMedidas.aspx.cs" Inherits="admin_ReporteEmbarqueMedidas" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function date1() {
            $('#<%=txtfecha1.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

        function date2() {
            $('#<%=txtfecha2.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

    </script>

    <style>
        .m {
            margin-top: 25px;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <br />
    <div class="container">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

            <div class="panel panel-primary" style="margin-top: 5px">
                <div class="panel-heading">Reporte Medidas Embarque</div>
                <div class="panel-body">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="panel-default">
                                <div class="panel-heading">Select </div>
                                <div class="panel-body">
                                    <asp:Panel runat="server" ID="panelfechas">
                                        Start Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                        End Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                    </asp:Panel>
                                    <div style="padding-top: 15px">
                                        <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" runat="server" OnClick="btngenerar_Click" />
                                    </div>
                                    <div style="padding-top: 15px">
                                        <asp:Button Text="Limpiar Campos" CssClass="btn btn-info form-control" ID="btnlimpiar" runat="server" OnClick="btnlimpiar_Click" />
                                    </div>
                                    <hr />
                                    <a href="#ventanaG" data-toggle="modal" style="width: 161px" class="btn btn-md btn-success"><span class="glyphicon glyphicon-align-justify"></span>Ver Piezas</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <hr />
                        <dx1:ASPxGridView ID="grdmi" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdmi_DataBinding" OnCustomCallback="grdmi_CustomCallback">
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Reporte Medidas Intex" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="POrder" Caption="Corte"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Style" Caption="Estilo"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="TG" Caption="Total"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="TOrder" Caption="Total entre Fechas"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="In Spec" Caption="In Spec"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Big" Caption="Big"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Small" Caption="Small"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:GridViewBandColumn>
                            </Columns>
                            <Settings ShowFooter="true" VerticalScrollBarMode="Visible" ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                            <SettingsPager PageSize="100"></SettingsPager>
                            <TotalSummary>
                                <dx1:ASPxSummaryItem FieldName="POrder" SummaryType="Count" DisplayFormat="Total: {0}" />
                                <dx1:ASPxSummaryItem FieldName="Style" SummaryType="Count" DisplayFormat="Total: {0}" />
                                <dx1:ASPxSummaryItem FieldName="TG" SummaryType="Sum" DisplayFormat="Total: {0}" />
                                <dx1:ASPxSummaryItem FieldName="TOrder" SummaryType="Sum" DisplayFormat="Total: {0}" />
                                <dx1:ASPxSummaryItem FieldName="In Spec" SummaryType="Sum" DisplayFormat="Total: {0}" />
                                <dx1:ASPxSummaryItem FieldName="Big" SummaryType="Sum" DisplayFormat="Total: {0}" />
                                <dx1:ASPxSummaryItem FieldName="Small" SummaryType="Sum" DisplayFormat="Total: {0}" />
                            </TotalSummary>
                        </dx1:ASPxGridView>

                        <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control m" Text="Export to Excel" OnClick="btnexcel_Click" />
                        <dx1:ASPxGridViewExporter ID="export" runat="server"></dx1:ASPxGridViewExporter>

                    </div>


                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <hr />
                        <dx1:ASPxGridView ID="grdmiDD" 
                            runat="server" 
                            Width="100%" 
                            Theme="Metropolis" 
                            OnDataBinding="grdmiDD_DataBinding" 
                            OnCustomCallback="grdmiDD_CustomCallback"
                            Settings-HorizontalScrollBarMode="Auto" 
                            SettingsBehavior-AllowSort="false"
                            Styles-Cell-CssClass="textAlignLeft" 
                            ClientInstanceName="grid">
                            <SettingsBehavior AllowEllipsisInText="true" />
                            <SettingsResizing ColumnResizeMode="Control" />
                            <Settings ShowColumnHeaders="true" />
                        </dx1:ASPxGridView>

                        <asp:Button ID="cmdExportDD" runat="server" CssClass="btn btn-success form-control m" Text="Export to Excel" OnClick="cmdExportDD_Click" />
                        <dx1:ASPxGridViewExporter ID="exportDD" runat="server"></dx1:ASPxGridViewExporter>

                    </div>
                </div>
            </div>



        </div>
    </div>

    <aside class="modal fade" id="ventanaG">
        <div class="modal-dialog">
            <div class="modal-content">
                <article class="modal-header">
                    <button id="cerrarG" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                    <h4>Ver Piezas Ingresadas </h4>
                </article>
                <article class="modal-body">
                    <asp:UpdatePanel ID="upd1" runat="server">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="panel1">
                                Start Date
                                    <dx1:ASPxDateEdit runat="server" ID="ASPxDateEdit1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                End Date
                                    <dx1:ASPxDateEdit runat="server" ID="ASPxDateEdit2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                            </asp:Panel>
                            <hr />
                            <asp:Button Text="Ver" CssClass="btn btn-primary form-control" ID="Button1" runat="server" OnClick="Button1_Click" />
                            <hr />
                            <asp:Button Text="Limpiar" CssClass="btn btn-primary form-control" ID="Button2" runat="server" OnClick="Button2_Click" />

                            <hr />
                            <hr />
                            <%-- <asp:GridView runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" CssClass="table  table-hover table-striped " ID="grdpiezas">                                         
                            </asp:GridView>--%>

                            <dx1:ASPxGridView ID="grdpiezas" runat="server" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false">
                                <Columns>
                                    <dx1:GridViewBandColumn Caption="Reporte Grande-Grande Intex" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx1:GridViewDataTextColumn FieldName="Corte" Caption="Corte"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Total_Grande_Grande" Caption="Total Grande-Grande"></dx1:GridViewDataTextColumn>
                                        </Columns>
                                    </dx1:GridViewBandColumn>
                                </Columns>
                                <Settings ShowFooter="true" VerticalScrollBarMode="Visible" ShowGroupFooter="VisibleAlways" />
                                <SettingsPager PageSize="50"></SettingsPager>
                                <TotalSummary>
                                    <dx1:ASPxSummaryItem FieldName="Corte" SummaryType="Count" DisplayFormat="Total: {0}" />
                                    <dx1:ASPxSummaryItem FieldName="Total_Grande_Grande" SummaryType="Sum" DisplayFormat="Total: {0}" />
                                </TotalSummary>
                            </dx1:ASPxGridView>

                            <hr />                            

                             <dx1:ASPxGridView ID="grdpiezas2" runat="server" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false">
                                <Columns>
                                    <dx1:GridViewBandColumn Caption="Reporte Grande-Grande Intex" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx1:GridViewDataTextColumn FieldName="Corte" Caption="Corte"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Total_Pequeño_Pequeño" Caption="Total Pequeño-Pequeño"></dx1:GridViewDataTextColumn>
                                        </Columns>
                                    </dx1:GridViewBandColumn>
                                </Columns>
                                <Settings ShowFooter="true" VerticalScrollBarMode="Visible" ShowGroupFooter="VisibleAlways" />
                                <SettingsPager PageSize="50"></SettingsPager>
                                <TotalSummary>
                                    <dx1:ASPxSummaryItem FieldName="Corte" SummaryType="Count" DisplayFormat="Total: {0}" />
                                    <dx1:ASPxSummaryItem FieldName="Total_Pequeño_Pequeño" SummaryType="Sum" DisplayFormat="Total: {0}" />
                                </TotalSummary>
                            </dx1:ASPxGridView>


                        </ContentTemplate>
                    </asp:UpdatePanel>

                </article>
            </div>
        </div>
    </aside>

</asp:Content>

