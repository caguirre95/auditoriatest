﻿using DevExpress.Web;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class admin_HistorialEstiloIntexMedida : System.Web.UI.Page
{
    class ClassArrm
    {
        public int id { get; set; }
        public string semana { get; set; }
    }

    DateTime fecha;
    DateTime fecha0;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                txtfecha1.Text = DateTime.Now.ToShortDateString();
                txtfecha2.Text = DateTime.Now.ToShortDateString();

                // cargacombo();
                // cargagridestilos();
            }
            gridpack.SettingsDetail.ExportMode = GridViewDetailExportMode.Expanded;
        }
        catch (Exception)
        {

            throw;
        }
    }

    //void cargagridestilos()
    //{
    //    string sqline = " select vw.Estilo as Style from vwMedidasNewIntex vw"
    //                  + " where datepart(wk, fechaRegistro) = DATEPART(WK, GETDATE()) and DATEPART(YY, fechaRegistro)= DATEPART(YY, GETDATE())"
    //                  + " group by vw.Estilo";

    //    DataTable dtEstilos = DataAccess.Get_DataTable(sqline);

    //    grvestilos.DataSource = dtEstilos;
    //    grvestilos.DataBind();
    //}

    protected void gridpack_DataBinding(object sender, EventArgs e)
    {
        try
        {
            gridpack.DataSource = Session["dtHG"];
        }
        catch (Exception ex)
        {
            string j = ex.Message;
        }
    }

    protected void gridpack_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridpack.DataBind();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {

        ASPxGridViewExporter1.GridViewID = "gridpack";

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link1.Component = ASPxGridViewExporter1;


        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link1 });

        compositeLink.CreatePageForEachLink();

        using (MemoryStream stream = new MemoryStream())
        {

            XlsxExportOptions options = new XlsxExportOptions();

            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
            options.SheetName = "Medida despues de Lavado";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=Medida-Despues-Lavado.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        ps.Dispose();

    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {

        if (txtstyle.Text != "")
        {

            Session["dtHG"] = null;

            DateTime dtime = DateTime.Parse(txtfecha1.Text);

            DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

            Session["f1"] = DateTime.Parse(txtfecha1.Text);

            Session["f2"] = DateTime.Parse(txtfecha2.Text);


            string query = " select vw.Id_Style,vw.Estilo,vw.color, sum(case estado when 0 then 1 else 0 end) as Rechazado,sum(case estado when 1 then 1 else 0 end)as Aceptado,count(vw.cantidad) as total"
                         + " from vwMedidasNewIntexColor vw"
                         + " where vw.estilo = '" + txtstyle.Text.Trim() + "' and fechaRegistro between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                         + " group by vw.Id_Style,vw.Estilo,vw.color";

           
            var dt = DataAccess.Get_DataTable(query);

            Session["dtHG"] = dt;
            gridpack.DataBind();
            gridpack.ExpandAll();
            gridpack.DetailRows.ExpandAllRows();

        }
    }

    public class order
    {
        public int idorder { get; set; }
        public string porder { get; set; }
    }

    protected void grdtalla_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
      //  int id = (int)detailGrid.GetMasterRowKeyValue();
        int id2 = (int)detailGrid.GetMasterRowFieldValues("Id_Style");
        string color = (string)detailGrid.GetMasterRowFieldValues("color");//.GetMasterRowKeyValue();

        var f1 = (DateTime)Session["f1"];
        var f2 = (DateTime)Session["f2"];
        var dt = OrderDetailDA.ExtraeMedidasIntexXTallasXEstilo(id2,f1,f2,color);

        detailGrid.DataSource = dt;
    }

    protected void grdtolSpc_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        int id2 = (int)detailGrid.GetMasterRowFieldValues("Id_Style");
        string color = (string)detailGrid.GetMasterRowFieldValues("color");
        var f1 = (DateTime)Session["f1"];
        var f2 = (DateTime)Session["f2"];
        var dt = OrderDetailDA.ExtraeMedidasIntexSpecXEstilo(id2, f1, f2,color);

        detailGrid.DataSource = dt;
    }

    protected void grdOutSpc_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        int id2 = (int)detailGrid.GetMasterRowFieldValues("Id_Style");
        string color = (string)detailGrid.GetMasterRowFieldValues("color");

        var f1 = (DateTime)Session["f1"];
        var f2 = (DateTime)Session["f2"];
        var dt = OrderDetailDA.ExtraeMedidasIntexFueraDRangoXEstilo(id2, f1, f2,color);

        detailGrid.DataSource = dt;
    }
}