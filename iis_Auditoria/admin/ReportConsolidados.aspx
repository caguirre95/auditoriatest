﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportConsolidados.aspx.cs" Inherits="admin_ReportConsolidados"    %>

<%@ Register Assembly="DevExpress.XtraReports.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <%-- <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />--%>
    <link href="../css/bootstrap.min.css" rel="stylesheet" />

    <link href="../StyleSheetAuto.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Measurement Report </strong></h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                            <asp:HiddenField ID="HiddenField2" runat="server" />
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                Cut:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>

                                        <asp:TextBox ID="txtcut" placeholder="Cut" class=" form-control" AutoPostBack="true" OnTextChanged="txtcut_TextChanged" Font-Size="14px" runat="server"></asp:TextBox>
                                        <ajaxToolkit:AutoCompleteExtender ID="txtcut_AutoCompleteExtender" runat="server" BehaviorID="txtcut_AutoCompleteExtender"
                                            ServicePath="ReportConsolidados.aspx" ServiceMethod="GetCut" TargetControlID="txtcut"
                                            CompletionInterval="10" Enabled="True"
                                            MinimumPrefixLength="1"
                                            CompletionListCssClass="CompletionList"
                                            CompletionListHighlightedItemCssClass="CompletionListHighlightedItem"
                                            CompletionListItemCssClass="CompletionListItem"
                                            DelimiterCharacters="">
                                        </ajaxToolkit:AutoCompleteExtender>

                                    </div>

                                Line:
                                     <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                                Plant:
                                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>

                                Style:
                                      <div class="input-group">
                                          <span class="input-group-btn">
                                              <asp:LinkButton CssClass="btn btn-default" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                          </span>

                                          <asp:TextBox ID="txtstyle" placeholder="Style" class=" form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                          <ajaxToolkit:AutoCompleteExtender ID="txtstyle_AutoCompleteExtender1" runat="server" BehaviorID="txtstyle_AutoCompleteExtender1"
                                              ServicePath="ReportMedidaMaster.aspx" ServiceMethod="GetStyle" TargetControlID="txtstyle"
                                              CompletionInterval="10" Enabled="True"
                                              MinimumPrefixLength="1"
                                              CompletionListCssClass="CompletionList"
                                              CompletionListHighlightedItemCssClass="CompletionListHighlightedItem"
                                              CompletionListItemCssClass="CompletionListItem"
                                              DelimiterCharacters="">
                                          </ajaxToolkit:AutoCompleteExtender>

                                      </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                    <asp:ListItem Value="1">Cut</asp:ListItem>
                                    <asp:ListItem Value="2">Style</asp:ListItem>
                                    <asp:ListItem Value="3">Line</asp:ListItem>
                                    <asp:ListItem Value="4">Plant</asp:ListItem>
                                </asp:RadioButtonList>

                            </div>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                Start Date
                            <div class="input-group">

                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>

                                <dx:ASPxDateEdit ID="txtfecha1" Theme="Metropolis" Height="35px" runat="server" EditFormat="Custom" Width="100%">
                                </dx:ASPxDateEdit>

                            </div>
                                End Date
                            <div class="input-group">

                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                <dx:ASPxDateEdit ID="txtfecha2" Theme="Metropolis" Height="35px" runat="server" EditFormat="Custom" Width="100%">
                                </dx:ASPxDateEdit>

                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-lg-4" style="padding-top: 10px; padding-bottom: 10px">
                            <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" OnClick="btngenerar_Click" runat="server" />
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <dx:ASPxWebDocumentViewer ID="ASPxWebDocumentViewer1" runat="server"></dx:ASPxWebDocumentViewer>
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

