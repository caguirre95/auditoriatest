﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReportConsolidados : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Date = DateTime.Now;
            txtfecha2.Date = DateTime.Now;
        
            cargacombo();

        }
    }

   
    void cargacombo()
    {
        string sqline = "select id_linea,numero from Linea l where l.numero not like '%-%' and l.numero not like '%A%' and idcliente=11";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT [id_planta],[descripcion] FROM[Auditoria].[dbo].[Planta]";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
        DropDownListPlant.Items.Insert(4, "All Plant");

    }

 
    protected void btngenerar_Click(object sender, EventArgs e)
    {

        try
        {
            int cmd =int.Parse(RadioButtonList1.SelectedValue);

            switch (cmd)
            {
                case 1:
                    XtraReportMasterTotalCut repC = new XtraReportMasterTotalCut();

                    repC.Parameters[0].Value = HiddenField1.Value;
                    repC.Parameters[1].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                    repC.Parameters[2].Value = txtfecha2.Date.ToString("yyyy-MM-dd");
                  

                    ASPxWebDocumentViewer1.OpenReport(repC);

                    break;
                case 2:
                    XtraReportMasterTotalStyle repS = new XtraReportMasterTotalStyle();

                
                    repS.Parameters[0].Value =txtstyle.Text;
                    repS.Parameters[1].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                    repS.Parameters[2].Value = txtfecha2.Date.ToString("yyyy-MM-dd");


                    ASPxWebDocumentViewer1.OpenReport(repS);
                    break;

                case 3:

                    if (DropDownListLine.SelectedItem.Text == "Select...")
                    {
                        return;
                    }
                    XtraReportMaterTotalLine repL = new XtraReportMaterTotalLine();

                    repL.Parameters[0].Value = int.Parse(DropDownListLine.SelectedValue);
                    repL.Parameters[1].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                    repL.Parameters[2].Value = txtfecha2.Date.ToString("yyyy-MM-dd");


                    ASPxWebDocumentViewer1.OpenReport(repL);
                    break;

                case 4:
                    if (DropDownListPlant.SelectedItem.Text == "Select...")
                    {
                        return;
                    }
                    XtraReportMasterTotalPlant repP = new XtraReportMasterTotalPlant();

                    repP.Parameters[0].Value = int.Parse(DropDownListPlant.SelectedValue);
                    repP.Parameters[1].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                    repP.Parameters[2].Value = txtfecha2.Date.ToString("yyyy-MM-dd");


                    ASPxWebDocumentViewer1.OpenReport(repP);
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {

            throw;
        }


    }

    [WebMethod]
    [ScriptMethod]
    public static List<string> GetCut(string prefixText)
    {
        try
        {
            List<string> lista = new List<string>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = " SELECT distinct top 10  p.POrder FROM  POrder p  where p.POrder like '%" + prefixText + "%' and p.Id_Cliente=11";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string obj;

                obj = dt.Rows[i][0].ToString();

                lista.Add(obj);

            }

            return lista;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    //protected void txtstyle_TextChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        DataTable dt = new DataTable();
    //        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

    //        cn.Open();
    //        string sql = " select top(1) s.Id_Style from Style s where s.Style='" + txtstyle.Text + "'";
    //        SqlCommand cmd = new SqlCommand(sql, cn);
    //        SqlDataAdapter da = new SqlDataAdapter(cmd);
    //        da.Fill(dt);
    //        cn.Close();

    //        HiddenField2.Value = "0";
    //        for (int i = 0; i < dt.Rows.Count; i++)
    //        {
    //            HiddenField2.Value = dt.Rows[i][0].ToString();
    //        }
           
           
    //    }
    //    catch (Exception ex)
    //    {
    //        Console.Write(ex);
           
    //    }
    //}

    protected void txtcut_TextChanged(object sender, EventArgs e)
    {

        try
        {
            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = " SELECT top 1  p.Id_Order FROM  POrder p  where p.POrder='" + txtcut.Text + "'";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            HiddenField1.Value = "0";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                HiddenField1.Value = dt.Rows[i][0].ToString();
            }


        }
        catch (Exception ex)
        {
            Console.Write(ex);

        }
    }

}