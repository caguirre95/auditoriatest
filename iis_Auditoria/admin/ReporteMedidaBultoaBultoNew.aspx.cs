﻿using DevExpress.Web;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReporteMedidaBultoaBultoNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();
            Session["embarqueAl"] = null;
            Session["intexmBB_DD"] = null;
            cargacombo();
            cargagridestilos();
        }
    }

    void cargagridestilos()
    {
        string query = " select Style from tbMedidasBultoaBultoAL ba join Style s on s.Id_Style = ba.idEstilo"
                      + " where DATEPART(WEEk,fechaRegistro) = (select DATEPART(WEEK,GETDATE())) and DATEPART(YEAR,fechaRegistro) = (select DATEPART(YEAR,GETDATE()))"
                      + " group by Style";

        DataTable dtEstilos = DataAccess.Get_DataTable(query);

        grvestilos.DataSource = dtEstilos;
        grvestilos.DataBind();
    }

    void cargacombo()
    {
        string sqline = "select id_linea,numero from Linea l where l.numero not like '%-%' and l.numero not like '%A%' order by CONVERT(float,numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT [id_planta],[descripcion] FROM[Auditoria].[dbo].[Planta]";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
        DropDownListPlant.Items.Insert(5, "All Plant");

    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {           

            DataTable prod = funcionCarga();

            if (prod == null)
            {
                return;
            }

            int contCol = prod.Columns.Count;

            DataRow workRow;

            workRow = prod.NewRow();

            workRow[0] = "Total";

            for (int i = 2; i < contCol; i++)
            {
                int suma = 0;

                for (int j = 0; j < 33; j++)
                {
                    if (!string.IsNullOrEmpty(prod.Rows[j][i].ToString()))
                    {
                        suma = suma + Convert.ToInt32(prod.Rows[j][i].ToString());
                    }

                }
                workRow[i] = suma;
            }

            prod.Rows.Add(workRow);

            Session["embarqueAl"] = prod;

            ASPxGridView1.DataBind();


            string resp = RadioButtonList1.SelectedValue;
            if (resp == "4") {
                if (DropDownListPlant.SelectedItem.Text == "All Plant") {
                    var consultaDD = new DataTable();
                    string sqlquery = "EXEC dbo.Medidas_Contadas_BULTO_A_BULTO_SP_Qry @desde='" + txtfecha1.Text + "', @hasta='" + txtfecha2.Text + "';";
                    consultaDD = DataAccess.Get_DataTable(sqlquery);
                    Session["intexmBB_DD"] = consultaDD;
                    grdmiDD.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;

        }
    }

    DataTable funcionCarga()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);

        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string resp = RadioButtonList1.SelectedValue;

        string query = "";

        DataTable prod = new DataTable();

        switch (resp)
        {
            case "1"://cut
                if (txtPorder.Text != string.Empty)
                {

                    if (!string.IsNullOrEmpty(hfidPorder.Value))
                    {
                        int idc = Convert.ToInt32(hfidPorder.Value);

                        prod = OrderDetailDA.ReporteBaBDinamicCorte(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), idc);
                    }
                }
                break;

            case "2"://style
                if (txtstyle.Text != string.Empty)
                {

                    if (!string.IsNullOrEmpty(hdnidstyle.Value))
                    {
                        int ides = Convert.ToInt16(hdnidstyle.Value);

                        //prod = OrderDetailDA.ReporteBaBDinamicStyle(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), ides);                        

                        query = " declare @fecha1 nchar(10) = '" + txtfecha1.Text + "', @fecha2 nchar(10)= '" + txtfecha2.Text + "', @idestilo int= " + ides + ""
                              + " DECLARE @sql nvarchar(MAX), @PuntosMedida nvarchar(500)"
                              + " SELECT @PuntosMedida = STUFF((SELECT ',' + QUOTENAME(LTRIM(Nombre)) FROM (select  pm.NombreMedida Nombre from tbMedidasBultoaBultoAL me"
                              + " join tbMedidasBultoaBultoP mep on mep.id = me.id"
                              + " join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida join POrder p on  me.idPorder = p.Id_Order join Style s on p.Id_Style = s.Id_Style"
                              + " where idEstilo = @idestilo and CONVERT(date,fechaRegistro) between @fecha1 and @fecha2 group by pm.NombreMedida) AS T ORDER BY Nombre DESC FOR XML PATH('')), 1, 1, '') SET @sql = N'select * from ("
                              + " select a.Medida medidaTexto, ISNULL(b.total, 0) total, b.NombreMedida, a.valor medidaDecimal from (select Medida, valor from tblMedidas) a left join"
                              + " (select mep.medidaTexto, COUNT(mep.medidaTexto) total, pm.NombreMedida, mep.medidaFlotante from tbMedidasBultoaBultoAL me join tbMedidasBultoaBultoP mep on me.id = mep.id"
                              + " join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida join Style s on s.Id_Style = me.idEstilo"
                              + " where s.Id_Style = '+convert(nchar(4), @idestilo)+' and CONVERT(date, me.fechaRegistro) between '''+ @fecha1+''' and '''+@fecha2+'''"
                              + " group by mep.medidaTexto, pm.NombreMedida, mep.medidaFlotante) b on a.valor = b.medidaFlotante group by a.Medida, b.total, b.NombreMedida, a.valor"
                              + " ) a pivot(sum(total) for NombreMedida in (' + LTRIM(RTRIM(@PuntosMedida))  + N')) AS P order by medidaDecimal desc; '; EXEC sp_executesql @sql;";

                        prod = DataAccess.Get_DataTable(query);
                    }
                }
                break;

            case "3"://line
                if (DropDownListLine.SelectedItem.Text != "Select...")
                {
                    //prod = OrderDetailDA.ReporteBaBDinamicLine(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListLine.SelectedValue));

                    //query = " declare @fecha1 nchar(10) = '" + txtfecha1.Text + "', @fecha2 nchar(10)= '" + txtfecha2.Text + "', @idlinea int= " + DropDownListLine.SelectedValue + ""
                    //      + " DECLARE @sql nvarchar(MAX)"
                    //      + " SET @sql = N'select * from (select a.Medida medidaTexto, ISNULL(b.total, 0) total, b.NombreMedida, a.valor medidaDecimal from(select Medida, valor from tblMedidas) a left join"
                    //      + " (select mep.medidaTexto, COUNT(mep.medidaTexto) total, pm.NombreMedida, mep.medidaFlotante from POrder p join tbMedidasBultoaBultoAL me on me.idPorder = p.Id_Order"
                    //      + " join tbMedidasBultoaBultoP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida join Style s on s.Id_Style = p.Id_Style"
                    //      + " where p.Id_Linea2 = '+convert(nchar(2), @idlinea)+' and CONVERT(date, me.fechaRegistro) between '''+convert(nchar(10),@fecha1)+''' and '''+convert(nchar(10),@fecha2)+'''"
                    //      + " group by mep.medidaTexto, pm.NombreMedida, mep.medidaFlotante) b on a.valor = b.medidaFlotante group by a.Medida, b.total, b.NombreMedida, a.valor"
                    //      + " ) a pivot(sum(total) for NombreMedida in (' + (SELECT STUFF((SELECT ',' + QUOTENAME(LTRIM(Nombre)) FROM (select DISTINCT pm.NombreMedida Nombre from POrder p"
                    //      + " join tbMedidasBultoaBultoAL me on me.idPorder = p.Id_Order join tbMedidasBultoaBultoP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida"
                    //      + " where p.Id_Linea2 = @idlinea and CONVERT(date, me.fechaRegistro) between @fecha1 and @fecha2) AS T ORDER BY Nombre DESC FOR XML PATH('')), 1, 1, '')) +N')) AS P order by medidaDecimal desc; ';"
                    //      + " EXEC sp_executesql @sql;";

                    string Fquery = " declare @fecha1 nchar(10) = '" + txtfecha1.Text + "', @fecha2 nchar(10)= '" + txtfecha2.Text + "', @idlinea int= " + DropDownListLine.SelectedValue + ""
                                  + " SELECT STUFF((SELECT ',' + QUOTENAME(LTRIM(Nombre)) FROM(select DISTINCT pm.NombreMedida Nombre from POrder p"
                                  + " join tbMedidasBultoaBultoAL me on me.idPorder = p.Id_Order join tbMedidasBultoaBultoP mep on mep.id = me.id"
                                  + " join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida where p.Id_Linea2 = @idlinea and CONVERT(date, me.fechaRegistro) between @fecha1 and @fecha2"
                                  + " ) AS T ORDER BY Nombre DESC FOR XML PATH('')), 1, 1, '')";

                    DataTable Fq = DataAccess.Get_DataTable(Fquery);

                    query = " declare @fecha1 nchar(10) = '" + txtfecha1.Text + "', @fecha2 nchar(10)= '" + txtfecha2.Text + "', @idlinea int= " + DropDownListLine.SelectedValue + ""
                          + " select* from (select a.Medida medidaTexto, ISNULL(b.total, 0) total, b.NombreMedida, a.valor medidaDecimal from (select Medida, valor from tblMedidas) a left join"
                          + " (select mep.medidaTexto, COUNT(mep.medidaTexto) total, pm.NombreMedida, mep.medidaFlotante from POrder p join tbMedidasBultoaBultoAL me on me.idPorder = p.Id_Order"
                          + " join tbMedidasBultoaBultoP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida where p.Id_Linea2 = @idlinea"
                          + " and CONVERT(date, me.fechaRegistro) between @fecha1 and @fecha2 group by mep.medidaTexto, pm.NombreMedida, mep.medidaFlotante) b on a.valor = b.medidaFlotante"
                          + " group by a.Medida, b.total, b.NombreMedida, a.valor) a pivot(sum(total) for NombreMedida in (" + Fq.Rows[0][0].ToString() + ")) AS P order by medidaDecimal desc";

                    prod = DataAccess.Get_DataTable(query);
                }
                break;

            case "4"://plant
                if (DropDownListPlant.SelectedItem.Text == "All Plant")
                {
                    //prod = OrderDetailDA.ReporteBaBDinamicplanAll(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));                    

                    //query = " declare @fecha1 nchar(10) = '" + txtfecha1.Text + "', @fecha2 nchar(10)= '" + txtfecha2.Text + "'"
                    //         + " DECLARE @sql nvarchar(MAX)"
                    //         + " SET @sql = N'select * from (select a.Medida medidaTexto, ISNULL(b.total, 0) total, b.NombreMedida, a.valor medidaDecimal from(select Medida, valor from tblMedidas) a left join"
                    //         + " (select mep.medidaTexto, COUNT(mep.medidaTexto) total, pm.NombreMedida, mep.medidaFlotante from POrder p join tbMedidasBultoaBultoAL me on me.idPorder = p.Id_Order"
                    //         + " join tbMedidasBultoaBultoP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida join Style s on s.Id_Style = p.Id_Style"
                    //         + " where CONVERT(date, me.fechaRegistro) between '''+convert(nchar(10),@fecha1)+''' and '''+convert(nchar(10),@fecha2)+''' group by mep.medidaTexto, pm.NombreMedida, mep.medidaFlotante"
                    //         + " ) b on a.valor = b.medidaFlotante group by a.Medida, b.total, b.NombreMedida, a.valor) a pivot(sum(total) for NombreMedida in (' + (SELECT STUFF((SELECT ',' + QUOTENAME(LTRIM(Nombre))"
                    //         + " FROM (select DISTINCT pm.NombreMedida Nombre from POrder p join tbMedidasBultoaBultoAL me on me.idPorder = p.Id_Order join tbMedidasBultoaBultoP mep on mep.id = me.id"
                    //         + " join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida where CONVERT(date, me.fechaRegistro) between @fecha1 and @fecha2) AS T ORDER BY Nombre DESC FOR XML PATH('')), 1, 1, '')) +N')"
                    //         + " ) AS P order by medidaDecimal desc; '; EXEC sp_executesql @sql;";

                    string Fquery = " declare @fecha1 nchar(10) = '" + txtfecha1.Text + "', @fecha2 nchar(10)= '" + txtfecha2.Text + "'"
                                  + " SELECT STUFF((SELECT ',' + QUOTENAME(LTRIM(Nombre)) FROM (select DISTINCT pm.NombreMedida Nombre"
                                  + " from POrder p join tbMedidasBultoaBultoAL me on me.idPorder = p.Id_Order join tbMedidasBultoaBultoP mep on mep.id = me.id"
                                  + " join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida where"
                                  + " CONVERT(date, me.fechaRegistro) between @fecha1 and @fecha2) AS T ORDER BY Nombre DESC FOR XML PATH('')), 1, 1, '')";

                    DataTable Fq = DataAccess.Get_DataTable(Fquery);

                    query = " declare @fecha1 nchar(10) = '" + txtfecha1.Text + "', @fecha2 nchar(10)= '" + txtfecha2.Text + "'"
                          + " select * from (select a.Medida medidaTexto, ISNULL(b.total, 0) total, b.NombreMedida, a.valor medidaDecimal from (select Medida, valor from tblMedidas"
                          + " ) a left join (select mep.medidaTexto, COUNT(mep.medidaTexto) total, pm.NombreMedida, mep.medidaFlotante from POrder p"
                          + " join tbMedidasBultoaBultoAL me on me.idPorder = p.Id_Order join tbMedidasBultoaBultoP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida"
                          + " join Style s on s.Id_Style = p.Id_Style where CONVERT(date, me.fechaRegistro) between @fecha1 and @fecha2"
                          + " group by mep.medidaTexto, pm.NombreMedida, mep.medidaFlotante) b on a.valor = b.medidaFlotante group by a.Medida, b.total, b.NombreMedida, a.valor"
                          + " ) a pivot(sum(total) for NombreMedida in (" + Fq.Rows[0][0].ToString() + ")) as p order by medidaDecimal desc";

                    prod = DataAccess.Get_DataTable(query);
                }
                else if (DropDownListPlant.SelectedItem.Text != "All Plant" && DropDownListPlant.SelectedItem.Text != "Select...")
                {
                    //prod = OrderDetailDA.ReporteBaBDinamicplan(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListPlant.SelectedValue));

                    //query = " declare @fecha1 nchar(10) = '" + txtfecha1.Text + "', @fecha2 nchar(10)= '" + txtfecha2.Text + "', @idplanta int= " + DropDownListPlant.SelectedValue + ""
                    //          + " DECLARE @sql nvarchar(MAX)"
                    //          + " SET @sql = N'select * from (select a.Medida medidaTexto, ISNULL(b.total, 0) total, b.NombreMedida, a.valor medidaDecimal from (select Medida, valor from tblMedidas) a left join"
                    //          + " (select mep.medidaTexto, COUNT(mep.medidaTexto) total, pm.NombreMedida, mep.medidaFlotante from POrder p join tbMedidasBultoaBultoAL me on me.idPorder = p.Id_Order"
                    //          + " join tbMedidasBultoaBultoP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida join Style s on s.Id_Style = p.Id_Style"
                    //          + " where p.Id_Planta = '+convert(nchar(2), @idplanta)+' and CONVERT(date, me.fechaRegistro) between '''+convert(nchar(10),@fecha1)+''' and '''+convert(nchar(10),@fecha2)+'''"
                    //          + " group by mep.medidaTexto, pm.NombreMedida, mep.medidaFlotante) b on a.valor = b.medidaFlotante group by a.Medida, b.total, b.NombreMedida, a.valor"
                    //          + " ) a pivot(sum(total) for NombreMedida in (' + (SELECT STUFF((SELECT ',' + QUOTENAME(LTRIM(Nombre)) FROM (select DISTINCT pm.NombreMedida Nombre"
                    //          + " from POrder p join tbMedidasBultoaBultoAL me on me.idPorder = p.Id_Order join tbMedidasBultoaBultoP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida"
                    //          + " where p.Id_Planta = @idplanta and CONVERT(date, me.fechaRegistro) between @fecha1 and @fecha2) AS T ORDER BY Nombre DESC FOR XML PATH('')), 1, 1, '')) +N')) AS P order by medidaDecimal desc; ';"
                    //          + " EXEC sp_executesql @sql;";

                    string Fquery = " declare @fecha1 nchar(10) = '" + txtfecha1.Text + "', @fecha2 nchar(10)= '" + txtfecha2.Text + "', @idplanta int= " + DropDownListPlant.SelectedValue + ""
                                  + " SELECT STUFF((SELECT ',' + QUOTENAME(LTRIM(Nombre)) FROM (select DISTINCT pm.NombreMedida Nombre"
                                  + " from POrder p join tbMedidasBultoaBultoAL me on me.idPorder = p.Id_Order join tbMedidasBultoaBultoP mep on mep.id = me.id"
                                  + " join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida where p.Id_Planta = @idplanta"
                                  + " and CONVERT(date, me.fechaRegistro) between @fecha1 and @fecha2) AS T ORDER BY Nombre DESC FOR XML PATH('')), 1, 1, '')";

                    DataTable Fq = DataAccess.Get_DataTable(Fquery);

                    query = " declare @fecha1 nchar(10) = '" + txtfecha1.Text + "', @fecha2 nchar(10)= '" + txtfecha2.Text + "', @idplanta int= " + DropDownListPlant.SelectedValue + ""
                          + " select * from (select a.Medida medidaTexto, ISNULL(b.total, 0) total, b.NombreMedida, a.valor medidaDecimal from (select Medida, valor from tblMedidas"
                          + " ) a left join (select mep.medidaTexto, COUNT(mep.medidaTexto) total, pm.NombreMedida, mep.medidaFlotante from POrder p"
                          + " join tbMedidasBultoaBultoAL me on me.idPorder = p.Id_Order join tbMedidasBultoaBultoP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida"
                          + " join Style s on s.Id_Style = p.Id_Style where p.Id_Planta = @idplanta and CONVERT(date, me.fechaRegistro) between @fecha1 and @fecha2"
                          + " group by mep.medidaTexto, pm.NombreMedida, mep.medidaFlotante) b on a.valor = b.medidaFlotante group by a.Medida, b.total, b.NombreMedida, a.valor"
                          + " ) a pivot(sum(total) for NombreMedida in (" + Fq.Rows[0][0].ToString() + ")) as p order by medidaDecimal desc";

                    prod = DataAccess.Get_DataTable(query);
                }
                break;

            default:

                break;
        }

        return prod;

    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView1.DataSource = Session["embarqueAl"];
    }

    protected void ASPxGridView1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView1.DataBind();
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        try
        {
            string rd = RadioButtonList1.SelectedValue;

            string adjunto = "";

            switch (rd)
            {
                case "1": adjunto = "Corte-" + txtPorder.Text; break;
                case "2": adjunto = "Estilo-" + txtstyle.Text; break;
                case "3": adjunto = "Linea-" + DropDownListPlant.SelectedItem.Text; break;
                case "4": adjunto = DropDownListLine.SelectedItem.Text; break;
                default: break;
            }

            ASPxGridView1.DataBind();

            ASPxGridViewExporter1.GridViewID = "ASPxGridView1";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreatePageForEachLink();
            //compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Reporte-" + adjunto;
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=MBulto-" + adjunto + "-" + txtfecha1.Text + "-" + txtfecha2.Text + "-Descargado:" + DateTime.Now + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void ASPxGridView1_DataBound(object sender, EventArgs e)
    {
        ASPxGridView grid = (ASPxGridView)sender;
        foreach (GridViewDataColumn c in grid.Columns)
        {
            if ((c.FieldName.ToString()).StartsWith("medidaDecimal"))
            {
                c.Visible = false;
            }
        }
    }

    protected void btnr_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView1.Columns.Clear();
            
        }
        catch (Exception ex)
        {
            string a = ex.Message;
           // throw;
        }
    }


    protected void cmdExportDD_Click(object sender, EventArgs e)
    {
        try
        {
            exportDD.GridViewID = "grdmiDD";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = exportDD;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link3 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Detalle Medidas";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=DetalleMedidasBultoBulto-" + DateTime.Now.ToShortDateString() + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void grdmiDD_DataBinding(object sender, EventArgs e)
    {
        grdmiDD.DataSource = Session["intexmBB_DD"];
    }

    protected void grdmiDD_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdmiDD.DataBind();
    }

}