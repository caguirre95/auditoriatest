﻿using DevExpress.Web;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;



public partial class admin_HistorialCorteIntexMedida : System.Web.UI.Page
{
    class ClassArrm
    {
        public int id { get; set; }
        public string semana { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Session["dt"] = null;
          
                System.Globalization.CultureInfo norwCulture = System.Globalization.CultureInfo.CreateSpecificCulture("es");
                System.Globalization.Calendar cal = norwCulture.Calendar;
                int x = cal.GetWeekOfYear(DateTime.Now, norwCulture.DateTimeFormat.CalendarWeekRule, norwCulture.DateTimeFormat.FirstDayOfWeek);

                var list = new List<ClassArrm>();
                for (int i = 1; i <= x; i++)
                {
                    ClassArrm obj = new ClassArrm();
                    obj.id = i;
                    obj.semana = "Semana " + i;
                    list.Add(obj);
                }

                drpSemana.DataSource = list;
                drpSemana.DataTextField = "semana";
                drpSemana.DataValueField = "id";
                drpSemana.DataBind();

                drpSemana.SelectedValue = (x).ToString();

               // getgrid();
            }

            gridpack.SettingsDetail.ExportMode = GridViewDetailExportMode.Expanded;// Enum.Parse(typeof(GridViewDetailExportMode), ddlExportMode.Text);
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void gridpack_DataBinding(object sender, EventArgs e)
    {
        try
        {
            gridpack.DataSource = Session["dtHG"];
        }
        catch (Exception ex)
        {
            string j = ex.Message;
        }
    }

    protected void gridpack_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridpack.DataBind();
    }

    void getgrid()
    {
        if (HiddenFieldCortesId.Value != string.Empty)
        {
            Session["dtHG"] = null;

            var dt = OrderDetailDA.ExtraeMedidasIntexPrincipal(HiddenFieldCortesId.Value);

            Session["dtHG"] = dt;
            gridpack.DataBind();
            gridpack.ExpandAll();
        
        }       
      
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {

        ASPxGridViewExporter1.GridViewID = "gridpack";

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link1.Component = ASPxGridViewExporter1;


        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link1 });

        compositeLink.CreatePageForEachLink();

        using (MemoryStream stream = new MemoryStream())
        {
           
            XlsxExportOptions options = new XlsxExportOptions();

            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
            options.SheetName = "Medida despues de Lavado";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=Medida-Despues-Lavado.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        ps.Dispose();

    }    

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
          
             //   getgrid();

            if (HiddenFieldCortesId.Value != "")
            {

                Session["dtHG"] = null;
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();

                string json = "[" + HiddenFieldCortesId.Value + "]";

                var result = json_serializer.Deserialize<List<order>>(json);


                var r = result.Select(x => x.idorder).ToArray();

                var cadena = string.Join(",", r);

                //Console.WriteLine(cadena);

                var dt = OrderDetailDA.ExtraeMedidasIntexPrincipal(cadena);
                HiddenFieldCortesId.Value = "";
                Session["dtHG"] = dt;
                gridpack.DataBind();
                gridpack.ExpandAll();
                gridpack.DetailRows.ExpandAllRows();
            }

        }
        catch (Exception)
        {
            throw;
        }
    }      

    protected void LinkButton1_Click(object sender, EventArgs e)
    {

        if (HiddenFieldCortesId.Value!="")
        {
            
            Session["dtHG"] = null;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            string json = "["+ HiddenFieldCortesId.Value + "]";

            var result = json_serializer.Deserialize<List<order>>(json);


            var r = result.Select(x=>x.idorder).ToArray();

            var cadena =string.Join(",",r);

            //Console.WriteLine(cadena);

            var dt = OrderDetailDA.ExtraeMedidasIntexPrincipal(cadena);
            HiddenFieldCortesId.Value = "";
            Session["dtHG"] = dt;
            gridpack.DataBind();
            gridpack.ExpandAll();
            gridpack.DetailRows.ExpandAllRows();
        }
    }

    public class order
    {
        public int idorder { get; set; }
        public string porder { get; set; }
    }

    protected void grdtalla_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        int id = (int)detailGrid.GetMasterRowKeyValue();

        var dt = OrderDetailDA.ExtraeMedidasIntexXTallas(id);

        detailGrid.DataSource = dt;
    }

    protected void grdtolSpc_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        int id = (int)detailGrid.GetMasterRowKeyValue();

        var dt = OrderDetailDA.ExtraeMedidasIntexSpec(id);

        detailGrid.DataSource = dt;
    }

    protected void grdOutSpc_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        int id = (int)detailGrid.GetMasterRowKeyValue();

        var dt = OrderDetailDA.ExtraeMedidasIntexFueraDRango(id);

        detailGrid.DataSource = dt;
    }
}