﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reportTallaXCorte : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    [ScriptMethod]
    public static List<string> GetCut(string prefixText)
    {
        try
        {
            List<string> lista = new List<string>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = " SELECT distinct top 10  p.POrder FROM  POrder p  where p.POrder like '%" + prefixText + "%' and p.Id_Cliente=11";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string obj;

                obj = dt.Rows[i][0].ToString();

                lista.Add(obj);

            }

            return lista;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }
    protected void DropDownListProcess_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "1")
        {
            Panel1.Visible = true;
        }
        else
        {
            Panel1.Visible = false;
        }
    }


    protected void btnGenerar_Click(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "0")
        {
            return;
        }


        int resp = int.Parse(DropDownListProcess.SelectedValue);
      
        switch (resp)
        {
            case 1:

                XtraReportextraerTallaAl repAL = new XtraReportextraerTallaAl();

                repAL.Parameters[0].Value = txtcorte.Text;
                repAL.Parameters[1].Value = RadioButtonList1.SelectedValue;
             
                ASPxWebDocumentViewer1.OpenReport(repAL);

                break;
            case 2:

                XtraReportExtraerTalla repC = new XtraReportExtraerTalla();

                repC.Parameters[0].Value = txtcorte.Text;
                repC.Parameters[1].Value = RadioButtonList1.SelectedValue;

                ASPxWebDocumentViewer1.OpenReport(repC);

                break;
            case 3:
                XtraReportextraerTallaM4 repM4 = new XtraReportextraerTallaM4();

                repM4.Parameters[0].Value = txtcorte.Text;
                repM4.Parameters[1].Value = "test";

                ASPxWebDocumentViewer1.OpenReport(repM4);
                

                break;
            default:
                break;
        }


    }
}