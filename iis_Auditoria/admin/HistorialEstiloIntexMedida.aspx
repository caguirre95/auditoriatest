﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HistorialEstiloIntexMedida.aspx.cs" Inherits="admin_HistorialEstiloIntexMedida" %>
<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v16.2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtstyle.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "Reportafterwash.aspx/GetStyle",
                        data: "{'pref' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    estilo: item.estilo,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

        });

    </script>
      <style>
        .a {
            margin-top: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Medidas Intex X Estilo</strong></div>
            <div class="panel-body">

                <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control" Visible="false"></asp:SiteMapPath>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                      Estilo:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text=""  CssClass="btn btn-default" ID="LinkButton3" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtstyle" placeholder="Estilo" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                    <br />
                    <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control" Text="Generar" Width="200px" OnClick="btngenerar_Click" />

                    <asp:Button ID="btnExport" OnClick="btnExport_Click" CssClass="btn btn-success form-control a" runat="server" Text="Exportar Excel" Width="200px" />
                    <hr />
                </div>
                 <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Seleccione Rango de fecha</div>
                            <div class="panel-body">
                                Fecha Inicio
                            <div class="input-group">

                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>

                            </div>
                                Fecha Final
                          
                                <div class="input-group">
                                    <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                   <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow-x: auto">
                    <dx:ASPxGridView ID="gridpack" runat="server" KeyFieldName="Id_Style;color" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnDataBinding="gridpack_DataBinding" OnCustomCallback="gridpack_CustomCallback">
                        <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                        <Columns>
                            <dx:GridViewBandColumn Caption="Reporte de Medidas despues de Lavado por Estilo" HeaderStyle-HorizontalAlign="Center">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="Id_Style" Visible="false" Caption="id" VisibleIndex="0">
                                    </dx:GridViewDataTextColumn>                                 
                                    <dx:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="color" Caption="Color" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Aceptado" Caption="Piezas Aceptadas" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Rechazado" Caption="Piezas Rechazadas" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="total" Caption="Piezas Totales" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>                                  
                                </Columns>
                            </dx:GridViewBandColumn>
                        </Columns>
                        <Templates>
                            <DetailRow>
                                <div style="padding: 3px 3px 2px 3px">
                                    <dx:ASPxPageControl runat="server" ID="pageControl" Width="100%" EnableCallBacks="true">
                                        <TabPages>

                                            <dx:TabPage Text="Por Talla" Visible="true">
                                                <ContentCollection>
                                                    <dx:ContentControl runat="server">
                                                        <dx:ASPxGridView ID="grdtalla" runat="server" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnBeforePerformDataSelect="grdtalla_BeforePerformDataSelect"
                                                            KeyFieldName="Id_Style" Width="100%">
                                                            <Columns>
                                                                <dx:GridViewBandColumn Caption="Detalle Por Talla" HeaderStyle-HorizontalAlign="Center">
                                                                    <Columns>
                                                                        <dx:GridViewDataColumn FieldName="Style" Caption="Estilo" />
                                                                        <dx:GridViewDataColumn FieldName="size" Caption="Tallas" />
                                                                        <dx:GridViewDataColumn FieldName="Aceptado" Caption="Aceptado" />
                                                                        <dx:GridViewDataColumn FieldName="Rechazado" Caption="Rechazado" />
                                                                    </Columns>
                                                                </dx:GridViewBandColumn>
                                                            </Columns>
                                                            <SettingsPager EnableAdaptivity="true" />
                                                            <Settings ShowFooter="true" />
                                                            <TotalSummary>
                                                                <dx:ASPxSummaryItem FieldName="Aceptado" SummaryType="Sum" DisplayFormat="{0}" />
                                                                <dx:ASPxSummaryItem FieldName="Rechazado" SummaryType="Sum" DisplayFormat="{0}" />
                                                            </TotalSummary>
                                                        </dx:ASPxGridView>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>

                                            <dx:TabPage Text="Tol Spec" Visible="true">
                                                <ContentCollection>
                                                    <dx:ContentControl runat="server">
                                                        <dx:ASPxGridView ID="grdtolSpc" runat="server" Theme="Metropolis" SettingsBehavior-AllowSort="false" KeyFieldName="Id_Style" OnBeforePerformDataSelect="grdtolSpc_BeforePerformDataSelect"
                                                            Width="100%">
                                                            <Columns>
                                                                <dx:GridViewBandColumn Caption="Detalle Tolerancias en Especificación" HeaderStyle-HorizontalAlign="Center">
                                                                    <Columns>
                                                                        <dx:GridViewDataColumn FieldName="Style" Caption="Estilo" Settings-AllowCellMerge="True" />
                                                                        <dx:GridViewDataColumn FieldName="NombreMedida" Caption="Punto de Medida" Settings-AllowCellMerge="False" />
                                                                        <dx:GridViewDataColumn FieldName="Tol-" Caption="Tol-" Settings-AllowCellMerge="False" />
                                                                        <dx:GridViewDataColumn FieldName="Spec" Caption="Spec" Settings-AllowCellMerge="False" />
                                                                        <dx:GridViewDataColumn FieldName="Tol+" Caption="Tol+" Settings-AllowCellMerge="False" />
                                                                        <dx:GridViewDataColumn FieldName="total" Caption="Total" Settings-AllowCellMerge="False" />
                                                                    </Columns>
                                                                </dx:GridViewBandColumn>
                                                            </Columns>
                                                            <SettingsBehavior AllowCellMerge="true" />
                                                            <SettingsPager EnableAdaptivity="true" />
                                                            <Settings ShowFooter="true" />
                                                            <TotalSummary>
                                                                <dx:ASPxSummaryItem FieldName="total" SummaryType="Sum" DisplayFormat="{0}" />
                                                            </TotalSummary>
                                                        </dx:ASPxGridView>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>

                                            <dx:TabPage Text="Out Spec" Visible="true">
                                                <ContentCollection>
                                                    <dx:ContentControl runat="server">
                                                        <dx:ASPxGridView ID="grdOutSpc" runat="server" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnBeforePerformDataSelect="grdOutSpc_BeforePerformDataSelect"
                                                            KeyFieldName="Id_Style" Width="100%">
                                                            <Columns>
                                                                <dx:GridViewBandColumn Caption="Detalle Fuera de Especificación" HeaderStyle-HorizontalAlign="Center">
                                                                    <Columns>
                                                                        <dx:GridViewDataColumn FieldName="Style" Caption="Estilo" Settings-AllowCellMerge="True" />
                                                                        <dx:GridViewDataColumn FieldName="NombreMedida" Caption="Punto de Medida" Settings-AllowCellMerge="False" />
                                                                        <dx:GridViewDataColumn FieldName="OOT-" Caption="OOT-" Settings-AllowCellMerge="False" />
                                                                        <dx:GridViewDataColumn FieldName="Spec" Caption="Spec" Settings-AllowCellMerge="False" />
                                                                        <dx:GridViewDataColumn FieldName="OOT+" Caption="OOT+" Settings-AllowCellMerge="False" />
                                                                        <dx:GridViewDataColumn FieldName="total" Caption="total" Settings-AllowCellMerge="False" />
                                                                    </Columns>
                                                                </dx:GridViewBandColumn>
                                                            </Columns>
                                                            <SettingsBehavior AllowCellMerge="true" />
                                                            <SettingsPager EnableAdaptivity="true" />
                                                            <Settings ShowFooter="true" />
                                                            <TotalSummary>
                                                                <dx:ASPxSummaryItem FieldName="total" SummaryType="Sum" DisplayFormat="{0}" />
                                                            </TotalSummary>
                                                        </dx:ASPxGridView>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>
                                        </TabPages>
                                    </dx:ASPxPageControl>
                                </div>
                            </DetailRow>
                        </Templates>
                        <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                        <Settings ShowFooter="true" />
                        <SettingsPager PageSize="100" />
                        <SettingsBehavior EnableCustomizationWindow="true" />
                        <SettingsDetail ShowDetailRow="true" />
                    </dx:ASPxGridView>
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                </div>
            </div>

        </div>

    </div>


    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

