﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_RepCutXDate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            txtfecha1.Date = DateTime.Now;
            txtfecha2.Date = DateTime.Now;
         //   RadioButtonList1.SelectedValue = "0";
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        
            if (DropDownListProcess.SelectedValue == "1")
            {
                XtraReportCortesXfecha rep = new XtraReportCortesXfecha();

                rep.Parameters[0].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                rep.Parameters[1].Value = txtfecha2.Date.ToString("yyyy-MM-dd");
                rep.Parameters[2].Value = RadioButtonList2.SelectedValue;

                ASPxWebDocumentViewer1.OpenReport(rep);
            }
            else if (DropDownListProcess.SelectedValue == "2")
            {
                XtraReportCortesXfechaIntex rep = new XtraReportCortesXfechaIntex();

                rep.Parameters[0].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                rep.Parameters[1].Value = txtfecha2.Date.ToString("yyyy-MM-dd");

                ASPxWebDocumentViewer1.OpenReport(rep);
            }
            else if (DropDownListProcess.SelectedValue == "3")
            {
                XtraReportCorteXfechaMod4 rep = new XtraReportCorteXfechaMod4();

                rep.Parameters[0].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                rep.Parameters[1].Value = txtfecha2.Date.ToString("yyyy-MM-dd");
               
                ASPxWebDocumentViewer1.OpenReport(rep);
            }
       
    }

    protected void DropDownListProcess_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "1")
        {
            Panel1.Visible = true;
        }
        else
        {
            Panel1.Visible = false;
        }
    }
}