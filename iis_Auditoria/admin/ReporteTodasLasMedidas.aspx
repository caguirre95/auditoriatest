﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteTodasLasMedidas.aspx.cs" Inherits="admin_ReporteTodasLasMedidas" %>


<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function date1() {
            $('#<%=txtfecha1.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

        function date2() {
            $('#<%=txtfecha2.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

    </script>

    <style>
        .m {
            margin-top: 25px;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <br />
    <div class="container">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

            <div class="panel panel-primary" style="margin-top: 5px">
                <div class="panel-heading">Reporte Todas las Medidas</div>
                <div class="panel-body">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="panel-default">
                                <div class="panel-heading">Select </div>
                                <div class="panel-body">
                                    <asp:Panel runat="server" ID="panelfechas">
                                        Start Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                        End Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                    </asp:Panel>
                                    <div style="padding-top: 15px">
                                        <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" runat="server" OnClick="btngenerar_Click" />
                                    </div>
                                    <div style="padding-top: 15px">
                                        <asp:Button Text="Limpiar Campos" CssClass="btn btn-info form-control" ID="btnlimpiar" runat="server" OnClick="btnlimpiar_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <hr />
                        <dx1:ASPxGridView ID="grdmiDD" 
                            runat="server" 
                            Width="100%" 
                            Theme="Metropolis" 
                            OnDataBinding="grdmiDD_DataBinding" 
                            OnCustomCallback="grdmiDD_CustomCallback"
                            Settings-HorizontalScrollBarMode="Auto" 
                            SettingsBehavior-AllowSort="false"
                            Styles-Cell-CssClass="textAlignLeft" 
                            ClientInstanceName="grid">
                            <SettingsBehavior AllowEllipsisInText="true" />
                            <SettingsResizing ColumnResizeMode="Control" />
                            <Settings ShowColumnHeaders="true" />
                        </dx1:ASPxGridView>

                        <asp:Button ID="cmdExportDD" runat="server" CssClass="btn btn-success form-control m" Text="Export to Excel" OnClick="cmdExportDD_Click" />
                        <dx1:ASPxGridViewExporter ID="exportDD" runat="server"></dx1:ASPxGridViewExporter>

                    </div>
                </div>
            </div>



        </div>
    </div>


</asp:Content>

