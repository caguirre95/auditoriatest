﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MultiLevel.aspx.cs" Inherits="admin_MultiLevel" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

        });

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">

        <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

        <div style="margin-top:5px">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Select search filter</div>
                    <div class="panel-body">
                        Start Date
                            <div class="input-group">

                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>

                            </div>
                        End Date
                          
                                <div class="input-group">
                                    <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                    <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                        <hr />
                       <%-- <dx:ASPxButton ID="ASPxButton1" OnClick="ASPxButton1_Click" Theme="Metropolis" runat="server" Text=""></dx:ASPxButton>--%>
                        <asp:Button ID="Button1" OnClick="ASPxButton1_Click" CssClass="btn btn-primary " Width="300px" runat="server" Text="Generate Report" />
                        <br />
                        <asp:Button ID="btnExport" OnClick="btnExport_Click" CssClass="btn btn-success " Width="300px" runat="server" Text="Export Excel" />
                        <hr />
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Select search filter</div>
                    <div class="panel-body">
                        Process
                      <asp:DropDownList ID="DropDownListProcess" CssClass="form-control" OnSelectedIndexChanged="DropDownListProcess_SelectedIndexChanged" AutoPostBack="true" runat="server">
                          <asp:ListItem Value="0"> Select...</asp:ListItem>
                          <asp:ListItem Value="1"> Produccion</asp:ListItem>
                          <asp:ListItem Value="2"> AfterWash</asp:ListItem>
                          <asp:ListItem Value="3"> Audit Final</asp:ListItem>
                      </asp:DropDownList>
                        <asp:Panel ID="Panel1" Visible="false" runat="server">
                            Seccion 
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                            <asp:ListItem Value="bultoabulto">Bulto a bulto</asp:ListItem>
                            <asp:ListItem Value="inspInter">Inspeccion Final</asp:ListItem>
                        </asp:RadioButtonList>
                        </asp:Panel>

                    </div>
                </div>

            </div>
        </div>
    </div>

     <dx:ASPxGridView ID="grid" ClientInstanceName="grid" Theme="Metropolis" runat="server" OnDataBinding="grid_DataBinding"
        Width="100%" AutoGenerateColumns="False" OnCustomCallback="grid_CustomCallback">
        <Columns>
            <dx:GridViewDataDateColumn FieldName="Style" GroupIndex="0" Settings-GroupInterval="Value" />
            <dx:GridViewDataDateColumn FieldName="POrder" />
            <dx:GridViewDataDateColumn FieldName="Size" />
            <dx:GridViewDataTextColumn FieldName="Area" />
            <dx:GridViewDataTextColumn FieldName="Medida" />
            <dx:GridViewDataTextColumn FieldName="+2" />
            <dx:GridViewDataTextColumn FieldName="+1 7/8" />
            <dx:GridViewDataTextColumn FieldName="+1 3/4" />
            <dx:GridViewDataTextColumn FieldName="+1 5/8" />
            <dx:GridViewDataTextColumn FieldName="+1 1/2" />
            <dx:GridViewDataTextColumn FieldName="+1 3/8" />
            <dx:GridViewDataTextColumn FieldName="+1 1/4" />
            <dx:GridViewDataTextColumn FieldName="+1 1/8" />
            <dx:GridViewDataTextColumn FieldName="+1" />
            <dx:GridViewDataTextColumn FieldName="+7/8" />
            <dx:GridViewDataTextColumn FieldName="+3/4" />
            <dx:GridViewDataTextColumn FieldName="+5/8" />
            <dx:GridViewDataTextColumn FieldName="+1/2" />
            <dx:GridViewDataTextColumn FieldName="+3/8" />
            <dx:GridViewDataTextColumn FieldName="+1/4" />
            <dx:GridViewDataTextColumn FieldName="+1/8" />
            <dx:GridViewDataTextColumn FieldName="+/-0" />
            <dx:GridViewDataTextColumn FieldName="-1/8" />
            <dx:GridViewDataTextColumn FieldName="-1/4" />
            <dx:GridViewDataTextColumn FieldName="-3/8" />
            <dx:GridViewDataTextColumn FieldName="-1/2" />
            <dx:GridViewDataTextColumn FieldName="-5/8" />
            <dx:GridViewDataTextColumn FieldName="-3/4" />
            <dx:GridViewDataTextColumn FieldName="-7/8" />
            <dx:GridViewDataTextColumn FieldName="-1" />
            <dx:GridViewDataTextColumn FieldName="-1 1/8" />
            <dx:GridViewDataTextColumn FieldName="-1 1/4" />
            <dx:GridViewDataTextColumn FieldName="-1 3/8" />
            <dx:GridViewDataTextColumn FieldName="-1 1/2" />
            <dx:GridViewDataTextColumn FieldName="-1 5/8" />
            <dx:GridViewDataTextColumn FieldName="-1 3/4" />
            <dx:GridViewDataTextColumn FieldName="-1 7/8" />
            <dx:GridViewDataTextColumn FieldName="-2" />
        </Columns>
        <Settings ShowGroupPanel="True"  ShowGroupFooter="VisibleIfExpanded" />
        <SettingsPager Mode="ShowAllRecords" />
        <GroupSummary>
           <dx:ASPxSummaryItem FieldName="+2" ShowInGroupFooterColumn="+2" SummaryType="Sum"   />
            <dx:ASPxSummaryItem FieldName="+1 7/8" ShowInGroupFooterColumn="+1 7/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+1 3/4" ShowInGroupFooterColumn="+1 3/4" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+1 5/8" ShowInGroupFooterColumn="+1 5/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+1 1/2" ShowInGroupFooterColumn="+1 1/2" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+1 3/8" ShowInGroupFooterColumn="+1 3/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+1 1/4" ShowInGroupFooterColumn="+1 1/4" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+1 1/8" ShowInGroupFooterColumn="+1 1/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+7/8" ShowInGroupFooterColumn="+7/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+3/4" ShowInGroupFooterColumn="+3/4" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+5/8" ShowInGroupFooterColumn="+5/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+1/2" ShowInGroupFooterColumn="+1/2" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+3/8" ShowInGroupFooterColumn="+3/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+1/4" ShowInGroupFooterColumn="+1/4" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+1/8" ShowInGroupFooterColumn="+1/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="+/-0" ShowInGroupFooterColumn="+/-0" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-1/8" ShowInGroupFooterColumn="-1/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-1/4" ShowInGroupFooterColumn="-1/4" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-3/8" ShowInGroupFooterColumn="-3/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-1/2" ShowInGroupFooterColumn="-1/2" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-5/8" ShowInGroupFooterColumn="-5/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-3/4" ShowInGroupFooterColumn="-3/4" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-7/8" ShowInGroupFooterColumn="-7/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-1" ShowInGroupFooterColumn="-1" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-1 1/8" ShowInGroupFooterColumn="-1 1/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-1 1/4" ShowInGroupFooterColumn="-1 1/4" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-1 3/8" ShowInGroupFooterColumn="-1 3/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-1 1/2" ShowInGroupFooterColumn="-1 1/2" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-1 5/8" ShowInGroupFooterColumn="-1 5/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-1 3/4" ShowInGroupFooterColumn="-1 3/4" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-1 7/8" ShowInGroupFooterColumn="-1 7/8" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName="-2" ShowInGroupFooterColumn="-2" SummaryType="Sum" />
        </GroupSummary>
    </dx:ASPxGridView>

    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

