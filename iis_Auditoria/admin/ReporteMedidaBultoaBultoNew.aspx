﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteMedidaBultoaBultoNew.aspx.cs" Inherits="admin_ReporteMedidaBultoaBultoNew" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({ 
                source: function (request, response) {
                    $.ajax({
                        url: "../MedidaNew.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    idstyle: item.idstyle,
                                    json: item 
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtstyle.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "Reportafterwash.aspx/GetStyle",
                        data: "{'pref' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    estilo: item.estilo,
                                    id: item.id,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);
                    $('#<%=hdnidstyle.ClientID%>').val(ui.item.id);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

        });

        function LimpiarFocus() {
            limpiar();            
        }

        function limpiar() {

            $('#<%=txtstyle.ClientID%>').val('');
            $('#<%=hdnidstyle.ClientID%>').val('');
            //$("table[id$='ASPxGridView1']").html(""); 
            ASPxGridView1.refresh(); // to refresh the content 
            ASPxGridView1.refreshColumns(); // to refresh the refreshColumns 
            ASPxGridView1.refreshHeader(); // to refresh the header content 

        }

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <link href="../css/font-awesome.css" rel="stylesheet" />

    <style>
        .es {
            margin-top:20px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <br />

    <div class="container">

        <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>
           <asp:HiddenField ID="hfidPorder" runat="server" />
                            <asp:HiddenField ID="hdnidstyle" runat="server" />
        <div class="panel panel-primary" style="margin-top: 5px">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Reporte Medidas Bulto a Bulto</strong></h3>
            </div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">

                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                    <asp:ListItem Value="1">Cut</asp:ListItem>
                                    <asp:ListItem Value="2">Style</asp:ListItem>
                                    <asp:ListItem Value="3">Line</asp:ListItem>
                                    <asp:ListItem Value="4">Plant</asp:ListItem>
                                </asp:RadioButtonList>
                                <hr />
                                <a href="#ventanaG" data-toggle="modal" style="width: 161px" class="btn btn-md btn-success"><span class="glyphicon glyphicon-align-justify"></span> Ver Estilos</a>
                                <asp:Button ID="btnr" runat="server" Text="Reinicar > " OnClick="btnr_Click" />
                                <hr />                                
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                Cut:                        
                                <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                Style:
                                <asp:TextBox ID="txtstyle" placeholder="Style" onfocus="LimpiarFocus()" class="textboxAuto text-info form-control" Font-Size="14px" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                Line:
                                <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                                Plant:
                                <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>                                
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                Start Date
                                <div class="input-group">
                                    <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                    <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                End Date                          
                                <div class="input-group">
                                    <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                    <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <br />
                                <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" OnClick="btngenerar_Click" runat="server" />

                                <asp:Button Text="Export Report" CssClass="btn btn-success form-control es" ID="btnexport" OnClick="btnexport_Click" runat="server" />
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <hr />
                        <dx:ASPxGridView ID="grdmiDD" 
                            runat="server" 
                            Width="100%" 
                            Theme="Metropolis" 
                            OnDataBinding="grdmiDD_DataBinding" 
                            OnCustomCallback="grdmiDD_CustomCallback"
                            Settings-HorizontalScrollBarMode="Auto" 
                            SettingsBehavior-AllowSort="false"
                            Styles-Cell-CssClass="textAlignLeft" 
                            ClientInstanceName="grid">
                            <SettingsBehavior AllowEllipsisInText="true" />
                            <SettingsResizing ColumnResizeMode="Control" />
                            <Settings ShowColumnHeaders="true" />
                        </dx:ASPxGridView>

                        <asp:Button ID="cmdExportDD" runat="server" CssClass="btn btn-success form-control m" Text="Export to Excel" OnClick="cmdExportDD_Click" />
                        <dx:ASPxGridViewExporter ID="exportDD" runat="server"></dx:ASPxGridViewExporter>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <hr />
                      
                        <dx:ASPxGridView ID="ASPxGridView1" OnDataBinding="ASPxGridView1_DataBinding" OnCustomCallback="ASPxGridView1_CustomCallback" OnDataBound="ASPxGridView1_DataBound" Width="100%" Theme="Metropolis" runat="server">                            
                            <Settings ShowFooter="false" />                            
                            <SettingsPager PageSize="36">
                            </SettingsPager>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                    </div>



                </div>

            </div>
        </div>

    </div>

    <aside class="modal fade"  id="ventanaG">
        <div class="modal-dialog">
            <div class="modal-content">
                <article class="modal-header">
                    <button id="cerrarG" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                    <h4>Estilos Ingresados en la Semana </h4>
                </article>
                <article class="modal-body">
                    <asp:GridView runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White"  CssClass="table  table-hover table-striped " ID="grvestilos">

                    </asp:GridView>
                </article>
            </div>
        </div>
    </aside>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

