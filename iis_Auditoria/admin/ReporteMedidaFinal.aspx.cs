﻿using DevExpress.Web;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReporteMedidaFinal : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();
            Session["embarqueAl"] = null;
            Session["intexmFIN_DD"] = null;
            cargacombo();
            cargagridestilos();
        }
    }

    void cargagridestilos()
    {
        string query = " select Style from tbMedidasFinalAL fa join Style s on s.Id_Style = fa.idEstilo"
                      + " where DATEPART(WEEk,fechaRegistro) = (select DATEPART(WEEK,GETDATE())) and DATEPART(YEAR,fechaRegistro) = (select DATEPART(YEAR,GETDATE()))"
                      + " group by Style";

        DataTable dtEstilos = DataAccess.Get_DataTable(query);

        grvestilos.DataSource = dtEstilos;
        grvestilos.DataBind();
    }

    void cargacombo()
    {
        string sqline = "select id_linea,numero from Linea l where l.numero not like '%-%' and l.numero not like '%A%' order by CONVERT(float,numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT [id_planta],[descripcion] FROM[Auditoria].[dbo].[Planta]";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
        DropDownListPlant.Items.Insert(5, "All Plant");

    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            Session["embarqueAl"] = null;

            DataTable prod = funcionCarga();

            if (prod == null)
            {
                return;
            }

            int contCol = prod.Columns.Count;

            if (contCol == 0)
            {
                Session["embarqueAl"] = prod;
                ASPxGridView1.DataBind();
            }
            else
            {
                DataRow workRow;

                workRow = prod.NewRow();

                workRow[1] = "Total";

                for (int i = 2; i < contCol; i++)
                {
                    int suma = 0;

                    for (int j = 0; j < 33; j++)
                    {
                        if (!string.IsNullOrEmpty(prod.Rows[j][i].ToString()))
                        {
                            suma = suma + Convert.ToInt32(prod.Rows[j][i].ToString());
                        }

                    }
                    workRow[i] = suma;
                }

                prod.Rows.Add(workRow);

                Session["embarqueAl"] = prod;
                ASPxGridView1.DataBind();
            }

            string resp = RadioButtonList1.SelectedValue;
            if (resp == "4")
            {
                if (DropDownListPlant.SelectedItem.Text == "All Plant")
                {
                    var consultaDD = new DataTable();
                    string sqlquery = "EXEC dbo.Medidas_Contadas_FINAL_SP_Qry @desde='" + txtfecha1.Text + "', @hasta='" + txtfecha2.Text + "';";
                    consultaDD = DataAccess.Get_DataTable(sqlquery);
                    Session["intexmFIN_DD"] = consultaDD;
                    grdmiDD.DataBind();
                }
            }
        }
        catch (Exception ex)
        {

          
            string a = ex.Message;
            //throw;
        }
    }

    DataTable funcionCarga()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);

        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string resp = RadioButtonList1.SelectedValue;

        string query = "";

        DataTable prod = new DataTable();

        switch (resp)
        {
            case "1"://cut
                if (txtPorder.Text != string.Empty)
                {

                    if (!string.IsNullOrEmpty(hfidPorder.Value))
                    {
                        int idc = Convert.ToInt32(hfidPorder.Value);

                        prod = OrderDetailDA.ReporteFinalDinamicCorte(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), idc);
                    }
                }
                break;

            case "2"://style
                if (txtstyle.Text != string.Empty)
                {

                    if (!string.IsNullOrEmpty(hdnidstyle.Value))
                    {
                        int ides = Convert.ToInt16(hdnidstyle.Value);

                        prod = OrderDetailDA.ReporteFinalDinamicStyle(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), ides);

                    }
                }
                break;

            case "3"://line
                if (DropDownListLine.SelectedItem.Text != "Select...")
                {
                    prod = OrderDetailDA.ReporteFinalDinamicLine(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListLine.SelectedValue));
                }
                break;

            case "4"://plant
                if (DropDownListPlant.SelectedItem.Text == "All Plant")
                {
                    prod = OrderDetailDA.ReporteFinalDinamicplanAll(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));
                }
                else if (DropDownListPlant.SelectedItem.Text != "All Plant" && DropDownListPlant.SelectedItem.Text != "Select...")
                {
                    prod = OrderDetailDA.ReporteFinalDinamicplan(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListPlant.SelectedValue));
                }
                break;

            default:

                break;
        }

        return prod;

    }

    //void funcionCarga()
    //{
    //    DateTime dtime = DateTime.Parse(txtfecha1.Text);

    //    DateTime dtime1 = DateTime.Parse(txtfecha2.Text);        

    //    string resp = RadioButtonList1.SelectedValue;

    //    string query = "";

    //    switch (resp)
    //    {
    //        case "1"://cut
    //            if (txtPorder.Text != string.Empty)
    //            {
    //                //query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([SEAT],'') SEAT, isnull([INSEAM],'') INSEAM, isnull([LOW HIP],'') LOWHIP, isnull([FRONT RISE],'') FRONTRISE, isnull([BACK RISE],'') BACKRISE from"
    //                //      + " (select a.Medida medidaTexto, ISNULL(b.total, 0) total, b.NombreMedida, a.valor from (select idMedida, Medida, valor from tblMedidas) a"
    //                //      + " left join (select mep.medidaTexto, COUNT(mep.medidaTexto) total, pm.NombreMedida, mep.medidaFlotante from POrder p "
    //                //      + " join tbMedidasFinalAL me on me.idPorder = p.Id_Order join tbMedidasFinalP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida"
    //                //      + " where p.POrder = '" + txtPorder.Text.Trim() + "' group by mep.medidaTexto, pm.NombreMedida, mep.medidaFlotante) b on a.valor = b.medidaFlotante group by a.Medida, b.total, b.NombreMedida, a.valor"
    //                //      + " ) a pivot(sum(total) for NombreMedida in ([WAIST], [SEAT], [INSEAM], [LOW HIP], [FRONT RISE], [BACK RISE])) as sec order by valor desc";

    //                //DataTable prod = DataAccess.Get_DataTable(query);

    //                query = "select Id_Order from Porder where POrder = '" + txtPorder.Text.Trim() + "'";
    //                DataTable cort = DataAccess.Get_DataTable(query);

    //                if (cort.Rows.Count > 0)
    //                {
    //                    int idc = Convert.ToInt32(cort.Rows[0][0]);

    //                    DataTable prod = new DataTable();
    //                    prod = OrderDetailDA.ReporteFinalDinamicCorte(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), idc);
    //                    Session["embarqueAl"] = prod;
    //                    ASPxGridView1.DataBind();
    //                }
    //            }
    //            else
    //            {
    //                resp = "fin";
    //            }

    //            break;

    //        case "2"://style
    //            if (txtstyle.Text != string.Empty)
    //            {
    //                //query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([SEAT],'') SEAT, isnull([INSEAM],'') INSEAM, isnull([LOW HIP],'') LOWHIP, isnull([FRONT RISE],'') FRONTRISE, isnull([BACK RISE],'') BACKRISE"
    //                //      + " from (select a.Medida medidaTexto, ISNULL(b.total, 0) total, b.NombreMedida, a.valor from (select idMedida, Medida, valor from tblMedidas) a"
    //                //      + " left join (select mep.medidaTexto, COUNT(mep.medidaTexto) total, pm.NombreMedida, mep.medidaFlotante from POrder p"
    //                //      + " join tbMedidasFinalAL me on me.idPorder = p.Id_Order join tbMedidasFinalP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida join Style s on s.Id_Style = p.Id_Style"
    //                //      + " where s.Style = '" + txtstyle.Text.Trim() + "' and CONVERT(date, me.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "' group by mep.medidaTexto, pm.NombreMedida, mep.medidaFlotante) b on a.valor = b.medidaFlotante group by a.Medida, b.total, b.NombreMedida, a.valor"
    //                //      + " ) a pivot(sum(total) for NombreMedida in ([WAIST], [SEAT], [INSEAM], [LOW HIP], [FRONT RISE], [BACK RISE])) as sec order by valor desc";

    //                //DataTable prod = DataAccess.Get_DataTable(query);

    //                query = "select Id_Style from Style where Style = '" + txtstyle.Text.Trim() + "'";
    //                DataTable st = DataAccess.Get_DataTable(query);

    //                if (st.Rows.Count > 0)
    //                {
    //                    int ides = Convert.ToInt16(st.Rows[0][0]);

    //                    DataTable prod = new DataTable();
    //                    prod = OrderDetailDA.ReporteFinalDinamicStyle(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), ides);
    //                    Session["embarqueAl"] = prod;
    //                    ASPxGridView1.DataBind();
    //                }                    
    //            }
    //            else
    //            {
    //                resp = "fin";
    //            }

    //            break;

    //        case "3"://line
    //            if (DropDownListLine.SelectedItem.Text != "Select...")
    //            {
    //                //query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([SEAT],'') SEAT, isnull([INSEAM],'') INSEAM, isnull([LOW HIP],'') LOWHIP, isnull([FRONT RISE],'') FRONTRISE, isnull([BACK RISE],'') BACKRISE"
    //                //      + " from (select a.Medida medidaTexto, ISNULL(b.total, 0) total, b.NombreMedida, a.valor from (select idMedida, Medida, valor from tblMedidas) a"
    //                //      + " left join (select mep.medidaTexto, COUNT(mep.medidaTexto) total, pm.NombreMedida, mep.medidaFlotante from POrder p"
    //                //      + " join tbMedidasFinalAL me on me.idPorder = p.Id_Order join tbMedidasFinalP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida join Style s on s.Id_Style = p.Id_Style"
    //                //      + " where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, me.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "' group by mep.medidaTexto, pm.NombreMedida, mep.medidaFlotante) b on a.valor = b.medidaFlotante group by a.Medida, b.total, b.NombreMedida, a.valor"
    //                //      + " ) a pivot(sum(total) for NombreMedida in ([WAIST], [SEAT], [INSEAM], [LOW HIP], [FRONT RISE], [BACK RISE])) as sec order by valor desc";

    //                //DataTable prod = DataAccess.Get_DataTable(query);
    //                DataTable prod = new DataTable();
    //                prod = OrderDetailDA.ReporteFinalDinamicLine(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListLine.SelectedValue));
    //                Session["embarqueAl"] = prod;
    //                ASPxGridView1.DataBind();
    //            }
    //            else
    //            {
    //                resp = "fin";
    //            }

    //            break;

    //        case "4"://plant
    //            if (DropDownListPlant.SelectedItem.Text == "Select...")
    //            {
    //                resp = "fin";
    //            }
    //            else if (DropDownListPlant.SelectedItem.Text == "All Plant")
    //            {
    //                //query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([SEAT],'') SEAT, isnull([INSEAM],'') INSEAM, isnull([LOW HIP],'') LOWHIP, isnull([FRONT RISE],'') FRONTRISE, isnull([BACK RISE],'') BACKRISE"
    //                //      + " from (select a.Medida medidaTexto, ISNULL(b.total, 0) total, b.NombreMedida, a.valor from (select idMedida, Medida, valor from tblMedidas) a"
    //                //      + " left join (select mep.medidaTexto, COUNT(mep.medidaTexto) total, pm.NombreMedida, mep.medidaFlotante from POrder p"
    //                //      + " join tbMedidasFinalAL me on me.idPorder = p.Id_Order join tbMedidasFinalP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida join Style s on s.Id_Style = p.Id_Style"
    //                //      + " where CONVERT(date, me.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "' group by mep.medidaTexto, pm.NombreMedida, mep.medidaFlotante) b on a.valor = b.medidaFlotante group by a.Medida, b.total, b.NombreMedida, a.valor"
    //                //      + " ) a pivot(sum(total) for NombreMedida in ([WAIST], [SEAT], [INSEAM], [LOW HIP], [FRONT RISE], [BACK RISE])) as sec order by valor desc";

    //                DataTable prod = new DataTable();
    //                prod = OrderDetailDA.ReporteFinalDinamicplanAll(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));                    
    //                Session["embarqueAl"] = prod;
    //                ASPxGridView1.DataBind();
    //            }
    //            else
    //            {
    //                //query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([SEAT],'') SEAT, isnull([INSEAM],'') INSEAM, isnull([LOW HIP],'') LOWHIP, isnull([FRONT RISE],'') FRONTRISE, isnull([BACK RISE],'') BACKRISE"
    //                //      + " from (select a.Medida medidaTexto, ISNULL(b.total, 0) total, b.NombreMedida, a.valor from (select idMedida, Medida, valor from tblMedidas) a"
    //                //      + " left join (select mep.medidaTexto, COUNT(mep.medidaTexto) total, pm.NombreMedida, mep.medidaFlotante from POrder p"
    //                //      + " join tbMedidasFinalAL me on me.idPorder = p.Id_Order join tbMedidasFinalP mep on mep.id = me.id join tblPuntosMedida pm on pm.idPuntosM = mep.idPuntoMedida join Style s on s.Id_Style = p.Id_Style"
    //                //      + " where p.Id_Planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date, me.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "' group by mep.medidaTexto, pm.NombreMedida, mep.medidaFlotante) b on a.valor = b.medidaFlotante group by a.Medida, b.total, b.NombreMedida, a.valor"
    //                //      + " ) a pivot(sum(total) for NombreMedida in ([WAIST], [SEAT], [INSEAM], [LOW HIP], [FRONT RISE], [BACK RISE])) as sec order by valor desc";

    //                DataTable prod = new DataTable();
    //                prod = OrderDetailDA.ReporteFinalDinamicplan(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListPlant.SelectedValue));
    //                Session["embarqueAl"] = prod;
    //                ASPxGridView1.DataBind();
    //            }

    //            break;

    //        default:
    //            resp = "fin";
    //            break;
    //    }
    //}

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView1.DataSource = Session["embarqueAl"];
    }

    protected void ASPxGridView1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView1.DataBind();
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView1.DataBind();

            ASPxGridViewExporter1.GridViewID = "ASPxGridView1";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreatePageForEachLink();
            //compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Reporte-FinalAL";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=MFinal.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void ASPxGridView1_DataBound(object sender, EventArgs e)
    {
        ASPxGridView grid = (ASPxGridView)sender;
        foreach (GridViewDataColumn c in grid.Columns)
        {
            if ((c.FieldName.ToString()).StartsWith("medidaDecimal"))
            {
                c.Visible = false;
            }
        }
    }



    protected void cmdExportDD_Click(object sender, EventArgs e)
    {
        try
        {
            exportDD.GridViewID = "grdmiDD";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = exportDD;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link3 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Detalle Medidas";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=DetalleMedidasFinales-" + DateTime.Now.ToShortDateString() + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void grdmiDD_DataBinding(object sender, EventArgs e)
    {
        grdmiDD.DataSource = Session["intexmFIN_DD"];
    }

    protected void grdmiDD_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdmiDD.DataBind();
    }

}