﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportePVDIntexMedida.aspx.cs" Inherits="admin_MultinivelXCorte" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script type="text/javascript">
        <%--$(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

         });--%>

        
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {    
                    $.ajax({
                        url: "MultinivelXCorte.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="container">

         <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control" Visible="false"></asp:SiteMapPath>
         <br />
        <div style="margin-top:5px">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Select search filter</div>
                    <div class="panel-body">                        

                        Codigo Corte                           
                                <div class="input-group">
                                    <i class="input-group-addon" aria-hidden="true"></i>
                                     <asp:TextBox ID="txtPorder" placeholder="Codigo Corte" required="true" AutoCompleteType="Disabled" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                      <asp:HiddenField ID="hfidPorder" runat="server" />
                                </div>
                        <hr />
                       <%-- <dx:ASPxButton ID="ASPxButton1" OnClick="ASPxButton1_Click" Theme="Metropolis" runat="server" Text=""></dx:ASPxButton>--%>
                        <asp:Button ID="Button1" OnClick="ASPxButton1_Click" CssClass="btn btn-primary form-control" runat="server" Text="Generate Report" />
                        <hr />  
                        <asp:Button ID="btnExport" OnClick="btnExport_Click" CssClass="btn btn-success form-control" runat="server" Text="Export Excel" />
                        <hr />
                    </div>
                </div>

            </div>            
        </div>
    </div>

    <dx:ASPxGridView ID="grid" runat="server" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnDataBinding="grid_DataBinding" OnCustomCallback="grid_CustomCallback">
        <SettingsPager Mode="ShowAllRecords" />
    </dx:ASPxGridView>

    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
    <hr />  
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

