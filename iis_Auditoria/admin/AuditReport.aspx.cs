﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SistemaAuditores.DataAccess;
using System.Data;
using DevExpress.Utils;
using DevExpress.XtraCharts;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;
using DevExpress.XtraPrinting;
//using iTextSharp.text;
//using iTextSharp.text.html.simpleparser;
//using iTextSharp.text.pdf;

public partial class AuditReport : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtfecha1.Date = DateTime.Now.Date;
            llenarcombo();
        }
    }

    void llenarcombo()
    {

        DataTable dt_linea = DataAccess.Get_DataTable("select id_linea, numero from Linea where idcliente = 11 and numero not like '%-%' and numero not like '%A%' order by numero asc");

        DropDownList1.DataSource = dt_linea;
        DropDownList1.DataValueField = "id_linea";
        DropDownList1.DataTextField = "numero";
        DropDownList1.DataBind();

        DropDownList1.SelectedIndex = 1;
    }

    public void Reporte_Bihorario()
    {
       /* DataTable aql_aprov = new DataTable();
        DataTable aql_reject = new DataTable();
        int Id_Biohorario = 0;
        string hora_actual = DateTime.Now.Hour.ToString();
        string minutos_actual = DateTime.Now.Minute.ToString();
        string date_now = hora_actual + '.' + minutos_actual;
        DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio FROM Biohorario WHERE @input between rango1 and rango2");
        if (Bihorarios.Rows.Count > 0)
        {
            Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);
        }
        DataTable Bio_auditados = DataAccess.Get_DataTable("SELECT id_bio, bihorario FROM Biohorario WHERE id_bio<=" + Id_Biohorario);
        cbxbihorario.DataSource = Bio_auditados;
        cbxbihorario.DataBind();
        cbxbihorario.TextField = "bihorario";
        cbxbihorario.ValueField = "id_bio";

        DateTime fecha_actual = DateTime.Now;
        string sqlfecha_actual = fecha_actual.ToString("yyyy-MM-dd");
        int bio_seleccionado = Convert.ToInt16(cbxbihorario.SelectedItem.Value);

        string query = "select BA.Id_Bundle, BA.Id_Order, B.Quantity, M.sample_size from BundleApprovedBulto BA inner join Bundle B on BA.Id_Bundle=B.Id_Bundle inner join Muestreo M on BA.id_muestreo=M.id_muestreo where BA.id_bio=" + bio_seleccionado + " and BA.FechaAprobado='" + sqlfecha_actual + "'";
        string query2 = "select BR.Id_Bundle, BR.Id_Order, B.Quantity, M.sample_size from BundleRejectedBulto BR inner join Bundle B on BR.Id_Bundle=B.Id_Bundle inner join Muestreo M on BR.id_muestreo=M.id_muestreo where BR.id_bio=" + bio_seleccionado + " and BR.FechaRecibido='" + sqlfecha_actual + "'";
        //Reporte de Auditoria por Bulto
        aql_aprov = DataAccess.Get_DataTable(query);
        int total_aprov = 0, total_rejec = 0;
        //, qty_aprov=0, qty_reject=0;
        for (int i = 0; i <= aql_aprov.Rows.Count - 1; i++)
        {
            /*if(Convert.ToInt16(aql_aprov.Rows[i][3])==5)
            {
                qty_aprov = 8;	
                total_aprov = total_aprov + Convert.ToInt16(qty_aprov);				
            }
            else
            {
            total_aprov = total_aprov + Convert.ToInt16(aql_aprov.Rows[i][2]);
            //}
        }
        aql_reject = DataAccess.Get_DataTable(query2);
        for (int j = 0; j <= aql_reject.Rows.Count - 1; j++)
        {
            if(Convert.ToInt16(aql_reject.Rows[j][3])==5)
            {
                qty_reject = 8;	
                total_rejec = total_rejec + Convert.ToInt16(qty_reject);				
            }
            else
            {
            total_rejec = total_rejec + Convert.ToInt16(aql_reject.Rows[j][2]);
            //}
        }
        lblAcep.Text = Convert.ToString(aql_aprov.Rows.Count);
        lblReje.Text = Convert.ToString(aql_reject.Rows.Count);
        //Total de Bultos Auditados
        decimal total_bultos = Convert.ToDecimal(aql_aprov.Rows.Count) + Convert.ToDecimal(aql_reject.Rows.Count);
        string piezas = Convert.ToString(total_aprov + total_rejec);
        lbltotbultau.Text = total_bultos + "/" + piezas;
        //%Calidad
        decimal calidad = 0;
        if (total_bultos > 0)
        {
            calidad = (Convert.ToDecimal(aql_aprov.Rows.Count) * 100) / total_bultos;
        }
        calidad = (decimal)Math.Round(Convert.ToDecimal(calidad), 0);
        lblcalidad.Text = Convert.ToString(calidad) + "%";

        //OQL Piezas Auditadas

        string queryOQL = "select isnull(max(m.sample_size),0) as Muestreo from BundleApprovedBulto bd inner join Muestreo m on bd.id_muestreo=m.id_muestreo where bd.id_bio=" + bio_seleccionado + " and FechaAprobado='" + sqlfecha_actual + "'";
        DataTable oql = DataAccess.Get_DataTable(queryOQL);
        int muestreo = 0;
        if (Convert.ToInt16(oql.Rows[0][0]) == 5)
        {
            muestreo = 8;
        }
        else
        {
            muestreo = Convert.ToInt16(oql.Rows[0][0]);
        }
        string queryOQLRej = "select Id_Seccion,cantidad_defectos from BundleRejectedBulto where id_bio=" + bio_seleccionado + " and FechaRecibido='" + sqlfecha_actual + "'";

        DataTable oqlreje = DataAccess.Get_DataTable(queryOQLRej);
        decimal rejectoql = 0;
        decimal oqltra = 0, oqldel = 0, oqlen = 0, oqlen1 = 0;
        DataTable dt_linea = DataAccess.Get_DataTable("select id_linea from Linea where numero='" + Convert.ToInt16(DropDownList1.SelectedValue) + "'");
        int id_linea = Convert.ToInt16(dt_linea.Rows[0][0]);
        DataTable secciones = DataAccess.Get_DataTable("select S.id_seccion, M.Modulo from Modulo M inner join Seccion S on M.id_seccion=S.id_seccion where S.id_linea=" + id_linea);

        for (int i = 0; i <= oqlreje.Rows.Count - 1; i++)
        {
            rejectoql = rejectoql + Convert.ToInt16(oqlreje.Rows[i][1]);
            if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[0][0]))
            {
                oqltra = oqltra + Convert.ToInt16(oqlreje.Rows[i][1]);
            }
            if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[1][0]))
            {
                oqldel = oqldel + Convert.ToInt16(oqlreje.Rows[i][1]);
            }
            if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[2][0]))
            {
                oqlen = oqlen + Convert.ToInt16(oqlreje.Rows[i][1]);
            }
            if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[3][0]))
            {
                oqlen1 = oqlen1 + Convert.ToInt16(oqlreje.Rows[i][1]);
            }
        }
        decimal oqlapr = Math.Round((Convert.ToDecimal(Convert.ToInt16(aql_aprov.Rows.Count) * muestreo) - rejectoql), 0);

        lblOQLacep.Text = Convert.ToString(oqlapr);
        decimal TotalOql = Convert.ToDecimal(oqlapr) + rejectoql;
        if (TotalOql > 0)
        {
            decimal porReject = Math.Round(((rejectoql * 100) / TotalOql), 0);
            decimal porAcep = Math.Round(((Convert.ToDecimal(oqlapr) * 100) / TotalOql), 0);
            lblOQLporRech.Text = porReject + "%";
            lblOQLporAcep.Text = porAcep + "%";
            lblOQLrec.Text = Convert.ToString(rejectoql);
        }

        if (rejectoql > 0)
        {
            if (oqltra > 0)
            {
                lblSecTra.Text = Convert.ToString(((oqltra * 100) / rejectoql)) + "%";
            }
            else
            {
                lblSecTra.Text = "100%";
            }
            if (oqldel > 0)
            {
                lblSecDel.Text = Convert.ToString(((oqldel * 100) / rejectoql)) + "%";
            }
            else
            {
                lblSecDel.Text = "100%";
            }
            if (oqlen > 0)
            {
                lblSecEn1.Text = Convert.ToString(Math.Round(((oqlen * 100) / rejectoql), 0)) + "%";
            }
            else
            {
                lblSecEn1.Text = "100%";
            }
            if (oqlen1 > 0)
            {
                lblSecEn2.Text = Convert.ToString(Math.Round(((oqlen1 * 100) / rejectoql), 0)) + "%";
            }
            else
            {
                lblSecEn2.Text = "100%";
            }
        }
        lblOQLtotp.Text = Convert.ToString(Convert.ToInt16(lblOQLacep.Text) + Convert.ToInt16(lblOQLrec.Text));*/
    }

    public DataTable get_bundle_approv(string bihorario, string fecha_actual)
    {
        DataTable dt_values = new DataTable();
        dt_values.Columns.Add("Qty_bdlapprov", typeof(int));
        dt_values.Columns.Add("Tot_bdlapprov", typeof(int));
        DataTable aql_aprov = new DataTable();
        string query = "select BA.Id_Bundle, BA.Id_Order, B.Quantity, M.sample_size from BundleApprovedBulto BA inner join Bundle B on BA.Id_Bundle=B.Id_Bundle inner join Muestreo M on BA.id_muestreo=M.id_muestreo where BA.id_bio=" + bihorario + " and BA.FechaAprobado='" + fecha_actual + "'";
        //Reporte de Auditoria por Bulto
        aql_aprov = DataAccess.Get_DataTable(query);
        if (aql_aprov.Rows.Count > 0)
        {
            int total_aprov = 0;
            for (int i = 0; i <= aql_aprov.Rows.Count - 1; i++)
            {
                total_aprov = total_aprov + Convert.ToInt16(aql_aprov.Rows[i][2]);
            }
            dt_values.Rows.Add(aql_aprov.Rows.Count, total_aprov);
            return dt_values;
        }
        else
        {
            dt_values.Rows.Add(0, 0);
            return dt_values;
        }
        
    }

    public DataTable get_bundle_reject(string bihorario, string fecha_actual)
    {        
        DataTable dt_values = new DataTable();
        dt_values.Columns.Add("Qty_bdlreject", typeof(int));
        dt_values.Columns.Add("Tot_bdlreject", typeof(int));
        string query2 = "select BR.Id_Bundle, BR.Id_Order, B.Quantity, M.sample_size from BundleRejectedBulto BR inner join Bundle B on BR.Id_Bundle=B.Id_Bundle inner join Muestreo M on BR.id_muestreo=M.id_muestreo where BR.id_bio=" + bihorario + " and BR.FechaRecibido='" + fecha_actual + "'";
        //Reporte de Auditoria por Bulto
        DataTable aql_reject = new DataTable();
        aql_reject = DataAccess.Get_DataTable(query2);
        if (aql_reject.Rows.Count > 0)
        {
            int total_rejec = 0;
            for (int j = 0; j <= aql_reject.Rows.Count - 1; j++)
            {
                total_rejec = total_rejec + Convert.ToInt16(aql_reject.Rows[j][2]);
            }
            dt_values.Rows.Add(aql_reject.Rows.Count, total_rejec);
            return dt_values;
        }
        else
        {
            dt_values.Rows.Add(0, 0);
            return dt_values;
        }
    }


    public void crear_reporte()
    {
        DataTable bihorarios = DataAccess.Get_DataTable("select id_bio, bihorario from Biohorario where id_bio<=6");
        HtmlTable table1 = new HtmlTable();
        int Rtot_bundAprov = 0, Rtot_bunReject = 0, TBA=0, TPA=0, tot_piezasAprov=0, tot_piezasReject=0, SumTotPiezas=0;
        int contador_porcentaje = 0;
        decimal TotPorcentaje = 0, TPOQL_A = 0, TPOQL_R = 0, TPOQL=0;
        // Set the table's formatting-related properties.
        table1.ID = "reporte";
        string fecha = txtfecha1.Date.ToString("yyyy-MM-dd");
        // Start adding content to the table.
        HtmlTableRow row;
        HtmlTableCell cell1, cell2, cell3;
        for (int i = 0; i <=11 ; i++)
        {
            // Create a new row and set its background color.
            row = new HtmlTableRow();
            for (int j = 0; j <= bihorarios.Rows.Count; j++)
            {
                if (i == 0)
                {
                    if (j == 0)
                    {
                        // Create a cell and set its text.
                        cell1 = new HtmlTableCell();
                        cell1.InnerText = "";
                        cell1.Style.Value = "border-left:1px solid #ccc;border-right:1px solid #ccc;";
                        // Add the cell to the current row.
                        row.Cells.Add(cell1);
                        cell2 = new HtmlTableCell();
                        cell2.Style.Value = "border-right:1px solid #ccc;";
                        cell2.InnerText = "";
                        cell2.Height = "50px";
                        row.Cells.Add(cell2);
                        cell3 = new HtmlTableCell();
                        cell3.Style.Value = "text-align:center;font-weight:bold;border-right:1px solid #ccc;";
                        cell3.InnerText = Convert.ToString(bihorarios.Rows[j][1]);
                        cell3.Height = "50px";
                        row.Cells.Add(cell3);
                    }
                    else
                    {
                        if (j < bihorarios.Rows.Count)
                        {
                            cell3 = new HtmlTableCell();
                            cell3.Style.Value = "text-align:center;font-weight:bold;border-right:1px solid #ccc;";
                            cell3.InnerText = Convert.ToString(bihorarios.Rows[j][1]);
                            cell3.Height = "50px";
                            row.Cells.Add(cell3);
                        }
                        else
                        {
                            cell3 = new HtmlTableCell();
                            cell3.Style.Value = "text-align:center;font-weight:bold;border-right:1px solid #ccc;";
                            cell3.InnerText = "TOTAL";
                            cell3.Height = "50px";
                            row.Cells.Add(cell3);
                        }
                    }
                }
                if (i==1)
                {
                    if (j == 0)//CELDA AQL/ CANTIDAD DE BULTOS APROBADOS Y RECHAZADOS
                    {
                        // Create a cell and set its text.
                        cell1 = new HtmlTableCell();
                        cell1.RowSpan = 3;
                        cell1.Style.Value = "font-weight:bold;text-align:center;border-left:1px solid #ccc;border-right:1px solid #ccc;";
                        cell1.InnerText = "AQL";
                        // Add the cell to the current row.
                        row.Cells.Add(cell1);
                        cell2 = new HtmlTableCell();
                        cell2.Style.Value = "border-right:1px solid #ccc;";
                        cell2.InnerText = "Reporte de Auditoria por Bulto";
                        cell2.Height = "50px";
                        row.Cells.Add(cell2);
                        cell3 = new HtmlTableCell();
                        cell3.Style.Value = "border-right:1px solid #ccc;";
                        DataTable bundles_approv = get_bundle_approv(Convert.ToString(bihorarios.Rows[j][0]), fecha);
                        DataTable bundles_reject = get_bundle_reject(Convert.ToString(bihorarios.Rows[j][0]), fecha);
                        cell3.InnerHtml = "A=" + Convert.ToInt16(bundles_approv.Rows[0][0]) + "<br/> R=" + Convert.ToInt16(bundles_reject.Rows[0][0]);
                        Rtot_bundAprov = Rtot_bundAprov + Convert.ToInt16(bundles_approv.Rows[0][0]);
                        Rtot_bunReject = Rtot_bunReject + Convert.ToInt16(bundles_reject.Rows[0][0]);
                        cell3.Height = "50px";
                        row.Cells.Add(cell3);
                    }
                    else
                    {
                        if (j < bihorarios.Rows.Count)
                        {
                            cell3 = new HtmlTableCell();
                            cell3.Style.Value = "border-right:1px solid #ccc;";
                            DataTable bundles_approv = get_bundle_approv(Convert.ToString(bihorarios.Rows[j][0]), fecha);
                            DataTable bundles_reject = get_bundle_reject(Convert.ToString(bihorarios.Rows[j][0]), fecha);
                            cell3.InnerHtml = "A=" + Convert.ToInt16(bundles_approv.Rows[0][0]) + "<br/> R=" + Convert.ToInt16(bundles_reject.Rows[0][0]);
                            Rtot_bundAprov = Rtot_bundAprov + Convert.ToInt16(bundles_approv.Rows[0][0]);
                            Rtot_bunReject = Rtot_bunReject + Convert.ToInt16(bundles_reject.Rows[0][0]);
                            cell3.Height = "50px";
                            row.Cells.Add(cell3);
                        }
                        else
                        {
                            cell3 = new HtmlTableCell();
                            cell3.Style.Value = "border-right:1px solid #ccc;";
                            cell3.InnerHtml = "A=" + Rtot_bundAprov + "<br/> R=" + Rtot_bunReject;
                            cell3.Height = "50px";
                            row.Cells.Add(cell3);
                        }
                    }
                }
                if (i==2)
                {
                    if (j == 0)//  TOTAL DE BULTOS AUDITADOS / PIEZAS
                    {
                        cell1 = new HtmlTableCell();
                        cell1.Style.Value = "border-right:1px solid #ccc;";
                        cell1.InnerText = "Total de Bultos Auditados/Piezas";
                        cell1.Height = "50px";
                        row.Cells.Add(cell1);
                        cell2 = new HtmlTableCell();
                        cell2.Style.Value = "border-right:1px solid #ccc;";
                        DataTable bundles_approv = get_bundle_approv(Convert.ToString(bihorarios.Rows[j][0]), fecha);
                        TBA = TBA + Convert.ToInt16(bundles_approv.Rows[0][0]);//bultos aprobados
                        tot_piezasAprov = Convert.ToInt16(bundles_approv.Rows.Count) * Convert.ToInt16(bundles_approv.Rows[0][1]);//piezas aprobadas
                        DataTable bundles_reject = get_bundle_reject(Convert.ToString(bihorarios.Rows[j][0]), fecha);
                        TPA = TPA + Convert.ToInt16(bundles_reject.Rows[0][0]);//total bultos rechazados
                        tot_piezasReject = Convert.ToInt16(bundles_reject.Rows.Count) * Convert.ToInt16(bundles_reject.Rows[0][1]);//total piezas rechazadas
                        int totalpiezas1 = tot_piezasAprov + tot_piezasReject;
                        SumTotPiezas = SumTotPiezas + totalpiezas1;
                        cell2.InnerText = (Convert.ToInt16(bundles_approv.Rows[0][0]) + Convert.ToInt16(bundles_reject.Rows[0][0])) + "/" + totalpiezas1;
                        cell2.Height = "50px";
                        row.Cells.Add(cell2);
                    }
                    else
                    {
                        if (j < bihorarios.Rows.Count)
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            DataTable bundles_approv = get_bundle_approv(Convert.ToString(bihorarios.Rows[j][0]), fecha);
                            TBA = TBA + Convert.ToInt16(bundles_approv.Rows[0][0]);//bultos aprobados
                            tot_piezasAprov = Convert.ToInt16(bundles_approv.Rows.Count) * Convert.ToInt16(bundles_approv.Rows[0][1]);//piezas aprobadas

                            DataTable bundles_reject = get_bundle_reject(Convert.ToString(bihorarios.Rows[j][0]), fecha);
                            TPA = TPA + Convert.ToInt16(bundles_reject.Rows[0][0]);//total bultos rechazados                            
                            tot_piezasReject = Convert.ToInt16(bundles_reject.Rows.Count) * Convert.ToInt16(bundles_reject.Rows[0][1]);//total piezas rechazadas
                            int totalpiezas = tot_piezasAprov + tot_piezasReject;
                            SumTotPiezas = SumTotPiezas + totalpiezas;
                            cell2.InnerText = (Convert.ToInt16(bundles_approv.Rows[0][0]) + Convert.ToInt16(bundles_reject.Rows[0][0])) + "/" + totalpiezas;
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                        else//Columna de Totales
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            cell2.InnerText = (TBA + TPA) + "/" + SumTotPiezas;
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                    }
                }
                if (i == 3)
                {
                    if (j == 0)//PORCENTAJE DE CALIDAD
                    {
                        cell1 = new HtmlTableCell();
                        cell1.Style.Value = "border-right:1px solid #ccc;";
                        cell1.InnerText = "% de Calidad";
                        cell1.Height = "50px";
                        row.Cells.Add(cell1);
                        cell2 = new HtmlTableCell();
                        cell2.Style.Value = "border-right:1px solid #ccc;";
                        decimal calidad = 0;
                        DataTable bundles_approv = get_bundle_approv(Convert.ToString(bihorarios.Rows[0][0]), fecha);
                        DataTable bundles_reject = get_bundle_reject(Convert.ToString(bihorarios.Rows[0][0]), fecha);
                        decimal total = Convert.ToDecimal(bundles_approv.Rows[0][0]) + Convert.ToDecimal(bundles_reject.Rows[0][0]);
                        if (total > 0)
                        {
                            calidad = (Convert.ToDecimal(bundles_approv.Rows[0][0]) * 100) / total;
                            TotPorcentaje = TotPorcentaje + calidad;
                            contador_porcentaje = contador_porcentaje + 1;
                        }
                        calidad = (decimal)Math.Round(Convert.ToDecimal(calidad), 0);
                        cell2.InnerText = Convert.ToString(calidad) + "%";
                        cell2.Height = "50px";
                        row.Cells.Add(cell2);
                    }
                    else
                    {
                        if (j < bihorarios.Rows.Count)
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            decimal calidad = 0;
                            DataTable bundles_approv = get_bundle_approv(Convert.ToString(bihorarios.Rows[j][0]), fecha);
                            DataTable bundles_reject = get_bundle_reject(Convert.ToString(bihorarios.Rows[j][0]), fecha);
                            decimal total = Convert.ToDecimal(bundles_approv.Rows[0][0]) + Convert.ToDecimal(bundles_reject.Rows[0][0]);
                            if (total > 0)
                            {
                                calidad = (Convert.ToDecimal(bundles_approv.Rows[0][0]) * 100) / total;
                                TotPorcentaje = TotPorcentaje + calidad;
                                contador_porcentaje = contador_porcentaje + 1;
                            }
                            calidad = (decimal)Math.Round(Convert.ToDecimal(calidad), 0);
                            cell2.InnerText = calidad + "%";
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                        else
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            cell2.InnerText = Convert.ToString((decimal)Math.Round(Convert.ToDecimal(TotPorcentaje)/Convert.ToDecimal(contador_porcentaje), 0)) + "%";
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                    }
                }
                if (i == 4)
                {
                    if (j == 0)//PIEZAS AUDITADAS
                    {
                        // Create a cell and set its text.
                        cell1 = new HtmlTableCell();
                        cell1.Style.Value = "font-weight:bold;text-align:center;border-left:1px solid #ccc;border-right:1px solid #ccc;";
                        cell1.RowSpan = 4;
                        cell1.InnerText = "OQL";
                        // Add the cell to the current row.
                        row.Cells.Add(cell1);
                        cell2 = new HtmlTableCell();
                        cell2.Style.Value = "border-right:1px solid #ccc;";
                        cell2.InnerText = "Piezas Auditadas";
                        cell2.Height = "50px";
                        row.Cells.Add(cell2);
                        cell3 = new HtmlTableCell();
                        cell3.Style.Value = "border-right:1px solid #ccc;";
                        int oqlapr = 0;
                        string query = "select BA.Id_Bundle, M.sample_size from BundleApprovedBulto BA inner join Bundle B on BA.Id_Bundle=B.Id_Bundle inner join Muestreo M on BA.id_muestreo=M.id_muestreo where BA.id_bio=" + bihorarios.Rows[0][0] + " and BA.FechaAprobado='" + fecha + "'";
                        DataTable aql_aprov = DataAccess.Get_DataTable(query);
                        if (aql_aprov.Rows.Count > 0)
                        {
                            for (int z = 0; z <= aql_aprov.Rows.Count - 1; z++)
                            {
                                oqlapr = oqlapr + Convert.ToInt16(aql_aprov.Rows[z][1]);
                            }
                        }
                        string queryOQLRej = "select Id_Seccion,cantidad_defectos from BundleRejectedBulto where id_bio=" + bihorarios.Rows[0][0] + " and FechaRecibido='" + fecha + "'";
                        DataTable oqlreje = DataAccess.Get_DataTable(queryOQLRej);
                        decimal rejectoql = 0;

                        DataTable dt_linea = DataAccess.Get_DataTable("select id_linea from Linea where numero='" + Convert.ToString(DropDownList1.SelectedItem) + "'");
                        int id_Linea = Convert.ToInt16(dt_linea.Rows[0][0]);
                        DataTable secciones = DataAccess.Get_DataTable("select S.id_seccion, M.Modulo from Modulo M inner join Seccion S on M.id_seccion=S.id_seccion where S.id_linea=" + id_Linea);
                        if (oqlreje.Rows.Count > 0)
                        {
                            for (int z = 0; z <= oqlreje.Rows.Count - 1; z++)
                            {
                                rejectoql = rejectoql + Convert.ToInt16(oqlreje.Rows[z][1]);
                            }
                        }
                        TPOQL_A = TPOQL_A + oqlapr;
                        TPOQL_R = TPOQL_R + rejectoql;
                        cell3.InnerHtml = "A=" + Convert.ToString(oqlapr) + "<br/> R=" + Convert.ToString(rejectoql);
                        cell3.Height = "50px";
                        row.Cells.Add(cell3);
                    }
                    else
                    {
                        if (j < bihorarios.Rows.Count)
                        {
                            cell3 = new HtmlTableCell();
                            cell3.Style.Value = "border-right:1px solid #ccc;";
                            string query = "select BA.Id_Bundle, M.sample_size from BundleApprovedBulto BA inner join Bundle B on BA.Id_Bundle=B.Id_Bundle inner join Muestreo M on BA.id_muestreo=M.id_muestreo where BA.id_bio=" + bihorarios.Rows[j][0] + " and BA.FechaAprobado='" + fecha + "'";
                            DataTable aql_aprov = DataAccess.Get_DataTable(query);
                            int oqlapr = 0;
                            for (int z = 0; z <= aql_aprov.Rows.Count - 1; z++)
                            {
                                oqlapr = oqlapr + Convert.ToInt16(aql_aprov.Rows[z][1]);
                            }

                            string queryOQLRej = "select Id_Seccion,cantidad_defectos from BundleRejectedBulto where id_bio=" + bihorarios.Rows[j][0] + " and FechaRecibido='" + fecha + "'";
                            DataTable oqlreje = DataAccess.Get_DataTable(queryOQLRej);
                            decimal rejectoql = 0;
                            DataTable dt_linea = DataAccess.Get_DataTable("select id_linea from Linea where numero='" + Convert.ToString(DropDownList1.SelectedItem) + "'");
                            int id_Linea = Convert.ToInt16(dt_linea.Rows[0][0]);
                            DataTable secciones = DataAccess.Get_DataTable("select S.id_seccion, M.Modulo from Modulo M inner join Seccion S on M.id_seccion=S.id_seccion where S.id_linea=" + id_Linea);

                            for (int z = 0; z <= oqlreje.Rows.Count - 1; z++)
                            {
                                rejectoql = rejectoql + Convert.ToInt16(oqlreje.Rows[z][1]);
                            }
                            TPOQL_A = TPOQL_A + oqlapr;
                            TPOQL_R = TPOQL_R + rejectoql;

                            cell3.InnerHtml = "A=" + Convert.ToString(oqlapr) + "<br/> R=" + Convert.ToString(rejectoql);
                            cell3.Height = "50px";
                            row.Cells.Add(cell3);
                        }
                        else
                        {
                            cell3 = new HtmlTableCell();
                            cell3.Style.Value = "border-right:1px solid #ccc;";
                            cell3.InnerHtml = "A=" + Convert.ToString(TPOQL_A) + "<br/> R=" + Convert.ToString(TPOQL_R);
                            cell3.Height = "50px";
                            row.Cells.Add(cell3);
                        }
                    }
                }
                if (i == 5)
                {
                    if (j == 0)
                    {
                        cell1 = new HtmlTableCell();
                        cell1.Style.Value = "border-right:1px solid #ccc;";
                        cell1.InnerText = "Total de Piezas";
                        cell1.Height = "50px";
                        row.Cells.Add(cell1);
                        cell2 = new HtmlTableCell();
                        cell2.Style.Value = "border-right:1px solid #ccc;";
                        string query = "select BA.Id_Bundle, M.sample_size from BundleApprovedBulto BA inner join Bundle B on BA.Id_Bundle=B.Id_Bundle inner join Muestreo M on BA.id_muestreo=M.id_muestreo where BA.id_bio=" + bihorarios.Rows[j][0] + " and BA.FechaAprobado='" + fecha + "'";
                        DataTable aql_aprov = DataAccess.Get_DataTable(query);
                        int oqlapr = 0;
                        for (int z = 0; z <= aql_aprov.Rows.Count - 1; z++)
                        {
                            oqlapr = oqlapr + Convert.ToInt16(aql_aprov.Rows[z][1]);
                        }

                        string queryOQLRej = "select Id_Seccion,cantidad_defectos from BundleRejectedBulto where id_bio=" + bihorarios.Rows[j][0] + " and FechaRecibido='" + fecha + "'";
                        DataTable oqlreje = DataAccess.Get_DataTable(queryOQLRej);
                        decimal rejectoql = 0;
                        DataTable dt_linea = DataAccess.Get_DataTable("select id_linea from Linea where numero='" + Convert.ToString(DropDownList1.SelectedItem) + "'");
                        int id_Linea = Convert.ToInt16(dt_linea.Rows[0][0]);
                        DataTable secciones = DataAccess.Get_DataTable("select S.id_seccion, M.Modulo from Modulo M inner join Seccion S on M.id_seccion=S.id_seccion where S.id_linea=" + id_Linea);

                        for (int z = 0; z <= oqlreje.Rows.Count - 1; z++)
                        {
                            rejectoql = rejectoql + Convert.ToInt16(oqlreje.Rows[z][1]);
                        }
                        TPOQL = TPOQL + oqlapr + rejectoql;
                        cell2.InnerText = Convert.ToString(oqlapr + rejectoql);
                        cell2.Height = "50px";
                        row.Cells.Add(cell2);
                    }
                    else
                    {
                        if (j < bihorarios.Rows.Count)
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            string query = "select BA.Id_Bundle, M.sample_size from BundleApprovedBulto BA inner join Bundle B on BA.Id_Bundle=B.Id_Bundle inner join Muestreo M on BA.id_muestreo=M.id_muestreo where BA.id_bio=" + bihorarios.Rows[j][0] + " and BA.FechaAprobado='" + fecha + "'";
                            DataTable aql_aprov = DataAccess.Get_DataTable(query);
                            int oqlapr = 0;
                            for (int z = 0; z <= aql_aprov.Rows.Count - 1; z++)
                            {
                                oqlapr = oqlapr + Convert.ToInt16(aql_aprov.Rows[z][1]);
                            }

                            string queryOQLRej = "select Id_Seccion,cantidad_defectos from BundleRejectedBulto where id_bio=" + bihorarios.Rows[j][0] + " and FechaRecibido='" + fecha + "'";
                            DataTable oqlreje = DataAccess.Get_DataTable(queryOQLRej);
                            decimal rejectoql = 0;
                            DataTable dt_linea = DataAccess.Get_DataTable("select id_linea from Linea where numero='" + Convert.ToString(DropDownList1.SelectedItem) + "'");
                            int id_Linea = Convert.ToInt16(dt_linea.Rows[0][0]);
                            DataTable secciones = DataAccess.Get_DataTable("select S.id_seccion, M.Modulo from Modulo M inner join Seccion S on M.id_seccion=S.id_seccion where S.id_linea=" + id_Linea);

                            for (int z = 0; z <= oqlreje.Rows.Count - 1; z++)
                            {
                                rejectoql = rejectoql + Convert.ToInt16(oqlreje.Rows[z][1]);
                            }
                            TPOQL = TPOQL + oqlapr + rejectoql;
                            cell2.InnerText = Convert.ToString(oqlapr + rejectoql);
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                        else
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            cell2.InnerText = Convert.ToString(TPOQL);
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                    }
                }
                if (i == 6)
                {
                    if (j == 0)
                    {
                        cell1 = new HtmlTableCell();
                        cell1.Style.Value = "border-right:1px solid #ccc;";
                        cell1.InnerText = "% de Rechazo";
                        cell1.Height = "50px";
                        row.Cells.Add(cell1);
                        cell2 = new HtmlTableCell();
                        cell2.Style.Value = "border-right:1px solid #ccc;";
                        cell2.InnerText = "9%";
                        cell2.Height = "50px";
                        row.Cells.Add(cell2);
                    }
                    else
                    {
                        if (j < bihorarios.Rows.Count)
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            cell2.InnerText = "7%";
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                        else
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            cell2.InnerText = "6%";
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                    }
                }
                if (i == 7)
                {
                    if (j == 0)
                    {
                        cell1 = new HtmlTableCell();
                        cell1.InnerText = "% de Aceptacion";
                        cell1.Height = "50px";
                        cell1.Style.Value = "border-right:1px solid #ccc;";
                        row.Cells.Add(cell1);
                        cell2 = new HtmlTableCell();
                        cell2.Style.Value = "border-right:1px solid #ccc;";
                        cell2.InnerText = "91%";
                        cell2.Height = "50px";
                        row.Cells.Add(cell2);
                    }
                    else
                    {
                        if (j < bihorarios.Rows.Count)
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            cell2.InnerText = "95%";
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                        else
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            cell2.InnerText = "92%";
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                    }
                }
                if (i == 8)
                {
                    if (j == 0)
                    {
                        // Create a cell and set its text.
                        cell1 = new HtmlTableCell();
                        cell1.Style.Value = "border-bottom:1px solid #ccc;font-weight:bold;border-left:1px solid #ccc;border-right:1px solid #ccc;";
                        cell1.RowSpan = 4;
                        cell1.InnerText = "% DE SECCION";
                        // Add the cell to the current row.
                        row.Cells.Add(cell1);
                        cell2 = new HtmlTableCell();
                        cell2.Style.Value = "border-right:1px solid #ccc;";
                        cell2.InnerText = "Trasero %";
                        cell2.Height = "50px";
                        row.Cells.Add(cell2);
                        cell3 = new HtmlTableCell();
                        cell3.Style.Value = "border-right:1px solid #ccc;";
                        cell3.InnerText = "94%";
                        cell3.Height = "50px";
                        row.Cells.Add(cell3);
                    }
                    else
                    {
                        if (j < bihorarios.Rows.Count)
                        {
                            cell3 = new HtmlTableCell();
                            cell3.Style.Value = "border-right:1px solid #ccc;";
                            cell3.InnerText = "90%";
                            cell3.Height = "50px";
                            row.Cells.Add(cell3);
                        }
                        else
                        {
                            cell3 = new HtmlTableCell();
                            cell3.Style.Value = "border-right:1px solid #ccc;";
                            cell3.InnerText = "92%";
                            cell3.Height = "50px";
                            row.Cells.Add(cell3);
                        }
                    }
                }
                if (i == 9)
                {
                    if (j == 0)
                    {
                        cell1 = new HtmlTableCell();
                        cell1.InnerText = "Delantero %";
                        cell1.Style.Value = "border-right:1px solid #ccc;";
                        cell1.Height = "50px";
                        row.Cells.Add(cell1);
                        cell2 = new HtmlTableCell();
                        cell2.Style.Value = "border-right:1px solid #ccc;";
                        cell2.InnerText = "100%";
                        cell2.Height = "50px";
                        row.Cells.Add(cell2);
                    }
                    else
                    {
                        if (j < bihorarios.Rows.Count)
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            cell2.InnerText = "90%";
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                        else
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            cell2.InnerText = "95%";
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                    }
                }
                if (i == 10)
                {
                    if (j == 0)
                    {
                        cell1 = new HtmlTableCell();
                        cell1.Style.Value = "border-right:1px solid #ccc;";
                        cell1.InnerText = "Ensamble I";
                        cell1.Height = "50px";
                        row.Cells.Add(cell1);
                        cell2 = new HtmlTableCell();
                        cell2.Style.Value = "border-right:1px solid #ccc;";
                        cell2.InnerText = "75%";
                        cell2.Height = "50px";
                        row.Cells.Add(cell2);
                    }
                    else
                    {
                        if (j < bihorarios.Rows.Count)
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            cell2.InnerText = "69%";
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                        else
                        {
                            cell2 = new HtmlTableCell();
                            cell2.Style.Value = "border-right:1px solid #ccc;";
                            cell2.InnerText = "70%";
                            cell2.Height = "50px";
                            row.Cells.Add(cell2);
                        }
                    }
                }
                if (i == 11)
                {
                    if (j == 0)
                    {
                        cell1 = new HtmlTableCell();
                        cell1.InnerText = "Ensamble II";
                        cell1.Height = "50px";
                        cell1.Style.Value = "border-bottom:1px solid #ccc;border-right:1px solid #ccc;";
                        row.Cells.Add(cell1);                        
                        cell2 = new HtmlTableCell();
                        cell2.InnerText = "53%";
                        cell2.Style.Value = "border-bottom:1px solid #ccc;border-right:1px solid #ccc;";
                        cell2.Height = "50px";
                        row.Cells.Add(cell2);
                    }
                    else
                    {
                        cell2 = new HtmlTableCell();
                        cell2.InnerText = "67%";
                        cell2.Height = "50px";
                        cell2.Style.Value = "border-bottom:1px solid #ccc;border-right:1px solid #ccc;";
                        row.Cells.Add(cell2);
                    }
                }
            }

            // Add the row to the table.
            table1.Rows.Add(row);
        }

        // Add the table to the page.
        this.Controls.Add(table1);
        contenedor.Controls.Add(table1);    
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        crear_reporte();
      
        /*DataTable aql_aprov = new DataTable();
        DataTable aql_reject = new DataTable();
        int Id_Biohorario = 0;
        string hora_actual = DateTime.Now.Hour.ToString();
        string minutos_actual = DateTime.Now.Minute.ToString();
        string date_now = hora_actual + '.' + minutos_actual;
        DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio, bihorario FROM Biohorario WHERE @input between rango1 and rango2");
        if (Bihorarios.Rows.Count > 0)
        {
            Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);
        }

        DataTable Bio_auditados = DataAccess.Get_DataTable("SELECT id_bio, bihorario FROM Biohorario WHERE id_bio<=" + Id_Biohorario);
        cbxbihorario.DataSource = Bio_auditados;
        cbxbihorario.DataBind();
        cbxbihorario.TextField = "bihorario";
        cbxbihorario.ValueField = "id_bio";

        DateTime fecha_actual = DateTime.Now;
        string sqlfecha_actual = fecha_actual.ToString("yyyy-MM-dd");
        cbxbihorario.Text = Convert.ToString(Bihorarios.Rows[0][1]);
        int bio_seleccionado = Convert.ToInt16(cbxbihorario.Value);

        string query = "select BA.Id_Bundle, BA.Id_Order, B.Quantity, M.sample_size from BundleApprovedBulto BA inner join Bundle B on BA.Id_Bundle=B.Id_Bundle inner join Muestreo M on BA.id_muestreo=M.id_muestreo where BA.id_bio=" + bio_seleccionado + " and BA.FechaAprobado='" + sqlfecha_actual + "'";
        string query2 = "select BR.Id_Bundle, BR.Id_Order, B.Quantity, M.sample_size from BundleRejectedBulto BR inner join Bundle B on BR.Id_Bundle=B.Id_Bundle inner join Muestreo M on BR.id_muestreo=M.id_muestreo where BR.id_bio=" + bio_seleccionado + " and BR.FechaRecibido='" + sqlfecha_actual + "'";
        //Reporte de Auditoria por Bulto
        aql_aprov = DataAccess.Get_DataTable(query);
        int total_aprov = 0, total_rejec = 0;
        for (int i = 0; i <= aql_aprov.Rows.Count - 1; i++)
        {
            total_aprov = total_aprov + Convert.ToInt16(aql_aprov.Rows[i][2]);
        }
        aql_reject = DataAccess.Get_DataTable(query2);
        for (int j = 0; j <= aql_reject.Rows.Count - 1; j++)
        {
            total_rejec = total_rejec + Convert.ToInt16(aql_reject.Rows[j][2]);
        }

        lblAcep.Text = Convert.ToString(aql_aprov.Rows.Count);
        lblReje.Text = Convert.ToString(aql_reject.Rows.Count);
        //Total de Bultos Auditados
        decimal total = Convert.ToDecimal(aql_aprov.Rows.Count) + Convert.ToDecimal(aql_reject.Rows.Count);
        string piezas = Convert.ToString(total_aprov + total_rejec);
        lbltotbultau.Text = total + "/" + piezas;
        //%Calidad
        decimal calidad = 0;
        if (total > 0)
        {
            calidad = (Convert.ToDecimal(aql_aprov.Rows.Count) * 100) / total;
        }
        calidad = (decimal)Math.Round(Convert.ToDecimal(calidad), 0);
        lblcalidad.Text = Convert.ToString(calidad) + "%";

        //OQL Piezas Auditadas
        string queryOQL = "select isnull(max(m.sample_size),0) as Muestreo from BundleApprovedBulto bd inner join Muestreo m on bd.id_muestreo=m.id_muestreo where bd.id_bio=" + bio_seleccionado + " and FechaAprobado='" + sqlfecha_actual + "'";
        DataTable oql = DataAccess.Get_DataTable(queryOQL);
        int oqlapr = Convert.ToInt16(aql_aprov.Rows.Count) * Convert.ToInt16(oql.Rows[0][0]);
        lblOQLacep.Text = Convert.ToString(oqlapr);
        string queryOQLRej = "select Id_Seccion,cantidad_defectos from BundleRejectedBulto where id_bio=" + bio_seleccionado + " and FechaRecibido='" + sqlfecha_actual + "'";

        DataTable oqlreje = DataAccess.Get_DataTable(queryOQLRej);
        decimal rejectoql = 0;
        int oqltra = 0, oqldel = 0, oqlen = 0, oqlen1 = 0;
        DataTable dt_linea = DataAccess.Get_DataTable("select id_linea from Linea where numero='" + Convert.ToInt16(DropDownList1.SelectedValue) + "'");
        int id_linea = Convert.ToInt16(dt_linea.Rows[0][0]);

        DataTable secciones = DataAccess.Get_DataTable("select S.id_seccion, M.Modulo from Modulo M inner join Seccion S on M.id_seccion=S.id_seccion where S.id_linea=" + id_linea);
        for (int i = 0; i <= oqlreje.Rows.Count - 1; i++)
        {
            rejectoql = rejectoql + Convert.ToInt16(oqlreje.Rows[i][1]);
            if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[0][0]))//Trasero
            {
                oqltra = oqltra + Convert.ToInt16(oqlreje.Rows[i][1]);
            }
            if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[1][0]))//Delantero
            {
                oqldel = oqldel + Convert.ToInt16(oqlreje.Rows[i][1]);
            }
            if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[2][0]))//E1
            {
                oqlen = oqlen + Convert.ToInt16(oqlreje.Rows[i][1]);
            }
            if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[3][0]))//E2
            {
                oqlen1 = oqlen1 + Convert.ToInt16(oqlreje.Rows[i][1]);
            }
        }
        decimal TotalOql = Convert.ToDecimal(oqlapr) + rejectoql;
        if (TotalOql > 0)
        {
            decimal porReject = (rejectoql * 100) / TotalOql;
            decimal porAcep = (Convert.ToDecimal(oqlapr) * 100) / TotalOql;
            lblOQLporRech.Text = Math.Round(porReject, 0) + "%";
            lblOQLporAcep.Text = Math.Round(porAcep, 0) + "%";
            lblOQLrec.Text = Convert.ToString(rejectoql);
        }

        if (rejectoql > 0)
        {
            if (oqltra > 0)
            {
                lblSecTra.Text = Convert.ToString(((oqltra * 100) / rejectoql)) + "%";
            }
            else
            {
                lblSecTra.Text = "100%";
            }
            if (oqldel > 0)
            {
                lblSecDel.Text = Convert.ToString(((oqldel * 100) / rejectoql)) + "%";
            }
            else
            {
                lblSecDel.Text = "100%";
            }
            if (oqlen > 0)
            {
                lblSecEn1.Text = Convert.ToString(Math.Round(((oqlen * 100) / rejectoql), 0)) + "%";
            }
            else
            {
                lblSecEn1.Text = "100%";
            }
            if (oqlen1 > 0)
            {
                lblSecEn2.Text = Convert.ToString(Math.Round(((oqlen1 * 100) / rejectoql), 0)) + "%";
            }
            else
            {
                lblSecEn2.Text = "100%";
            }
        }*/
    }

    protected void cbxbihorario_SelectedIndexChanged(object sender, EventArgs e)
    {
        Reporte_Bihorario();
    }

}
