﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RepCutXDate.aspx.cs" Inherits="admin_RepCutXDate" %>

<%@ Register Assembly="DevExpress.XtraReports.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/bootstrap.min.css" rel="stylesheet" />

    <link href="../StyleSheetAuto.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Measurement Report </strong></h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                <asp:UpdatePanel ID="update" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="DropDownListProcess" OnSelectedIndexChanged="DropDownListProcess_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="0"> Select...</asp:ListItem>
                                            <asp:ListItem Value="1"> Production</asp:ListItem>
                                            <asp:ListItem Value="2"> After Wash</asp:ListItem>
                                            <asp:ListItem Value="3"> Audit Final</asp:ListItem>
                                        </asp:DropDownList>

                                        <br />
                                        <asp:Panel ID="Panel1" Visible="false" runat="server">
                                                Seccion 
                                            <asp:RadioButtonList ID="RadioButtonList2" runat="server">
                                                <asp:ListItem Value="bultoabulto">Before Wash Final Audit</asp:ListItem>
                                                <asp:ListItem Value="inspInter">Before Wash In Line</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                Start Date
                            <div class="input-group">

                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>

                                <dx:ASPxDateEdit ID="txtfecha1" Theme="Metropolis" Height="35px" runat="server" EditFormat="Custom" Width="100%">
                                </dx:ASPxDateEdit>

                            </div>
                                End Date
                            <div class="input-group">

                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                <dx:ASPxDateEdit ID="txtfecha2" Theme="Metropolis" Height="35px" runat="server" EditFormat="Custom" Width="100%">
                                </dx:ASPxDateEdit>

                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-lg-4" style="padding-top: 10px; padding-bottom: 10px">
                            <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" OnClick="btngenerar_Click" runat="server" />
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <dx:ASPxWebDocumentViewer ID="ASPxWebDocumentViewer1" runat="server"></dx:ASPxWebDocumentViewer>
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

