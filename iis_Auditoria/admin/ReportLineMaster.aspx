﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportLineMaster.aspx.cs" Inherits="admin_ReportLineMaster" %>


<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Measurement Report </strong></h3>
            </div>
            <div class="panel-body">

                <div class="col-lg-4">
                    Plant:
                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server">
                         <asp:ListItem Value="0" Text="Select..."></asp:ListItem>
                         <asp:ListItem Value="1" Text="Roc 1"></asp:ListItem>
                         <asp:ListItem Value="2" Text="Roc 3"></asp:ListItem>
                         <asp:ListItem Value="3" Text="Roc 6"></asp:ListItem>
                     </asp:DropDownList>
                    Section 
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                        <asp:ListItem Value="bultoabulto">Bundle By Bundle</asp:ListItem>
                        <asp:ListItem Value="inspInter">Final Inspection</asp:ListItem>
                    </asp:RadioButtonList>

                  
                   
                </div>
                <div class="col-lg-4">
                    Start Date
                      <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>
                    End Date
                      <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>
                    
                </div>
               
                <div class="col-lg-12">
                    
                     <asp:Button ID="btnGenerar" OnClick="btnGenerar_Click" CssClass="btn btn-primary " Width="300px" runat="server" Text="Generate Report" />
                    <br />
                    <asp:Button ID="btnExport" OnClick="btnExport_Click" CssClass="btn btn-success " Width="300px" runat="server" Text="Export Excel" />
                      <hr />
                </div>
              
                <div class="col-lg-12">
                    <dxchartsui:WebChartControl ID="packet_chart" Width="1000px" Height="394px"
                        runat="server" AppearanceNameSerializable="Pastel Kit" CrosshairEnabled="True"
                        SeriesDataMember="NombreMedida">
                        <BorderOptions Visibility="False" />
                        <BorderOptions Visibility="False"></BorderOptions>
                        <DiagramSerializable>
                            <cc1:XYDiagram
                                RangeControlDateTimeGridOptions-GridMode="ChartGrid">
                                <AxisX VisibleInPanesSerializable="-1">
                                    <GridLines Visible="True">
                                    </GridLines>
                                </AxisX>
                                <AxisY VisibleInPanesSerializable="-1">
                                </AxisY>
                            </cc1:XYDiagram>
                        </DiagramSerializable>
                        <SeriesTemplate ArgumentDataMember="line" LabelsVisibility="True" CrosshairLabelPattern="{S}:{V}"
                            ValueDataMembersSerializable="total">
                            <LabelSerializable>
                                <cc1:SideBySideBarSeriesLabel Position="Top" TextPattern="Qty:{V}"
                                    Indent="5" LineLength="10" LineVisibility="True"
                                    ResolveOverlappingMode="Default">
                                </cc1:SideBySideBarSeriesLabel>
                            </LabelSerializable>
                        </SeriesTemplate>
                        <Titles>
                            <cc1:ChartTitle Text="Report Measurement Of Line "></cc1:ChartTitle>
                        </Titles>
                    </dxchartsui:WebChartControl>
                </div>
                <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

