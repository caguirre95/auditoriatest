﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReporteInteXUser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Date = DateTime.Now;
            txtfecha2.Date = DateTime.Now;
            cargaUser();
        }
    }


    void cargaUser()
    {
        string sqline = "select userms,userName from tblUserMedida where rol='intex'";

        DataTable dt_user = DataAccess.Get_DataTable(sqline);

        DropDownListUser.DataSource = dt_user;
        DropDownListUser.DataTextField = "userName";
        DropDownListUser.DataValueField = "userms";
        DropDownListUser.DataBind();
        DropDownListUser.Items.Insert(0, "Select...");
    }

    protected void btnGenerar_Click(object sender, EventArgs e)
    {
        if (DropDownListUser.SelectedItem.Text != "Select...")
        {
            XtraReportCorteUser rep = new XtraReportCorteUser();

            rep.Parameters[0].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
            rep.Parameters[1].Value = txtfecha2.Date.ToString("yyyy-MM-dd");
            rep.Parameters[2].Value = DropDownListUser.SelectedItem.Value;

            ASPxWebDocumentViewer1.OpenReport(rep);
        }
    }
}