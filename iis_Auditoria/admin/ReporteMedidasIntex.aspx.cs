﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReporteMedidasIntex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                txtfecha1.Text = DateTime.Now.ToShortDateString();
                txtfecha2.Text = DateTime.Now.ToShortDateString();

                cargacombo();
                cargagridestilos();
            }

        }
        catch (Exception)
        {

            throw;
        }
    }

    void cargacombo()
    {
        string sqline = "select id_linea,numero from Linea l where l.numero not like '%-%' and l.numero not like '%A%' order by CONVERT(float,numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT [id_planta],[descripcion] FROM[Auditoria].[dbo].[Planta] order by descripcion";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
        DropDownListPlant.Items.Insert(6, "All Plant");

    }

    DataTable funcionCarga2()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);

        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string resp = RadioButtonList1.SelectedValue;

        string query = "";

        switch (resp)
        {
            case "1"://cut
                if (txtPorder.Text != string.Empty)
                {
                    query = " select vw.Estilo,vw.color,vw.POrder,vw.UnidadesDelCorte as Cantidad, sum(case estado when 0 then 1 else 0 end) as Rechazado,sum(case estado when 1 then 1 else 0 end)as Aceptado,count(vw.cantidad) as total"
                          + " from vwMedidasNewIntex vw"
                          + " where vw.porder='" + txtPorder.Text.Trim() + "'"
                          + " group by vw.Estilo,vw.porder,vw.UnidadesDelCorte,vw.UnidadesDelCorte, vw.Color";
                }
                else
                {
                    resp = "fin";
                }
                break;
            case "2"://style
                if (txtstyle.Text != string.Empty)
                {
                    query = " select vw.Estilo,vw.color,vw.POrder,vw.UnidadesDelCorte as Cantidad, sum(case estado when 0 then 1 else 0 end) as Rechazado,sum(case estado when 1 then 1 else 0 end)as Aceptado,count(vw.cantidad) as total"
                           + " from vwMedidasNewIntex vw"
                           + " where vw.estilo = '" + txtstyle.Text.Trim() + "' and fechaRegistro between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                           + " group by vw.Estilo,vw.porder,vw.UnidadesDelCorte,vw.UnidadesDelCorte,vw.color";
                }
                else
                {
                    resp = "fin";
                }
                break;
            case "3"://line
                if (DropDownListLine.SelectedItem.Text != "Select...")
                {
                    query = " select vw.Estilo,vw.color,vw.POrder,vw.UnidadesDelCorte as Cantidad, sum(case estado when 0 then 1 else 0 end) as Rechazado,sum(case estado when 1 then 1 else 0 end)as Aceptado,count(vw.cantidad) as total"
                           + " from vwMedidasNewIntex vw"
                           + " where vw.Id_Linea2 = " + DropDownListLine.SelectedValue + " and fechaRegistro between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                           + " group by vw.Estilo,vw.porder,vw.UnidadesDelCorte,vw.UnidadesDelCorte,vw.color";
                }
                else
                {
                    resp = "fin";
                }
                break;
            case "4"://plant
                if (DropDownListPlant.SelectedItem.Text == "Select...")
                {
                    resp = "fin";
                }
                else if (DropDownListPlant.SelectedItem.Text == "All Plant")
                {
                    query = " select vw.Estilo,vw.color,vw.POrder,vw.UnidadesDelCorte as Cantidad, sum(case estado when 0 then 1 else 0 end) as Rechazado,sum(case estado when 1 then 1 else 0 end)as Aceptado,count(vw.cantidad) as total"
                         + " from vwMedidasNewIntex vw"
                         + " where fechaRegistro between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                         + " group by vw.Estilo,vw.porder,vw.UnidadesDelCorte,vw.UnidadesDelCorte,vw.color";

                }
                else
                {

                    query = " select vw.Estilo,vw.color,vw.POrder,vw.UnidadesDelCorte as Cantidad, sum(case estado when 0 then 1 else 0 end) as Rechazado,sum(case estado when 1 then 1 else 0 end)as Aceptado,count(vw.cantidad) as total"
                         + " from vwMedidasNewIntex vw"
                         + " where vw.Id_Planta = " + DropDownListPlant.SelectedValue + " and fechaRegistro between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                         + " group by vw.Estilo,vw.porder,vw.UnidadesDelCorte,vw.UnidadesDelCorte,vw.color";


                }
                break;
            case "":
                resp = "fin";
                break;
            default:
                resp = "fin";
                break;
        }

        DataTable dt = new DataTable();
        if (resp != "fin")
        {

            dt = new DataTable();
            dt = DataAccess.Get_DataTable(query);

            return dt;
        }
        else
        {
            //debe seleccionar una opcion
            dt = null;
            return dt;
        }

    }

    void cargagridestilos()
    {
        //string sqline = " select vw.Estilo as Style from vwMedidasNewIntex vw"
        //              + " where datepart(wk, fechaRegistro) = DATEPART(WK, GETDATE()) and DATEPART(YY, fechaRegistro)= DATEPART(YY, GETDATE())"
        //              + " group by vw.Estilo";

        string sqline = " select s.Style from tbMedidasIntexDL i join Style s on s.Id_Style = i.idEstilo"
                      + " where datepart(wk, fechaRegistro) = DATEPART(WK, GETDATE()) and DATEPART(YY, fechaRegistro)= DATEPART(YY, GETDATE())"
                      + " group by idEstilo, s.Style";

        DataTable dtEstilos = DataAccess.Get_DataTable(sqline);

        grvestilos.DataSource = dtEstilos;
        grvestilos.DataBind();
    }

    protected void gridpack_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridpack.DataBind();
    }

    protected void gridpack_DataBinding(object sender, EventArgs e)
    {
        var dt = funcionCarga2();

        gridpack.DataSource = dt;
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            gridpack.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridViewExporter1.GridViewID = "gridpack";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;


            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {

                XlsxExportOptions options = new XlsxExportOptions();

                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Medida despues de Lavado";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Medida-Despues-Lavado.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }

            ps.Dispose();

        }
        catch (Exception)
        {

            throw;
        }

    }
}