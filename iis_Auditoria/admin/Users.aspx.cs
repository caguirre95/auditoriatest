﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using SistemaAuditores.DataAccess;
using System.Web.Security;
using System.Web.UI.HtmlControls;


public partial class admin_Users : System.Web.UI.Page
{
    GridViewRow row;
    bool flag = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Load_Data();
            Roldrop.SelectedValue = null;
        }

    }

    public void Load_Data()
    {
        DataTable UserInfo = new DataTable();
        DataTable rol = new DataTable();

        UserInfo = DataAccess.Get_DataTable("SELECT [Nombre], [Username], [Rol] FROM [tblusuario] where Rol='Auditor' or Rol='Administrator'");
        UserGrid.DataSource = UserInfo;
        UserGrid.DataBind();

        rol = DataAccess.Get_DataTable("Select [RoleName] from aspnet_Roles where RoleName='Administrator' or RoleName='Auditor'");
        Roldrop.DataSource = rol;
        Roldrop.DataValueField = "RoleName";
        Roldrop.DataBind();

    }

    protected void UserGrid_RowCommand1(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Editbtn")
        {
            try
            {
                int index = Convert.ToInt32(e.CommandArgument);
                row = UserGrid.Rows[index];

                TxtNombre.Text = row.Cells[0].Text;
                TxtUsername.Text = row.Cells[1].Text;
                Roldrop.SelectedValue = row.Cells[2].Text;
                flag = true;


            }
            catch (Exception ex)
            {
                string error = "alert('Error 400 * " + ex.Message + " *');";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
            }

        }

        if (e.CommandName == "Deletebtn")
        {
            try
            {
                int index = Convert.ToInt32(e.CommandArgument);
                row = UserGrid.Rows[index];
                DataTable UserInfo = new DataTable();
                string query = "SELECT [IdUsuario] FROM [tblusuario] where Username='" + row.Cells[1].Text + "'";
                UserInfo = DataAccess.Get_DataTable(query);
                UsersAcess.DeleteUser(Convert.ToInt16(UserInfo.Rows[0][0]));
                UserGrid.DataBind();
            }
            catch (Exception ex)
            {
                string error = "alert('Error 400 * " + ex.Message + " *');";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
            }
        }
    }
    protected void Savebtn_Click(object sender, ImageClickEventArgs e)
    {
        if (flag == true)
        {
            try
            {
                DataTable UserInfo = new DataTable();
                string query = "SELECT [IdUsuario], [Username] FROM [tblusuario] where Username='" + TxtUsername.Text + "'";
                UserInfo = DataAccess.Get_DataTable(query);
                UsersAcess.UpdateUser(Convert.ToInt16(UserInfo.Rows[0][0]), TxtNombre.Text, TxtUsername.Text, Password1.Value, Roldrop.SelectedValue.ToString());
            }
            catch (Exception ex)
            {
                string error = "alert('Error 401 * " + ex.Message + " *');";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
            }
            flag = false;
        }
        else
        {
            if ((TxtUsername.Text == null) || (Password1.Value == null) || (TxtNombre.Text == null))
            {
                string error = "alert('Todos los campos deben estar llenos');";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
            }
            else
            {
                try
                {
                    MembershipCreateStatus status;
                    string email = TxtUsername.Text + "@rocedes.com.ni";
                    MembershipUser user = Membership.CreateUser(TxtUsername.Text, Password1.Value, email, "none", "none", true, out status);
                    DataTable UserInfo = new DataTable();
                    int permiso;
                    if (Roldrop.SelectedValue == "Administrator")
                    {
                        permiso = 1;
                    }
                    else
                    {
                        permiso = 2;
                    }
                    UsersAcess.CreateAdminUser(TxtNombre.Text, email, TxtUsername.Text, Password1.Value, permiso, Roldrop.SelectedValue, "SAuditor");
                    UserGrid.DataBind();
                }
                catch (Exception ex)
                {
                    string error = "alert('Error 400 * " + ex.Message + " *');";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
                }
            }
        }
    }
}