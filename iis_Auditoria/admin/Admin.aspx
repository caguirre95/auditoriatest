﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Admin.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
.box_search
{
background-color:#fff !important;   
}
#search_panel
{
margin-top:50px !important;
}
#ContentPlaceHolder2_GridView1
{
margin-top:50px;    
}
.PagerCS td table tbody tr td span
{
padding:5px;
font-size:18px;    
color:#e2b607 !important;
font-weight:bold;
}
.PagerCS td table tbody tr td a
{
padding:5px;
font-size:18px;    
color:#4d7496 !important;
font-weight:lighter;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
    <h2><asp:Label ID="Label1" runat="server" Text="Admin Panel"></asp:Label></h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<div id="search_panel" class="box_search">
     <div class="row">
          <div class="izq">
    <div style="width:100%;text-align: center; padding:10px; display: table;">
        <div style="display: table-cell;padding:10px;">
         <asp:ImageButton ID="imgNewPO" runat="server" Width="96" ImageUrl="/img/newrecords.png"
         onclick="imgNewPO_Click" /><br />
        <asp:LinkButton ID="lnk_NewPOrder" runat="server" onclick="lnk_NewPOrder_Click">Production Orders</asp:LinkButton>
        </div>
         <div style="display: table-cell;padding:10px;">
         <asp:ImageButton ID="ImageButton5" runat="server" Width="96" ImageUrl="/img/bundles.png"
         onclick="ImageButton5_Click" /><br />
        <asp:LinkButton ID="lnk_ConvertBundles" runat="server" 
                 onclick="lnk_ConvertBundles_Click">Bundles</asp:LinkButton>
        </div>
    </div>
          </div>
     </div>
</div>

    <div style="width:100%;">
    <center>
    <asp:GridView ID="GridView1" runat="server" CellPadding="5" AllowPaging="True" 
            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" 
            ForeColor="Black" GridLines="Horizontal" PageSize="500" CellSpacing="5" 
            onpageindexchanging="GridView1_PageIndexChanging">
        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
        <PagerSettings PageButtonCount="100" Position="Top" />
        <PagerStyle CssClass="PagerCS" BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#242121" />
    </asp:GridView>
    </center>
    </div>
</asp:Content>

