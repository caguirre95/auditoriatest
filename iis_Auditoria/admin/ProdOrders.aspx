﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProdOrders.aspx.cs" Inherits="admin_ProdOrders" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style>
.box_search
{
    background-color:#fff !important;
}
#ContentPlaceHolder2_GridView1
{
width:100% !important;
}
.PagerCS td table tbody tr td span
{
padding:5px;
font-size:18px;    
color:#e2b607 !important;
font-weight:bold;
}

.PagerCS td table tbody tr td a
{
padding:5px;
font-size:18px;    
color:#4d7496 !important;
font-weight:lighter;
}

</style>
<script type="text/javascript" src="/scripts/jquery.blockUI.js"></script>   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <script type="text/javascript">
    function BlockUI(elementID) {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $("#" + elementID).block({ message: '<table><tr><td>' + '<img src="/img/please_wait.gif"/></td></tr></table>',
                css: {},
                overlayCSS: { backgroundColor: '#FFFFFF', opacity: 0.6, border: '1px solid #000000' }
            });
        });
        prm.add_endRequest(function () {
            $("#" + elementID).unblock();
        });
    }
    $(document).ready(function () {
        BlockUI("search_panel");
        $.blockUI.defaults.css = {};
    });
</script>
 <asp:UpdatePanel runat="server" id="Panel">
     <ContentTemplate>
     <div id="search_panel" class="box_search">
        <div class="row">
            <div class="izq">
                    <div style="width:100%;text-align: center; padding:10px; display: table;">
                        <div style="display: table-cell;padding:10px;">
                         <asp:ImageButton ID="img_importdata" runat="server" Width="96" ImageUrl="/img/updateallbdl.png"
                         onclick="img_importdata_Click" />
                            <ajaxToolkit:ConfirmButtonExtender ID="img_importdata_ConfirmButtonExtender" 
                                runat="server" ConfirmText="Esta Seguro que desea realizar esta Operación?" 
                                TargetControlID="img_importdata" />
                            <br />
                        <asp:LinkButton ID="lnk_importdata" runat="server" onclick="lnk_importdata_Click">Import Data from PERVASIVE</asp:LinkButton>
                            <ajaxToolkit:ConfirmButtonExtender ID="lnk_importdata_ConfirmButtonExtender" 
                                runat="server" ConfirmText="Esta Seguro que desea realizar esta Operación?" 
                                TargetControlID="lnk_importdata" />
                        </div>
                        <div style="display: none;padding:10px;">
                         <asp:ImageButton ID="imgupdIdCliente" runat="server" Width="96" ImageUrl="/img/newrecords.png"
                         onclick="imgupdIdCliente_Click" /><br />
                        <asp:LinkButton ID="lnk_IdCliente" runat="server" onclick="lnk_IdCliente_Click">Update 
                            Id Cliente PV</asp:LinkButton>
                        </div>
                        <div style="display: none;padding:10px;">
                         <asp:ImageButton ID="imgupdIdStyle" runat="server" Width="96" ImageUrl="/img/newrecords.png"
                         onclick="imgupdIdStyle_Click" /><br />
                        <asp:LinkButton ID="lnk_IdStyle" runat="server" onclick="lnk_IdStyle_Click">Update 
                            Id Style PV</asp:LinkButton>
                        </div>
                        <div style="display: table-cell;padding:10px;">
                        <asp:ImageButton ID="img_addnewpo" runat="server" Width="96" 
                                ImageUrl="/img/bundles.png" onclick="img_addnewpo_Click" /><br />
                        <asp:LinkButton ID="lnk_UpdateNewpo" runat="server" 
                                onclick="lnk_UpdateNewpo_Click">New POrders Local DB</asp:LinkButton>
                        </div>
                        <div style="display: table-cell;padding:10px;">
                        <asp:ImageButton ID="img_showallpo" runat="server" Width="96" 
                                ImageUrl="/img/show.png" onclick="img_showallpo_Click" /><br />
                        <asp:LinkButton ID="lnkshowpo" runat="server" 
                                onclick="lnkshowpo_Click">Show POrders Local DB</asp:LinkButton>
                        </div>
                        <div style="display: table-cell;padding:10px;">
                        <asp:ImageButton ID="img_showallPV" runat="server" Width="96" 
                                ImageUrl="/img/show.png" onclick="img_showallPV_Click" /><br />
                        <asp:LinkButton ID="lnkshowPV" runat="server" 
                                onclick="lnkshowPV_Click">Show POrders Pervasive DB</asp:LinkButton>
                        </div>
                   </div>
            </div>
        </div>
     </div><br />
     <div style="margin:0px auto;width:70%;padding:10px;">
        <asp:GridView ID="GridView1" runat="server" CellPadding="5" BackColor="White" 
                BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" ForeColor="Black" 
                GridLines="Horizontal" AllowPaging="True" CellSpacing="5" 
             PageSize="500">
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
            <PagerSettings PageButtonCount="100" Position="TopAndBottom" />
            <PagerStyle CssClass="PagerCS" BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#242121" />
        </asp:GridView>
        </div>
     </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

