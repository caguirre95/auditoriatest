﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReportLineMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtfecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }
    
    }

    protected void btnGenerar_Click(object sender, EventArgs e)
    {
        if (RadioButtonList1.SelectedValue == "" && DropDownListPlant.SelectedValue!="0")
        {
            return;
        }

        string query = " select ISNULL(c2.total,0)as total,c2.NombreMedida,'Line '+c1.numero as line from " 
                       +" (select l.numero, l.id_linea from Linea l where l.id_planta = "+ DropDownListPlant.SelectedItem.Value + "  and numero not like '%-%' and numero not like '%A%') c1"
                       + " left join"
                       +" (select  COUNT(m.Medida) as total, ptsm.NombreMedida, 'Linea ' + l.numero as linea, sec.sec, l.id_linea  from tblMedidaBultoAL mbal"   
                       //+" join tblUserMedida u on mbal.insp = u.userms"
                       +" join tblPuntoMasterAL pm on mbal.idMaster = pm.idPuntoMaster"
                       +" join tblPuntosMedida ptsm on pm.idpuntosM = ptsm.idPuntosM"
                       +" join tblMedidas m on mbal.idMedida = m.idMedida"
                       +" join tblSecuenciaPMedida sec on ptsm.idPuntosM = sec.idPuntoM"
                      
                       +" join Bundle b on pm.idbulto = b.Id_Bundle"
                       +" join POrder po on b.Id_Order = po.Id_Order"
                       +" join linea l on po.Id_Linea2 = l.id_linea"
                       + " where pm.rol = '" + RadioButtonList1.SelectedItem.Value + "'"
                       + " and sec.rol = '"+ RadioButtonList1.SelectedItem.Value + "'"
                      
                       +" and l.id_planta = "+ DropDownListPlant.SelectedItem.Value
                       + " and CONVERT(date, mbal.fecha)between '"+txtfecha1.Text+"' and '"+txtfecha2.Text+"'"
                       +" group by ptsm.NombreMedida, l.numero, sec.sec, l.id_linea"
                       +" ) c2"
                       +" on c1.id_linea = c2.id_linea"
                       +" order by c2.linea,c2.sec desc";


        //string query = " select COUNT(m.Medida)as total ,pmd.NombreMedida,'Linea '+l.numero as linea"
        //                   + " from Bundle b"
        //                   +" join POrder p on b.Id_Order = p.Id_Order"
        //                   +" join Linea l on p.Id_Linea = l.id_linea"
        //                   +" join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
        //                   +" join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
        //                   +" join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
        //                   +" join tblMedidas m on mb.idMedida = m.idMedida"
        //                   +" join tblSecuenciaPMedida sec on pmd.idPuntosM = sec.idPuntoM"
        //                   +" where pm.idbulto in(select distinct b.Id_Bundle from Bundle b"
        //                   +" join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
        //                   +" join POrder p on b.Id_Order = p.Id_Order"
        //                   +" join Linea l on l.id_linea = p.Id_Linea"
        //                  +" where l.id_planta ="" and p.Id_Cliente = 11 and pm.rol = '"+ RadioButtonList1.SelectedItem.Value +"') and CONVERT(date, mb.fecha) between '"+txtfecha1.Text+"' and '"+ txtfecha2.Text+"' and pm.rol = '" +  + "' and sec.rol = '" + RadioButtonList1.SelectedItem.Value + "'"  
        //                  +" group by pmd.NombreMedida, l.numero, sec.sec"
        //                  +"  order by l.numero, sec.sec asc";

        DataTable dt = new DataTable();
        dt = DataAccess.Get_DataTable(query);

        packet_chart.DataSource = dt;
        packet_chart.DataBind();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (RadioButtonList1.SelectedValue == "" && DropDownListPlant.SelectedValue != "0")
        {
            return;
        }

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

            string query = " select ISNULL(c2.total,0)as total,c2.NombreMedida,'Line '+c1.numero as line from "
                       + " (select l.numero, l.id_linea from Linea l where l.id_planta = " + DropDownListPlant.SelectedItem.Value + " and l.idcliente = 11 and numero not like '%-%' and numero not like '%A%') c1"
                       + " left join"
                       + " (select  COUNT(m.Medida) as total, ptsm.NombreMedida, 'Linea ' + l.numero as linea, sec.sec, l.id_linea  from tblMedidaBultoAL mbal"
                       //+" join tblUserMedida u on mbal.insp = u.userms"
                       + " join tblPuntoMasterAL pm on mbal.idMaster = pm.idPuntoMaster"
                       + " join tblPuntosMedida ptsm on pm.idpuntosM = ptsm.idPuntosM"
                       + " join tblMedidas m on mbal.idMedida = m.idMedida"
                       + " join tblSecuenciaPMedida sec on ptsm.idPuntosM = sec.idPuntoM"
                       + " join Bundle b on pm.idbulto = b.Id_Bundle"
                       + " join POrder po on b.Id_Order = po.Id_Order"
                       + " join linea l on po.Id_Linea2 = l.id_linea"
                       + " where pm.rol = '" + RadioButtonList1.SelectedItem.Value + "'"
                       + " and sec.rol = '" + RadioButtonList1.SelectedItem.Value + "'"
                       + " and l.idcliente = 11"
                       + " and l.id_planta = " + DropDownListPlant.SelectedItem.Value
                       + " and CONVERT(date, mbal.fecha)between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                       + " group by ptsm.NombreMedida, l.numero, sec.sec, l.id_linea"
                       + " ) c2"
                       + " on c1.id_linea = c2.id_linea"
                       + " order by c2.linea,c2.sec desc";

        //string query = " select ISNULL(c2.total,0)as total,ISNULL(c2.NombreMedida,'')as NombreMedida ,'Line '+c1.numero as line from "
        //              + " (select l.numero, l.id_linea from Linea l where l.id_planta = " + DropDownListPlant.SelectedItem.Value + " and l.idcliente = 11 and numero not like '%-%' and numero not like '%A%' and numero not like '%11%' and numero not like '%14%') c1"
        //              + " left join"
        //              + " (select  COUNT(m.Medida) as total, ptsm.NombreMedida, 'Linea ' + l.numero as linea, sec.sec, l.id_linea  from tblMedidaBultoAL mbal"
        //              + " join tblUserMedida u on mbal.insp = u.userms"
        //              + " join tblPuntoMasterAL pm on mbal.idMaster = pm.idPuntoMaster"
        //              + " join tblPuntosMedida ptsm on pm.idpuntosM = ptsm.idPuntosM"
        //              + " join tblMedidas m on mbal.idMedida = m.idMedida"
        //              + " join tblSecuenciaPMedida sec on ptsm.idPuntosM = sec.idPuntoM"
        //              + " join linea l on u.linea = l.id_linea"
        //              + " join Bundle b on pm.idbulto = b.Id_Bundle"
        //              + " join POrder po on b.Id_Order = po.Id_Order"
        //              + " where u.rol = '" + RadioButtonList1.SelectedItem.Value + "'"
        //              + " and sec.rol = '" + RadioButtonList1.SelectedItem.Value + "'"
        //              + " and l.idcliente = 11"
        //              + " and l.id_planta = " + DropDownListPlant.SelectedItem.Value
        //              + " and CONVERT(date, mbal.fecha)between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
        //              + " group by ptsm.NombreMedida, l.numero, sec.sec, l.id_linea"
        //              + " ) c2"
        //              + " on c1.id_linea = c2.id_linea"
        //              + " order by c2.linea,c2.sec desc";

        //string query = " select COUNT(m.Medida)as total ,pmd.NombreMedida,'Linea '+l.numero as linea"
        //                   + " from Bundle b"
        //                   + " join POrder p on b.Id_Order = p.Id_Order"
        //                   + " join Linea l on p.Id_Linea = l.id_linea"
        //                   + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
        //                   + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
        //                   + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
        //                   + " join tblMedidas m on mb.idMedida = m.idMedida"
        //                   + " join tblSecuenciaPMedida sec on pmd.idPuntosM = sec.idPuntoM"
        //                   + " where pm.idbulto in(select distinct b.Id_Bundle from Bundle b"
        //                   + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
        //                   + " join POrder p on b.Id_Order = p.Id_Order"
        //                   + " join Linea l on l.id_linea = p.Id_Linea"
        //                  + " where l.id_planta =" + DropDownListPlant.SelectedItem.Value + " and p.Id_Cliente = 11 and pm.rol = '" + RadioButtonList1.SelectedItem.Value + "') and CONVERT(date, mb.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and pm.rol = '" + RadioButtonList1.SelectedItem.Value + "' and sec.rol = '" + RadioButtonList1.SelectedItem.Value + "'"
        //                  + " group by pmd.NombreMedida, l.numero, sec.sec"
        //                  + "  order by l.numero, sec.sec asc";

        DataTable dt = new DataTable();
        dt = DataAccess.Get_DataTable(query);

        packet_chart.DataSource = dt;
        packet_chart.DataBind();

        link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)packet_chart).Chart;

        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link2 });

        compositeLink.CreatePageForEachLink();

        using (MemoryStream stream = new MemoryStream())
        {
            XlsxExportOptions options = new XlsxExportOptions();
            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
            options.SheetName = "Master_Report_Line";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=Top5Defects.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }
        ps.Dispose();

    }
}