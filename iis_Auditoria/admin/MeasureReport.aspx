﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MeasureReport.aspx.cs" Inherits="MeasureReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style>
  #ContentPlaceHolder2_GV_Defects
  {
  margin-top:-20px;
  }

  </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h3>
        <asp:Label ID="Label1" runat="server" Text="Top 5 Defects by Bundle"></asp:Label></h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
<br />
    <div class="busqueda" style="width:100%;">
    <center>
    <div style="margin-bottom:10px;">
    <div style="display:table-cell;padding:5px;border-left:1px solid #ccc;border-top:1px solid #ccc;border-bottom:1px solid #ccc;">
    <asp:Label ID="Label3" runat="server" Text="Linea:"></asp:Label>&nbsp;&nbsp;
    </div>
    <div style="display:table-cell;padding:5px;border-top:1px solid #ccc;border-bottom:1px solid #ccc;">
     <asp:DropDownList ID="DropDownList1" runat="server">
                <%--<asp:ListItem Value="0">Select a Value</asp:ListItem>
                <asp:ListItem Value="1">1</asp:ListItem>
                <asp:ListItem Value="2">2</asp:ListItem>
                <asp:ListItem Value="3">3</asp:ListItem>
                <asp:ListItem Value="4">4</asp:ListItem>
				<asp:ListItem Value="5">5</asp:ListItem>
				<asp:ListItem Value="6">6</asp:ListItem>
				<asp:ListItem Value="7">7</asp:ListItem>
				<asp:ListItem Value="8">8</asp:ListItem>
				<asp:ListItem Selected="True" Value="9">9</asp:ListItem>--%>
    </asp:DropDownList>
    </div>
    <div style="display:table-cell;padding:5px;border-left:1px solid #ccc;border-bottom:1px solid #ccc;border-top:1px solid #ccc;">
    <asp:Label ID="Label2" runat="server" Text="Fecha Inicial: "></asp:Label>&nbsp;&nbsp;
    </div>
    <div style="display:table-cell;padding:5px;border-bottom:1px solid #ccc;border-top:1px solid #ccc;">
        <dx:ASPxDateEdit ID="txtfecha1" runat="server" AnimationType="Fade" width="100px">
        </dx:ASPxDateEdit>   
    </div>
        <div style="display:table-cell;padding:5px;border-left:1px solid #ccc;border-bottom:1px solid #ccc;border-top:1px solid #ccc;">
    <asp:Label ID="Label4" runat="server" Text="Fecha Final: "></asp:Label>&nbsp;&nbsp;
    </div>
    <div style="display:table-cell;padding:5px;border-bottom:1px solid #ccc;border-top:1px solid #ccc;">
        <dx:ASPxDateEdit ID="txtfecha2" runat="server" AnimationType="Fade" width="100px">
        </dx:ASPxDateEdit>   
    </div>
    <div style="display:table-cell;padding:5px;border-top:1px solid #ccc;border-right:1px solid #ccc;border-bottom:1px solid #ccc;">
        <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Buscar" 
            onclick="ASPxButton1_Click" Theme="MetropolisBlue">
        </dx:ASPxButton>    
    </div>
    </div>    
    </center>
    </div>
<br />
<center>
<div width="80%">
	       <dx:ASPxLabel ID="txtmensaje" runat="server" Text="" Visible="False">
    </dx:ASPxLabel>
</div>
</center>
<center>
    <div class="col-sm-10 col-sm-offset-1" style="margin-bottom:40px;">
   <div class="col-lg-6">
        <dx:ASPxGridView ID="GV_Defects" runat="server" EnableTheming="True" Max-Width="600px"
            Theme="Moderno" AutoGenerateColumns="False" 
            DataSourceID="SqlDataSource1">
            <SettingsPager EnableAdaptivity="True" PageSize="15">
            </SettingsPager>
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" 
                AllowInsert="False" />
            <SettingsSearchPanel Visible="True" />            
            <Columns>
                <dx:GridViewDataTextColumn FieldName="No" ReadOnly="True" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
				 <dx:GridViewDataTextColumn FieldName="POrder" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
				 <dx:GridViewDataTextColumn FieldName="Style" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
				 <dx:GridViewDataTextColumn FieldName="Size" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Operacion" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Area" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Defecto" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Cantidad" ReadOnly="True" 
                    VisibleIndex="7">
                </dx:GridViewDataTextColumn>
            </Columns>
           <SettingsDataSecurity AllowDelete="False" AllowEdit="False" 
            AllowInsert="False" />
            <Settings ShowFooter="true" ShowGroupFooter="VisibleAlways" />
            <TotalSummary>
            <dx:ASPxSummaryItem FieldName="Cantidad" ShowInGroupFooterColumn="Cantidad" DisplayFormat="Total: {0:n0}" SummaryType="Sum" />
            </TotalSummary>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:RocedesCS %>" SelectCommand="select ROW_NUMBER() OVER (order by sum(br.cantidad_defectos) desc) as No, Oper.descripcion as Operacion, AD.Nombre as Area, D.nombre as Defecto, sum(br.cantidad_defectos) as Defectos from BundleRejected br  
inner join Operario O on O.id_operario= br.Id_operario
inner join Operacion Oper on O.id_operacion=Oper.id_operacion 
inner join DefectosxArea DA on DA.Id_DefxArea=BR.Id_DefxArea
inner join AreaDefecto AD on DA.Id_Area = AD.Id_Area
inner join Defectos D on DA.Id_defectos=D.id_defectos
where br.id_operario in (select id_operario from operario where Id_Seccion=1) 
and br.FechaRecibido between '2016-01-13' and '2016-01-14' and br.Id_Seccion=1
group by  Oper.descripcion, AD.Nombre, D.nombre
"></asp:SqlDataSource>
    </div>
    <div id="Chart_Defectos" class="col-lg-6">
	  <dx:ASPxGridViewExporter ID="ExportToExcell" GridViewID="GV_Defects" runat="server">
            <Styles>
                <Header BackColor="#006699" ForeColor="White">
                </Header>
            </Styles>
        </dx:ASPxGridViewExporter>
   <dx:ASPxButton ID="btnexport" runat="server" Text="Exportar" Theme="Moderno" Width="100px" style="margin-bottom:12px;" onclick="btnexport_Click" >
             <Image IconID="export_exporttoxls_16x16">
             </Image>
            </dx:ASPxButton>
    <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" 
        CrosshairEnabled="True" SeriesDataMember="Area" Height="400px" 
        Width="600px" AppearanceNameSerializable="Pastel Kit" AutoLayout="True">
        <diagramserializable>
            <cc1:XYDiagram>
                <axisx visibleinpanesserializable="-1">
                    <gridlines visible="True">
                    </gridlines>
                </axisx>
                <axisy visibleinpanesserializable="-1">
                </axisy>
            </cc1:XYDiagram>
        </diagramserializable>
        <legend visibility="True"></legend>
        <seriestemplate argumentdatamember="Defecto" labelsvisibility="True" 
            valuedatamembersserializable="Defectos">
            <viewserializable>
                <cc1:SideBySideBarSeriesView>
                    <fillstyle fillmode="Solid">
                    </fillstyle>
                </cc1:SideBySideBarSeriesView>
            </viewserializable>
            <labelserializable>
                <cc1:SideBySideBarSeriesLabel TextPattern="Qty:{V}" Position="Top">
                </cc1:SideBySideBarSeriesLabel>
            </labelserializable>
        </seriestemplate>
    </dxchartsui:WebChartControl>
    </div>
    </div>    

</center>
</asp:Content>

