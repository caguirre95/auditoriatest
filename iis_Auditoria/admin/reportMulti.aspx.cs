﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_reportMulti : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnGenerar_Click(object sender, EventArgs e)
    {
        ASPxGridView1.DataBind();
   //     ASPxGridView5.DataBind();
        //
        //    functionNivel1();
        //  level2();
        //  DataTable dt_Line = DataAccess.Get_DataTable(query1);
    }

    void functionNivel1()
    {
        string query1, query2, query3, query4;

        query1 = "select NombreMedida as Area, 'Total'as Medida, isnull([1],0) as '+2',isnull([2],0) as '+1 7/8' ,isnull([3],0) as '+1 3/4' ,isnull([4],0) as '+1 5/8'"
                     + ", isnull([5], 0) as '+1 1/2',isnull([6],0) as '+1 3/8',isnull([7],0)as '+1 1/4',isnull([8],0)as '+1 1/8',"
                     + "isnull([9],0)as '+1',isnull([10],0)as '+7/8',isnull([11],0)as '+3/4',isnull([12],0) as '+5/8',isnull([13],0) as '+1/2',isnull([14],0)as '+3/8',isnull([15],0)as '+1/4',isnull([16],0)as '+1/8'"
                     + ",isnull([17],0)as '+/-0'"
                     + ",isnull([33],0)as '-1/8',isnull([32],0)as '-1/4',isnull([31],0)as '-3/8',isnull([30],0)as '-1/2',isnull([29],0)as '-5/8',isnull([28],0)as '-3/4',isnull([27],0)as '-7/8',isnull([26],0)as '-1'"
                     + ",isnull([25],0)as '-1 1/8',isnull([24],0)as '-1 1/4',isnull([23],0)as '-1 3/8',isnull([22],0)as '-1 1/2',isnull([21],0)as '-1 5/8',isnull([20],0)as '-1 3/4',isnull([19],0)as '-1 7/8'"
                     + ",isnull([18],0)as '-2'"
                + " from"
                + " ("
                + " select c2.NombreMedida,c1.idMedida, ISNULL(c2.total,0)as total from"
                + " (select m.idMedida, m.Medida, m.valor from tblMedidas m) as c1"
                + " left join"
                + " (select pts.NombreMedida,m.idMedida as ID, m.valor as valor, m.Medida as Medida, COUNT(mb.idMedida) as total"
                + " from tblPuntoMasterAL pm"
                + " join tblMedidaBultoAL mb on pm.idPuntoMaster=mb.idMaster"
                + " join tblPuntosMedida pts on pm.idpuntosM=pts.idPuntosM"
                + " join tblMedidas m on mb.idMedida= m.idMedida"
                + " where CONVERT(date, mb.fecha) between '" + txtfecha1.Date + "' and '" + txtfecha2.Date + "' and pm.rol='inspInter'"
                + " group by  pts.NombreMedida,m.idMedida, m.valor, m.Medida)as c2"
                + " on c1.idMedida=c2.ID"
                + " ) p"
                + " pivot"
                + " ("
                + "  sum(total)"
                + "  for idMedida in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],"
                + "[11],[12],[13],[14],[15],[16],[17],[18],[19],"
                + "[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],"
                + "[30],[31],[32],[33])"
                + " ) as pvt";

        query2 = "select NombreMedida as Area, 'Total'as Medida, isnull([1],0) as '+2',isnull([2],0) as '+1 7/8' ,isnull([3],0) as '+1 3/4' ,isnull([4],0) as '+1 5/8'"
                     + ", isnull([5], 0) as '+1 1/2',isnull([6],0) as '+1 3/8',isnull([7],0)as '+1 1/4',isnull([8],0)as '+1 1/8',"
                     + "isnull([9],0)as '+1',isnull([10],0)as '+7/8',isnull([11],0)as '+3/4',isnull([12],0) as '+5/8',isnull([13],0) as '+1/2',isnull([14],0)as '+3/8',isnull([15],0)as '+1/4',isnull([16],0)as '+1/8'"
                     + ",isnull([17],0)as '+/-0'"
                     + ",isnull([33],0)as '-1/8',isnull([32],0)as '-1/4',isnull([31],0)as '-3/8',isnull([30],0)as '-1/2',isnull([29],0)as '-5/8',isnull([28],0)as '-3/4',isnull([27],0)as '-7/8',isnull([26],0)as '-1'"
                     + ",isnull([25],0)as '-1 1/8',isnull([24],0)as '-1 1/4',isnull([23],0)as '-1 3/8',isnull([22],0)as '-1 1/2',isnull([21],0)as '-1 5/8',isnull([20],0)as '-1 3/4',isnull([19],0)as '-1 7/8'"
                     + ",isnull([18],0)as '-2'"
             + " from"
             + " ("
             + " select c2.NombreMedida,c1.idMedida, ISNULL(c2.total,0)as total from"
             + " (select m.idMedida, m.Medida, m.valor from tblMedidas m) as c1"
             + " left join"
             + " (select pts.NombreMedida,m.idMedida as ID, m.valor as valor, m.Medida as Medida, COUNT(mb.idMedida) as total"
             + " from tblPuntoMasterAL pm"
             + " join tblMedidaBultoAL mb on pm.idPuntoMaster=mb.idMaster"
             + " join tblPuntosMedida pts on pm.idpuntosM=pts.idPuntosM"
             + " join tblMedidas m on mb.idMedida= m.idMedida"
             + " where CONVERT(date, mb.fecha) between '" + txtfecha1.Date + "' and '" + txtfecha2.Date + "' and pm.rol='bultoabulto'"
             + " group by  pts.NombreMedida,m.idMedida, m.valor, m.Medida)as c2"
             + " on c1.idMedida=c2.ID"
             + " ) p"
             + " pivot"
             + " ("
             + "  sum(total)"
             + "  for idMedida in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],"
             + "[11],[12],[13],[14],[15],[16],[17],[18],[19],"
             + "[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],"
             + "[30],[31],[32],[33])"
             + "  ) as pvt";

        query3 = "select NombreMedida as Area, 'Total'as Medida, isnull([1],0) as '+2',isnull([2],0) as '+1 7/8' ,isnull([3],0) as '+1 3/4' ,isnull([4],0) as '+1 5/8'"
                     + ", isnull([5], 0) as '+1 1/2',isnull([6],0) as '+1 3/8',isnull([7],0)as '+1 1/4',isnull([8],0)as '+1 1/8',"
                     + "isnull([9],0)as '+1',isnull([10],0)as '+7/8',isnull([11],0)as '+3/4',isnull([12],0) as '+5/8',isnull([13],0) as '+1/2',isnull([14],0)as '+3/8',isnull([15],0)as '+1/4',isnull([16],0)as '+1/8'"
                     + ",isnull([17],0)as '+/-0'"
                     + ",isnull([33],0)as '-1/8',isnull([32],0)as '-1/4',isnull([31],0)as '-3/8',isnull([30],0)as '-1/2',isnull([29],0)as '-5/8',isnull([28],0)as '-3/4',isnull([27],0)as '-7/8',isnull([26],0)as '-1'"
                     + ",isnull([25],0)as '-1 1/8',isnull([24],0)as '-1 1/4',isnull([23],0)as '-1 3/8',isnull([22],0)as '-1 1/2',isnull([21],0)as '-1 5/8',isnull([20],0)as '-1 3/4',isnull([19],0)as '-1 7/8'"
                     + ",isnull([18],0)as '-2'"
                + " from"
                + " ("
                + " select c2.NombreMedida,c1.idMedida, ISNULL(c2.total,0)as total from"
                + " (select m.idMedida, m.Medida, m.valor from tblMedidas m) as c1"
                + " left join"
                + " (select pts.NombreMedida,m.idMedida as ID, m.valor as valor, m.Medida as Medida, COUNT(mb.idMedida) as total"
                + " from tblPuntoMasterIntex pm"
                + " join tblMedidaBultoIntex mb on pm.idPuntoMaster=mb.idMaster"
                + " join tblPuntosMedida pts on pm.idpuntosM=pts.idPuntosM"
                + " join tblMedidas m on mb.idMedida= m.idMedida"
                + " where CONVERT(date, mb.fecha) between '" + txtfecha1.Date + "' and '" + txtfecha2.Date + "'"
                + " group by  pts.NombreMedida,m.idMedida, m.valor, m.Medida)as c2"
                + " on c1.idMedida=c2.ID"
                + " ) p"
                + " pivot"
                + " ("
                + "  sum(total)"
                + "  for idMedida in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],"
                + "[11],[12],[13],[14],[15],[16],[17],[18],[19],"
                + "[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],"
                + "[30],[31],[32],[33])"
                + "  ) as pvt";


        query4 = "select NombreMedida as Area, 'Total'as Medida, isnull([1],0) as '+2',isnull([2],0) as '+1 7/8' ,isnull([3],0) as '+1 3/4' ,isnull([4],0) as '+1 5/8'"
                     + ", isnull([5], 0) as '+1 1/2',isnull([6],0) as '+1 3/8',isnull([7],0)as '+1 1/4',isnull([8],0)as '+1 1/8',"
                     + "isnull([9],0)as '+1',isnull([10],0)as '+7/8',isnull([11],0)as '+3/4',isnull([12],0) as '+5/8',isnull([13],0) as '+1/2',isnull([14],0)as '+3/8',isnull([15],0)as '+1/4',isnull([16],0)as '+1/8'"
                     + ",isnull([17],0)as '+/-0'"
                     + ",isnull([33],0)as '-1/8',isnull([32],0)as '-1/4',isnull([31],0)as '-3/8',isnull([30],0)as '-1/2',isnull([29],0)as '-5/8',isnull([28],0)as '-3/4',isnull([27],0)as '-7/8',isnull([26],0)as '-1'"
                     + ",isnull([25],0)as '-1 1/8',isnull([24],0)as '-1 1/4',isnull([23],0)as '-1 3/8',isnull([22],0)as '-1 1/2',isnull([21],0)as '-1 5/8',isnull([20],0)as '-1 3/4',isnull([19],0)as '-1 7/8'"
                     + ",isnull([18],0)as '-2'"
                + " from"
                + " ("
                + " select c2.NombreMedida,c1.idMedida, ISNULL(c2.total,0)as total from"
                + " (select m.idMedida, m.Medida, m.valor from tblMedidas m) as c1"
                + " left join"
                + " (select pts.NombreMedida,m.idMedida as ID, m.valor as valor, m.Medida as Medida, COUNT(mb.idMedida) as total"
                + " from tblPuntoMaster pm"
                + " join tblMedidaBulto mb on pm.idPuntoMaster=mb.idMaster"
                + " join tblPuntosMedida pts on pm.idpuntosM=pts.idPuntosM"
                + " join tblMedidas m on mb.idMedida= m.idMedida"
                + " where CONVERT(date, mb.fecha) between '" + txtfecha1.Date + "' and '" + txtfecha2.Date + "'"
                + " group by  pts.NombreMedida,m.idMedida, m.valor, m.Medida)as c2"
                + " on c1.idMedida=c2.ID"
                + " ) p"
                + " pivot"
                + " ("
                + "  sum(total)"
                + "  for idMedida in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],"
                + "[11],[12],[13],[14],[15],[16],[17],[18],[19],"
                + "[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],"
                + "[30],[31],[32],[33])"
                + " ) as pvt";


        DataTable dt1 = DataAccess.Get_DataTable(query1);
        DataTable dt2 = DataAccess.Get_DataTable(query2);
        DataTable dt3 = DataAccess.Get_DataTable(query3);
        DataTable dt4 = DataAccess.Get_DataTable(query4);

        ASPxGridView1.DataSource = dt1;
        ASPxGridView2.DataSource = dt2;
        ASPxGridView3.DataSource = dt3; 
        ASPxGridView4.DataSource = dt4;
       

    }

    void level3()
    {
        string query = "select isnull(Size,' ') as Style,isnull(NombreMedida,'')as Area, 'Total'as Medida,isnull([1],0) as '+2',isnull([2],0) as '+1 7/8' ,isnull([3],0) as '+1 3/4' ,isnull([4],0) as '+1 5/8'"
                     + ",isnull([5], 0) as '+1 1/2',isnull([6],0) as '+1 3/8',isnull([7],0)as '+1 1/4',isnull([8],0)as '+1 1/8',"
                     + "isnull([9],0)as '+1',isnull([10],0)as '+7/8',isnull([11],0)as '+3/4',isnull([12],0) as '+5/8',isnull([13],0) as '+1/2',isnull([14],0)as '+3/8',isnull([15],0)as '+1/4',isnull([16],0)as '+1/8'"
                     + ",isnull([17],0)as '+/-0'"
                     + ",isnull([33],0)as '-1/8',isnull([32],0)as '-1/4',isnull([31],0)as '-3/8',isnull([30],0)as '-1/2',isnull([29],0)as '-5/8',isnull([28],0)as '-3/4',isnull([27],0)as '-7/8',isnull([26],0)as '-1'"
                     + ",isnull([25],0)as '-1 1/8',isnull([24],0)as '-1 1/4',isnull([23],0)as '-1 3/8',isnull([22],0)as '-1 1/2',isnull([21],0)as '-1 5/8',isnull([20],0)as '-1 3/4',isnull([19],0)as '-1 7/8'"
                     + ",isnull([18],0)as '-2'"
                     + "from"
                     + "("
                     + "select c2.Size, c2.NombreMedida, c1.idMedida, ISNULL(c2.total, 0) as total from"
                     + "(select m.idMedida, m.Medida, m.valor from tblMedidas m) as c1"
                     + " left join"
                     + "(select b.Size, pts.NombreMedida, m.idMedida as ID, m.valor as valor, m.Medida as Medida, COUNT(mb.idMedida) as total"
                     + " from tblPuntoMasterAL pm"
                     + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                     + " join tblPuntosMedida pts on pm.idpuntosM = pts.idPuntosM"
                     + " join tblMedidas m on mb.idMedida = m.idMedida"
                     + " join Bundle b on pm.idbulto = b.Id_Bundle"
                     + " where pm.idpuntosM = 1 and CONVERT(date, mb.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and pm.rol = 'inspInter'"
                     + " group by b.Size, pts.NombreMedida, m.idMedida, m.valor, m.Medida) as c2"
                     + " on c1.idMedida = c2.ID"
                     + " ) p"
                     + " pivot"
                     + " ("
                     + " sum(total)"
                     + " for idMedida in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],"
                     + "[11],[12],[13],[14],[15],[16],[17],[18],[19],"
                     + "[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],"
                     + "[30],[31],[32],[33])"
                     + ") as pvt"
                     + "  order by pvt.Size,pvt.NombreMedida asc";

        DataTable dt1 = DataAccess.Get_DataTable(query);

        ASPxGridView6.DataSource = dt1;
     
    }

    void level2()
    {
        string query = "select isnull(Style,' ') as Style,isnull(NombreMedida,'')as Area, 'Total'as Medida, isnull([1],0) as '+2',isnull([2],0) as '+1 7/8' ,isnull([3],0) as '+1 3/4' ,isnull([4],0) as '+1 5/8'  ,isnull([5],0) as '+1 1/2',isnull([6],0) as '+1 3/8',isnull([7],0)as '+1 1/4',isnull([8],0)as '+1 1/8',"
       +"isnull([9],0)as '+1',isnull([10],0)as '+7/8',isnull([11],0)as '+3/4',isnull([12],0) as '+5/8',isnull([13],0) as '+1/2',isnull([14],0)as '+3/8',isnull([15],0)as '+1/4',isnull([16],0)as '+1/8'"
	   +",isnull([17],0)as '+/-0'"
	   +",isnull([33],0)as '-1/8',isnull([32],0)as '-1/4',isnull([31],0)as '-3/8',isnull([30],0)as '-1/2',isnull([29],0)as '-5/8',isnull([28],0)as '-3/4',isnull([27],0)as '-7/8',isnull([26],0)as '-1'"
	   +",isnull([25],0)as '-1 1/8',isnull([24],0)as '-1 1/4',isnull([23],0)as '-1 3/8',isnull([22],0)as '-1 1/2',isnull([21],0)as '-1 5/8',isnull([20],0)as '-1 3/4',isnull([19],0)as '-1 7/8',isnull([18],0)as '-2'"
       +" from"
       +" ("
       +" select c2.Style, c2.NombreMedida, c1.idMedida, ISNULL(c2.total, 0) as total from"
       +" (select m.idMedida, m.Medida, m.valor from tblMedidas m) as c1"
       +" left join"
       +" (select s.Style, pts.NombreMedida, m.idMedida as ID, m.valor as valor, m.Medida as Medida, COUNT(mb.idMedida) as total"
       +" from tblPuntoMasterAL pm"
       +" join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
       +" join tblPuntosMedida pts on pm.idpuntosM = pts.idPuntosM"
       +" join tblMedidas m on mb.idMedida = m.idMedida"
       +" join Bundle b on pm.idbulto = b.Id_Bundle"
       +" join POrder p on b.Id_Order = p.Id_Order"
       +" join Style s on p.Id_Style = s.Id_Style"
       +" where CONVERT(date, mb.fecha) between '"+txtfecha1.Text+"' and '"+txtfecha2.Text+"'"
       +" group by s.Style, pts.NombreMedida, m.idMedida, m.valor, m.Medida) as c2"
       +" on c1.idMedida = c2.ID"
       +" ) p"
       +" pivot"
       +" ("
       +" sum(total)"
       +" for idMedida in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],"
       +"[11],[12],[13],[14],[15],[16],[17],[18],[19],"
       +"[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],"
       +"[30],[31],[32],[33])"
	   +") as pvt"
       +" order by pvt.Style,pvt.NombreMedida asc";


        DataTable dt1 = DataAccess.Get_DataTable(query);

        ASPxGridView5.DataSource = dt1;

    }


    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        functionNivel1();
    }

    protected void ASPxGridView1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView1.DataBind();
    }

    protected void ASPxGridView5_DataBinding(object sender, EventArgs e)
    {
        level2();
    }

    protected void ASPxGridView5_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView5.DataBind();
    }

    protected void ASPxGridView6_DataBinding(object sender, EventArgs e)
    {
        level3();
    }

    protected void ASPxGridView6_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView6.DataBind();
    }
}