﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IrregularesReport2.aspx.cs" Inherits="admin_IrregularesReport" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "IrregularesReport2.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,                                    
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);                    
                    // $('#<=txtstyle.ClientID%>').val(ui.item.style);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        })
    </script>

    <script lang="javascript" type="text/javascript">
        function autoestilo() {

            $(function () {
                $('#<%=txtstyle.ClientID%>').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "IrregularesReport2.aspx/GetStyle",
                            data: "{'pref' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        estilo: item.estilo,
                                        id: item.id,
                                        json: item
                                    }
                                }))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                        return false;
                    },
                    select: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);
                        $('#<%=hdnidstyle.ClientID%>').val(ui.item.id);

                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li  >")
                        .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
                };
            })
        }
    </script>

    <script type="text/javascript">
        function date1() {
            $('#<%=txtfecha1.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

        function date2() {
            $('#<%=txtfecha2.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

    </script>

    <style>
        .m {
            margin-top: 25px;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <br />
    <div class="container">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

            <div class="panel panel-primary" style="margin-top: 5px">
                <div class="panel-heading">Reporte Irregulares Antes de lavado</div>
                <div class="panel-body">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="panel-default">
                                <div class="panel-heading">Filtros</div>
                                <div class="panel-body">

                                    <asp:RadioButtonList ID="rd1" runat="server">
                                        <asp:ListItem Value="1">Cut</asp:ListItem>
                                        <asp:ListItem Value="2">Style</asp:ListItem>
                                        <asp:ListItem Value="3">Line</asp:ListItem>
                                        <asp:ListItem Value="4">Plant</asp:ListItem>
                                    </asp:RadioButtonList>

                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="panel-default">
                                <div class="panel-heading">Opciones</div>
                                <div class="panel-body">
                                    <asp:Panel runat="server" ID="panelCorte">
                                        Cut:                                   
                                    <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        <asp:HiddenField runat="server" ID="hdnidporder" />
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="panelEstilo">
                                        Style:                                   
                                    <asp:TextBox ID="txtstyle" onkeypress="autoestilo()" AutoCompleteType="Disabled" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        <asp:HiddenField runat="server" ID="hdnidstyle" />
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="panelLinea">
                                        Line:
                                     <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="panelPlanta">
                                        Plant:
                                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="panel-default">
                                <div class="panel-heading">Select </div>
                                <div class="panel-body">
                                    <asp:Panel runat="server" ID="panelfechas">
                                        Start Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                        End Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                    </asp:Panel>
                                    <div style="padding-top: 15px">
                                        <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" runat="server" OnClick="btngenerar_Click" />
                                    </div>
                                    <div style="padding-top: 15px">
                                        <asp:Button Text="Limpiar Campos" CssClass="btn btn-info form-control" ID="btnlimpiar" runat="server" OnClick="btnlimpiar_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <hr />
                        <dx1:ASPxGridView ID="grdIrregular" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdIrregulares_DataBinding" OnCustomCallback="grdIrregulares_CustomCallback">
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Reporte Irregulares B/W" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="Planta" Caption="Planta" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Linea" Caption="Linea" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Corte" Caption="Corte" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Quantity" Caption="Cantidad del Corte" Settings-AllowCellMerge="True" CellStyle-HorizontalAlign="Center"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="talla" Caption="Talla"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="area" Caption="Area"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Defecto" Caption="Defecto"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Posicion" Caption="Posicion"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Cantidad" Caption="Cantidad"></dx1:GridViewDataTextColumn>
                                    </Columns>                                    
                                </dx1:GridViewBandColumn>                                
                            </Columns>
                            <Settings ShowFooter="true" VerticalScrollBarMode="Visible" ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                            <SettingsPager PageSize="100"></SettingsPager>
                            <TotalSummary>                                
                                <dx1:ASPxSummaryItem FieldName="Cantidad" SummaryType="Sum" DisplayFormat="Total: {0}" />
                            </TotalSummary>                           
                        </dx1:ASPxGridView>

                        <hr />
                        <dx1:ASPxGridView ID="grdporcent" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdporcent_DataBinding" OnCustomCallback="grdporcent_CustomCallback">
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Porcentaje por Defecto Irregulares B/W" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="Planta" Caption="Planta" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Linea" Caption="Linea" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Corte" Caption="Corte" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Quantity" Caption="Cantidad del Corte" Settings-AllowCellMerge="True" CellStyle-HorizontalAlign="Center"></dx1:GridViewDataTextColumn>                                        
                                        <dx1:GridViewDataTextColumn FieldName="Defecto" Caption="Defecto"></dx1:GridViewDataTextColumn>                                        
                                        <dx1:GridViewDataTextColumn FieldName="Cantidad" Caption="Cantidad"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Porcentaje" Caption="% Por Defecto" PropertiesTextEdit-DisplayFormatString="{0} %"></dx1:GridViewDataTextColumn>
                                    </Columns>                                    
                                </dx1:GridViewBandColumn>                                
                            </Columns>
                            <Settings ShowFooter="true" VerticalScrollBarMode="Visible" ShowGroupFooter="VisibleAlways" />
                            <SettingsPager PageSize="100"></SettingsPager>
                            <TotalSummary>                                
                                <dx1:ASPxSummaryItem FieldName="Cantidad" SummaryType="Sum" DisplayFormat="Unidades {0}" />
                            </TotalSummary> 
                        </dx1:ASPxGridView>

                        <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control m" Text="Export to Excel" OnClick="btnexcel_Click" />
                        <dx1:ASPxGridViewExporter ID="export" runat="server"></dx1:ASPxGridViewExporter>
                        <dx1:ASPxGridViewExporter ID="exportPorcent" runat="server"></dx1:ASPxGridViewExporter>

                    </div>

                </div>
            </div>



        </div>
    </div>

</asp:Content>

