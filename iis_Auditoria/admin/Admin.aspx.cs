﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SistemaAuditores.DataAccess;
using System.Data;

public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Load_Data();
        }

    }

    public void Load_Data()
    {
        /*DataTable dt_production_pervasive = new DataTable();
        dt_production_pervasive = DataAccess.Get_DataTable("select * from ProductionPervasive");
        DataTable dt_customer = new DataTable();
        dt_customer = DataAccess.Get_DataTable("select * from Production_Customer");
        for (int i = 0; i <= dt_production_pervasive.Rows.Count - 1; i++)
        {
            for (int j = 0; j <= dt_customer.Rows.Count - 1; j++)
            {
                if (Convert.ToString(dt_production_pervasive.Rows[i].ItemArray[2]).Trim() == Convert.ToString(dt_customer.Rows[j].ItemArray[1]).Trim())
                {
                    dt_production_pervasive.Rows[i]["Id_Cliente"] = dt_customer.Rows[j].ItemArray[0];
                    break;
                }
            }
        }*/

       // GridView1.DataSource = dt_production_pervasive;
       // GridView1.DataBind();
    }

    public void UpdateIdCliente()
    {
        try
        {
          /*  DataTable dt_production_pervasive = new DataTable();
            dt_production_pervasive = DataAccess.Get_DataTable("select * from ProductionPervasive");
            DataTable dt_customer = new DataTable();
            dt_customer = DataAccess.Get_DataTable("select * from Production_Customer");
            for (int i = 0; i <= dt_production_pervasive.Rows.Count - 1; i++)
            {
                for (int j = 0; j <= dt_customer.Rows.Count - 1; j++)
                {
                    if (Convert.ToString(dt_production_pervasive.Rows[i].ItemArray[2]).Trim() == Convert.ToString(dt_customer.Rows[j].ItemArray[1]).Trim())
                    {
                        dt_production_pervasive.Rows[i]["Id_Cliente"] = dt_customer.Rows[j].ItemArray[0];
                        break;
                    }
                }
            }

            // dt_production_pervasive.Columns["Id_Cliente"].SetOrdinal(1);
          //  GridView1.DataSource = dt_production_pervasive;
         //   GridView1.DataBind();

            for (int i = 0; i <= dt_production_pervasive.Rows.Count - 1; i++)
            {
                DataAccess.UpdatePOrder(Convert.ToInt16(dt_production_pervasive.Rows[i]["Id_Cliente"]), Convert.ToInt16(dt_production_pervasive.Rows[i][0]));
            }
            string message = "alert('Actualizacion exitosa!');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);*/
        }
        catch (Exception ex)
        {
            string message = "alert('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        }
    }

    public void Update_IdStyle()
    {
        try
        {
            DataTable dt_production_pervasive = new DataTable();
            dt_production_pervasive = DataAccess.Get_DataTable("select * from ProductionPervasive");
            DataTable dt_styles = new DataTable();
            dt_styles = DataAccess.Get_DataTable("select * from Production_Styles");
            for (int i = 0; i <= dt_production_pervasive.Rows.Count - 1; i++)
            {
                for (int j = 0; j <= dt_styles.Rows.Count - 1; j++)
                {
                    if (Convert.ToString(dt_production_pervasive.Rows[i].ItemArray[4]).Trim() == Convert.ToString(dt_styles.Rows[j].ItemArray[1]).Trim())
                    {
                        dt_production_pervasive.Rows[i]["Id_Estilo"] = dt_styles.Rows[j].ItemArray[0];
                        break;
                    }
                }
            }

            GridView1.DataSource = dt_production_pervasive;
            GridView1.DataBind();

            for (int i = 0; i <= dt_production_pervasive.Rows.Count - 1; i++)
            {
                DataAccess.UpdatePOrderStyles(Convert.ToInt16(dt_production_pervasive.Rows[i]["Id_Estilo"]), Convert.ToInt16(dt_production_pervasive.Rows[i][0]));
            }

            string message = "alert('Actualizacion exitosa!');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        }
        catch (Exception ex)
        {
            string message = "alert('" + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        }
    }

    protected void lnk_NewPOrder_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProdOrders.aspx");
    }
    protected void lnk_UpdateIdCliente_Click(object sender, EventArgs e)
    {
        UpdateIdCliente();
    }
    protected void lnk_UpdateIdEstilo_Click(object sender, EventArgs e)
    {
        Update_IdStyle();
    }
    protected void lnk_NewBundles_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddBundles.aspx");
    }
    protected void lnk_ConvertBundles_Click(object sender, EventArgs e)
    {
        Response.Redirect("Bundles.aspx");
    }
    protected void imgNewPO_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("ProdOrders.aspx");
    }
    protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("Bundles.aspx");
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        Load_Data();
    }
}