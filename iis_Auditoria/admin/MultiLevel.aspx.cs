﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using DevExpress.XtraPrinting;

public partial class admin_MultiLevel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();
            Session["dt"] = null;
        }
    }

    protected void grid_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grid.DataBind();
    }

    protected void grid_DataBinding(object sender, EventArgs e)
    {   
        grid.DataSource = Session["dt"];
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "0")
        {
            return;
        }

        DataTable dt = new DataTable();
        int resp = int.Parse(DropDownListProcess.SelectedValue);
       
        switch (resp)
        {
            case 1:

                dt = OrderDetailDA.ReportqueryMulti(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text), RadioButtonList1.SelectedValue);
                break;
            case 2:


                dt = OrderDetailDA.ReportqueryMultiIntex(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text));
                break;
            case 3:

                dt = OrderDetailDA.ReportqueryMultiMod4(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text));
                break;
            default:
                break;
        }


        Session["dt"] = dt;   
        grid.DataBind();


    }

    protected void DropDownListProcess_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "1")
        {
            Panel1.Visible = true;
        }
        else
        {
            Panel1.Visible = false;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "0")
        {
            return;
        }

        DataTable dt = new DataTable();
        int resp = int.Parse(DropDownListProcess.SelectedValue);

        switch (resp)
        {
            case 1:

                dt = OrderDetailDA.ReportqueryMulti(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text), RadioButtonList1.SelectedValue);
                break;
            case 2:


                dt = OrderDetailDA.ReportqueryMultiIntex(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text));
                break;
            case 3:

                dt = OrderDetailDA.ReportqueryMultiMod4(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text));
                break;
            default:
                break;
        }


        Session["dt"] = dt;
        grid.DataBind();

        ASPxGridViewExporter1.GridViewID = "grid";

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
   
        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link1.Component = ASPxGridViewExporter1;

        
        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link1});

        compositeLink.CreatePageForEachLink();

        using (MemoryStream stream = new MemoryStream())
        {
            XlsxExportOptions options = new XlsxExportOptions();
            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
            options.SheetName = "Master_Report_Line";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=Multilevel_Report.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }
        ps.Dispose();
    }
}