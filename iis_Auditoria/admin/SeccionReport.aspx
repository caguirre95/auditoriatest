﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SeccionReport.aspx.cs" Inherits="admin_Report3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h3>
        <asp:Label ID="Label1" runat="server" Text="Live Checkoff Report"></asp:Label></h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">

        <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>
        <center>

            <div class="col-md-4">           
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="/img/rank.png" Width="128px" Height="128px" onclick="ImageButton2_Click" /> <br />
                    <asp:LinkButton ID="LinkButton2" runat="server" onclick="LinkButton2_Click">Defectos</asp:LinkButton>
            </div>

           <div class="col-md-4">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="/img/pie.png" Width="128px" Height="128px" onclick="ImageButton1_Click" /> <br />
                    <asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click">AQL / OQL</asp:LinkButton>
            </div>

            <div class="col-md-4">
                    <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="/img/tabla1.png" Width="128px" Height="128px" onclick="ImageButton3_Click" /><br />
                    <asp:LinkButton ID="LinkButton3" runat="server" onclick="LinkButton3_Click">Tabla Defectos</asp:LinkButton>
            </div>
                       
            <div class="col-md-4" style="margin-top:30px">   
                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/img/tabla.png" Width="128px" Height="128px" onclick="ImageButton4_Click" /><br />
                    <asp:LinkButton ID="LinkButton4" runat="server" onclick="LinkButton4_Click">Historial Auditorias</asp:LinkButton>
            </div>

            <div class="col-md-4" style="margin-top:30px">
                    <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/img/packing.png" Width="120px" Height="128px" onclick="ImageButton5_Click" /> <br />
                    <asp:LinkButton ID="LinkButton5" runat="server" onclick="LinkButton5_Click">Embarque</asp:LinkButton>
            </div>

        <div class="col-md-4" style="margin-top:30px">   
                <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/img/tabla.png" Width="128px" Height="128px" onclick="ImageButton6_Click" /><br />
                    <asp:LinkButton ID="LinkButton6" runat="server" onclick="LinkButton6_Click">Historial Defecto</asp:LinkButton>
            </div>

        <div class="col-md-4" style="margin-top:30px">   
                <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/img/line_iinspection.png" Width="128px" Height="128px" onclick="ImageButton7_Click" /><br />
                    <asp:LinkButton ID="LinkButton7" runat="server" onclick="LinkButton7_Click">Defecto Embarque</asp:LinkButton>
            </div>

            <div class="col-md-4" style="margin-top:30px">   
                <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/img/biho.png" Width="128px" Height="128px" onclick="ImageButton8_Click" /><br />
                    <asp:LinkButton ID="LinkButton8" runat="server" onclick="LinkButton8_Click">Dashboard</asp:LinkButton>
            </div>

             <div class="col-md-4" style="margin-top:30px">   
                <asp:ImageButton ID="ImageButton9" runat="server" ImageUrl="~/img/bundle_by_bundle.png" Width="128px" Height="128px" onclick="ImageButton9_Click" /><br />
                    <asp:LinkButton ID="LinkButton9" runat="server" onclick="LinkButton9_Click">Control Auditorias</asp:LinkButton>
            </div>

            <div class="col-md-4" style="margin-top:30px">   
                <asp:ImageButton ID="ImageButton10" runat="server" ImageUrl="~/img/in-line.png" Width="128px" Height="128px" onclick="ImageButton10_Click" /><br />
                    <asp:LinkButton ID="LinkButton10" runat="server" onclick="LinkButton10_Click">Auditorias Fechh</asp:LinkButton>
            </div>

    </center>

    </div>

</asp:Content>

