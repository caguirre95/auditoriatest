﻿using DevExpress.Export;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;

public partial class admin_AuditoriaBultoABultoB : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SideBySideBarSeriesLabel label = (SideBySideBarSeriesLabel)WebChartControl1.SeriesTemplate.Label;

        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();

            visible();
            cargacombo();
            cargagridestilos();

            Session["rep"] = null;
            Session["rep1"] = null;
            Session["rep2"] = null;
            Session["rep3"] = null;


            chbShowLabels.Checked = WebChartControl1.SeriesTemplate.LabelsVisibility == DefaultBoolean.True;
            spnLabelIndent.Value = label.Indent;

        }

        bool indentEnabled = chbShowLabels.Checked;
        spnLabelIndent.ClientEnabled = indentEnabled;
        WebChartControl1.CrosshairOptions.CrosshairLabelMode = CrosshairLabelMode.ShowForNearestSeries;

    }



    void PerformShowLabelsAction()
    {
        WebChartControl1.SeriesTemplate.LabelsVisibility = chbShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False;
        WebChartControl1.CrosshairEnabled = chbShowLabels.Checked ? DefaultBoolean.False : DefaultBoolean.True;
    }

    void PerformLabelIndentAction()
    {
        ((BarSeriesLabel)WebChartControl1.SeriesTemplate.Label).Indent = Convert.ToInt32(spnLabelIndent.Value);
    }

    protected void WebChartControl1_CustomCallback(object sender, CustomCallbackEventArgs e)
    {
        if (e.Parameter == "ShowLabels")
            PerformShowLabelsAction();
        else if (e.Parameter == "LabelIndent")
            PerformLabelIndentAction();

        WebChartControl1.DataBind();
    }

    void visible()
    {
        //  ASPxGridView2.Visible = false;
        btnexport1.Visible = false;

        ASPxGridView1.Visible = true;
        btnexport.Visible = true;

    }

    void nvisible()
    {
        //   ASPxGridView2.Visible = true;
        btnexport1.Visible = true;

        ASPxGridView1.Visible = false;
        btnexport.Visible = false;
    }

    void cargacombo()
    {
        string sqline = "select id_linea,numero from Linea l where l.numero not like '%-%' and l.numero not like '%A%' order by CONVERT(float,numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT [id_planta],[descripcion] FROM[Auditoria].[dbo].[Planta]";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
        DropDownListPlant.Items.Insert(5, "All Plant");

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    if (txtPorder.Text != string.Empty)
        //    {
        //        string queryBundle = "Select distinct b.Size from POrder p join Bundle b on p.Id_Order = b.Id_Order where p.POrder = '" + txtPorder.Text + "'"; //Select b.Id_Bundle,b.Size,b.Quantity,b.NSeq from POrder p join Bundle b on p.Id_Order = b.Id_Order where p.Id_Order = " + hfidPorder.Value + " order by NSeq asc";
        //        DataTable dt_Bundle = DataAccess.Get_DataTable(queryBundle);

        //        DropDownList2.DataSource = dt_Bundle;
        //        DropDownList2.DataTextField = "Size";
        //        DropDownList2.DataValueField = "Size";
        //        DropDownList2.DataBind();
        //        DropDownList2.Items.Insert(0, "Select...");

        //    }
        //}
        //catch (Exception)
        //{
        //    throw;
        //}
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {

        visible();

        ASPxGridView1.DataBind();


    }

    void cargagridestilos()
    {
        string sqline =  " select s.Style"
                       + " from Style s"
                       + " join POrder p on s.Id_Style = p.Id_Style"
                       + " join Bundle b on p.Id_Order = b.Id_Order"
                       + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                       + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                       + " pm.rol='bultoabultoB' and datepart(wk, mb.fecha) = DATEPART(WK, GETDATE()) and DATEPART(YY,mb.fecha)=DATEPART(YY,GETDATE())"
                       + " group by s.Style";

        DataTable dtEstilos = DataAccess.Get_DataTable(sqline);

        grvestilos.DataSource = dtEstilos;
        grvestilos.DataBind();
    }

    DataTable funcionCarga()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);

        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string resp = RadioButtonList1.SelectedValue;

        string query = "";

        switch (resp)
        {
            case "1"://cut
                if (txtPorder.Text != string.Empty)
                {

                    query = " select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,m.valor"
                    + " from Bundle b"
                    + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                    + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                    + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                    + " join tblMedidas m on mb.idMedida = m.idMedida"
                    + " join POrder p on b.Id_Order = p.Id_Order"
                    + " where p.POrder='" + txtPorder.Text + "'  and pm.rol='bultoabultoB'"
                    + " group by m.Medida,pmd.NombreMedida,m.valor"
                    + " order by m.valor desc";


                }
                else
                {
                    resp = "fin";
                }
                break;
            case "2"://style
                if (txtstyle.Text != string.Empty)
                {

                    query = "select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,m.valor"
                     + " from Bundle b"
                     + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                     + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                     + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                     + " join tblMedidas m on mb.idMedida = m.idMedida"
                     + " join tblUserMedida us on mb.insp=us.userms"
                     + " join POrder p on b.Id_Order = p.Id_Order"
                     + " join Style s on p.Id_Style = s.Id_Style"
                     + " where s.Style = '" + txtstyle.Text + "' "
                     + " and pm.rol='bultoabultoB' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                     + " group by m.Medida,pmd.NombreMedida,m.valor"
                     + " order by m.valor desc";
                    // }

                }
                else
                {
                    resp = "fin";
                }
                break;
            case "3"://line
                if (DropDownListLine.SelectedItem.Text != "Select...")
                {

                    query = " select m.Medida,COUNT(m.Medida) as total ,pmd.NombreMedida,m.valor"
                       + " from Bundle b"
                       + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                       + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                       + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                       + " join tblMedidas m on mb.idMedida = m.idMedida"
                       + " join tblUserMedida us on mb.insp=us.userms"
                       + " join POrder p on b.Id_Order=p.Id_Order"
                       + " where p.Id_Linea2= " + DropDownListLine.SelectedValue
                       + " and pm.rol='bultoabultoB' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                       + " group by m.Medida,pmd.NombreMedida,m.valor"
                       + " order by m.valor desc";
                    //  }

                }
                else
                {
                    resp = "fin";
                }
                break;
            case "4"://plant
                if (DropDownListPlant.SelectedItem.Text == "Select...")
                {
                    resp = "fin";
                }
                else if (DropDownListPlant.SelectedItem.Text == "All Plant")
                {
                    query = " select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,m.valor"
                             + " from Bundle b"
                             + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                             + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                             + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                             + " join tblMedidas m on mb.idMedida = m.idMedida"
                             + " where  pm.rol='bultoabultoB' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                             + " group by m.Medida,pmd.NombreMedida,m.valor"
                             + " order by m.valor desc";
                }
                else
                {

                    query = " select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,m.valor"
                      + " from Bundle b"
                      + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                      + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                      + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                      + " join tblMedidas m on mb.idMedida = m.idMedida"
                      + " join tblUserMedida us on mb.insp=us.userms"
                      + " join Linea l on us.linea = l.id_linea"
                      + " where l.id_planta =" + DropDownListPlant.SelectedValue + " and pm.rol='bultoabultoB' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                      + " group by m.Medida,pmd.NombreMedida,m.valor"
                      + " order by m.valor desc";

                }
                break;
            case "":
                resp = "fin";
                break;
            default:
                resp = "fin";
                break;
        }

        DataTable dt = new DataTable();
        if (resp != "fin")
        {
            //carga comentario del corte
            if (txtPorder.Text != "")
            {
                DataTable dt2 = new DataTable();
                dt2 = DataAccess.Get_DataTable("select p.comments from POrder p where p.POrder='" + txtPorder.Text + "'");

                foreach (DataRow item in dt2.Rows)
                {
                    memo.Text = item[0].ToString();
                }

            }

            //Extrae la informacion de medidas del cut
            dt = new DataTable();
            dt = DataAccess.Get_DataTable(query);

            return dt;
        }
        else
        {
            //debe seleccionar una opcion
            dt = null;
            return dt;
        }

    }

    protected void ASPxGridView1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView1.DataBind();
    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        DataTable dt_seccion = new DataTable();

        dt_seccion.Columns.Add("PUNTOS_MEDIDA", typeof(string));

        dt_seccion.Columns.Add("WAIST", typeof(int));
        dt_seccion.Columns.Add("%WAIST", typeof(string));

        dt_seccion.Columns.Add("HI_HIP", typeof(int));
        dt_seccion.Columns.Add("%HI_HIP", typeof(string));

        dt_seccion.Columns.Add("LOW_HIP", typeof(int));
        dt_seccion.Columns.Add("%LOW_HIP", typeof(string));

        dt_seccion.Columns.Add("THIGH", typeof(int));
        dt_seccion.Columns.Add("%THIGH", typeof(string));

        dt_seccion.Columns.Add("KNEE", typeof(int));
        dt_seccion.Columns.Add("%KNEE", typeof(string));

        dt_seccion.Columns.Add("LEG_OPENING", typeof(int));
        dt_seccion.Columns.Add("%LEG_OPENING", typeof(string));

        dt_seccion.Columns.Add("FRONT_RISE", typeof(int));
        dt_seccion.Columns.Add("%FRONT_RISE", typeof(string));

        dt_seccion.Columns.Add("BACK_RISE", typeof(int));
        dt_seccion.Columns.Add("%BACK_RISE", typeof(string));

        dt_seccion.Columns.Add("INSEAM", typeof(int));
        dt_seccion.Columns.Add("%INSEAM", typeof(string));


        #region region
        DataTable dt = new DataTable();

        string queryM = "select medida from tblMedidas order by valor desc";

        dt = DataAccess.Get_DataTable(queryM);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            DataRow row2 = dt_seccion.NewRow();

            row2[0] = dt.Rows[i][0];
            dt_seccion.Rows.Add(row2);

        }

        dt = new DataTable();
        dt = funcionCarga();

        int n1 = 0, n2 = 0, n3 = 0, n4 = 0, n5 = 0, n6 = 0, n7 = 0,n8=0,n9=0;

        List<listatotal> list = new List<listatotal>();
        listatotal obj = new listatotal();
        listatotal obj1 = new listatotal();
        listatotal obj2 = new listatotal();

        List<listatotal> list3_4 = new List<listatotal>();
        listatotal obj3_4 = new listatotal();
        listatotal obj3_41 = new listatotal();
        listatotal obj3_42 = new listatotal();

        List<listatotal> list3_8 = new List<listatotal>();
        listatotal obj3_8 = new listatotal();
        listatotal obj3_81 = new listatotal();
        listatotal obj3_82 = new listatotal();

        List<listatotal> list1_2 = new List<listatotal>();
        listatotal obj1_2 = new listatotal();
        listatotal obj1_21 = new listatotal();
        listatotal obj1_22 = new listatotal();

        obj.r = "Over 1''";
        obj1.r = "In Speck";
        obj2.r = "Below -1'' ";

        obj3_4.r = "Over 3/4''";
        obj3_41.r = "In Speck";
        obj3_42.r = "Below -3/4''";

        obj3_8.r = "Over 3/8''";
        obj3_81.r = "In Speck";
        obj3_82.r = "Below -3/8''";

        obj1_2.r = "Over 1/2''";
        obj1_21.r = "In Speck";
        obj1_22.r = "Below -1/2''";
        #endregion


        if (dt != null)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][2].Equals("WAIST"))
                {
                    n1 = n1 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalw = obj.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalw = obj1.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalw = obj2.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalw = obj3_4.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalw = obj3_41.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalw = obj3_42.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("THIGH"))
                {
                    n8 = n8 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalT = obj.totalT + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalT = obj1.totalT + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalT = obj2.totalT + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalT = obj3_4.totalT + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalT = obj3_41.totalT + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalT = obj3_42.totalT + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("KNEE"))
                {
                    n9 = n9 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalK = obj.totalK + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalK = obj1.totalK + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalK = obj2.totalK + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalK = obj3_4.totalK + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalK = obj3_41.totalK + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalK = obj3_42.totalK + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("INSEAM"))
                {
                    n2 = n2 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalI = obj.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalI = obj1.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalI = obj2.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.5)
                    {
                        obj1_2.totalI = obj1_2.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.5 && float.Parse(dt.Rows[i][3].ToString()) >= -0.5)
                    {
                        obj1_21.totalI = obj1_21.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.5)
                    {
                        obj1_22.totalI = obj1_22.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("FRONT RISE"))
                {
                    n3 = n3 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalF = obj.totalF + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalF = obj1.totalF + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalF = obj2.totalF + Convert.ToInt32(dt.Rows[i][1]);

                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.375)
                    {
                        obj3_8.totalF = obj3_8.totalF + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.375 && float.Parse(dt.Rows[i][3].ToString()) >= -0.375)
                    {
                        obj3_81.totalF = obj3_81.totalF + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.375)
                    {
                        obj3_82.totalF = obj3_82.totalF + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("BACK RISE"))
                {
                    n4 = n4 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalB = obj.totalB + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalB = obj1.totalB + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalB = obj2.totalB + Convert.ToInt32(dt.Rows[i][1]);

                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.375)
                    {
                        obj3_8.totalB = obj3_8.totalB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.375 && float.Parse(dt.Rows[i][3].ToString()) >= -0.375)
                    {
                        obj3_81.totalB = obj3_81.totalB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.375)
                    {
                        obj3_82.totalB = obj3_82.totalB + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("HI HIP"))
                {
                    n5 = n5 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalH = obj.totalH + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalH = obj1.totalH + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalH = obj2.totalH + Convert.ToInt32(dt.Rows[i][1]);

                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalH = obj3_4.totalH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalH = obj3_41.totalH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalH = obj3_42.totalH + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("LEG OPENING"))
                {
                    n6 = n6 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalL = obj.totalL + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalL = obj1.totalL + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalL = obj2.totalL + Convert.ToInt32(dt.Rows[i][1]);

                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.375)
                    {
                        obj3_8.totalL = obj3_8.totalL + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.375 && float.Parse(dt.Rows[i][3].ToString()) >= -0.375)
                    {
                        obj3_81.totalL = obj3_81.totalL + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.375)
                    {
                        obj3_82.totalL = obj3_82.totalL + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("LOW HIP"))
                {
                    n7 = n7 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalH2 = obj.totalH2 + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalH2 = obj1.totalH2 + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalH2 = obj2.totalH2 + Convert.ToInt32(dt.Rows[i][1]);

                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalH2 = obj3_4.totalH2 + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalH2 = obj3_41.totalH2 + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalH2 = obj3_42.totalH2 + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }

            }

            obj.procenW = ((Convert.ToSingle(obj.totalw) / n1) * 100).ToString("0.00") + "% ";
            obj1.procenW = ((Convert.ToSingle(obj1.totalw) / n1) * 100).ToString("0.00") + "% ";
            obj2.procenW = ((Convert.ToSingle(obj2.totalw) / n1) * 100).ToString("0.00") + "% ";

            obj.procenI = ((Convert.ToSingle(obj.totalI) / n2) * 100).ToString("0.00") + "% ";
            obj1.procenI = ((Convert.ToSingle(obj1.totalI) / n2) * 100).ToString("0.00") + "% ";
            obj2.procenI = ((Convert.ToSingle(obj2.totalI) / n2) * 100).ToString("0.00") + "% ";

            obj.procenF = ((Convert.ToSingle(obj.totalF) / n3) * 100).ToString("0.00") + "% ";
            obj1.procenF = ((Convert.ToSingle(obj1.totalF) / n3) * 100).ToString("0.00") + "% ";
            obj2.procenF = ((Convert.ToSingle(obj2.totalF) / n3) * 100).ToString("0.00") + "% ";

            obj.procenB = ((Convert.ToSingle(obj.totalB) / n4) * 100).ToString("0.00") + "%";
            obj1.procenB = ((Convert.ToSingle(obj1.totalB) / n4) * 100).ToString("0.00") + "% ";
            obj2.procenB = ((Convert.ToSingle(obj2.totalB) / n4) * 100).ToString("0.00") + "% ";

            obj.procenH = ((Convert.ToSingle(obj.totalH) / n5) * 100).ToString("0.00") + "% ";
            obj1.procenH = ((Convert.ToSingle(obj1.totalH) / n5) * 100).ToString("0.00") + "% ";
            obj2.procenH = ((Convert.ToSingle(obj2.totalH) / n5) * 100).ToString("0.00") + "% ";

            obj.procenL = ((Convert.ToSingle(obj.totalL) / n6) * 100).ToString("0.00") + "% ";
            obj1.procenL = ((Convert.ToSingle(obj1.totalL) / n6) * 100).ToString("0.00") + "% ";
            obj2.procenL = ((Convert.ToSingle(obj2.totalL) / n6) * 100).ToString("0.00") + "% ";

            obj.procenH2 = ((Convert.ToSingle(obj.totalH2) / n7) * 100).ToString("0.00") + "% ";
            obj1.procenH2 = ((Convert.ToSingle(obj1.totalH2) / n7) * 100).ToString("0.00") + "% ";
            obj2.procenH2 = ((Convert.ToSingle(obj2.totalH2) / n7) * 100).ToString("0.00") + "% ";

            obj.procenT = ((Convert.ToSingle(obj.totalT) / n8) * 100).ToString("0.00") + "% ";
            obj1.procenT = ((Convert.ToSingle(obj1.totalT) / n8) * 100).ToString("0.00") + "% ";
            obj2.procenT = ((Convert.ToSingle(obj2.totalT) / n8) * 100).ToString("0.00") + "% ";

            obj.procenK = ((Convert.ToSingle(obj.totalK) / n9) * 100).ToString("0.00") + "% ";
            obj1.procenK = ((Convert.ToSingle(obj1.totalK) / n9) * 100).ToString("0.00") + "% ";
            obj2.procenK = ((Convert.ToSingle(obj2.totalK) / n9) * 100).ToString("0.00") + "% ";

            list.Add(obj);

            list.Add(obj1);

            list.Add(obj2);


            obj3_4.procenW = ((Convert.ToSingle(obj3_4.totalw) / n1) * 100).ToString("0.00") + "% ";
            obj3_41.procenW = ((Convert.ToSingle(obj3_41.totalw) / n1) * 100).ToString("0.00") + "% ";
            obj3_42.procenW = ((Convert.ToSingle(obj3_42.totalw) / n1) * 100).ToString("0.00") + "% ";

            obj1_2.procenI = ((Convert.ToSingle(obj1_2.totalI) / n2) * 100).ToString("0.00") + "% ";
            obj1_21.procenI = ((Convert.ToSingle(obj1_21.totalI) / n2) * 100).ToString("0.00") + "% ";
            obj1_22.procenI = ((Convert.ToSingle(obj1_22.totalI) / n2) * 100).ToString("0.00") + "% ";

            obj3_8.procenF = ((Convert.ToSingle(obj3_8.totalF) / n3) * 100).ToString("0.00") + "% ";
            obj3_81.procenF = ((Convert.ToSingle(obj3_81.totalF) / n3) * 100).ToString("0.00") + "% ";
            obj3_82.procenF = ((Convert.ToSingle(obj3_82.totalF) / n3) * 100).ToString("0.00") + "% ";

            obj3_8.procenB = ((Convert.ToSingle(obj3_8.totalB) / n4) * 100).ToString("0.00") + "% ";
            obj3_81.procenB = ((Convert.ToSingle(obj3_81.totalB) / n4) * 100).ToString("0.00") + "% ";
            obj3_82.procenB = ((Convert.ToSingle(obj3_82.totalB) / n4) * 100).ToString("0.00") + "% ";

            obj3_8.procenL = ((Convert.ToSingle(obj3_8.totalL) / n6) * 100).ToString("0.00") + "% ";
            obj3_81.procenL = ((Convert.ToSingle(obj3_81.totalL) / n6) * 100).ToString("0.00") + "% ";
            obj3_82.procenL = ((Convert.ToSingle(obj3_82.totalL) / n6) * 100).ToString("0.00") + "% ";

            obj3_4.procenH = ((Convert.ToSingle(obj3_4.totalH) / n5) * 100).ToString("0.00") + "% ";
            obj3_41.procenH = ((Convert.ToSingle(obj3_41.totalH) / n5) * 100).ToString("0.00") + "% ";
            obj3_42.procenH = ((Convert.ToSingle(obj3_42.totalH) / n5) * 100).ToString("0.00") + "% ";

            obj3_4.procenH2 = ((Convert.ToSingle(obj3_4.totalH2) / n7) * 100).ToString("0.00") + "% ";
            obj3_41.procenH2 = ((Convert.ToSingle(obj3_41.totalH2) / n7) * 100).ToString("0.00") + "% ";
            obj3_42.procenH2 = ((Convert.ToSingle(obj3_42.totalH2) / n7) * 100).ToString("0.00") + "% ";

            obj3_4.procenT = ((Convert.ToSingle(obj3_4.totalT) / n8) * 100).ToString("0.00") + "% ";
            obj3_41.procenT = ((Convert.ToSingle(obj3_41.totalT) / n8) * 100).ToString("0.00") + "% ";
            obj3_42.procenT = ((Convert.ToSingle(obj3_42.totalT) / n8) * 100).ToString("0.00") + "% ";

            obj3_4.procenK = ((Convert.ToSingle(obj3_4.totalK) / n9) * 100).ToString("0.00") + "% ";
            obj3_41.procenK = ((Convert.ToSingle(obj3_41.totalK) / n9) * 100).ToString("0.00") + "% ";
            obj3_42.procenK = ((Convert.ToSingle(obj3_42.totalK) / n9) * 100).ToString("0.00") + "% ";

            list3_4.Add(obj3_4);
            list3_4.Add(obj3_41);
            list3_4.Add(obj3_42);

            list1_2.Add(obj1_2);
            list1_2.Add(obj1_21);
            list1_2.Add(obj1_22);

            list3_8.Add(obj3_8);
            list3_8.Add(obj3_81);
            list3_8.Add(obj3_82);


            //carga grid Pincipal con Porcentajes
            for (int i = 0; i < dt_seccion.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (dt.Rows[j][2].Equals("WAIST"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][1] = dt.Rows[j][1];
                            dt_seccion.Rows[i][2] = ((float.Parse(dt.Rows[j][1].ToString()) / n1) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("HI HIP"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][3] = dt.Rows[j][1];
                            dt_seccion.Rows[i][4] = ((float.Parse(dt.Rows[j][1].ToString()) / n5) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("LOW HIP"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][5] = dt.Rows[j][1];
                            dt_seccion.Rows[i][6] = ((float.Parse(dt.Rows[j][1].ToString()) / n7) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("THIGH"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][7] = dt.Rows[j][1];
                            dt_seccion.Rows[i][8] = ((float.Parse(dt.Rows[j][1].ToString()) / n8) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("KNEE"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][9] = dt.Rows[j][1];
                            dt_seccion.Rows[i][10] = ((float.Parse(dt.Rows[j][1].ToString()) / n9) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("LEG OPENING"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][11] = dt.Rows[j][1];
                            dt_seccion.Rows[i][12] = ((float.Parse(dt.Rows[j][1].ToString()) / n6) * 100).ToString("0.00") + "%";
                        }
                    }
                  
                    else if (dt.Rows[j][2].Equals("FRONT RISE"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][13] = dt.Rows[j][1];
                            dt_seccion.Rows[i][14] = ((float.Parse(dt.Rows[j][1].ToString()) / n3) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("BACK RISE"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][15] = dt.Rows[j][1];
                            dt_seccion.Rows[i][16] = ((float.Parse(dt.Rows[j][1].ToString()) / n4) * 100).ToString("0.00") + "%";
                        }
                    }

                    else if (dt.Rows[j][2].Equals("INSEAM"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][17] = dt.Rows[j][1];
                            dt_seccion.Rows[i][18] = ((float.Parse(dt.Rows[j][1].ToString()) / n2) * 100).ToString("0.00") + "%";
                        }
                    }

                }
            }


        }

        WebChartControl1.DataBind();

        ASPxGridView1.DataSource = dt_seccion;

        Session["rep"] = list;
        Session["rep1"] = list1_2;
        Session["rep2"] = list3_4;
        Session["rep3"] = list3_8;

        ASPxGridView3.DataBind();
        ASPxGridView4.DataBind();
        ASPxGridView2.DataBind();
        ASPxGridView5.DataBind();

    }

    protected void btnexport_Click(object sender, EventArgs e)
    {

        try
        {
            ASPxGridView1.DataBind();

            ASPxGridViewExporter1.GridViewID = "ASPxGridView1";
            ASPxGridViewExporter2.GridViewID = "ASPxGridView3";
            ASPxGridViewExporter3.GridViewID = "ASPxGridView4";
            ASPxGridViewExporter4.GridViewID = "ASPxGridView2";
            ASPxGridViewExporter5.GridViewID = "ASPxGridView5";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;
            link1.CreateMarginalHeaderArea += new CreateAreaEventHandler(Link_CreateMarginalHeaderArea);

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link2.Component = ASPxGridViewExporter2;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = ASPxGridViewExporter3;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link4 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link4.Component = ASPxGridViewExporter4;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link5 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link5.Component = ASPxGridViewExporter5;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1, link2, link3, link4, link5 });

            compositeLink.CreatePageForEachLink();
            //compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Report_BeforeWash";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=ReportMeasurement.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            throw;
        }

    }

    private void Link_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
    {
        string resp = RadioButtonList1.SelectedValue;
        string mess = "";
        if (resp == "1")
        {
            mess = txtPorder.Text + "\n" + memo.Text;
        }
        else
        {
            mess = memo.Text;
        }

        DevExpress.XtraPrinting.TextBrick brick;
        brick = e.Graph.DrawString(mess, Color.Navy, new RectangleF(0, 0, 700, 50), DevExpress.XtraPrinting.BorderSide.None);
        brick.Font = new Font("Times New Roman", 12);
        brick.StringFormat = new DevExpress.XtraPrinting.BrickStringFormat(StringAlignment.Center);

        // string reps="31313131311sdsfsdf";
        //PageInfoBrick brick = e.Graph.DrawPageInfo(PageInfo., "", Color.DarkBlue,new RectangleF(0, 0, 100, 20), BorderSide.None);

        //brick.LineAlignment = BrickAlignment.Center;

        //brick.Alignment = BrickAlignment.Center;

        //brick.AutoWidth = true;

    }

    protected void btnexport1_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridViewExporter1.GridViewID = "ASPxGridView2";
            ASPxGridViewExporter1.FileName = "Report " + DateTime.Now.ToShortDateString();

            ASPxGridViewExporter1.WriteXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });

        }
        catch (Exception)
        {

            throw;
        }
    }

    public class listatotal
    {
        public string r { get; set; }
        public int totalw { get; set; }
        public string procenW { get; set; }
        public int totalT { get; set; }
        public string procenT { get; set; }
        public int totalK { get; set; }
        public string procenK { get; set; }
        public int totalL { get; set; }
        public string procenL { get; set; }
        public int totalI { get; set; }
        public string procenI { get; set; }
        public int totalF { get; set; }
        public string procenF { get; set; }
        public int totalB { get; set; }
        public string procenB { get; set; }
       // public int totalM { get; set; }
       // public string procenM { get; set; }
        public int totalH { get; set; }
        public string procenH { get; set; }
        public int totalH2 { get; set; }
        public string procenH2 { get; set; }

    }

    protected void ASPxGridView3_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView3.DataSource = (List<listatotal>)Session["rep"];
    }

    protected void ASPxGridView3_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView3.DataBind();
    }

    protected void ASPxGridView4_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView4.DataSource = (List<listatotal>)Session["rep2"];
    }

    protected void ASPxGridView4_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView4.DataBind();
    }

    protected void ASPxGridView2_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView2.DataSource = (List<listatotal>)Session["rep3"];
    }

    protected void ASPxGridView2_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView2.DataBind();
    }

    protected void ASPxGridView5_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView5.DataSource = (List<listatotal>)Session["rep1"];
    }

    protected void ASPxGridView5_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView5.DataBind();
    }

    protected void WebChartControl1_DataBinding(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();

        dt = funcionCarga2();

        WebChartControl1.DataSource = dt;

    }

    DataTable funcionCarga2()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);

        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string resp = RadioButtonList1.SelectedValue;

        string query = "";

        switch (resp)
        {
            case "1"://cut
                if (txtPorder.Text != string.Empty)
                {

                    query = " select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,Round(m.valor, 1,1) as valor"
                    + " from Bundle b"
                    + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                    + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                    + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                    + " join tblMedidas m on mb.idMedida = m.idMedida"
                    + " join POrder p on b.Id_Order = p.Id_Order"
                    + " where p.POrder='" + txtPorder.Text + "'  and pm.rol='bultoabultoB'"
                    + " group by m.Medida,pmd.NombreMedida,m.valor"
                    + " order by m.valor desc";


                }
                else
                {
                    resp = "fin";
                }
                break;
            case "2"://style
                if (txtstyle.Text != string.Empty)
                {

                    query = "select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,Round(m.valor, 1,1) as valor"
                     + " from Bundle b"
                     + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                     + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                     + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                     + " join tblMedidas m on mb.idMedida = m.idMedida"
                     + " join tblUserMedida us on mb.insp=us.userms"
                     + " join POrder p on b.Id_Order = p.Id_Order"
                     + " join Style s on p.Id_Style = s.Id_Style"
                     + " where s.Style='" + txtstyle.Text + "'"
                     + " and pm.rol='bultoabultoB' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                     + " group by m.Medida,pmd.NombreMedida,m.valor"
                     + " order by m.valor desc";

                }
                else
                {
                    resp = "fin";
                }
                break;
            case "3"://line
                if (DropDownListLine.SelectedItem.Text != "Select...")
                {

                    query = " select m.Medida,COUNT(m.Medida) as total ,pmd.NombreMedida,Round(m.valor, 1,1) as valor"
                       + " from Bundle b"
                       + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                       + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                       + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                       + " join tblMedidas m on mb.idMedida = m.idMedida"
                       + " join tblUserMedida us on mb.insp=us.userms"
                       + " join POrder p on b.Id_Order=p.Id_Order"
                       + " where p.Id_Linea2= " + DropDownListLine.SelectedValue
                       + " and pm.rol='bultoabultoB' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                       + " group by m.Medida,pmd.NombreMedida,m.valor"
                       + " order by m.valor desc";


                }
                else
                {
                    resp = "fin";
                }
                break;
            case "4"://plant
                if (DropDownListPlant.SelectedItem.Text == "Select...")
                {
                    resp = "fin";
                }
                else if (DropDownListPlant.SelectedItem.Text == "All Plant")
                {
                    query = " select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,Round(m.valor, 1,1) as valor"
                             + " from Bundle b"
                             + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                             + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                             + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                             + " join tblMedidas m on mb.idMedida = m.idMedida"
                             + " where  pm.rol='bultoabultoB' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                             + " group by m.Medida,pmd.NombreMedida,m.valor"
                             + " order by m.valor desc";
                }
                else
                {

                    query = " select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,Round(m.valor, 1,1) as valor"
                      + " from Bundle b"
                      + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                      + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                      + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                      + " join tblMedidas m on mb.idMedida = m.idMedida"
                      + " join tblUserMedida us on mb.insp=us.userms"
                      + " join Linea l on us.linea = l.id_linea"
                      + " where l.id_planta =" + DropDownListPlant.SelectedValue + " and pm.rol='bultoabultoB' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                      + " group by m.Medida,pmd.NombreMedida,m.valor"
                      + " order by m.valor desc";

                }
                break;
            case "":
                resp = "fin";
                break;
            default:
                resp = "fin";
                break;
        }

        DataTable dt = new DataTable();
        if (resp != "fin")
        {
            if (txtPorder.Text != "")
            {
                DataTable dt2 = new DataTable();
                dt2 = DataAccess.Get_DataTable("select p.comments from POrder p where p.POrder='" + txtPorder.Text + "'");

                foreach (DataRow item in dt2.Rows)
                {
                    memo.Text = item[0].ToString();
                }

            }

            dt = new DataTable();
            dt = DataAccess.Get_DataTable(query);

            return dt;
        }
        else
        {
            //debe seleccionar una opcion
            dt = null;
            return dt;
        }

    }
}