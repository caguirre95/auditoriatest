﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SistemaAuditores.DataAccess;
using System.Data;
using DevExpress.Utils;
using DevExpress.XtraCharts;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;
using DevExpress.XtraPrinting;
//using iTextSharp.text;
//using iTextSharp.text.html.simpleparser;
//using iTextSharp.text.pdf;

public partial class MeasureReport : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtfecha1.Date = DateTime.Now.Date;
            txtfecha2.Date = DateTime.Now.Date;
            llenarcombo();
          //  Load_Data();           
        }
        Load_Grid();
    }

    void llenarcombo()
    {

        DataTable dt_linea = DataAccess.Get_DataTable("select id_linea, numero from Linea where idcliente = 11 and numero not like '%-%' and numero not like '%A%' order by numero asc");

        DropDownList1.DataSource = dt_linea;
        DropDownList1.DataValueField = "id_linea";
        DropDownList1.DataTextField = "numero";
        DropDownList1.DataBind();

        DropDownList1.SelectedIndex = 1;
    }
    public void Load_Grid()
    { 
        string sqlfecha_actual1 = txtfecha1.Date.ToString("yyyy-MM-dd");
        string sqlfecha_actual2 = txtfecha2.Date.ToString("yyyy-MM-dd");
        if (DropDownList1.SelectedValue == "0")
        { 
            DateTime fecha_actual = DateTime.Now;
            sqlfecha_actual1 = fecha_actual.Date.ToString("yyyy-MM-dd");
            sqlfecha_actual2 = fecha_actual.Date.ToString("yyyy-MM-dd");
        }        
        DataTable dt_2 = new DataTable();
		//DataTable dt_linea = DataAccess.Get_DataTable("select id_linea from Linea where numero='" + Convert.ToInt16(DropDownList1.SelectedValue) + "'");
		int id_Linea = Convert.ToInt16(DropDownList1.SelectedValue);
		string query_update = " select ROW_NUMBER() OVER (order by sum(br.cantidad_defectos) desc) as No, PO.POrder as POrder, S.Style as Style, B.Size as Size, Oper.descripcion as Operacion, AD.Nombre as Area, D.nombre as Defecto, sum(br.cantidad_defectos) as Cantidad from BundleRejectedBulto br"
							+ " inner join Bundle B on br.Id_Bundle=B.Id_Bundle"
							+ " inner join POrder PO on br.Id_Order=PO.Id_Order"
							+ " inner join Style S on PO.Id_Style = S.Id_Style"
							+ " inner join Operario O on O.id_operario= br.Id_operario"
                            + " inner join Catalogo c on o.id_operario=c.Id_operario"
                            + " inner join Operacion Oper on Oper.id_operacion = c.id_operacion" //modificado 11-08-2017
                            // + " inner join Operacion Oper on O.id_operacion=Oper.id_operacion"
                            + " inner join DefectosxArea DA on DA.Id_DefxArea=BR.Id_DefxArea"
							+ " inner join AreaDefecto AD on DA.Id_Area = AD.Id_Area"
							+ " inner join Defectos D on DA.Id_defectos=D.id_defectos"
							+ " where br.id_operario in (select id_operario from operario)"
							+ " and br.Id_linea=" + id_Linea + " and br.FechaRecibido between '" + sqlfecha_actual1 + "' and '" + sqlfecha_actual2 + "' and (D.nombre='Medida Grande' or D.nombre='Medida Pequeña')"
							+ " group by PO.POrder, S.Style, B.Size, Oper.descripcion, AD.Nombre, D.nombre";

         dt_2 = DataAccess.Get_DataTable(query_update);
         if (dt_2.Rows.Count > 0)
         {
             txtmensaje.Visible = false;
             GV_Defects.Visible = true;
             GV_Defects.DataSourceID = "";
             SqlDataSource1.SelectCommand = query_update;
             GV_Defects.DataSource = dt_2;
             GV_Defects.DataBind();
			 btnexport.Visible = true;
         }
         else
         {
             GV_Defects.Visible = false;
             txtmensaje.Visible = true;
			 btnexport.Visible = false;
             txtmensaje.Text = "No Data to Display!";
         }
    }
    
    public void Load_Data()
    {
        //DateTime fecha_actual = ;
        string sqlfecha_actual1 = txtfecha1.Date.ToString("yyyy-MM-dd");
        string sqlfecha_actual2 = txtfecha2.Date.ToString("yyyy-MM-dd");
        if (DropDownList1.SelectedValue == "0")
        { 
            DateTime fecha_actual = DateTime.Now;
            sqlfecha_actual1 = fecha_actual.Date.ToString("yyyy-MM-dd");
            sqlfecha_actual2 = fecha_actual.Date.ToString("yyyy-MM-dd");
        }
        
        DataTable dt_1 = new DataTable();
		//DataTable dt_linea = DataAccess.Get_DataTable("select id_linea from Linea where numero='" + Convert.ToInt16(DropDownList1.SelectedValue) + "'");
		int id_Linea = Convert.ToInt16(DropDownList1.SelectedValue);
		string query_update = " select ROW_NUMBER() OVER (order by sum(br.cantidad_defectos) desc) as No, AD.Nombre as Area, D.nombre as Defecto, sum(br.cantidad_defectos) as Defectos from BundleRejectedBulto br"
							+ " inner join Operario O on O.id_operario= br.Id_operario"
							+ " inner join DefectosxArea DA on DA.Id_DefxArea=BR.Id_DefxArea"
							+ " inner join AreaDefecto AD on DA.Id_Area = AD.Id_Area"
							+ "	inner join Defectos D on DA.Id_defectos=D.id_defectos"
							+ "	where br.id_operario in (select id_operario from operario)"
							+ "	and br.Id_linea=" + id_Linea + " and br.FechaRecibido between '" + sqlfecha_actual1 + "' and '" + sqlfecha_actual2 + "' and (D.nombre='Medida Grande' or D.nombre='Medida Pequeña')"
							+ "	group by AD.Nombre, D.nombre";

        //char[] MyChar = {'\t'};
        //string NewString = query_update.TrimEnd(MyChar);

        // query_update = query_update.Trim(''

        query_update = query_update.Replace("\t", " ");
        //string arr[] =new string[ query_update.ToCharArray()];

        //foreach (var item in query_update.ToCharArray())
        //{
             
        //}

        dt_1 = DataAccess.Get_DataTable(query_update);   
		if(dt_1.Rows.Count>0)
		{			
		WebChartControl1.Visible = true;
		txtmensaje.Visible = false;
        WebChartControl1.DataSource = dt_1;
        WebChartControl1.DataBind();	
		}
		else
		{
		WebChartControl1.Visible = false;	
		txtmensaje.Visible = true;
        txtmensaje.Text = "No Data to Display!";
		}        
    }
	
	 protected void btnexport_Click(object sender, EventArgs e)
    {
		DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link1.Component = ExportToExcell;

        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

        string sqlfecha_actual1 = txtfecha1.Date.ToString("yyyy-MM-dd");
        string sqlfecha_actual2 = txtfecha2.Date.ToString("yyyy-MM-dd");
        if (DropDownList1.SelectedValue == "0")
        { 
            DateTime fecha_actual = DateTime.Now;
            sqlfecha_actual1 = fecha_actual.Date.ToString("yyyy-MM-dd");
            sqlfecha_actual2 = fecha_actual.Date.ToString("yyyy-MM-dd");
        }
        
        DataTable dt_1 = new DataTable();
		//DataTable dt_linea = DataAccess.Get_DataTable("select id_linea from Linea where numero='" + Convert.ToInt16(DropDownList1.SelectedValue) + "'");
		int id_Linea = Convert.ToInt16(DropDownList1.SelectedValue);
		string query_update = " select ROW_NUMBER() OVER (order by sum(br.cantidad_defectos) desc) as No, PO.POrder as POrder, S.Style as Style, B.Size as Size, Oper.descripcion as Operacion, AD.Nombre as Area, D.nombre as Defecto, sum(br.cantidad_defectos) as Defectos from BundleRejectedBulto br"
							+ " inner join Bundle B on br.Id_Bundle=B.Id_Bundle"
							+ " inner join POrder PO on br.Id_Order=PO.Id_Order"
							+ " inner join Style S on PO.Id_Style = S.Id_Style"
							+ " inner join Operario O on O.id_operario= br.Id_operario"
                            + " inner join Catalogo c on o.id_operario=c.Id_operario"
                            + " inner join Operacion Oper on Oper.id_operacion = c.id_operacion" //modificado 11-08-2017
                            // + " inner join Operacion Oper on O.id_operacion=Oper.id_operacion"
                            //+ " inner join Operacion Oper on O.id_operacion=Oper.id_operacion"
							+ " inner join DefectosxArea DA on DA.Id_DefxArea=BR.Id_DefxArea"
							+ " inner join AreaDefecto AD on DA.Id_Area = AD.Id_Area"
							+ " inner join Defectos D on DA.Id_defectos=D.id_defectos"
							+ " where br.id_operario in (select id_operario from operario)"
							+ " and br.Id_linea=" + id_Linea + " and br.FechaRecibido between '" + sqlfecha_actual1 + "' and '" + sqlfecha_actual2 + "' and (D.nombre='Medida Grande' or D.nombre='Medida Pequeña')"
							+ " group by PO.POrder, S.Style, B.Size, Oper.descripcion, AD.Nombre, D.nombre";
							
        dt_1 = DataAccess.Get_DataTable(query_update);   
        WebChartControl1.DataSource = dt_1;
        WebChartControl1.DataBind();
        link3.Component = ((DevExpress.XtraCharts.Native.IChartContainer)WebChartControl1).Chart;

        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link1, link3 });

        compositeLink.CreatePageForEachLink();

        using(MemoryStream stream = new MemoryStream()) {
            XlsxExportOptions options = new XlsxExportOptions();
            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
			options.SheetName = "Top5Defects_" + DropDownList1.SelectedItem.Text;
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=Top5Defects.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }
        ps.Dispose();

    }
	

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        //int id_linea = Convert.ToInt16(DropDownList1.SelectedValue);

        //if (id_linea == 9 || id_linea ==2)
        //{
        try
        {
            Load_Data();
            txtmensaje.Visible = false;
        }
        catch (Exception)
        {

            throw;
        }
           
        //}
        //else
        //{
        //    WebChartControl1.Visible = false;
        //    txtmensaje.Visible = true;
        //    txtmensaje.Text = "No Data to Display!";
        //}
    }
}
