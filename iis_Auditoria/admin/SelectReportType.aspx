﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SelectReportType.aspx.cs" Inherits="_SelectReport" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h3 class="quitPadH">
        <asp:Label ID="Label1" runat="server" Text="Checkoff Reports"></asp:Label>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <style type="text/css">
        .box_effect {
            height: 250px;
            width: 180px;
            padding-top: 15px;
            /*-webkit-transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
            transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);*/
            text-align: center;
            padding: 5px;
            border-width: 160px;
            /*-webkit-box-shadow: 7px 7px 7px rgba(0,0,0,0.2);
            -moz-box-shadow: 7px 7px 7px rgba(0,0,0,0.2);
            box-shadow: 7px 7px 7px rgba(0,0,0,0.2);
            -webkit-transition: all 0.5s ease-out;
            -moz-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;*/
            margin: auto;
            opacity: 0.8;
            border: 1px solid rgba(0,0,0,0.2);
            /*-webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            -webkit-box-reflect: below 0px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(.7, transparent), to(rgba(0,0,0,0.1)));*/
        }

        /*.box_effect:hover {
                -webkit-transform: scale(1.25, 1.25);
                transform: scale(1.25, 1.25);
                background-color: #6E6E6E;
                color: white !important;
                text-decoration: none;
                margin-top: 2px;
                -webkit-transform: rotate(-7deg);
                -moz-transform: rotate(-7deg);
                -o-transform: rotate(-7deg);
                z-index: 1000;
                opacity: 1;
                -webkit-box-reflect: below 0px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(.7, transparent), to(rgba(0,0,0,0.4)));
                -webkit-box-shadow: 0px 0px 20px rgba(255,255,255,0.8);
                -moz-box-shadow: 0px 0px 20px rgba(255,255,255,0.8);
                box-shadow: 0px 0px 20px rgba(255,255,255,0.8);
            }*/

        .te {
            text-decoration: none;
        }

        .im {
            z-index: -1;
        }

        .mt {
            margin-left: 10px;
            margin-top: 15px;
        }

        .te:hover {
            color: orangered !important;
            text-decoration: none;
            outline-style: none;
        }

        .mb-0 {
            margin-bottom: 5px;
        }
    </style>

    <div class="container">

        <div class="col-lg-4 mb-0">
            <div class="box_effect">
                <asp:LinkButton ID="lnk1section" runat="server" CssClass="te" OnClick="lnk1section_Click">
                    <div>                        
                        <img src="../img/by_section.png" alt="Inspection" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">IN PROCESS INSPECTION</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>

        <div class="col-lg-4 mb-0">
            <div class="box_effect">
                <asp:LinkButton ID="lnnkmedida" runat="server" CssClass="te" OnClick="lnnkmedida_Click">
                    <div>                        
                        <img class="im mt" src="../img/Measurements.png" alt="Bundle by Bundle" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">MEASUREMENT</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>

        <div class="col-lg-4 mb-0" style="padding-top: 10px">
            <div class="box_effect">
                <asp:LinkButton ID="packmedi" runat="server" CssClass="te" OnClick="packmedi_Click" >
                    <div>                        
                        <img class="im mt" src="../img/medidasemp.png" alt="packing" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">MEASUREMENT PACKING</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>

        <div class="col-lg-4 col-lg-offset-4" style="margin-bottom:5px">
            <div class="box_effect">
                <asp:LinkButton ID="linkLavanderia" runat="server" CssClass="te" OnClick="linkLavanderia_Click">
                    <div>                        
                        <img src="../img/Wash.jpg"  height="128" width="128" alt="Shipping Audit" class="mt"  />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;" class="text-uppercase">shipping control</span>
                    </div>
                </asp:LinkButton>
            </div>
        </div>


    </div>
</asp:Content>

