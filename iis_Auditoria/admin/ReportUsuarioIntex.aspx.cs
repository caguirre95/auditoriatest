﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReportUsuarioIntex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (User.Identity.IsAuthenticated)
        {
            var usuario = Context.User.Identity.Name.ToLower();

            if (!IsPostBack)
            {
                if (usuario.Equals("administrator"))
                {
                    FormsAuthentication.SignOut();
                    Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
                }

                txtfecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
                txtfecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
                //
                Session["tot"] = null;
            }

        }
        else
        {
            FormsAuthentication.SignOut();
            Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
        }

      
    }

    void cargaChart()
    {
        string query = " select u.userName,COUNT(cantidad) as unidadesAuditadas"
                   + " from tbmedidasintexdl dl join tblUserMedida u on dl.usuario = u.userms"
                   + " where convert(date, fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                   + " group by u.userName";


        DataTable dt = new DataTable();
        dt = DataAccess.Get_DataTable(query);

        packet_chart.DataSource = dt;
        packet_chart.DataBind();


        query = " select u.userName,COUNT(cantidad) as unidadesAuditadas,case dl.estado when 1 then 'Aceptado' else 'Rechazo' end as estado"
                      + " from tbmedidasintexdl dl join tblUserMedida u on dl.usuario = u.userms"
                      + " where convert(date, fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                      + " group by u.userName,dl.estado"
                      + " order by u.userName";

        dt = new DataTable();
        dt = DataAccess.Get_DataTable(query);

        WebChartControl1.DataSource = dt;
        WebChartControl1.DataBind();
    }

    protected void btnGenerar_Click(object sender, EventArgs e)
    {

        cargaChart();

       
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        cargaChart();

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
  

        link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)packet_chart).Chart;
        link1.Component = ((DevExpress.XtraCharts.Native.IChartContainer)WebChartControl1).Chart;

        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link2,link1
        });

        compositeLink.CreateDocument();

        using (MemoryStream stream = new MemoryStream())
        {
            XlsxExportOptions options = new XlsxExportOptions();
            options.ExportMode = XlsxExportMode.SingleFile;
            options.SheetName = "ReportUserIntex";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=ReportUserIntex.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }
        ps.Dispose();
    }  

    public class classtotal
    {
        public string total { get; set; }
        public int waist { get; set; }
        public int inseam { get; set; }
        public int frontrise { get; set; }
        public int backrise { get; set; }
        public int hip { get; set; }
    }
}