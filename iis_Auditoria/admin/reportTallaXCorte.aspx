﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="reportTallaXCorte.aspx.cs" Inherits="reportTallaXCorte" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraReports.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
       <link href="../StyleSheetAuto.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="container">
        <div class="col-lg-10 col-lg-6 col-lg-4 col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Filter Select</h3>
                </div>
                <div class="panel-body">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <asp:LinkButton CssClass="btn btn-default" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                        </span>
                        <div class="ui-widget" style="text-align: left;">
                            <asp:TextBox ID="txtcorte" placeholder="Cut" class=" form-control" Font-Size="14px" runat="server"></asp:TextBox>
                            <ajaxToolkit:AutoCompleteExtender ID="txtcorte_AutoCompleteExtender" runat="server" BehaviorID="txtcorte_AutoCompleteExtender"
                                ServicePath="reportTallaXCorte.aspx" ServiceMethod="GetCut" TargetControlID="txtcorte"
                                CompletionInterval="10" Enabled="True"
                                MinimumPrefixLength="1"
                                CompletionListCssClass="CompletionList"
                                CompletionListHighlightedItemCssClass="CompletionListHighlightedItem"
                                CompletionListItemCssClass="CompletionListItem"
                                DelimiterCharacters="">
                            </ajaxToolkit:AutoCompleteExtender>
                        </div>
                    </div>
                    <br />
                    <asp:DropDownList ID="DropDownListProcess" CssClass="form-control" OnSelectedIndexChanged="DropDownListProcess_SelectedIndexChanged" AutoPostBack="true"  runat="server">
                        <asp:ListItem Value="0"> Select...</asp:ListItem>
                        <asp:ListItem Value="1"> Production</asp:ListItem>
                        <asp:ListItem Value="2"> After Wash</asp:ListItem>
                        <asp:ListItem Value="3"> Audit Final</asp:ListItem>
                    </asp:DropDownList>
                    <br />  
                    <asp:Panel ID="Panel1" Visible="false" runat="server">
                    Seccion 
                        <asp:RadioButtonList ID="RadioButtonList1"  runat="server">
                            <asp:ListItem Value="bultoabulto">Bulto a bulto</asp:ListItem>
                            <asp:ListItem Value="inspInter">Inspeccion Final</asp:ListItem>
                        </asp:RadioButtonList>
                    </asp:Panel>
                    <br />
                    <hr />
                    <asp:Button ID="btnGenerar" OnClick="btnGenerar_Click" runat="server" Text="Generate Report" CssClass="btn btn-primary form-control" />
                </div>
            </div>
        </div>
       
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Report</h3>
                </div>
                <div class="panel-body">
                    <dx:ASPxWebDocumentViewer ID="ASPxWebDocumentViewer1" Visible="true" runat="server">
                    </dx:ASPxWebDocumentViewer>                   

                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

