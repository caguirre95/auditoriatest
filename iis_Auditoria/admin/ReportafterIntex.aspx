﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportafterIntex.aspx.cs" Inherits="admin_ReportafterIntex" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    // $('#<=hfidPorder.ClientID%>').val(ui.item.idorder);
                    // $('#<=txtstyle.ClientID%>').val(ui.item.style);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>
    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtstyle.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "Reportafterwash.aspx/GetStyle",
                        data: "{'pref' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    estilo: item.estilo,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);
                    // $('#<=hfidPorder.ClientID%>').val(ui.item.idorder);
                    // $('#<=txtstyle.ClientID%>').val(ui.item.style);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
            };
        });
    </script>

     <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })

            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })

        });

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <br />
     <div class="container">

          <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

        <div class="panel panel-primary" style="margin-top:5px">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Measurement Report </strong></h3>
            </div>
            <div class="panel-body">
                <div class="col-lg-12">
                   <%-- <div class="col-lg-8">--%>
                        <div class="col-lg-4">
                            Cut:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" OnClick="LinkButton1_Click" CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                          <%--  Bundles:
                                     <asp:DropDownList ID="DropDownList2" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" runat="server"></asp:DropDownList>--%>
                            Line:
                                     <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                            Plant:
                                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>

                            Style:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" OnClick="LinkButton1_Click" CssClass="btn btn-default" ID="LinkButton2" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtstyle" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                        </div>
                        <div class="col-lg-4">
                        
                            <div class="panel panel-default">
                                <div class="panel-heading">Select search filter</div>
                                <div class="panel-body">
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                        <asp:ListItem Value="1">Cut</asp:ListItem>
                                        <asp:ListItem Value="2">Style</asp:ListItem>
                                        <asp:ListItem Value="3">Line</asp:ListItem>
                                        <asp:ListItem Value="4">Plant</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>

                        </div>
                   <%-- </div>--%>

                        <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                               Start Date
                            <div class="input-group">

                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>

                            </div>
                                End Date
                            <div class="input-group">

                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>

                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12" style="padding:0px !important">
                        <div class="col-lg-4" style="padding-top: 10px; padding-bottom: 10px">
                            <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" OnClick="btngenerar_Click" runat="server" />
                        </div>
                         <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="padding-top: 10px; padding-bottom: 10px">
                             <a href="#ventanaG" data-toggle="modal" style="width: 161px" class="btn btn-md btn-success"><span class="glyphicon glyphicon-align-justify"></span> Ver Estilos</a>
                    </div>
                        <div class="col-lg-12" style="overflow-x:auto">
                            <dx:ASPxFormLayout runat="server" ID="OptionsFormLayout">
                                <Items>
                                    <dx:LayoutGroup Caption="">
                                        <Items>
                                            <dx:LayoutItem Caption="Show Labels">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxCheckBox ID="chbShowLabels" runat="server" ClientInstanceName="chbShowLabels">
                                                            <ClientSideEvents CheckedChanged="function(s, e) {
                                                                {
                                                                   spinLabelIndent.SetEnabled(chbShowLabels.GetChecked());
                                                                }
                                                                   chart.PerformCallback(&quot;ShowLabels&quot;);
                                                             }" />
                                                        </dx:ASPxCheckBox>
                                                        <div style="display: none">


                                                            <dx:ASPxSpinEdit ID="spnLabelIndent" runat="server" Number="0" ClientInstanceName="spinLabelIndent" MaxValue="10000" NumberType="Integer">
                                                                <ClientSideEvents NumberChanged="function(s, e) { chart.PerformCallback(&quot;LabelIndent&quot;); }" />
                                                            </dx:ASPxSpinEdit>
                                                        </div>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>

                                        </Items>
                                    </dx:LayoutGroup>
                                </Items>
                            </dx:ASPxFormLayout>

                             <dxchartsui:WebChartControl ID="WebChartControl1" OnDataBinding="WebChartControl1_DataBinding" runat="server"
                                Width="1000px" Height="394px" ClientInstanceName="chart" AppearanceNameSerializable="Pastel Kit"
                                OnCustomCallback="WebChartControl1_CustomCallback" CrosshairEnabled="true"
                                EnableClientSideAPI="True" ToolTipEnabled="False"
                                SeriesDataMember="NombreMedida">
                                <BorderOptions Visibility="False" />
                                <BorderOptions Visibility="False"></BorderOptions>
                                <SeriesTemplate ArgumentDataMember="valor" ValueDataMembersSerializable="total" LabelsVisibility="True"
                                    CrosshairLabelPattern="{V:#.##}">
                                    <LabelSerializable>
                                        <cc1:SideBySideBarSeriesLabel TextPattern="{V:#.##}">
                                        </cc1:SideBySideBarSeriesLabel>
                                    </LabelSerializable>
                                </SeriesTemplate>
                                <Titles>
                                    <cc1:ChartTitle Text="Report Measurement "></cc1:ChartTitle>
                                </Titles>
                                <Legend AlignmentHorizontal="Right" />
                                <DiagramSerializable>
                                    <cc1:XYDiagram>
                                        <AxisX VisibleInPanesSerializable="-1">
                                            <GridLines Visible="True">
                                            </GridLines>
                                        </AxisX>
                                        <AxisY VisibleInPanesSerializable="-1">
                                            <%-- <NumericScaleOptions AutoGrid="False" GridSpacing="1" />--%>
                                        </AxisY>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                            </dxchartsui:WebChartControl>

                          <%--  <dxchartsui:WebChartControl ID="packet_chart" Width="1000px" Height="394px"
                                runat="server" AppearanceNameSerializable="Pastel Kit" CrosshairEnabled="True"
                                SeriesDataMember="NombreMedida">
                                <BorderOptions Visibility="False" />
                                <BorderOptions Visibility="False"></BorderOptions>
                                <DiagramSerializable>
                                    <cc1:XYDiagram
                                        RangeControlDateTimeGridOptions-GridMode="ChartGrid">
                                        <AxisX VisibleInPanesSerializable="-1">
                                            <GridLines Visible="True">
                                            </GridLines>
                                        </AxisX>
                                        <AxisY VisibleInPanesSerializable="-1">
                                        </AxisY>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <SeriesTemplate ArgumentDataMember="Medida" LabelsVisibility="True" CrosshairLabelPattern="{S}:{V}"
                                    ValueDataMembersSerializable="total">
                                    <LabelSerializable>
                                        <cc1:SideBySideBarSeriesLabel Position="Top" TextPattern="Qty:{V}"
                                            Indent="5" LineLength="20" LineVisibility="True"
                                            ResolveOverlappingMode="Default">
                                        </cc1:SideBySideBarSeriesLabel>
                                    </LabelSerializable>
                                </SeriesTemplate>
                                <Titles>
                                    <cc1:ChartTitle Text="Report Measurement "></cc1:ChartTitle>
                                </Titles>
                            </dxchartsui:WebChartControl>--%>
                                                    
                        </div>
                        <div class="col-lg-4" style="padding-top: 20px; padding-bottom: 10px">
                                <asp:Button ID="btnexport" CssClass="btn btn-success form-control" OnClick="btnexport_Click" runat="server" Text="Export Excel" />
                                <asp:Button ID="btnexport1" CssClass="btn btn-success form-control" OnClick="btnexport1_Click" runat="server" Text="Export Excel" />
                            </div>
                      <div class="col-lg-12" style="overflow-x:auto">
                            <dx:ASPxMemo ID="memo" ClientInstanceName="memo" CssClass="form-control" runat="server" Height="130px" Width="400px" Caption="Note" Text="">
                                <CaptionSettings Position="Top" RequiredMarkDisplayMode="Optional" OptionalMark=" (optional)" />
                            </dx:ASPxMemo>
                            <br />
                            <dx:ASPxGridView ID="ASPxGridView1" OnDataBinding="ASPxGridView1_DataBinding" OnCustomCallback="ASPxGridView1_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                                <Columns>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="PUNTOS_MEDIDA" Caption="Measured Points">
                                        <FooterTemplate>
                                            <dx:ASPxLabel ID="ASPxLabeltotalydsrestantes" Font-Bold="true" runat="server" Text="Total"></dx:ASPxLabel>
                                        </FooterTemplate>
                                        <FooterCellStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="WAIST" Caption="Waist" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" FieldName="%WAIST" Caption="Waist %">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="INSEAM" Caption="Inseam" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" CellStyle-ForeColor="#0b993d" FieldName="%INSEAM" Caption="Inseam %">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="FRONT_RISE" Caption="Front Rise" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%FRONT_RISE" CellStyle-ForeColor="#0b993d" Caption="Front Rise %">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="BACK_RISE" Caption="Back Rise" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%BACK_RISE" Caption="Back Rise %" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="HI_HIP" Caption="H-Hip" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%HI_HIP" Caption="H-Hip %" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="LOW_HIP" Caption="L-Hip" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%LOW_HIP" Caption="L-Hip %" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="MUSLO" Caption="Muslo" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%MUSLO" Caption="Muslo %" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings ShowFooter="True" />
                                <TotalSummary>
                                    <dx:ASPxSummaryItem FieldName="WAIST" ShowInGroupFooterColumn="WAIST" DisplayFormat="{0:N0}" SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName="INSEAM" ShowInGroupFooterColumn="INSEAM" DisplayFormat="{0:N0}" SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName="FRONT_RISE" ShowInGroupFooterColumn="FRONT_RISE" DisplayFormat="{0:N0}" SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName="BACK_RISE" ShowInGroupFooterColumn="BACK_RISE" DisplayFormat="{0:N0}" SummaryType="Sum" />
 <dx:ASPxSummaryItem FieldName="HI_HIP" ShowInGroupFooterColumn="HI_HIP" DisplayFormat="{0:N0}" SummaryType="Sum" />
                                      <dx:ASPxSummaryItem FieldName="LOW_HIP" ShowInGroupFooterColumn="LOW_HIP" DisplayFormat="{0:N0}" SummaryType="Sum" />
                                     <dx:ASPxSummaryItem FieldName="MUSLO" ShowInGroupFooterColumn="MUSLO" DisplayFormat="{0:N0}" SummaryType="Sum" />
                                </TotalSummary>
                                <SettingsPager PageSize="33">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <br />
                            <dx:ASPxGridView ID="ASPxGridView3" OnDataBinding="ASPxGridView3_DataBinding" OnCustomCallback="ASPxGridView3_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                                <Columns>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="r" Caption="Ranges" />
                                    <dx:GridViewDataTextColumn FieldName="totalw" Caption="Waist" PropertiesTextEdit-DisplayFormatString="{0:N0}" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="procenW" Caption="Waist %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalI" Caption="Inseam" PropertiesTextEdit-DisplayFormatString="{0:N0}" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="procenI" Caption="Inseam %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalF" Caption="Front Rise" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenF" Caption="Front Rise %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalB" Caption="Back_Rise" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenB" Caption="Back Rise %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                     <dx:GridViewDataTextColumn FieldName="totalH" Caption="H-Hip" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenH" Caption="H-Hip %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                     <dx:GridViewDataTextColumn FieldName="totalH2" Caption="L-Hip" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenH2" Caption="L-Hip %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                     <dx:GridViewDataTextColumn FieldName="totalM" Caption="Muslo" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenM" Caption="Muslo %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                </Columns>
                                <SettingsPager PageSize="10">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <br />
                            <div class="col-lg-8" style="padding-left:0;padding-right: 2px;padding-bottom:10px">
                                  <dx:ASPxGridView ID="ASPxGridView4" OnDataBinding="ASPxGridView4_DataBinding" OnCustomCallback="ASPxGridView4_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="r" Caption="Ranges" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalw" Caption="Waist" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenW" Caption="Waist %" CellStyle-HorizontalAlign="Center" CellStyle-ForeColor="#0b993d" />
                                   <dx:GridViewDataTextColumn FieldName="totalH" Caption="H-Hip" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenH" Caption="H-Hip %" CellStyle-HorizontalAlign="Center" CellStyle-ForeColor="#0b993d" />
                                      <dx:GridViewDataTextColumn FieldName="totalH2" Caption="L-Hip" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenH2" Caption="L-Hip %" CellStyle-HorizontalAlign="Center" CellStyle-ForeColor="#0b993d" />
                                </Columns>
                                <SettingsPager PageSize="10">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            </div>
                          <div class="col-lg-4" style="padding-left:0;padding-right: 0;padding-bottom:10px">
                               <dx:ASPxGridView ID="ASPxGridView5" OnDataBinding="ASPxGridView5_DataBinding" OnCustomCallback="ASPxGridView5_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="r" Caption="Ranges" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalI" Caption="Inseam" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenI" Caption="Inseam %" CellStyle-HorizontalAlign="Center" CellStyle-ForeColor="#0b993d" />                                   
                                </Columns>
                                <SettingsPager PageSize="10">
                                </SettingsPager>
                            </dx:ASPxGridView>
                          </div>
                            
                            <br />
                            <dx:ASPxGridView ID="ASPxGridView2" OnDataBinding="ASPxGridView2_DataBinding" OnCustomCallback="ASPxGridView2_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="r" Caption="Ranges" CellStyle-HorizontalAlign="Center" />                            
                                    <dx:GridViewDataTextColumn FieldName="totalF" Caption="Front Rise" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenF" Caption="Front Rise %" CellStyle-HorizontalAlign="Center" CellStyle-ForeColor="#0b993d" />
                                    <dx:GridViewDataTextColumn FieldName="totalB" Caption="Back Rise" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenB" Caption="Back Rise %" CellStyle-HorizontalAlign="Center" CellStyle-ForeColor="#0b993d" />
                                    <dx:GridViewDataTextColumn FieldName="totalM" Caption="Muslo" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenM" Caption="Muslo %" CellStyle-HorizontalAlign="Center" CellStyle-ForeColor="#0b993d" />
                                </Columns>
                                <SettingsPager PageSize="10">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <br />
                           

                            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server"></dx:ASPxGridViewExporter>
                            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter3" runat="server"></dx:ASPxGridViewExporter>
                            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter4" runat="server"></dx:ASPxGridViewExporter>
                            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter5" runat="server"></dx:ASPxGridViewExporter>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

        <aside class="modal fade"  id="ventanaG">
        <div class="modal-dialog">
            <div class="modal-content">
                <article class="modal-header">
                    <button id="cerrarG" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                    <h4>Estilos Ingresados en la Semana </h4>
                </article>
                <article class="modal-body">
                    <asp:GridView runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White"  CssClass="table  table-hover table-striped " ID="grvestilos">

                    </asp:GridView>
                </article>
            </div>
        </div>
    </aside>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

