﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AuditoriaBultoABultoC21.aspx.cs" Inherits="admin_AuditoriaBultoABultoC21" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtstyle.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "Reportafterwash.aspx/GetStyle",
                        data: "{'pref' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    estilo: item.estilo,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

        });

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <link href="../css/font-awesome.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <br />
    <div class="container">

         <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

        <div class="panel panel-primary" style="margin-top:5px">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Measurement Report </strong></h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        Cut:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" OnClick="LinkButton1_Click" CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                        Line:
                                     <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                        Plant:
                                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>

                        Style:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" OnClick="LinkButton1_Click" CssClass="btn btn-default" ID="LinkButton2" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtstyle" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">

                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                    <asp:ListItem Value="1">Cut</asp:ListItem>
                                    <asp:ListItem Value="2">Style</asp:ListItem>
                                    <asp:ListItem Value="3">Line</asp:ListItem>
                                    <asp:ListItem Value="4">Plant</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                Start Date
                            <div class="input-group">

                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>

                            </div>
                                End Date
                          
                                <div class="input-group">
                                    <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                   <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                  
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0 !important">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="padding-top: 10px; padding-bottom: 10px">
                            <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" OnClick="btngenerar_Click" runat="server" />
                    </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="padding-top: 10px; padding-bottom: 10px">
                             <a href="#ventanaG" data-toggle="modal" style="width: 161px" class="btn btn-md btn-success"><span class="glyphicon glyphicon-align-justify"></span> Ver Estilos</a>
                    </div>
                        <div class="col-lg-12" style="overflow-x:auto">
                            <dx:ASPxFormLayout runat="server" ID="OptionsFormLayout">
                                <Items>
                                    <dx:LayoutGroup Caption="">
                                        <Items>
                                            <dx:LayoutItem Caption="Show Labels">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxCheckBox ID="chbShowLabels" runat="server" ClientInstanceName="chbShowLabels">
                                                            <ClientSideEvents CheckedChanged="function(s, e) {
                                                                {
                                                                   spinLabelIndent.SetEnabled(chbShowLabels.GetChecked());
                                                                }
                                                                   chart.PerformCallback(&quot;ShowLabels&quot;);
                                                             }" />
                                                        </dx:ASPxCheckBox>
                                                        <div style="display: none">


                                                            <dx:ASPxSpinEdit ID="spnLabelIndent" runat="server" Number="0" ClientInstanceName="spinLabelIndent" MaxValue="10000" NumberType="Integer">
                                                                <ClientSideEvents NumberChanged="function(s, e) { chart.PerformCallback(&quot;LabelIndent&quot;); }" />
                                                            </dx:ASPxSpinEdit>
                                                        </div>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>

                                        </Items>
                                    </dx:LayoutGroup>
                                </Items>
                            </dx:ASPxFormLayout>

                            <%--<dxchartsui:WebChartControl ID="packet_chart" Width="1000px" Height="394px"
                                runat="server" AppearanceNameSerializable="Pastel Kit" CrosshairEnabled="True"
                                SeriesDataMember="NombreMedida">
                                <BorderOptions Visibility="False" />
                                <BorderOptions Visibility="False"></BorderOptions>
                                <DiagramSerializable>
                                    <cc1:XYDiagram
                                        RangeControlDateTimeGridOptions-GridMode="ChartGrid">
                                        <AxisX VisibleInPanesSerializable="-1">
                                            <GridLines Visible="True">
                                            </GridLines>
                                        </AxisX>
                                        <AxisY VisibleInPanesSerializable="-1">
                                        </AxisY>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <SeriesTemplate ArgumentDataMember="Medida" LabelsVisibility="True" CrosshairLabelPattern="{S}:{V}"
                                    ValueDataMembersSerializable="total">
                                    <LabelSerializable>
                                        <cc1:SideBySideBarSeriesLabel Position="Top" TextPattern="Qty:{V}"
                                            Indent="5" LineLength="10" LineVisibility="True"
                                            ResolveOverlappingMode="Default">
                                        </cc1:SideBySideBarSeriesLabel>
                                    </LabelSerializable>
                                </SeriesTemplate>
                                <Titles>
                                    <cc1:ChartTitle Text="Report Measurement "></cc1:ChartTitle>
                                </Titles>
                            </dxchartsui:WebChartControl>--%>

                            <dxchartsui:WebChartControl ID="WebChartControl1" OnDataBinding="WebChartControl1_DataBinding" runat="server"
                                Width="1000px" Height="394px" ClientInstanceName="chart" AppearanceNameSerializable="Pastel Kit"
                                OnCustomCallback="WebChartControl1_CustomCallback" CrosshairEnabled="true"
                                EnableClientSideAPI="True" ToolTipEnabled="False"
                                SeriesDataMember="NombreMedida">
                                <BorderOptions Visibility="False" />
                                <BorderOptions Visibility="False"></BorderOptions>
                                <SeriesTemplate ArgumentDataMember="valor" ValueDataMembersSerializable="total" LabelsVisibility="True"
                                    CrosshairLabelPattern="{V:#.##} ">
                                    <LabelSerializable>
                                        <cc1:SideBySideBarSeriesLabel TextPattern="{V:#.##}">
                                        </cc1:SideBySideBarSeriesLabel>
                                    </LabelSerializable>
                                </SeriesTemplate>
                                <Titles>
                                    <cc1:ChartTitle Text="Report Measurement "></cc1:ChartTitle>
                                </Titles>
                                <Legend AlignmentHorizontal="Right" />
                                <DiagramSerializable>
                                    <cc1:XYDiagram>
                                        <AxisX VisibleInPanesSerializable="-1">
                                            <GridLines Visible="True">
                                            </GridLines>
                                        </AxisX>
                                        <AxisY VisibleInPanesSerializable="-1">
                                            
                                            <%-- <NumericScaleOptions AutoGrid="False" GridSpacing="1" {S}:{V}   Qty:{V}  {}{V:#.##0} />--%>
                                        </AxisY>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                            </dxchartsui:WebChartControl>


                        </div>
                        <div class="col-lg-4" style="padding-top: 20px; padding-bottom: 10px">
                            <asp:Button ID="btnexport" CssClass="btn btn-success form-control" OnClick="btnexport_Click" runat="server" Text="Export Excel" />
                            <asp:Button ID="btnexport1" CssClass="btn btn-success form-control" OnClick="btnexport1_Click" runat="server" Text="Export Excel" />
                        </div>

                        <%-- <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="PUNTOS_MEDIDA" Caption="Measured Points" />--%>

                        <div class="col-lg-12" style="overflow-x:auto">
                            <dx:ASPxMemo ID="memo" ClientInstanceName="memo" CssClass="form-control" runat="server" Height="130px" Width="400px" Caption="Note" Text="">
                                <CaptionSettings Position="Top" RequiredMarkDisplayMode="Optional" OptionalMark=" (optional)" />
                            </dx:ASPxMemo>
                            <br />
                            <dx:ASPxGridView ID="ASPxGridView1" OnDataBinding="ASPxGridView1_DataBinding" OnCustomCallback="ASPxGridView1_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                                <Columns>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="PUNTOS_MEDIDA" Caption="Measured Points">
                                        <FooterTemplate>
                                            <dx:ASPxLabel ID="ASPxLabeltotalydsrestantes" Font-Bold="true" runat="server" Text="Total"></dx:ASPxLabel>
                                        </FooterTemplate>
                                        <FooterCellStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="ACROSS_SHOULDER" Caption="Across Shoulder" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" FieldName="%ACROSS_SHOULDER" Caption="Across Shoulder %">
                                    </dx:GridViewDataTextColumn>
                                      <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="CHEST" Caption="Chest" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%CHEST" Caption="Chest %" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="SWEEP_RELAXED" Caption="Sweep Relaxed" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%SWEEP_RELAXED" Caption="Sweep Relaxed %" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="HPS_LENGHT" Caption="HPS Lenght" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%HPS_LENGHT" Caption="HPS Lenght %" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="CENTER_BACK_LENGHT_(Regular)" Caption="Center Back Lenght (Regular)" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%CENTER_BACK_LENGHT_(Regular)" Caption="Center Back Lenght (Regular)%" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="CENTER_BACK_LENGHT_(Tall)" Caption="Center Back Lenght (Tall)" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%CENTER_BACK_LENGHT_(Tall)" Caption="Center Back Lenght (Tall)%" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="SLEEVE_LENGHT_(Regular)" Caption="Sleeve Lenght (Regular)" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%SLEEVE_LENGHT_(Regular)" Caption="Sleeve Lenght (Regular)%" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>                                    
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="SLEEVE_LENGHT_(Tall)" Caption="Sleeve Lenght (Tall)" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%SLEEVE_LENGHT_(Tall)" Caption="Sleeve Lenght (Tall)%" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="HOOD_WIDTH_X_FROM_CROWN" Caption="Hood Width X” From Crown" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%HOOD_WIDTH_X_FROM_CROWN" Caption="Hood Width X” From Crown%" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="SLEEVE_CUFF_HEIGHT_RELAXED_AT_EDGE_SWEATS" Caption="Sleeve Cuff Height Relaxed At Eedge—Sweats" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%SLEEVE_CUFF_HEIGHT_RELAXED_AT_EDGE_SWEATS" Caption="Sleeve Cuff Height Relaxed At Eedge—Sweats%" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="HOOD_HEIGHT_AT_OPENING_EXTENDED" Caption="Hood Height At Opening-Extended" FooterCellStyle-Font-Bold="true" />
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="%HOOD_HEIGHT_AT_OPENING_EXTENDED" Caption="Hood Height At Opening-Extended%" CellStyle-ForeColor="#0b993d">
                                    </dx:GridViewDataTextColumn>
                                   
                                    
                                </Columns>
                                <Settings ShowFooter="True" />
                                <TotalSummary>
                                    <dx:ASPxSummaryItem FieldName="ACROSS_SHOULDER" ShowInGroupFooterColumn="ACROSS_SHOULDER" DisplayFormat="{0:N0}" SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName="CHEST" ShowInGroupFooterColumn="CHEST" DisplayFormat="{0:N0}" SummaryType="Sum" />
                                      <dx:ASPxSummaryItem FieldName="SWEEP_RELAXED" ShowInGroupFooterColumn="SWEEP_RELAXED" DisplayFormat="{0:N0}" SummaryType="Sum" />                                   
                                     <dx:ASPxSummaryItem FieldName="HPS_LENGHT" ShowInGroupFooterColumn="HPS_LENGHT" DisplayFormat="{0:N0}" SummaryType="Sum" />
                                     <dx:ASPxSummaryItem FieldName="CENTER_BACK_LENGHT_(Regular)" ShowInGroupFooterColumn="CENTER_BACK_LENGHT_(Regular)" DisplayFormat="{0:N0}" SummaryType="Sum" />                              
                                     <dx:ASPxSummaryItem FieldName="CENTER_BACK_LENGHT_(Tall)" ShowInGroupFooterColumn="CENTER_BACK_LENGHT_(Tall)" DisplayFormat="{0:N0}" SummaryType="Sum" />                              
                                     <dx:ASPxSummaryItem FieldName="SLEEVE_LENGHT_(Regular)" ShowInGroupFooterColumn="SLEEVE_LENGHT_(Regular)" DisplayFormat="{0:N0}" SummaryType="Sum" />                                     
                                     <dx:ASPxSummaryItem FieldName="SLEEVE_LENGHT_(Tall)" ShowInGroupFooterColumn="SLEEVE_LENGHT_(Tall)" DisplayFormat="{0:N0}" SummaryType="Sum" />

                                    <dx:ASPxSummaryItem FieldName="HOOD_WIDTH_X_FROM_CROWN" ShowInGroupFooterColumn="HOOD_WIDTH_X_FROM_CROWN" DisplayFormat="{0:N0}" SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName="SLEEVE_CUFF_HEIGHT_RELAXED_AT_EDGE_SWEATS" ShowInGroupFooterColumn="SLEEVE_CUFF_HEIGHT_RELAXED_AT_EDGE_SWEATS" DisplayFormat="{0:N0}" SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName="HOOD_HEIGHT_AT_OPENING_EXTENDED" ShowInGroupFooterColumn="HOOD_HEIGHT_AT_OPENING_EXTENDED" DisplayFormat="{0:N0}" SummaryType="Sum" />
                                                                    
                                </TotalSummary>
                                <SettingsPager PageSize="33">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <br />
                            <dx:ASPxGridView ID="ASPxGridView3" OnDataBinding="ASPxGridView3_DataBinding" OnCustomCallback="ASPxGridView3_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                                <Columns>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="r" Caption="Ranges" />
                                    <dx:GridViewDataTextColumn FieldName="totalA" Caption="Across Shoulder" PropertiesTextEdit-DisplayFormatString="{0:N0}" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="procenA" Caption="Across Shoulder %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalC" Caption="Chest" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenC" Caption="Chest %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                     <dx:GridViewDataTextColumn FieldName="totalSL" Caption="Sweep Relaxed" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenSL" Caption="Sweep Relaxed %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />                                  
                                    <dx:GridViewDataTextColumn FieldName="totalCB" Caption="HPS Lenght" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenCB" Caption="HPS Lenght %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalN" Caption="Center Back Lenght (Regular)" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenN" Caption="Center Back Lenght (Regular)%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalNT" Caption="Center Back Lenght (Tall)" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenNT" Caption="Center Back Lenght (Tall)%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalSR" Caption="Sleeve Lenght (Regular)" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenSR" Caption="Sleeve Lenght (Regular)%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />                                    
                                    <dx:GridViewDataTextColumn FieldName="totalST" Caption="Sleeve Lenght (Tall)" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenST" Caption="Sleeve Lenght (Tall)%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />

                                    <dx:GridViewDataTextColumn FieldName="totalHH" Caption="Hood Width X” From Crown" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenHH" Caption="Hood Width X” From Crown%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalSH" Caption="Sleeve Cuff Height Relaxed At Eedge—Sweats" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenSH" Caption="Sleeve Cuff Height Relaxed At Eedge—Sweats%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalHO" Caption="Hood Height At Opening-Extended" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenHO" Caption="Hood Height At Opening-Extended%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    
                                </Columns>
                                <SettingsPager PageSize="10">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <br />
                              <dx:ASPxGridView ID="ASPxGridView4" OnDataBinding="ASPxGridView4_DataBinding" OnCustomCallback="ASPxGridView4_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                                <Columns>
                                    <dx:GridViewDataTextColumn CellStyle-HorizontalAlign="Center" FieldName="r" Caption="Ranges" />
                                    <dx:GridViewDataTextColumn FieldName="totalA" Caption="Across Shoulder" PropertiesTextEdit-DisplayFormatString="{0:N0}" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="procenA" Caption="Across Shoulder %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalC" Caption="Chest" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenC" Caption="Chest %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                     <dx:GridViewDataTextColumn FieldName="totalSL" Caption="Sweep Relaxed" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenSL" Caption="Sweep Relaxed %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />                                  
                                    <dx:GridViewDataTextColumn FieldName="totalCB" Caption="HPS Lenght" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenCB" Caption="HPS Lenght %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalN" Caption="Center Back Lenght (Regular)" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenN" Caption="Center Back Lenght (Regular)%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalNT" Caption="Center Back Lenght (Tall)" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenNT" Caption="Center Back Lenght (Tall)%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalSR" Caption="Sleeve Lenght (Regular)" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenSR" Caption="Sleeve Lenght (Regular)%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />                                    
                                    <dx:GridViewDataTextColumn FieldName="totalST" Caption="Sleeve Lenght (Tall)" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenST" Caption="Sleeve Lenght (Tall)%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                   
                                    <dx:GridViewDataTextColumn FieldName="totalHH" Caption="Hood Width X” From Crown" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenHH" Caption="Hood Width X” From Crown%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalSH" Caption="Sleeve Cuff Height Relaxed At Eedge—Sweats" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenSH" Caption="Sleeve Cuff Height Relaxed At Eedge—Sweats%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalHO" Caption="Hood Height At Opening-Extended" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenHO" Caption="Hood Height At Opening-Extended%" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                    
                                </Columns>
                                <SettingsPager PageSize="10">
                                </SettingsPager>
                            </dx:ASPxGridView>
                              <br />
                            <div class="col-lg-8" style="padding-left:0;padding-right: 2px;padding-bottom:10px">
                                <%-- <dx:ASPxGridView ID="ASPxGridView2" OnDataBinding="ASPxGridView2_DataBinding" OnCustomCallback="ASPxGridView2_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="r" Caption="Ranges" CellStyle-HorizontalAlign="Center" />                            
                                    <dx:GridViewDataTextColumn FieldName="totalF" Caption="Front Rise" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenF" Caption="Front Rise %" CellStyle-HorizontalAlign="Center" CellStyle-ForeColor="#0b993d" />
                                    <dx:GridViewDataTextColumn FieldName="totalB" Caption="Back Rise" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenB" Caption="Back Rise %" CellStyle-HorizontalAlign="Center" CellStyle-ForeColor="#0b993d" />
                                     <dx:GridViewDataTextColumn FieldName="totalL" Caption="Leg Opening" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenL" Caption="Leg Opening %" CellStyle-ForeColor="#0b993d" CellStyle-HorizontalAlign="Center" />
                                
                                </Columns>
                                <SettingsPager PageSize="10">
                                </SettingsPager>
                            </dx:ASPxGridView>--%>
                            </div>
                          <div class="col-lg-4" style="padding-left:0;padding-right: 0;padding-bottom:10px">
                               <%--<dx:ASPxGridView ID="ASPxGridView5" OnDataBinding="ASPxGridView5_DataBinding" OnCustomCallback="ASPxGridView5_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="r" Caption="Ranges" CellStyle-HorizontalAlign="Center" />
                                    <dx:GridViewDataTextColumn FieldName="totalI" Caption="Inseam" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="{0:N0}" />
                                    <dx:GridViewDataTextColumn FieldName="procenI" Caption="Inseam %" CellStyle-HorizontalAlign="Center" CellStyle-ForeColor="#0b993d" />                                   
                                </Columns>
                                <SettingsPager PageSize="10">
                                </SettingsPager>
                            </dx:ASPxGridView>--%>
                          </div>
                            
                          
                           
                            <br />
                           

                            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server"></dx:ASPxGridViewExporter>
                            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter3" runat="server"></dx:ASPxGridViewExporter>
                            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter4" runat="server"></dx:ASPxGridViewExporter>
                            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter5" runat="server"></dx:ASPxGridViewExporter>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <aside class="modal fade"  id="ventanaG">
        <div class="modal-dialog">
            <div class="modal-content">
                <article class="modal-header">
                    <button id="cerrarG" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                    <h4>Estilos Ingresados en la Semana </h4>
                </article>
                <article class="modal-body">
                    <asp:GridView runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White"  CssClass="table  table-hover table-striped " ID="grvestilos">

                    </asp:GridView>
                </article>
            </div>
        </div>
    </aside>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

