﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SistemaAuditores.DataAccess;
using System.Data;
using System.Web.Security;

public partial class _SelectReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!this.IsPostBack)
            {
                if (Page.User.Identity.IsAuthenticated == true)//
                {
                    DataTable dt = DataAccess.Get_DataTable("select idcliente from tbusuariosDash where NameUser ='" + User.Identity.Name.Trim() + "'");

                    if (dt.Rows.Count > 0)
                    {
                      //  FormsAuthentication.SignOut();
                        Response.Redirect("../DashBoard?idcliente=" + dt.Rows[0][0].ToString(), false);
                    }
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
                }
            }
                        
        }
        catch (Exception)
        {
            throw;
        }
    }

    //protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    //{
    //    Response.Redirect("BundlesReport.aspx");
    //}
    //protected void LinkButton1_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("BundlesReport.aspx");
    //}
    //protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    //{
    //    Response.Redirect("SeccionReport.aspx");
    //}
    //protected void LinkButton2_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("SeccionReport.aspx");
    //}
    //protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
    //{
    //    Response.Redirect("ROC3.aspx");
    //}
    //protected void LinkButton3_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("ROC3.aspx");
    //}
    //protected void ImageButton3_Click1(object sender, ImageClickEventArgs e)
    //{
    //    string name= User.Identity.Name.ToString();
    //    if(name!="rrivera")
    //    Response.Redirect("ReporteMedidaMaster.aspx");
    //}

    protected void LinkButton3_Click1(object sender, EventArgs e)
    {
        string name = User.Identity.Name.ToString();
        if (name != "rrivera")
            Response.Redirect("ReporteMedidaMaster.aspx");
    }

    protected void lnk1section_Click(object sender, EventArgs e)
    {
        Response.Redirect("SeccionReport.aspx");
    }

    protected void lnnkmedida_Click(object sender, EventArgs e)
    {
        Response.Redirect("ReporteMedidaMaster.aspx");
    }

    protected void bundlebybunlde_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin/SeccionReport.aspx");
    }

    protected void packmedi_Click(object sender, EventArgs e)
    {
        //Response.Redirect("../adminRepMedidasNuevos/ReporteMedidaMasterGuia.aspx");
        //string url = "../adminRepMedidasNuevos/ReporteMedidaMasterGuia.aspx";
        //Response.Write("<script>window.open('" + url + "','_blank');</script>");
        Response.Redirect("../adminRepMedidasNuevos/ReporteMedidaMasterGuia.aspx");
    }

    protected void linkLavanderia_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportesEnvios.aspx");
    }
}