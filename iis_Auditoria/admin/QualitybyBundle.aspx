﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="QualitybyBundle.aspx.cs" Inherits="OQLbyBundle" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .myCenteredGrid {
            margin: 0 auto;
            display: block;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentTitulo" Runat="Server">
        <div style="font-size: 24px; margin-left: auto; margin-right: auto; text-align: center;">
            <asp:Label ID="lbl" runat="server" Text="Quality Report by Bundle" Visible="false"></asp:Label><br />            
        </div> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
    <div style="display: table-row;">
        <div style="display: table-cell; padding: 5px; border-left: 1px solid #ccc; border-top: 1px solid #ccc; border-bottom: 1px solid #ccc;">
            <asp:Label ID="Label3" runat="server" Text="Line: "></asp:Label>&nbsp;&nbsp;
        </div>        
        <div style="display: table-cell; padding: 5px; border-top: 1px solid #ccc; border-bottom: 1px solid #ccc;">
            <asp:DropDownList ID="DropDownList1" runat="server">
               <%-- <asp:ListItem Value="0">Select a Value</asp:ListItem>
                <asp:ListItem Value="1">1</asp:ListItem>
                <asp:ListItem Value="2">2</asp:ListItem>
                <asp:ListItem Value="3">3</asp:ListItem>
                <asp:ListItem Value="4">4</asp:ListItem>
				<asp:ListItem Value="5">5</asp:ListItem>
				<asp:ListItem Value="6">6</asp:ListItem>
				<asp:ListItem Value="7">7</asp:ListItem>
				<asp:ListItem Value="8">8</asp:ListItem>
				<asp:ListItem Selected="True" Value="9">9</asp:ListItem>--%>
            </asp:DropDownList>
        </div>
        <div style="display: table-cell; padding: 5px; border-left: 1px solid #ccc; border-bottom: 1px solid #ccc; border-top: 1px solid #ccc;">
            <asp:Label ID="Label2" runat="server" Text="Start Date: "></asp:Label>&nbsp;&nbsp;
        </div>
        <div style="display: table-cell; padding: 5px; border-bottom: 1px solid #ccc; border-top: 1px solid #ccc;">
            <dx:aspxdateedit id="txtfecha1" runat="server" animationtype="Fade" theme="Metropolis">
            <CalendarProperties FirstDayOfWeek="Monday">
            </CalendarProperties>
        </dx:aspxdateedit>
        </div>
        <div style="display: table-cell; padding: 5px; border-left: 1px solid #ccc; border-bottom: 1px solid #ccc; border-top: 1px solid #ccc;">
            <asp:Label ID="Label4" runat="server" Text="End Date: "></asp:Label>&nbsp;&nbsp;
        </div>
        <div style="display: table-cell; padding: 5px; border-bottom: 1px solid #ccc; border-top: 1px solid #ccc;">
            <dx:aspxdateedit id="txtfecha2" runat="server" animationtype="Fade" theme="Metropolis">
            <CalendarProperties FirstDayOfWeek="Monday" HighlightWeekends="False">
            </CalendarProperties>
        </dx:aspxdateedit>
        </div>
        <div style="display: table-cell; padding: 5px; border-top: 1px solid #ccc; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;">
            <dx:aspxbutton id="ASPxButton1" runat="server" text="Search" OnClick="ASPxButton1_Click"
                theme="MetropolisBlue">
        </dx:aspxbutton>
        </div>
    </div>
    </center>
    <br />
    <div class="container">
        <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" CssClass="myCenteredGrid" PaletteName="Pastel Kit" CrosshairEnabled="True"  Height="400px" Width="600px">
            <diagramserializable>
                <cc1:XYDiagram>
                    <axisx visibleinpanesserializable="-1">
                    </axisx>
                    <axisy visibleinpanesserializable="-1">
                    </axisy>
                </cc1:XYDiagram>
            </diagramserializable>
            <legend visibility="True"></legend>
            <seriesserializable>
                <cc1:Series ArgumentDataMember="Calidad" Name="Series 1" ValueDataMembersSerializable="Valor" LabelsVisibility="True" LegendTextPattern="{A} {V}%">
                    <viewserializable>
                        <cc1:SideBySideBarSeriesView ColorEach="True" BarWidth="0.4">
                        </cc1:SideBySideBarSeriesView>
                    </viewserializable>
                    <labelserializable>
                        <cc1:SideBySideBarSeriesLabel Position="Top" TextPattern="{V}%">
                        </cc1:SideBySideBarSeriesLabel>
                    </labelserializable>
                </cc1:Series>
            </seriesserializable>
        </dxchartsui:WebChartControl>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:RocedesCS %>" SelectCommand="select distinct(Id_Bundle)as Calidad, [Id_Linea] as Valor from BundleRejectedBulto where id_bio=5 and FechaRecibido='2016-03-02'"></asp:SqlDataSource>
    </div>

</asp:Content>

