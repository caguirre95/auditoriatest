﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BundlesReport.aspx.cs" Inherits="_BundlesReport" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
    <h3><asp:Label ID="Label1" runat="server" Text="Live Checkoff Report"></asp:Label></h3>    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><br /><br /><br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
     <center>
     <div class="container">
       <div class="col-md-4">
    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="/img/rank.png"  
             Width="128px" Height="128px"  
        onclick="ImageButton2_Click" /><br />
    <asp:LinkButton ID="LinkButton2" runat="server" onclick="LinkButton2_Click">Top 5 Defects</asp:LinkButton>
    </div>
   <div class="col-md-4">
    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="/img/pie.png"  
             Width="128px" Height="128px"  
        onclick="ImageButton1_Click" /><br />
    <asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click">OQL/AQL</asp:LinkButton>
    </div>
     <div class="col-md-4">
    <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="/img/workers.png"  
             Width="128px" Height="128px"  
        onclick="ImageButton3_Click" /><br />
    <asp:LinkButton ID="LinkButton3" runat="server" onclick="LinkButton3_Click">Top 5 Operators</asp:LinkButton>
    </div>
	
     <div class="col-md-4">
    <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="/img/biho.png"  
             Width="128px" Height="128px"  
        onclick="ImageButton5_Click" /><br />
    <asp:LinkButton ID="LinkButton5" runat="server" onclick="LinkButton5_Click">Bihorario Audit Control</asp:LinkButton>
    </div>
     </div>
     </center>
</asp:Content>

