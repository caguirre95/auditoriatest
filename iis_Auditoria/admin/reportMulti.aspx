﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="reportMulti.aspx.cs" Inherits="admin_reportMulti" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Measurement Report </strong></h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <dx:ASPxDateEdit ID="txtfecha1" Theme="Metropolis" Height="35px" runat="server" EditFormat="Custom" Width="100%">
                    </dx:ASPxDateEdit>

                    <br />
                    <dx:ASPxDateEdit ID="txtfecha2" Theme="Metropolis" Height="35px" runat="server" EditFormat="Custom" Width="100%">
                    </dx:ASPxDateEdit>
                    <br />
                    <asp:Button ID="btnGenerar" OnClick="btnGenerar_Click" runat="server" Text="Generate Report" CssClass="btn btn-primary form-control" />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    Before Wash In Line
                <dx:ASPxGridView ID="ASPxGridView1" Width="100%" OnDataBinding="ASPxGridView1_DataBinding" OnCustomCallback="ASPxGridView1_CustomCallback" runat="server"></dx:ASPxGridView>
                    Before Wash In Line
                <dx:ASPxGridView ID="ASPxGridView2" Width="100%" runat="server"></dx:ASPxGridView>
                    After Wash Measurement
                <dx:ASPxGridView ID="ASPxGridView3" Width="100%" runat="server"></dx:ASPxGridView>
                    After Wash Final Audit
                <dx:ASPxGridView ID="ASPxGridView4" Width="100%" runat="server"></dx:ASPxGridView>
                    After Wash Final Audit																
                <dx:ASPxGridView ID="ASPxGridView5" Width="100%" OnDataBinding="ASPxGridView5_DataBinding" OnCustomCallback="ASPxGridView5_CustomCallback" runat="server"></dx:ASPxGridView>
                    After Wash Final Audit
                <dx:ASPxGridView ID="ASPxGridView6" Width="100%" OnDataBinding="ASPxGridView6_DataBinding" OnCustomCallback="ASPxGridView6_CustomCallback" runat="server"></dx:ASPxGridView>
                </div>

                <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                <dx:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server"></dx:ASPxGridViewExporter>
                <dx:ASPxGridViewExporter ID="ASPxGridViewExporter3" runat="server"></dx:ASPxGridViewExporter>
                <dx:ASPxGridViewExporter ID="ASPxGridViewExporter4" runat="server"></dx:ASPxGridViewExporter>
                <dx:ASPxGridViewExporter ID="ASPxGridViewExporter5" runat="server"></dx:ASPxGridViewExporter>
                <dx:ASPxGridViewExporter ID="ASPxGridViewExporter6" runat="server"></dx:ASPxGridViewExporter>
                
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

