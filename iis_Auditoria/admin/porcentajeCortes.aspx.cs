﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_porcentajeCortes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
          //  txtfecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["tot"] = null;
        }
    }

    protected void btnGenerar_Click(object sender, EventArgs e)
    {
        try
        {        
            ASPxGridView2.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
    }

    void funcion()
    {
        //if (DropDownListProcess.SelectedValue == "0")
        //{
        //    return;
        //}

        //int resp = int.Parse(DropDownListProcess.SelectedValue);
        string query = "";
        query =  " select po.POrder,(convert(decimal, (COUNT(mb.idMedidaMaster) / 2)) / po.Quantity) * 100 as porcent"  
                        +" from(select po.Id_Order, po.POrder, po.Quantity from porder po"
                                              +" join Bundle b on po.Id_Order = b.Id_Order"
                                              +" join tblPuntoMasterIntex pm on b.Id_Bundle = pm.idbulto"
                                              + " join tblMedidaBultoIntex mb on pm.idPuntoMaster = mb.idMaster where convert(date, mb.fecha) = '" + txtfecha1.Text + "'"
                                              + " group by po.Id_Order, po.POrder, po.Quantity) po"
                                              +" join Bundle b on po.Id_Order = b.Id_Order"
                        +" join tblPuntoMasterIntex pm on b.Id_Bundle = pm.idbulto"
                        +" join tblMedidaBultoIntex mb on pm.idPuntoMaster = mb.idMaster"
                        +" group by po.POrder,po.Quantity";
        //switch (resp)
        //{
        //    case 1:
        //        //query = " select COUNT( um.userms)as total,um.userName,pm.NombreMedida"
        //        //      + " from tblMedidaBultoAL mbi"
        //        //      + " join tblPuntoMasterAL pmi on mbi.idMaster = pmi.idPuntoMaster"
        //        //      + " join tblPuntosMedida pm on pmi.idpuntosM = pm.idPuntosM"
        //        //      + " join tblUserMedida um on mbi.insp = um.userms"
        //        //      + " where CONVERT(date, mbi.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and um.rol = '" + RadioButtonList1.SelectedValue + "'"
        //        //      + " group by um.userName,pm.NombreMedida";

        //        break;
        //    case 2:
        //        //query= " select po.POrder,(convert(decimal, (COUNT(mb.idMedidaMaster) / 2)) / po.Quantity) * 100 as porcent  from porder po"
        //        //     + " join Bundle b on po.Id_Order = b.Id_Order"
        //        //     + " join tblPuntoMasterIntex pm on b.Id_Bundle = pm.idbulto"
        //        //     + " join tblMedidaBultoIntex mb on pm.idPuntoMaster = mb.idMaster"
        //        //     + " where convert(date, mb.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
        //        //     + " group by po.POrder,po.Quantity";
               

        //        break;
        //    case 3:

        //        //query = " select COUNT( um.userms)as total,um.userName,pm.NombreMedida"
        //        //       + " from tblMedidaBulto mbi"
        //        //       + " join tblPuntoMaster pmi on mbi.idMaster = pmi.idPuntoMaster"
        //        //       + " join tblPuntosMedida pm on pmi.idpuntosM = pm.idPuntosM"
        //        //       + " join tblUserMedida um on mbi.insp = um.userms"
        //        //       + " where CONVERT(date, mbi.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
        //        //       + " group by um.userName,pm.NombreMedida";


        //        break;
        //    default:
        //        break;
        //}


        DataTable dt = new DataTable();
        dt = DataAccess.Get_DataTable(query);

        // packet_chart.DataSource = dt;
        // packet_chart.DataBind();

        List<classPoCent> list = new List<classPoCent>();

       

        //      obj.total = "Total";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            classPoCent obj = new classPoCent();

            obj.porder = dt.Rows[i][0].ToString();
            obj.porcent = (Math.Round((Convert.ToDecimal(dt.Rows[i][1])), 2)).ToString() + "%";

            list.Add(obj);
        }
        ASPxGridView2.DataSource = list;
        //list.Add(obj);
       // Session["tot"] = list;
       // ASPxGridView2.DataBind();

    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        ASPxGridViewExporter1.GridViewID = "ASPxGridView2";
        ASPxGridViewExporter1.FileName = "Report " + DateTime.Now.ToShortDateString();
        ASPxGridViewExporter1.WriteXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });

     //   funcion();

     //   ASPxGridViewExporter1.GridViewID = "";

     //   DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
     //// DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
     //   DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
     //   link1.Component = ASPxGridViewExporter1;

     //  // link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)packet_chart).Chart;

     //   DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
     //   compositeLink.Links.AddRange(new object[] { link1});

     //  // compositeLink.CreatePageForEachLink();

     //   using (MemoryStream stream = new MemoryStream())
     //   {
     //       XlsxExportOptions options = new XlsxExportOptions();
     //       options.ExportMode = XlsxExportMode.SingleFilePageByPage;
     //       options.SheetName = "Report";
     //       compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
     //       Response.Clear();
     //       Response.Buffer = false;
     //       Response.AppendHeader("Content-Type", "application/xlsx");
     //       Response.AppendHeader("Content-Transfer-Encoding", "binary");
     //       Response.AppendHeader("Content-Disposition", "attachment; filename=Master_Report_Line.xlsx");
     //       Response.BinaryWrite(stream.ToArray());
     //       Response.End();
     //   }
     //   ps.Dispose();
    }

    protected void DropDownListProcess_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (DropDownListProcess.SelectedValue == "1")
        //{
        //    Panel1.Visible = true;
        //}
        //else
        //{
        //    Panel1.Visible = false;
        //}
    }

    protected void ASPxGridView2_DataBinding(object sender, EventArgs e)
    {
        try
        {
            funcion();
            //ASPxGridView2.DataSource = ReportDat.reporteplan(DropDownList1.SelectedItem.Text, fecha1.Date, fecha2.Date);
            ASPxGridView2.ExpandAll();

            //ASPxGridView2.DataSource = (List<classPoCent>)Session["tot"];
            //Session["tot"] = null;
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void ASPxGridView2_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        //funcion();
        ASPxGridView2.DataBind();
    }

    public class classPoCent
    {
        public string porder { get; set; }

        public string porcent { get; set; }
        
    }


}