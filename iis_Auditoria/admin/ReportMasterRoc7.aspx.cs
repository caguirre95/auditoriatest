﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReportMasterRoc7 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtfecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["tot"] = null;
        }
        //planta();
    }

    //public void planta()
    //{
    //    string planta = "select id_planta as id, descripcion as Planta from Planta where descripcion not like '%7%' ";
    //    DataTable p = DataAccess.Get_DataTable(planta);
    //    plantad.DataSource = p;
    //    plantad.DataValueField = "id" ;
    //    plantad.DataTextField = "Planta" ;
    //    plantad.DataBind();        
    //}

    protected void btnGenerar_Click(object sender, EventArgs e)
    {
        int resp;
        
        if (DropDownListProcess.SelectedValue == "0" || plantad.SelectedValue=="0")
        {
            return;
        }

        if (plantad.SelectedValue == "4")
        {
            resp = 4;
        }
        else
        {
            resp = int.Parse(DropDownListProcess.SelectedValue);
        }


        string query = "";
        switch (resp)
        {
            case 1:
                query = " select COUNT( um.userms)as total,RTRIM(um.userName) +'-L'+l.numero as userName,pm.NombreMedida"
                      + " from tblMedidaBultoAL mbi"
                      + " join tblPuntoMasterAL pmi on mbi.idMaster = pmi.idPuntoMaster"
                      + " join tblPuntosMedida pm on pmi.idpuntosM = pm.idPuntosM"
                      + " join tblUserMedida um on mbi.insp = um.userms"
                      + " join Linea l on l.id_linea = um.linea"
                      + " where CONVERT(date, mbi.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and um.rol = '" + RadioButtonList1.SelectedValue + "' and l.id_planta = '" + plantad.SelectedValue + "'"
                      + " group by um.userName,l.numero,pm.NombreMedida   order by convert(int,l.numero) asc";

                break;
            case 4:
                query = " select COUNT( um.userms)as total,RTRIM(um.userName) +'-L'+l.numero as userName,pm.NombreMedida"
                     + " from tblMedidaBultoAL mbi"
                     + " join tblPuntoMasterAL pmi on mbi.idMaster = pmi.idPuntoMaster"
                     + " join tblPuntosMedida pm on pmi.idpuntosM = pm.idPuntosM"
                     + " join tblUserMedida um on mbi.insp = um.userms"
                     + " join Linea l on l.id_linea = um.linea"
                     + " where CONVERT(date, mbi.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and um.rol = '" + RadioButtonList1.SelectedValue + "'"
                     + " group by um.userName,l.numero,pm.NombreMedida   order by convert(int,l.numero) asc";

                plantad.SelectedValue = "0";

                break;
            case 2:

                query = " select COUNT( um.userms)as total,um.userName,pm.NombreMedida"
                     + " from tblMedidaBultoIntex mbi"
                     + " join tblPuntoMasterIntex pmi on mbi.idMaster = pmi.idPuntoMaster"
                     + " join tblPuntosMedida pm on pmi.idpuntosM = pm.idPuntosM"
                     + " join tblUserMedida um on mbi.inspec = um.userms"
                     + " where CONVERT(date, mbi.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                     + " group by um.userName,pm.NombreMedida";

                break;
            case 3:

                query = " select COUNT( um.userms)as total,um.userName,pm.NombreMedida"
                       + " from tblMedidaBulto mbi"
                       + " join tblPuntoMaster pmi on mbi.idMaster = pmi.idPuntoMaster"
                       + " join tblPuntosMedida pm on pmi.idpuntosM = pm.idPuntosM"
                       + " join tblUserMedida um on mbi.insp = um.userms"
                       + " where CONVERT(date, mbi.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                       + " group by um.userName,pm.NombreMedida";


                break;
            default:
                break;
        }


        DataTable dt = new DataTable();
        dt = DataAccess.Get_DataTable(query);

        packet_chart.DataSource = dt;
        packet_chart.DataBind();

        List<classtotal> list = new List<classtotal>();

        classtotal obj = new classtotal();

        obj.total = "Total";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i][2].ToString().Equals("WAIST"))
            {
                obj.waist = obj.waist + int.Parse(dt.Rows[i][0].ToString());
            }
            else if (dt.Rows[i][2].ToString().Equals("INSEAM"))
            {
                obj.inseam = obj.inseam + int.Parse(dt.Rows[i][0].ToString());
            }
            else if (dt.Rows[i][2].ToString().Equals("BACK RISE"))
            {
                obj.backrise = obj.backrise + int.Parse(dt.Rows[i][0].ToString());
            }
            else if (dt.Rows[i][2].ToString().Equals("FRONT RISE"))
            {
                obj.frontrise = obj.frontrise + int.Parse(dt.Rows[i][0].ToString());
            }
            else if (dt.Rows[i][2].ToString().Equals("HIP"))
            {
                obj.hip = obj.hip + int.Parse(dt.Rows[i][0].ToString());
            }
        }

        list.Add(obj);
        Session["tot"] = list;

        ASPxGridView2.DataBind();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        int resp;
        if (DropDownListProcess.SelectedValue == "0")
        {
            return;
        }

        if (plantad.SelectedValue == "4")
        {
            resp = 4;
        }
        else
        {
            resp = int.Parse(DropDownListProcess.SelectedValue);
        }

        string query = "";
        switch (resp)
        {
            case 1:
                query = " select COUNT( um.userms)as total,RTRIM(um.userName) +'-L'+l.numero as userName,pm.NombreMedida"
                      + " from tblMedidaBultoAL mbi"
                      + " join tblPuntoMasterAL pmi on mbi.idMaster = pmi.idPuntoMaster"
                      + " join tblPuntosMedida pm on pmi.idpuntosM = pm.idPuntosM"
                      + " join tblUserMedida um on mbi.insp = um.userms"
                      + " join Linea l on l.id_linea = um.linea"
                      + " where CONVERT(date, mbi.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and um.rol = '" + RadioButtonList1.SelectedValue + "' and l.id_planta = '" + plantad.SelectedValue + "'"
                      + " group by um.userName,l.numero,pm.NombreMedida   order by convert(int,l.numero) asc";

                break;
            case 4:
                query = " select COUNT( um.userms)as total,RTRIM(um.userName) +'-L'+l.numero as userName,pm.NombreMedida"
                     + " from tblMedidaBultoAL mbi"
                     + " join tblPuntoMasterAL pmi on mbi.idMaster = pmi.idPuntoMaster"
                     + " join tblPuntosMedida pm on pmi.idpuntosM = pm.idPuntosM"
                     + " join tblUserMedida um on mbi.insp = um.userms"
                     + " join Linea l on l.id_linea = um.linea"
                     + " where CONVERT(date, mbi.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and um.rol = '" + RadioButtonList1.SelectedValue + "'"
                     + " group by um.userName,l.numero,pm.NombreMedida   order by convert(int,l.numero) asc";

                break;

            case 2:

                query = " select COUNT( um.userms)as total,um.userName,pm.NombreMedida"
                     + " from tblMedidaBultoIntex mbi"
                     + " join tblPuntoMasterIntex pmi on mbi.idMaster = pmi.idPuntoMaster"
                     + " join tblPuntosMedida pm on pmi.idpuntosM = pm.idPuntosM"
                     + " join tblUserMedida um on mbi.inspec = um.userms"
                     + " where CONVERT(date, mbi.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                     + " group by um.userName,pm.NombreMedida";

                break;
            case 3:

                query = " select COUNT( um.userms)as total,um.userName,pm.NombreMedida"
                       + " from tblMedidaBulto mbi"
                       + " join tblPuntoMaster pmi on mbi.idMaster = pmi.idPuntoMaster"
                       + " join tblPuntosMedida pm on pmi.idpuntosM = pm.idPuntosM"
                       + " join tblUserMedida um on mbi.insp = um.userms"
                       + " where CONVERT(date, mbi.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                       + " group by um.userName,pm.NombreMedida";


                break;
            default:
                break;
        }

        DataTable dt = new DataTable();
        dt = DataAccess.Get_DataTable(query);


        packet_chart.DataSource = dt;
        packet_chart.DataBind();

        List<classtotal> list = new List<classtotal>();
        classtotal obj = new classtotal();

        obj.total = "Total";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i][2].ToString().Equals("WAIST"))
            {
                obj.waist = obj.waist + int.Parse(dt.Rows[i][0].ToString());
            }
            else if (dt.Rows[i][2].ToString().Equals("INSEAM"))
            {
                obj.inseam = obj.inseam + int.Parse(dt.Rows[i][0].ToString());
            }
            else if (dt.Rows[i][2].ToString().Equals("BACK RISE"))
            {
                obj.backrise = obj.backrise + int.Parse(dt.Rows[i][0].ToString());
            }
            else if (dt.Rows[i][2].ToString().Equals("FRONT RISE"))
            {
                obj.frontrise = obj.frontrise + int.Parse(dt.Rows[i][0].ToString());
            }
            else if (dt.Rows[i][2].ToString().Equals("HIP"))
            {
                obj.hip = obj.hip + int.Parse(dt.Rows[i][0].ToString());
            }
        }

        list.Add(obj);
        Session["tot"] = list;

        ASPxGridView2.DataBind();

        ASPxGridViewExporter1.GridViewID = "ASPxGridView2";

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link1.Component = ASPxGridViewExporter1;

        link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)packet_chart).Chart;

        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link1, link2 });

        compositeLink.CreatePageForEachLink();

        using (MemoryStream stream = new MemoryStream())
        {
            XlsxExportOptions options = new XlsxExportOptions();
            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
            options.SheetName = "Master_Report_Line";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=Master_Report_Line.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }
        ps.Dispose();
    }

    protected void DropDownListProcess_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "1")
        {
            Panel1.Visible = true;
        }
        else
        {
            Panel1.Visible = false;
        }
    }

    protected void ASPxGridView2_DataBinding(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView2.DataSource = (List<classtotal>)Session["tot"];
            Session["tot"] = null;
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void ASPxGridView2_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView2.DataBind();
    }

    public class classtotal
    {
        public string total { get; set; }
        public int waist { get; set; }
        public int inseam { get; set; }
        public int frontrise { get; set; }
        public int backrise { get; set; }
        public int hip { get; set; }
    }



}