﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MedidaIntex2.aspx.cs" Inherits="admin_MedidaIntex2" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtstyle.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "Reportafterwash.aspx/GetStyle",
                        data: "{'pref' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    estilo: item.estilo,
                                    id: item.id,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);
                    $('#<%=hdidestilo.ClientID%>').val(ui.item.id);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

        });

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <link href="../css/font-awesome.css" rel="stylesheet" />

    <style>
        .es {
            margin-top: 20px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <br />

    <div class="container">

        <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

        <div class="panel panel-primary" style="margin-top: 5px">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Reporte de Medida Intex</strong></h3>
            </div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">

                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                    <asp:ListItem Value="1">Cut</asp:ListItem>
                                    <asp:ListItem Value="2">Style</asp:ListItem>
                                    <asp:ListItem Value="3">Line</asp:ListItem>
                                    <asp:ListItem Value="4">Plant</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>

                        </div>

                        <br />
                        <a href="#ventanaG" data-toggle="modal" style="width: 200px; margin-top: 5px" class="btn  btn-info"><span class="glyphicon glyphicon-align-justify"></span>Ver Estilos</a>

                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                Cut:                        
                                <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                Style:
                                <asp:TextBox ID="txtstyle" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                Line:
                                <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                                Plant:
                                <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                Start Date
                                <div class="input-group">
                                    <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                    <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                End Date                          
                                <div class="input-group">
                                    <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                    <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <br />
                                <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" OnClick="btngenerar_Click" runat="server" />
                                <asp:HiddenField runat="server" ID="hdidestilo" />
                                <asp:Button Text="Export Report" CssClass="btn btn-success form-control es" ID="btnexport" OnClick="btnexport_Click" runat="server" />
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <hr />
                        <dx:ASPxGridView ID="ASPxGridView1" OnDataBinding="ASPxGridView1_DataBinding" OnCustomCallback="ASPxGridView1_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                            <Columns>
                                <dx:GridViewBandColumn Caption="Puntos de Medida y Frecuencias" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="PuntoMedida" Caption="Puntos de Medida"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="WAIST" Caption="WAIST"></dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="INSEAM" Caption="INSEAM"></dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                            </Columns>
                            <Settings ShowFooter="True" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="WAIST" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="INSEAM" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <SettingsPager PageSize="36">
                            </SettingsPager>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                    </div>


                </div>

            </div>
        </div>

    </div>

    <aside class="modal fade" id="ventanaG">
        <div class="modal-dialog">
            <div class="modal-content">
                <article class="modal-header">
                    <button id="cerrarG" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                    <h4>Estilos Ingresados en la Semana </h4>
                </article>
                <article class="modal-body">
                    <asp:GridView runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" CssClass="table  table-hover table-striped " ID="grvestilos">
                    </asp:GridView>
                </article>
            </div>
        </div>
    </aside>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

