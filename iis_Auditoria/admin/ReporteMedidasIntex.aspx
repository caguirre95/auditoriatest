﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteMedidasIntex.aspx.cs" Inherits="admin_ReporteMedidasIntex" %>


<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v16.2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

   <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtstyle.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "Reportafterwash.aspx/GetStyle",
                        data: "{'pref' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    estilo: item.estilo,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

        });

    </script>

    <style>
        .a {
            margin-top: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Reporte de Medidas despues de Lavado</strong></div>
            <div class="panel-body">

                <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control" Visible="false"></asp:SiteMapPath>

                 <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        Cut:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                            <asp:LinkButton Text="" CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                        </span>
                        <div class="ui-widget" style="text-align: left;">
                            <asp:TextBox ID="txtPorder" placeholder="Codigo de Corte" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hfidPorder" />
                        </div>
                                    </div>
                        Line:
                                     <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                        Plant:
                                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>

                        Style:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text=""  CssClass="btn btn-default" ID="LinkButton3" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtstyle" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                     <br />
                    <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-primary form-control" Text="Generar" Width="200px" OnClick="btngenerar_Click" />

                    <asp:Button ID="btnExport" OnClick="btnExport_Click" CssClass="btn btn-success form-control a" runat="server" Text="Export Excel" Width="200px" />

                    <a href="#ventanaG" data-toggle="modal" style="width: 200px;margin-top:5px" class="btn  btn-info"><span class="glyphicon glyphicon-align-justify"></span> Ver Estilos</a>
                    <hr />
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">

                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                    <asp:ListItem Value="1">Cut</asp:ListItem>
                                    <asp:ListItem Value="2">Style</asp:ListItem>
                                    <asp:ListItem Value="3">Line</asp:ListItem>
                                    <asp:ListItem Value="4">Plant</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                Start Date
                            <div class="input-group">

                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>

                            </div>
                                End Date
                          
                                <div class="input-group">
                                    <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                   <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                               
                            </div>
                        </div>
                    </div>



                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow-x: auto">
                    <dx:ASPxGridView ID="gridpack" runat="server" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnDataBinding="gridpack_DataBinding" OnCustomCallback="gridpack_CustomCallback">
                        <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                        <Columns>
                            <dx:GridViewBandColumn Caption="Reporte de Medidas despues de Lavado" HeaderStyle-HorizontalAlign="Center">
                                <Columns>
                                  
                                    <dx:GridViewDataTextColumn FieldName="POrder" Caption="Corte" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="color" Caption="Color" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Cantidad" Caption="Cantidad" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Aceptado" Caption="Piezas Aceptadas" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Rechazado" Caption="Piezas Rechazadas" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="total" Caption="Piezas Totales" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                   
                                </Columns>
                            </dx:GridViewBandColumn>
                        </Columns>
                       
                        <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                        <Settings ShowFooter="true" />
                        <SettingsPager PageSize="100" />
                        <SettingsBehavior EnableCustomizationWindow="true" />
                       
                    </dx:ASPxGridView>
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                </div>
            </div>

        </div>

    </div>

     <aside class="modal fade"  id="ventanaG">
        <div class="modal-dialog">
            <div class="modal-content">
                <article class="modal-header">
                    <button id="cerrarG" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                    <h4>Estilos Ingresados en la Semana </h4>
                </article>
                <article class="modal-body">
                    <asp:GridView runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White"  CssClass="table  table-hover table-striped " ID="grvestilos">

                    </asp:GridView>
                </article>
            </div>
        </div>
    </aside>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

