﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReporteEmbarqueMedidas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["intexm"] = null;
            Session["intexmEMB_DD"] = null;
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();
            ASPxDateEdit1.Text = DateTime.Now.ToShortDateString();
            ASPxDateEdit2.Text = DateTime.Now.ToShortDateString();
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin/ReporteEmbarqueMedidas.aspx");
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            var consulta = new DataTable();
            var consultaDD = new DataTable();
            string sqlquery = "";

            sqlquery = " select * from FN_MedidasEmbarques ('" + txtfecha1.Text + "','" + txtfecha2.Text + "')";


            consulta = DataAccess.Get_DataTable(sqlquery);
            Session["intexm"] = consulta;

            sqlquery = "EXEC dbo.Medidas_Contadas_Embarque_SP_Qry @desde='" + txtfecha1.Text + "', @hasta='" + txtfecha2.Text + "';";
            consultaDD = DataAccess.Get_DataTable(sqlquery);
            Session["intexmEMB_DD"] = consultaDD;

            grdmi.DataBind();
            grdmiDD.DataBind();


        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            export.GridViewID = "grdmi";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = export;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link3 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Medidas IntexC";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=MedidasInC-" + DateTime.Now.ToShortDateString() + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            //throw;
        }
    }


    protected void cmdExportDD_Click(object sender, EventArgs e)
    {
        try
        {
            exportDD.GridViewID = "grdmiDD";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = exportDD;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link3 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Detalle Medidas";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=DetalleMedidas-" + DateTime.Now.ToShortDateString() + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void grdmi_DataBinding(object sender, EventArgs e)
    {
        grdmi.DataSource = Session["intexm"];
    }

    protected void grdmiDD_DataBinding(object sender, EventArgs e)
    {
        grdmiDD.DataSource = Session["intexmEMB_DD"];
    }

    protected void grdmi_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdmi.DataBind();
    }

    protected void grdmiDD_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdmiDD.DataBind();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            string us = Page.User.Identity.Name.ToString().ToLower().Trim();

            if (us.Equals("repintex"))
            {
                string sqlb = " select a.POrder Corte, COUNT(id) 'Total_Grande_Grande' from (select p.POrder, mp.id, mp.estado from tbMedidasEmbarqueP mp join tbMedidasEmbarqueAL mi on mi.id = mp.id"
                        + " join POrder p on p.Id_Order = mi.idPorder where mp.estado = 'Big' and convert(date, mi.fechaRegistro) between '" + ASPxDateEdit1.Text + "' and '" + ASPxDateEdit2.Text + "' group by p.POrder, mp.id, mp.estado"
                        + " having COUNT(mp.id) > 1) a group by a.POrder";

                DataTable dtb = DataAccess.Get_DataTable(sqlb);

                string sqls = " select a.POrder Corte, COUNT(id) 'Total_Pequeño_Pequeño' from (select p.POrder, mp.id, mp.estado from tbMedidasEmbarqueP mp join tbMedidasEmbarqueAL mi on mi.id = mp.id"
                            + " join POrder p on p.Id_Order = mi.idPorder where mp.estado = 'Small' and convert(date, mi.fechaRegistro) between '" + ASPxDateEdit1.Text + "' and '" + ASPxDateEdit2.Text + "'"
                            + " group by p.POrder, mp.id, mp.estado having COUNT(mp.id) > 1) a group by a.POrder";

                DataTable dts = DataAccess.Get_DataTable(sqls);

                grdpiezas.DataSource = dtb;
                grdpiezas.DataBind();

                grdpiezas2.DataSource = dts;
                grdpiezas2.DataBind();
            }
            else
            {
                grdpiezas.DataSource = null;
                grdpiezas.DataBind();

                grdpiezas2.DataSource = null;
                grdpiezas2.DataBind();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        grdpiezas.DataSource = null;
        grdpiezas.DataBind();

        grdpiezas2.DataSource = null;
        grdpiezas2.DataBind();

        ASPxDateEdit1.Text = DateTime.Now.ToShortDateString();
        ASPxDateEdit2.Text = DateTime.Now.ToShortDateString();
    }
}