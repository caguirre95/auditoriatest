﻿using DevExpress.Export;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;

public partial class admin_AuditoriaBultoABultoC21 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SideBySideBarSeriesLabel label = (SideBySideBarSeriesLabel)WebChartControl1.SeriesTemplate.Label;

        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();

            visible();
            cargacombo();
            cargagridestilos();

            Session["rep"] = null;
            Session["rep1"] = null;
            Session["rep2"] = null;
            Session["rep3"] = null;


            chbShowLabels.Checked = WebChartControl1.SeriesTemplate.LabelsVisibility == DefaultBoolean.True;
            spnLabelIndent.Value = label.Indent;

        }

        bool indentEnabled = chbShowLabels.Checked;
        spnLabelIndent.ClientEnabled = indentEnabled;
        WebChartControl1.CrosshairOptions.CrosshairLabelMode = CrosshairLabelMode.ShowForNearestSeries;

    }

    void PerformShowLabelsAction()
    {
        WebChartControl1.SeriesTemplate.LabelsVisibility = chbShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False;
        WebChartControl1.CrosshairEnabled = chbShowLabels.Checked ? DefaultBoolean.False : DefaultBoolean.True;
    }

    void PerformLabelIndentAction()
    {
        ((BarSeriesLabel)WebChartControl1.SeriesTemplate.Label).Indent = Convert.ToInt32(spnLabelIndent.Value);
    }

    protected void WebChartControl1_CustomCallback(object sender, CustomCallbackEventArgs e)
    {
        if (e.Parameter == "ShowLabels")
            PerformShowLabelsAction();
        else if (e.Parameter == "LabelIndent")
            PerformLabelIndentAction();

        WebChartControl1.DataBind();
    }

    void visible()
    {
        //  ASPxGridView2.Visible = false;
        btnexport1.Visible = false;

        ASPxGridView1.Visible = true;
        btnexport.Visible = true;

    }

    void nvisible()
    {
        //   ASPxGridView2.Visible = true;
        btnexport1.Visible = true;

        ASPxGridView1.Visible = false;
        btnexport.Visible = false;
    }

    void cargacombo()
    {
        string sqline = "select id_linea,numero from Linea l where l.numero not like '%-%' and l.numero not like '%A%' order by CONVERT(float,numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT [id_planta],[descripcion] FROM[Auditoria].[dbo].[Planta]";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
        DropDownListPlant.Items.Insert(5, "All Plant");

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    if (txtPorder.Text != string.Empty)
        //    {
        //        string queryBundle = "Select distinct b.Size from POrder p join Bundle b on p.Id_Order = b.Id_Order where p.POrder = '" + txtPorder.Text + "'"; //Select b.Id_Bundle,b.Size,b.Quantity,b.NSeq from POrder p join Bundle b on p.Id_Order = b.Id_Order where p.Id_Order = " + hfidPorder.Value + " order by NSeq asc";
        //        DataTable dt_Bundle = DataAccess.Get_DataTable(queryBundle);

        //        DropDownList2.DataSource = dt_Bundle;
        //        DropDownList2.DataTextField = "Size";
        //        DropDownList2.DataValueField = "Size";
        //        DropDownList2.DataBind();
        //        DropDownList2.Items.Insert(0, "Select...");

        //    }
        //}
        //catch (Exception)
        //{
        //    throw;
        //}
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {

        visible();

        ASPxGridView1.DataBind();


    }

    void cargagridestilos()
    {
        string sqline = " select s.Style"
                       + " from Style s"
                       + " join POrder p on s.Id_Style = p.Id_Style"
                       + " join Bundle b on p.Id_Order = b.Id_Order"
                       + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                       + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                       + " pm.rol='bultoabultoC21' and datepart(wk, mb.fecha) = DATEPART(WK, GETDATE()) and DATEPART(YY,mb.fecha)=DATEPART(YY,GETDATE())"
                       + " group by s.Style";

        DataTable dtEstilos = DataAccess.Get_DataTable(sqline);

        grvestilos.DataSource = dtEstilos;
        grvestilos.DataBind();
    }

    DataTable funcionCarga()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);

        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string resp = RadioButtonList1.SelectedValue;

        string query = "";

        switch (resp)
        {
            case "1"://cut
                if (txtPorder.Text != string.Empty)
                {

                    query = " select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,m.valor"
                    + " from Bundle b"
                    + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                    + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                    + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                    + " join tblMedidas m on mb.idMedida = m.idMedida"
                    + " join POrder p on b.Id_Order = p.Id_Order"
                    + " where p.POrder='" + txtPorder.Text + "'  and pm.rol='bultoabultoC21'"
                    + " group by m.Medida,pmd.NombreMedida,m.valor"
                    + " order by m.valor desc";


                }
                else
                {
                    resp = "fin";
                }
                break;
            case "2"://style
                if (txtstyle.Text != string.Empty)
                {

                    query = "select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,m.valor"
                     + " from Bundle b"
                     + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                     + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                     + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                     + " join tblMedidas m on mb.idMedida = m.idMedida"
                     + " join tblUserMedida us on mb.insp=us.userms"
                     + " join POrder p on b.Id_Order = p.Id_Order"
                     + " join Style s on p.Id_Style = s.Id_Style"
                     + " where s.Style = '" + txtstyle.Text + "' "
                     + " and pm.rol='bultoabultoC21' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                     + " group by m.Medida,pmd.NombreMedida,m.valor"
                     + " order by m.valor desc";
                    // }

                }
                else
                {
                    resp = "fin";
                }
                break;
            case "3"://line
                if (DropDownListLine.SelectedItem.Text != "Select...")
                {

                    query = " select m.Medida,COUNT(m.Medida) as total ,pmd.NombreMedida,m.valor"
                       + " from Bundle b"
                       + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                       + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                       + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                       + " join tblMedidas m on mb.idMedida = m.idMedida"
                       + " join tblUserMedida us on mb.insp=us.userms"
                       + " join POrder p on b.Id_Order=p.Id_Order"
                       + " where p.Id_Linea2= " + DropDownListLine.SelectedValue
                       + " and pm.rol='bultoabultoC21' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                       + " group by m.Medida,pmd.NombreMedida,m.valor"
                       + " order by m.valor desc";
                    //  }

                }
                else
                {
                    resp = "fin";
                }
                break;
            case "4"://plant
                if (DropDownListPlant.SelectedItem.Text == "Select...")
                {
                    resp = "fin";
                }
                else if (DropDownListPlant.SelectedItem.Text == "All Plant")
                {
                    query = " select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,m.valor"
                             + " from Bundle b"
                             + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                             + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                             + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                             + " join tblMedidas m on mb.idMedida = m.idMedida"
                             + " where  pm.rol='bultoabultoC21' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                             + " group by m.Medida,pmd.NombreMedida,m.valor"
                             + " order by m.valor desc";
                }
                else
                {

                    query = " select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,m.valor"
                      + " from Bundle b"
                      + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                      + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                      + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                      + " join tblMedidas m on mb.idMedida = m.idMedida"
                      + " join tblUserMedida us on mb.insp=us.userms"
                      + " join Linea l on us.linea = l.id_linea"
                      + " where l.id_planta =" + DropDownListPlant.SelectedValue + " and pm.rol='bultoabultoC21' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                      + " group by m.Medida,pmd.NombreMedida,m.valor"
                      + " order by m.valor desc";

                }
                break;
            case "":
                resp = "fin";
                break;
            default:
                resp = "fin";
                break;
        }

        DataTable dt = new DataTable();
        if (resp != "fin")
        {
            //carga comentario del corte
            if (txtPorder.Text != "")
            {
                DataTable dt2 = new DataTable();
                dt2 = DataAccess.Get_DataTable("select p.comments from POrder p where p.POrder='" + txtPorder.Text + "'");

                foreach (DataRow item in dt2.Rows)
                {
                    memo.Text = item[0].ToString();
                }

            }

            //Extrae la informacion de medidas del cut
            dt = new DataTable();
            dt = DataAccess.Get_DataTable(query);

            return dt;
        }
        else
        {
            //debe seleccionar una opcion
            dt = null;
            return dt;
        }

    }

    protected void ASPxGridView1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView1.DataBind();
    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        DataTable dt_seccion = new DataTable();

        dt_seccion.Columns.Add("PUNTOS_MEDIDA", typeof(string));

        dt_seccion.Columns.Add("ACROSS_SHOULDER", typeof(int));
        dt_seccion.Columns.Add("%ACROSS_SHOULDER", typeof(string));

        dt_seccion.Columns.Add("CHEST", typeof(int));
        dt_seccion.Columns.Add("%CHEST", typeof(string));

        dt_seccion.Columns.Add("SWEEP_RELAXED", typeof(int));
        dt_seccion.Columns.Add("%SWEEP_RELAXED", typeof(string));

        dt_seccion.Columns.Add("HPS_LENGHT", typeof(int));
        dt_seccion.Columns.Add("%HPS_LENGHT", typeof(string));

        dt_seccion.Columns.Add("CENTER_BACK_LENGHT_(Regular)", typeof(int));
        dt_seccion.Columns.Add("%CENTER_BACK_LENGHT_(Regular)", typeof(string));

        dt_seccion.Columns.Add("CENTER_BACK_LENGHT_(Tall)", typeof(int));
        dt_seccion.Columns.Add("%CENTER_BACK_LENGHT_(Tall)", typeof(string));

        dt_seccion.Columns.Add("SLEEVE_LENGHT_(Regular)", typeof(int));
        dt_seccion.Columns.Add("%SLEEVE_LENGHT_(Regular)", typeof(string));

        dt_seccion.Columns.Add("SLEEVE_LENGHT_(Tall)", typeof(int));
        dt_seccion.Columns.Add("%SLEEVE_LENGHT_(Tall)", typeof(string));
        /**/
        dt_seccion.Columns.Add("HOOD_WIDTH_X_FROM_CROWN", typeof(int));
        dt_seccion.Columns.Add("%HOOD_WIDTH_X_FROM_CROWN", typeof(string));

        dt_seccion.Columns.Add("SLEEVE_CUFF_HEIGHT_RELAXED_AT_EDGE_SWEATS", typeof(int));
        dt_seccion.Columns.Add("%SLEEVE_CUFF_HEIGHT_RELAXED_AT_EDGE_SWEATS", typeof(string));

        dt_seccion.Columns.Add("HOOD_HEIGHT_AT_OPENING_EXTENDED", typeof(int));
        dt_seccion.Columns.Add("%HOOD_HEIGHT_AT_OPENING_EXTENDED", typeof(string));


        #region region
        DataTable dt = new DataTable();

        string queryM = "select medida from tblMedidas order by valor desc";

        dt = DataAccess.Get_DataTable(queryM);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            DataRow row2 = dt_seccion.NewRow();

            row2[0] = dt.Rows[i][0];
            dt_seccion.Rows.Add(row2);

        }

        dt = new DataTable();
        dt = funcionCarga();

        int n1 = 0, n2 = 0, n3 = 0, n4 = 0, n5 = 0, n6 = 0, n7 = 0, n8 = 0, n9 = 0, n10 = 0, n11 = 0;

        List<listatotal> list = new List<listatotal>();
        listatotal obj = new listatotal();
        listatotal obj1 = new listatotal();
        listatotal obj2 = new listatotal();

        List<listatotal> list3_4 = new List<listatotal>();
        listatotal obj3_4 = new listatotal();
        listatotal obj3_41 = new listatotal();
        listatotal obj3_42 = new listatotal();

        List<listatotal> list3_8 = new List<listatotal>();
        listatotal obj3_8 = new listatotal();
        listatotal obj3_81 = new listatotal();
        listatotal obj3_82 = new listatotal();

        List<listatotal> list1_2 = new List<listatotal>();
        listatotal obj1_2 = new listatotal();
        listatotal obj1_21 = new listatotal();
        listatotal obj1_22 = new listatotal();

        obj.r = "Over 1''";
        obj1.r = "In Speck";
        obj2.r = "Below -1'' ";

        obj3_4.r = "Over 3/4''";
        obj3_41.r = "In Speck";
        obj3_42.r = "Below -3/4''";

        obj3_8.r = "Over 3/8''";
        obj3_81.r = "In Speck";
        obj3_82.r = "Below -3/8''";

        obj1_2.r = "Over 1/2''";
        obj1_21.r = "In Speck";
        obj1_22.r = "Below -1/2''";
        #endregion


        if (dt != null)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //var a = dt.Rows[i][2];
                if (dt.Rows[i][2].Equals("ACROSS SHOULDER"))
                {
                    n1 = n1 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalA = obj.totalA + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalA = obj1.totalA + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalA = obj2.totalA + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalA = obj3_4.totalA + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalA = obj3_41.totalA + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalA = obj3_42.totalA + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("CHEST"))
                {
                    n2 = n2 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalC = obj.totalC + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalC = obj1.totalC + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalC = obj2.totalC + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalC = obj3_4.totalC + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalC = obj3_41.totalC + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalC = obj3_42.totalC + Convert.ToInt32(dt.Rows[i][1]);
                    }


                }
                else if (dt.Rows[i][2].Equals("SWEEP RELAXED"))
                {
                    n3 = n3 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalSL = obj.totalSL + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalSL = obj1.totalSL + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalSL = obj2.totalSL + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalSL = obj3_4.totalSL + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalSL = obj3_41.totalSL + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalSL = obj3_42.totalSL + Convert.ToInt32(dt.Rows[i][1]);
                    }


                }
                else if (dt.Rows[i][2].Equals("HPS LENGHT"))
                {
                    n4 = n4 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalCB = obj.totalCB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalCB = obj1.totalCB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalCB = obj2.totalCB + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalCB = obj3_4.totalCB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalCB = obj3_41.totalCB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalCB = obj3_42.totalCB + Convert.ToInt32(dt.Rows[i][1]);
                    }


                }
                else if (dt.Rows[i][2].Equals("CENTER BACK LENGHT (Regular)"))
                {
                    n5 = n5 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalN = obj.totalN + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalN = obj1.totalN + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalN = obj2.totalN + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalN = obj3_4.totalN + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalN = obj3_41.totalN + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalN = obj3_42.totalN + Convert.ToInt32(dt.Rows[i][1]);
                    }


                }
                else if (dt.Rows[i][2].Equals("CENTER BACK LENGHT (Tall)"))
                {
                    n6 = n6 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalNT = obj.totalNT + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalNT = obj1.totalNT + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalNT = obj2.totalNT + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalNT = obj3_4.totalNT + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalNT = obj3_41.totalNT + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalNT = obj3_42.totalNT + Convert.ToInt32(dt.Rows[i][1]);
                    }


                }
                else if (dt.Rows[i][2].Equals("SLEEVE LENGTH (Regular)"))
                {
                    n7 = n7 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalSR = obj.totalSR + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalSR = obj1.totalSR + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalSR = obj2.totalSR + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalSR = obj3_4.totalSR + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalSR = obj3_41.totalSR + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalSR = obj3_42.totalSR + Convert.ToInt32(dt.Rows[i][1]);
                    }


                }
                else if (dt.Rows[i][2].Equals("SLEEVE LENGTH (Tall)"))
                {
                    n8 = n8 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalST = obj.totalST + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalST = obj1.totalST + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalST = obj2.totalST + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalST = obj3_4.totalST + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalST = obj3_41.totalST + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalST = obj3_42.totalST + Convert.ToInt32(dt.Rows[i][1]);
                    }


                }//
                else if (dt.Rows[i][2].Equals("HOOD WIDTH X” FROM CROWN"))
                {
                    n9 = n9 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalHH = obj.totalHH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalHH = obj1.totalHH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalHH = obj2.totalHH + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalHH = obj3_4.totalHH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalHH = obj3_41.totalHH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalHH = obj3_42.totalHH + Convert.ToInt32(dt.Rows[i][1]);
                    }


                }//
                else if (dt.Rows[i][2].Equals("SLEEVE CUFF HEIGHT- RELAXED AT EDGE—SWEATS"))
                {
                    n10 = n10 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalSH = obj.totalSH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalSH = obj1.totalSH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalSH = obj2.totalSH + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalSH = obj3_4.totalSH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalSH = obj3_41.totalSH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalSH = obj3_42.totalSH + Convert.ToInt32(dt.Rows[i][1]);
                    }


                }//
                else if (dt.Rows[i][2].Equals("HOOD HEIGHT AT OPENING-EXTENDED"))
                {
                    n11 = n11 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalHO = obj.totalHO + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalHO = obj1.totalHO + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalHO = obj2.totalHO + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalHO = obj3_4.totalHO + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalHO = obj3_41.totalHO + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalHO = obj3_42.totalHO + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }


            }

            obj.procenA = ((Convert.ToSingle(obj.totalA) / n1) * 100).ToString("0.00") + "% ";
            obj1.procenA = ((Convert.ToSingle(obj1.totalA) / n1) * 100).ToString("0.00") + "% ";
            obj2.procenA = ((Convert.ToSingle(obj2.totalA) / n1) * 100).ToString("0.00") + "% ";

            obj.procenC = ((Convert.ToSingle(obj.totalC) / n2) * 100).ToString("0.00") + "% ";
            obj1.procenC = ((Convert.ToSingle(obj1.totalC) / n2) * 100).ToString("0.00") + "% ";
            obj2.procenC = ((Convert.ToSingle(obj2.totalC) / n2) * 100).ToString("0.00") + "% ";

            obj.procenSL = ((Convert.ToSingle(obj.totalSL) / n3) * 100).ToString("0.00") + "% ";
            obj1.procenSL = ((Convert.ToSingle(obj1.totalSL) / n3) * 100).ToString("0.00") + "% ";
            obj2.procenSL = ((Convert.ToSingle(obj2.totalSL) / n3) * 100).ToString("0.00") + "% ";

            obj.procenCB = ((Convert.ToSingle(obj.totalCB) / n4) * 100).ToString("0.00") + "%";
            obj1.procenCB = ((Convert.ToSingle(obj1.totalCB) / n4) * 100).ToString("0.00") + "% ";
            obj2.procenCB = ((Convert.ToSingle(obj2.totalCB) / n4) * 100).ToString("0.00") + "% ";

            obj.procenN = ((Convert.ToSingle(obj.totalN) / n5) * 100).ToString("0.00") + "% ";
            obj1.procenN = ((Convert.ToSingle(obj1.totalN) / n5) * 100).ToString("0.00") + "% ";
            obj2.procenN = ((Convert.ToSingle(obj2.totalN) / n5) * 100).ToString("0.00") + "% ";

            obj.procenNT = ((Convert.ToSingle(obj.totalNT) / n6) * 100).ToString("0.00") + "% ";
            obj1.procenNT = ((Convert.ToSingle(obj1.totalNT) / n6) * 100).ToString("0.00") + "% ";
            obj2.procenNT = ((Convert.ToSingle(obj2.totalNT) / n6) * 100).ToString("0.00") + "% ";

            obj.procenSR = ((Convert.ToSingle(obj.totalSR) / n7) * 100).ToString("0.00") + "% ";
            obj1.procenSR = ((Convert.ToSingle(obj1.totalSR) / n7) * 100).ToString("0.00") + "% ";
            obj2.procenSR = ((Convert.ToSingle(obj2.totalSR) / n7) * 100).ToString("0.00") + "% ";

            obj.procenST = ((Convert.ToSingle(obj.totalST) / n8) * 100).ToString("0.00") + "% ";
            obj1.procenST = ((Convert.ToSingle(obj1.totalST) / n8) * 100).ToString("0.00") + "% ";
            obj2.procenST = ((Convert.ToSingle(obj2.totalST) / n8) * 100).ToString("0.00") + "% ";

            /**/
            obj.procenHH = ((Convert.ToSingle(obj.totalHH) / n9) * 100).ToString("0.00") + "% ";
            obj1.procenHH = ((Convert.ToSingle(obj1.totalHH) / n9) * 100).ToString("0.00") + "% ";
            obj2.procenHH = ((Convert.ToSingle(obj2.totalHH) / n9) * 100).ToString("0.00") + "% ";

            obj.procenSH = ((Convert.ToSingle(obj.totalSH) / n10) * 100).ToString("0.00") + "% ";
            obj1.procenSH = ((Convert.ToSingle(obj1.totalSH) / n10) * 100).ToString("0.00") + "% ";
            obj2.procenSH = ((Convert.ToSingle(obj2.totalSH) / n10) * 100).ToString("0.00") + "% ";

            obj.procenHO = ((Convert.ToSingle(obj.totalHO) / n11) * 100).ToString("0.00") + "% ";
            obj1.procenHO = ((Convert.ToSingle(obj1.totalHO) / n11) * 100).ToString("0.00") + "% ";
            obj2.procenHO = ((Convert.ToSingle(obj2.totalHO) / n11) * 100).ToString("0.00") + "% ";



            list.Add(obj);

            list.Add(obj1);

            list.Add(obj2);


            obj3_4.procenA = ((Convert.ToSingle(obj3_4.totalA) / n1) * 100).ToString("0.00") + "% ";
            obj3_41.procenA = ((Convert.ToSingle(obj3_41.totalA) / n1) * 100).ToString("0.00") + "% ";
            obj3_42.procenA = ((Convert.ToSingle(obj3_42.totalA) / n1) * 100).ToString("0.00") + "% ";

            obj3_4.procenC = ((Convert.ToSingle(obj3_4.totalC) / n2) * 100).ToString("0.00") + "% ";
            obj3_41.procenC = ((Convert.ToSingle(obj3_41.totalC) / n2) * 100).ToString("0.00") + "% ";
            obj3_42.procenC = ((Convert.ToSingle(obj3_42.totalC) / n2) * 100).ToString("0.00") + "% ";

            obj3_4.procenSL = ((Convert.ToSingle(obj3_4.totalSL) / n3) * 100).ToString("0.00") + "% ";
            obj3_41.procenSL = ((Convert.ToSingle(obj3_41.totalSL) / n3) * 100).ToString("0.00") + "% ";
            obj3_42.procenSL = ((Convert.ToSingle(obj3_42.totalSL) / n3) * 100).ToString("0.00") + "% ";

            obj3_4.procenCB = ((Convert.ToSingle(obj3_4.totalCB) / n4) * 100).ToString("0.00") + "% ";
            obj3_41.procenCB = ((Convert.ToSingle(obj3_41.totalCB) / n4) * 100).ToString("0.00") + "% ";
            obj3_42.procenCB = ((Convert.ToSingle(obj3_42.totalCB) / n4) * 100).ToString("0.00") + "% ";

            obj3_4.procenN = ((Convert.ToSingle(obj3_4.totalN) / n5) * 100).ToString("0.00") + "% ";
            obj3_41.procenN = ((Convert.ToSingle(obj3_41.totalN) / n5) * 100).ToString("0.00") + "% ";
            obj3_42.procenN = ((Convert.ToSingle(obj3_42.totalN) / n5) * 100).ToString("0.00") + "% ";

            obj3_4.procenNT = ((Convert.ToSingle(obj3_4.totalNT) / n6) * 100).ToString("0.00") + "% ";
            obj3_41.procenNT = ((Convert.ToSingle(obj3_41.totalNT) / n6) * 100).ToString("0.00") + "% ";
            obj3_42.procenNT = ((Convert.ToSingle(obj3_42.totalNT) / n6) * 100).ToString("0.00") + "% ";

            obj3_4.procenSR = ((Convert.ToSingle(obj3_4.totalSR) / n7) * 100).ToString("0.00") + "% ";
            obj3_41.procenSR = ((Convert.ToSingle(obj3_41.totalSR) / n7) * 100).ToString("0.00") + "% ";
            obj3_42.procenSR = ((Convert.ToSingle(obj3_42.totalSR) / n7) * 100).ToString("0.00") + "% ";

            obj3_4.procenST = ((Convert.ToSingle(obj3_4.totalST) / n8) * 100).ToString("0.00") + "% ";
            obj3_41.procenST = ((Convert.ToSingle(obj3_41.totalST) / n8) * 100).ToString("0.00") + "% ";
            obj3_42.procenST = ((Convert.ToSingle(obj3_42.totalST) / n8) * 100).ToString("0.00") + "% ";

            /**/
            obj3_4.procenHH = ((Convert.ToSingle(obj3_4.totalHH) / n9) * 100).ToString("0.00") + "% ";
            obj3_41.procenHH = ((Convert.ToSingle(obj3_41.totalHH) / n9) * 100).ToString("0.00") + "% ";
            obj3_42.procenHH = ((Convert.ToSingle(obj3_42.totalHH) / n9) * 100).ToString("0.00") + "% ";

            obj3_4.procenSH = ((Convert.ToSingle(obj3_4.totalSH) / n10) * 100).ToString("0.00") + "% ";
            obj3_41.procenSH = ((Convert.ToSingle(obj3_41.totalSH) / n10) * 100).ToString("0.00") + "% ";
            obj3_42.procenSH = ((Convert.ToSingle(obj3_42.totalSH) / n10) * 100).ToString("0.00") + "% ";

            obj3_4.procenHO = ((Convert.ToSingle(obj3_4.totalHO) / n11) * 100).ToString("0.00") + "% ";
            obj3_41.procenHO = ((Convert.ToSingle(obj3_41.totalHO) / n11) * 100).ToString("0.00") + "% ";
            obj3_42.procenHO = ((Convert.ToSingle(obj3_42.totalHO) / n11) * 100).ToString("0.00") + "% ";


            list3_4.Add(obj3_4);
            list3_4.Add(obj3_41);
            list3_4.Add(obj3_42);

            //list1_2.Add(obj1_2);
            //list1_2.Add(obj1_21);
            //list1_2.Add(obj1_22);

            //list3_8.Add(obj3_8);
            //list3_8.Add(obj3_81);
            //list3_8.Add(obj3_82);


            //carga grid Pincipal con Porcentajes
            for (int i = 0; i < dt_seccion.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (dt.Rows[j][2].Equals("ACROSS SHOULDER"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][1] = dt.Rows[j][1];
                            dt_seccion.Rows[i][2] = ((float.Parse(dt.Rows[j][1].ToString()) / n1) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("CHEST"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][3] = dt.Rows[j][1];
                            dt_seccion.Rows[i][4] = ((float.Parse(dt.Rows[j][1].ToString()) / n2) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("SWEEP RELAXED"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][5] = dt.Rows[j][1];
                            dt_seccion.Rows[i][6] = ((float.Parse(dt.Rows[j][1].ToString()) / n3) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("HPS LENGHT"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][7] = dt.Rows[j][1];
                            dt_seccion.Rows[i][8] = ((float.Parse(dt.Rows[j][1].ToString()) / n4) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("CENTER BACK LENGHT (Regular)"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][9] = dt.Rows[j][1];
                            dt_seccion.Rows[i][10] = ((float.Parse(dt.Rows[j][1].ToString()) / n5) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("CENTER BACK LENGHT (Tall)"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][11] = dt.Rows[j][1];
                            dt_seccion.Rows[i][12] = ((float.Parse(dt.Rows[j][1].ToString()) / n6) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("SLEEVE LENGTH (Regular)"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][13] = dt.Rows[j][1];
                            dt_seccion.Rows[i][14] = ((float.Parse(dt.Rows[j][1].ToString()) / n7) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("SLEEVE LENGTH (Tall)"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][15] = dt.Rows[j][1];
                            dt_seccion.Rows[i][16] = ((float.Parse(dt.Rows[j][1].ToString()) / n8) * 100).ToString("0.00") + "%";
                        }
                    }//
                    else if (dt.Rows[j][2].Equals("HOOD WIDTH X” FROM CROWN"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][17] = dt.Rows[j][1];
                            dt_seccion.Rows[i][18] = ((float.Parse(dt.Rows[j][1].ToString()) / n9) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("SLEEVE CUFF HEIGHT- RELAXED AT EDGE—SWEATS"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][19] = dt.Rows[j][1];
                            dt_seccion.Rows[i][20] = ((float.Parse(dt.Rows[j][1].ToString()) / n10) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("HOOD HEIGHT AT OPENING-EXTENDED"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][21] = dt.Rows[j][1];
                            dt_seccion.Rows[i][22] = ((float.Parse(dt.Rows[j][1].ToString()) / n11) * 100).ToString("0.00") + "%";
                        }
                    }
                }
            }


        }

        WebChartControl1.DataBind();

        ASPxGridView1.DataSource = dt_seccion;

        Session["rep"] = list;
        // Session["rep1"] = list1_2;
        Session["rep2"] = list3_4;
        // Session["rep3"] = list3_8;

        ASPxGridView3.DataBind();
        ASPxGridView4.DataBind();
        //ASPxGridView2.DataBind();
        //ASPxGridView5.DataBind();

    }

    protected void btnexport_Click(object sender, EventArgs e)
    {

        try
        {
            ASPxGridView1.DataBind();

            ASPxGridViewExporter1.GridViewID = "ASPxGridView1";
            ASPxGridViewExporter2.GridViewID = "ASPxGridView3";
            ASPxGridViewExporter3.GridViewID = "ASPxGridView4";
            //  ASPxGridViewExporter4.GridViewID = "ASPxGridView2";
            //  ASPxGridViewExporter5.GridViewID = "ASPxGridView5";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;
            link1.CreateMarginalHeaderArea += new CreateAreaEventHandler(Link_CreateMarginalHeaderArea);

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link2.Component = ASPxGridViewExporter2;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = ASPxGridViewExporter3;

            //DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link4 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            //link4.Component = ASPxGridViewExporter4;

            //DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link5 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            //link5.Component = ASPxGridViewExporter5;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1, link2, link3 });

            compositeLink.CreatePageForEachLink();
            //compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Report_BeforeWash";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=ReportMeasurement.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            throw;
        }

    }

    private void Link_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
    {
        string resp = RadioButtonList1.SelectedValue;
        string mess = "";
        if (resp == "1")
        {
            mess = txtPorder.Text + "\n" + memo.Text;
        }
        else
        {
            mess = memo.Text;
        }

        DevExpress.XtraPrinting.TextBrick brick;
        brick = e.Graph.DrawString(mess, Color.Navy, new RectangleF(0, 0, 700, 50), DevExpress.XtraPrinting.BorderSide.None);
        brick.Font = new Font("Times New Roman", 12);
        brick.StringFormat = new DevExpress.XtraPrinting.BrickStringFormat(StringAlignment.Center);

        // string reps="31313131311sdsfsdf";
        //PageInfoBrick brick = e.Graph.DrawPageInfo(PageInfo., "", Color.DarkBlue,new RectangleF(0, 0, 100, 20), BorderSide.None);

        //brick.LineAlignment = BrickAlignment.Center;

        //brick.Alignment = BrickAlignment.Center;

        //brick.AutoWidth = true;

    }

    protected void btnexport1_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridViewExporter1.GridViewID = "ASPxGridView2";
            ASPxGridViewExporter1.FileName = "Report " + DateTime.Now.ToShortDateString();

            ASPxGridViewExporter1.WriteXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });

        }
        catch (Exception)
        {

            throw;
        }
    }

    public class listatotal
    {
        public string r { get; set; }
        public int totalA { get; set; }
        public string procenA { get; set; }
        public int totalC { get; set; }
        public string procenC { get; set; }
        public int totalSL { get; set; }
        public string procenSL { get; set; }
        public int totalCB { get; set; }
        public string procenCB { get; set; }
        public int totalN { get; set; }
        public string procenN { get; set; }
        public int totalST { get; set; }
        public string procenST { get; set; }
        public int totalNT { get; set; }
        public string procenNT { get; set; }
        public int totalSR { get; set; }
        public string procenSR { get; set; }
        /**/
        public int totalHH { get; set; }
        public string procenHH { get; set; }
        public int totalSH { get; set; }
        public string procenSH { get; set; }
        public int totalHO { get; set; }
        public string procenHO { get; set; }

    }

    protected void ASPxGridView3_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView3.DataSource = (List<listatotal>)Session["rep"];
    }

    protected void ASPxGridView3_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView3.DataBind();
    }

    protected void ASPxGridView4_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView4.DataSource = (List<listatotal>)Session["rep2"];
    }

    protected void ASPxGridView4_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView4.DataBind();
    }

    protected void ASPxGridView2_DataBinding(object sender, EventArgs e)
    {
        // ASPxGridView2.DataSource = (List<listatotal>)Session["rep3"];
    }

    protected void ASPxGridView2_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        //  ASPxGridView2.DataBind();
    }

    protected void ASPxGridView5_DataBinding(object sender, EventArgs e)
    {
        // ASPxGridView5.DataSource = (List<listatotal>)Session["rep1"];
    }

    protected void ASPxGridView5_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        // ASPxGridView5.DataBind();
    }

    protected void WebChartControl1_DataBinding(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();

        dt = funcionCarga2();

        WebChartControl1.DataSource = dt;

    }

    DataTable funcionCarga2()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);

        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string resp = RadioButtonList1.SelectedValue;

        string query = "";

        switch (resp)
        {
            case "1"://cut
                if (txtPorder.Text != string.Empty)
                {

                    query = " select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,Round(m.valor, 1,1) as valor"
                    + " from Bundle b"
                    + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                    + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                    + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                    + " join tblMedidas m on mb.idMedida = m.idMedida"
                    + " join POrder p on b.Id_Order = p.Id_Order"
                    + " where p.POrder='" + txtPorder.Text + "'  and pm.rol='bultoabultoC21'"
                    + " group by m.Medida,pmd.NombreMedida,m.valor"
                    + " order by m.valor desc";


                }
                else
                {
                    resp = "fin";
                }
                break;
            case "2"://style
                if (txtstyle.Text != string.Empty)
                {

                    query = "select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,Round(m.valor, 1,1) as valor"
                     + " from Bundle b"
                     + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                     + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                     + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                     + " join tblMedidas m on mb.idMedida = m.idMedida"
                     + " join tblUserMedida us on mb.insp=us.userms"
                     + " join POrder p on b.Id_Order = p.Id_Order"
                     + " join Style s on p.Id_Style = s.Id_Style"
                     + " where s.Style='" + txtstyle.Text + "'"
                     + " and pm.rol='bultoabultoC21' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                     + " group by m.Medida,pmd.NombreMedida,m.valor"
                     + " order by m.valor desc";

                }
                else
                {
                    resp = "fin";
                }
                break;
            case "3"://line
                if (DropDownListLine.SelectedItem.Text != "Select...")
                {

                    query = " select m.Medida,COUNT(m.Medida) as total ,pmd.NombreMedida,Round(m.valor, 1,1) as valor"
                       + " from Bundle b"
                       + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                       + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                       + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                       + " join tblMedidas m on mb.idMedida = m.idMedida"
                       + " join tblUserMedida us on mb.insp=us.userms"
                       + " join POrder p on b.Id_Order=p.Id_Order"
                       + " where p.Id_Linea2= " + DropDownListLine.SelectedValue
                       + " and pm.rol='bultoabultoC21' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                       + " group by m.Medida,pmd.NombreMedida,m.valor"
                       + " order by m.valor desc";


                }
                else
                {
                    resp = "fin";
                }
                break;
            case "4"://plant
                if (DropDownListPlant.SelectedItem.Text == "Select...")
                {
                    resp = "fin";
                }
                else if (DropDownListPlant.SelectedItem.Text == "All Plant")
                {
                    query = " select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,Round(m.valor, 1,1) as valor"
                             + " from Bundle b"
                             + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                             + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                             + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                             + " join tblMedidas m on mb.idMedida = m.idMedida"
                             + " where  pm.rol='bultoabultoC21' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                             + " group by m.Medida,pmd.NombreMedida,m.valor"
                             + " order by m.valor desc";
                }
                else
                {

                    query = " select m.Medida,COUNT(m.Medida)as total ,pmd.NombreMedida,Round(m.valor, 1,1) as valor"
                      + " from Bundle b"
                      + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                      + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                      + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                      + " join tblMedidas m on mb.idMedida = m.idMedida"
                      + " join tblUserMedida us on mb.insp=us.userms"
                      + " join Linea l on us.linea = l.id_linea"
                      + " where l.id_planta =" + DropDownListPlant.SelectedValue + " and pm.rol='bultoabultoC21' and  CONVERT(date,mb.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                      + " group by m.Medida,pmd.NombreMedida,m.valor"
                      + " order by m.valor desc";

                }
                break;
            case "":
                resp = "fin";
                break;
            default:
                resp = "fin";
                break;
        }

        DataTable dt = new DataTable();
        if (resp != "fin")
        {
            if (txtPorder.Text != "")
            {
                DataTable dt2 = new DataTable();
                dt2 = DataAccess.Get_DataTable("select p.comments from POrder p where p.POrder='" + txtPorder.Text + "'");

                foreach (DataRow item in dt2.Rows)
                {
                    memo.Text = item[0].ToString();
                }

            }

            dt = new DataTable();
            dt = DataAccess.Get_DataTable(query);

            return dt;
        }
        else
        {
            //debe seleccionar una opcion
            dt = null;
            return dt;
        }

    }
}