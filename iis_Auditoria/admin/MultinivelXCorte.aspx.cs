﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_MultinivelXCorte : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();
            Session["dt"] = null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top 10 p.Id_Order,p.POrder from POrder p  where p.POrder like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][1].ToString();
               

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public int idorder { get; set; }
        public string porder { get; set; }
        //public string style { get; set; }
    }

    protected void grid_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grid.DataBind();
    }

    protected void grid_DataBinding(object sender, EventArgs e)
    {
        grid.DataSource = Session["dt"];
        grid.ExpandAll();
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "0" && hfidPorder.Value==string.Empty)
        {
            return;
        }

        DataTable dt = new DataTable();
        int resp = int.Parse(DropDownListProcess.SelectedValue);

        switch (resp)
        {
            case 1:

                dt = OrderDetailDA.ReportqueryMultiXCorte(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text), RadioButtonList1.SelectedValue,int.Parse(hfidPorder.Value));
                break;
            case 2:
                dt = OrderDetailDA.ReportqueryMultiIntexXcorte(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text), int.Parse(hfidPorder.Value));
                break;
            case 3:

                dt = OrderDetailDA.ReportqueryMultiMod4XCorte(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text), int.Parse(hfidPorder.Value));
                break;
            case 4:

                dt = OrderDetailDA.ReportqueryMultiEspecialIXCorte(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text), int.Parse(hfidPorder.Value));
                break;
            default:
                break;
        }


        Session["dt"] = dt;
        grid.DataBind();


    }

    protected void DropDownListProcess_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "1")
        {
            Panel1.Visible = true;
        }
        else
        {
            Panel1.Visible = false;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "0" && hfidPorder.Value == string.Empty)
        {
            return;
        }

        DataTable dt = new DataTable();
        int resp = int.Parse(DropDownListProcess.SelectedValue);

        switch (resp)
        {
            case 1:

                dt = OrderDetailDA.ReportqueryMultiXCorte(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text), RadioButtonList1.SelectedValue, int.Parse(hfidPorder.Value));
                break;
            case 2:


                dt = OrderDetailDA.ReportqueryMultiIntexXcorte(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text), int.Parse(hfidPorder.Value));
                break;
            case 3:

                dt = OrderDetailDA.ReportqueryMultiMod4XCorte(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text), int.Parse(hfidPorder.Value));
                break;
            case 4:

                dt = OrderDetailDA.ReportqueryMultiEspecialIXCorte(DateTime.Parse(txtfecha1.Text), DateTime.Parse(txtfecha2.Text), int.Parse(hfidPorder.Value));
                break;
            default:
                break;
        }


        Session["dt"] = dt;
        grid.DataBind();

        ASPxGridViewExporter1.GridViewID = "grid";

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link1.Component = ASPxGridViewExporter1;


        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link1 });

        compositeLink.CreatePageForEachLink();

        using (MemoryStream stream = new MemoryStream())
        {
            XlsxExportOptions options = new XlsxExportOptions();
            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
            options.SheetName = "ReporteDeCorteXtalla";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=Multilevel_Report_X_Corte.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }
        ps.Dispose();
    }


}