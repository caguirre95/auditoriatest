﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ReportMedidaMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Date = DateTime.Now;
            txtfecha2.Date = DateTime.Now;
            RadioButtonList1.SelectedValue = "0";
        }
    }

    protected void btnGenerar_Click(object sender, EventArgs e)
    {
        if (RadioButtonList1.SelectedValue == "0")
        {
            if (DropDownListProcess.SelectedValue == "1")
            {
                if (RadioButtonList2.SelectedIndex > -1)
                {
                    XtraReport1 rep = new XtraReport1();

                    rep.Parameters[0].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                    rep.Parameters[1].Value = txtfecha2.Date.ToString("yyyy-MM-dd");
                    rep.Parameters[2].Value = RadioButtonList2.SelectedValue;

                    ASPxWebDocumentViewer1.OpenReport(rep);
                }               
            }
            else if (DropDownListProcess.SelectedValue == "2")
            {
                XtraReport1Intex rep = new XtraReport1Intex();

                rep.Parameters[0].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                rep.Parameters[1].Value = txtfecha2.Date.ToString("yyyy-MM-dd");
               // rep.Parameters[2].Value = "inspInter";

                ASPxWebDocumentViewer1.OpenReport(rep);
            }
            else if (DropDownListProcess.SelectedValue=="3")
            {
                XtraReport1After rep = new XtraReport1After();

                rep.Parameters[0].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                rep.Parameters[1].Value = txtfecha2.Date.ToString("yyyy-MM-dd");
              //  rep.Parameters[2].Value = "inspInter";

                ASPxWebDocumentViewer1.OpenReport(rep);
            }
        }
        else
        {
            if (DropDownListProcess.SelectedValue == "1")
            {
                if (RadioButtonList2.SelectedIndex > -1)
                {
                    XtraReportXStyleMaster rep = new XtraReportXStyleMaster();

                    rep.Parameters[1].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                    rep.Parameters[2].Value = txtfecha2.Date.ToString("yyyy-MM-dd");
                    rep.Parameters[3].Value = RadioButtonList2.SelectedValue;
                    rep.Parameters[0].Value = txtstyle.Text;

                    ASPxWebDocumentViewer1.OpenReport(rep);
                }
                   
            }
            else if (DropDownListProcess.SelectedValue == "2")
            {
                XtraReportXStyleMasterInt rep = new XtraReportXStyleMasterInt();

                rep.Parameters[0].Value = txtstyle.Text;
                rep.Parameters[1].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                rep.Parameters[2].Value = txtfecha2.Date.ToString("yyyy-MM-dd");

                ASPxWebDocumentViewer1.OpenReport(rep);
            }
            else if (DropDownListProcess.SelectedValue == "3")
            {
                XtraReportXStyleMasterAf rep = new XtraReportXStyleMasterAf();

                rep.Parameters[0].Value = txtstyle.Text;
                rep.Parameters[1].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
                rep.Parameters[2].Value = txtfecha2.Date.ToString("yyyy-MM-dd");

                ASPxWebDocumentViewer1.OpenReport(rep);
            }

        }

    }

    [WebMethod]
    [ScriptMethod]
    public static List<string> GetStyle(string prefixText)
    {
        try
        {
            List<string> lista = new List<string>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = "SELECT distinct top 10  Style FROM Style s join POrder p on s.Id_Style=p.Id_Style  where Style like '%" + prefixText + "%' and p.Id_Cliente=11";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string obj;

                obj = dt.Rows[i][0].ToString();

                lista.Add(obj);

            }

            return lista;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    protected void DropDownListProcess_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "1")
        {
            Panel1.Visible = true;
        }
        else
        {
            Panel1.Visible = false;
        }
    }
}