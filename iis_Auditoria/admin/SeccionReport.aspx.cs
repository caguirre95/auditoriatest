﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SistemaAuditores.DataAccess;
using System.Data;

public partial class admin_Report3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("/adminRepDefectos/AqlOqlCorte.aspx");
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/AqlOqlCorte.aspx");
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("/adminRepDefectos/TopDefectos.aspx");
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/TopDefectos.aspx");
    }
    protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
    {
        //Response.Redirect("ROC3.aspx");
        Response.Redirect("/adminRepDefectos/TablaDefectos.aspx");
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        //Response.Redirect("ROC3.aspx");
        Response.Redirect("/adminRepDefectos/TablaDefectos.aspx");
    }

    protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
    {
        //Response.Redirect("/adminRepDefectos/OperarioDef.aspx");
        Response.Redirect("/adminRepDefectos/AuditoriaEmbDef.aspx");        
    }

    protected void LinkButton5_Click(object sender, EventArgs e)
    {
        //Response.Redirect("/adminRepDefectos/OperarioDef.aspx");
        Response.Redirect("/adminRepDefectos/AuditoriaEmbDef.aspx");        
    }

    protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("/adminRepDefectos/HistorialAudit.aspx");
    }

    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/HistorialAudit.aspx");
    }

    protected void LinkButton6_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/HistorialDefecto.aspx");
    }

    protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("/adminRepDefectos/HistorialDefecto.aspx");
    }

    protected void LinkButton7_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/HistoricoDefCorte.aspx");
    }

    protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("/adminRepDefectos/HistoricoDefCorte.aspx");
    }

    protected void ImageButton8_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("/adminRepDefectos/DashBoardEmbarque.aspx");
    }

    protected void LinkButton8_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/DashBoardEmbarque.aspx");
    }

    protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("/adminRepDefectos/ControlAuditoriasEmb.aspx");
    }

    protected void LinkButton9_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/ControlAuditoriasEmb.aspx");
    }

    protected void ImageButton10_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("/adminRepDefectos/FechhAudit.aspx");
    }

    protected void LinkButton10_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/FechhAudit.aspx");
    }
}