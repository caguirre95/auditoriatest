﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using SistemaAuditores.DataAccess;

public partial class OQLbyBundle : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtfecha1.Date = DateTime.Now.Date;
            txtfecha1.MaxDate = DateTime.Now.Date;
            txtfecha2.Date = DateTime.Now.Date;
            txtfecha2.MaxDate = DateTime.Now.Date;
            llenarcombo();
            Load_Data();
            //WebChartControl1.Visible = false;
        }
    }

    void llenarcombo()
    {

        DataTable dt_linea = DataAccess.Get_DataTable("select id_linea, numero from Linea where idcliente = 11 and numero not like '%-%' and numero not like '%A%' order by numero asc");

        DropDownList1.DataSource = dt_linea;
        DropDownList1.DataValueField = "id_linea";
        DropDownList1.DataTextField = "numero";
        DropDownList1.DataBind();

        DropDownList1.SelectedIndex = 1;
    }

    public void Load_Data()
    {
        string sqlfecha_actual1 = txtfecha1.Date.ToString("yyyy-MM-dd");
        string sqlfecha_actual2 = txtfecha2.Date.ToString("yyyy-MM-dd");

        if (DropDownList1.SelectedValue == "0")
        {
            DateTime fecha_actual = DateTime.UtcNow;
            sqlfecha_actual1 = fecha_actual.Date.ToString("yyyy-MM-dd");
            sqlfecha_actual2 = fecha_actual.Date.ToString("yyyy-MM-dd");
        }

        DataTable aql_aprov = new DataTable();
        DataTable aql_reject = new DataTable();

        DataTable grid = new DataTable();
        DataColumn acep = new DataColumn("Calidad", typeof(string));
        DataColumn rejec = new DataColumn("Valor", typeof(decimal));
        grid.Columns.Add(acep);
        grid.Columns.Add(rejec);

        decimal totacep = 0, totrejec = 0, totp = 0;
        decimal rejectoql = 0;
        int oqlapr = 0;
        
        ////
        
        //DataTable aql_total = new DataTable();
        //if (aql_aprov.Rows.Count > 0)
        //{
        //    Id_Bundle = Request.QueryString["IdBundle"] ?? "";
        //    query3 = "select Quantity from Bundle where Id_Bundle=" + Convert.ToInt16(aql_aprov.Rows[0][1]);
        //    aql_total = DataAccess.Get_DataTable(query3);

        //    int total = Convert.ToInt16(aql_aprov.Rows.Count) + Convert.ToInt16(aql_reject.Rows.Count);
        //    string piezas = Convert.ToString(total * Convert.ToInt16(aql_total.Rows[0][0]));
        //    lbltotbultau.Text = total + "/" + piezas;
        //    //%Calidad
        //    double calidad = 0;
        //    if (total > 0)
        //    {
        //        calidad = (Convert.ToInt16(aql_aprov.Rows.Count) * 100) / total;
        //    }
        //    calidad = (double)Math.Round(Convert.ToDecimal(calidad), 2);
        //    lblcalidad.Text = Convert.ToString(calidad) + "%";
        //}//

        decimal aql = 0;
		//DataTable dt_linea = DataAccess.Get_DataTable("select id_linea from Linea where numero='" + Convert.ToInt16(DropDownList1.SelectedValue) + "'");
		int id_Linea = Convert.ToInt16(DropDownList1.SelectedValue);
        do
        {
            for (int i = 1; i <= 5; i++)
            {
                string query = "select * from BundleApprovedBulto where id_bio=" + i + " and FechaAprobado='" + sqlfecha_actual1 + "' and Id_Linea=" + id_Linea;
                string query2 = "select distinct(Id_Bundle) from BundleRejectedBulto where id_bio=" + i + " and FechaRecibido='" + sqlfecha_actual1 + "' and Id_linea=" + id_Linea;                             
                //Reporte de Auditoria por Bulto
                aql_aprov = DataAccess.Get_DataTable(query);
                aql_reject = DataAccess.Get_DataTable(query2);
                
                if ((aql_aprov.Rows.Count > 0) && (aql_reject.Rows.Count > 0))
                {
                    string queryOQL = "select isnull(max(m.sample_size),0) as Muestreo from BundleApprovedBulto bd inner join Muestreo m on bd.id_muestreo=m.id_muestreo where bd.id_bio=" + i + " and bd.FechaAprobado='" + sqlfecha_actual1 + "'  and bd.Id_Linea=" + id_Linea;
                    DataTable oql = DataAccess.Get_DataTable(queryOQL);
                    oqlapr = (Convert.ToInt32(aql_aprov.Rows.Count) + Convert.ToInt32(aql_reject.Rows.Count)) * Convert.ToInt32(oql.Rows[0][0]);//Total Piezas        

                    string queryOQLRej = "select Id_Seccion,cantidad_defectos from BundleRejectedBulto where id_bio=" + i + " and FechaRecibido='" + sqlfecha_actual1 + "' and Id_linea=" + id_Linea;
                    DataTable oqlreje = DataAccess.Get_DataTable(queryOQLRej);

                    string query3 = "select Quantity from Bundle where Id_Bundle=" + Convert.ToInt32(aql_aprov.Rows[0][1]);
                    DataTable aql_total = DataAccess.Get_DataTable(query3);

                    int total = Convert.ToInt16(aql_aprov.Rows.Count) + Convert.ToInt16(aql_reject.Rows.Count);
                    
                    aql = (Convert.ToInt16(aql_aprov.Rows.Count) * 100) / total;
                    aql = Math.Round(aql, 2);

                    for (int y = 0; y <= oqlreje.Rows.Count - 1; y++)
                    {
                        rejectoql = rejectoql + Convert.ToInt16(oqlreje.Rows[y][1]);
                    }

                    totacep = totacep + (oqlapr - rejectoql);
                    totrejec = totrejec + rejectoql;
                    totp = totp + oqlapr;
                }
            }
            DateTime sqldate1 = new DateTime();
            sqldate1 = Convert.ToDateTime(sqlfecha_actual1);
            sqldate1 = sqldate1.AddDays(1);
            sqlfecha_actual1 = sqldate1.Date.ToString("yyyy-MM-dd");

        } while (Convert.ToDateTime(sqlfecha_actual1) <= Convert.ToDateTime(sqlfecha_actual2));
    
        if (totp > 0)
        {
            decimal porReject = (totrejec * 100) / totp;
            porReject = Math.Round(porReject, 2);
            decimal porAcep = (totacep * 100) / totp;
            porAcep = Math.Round(porAcep, 2);
            DataRow row = grid.NewRow();
            row["Calidad"] = "AQL";
            row["Valor"] = aql;

            DataRow row1 = grid.NewRow();
            row1["Calidad"] = "OQL";
            row1["Valor"] = porReject;

            grid.Rows.Add(row1);
            grid.Rows.Add(row);
            lbl.Visible = true;            
            WebChartControl1.Visible = true;
            WebChartControl1.DataSource = grid;
            WebChartControl1.DataBind();
        }
        else
        {            
            WebChartControl1.Visible = false;
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "infoalert();", true);
        }

        //Response.Write(totrejec + "<br/>");
        //Response.Write(totacep + "<br/>");
        //Response.Write(totp + "<br/>");
        //Response.Write(sqlfecha_actual1);

    }
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        Load_Data();
    }
}