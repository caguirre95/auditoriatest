﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteMedidaMaster.aspx.cs" Inherits="admin_ReporteMedidaMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .box_effect {
            height: 250px;
            width: 180px;
            padding-top: 15px;
            /*-webkit-transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
            transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);*/
            text-align: center;
            /*padding: 5px;*/
            border-width: 160px;
            /*-webkit-box-shadow: 7px 7px 7px rgba(0,0,0,0.2);
            -moz-box-shadow: 7px 7px 7px rgba(0,0,0,0.2);
            box-shadow: 7px 7px 7px rgba(0,0,0,0.2);
            -webkit-transition: all 0.5s ease-out;
            -moz-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;*/
            margin: auto;
            margin-bottom: 15px !important;
            opacity: 0.8;
            border: 1px solid rgba(0,0,0,0.2);
            /*-webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            -webkit-box-reflect: below 0px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(.7, transparent), to(rgba(0,0,0,0.1)));*/
        }

        /*.box_effect:hover {
                -webkit-transform: scale(1.25, 1.25);
                transform: scale(1.25, 1.25);
                background-color: #6E6E6E;
                color: white !important;
                text-decoration: none;
                margin-top: 2px;
                -webkit-transform: rotate(-7deg);
                -moz-transform: rotate(-7deg);
                -o-transform: rotate(-7deg);
                z-index: 1000;
                opacity: 1;
                -webkit-box-reflect: below 0px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(.7, transparent), to(rgba(0,0,0,0.4)));
                -webkit-box-shadow: 0px 0px 20px rgba(255,255,255,0.8);
                -moz-box-shadow: 0px 0px 20px rgba(255,255,255,0.8);
                box-shadow: 0px 0px 20px rgba(255,255,255,0.8);
            }*/

        .te {
            text-decoration: none;
        }

        .im {
            z-index: -1;
        }

        .mty {
            margin-left: 50px;
            margin-top: 15px;
        }

        .te:hover {
            color: white !important;
            text-decoration: none;
            outline-style: none;
        }

        .ocultar {
            display: none !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">

        <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

        <div class="row ocultar" style="margin-top: 5px">
            <div class="col-lg-4 ">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton15" runat="server" CssClass="te" OnClick="LinkButton15_Click">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Before Wash Final Audit </span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-lg-4 ">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="te" OnClick="LinkButton2_Click">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Before Wash In Line</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4 ">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="te" OnClick="LinkButton1_Click">
                    <div>                        
                        <img class=" mt" src="/img/medidas.jpg" alt="Bundle by Bundle" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">After Wash</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>


        </div>
        <div class="row ocultar">
            <div class="col-md-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton18" runat="server" CssClass="te" PostBackUrl="AuditoriaBultoABultoB.aspx">
                    <div>                        
                        <img src="/img/medidas.jpg" alt="Shipping Audit" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">Boot Barn Measurement Audit</span>
                    </div>
                    </asp:LinkButton>
                </div>

            </div>
            <div class="col-md-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton19" runat="server" CssClass="te" PostBackUrl="AuditoriaBultoABultoC.aspx">
                    <div>                        
                        <img src="/img/camisa.png" alt="Shipping Audit" class="mt" width="170" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">Shirt Measurement Audit</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton20" runat="server" CssClass="te" PostBackUrl="AuditoriaBultoABultoJ.aspx">
                    <div>                        
                        <img src="/img/jacket.png" alt="Shipping Audit" class="mt" width="170" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">Jacket Measurement Audit</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>
        </div>

        <div class="row ocultar">
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton3" runat="server" CssClass="te" OnClick="LinkButton3_Click">
                    <div>                        
                        <img src="/img/medidas.jpg" alt="Shipping Audit" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">Final Measurement Audit</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-lg-4 ocultar">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton4" runat="server" CssClass="te" PostBackUrl="ReportMedidaMaster.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Style Measurement Report</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-lg-4 ocultar">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton5" runat="server" CssClass="te" PostBackUrl="ReportLineMaster.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;"> Master Line Report</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton7" runat="server" CssClass="te" PostBackUrl="~/admin/ReportMasterRoc7.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Users Production Report</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="ImageButton12" runat="server" CssClass="te" PostBackUrl="~/admin/MultiLevel.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">MultiLevel Report</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <%-- <div class="col-md-4">
            <asp:ImageButton ID="ImageButton4" PostBackUrl="ReportMedidaMaster.aspx" runat="server" ImageUrl="/img/medidas.jpg"
                Width="128px" Height="128px"
                OnClick="ImageButton3_Click" /><br />
            <asp:LinkButton ID="LinkButton4" runat="server" OnClick="LinkButton3_Click" PostBackUrl="ReportMedidaMaster.aspx" Font-Bold="true" Font-Size="16px">  Style Measurement Report</asp:LinkButton>

        </div>--%>
            <%--  <div class="col-md-4">
            <asp:ImageButton ID="ImageButton5" PostBackUrl="ReportLineMaster.aspx" runat="server" ImageUrl="/img/medidas.jpg"
                Width="128px" Height="128px"
                OnClick="ImageButton3_Click" /><br />
            <asp:LinkButton ID="LinkButton5" runat="server" OnClick="LinkButton3_Click" PostBackUrl="ReportLineMaster.aspx" Font-Bold="true" Font-Size="16px">  Master Line Report </asp:LinkButton>

        </div>--%>
            <%--<div class="col-md-4">
            <asp:ImageButton ID="ImageButton6" PostBackUrl="ReporteInteXUser.aspx" runat="server" ImageUrl="/img/medidas.jpg"
                Width="128px" Height="128px"
                OnClick="ImageButton3_Click" /><br />
            <asp:LinkButton ID="LinkButton6" runat="server" OnClick="LinkButton3_Click" PostBackUrl="ReporteInteXUser.aspx" Font-Bold="true" Font-Size="16px"> Users Measurement Review</asp:LinkButton>

        </div>--%>
        </div>

        <div class="row ocultar">
            <div class="col-lg-4 ocultar">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton6" runat="server" CssClass="te" PostBackUrl="ReporteInteXUser.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;"> Users Measurement Review</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4 ocultar">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton8" runat="server" CssClass="te" PostBackUrl="~/admin/ReportConsolidados.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Consolidated Report</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>


        </div>

        <div class="row ocultar">
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton9" runat="server" CssClass="te" PostBackUrl="~/admin/RepCutXDate.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Cut Report</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton10" runat="server" CssClass="te" PostBackUrl="~/admin/reportSizeXStyle.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Size Report</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton11" runat="server" CssClass="te" PostBackUrl="~/admin/reportTallaXCorte.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Size by Cut Report</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>


            <%-- <div class="col-md-4">
            <asp:ImageButton ID="ImageButton10" PostBackUrl="~/admin/reportSizeXStyle.aspx" runat="server" ImageUrl="/img/medidas.jpg"
                Width="128px" Height="128px"
                OnClick="ImageButton3_Click" /><br />
            <asp:LinkButton ID="LinkButton10" runat="server" OnClick="LinkButton3_Click" PostBackUrl="~/admin/reportSizeXStyle.aspx" Font-Bold="true" Font-Size="16px">Size Report</asp:LinkButton>

        </div>
        <div class="col-md-4">
            <asp:ImageButton ID="ImageButton11" PostBackUrl="~/admin/reportTallaXCorte.aspx" runat="server" ImageUrl="/img/medidas.jpg"
                Width="128px" Height="128px"
                OnClick="ImageButton3_Click" /><br />
            <asp:LinkButton ID="LinkButton11" runat="server" OnClick="LinkButton3_Click" PostBackUrl="~/admin/reportTallaXCorte.aspx" Font-Bold="true" Font-Size="16px">Size by Cut Report</asp:LinkButton>

        </div>
        <div class="col-md-4">
            <asp:ImageButton ID="ImageButton12" PostBackUrl="~/admin/MultiLevel.aspx" runat="server" ImageUrl="/img/medidas.jpg"
                Width="128px" Height="128px" OnClick="ImageButton3_Click" />
            <br />
            <asp:LinkButton ID="LinkButton12" runat="server" OnClick="LinkButton3_Click" PostBackUrl="~/admin/MultiLevel.aspx" Font-Bold="true" Font-Size="16px">MultiLevel Report</asp:LinkButton>
        </div>--%>
        </div>

        <div class="row ocultar ">

            <div class="col-lg-4 ocultar">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton13" runat="server" CssClass="te" PostBackUrl="~/admin/porcentajeCortes.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Percent Measurement Report</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton12" runat="server" CssClass="te" PostBackUrl="~/admin/SpecialAudit1.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Special Audit I Report</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton14" runat="server" CssClass="te" PostBackUrl="~/admin/SpecialAudit2.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Special Audit II Report</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton16" runat="server" CssClass="te" PostBackUrl="~/admin/MultinivelXCorte.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Reporte Por Corte</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <%-- <div class="col-md-4">
            <asp:ImageButton ID="ImageButton13" PostBackUrl="~/admin/porcentajeCortes.aspx" runat="server" ImageUrl="/img/medidas.jpg"
                Width="128px" Height="128px" OnClick="ImageButton3_Click" />
            <br />
            <asp:LinkButton ID="LinkButton13" runat="server" OnClick="LinkButton3_Click" PostBackUrl="~/admin/porcentajeCortes.aspx" Font-Bold="true" Font-Size="16px">Percent Measurement Report </asp:LinkButton>
        </div>--%>
        </div>
     

        <div class="row">
           <%-- <div class="col-md-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton17" runat="server" CssClass="te" PostBackUrl="AuditoriaBultoABultoC20.aspx">
                    <div>                        
                        <img src="/img/camisa.png" alt="Shipping Audit" class="mt" width="170" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">Shirt Measurement Audit </span>
                         <span style="font-weight: bold;">Style : Gown ; Line: 20 </span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton21" runat="server" CssClass="te" PostBackUrl="AuditoriaBultoABultoC21.aspx">
                    <div>                        
                        <img src="/img/camisa.png" alt="Shipping Audit" class="mt" width="170" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">Shirt Measurement Audit </span>
                          <span style="font-weight: bold;">Style : 102908 ; Line: 21  </span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>--%>
          
           
        </div>

        <div class="row">
              <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkembAL" runat="server" CssClass="te" PostBackUrl="ReporteMedidaFinal.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Reporte Medida Final</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkbab" runat="server" CssClass="te" PostBackUrl="ReporteMedidaBultoaBultoNew.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Reporte Medida Bulto a Bulto</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkmit" runat="server" CssClass="te" PostBackUrl="ReporteIntexCountMedidas.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Reporte Medidas Intex T</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            
        </div>
        
        <div class="row">
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkmemb" runat="server" CssClass="te" PostBackUrl="ReporteEmbarqueMedidas.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Reporte Medidas Embarque</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnktodasmedidas" runat="server" CssClass="te" PostBackUrl="ReporteTodasLasMedidas.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Reporte Todas las Medidas</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
        </div>

         <div class="row">               
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkirreg" runat="server" CssClass="te" PostBackUrl="IrregularesReport.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Reporte Irregulares</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkirr2" runat="server" CssClass="te" PostBackUrl="IrregularesReport2.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Reporte Irregulares B/W</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkReproceso" runat="server" CssClass="te" PostBackUrl="MedidaReproceso.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Reporte Reproceso Medidas</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

        </div>
    

        <div class="row">
         
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkpvt" runat="server" CssClass="te" PostBackUrl="ReportePVDIntexMedida.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Medidas Intex PVT</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkmintex2" runat="server" CssClass="te" PostBackUrl="MedidaIntex2.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Medidas R2</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            
            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkrptintex" runat="server" CssClass="te" PostBackUrl="ReporteMedidasIntex.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Aceptado/Rechazado Intex</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkspc" runat="server" CssClass="te" PostBackUrl="HistorialCorteIntexMedida.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Medidas Intex</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton22" runat="server" CssClass="te" PostBackUrl="HistorialEstiloIntexMedida.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Medidas Intex X Estilo</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton23" runat="server" CssClass="te" PostBackUrl="ReportUsuarioIntex.aspx">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Usuarios Intex</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

        </div>	
    </div>
  
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

