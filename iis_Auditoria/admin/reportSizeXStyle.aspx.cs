﻿using DevExpress.Utils;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_reportSizeXStyle : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SideBySideBarSeriesLabel label = (SideBySideBarSeriesLabel)WebChartControl1.SeriesTemplate.Label;

        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtfecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");

            Session["rep"] = null;
            Session["rep1"] = null;


            chbShowLabels.Checked = WebChartControl1.SeriesTemplate.LabelsVisibility == DefaultBoolean.True;
            spnLabelIndent.Value = label.Indent;

        }

        bool indentEnabled = chbShowLabels.Checked;
        spnLabelIndent.ClientEnabled = indentEnabled;
        WebChartControl1.CrosshairOptions.CrosshairLabelMode = CrosshairLabelMode.ShowForNearestSeries;
    }

    void PerformShowLabelsAction()
    {
        WebChartControl1.SeriesTemplate.LabelsVisibility = chbShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False;
        WebChartControl1.CrosshairEnabled = chbShowLabels.Checked ? DefaultBoolean.False : DefaultBoolean.True;
    }

    void PerformLabelIndentAction()
    {
        ((BarSeriesLabel)WebChartControl1.SeriesTemplate.Label).Indent = Convert.ToInt32(spnLabelIndent.Value);
    }
    protected void WebChartControl1_CustomCallback(object sender, CustomCallbackEventArgs e)
    {
        if (e.Parameter == "ShowLabels")
            PerformShowLabelsAction();
        else if (e.Parameter == "LabelIndent")
            PerformLabelIndentAction();

        WebChartControl1.DataBind();
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtstyle.Text != string.Empty)
            {
                string queryBundle = "select distinct b.Size from Bundle b join POrder p on b.Id_Order=p.Id_Order join Style s on p.Id_Style=s.Id_Style where s.Style='" + txtstyle.Text + "' order by b.Size asc";
                DataTable dt_Bundle = DataAccess.Get_DataTable(queryBundle);

                DropDownListSize.DataSource = dt_Bundle;
                DropDownListSize.DataTextField = "Size";
                DropDownListSize.DataValueField = "Size";
                DropDownListSize.DataBind();
                DropDownListSize.Items.Insert(0, "Select...");

            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "0" || RadioButtonList1.SelectedValue == null)
        {
            return;
        }

        ASPxGridView1.DataBind();
    }

    protected void ASPxGridView1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView1.DataBind();
    }

    DataTable funcionCarga()
    {

        int resp = int.Parse(DropDownListProcess.SelectedValue);
        string query = "";
        switch (resp)
        {
            case 1:
                query = " select m.Medida,COUNT(m.Medida) as total ,pmd.NombreMedida,m.valor"
                       + " from Bundle b"
                       + " join tblPuntoMasterAL pm on b.Id_Bundle = pm.idbulto"
                       + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                       + " join tblMedidaBultoAL mb on pm.idPuntoMaster = mb.idMaster"
                       + " join tblMedidas m on mb.idMedida = m.idMedida"
                       + " join POrder p on p.Id_Order=b.Id_Order"
                       + " where b.Size = '" + DropDownListSize.SelectedItem.Text + "' and p.Id_Style=" + hdidstyle.Value 
                       + " and pm.rol = '" + RadioButtonList1.SelectedValue + "'"
                       + " and CONVERT(date, mb.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                       + " group by m.Medida, pmd.NombreMedida, m.valor"
                       + " order by m.valor desc";
                break;
            case 2:

                query = " select m.Medida,COUNT(m.Medida) as total ,pmd.NombreMedida,m.valor"
                       + " from Bundle b"
                       + " join tblPuntoMasterIntex pm on b.Id_Bundle = pm.idbulto"
                       + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                       + " join tblMedidaBultoIntex mb on pm.idPuntoMaster = mb.idMaster"
                       + " join tblMedidas m on mb.idMedida = m.idMedida"
                       + " join POrder p on p.Id_Order=b.Id_Order"
                       + " where b.Size = '" + DropDownListSize.SelectedItem.Text + "' and p.Id_Style=" + hdidstyle.Value
                       + " and CONVERT(date, mb.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                       + " group by m.Medida, pmd.NombreMedida, m.valor"
                       + " order by m.valor desc";

                break;
            case 3:

                query = " select m.Medida,COUNT(m.Medida) as total ,pmd.NombreMedida,m.valor"
                       + " from Bundle b"
                       + " join tblPuntoMaster pm on b.Id_Bundle = pm.idbulto"
                       + " join tblPuntosMedida pmd on pm.idpuntosM = pmd.idPuntosM"
                       + " join tblMedidaBulto mb on pm.idPuntoMaster = mb.idMaster"
                       + " join tblMedidas m on mb.idMedida = m.idMedida"
                       + " join POrder p on p.Id_Order=b.Id_Order"
                       + " where b.Size = '" + DropDownListSize.SelectedItem.Text + "' and p.Id_Style=" + hdidstyle.Value
                       + " and CONVERT(date, mb.fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                       + " group by m.Medida, pmd.NombreMedida, m.valor"
                       + " order by m.valor desc";

                break;
            default:
                break;
        }

        DataTable dt = new DataTable();
        dt = DataAccess.Get_DataTable(query);

        return dt;
    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {

        DataTable dt_seccion = new DataTable();

        dt_seccion.Columns.Add("PUNTOS_MEDIDA", typeof(string));
        dt_seccion.Columns.Add("WAIST", typeof(int));
        dt_seccion.Columns.Add("%WAIST", typeof(string));
        dt_seccion.Columns.Add("INSEAM", typeof(int));
        dt_seccion.Columns.Add("%INSEAM", typeof(string));
        dt_seccion.Columns.Add("FRONT_RISE", typeof(int));
        dt_seccion.Columns.Add("%FRONT_RISE", typeof(string));
        dt_seccion.Columns.Add("BACK_RISE", typeof(int));
        dt_seccion.Columns.Add("%BACK_RISE", typeof(string));
        dt_seccion.Columns.Add("HIP", typeof(int));
        dt_seccion.Columns.Add("%HIP", typeof(string));

        DataTable dt = new DataTable();

        string queryM = "select medida from tblMedidas order by valor desc";

        dt = DataAccess.Get_DataTable(queryM);

      
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            DataRow row2 = dt_seccion.NewRow();

            row2[0] = dt.Rows[i][0];
            dt_seccion.Rows.Add(row2);
        }

        dt = new DataTable();
        dt = funcionCarga();

        int n1 = 0, n2 = 0, n3 = 0, n4 = 0, n5 = 0;
        List<listatotal> list = new List<listatotal>();
        listatotal obj = new listatotal();
        listatotal obj1 = new listatotal();
        listatotal obj2 = new listatotal();

        List<listatotal> list1 = new List<listatotal>();
        listatotal obj01 = new listatotal();
        listatotal obj11 = new listatotal();
        listatotal obj12 = new listatotal();

        obj.r = "Over 1''";
        obj1.r = "In Speck";
        obj2.r = "Below -1''";

        obj01.r = "Over 1/2''";
        obj11.r = "In Speck";
        obj12.r = "Below -1/2''";

        if (dt != null)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][2].Equals("WAIST"))
                {
                    n1 = n1 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalw = obj.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalw = obj1.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalw = obj2.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.5)
                    {
                        obj01.totalw = obj01.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.5 && float.Parse(dt.Rows[i][3].ToString()) >= -0.5)
                    {
                        obj11.totalw = obj11.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.5)
                    {
                        obj12.totalw = obj12.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                }
                else if (dt.Rows[i][2].Equals("INSEAM"))
                {
                    n2 = n2 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalI = obj.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalI = obj1.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalI = obj2.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.5)
                    {
                        obj01.totalI = obj01.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.5 && float.Parse(dt.Rows[i][3].ToString()) >= -0.5)
                    {
                        obj11.totalI = obj11.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.5)
                    {
                        obj12.totalI = obj12.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                }
                else if (dt.Rows[i][2].Equals("FRONT RISE"))
                {
                    n3 = n3 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalF = obj.totalF + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalF = obj1.totalF + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalF = obj2.totalF + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.5)
                    {
                        obj01.totalF = obj01.totalF + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.5 && float.Parse(dt.Rows[i][3].ToString()) >= -0.5)
                    {
                        obj11.totalF = obj11.totalF + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.5)
                    {
                        obj12.totalF = obj12.totalF + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("BACK RISE"))
                {
                    n4 = n4 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalB = obj.totalB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalB = obj1.totalB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalB = obj2.totalB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.5)
                    {
                        obj01.totalB = obj01.totalB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.5 && float.Parse(dt.Rows[i][3].ToString()) >= -0.5)
                    {
                        obj11.totalB = obj11.totalB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.5)
                    {
                        obj12.totalB = obj12.totalB + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("HIP"))
                {
                    n5 = n5 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalH = obj.totalH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalH = obj1.totalH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalH = obj2.totalH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.5)
                    {
                        obj01.totalH = obj01.totalH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.5 && float.Parse(dt.Rows[i][3].ToString()) >= -0.5)
                    {
                        obj11.totalH = obj11.totalH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.5)
                    {
                        obj12.totalH = obj12.totalH + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
            }

            obj.procenW = ((Convert.ToSingle(obj.totalw) / n1) * 100).ToString("0.00") + "% ";
            obj1.procenW = ((Convert.ToSingle(obj1.totalw) / n1) * 100).ToString("0.00") + "% ";
            obj2.procenW = ((Convert.ToSingle(obj2.totalw) / n1) * 100).ToString("0.00") + "% ";

            obj.procenI = ((Convert.ToSingle(obj.totalI) / n2) * 100).ToString("0.00") + "% ";
            obj1.procenI = ((Convert.ToSingle(obj1.totalI) / n2) * 100).ToString("0.00") + "% ";
            obj2.procenI = ((Convert.ToSingle(obj2.totalI) / n2) * 100).ToString("0.00") + "% ";

            obj.procenF = ((Convert.ToSingle(obj.totalF) / n3) * 100).ToString("0.00") + "% ";
            obj1.procenF = ((Convert.ToSingle(obj1.totalF) / n3) * 100).ToString("0.00") + "% ";
            obj2.procenF = ((Convert.ToSingle(obj2.totalF) / n3) * 100).ToString("0.00") + "% ";

            obj.procenB = ((Convert.ToSingle(obj.totalB) / n4) * 100).ToString("0.00") + "%";
            obj1.procenB = ((Convert.ToSingle(obj1.totalB) / n4) * 100).ToString("0.00") + "% ";
            obj2.procenB = ((Convert.ToSingle(obj2.totalB) / n4) * 100).ToString("0.00") + "% ";

            obj.procenH = ((Convert.ToSingle(obj.totalH) / n5) * 100).ToString("0.00") + "% ";
            obj1.procenH = ((Convert.ToSingle(obj1.totalH) / n5) * 100).ToString("0.00") + "% ";
            obj2.procenH = ((Convert.ToSingle(obj2.totalH) / n5) * 100).ToString("0.00") + "% ";

            list.Add(obj);

            list.Add(obj1);

            list.Add(obj2);


            obj01.procenW = ((Convert.ToSingle(obj01.totalw) / n1) * 100).ToString("0.00") + "% ";
            obj11.procenW = ((Convert.ToSingle(obj11.totalw) / n1) * 100).ToString("0.00") + "% ";
            obj12.procenW = ((Convert.ToSingle(obj12.totalw) / n1) * 100).ToString("0.00") + "% ";

            obj01.procenI = ((Convert.ToSingle(obj01.totalI) / n2) * 100).ToString("0.00") + "% ";
            obj11.procenI = ((Convert.ToSingle(obj11.totalI) / n2) * 100).ToString("0.00") + "% ";
            obj12.procenI = ((Convert.ToSingle(obj12.totalI) / n2) * 100).ToString("0.00") + "% ";

            obj01.procenF = ((Convert.ToSingle(obj01.totalF) / n3) * 100).ToString("0.00") + "% ";
            obj11.procenF = ((Convert.ToSingle(obj11.totalF) / n3) * 100).ToString("0.00") + "% ";
            obj12.procenF = ((Convert.ToSingle(obj12.totalF) / n3) * 100).ToString("0.00") + "% ";

            obj01.procenB = ((Convert.ToSingle(obj01.totalB) / n4) * 100).ToString("0.00") + "% ";
            obj11.procenB = ((Convert.ToSingle(obj11.totalB) / n4) * 100).ToString("0.00") + "% ";
            obj12.procenB = ((Convert.ToSingle(obj12.totalB) / n4) * 100).ToString("0.00") + "% ";

            obj01.procenH = ((Convert.ToSingle(obj01.totalH) / n5) * 100).ToString("0.00") + "% ";
            obj11.procenH = ((Convert.ToSingle(obj11.totalH) / n5) * 100).ToString("0.00") + "% ";
            obj12.procenH = ((Convert.ToSingle(obj12.totalH) / n5) * 100).ToString("0.00") + "% ";

            list1.Add(obj01);

            list1.Add(obj11);

            list1.Add(obj12);

            for (int i = 0; i < dt_seccion.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (dt.Rows[j][2].Equals("WAIST"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][1] = dt.Rows[j][1];
                            dt_seccion.Rows[i][2] = ((float.Parse(dt.Rows[j][1].ToString()) / n1) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("INSEAM"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][3] = dt.Rows[j][1];
                            dt_seccion.Rows[i][4] = ((float.Parse(dt.Rows[j][1].ToString()) / n2) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("FRONT RISE"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][5] = dt.Rows[j][1];
                            dt_seccion.Rows[i][6] = ((float.Parse(dt.Rows[j][1].ToString()) / n3) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("BACK RISE"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][7] = dt.Rows[j][1];
                            dt_seccion.Rows[i][8] = ((float.Parse(dt.Rows[j][1].ToString()) / n4) * 100).ToString("0.00") + "%";
                        }
                    }
                    else if (dt.Rows[j][2].Equals("HIP"))
                    {
                        if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
                        {
                            dt_seccion.Rows[i][9] = dt.Rows[j][1];
                            dt_seccion.Rows[i][10] = ((float.Parse(dt.Rows[j][1].ToString()) / n5) * 100).ToString("0.00") + "%";
                        }
                    }
                }
            }
        }

        WebChartControl1.DataBind();

        ASPxGridView1.DataSource = dt_seccion;

        Session["rep"] = list;
        Session["rep1"] = list1;

        ASPxGridView3.DataBind();
        ASPxGridView4.DataBind();
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView1.DataBind();

            ASPxGridViewExporter1.GridViewID = "ASPxGridView1";
            ASPxGridViewExporter2.GridViewID = "ASPxGridView3";
            ASPxGridViewExporter3.GridViewID = "ASPxGridView4";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;
            link1.CreateMarginalHeaderArea += new CreateAreaEventHandler(Link_CreateMarginalHeaderArea);

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link2.Component = ASPxGridViewExporter2;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = ASPxGridViewExporter3;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1, link2, link3 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Report_AfterWash";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=ReportMeasurement.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {
            throw;
        }
    }

    private void Link_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
    {
        // string resp = RadioButtonList1.SelectedValue;
        string mess = "";
        //if (resp == "1")
        //{
        mess = "Style=" + txtstyle.Text + "  Size=" + DropDownListSize.SelectedItem.Text;// + "\n" + memo.Text;
        DevExpress.XtraPrinting.TextBrick brick;
        brick = e.Graph.DrawString(mess, Color.Navy, new RectangleF(0, 0, 700, 50), DevExpress.XtraPrinting.BorderSide.None);
        brick.Font = new Font("Times New Roman", 12);
        brick.StringFormat = new DevExpress.XtraPrinting.BrickStringFormat(StringAlignment.Center);
        //}
        //else
        //{
        //    // mess = memo.Text;
        //}
        // string reps="31313131311sdsfsdf";
        //PageInfoBrick brick = e.Graph.DrawPageInfo(PageInfo., "", Color.DarkBlue,new RectangleF(0, 0, 100, 20), BorderSide.None);

        //brick.LineAlignment = BrickAlignment.Center;

        //brick.Alignment = BrickAlignment.Center;

        //brick.AutoWidth = true;

    }

    protected void DropDownListProcess_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownListProcess.SelectedValue == "1")
        {
            Panel1.Visible = true;
        }
        else
        {
            Panel1.Visible = false;
        }
    }

    protected void ASPxGridView3_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView3.DataSource = (List<listatotal>)Session["rep"];
    }

    protected void ASPxGridView3_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView3.DataBind();
    }

    protected void ASPxGridView4_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView4.DataSource = (List<listatotal>)Session["rep1"];
    }

    protected void ASPxGridView4_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView4.DataBind();
    }

    protected void WebChartControl1_DataBinding(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();

        dt = funcionCarga();

        WebChartControl1.DataSource = dt;

    }
    public class listatotal
    {
        public string r { get; set; }
        public int totalw { get; set; }
        public string procenW { get; set; }
        public int totalI { get; set; }
        public string procenI { get; set; }
        public int totalF { get; set; }
        public string procenF { get; set; }
        public int totalB { get; set; }
        public string procenB { get; set; }
        public int totalH { get; set; }
        public string procenH { get; set; }

    }

}