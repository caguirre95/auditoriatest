﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_MedidaIntex2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();
            Session["intexR"] = null;
            cargacombo();
            cargagridestilos();
           
        }
    }

    void cargacombo()
    {
        string sqline = "select id_linea,numero from Linea l where l.numero not like '%-%' and l.numero not like '%A%' order by CONVERT(float,numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT [id_planta],[descripcion] FROM[Auditoria].[dbo].[Planta]";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
        DropDownListPlant.Items.Insert(5, "All Plant");

    }

    void cargagridestilos()
    {
        //string sqline = " select vw.Estilo as Style from vwMedidasNewIntex vw"
        //              + " where datepart(wk, fechaRegistro) = DATEPART(WK, GETDATE()) and DATEPART(YY, fechaRegistro)= DATEPART(YY, GETDATE())"
        //              + " group by vw.Estilo";

        string sqline = " select s.Style from tbMedidasIntexDL i join Style s on s.Id_Style = i.idEstilo"
                      + " where datepart(wk, fechaRegistro) = DATEPART(WK, GETDATE()) and DATEPART(YY, fechaRegistro)= DATEPART(YY, GETDATE())"
                      + " group by idEstilo, s.Style";

        DataTable dtEstilos = DataAccess.Get_DataTable(sqline);

        grvestilos.DataSource = dtEstilos;
        grvestilos.DataBind();
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            funcionCarga();
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    void funcionCarga()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);

        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string resp = RadioButtonList1.SelectedValue;

        string query = "";

        switch (resp)
        {
            case "1"://cut
                if (txtPorder.Text != string.Empty)
                {
                    query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([INSEAM],'') INSEAM from (select a.Medida medidaTexto, ISNULL(b.total,0) total, b.NombreMedida, a.valor from "
                          + " (select idMedida, Medida, valor from tblMedidasIntex) a left join (select mp.medidaTexto, COUNT(mp.medidaTexto) total, (case when pm.NombreMedida = 'WAIST' then 'WAIST' else 'INSEAM' end) NombreMedida,"
                          + " mp.medidaFlotante from POrder p join tbMedidasIntexDL mi on mi.idPorder = p.Id_Order join tbMedidasIntexP mp on mp.id = mi.id"
                          + " join tblPuntosMedida pm on pm.idPuntosM = mp.idPuntoMedida where p.POrder = '" + txtPorder.Text.Trim() + "' group by mp.medidaTexto, pm.NombreMedida, mp.medidaFlotante"
                          + " ) b on a.Medida = b.medidaTexto group by a.Medida, b.total, b.NombreMedida, a.valor) a pivot(sum(total) for NombreMedida in ([WAIST], [INSEAM])) as sec"
                          + " order by valor desc";

                    DataTable prod = DataAccess.Get_DataTable(query);
                    Session["intexR"] = prod;
                    ASPxGridView1.DataSource = prod;
                    ASPxGridView1.DataBind();
                }
                else
                {
                    resp = "fin";
                }

                break;

            case "2"://style
                if (txtstyle.Text != string.Empty)
                {

                    query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([INSEAM],'') INSEAM from (select a.Medida medidaTexto, ISNULL(b.total,0) total, b.NombreMedida, a.valor from "
                          + " (select idMedida, Medida, valor from tblMedidasIntex) a left join (select mp.medidaTexto, COUNT(mp.medidaTexto) total, (case when pm.NombreMedida = 'WAIST' then 'WAIST' else 'INSEAM' end) NombreMedida,"
                          + " mp.medidaFlotante from tbMedidasIntexDL mi join tbMedidasIntexP mp on mp.id = mi.id"
                          + " join tblPuntosMedida pm on pm.idPuntosM = mp.idPuntoMedida  where mi.IdEstilo = " + hdidestilo.Value + " and CONVERT(date, mi.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                          + " group by mp.medidaTexto, pm.NombreMedida, mp.medidaFlotante) b on a.Medida = b.medidaTexto group by a.Medida, b.total, b.NombreMedida, a.valor) a pivot(sum(total) for NombreMedida in ([WAIST], [INSEAM])) as sec"
                          + " order by valor desc";

                    DataTable prod = DataAccess.Get_DataTable(query);
                    Session["intexR"] = prod;
                    ASPxGridView1.DataSource = prod;
                    ASPxGridView1.DataBind();
                }
                else
                {
                    resp = "fin";
                }

                break;

            case "3"://line
                if (DropDownListLine.SelectedItem.Text != "Select...")
                {

                    query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([INSEAM],'') INSEAM"
                         + " from(select a.Medida as medidaTexto, ISNULL(b.total, 0) as total, b.NombreMedida, a.valor"
                         + " from(select idMedida, Medida, valor from tblMedidasIntex ) a"
                         + " left join"
                         + " (select c1.medidaTexto, sum(c1.total) as total, c1.NombreMedida, c1.medidaFlotante from"
                         + " (select mi.idPorder, mp.medidaTexto, COUNT(mp.medidaTexto) total,(case when pm.NombreMedida = 'WAIST' then 'WAIST' else 'INSEAM' end) NombreMedida, mp.medidaFlotante"
                         + " from tbMedidasIntexDL mi  join tbMedidasIntexP mp on mp.id = mi.id join tblPuntosMedida pm on pm.idPuntosM = mp.idPuntoMedida"
                         + " where CONVERT(date, mi.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "' group by mi.idPorder, mp.medidaTexto, pm.NombreMedida, mp.medidaFlotante"
                         + " ) as c1"
                         + " join"
                         + " (select po.Id_Order from POrder po where po.id_linea2 = " + DropDownListLine.SelectedValue + ") as c2 on c1.idPorder = c2.Id_Order"
                         + " group by c1.medidaTexto, c1.NombreMedida, c1.medidaFlotante"
                         + " ) b on a.Medida = b.medidaTexto"
                         + " ) a pivot(sum(total) for NombreMedida in ([WAIST], [INSEAM])) as sec order by valor desc";

                    //query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([INSEAM],'') INSEAM from (select a.Medida medidaTexto, ISNULL(b.total,0) total, b.NombreMedida, a.valor from "
                    //      + " (select idMedida, Medida, valor from tblMedidasIntex) a left join (select mp.medidaTexto, COUNT(mp.medidaTexto) total, (case when pm.NombreMedida = 'WAIST' then 'WAIST' else 'INSEAM' end) NombreMedida,"
                    //      + " mp.medidaFlotante from POrder p join tbMedidasIntexDL mi on mi.idPorder = p.Id_Order join tbMedidasIntexP mp on mp.id = mi.id"
                    //      + " join tblPuntosMedida pm on pm.idPuntosM = mp.idPuntoMedida where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, mi.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                    //      + " group by mp.medidaTexto, pm.NombreMedida, mp.medidaFlotante) b on a.Medida = b.medidaTexto group by a.Medida, b.total, b.NombreMedida, a.valor) a pivot(sum(total) for NombreMedida in ([WAIST], [INSEAM])) as sec"
                    //      + " order by valor desc";

                    DataTable prod = DataAccess.Get_DataTable(query);
                    Session["intexR"] = prod;
                    ASPxGridView1.DataSource = prod;
                    ASPxGridView1.DataBind();
                }
                else
                {
                    resp = "fin";
                }

                break;

            case "4"://plant
                if (DropDownListPlant.SelectedItem.Text == "Select...")
                {
                    resp = "fin";
                }
                else if (DropDownListPlant.SelectedItem.Text == "All Plant")
                {

                    //query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([INSEAM],'') INSEAM from (select a.Medida medidaTexto, ISNULL(b.total,0) total, b.NombreMedida, a.valor from "
                    //      + " (select idMedida, Medida, valor from tblMedidasIntex) a left join (select mp.medidaTexto, COUNT(mp.medidaTexto) total, (case when pm.NombreMedida = 'WAIST' then 'WAIST' else 'INSEAM' end) NombreMedida,"
                    //      + " mp.medidaFlotante from POrder p join tbMedidasIntexDL mi on mi.idPorder = p.Id_Order join tbMedidasIntexP mp on mp.id = mi.id"
                    //      + " join tblPuntosMedida pm on pm.idPuntosM = mp.idPuntoMedida where CONVERT(date, mi.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                    //      + " group by mp.medidaTexto, pm.NombreMedida, mp.medidaFlotante) b on a.Medida = b.medidaTexto group by a.Medida, b.total, b.NombreMedida, a.valor) a pivot(sum(total) for NombreMedida in ([WAIST], [INSEAM])) as sec"
                    //      + " order by valor desc";

                    query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([INSEAM],'') INSEAM from (select a.Medida medidaTexto, ISNULL(SUM(b.total),0) total, isnull(b.NombreMedida, '') NombreMedida, a.valor"
                          + " from(select idMedida, Medida, valor from tblMedidasIntex) a left join (select mp.medidaTexto, COUNT(mp.medidaTexto) Total, (case when pm.NombreMedida = 'WAIST' then 'WAIST' else 'INSEAM' end) NombreMedida"
                          + " from tbMedidasIntexDL mi join tbMedidasIntexP mp on mp.id = mi.id join tblPuntosMedida pm on pm.idPuntosM = mp.idPuntoMedida"
                          + " where CONVERT(date, mi.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                          + " group by mp.medidaTexto, pm.NombreMedida) b on a.Medida = b.medidaTexto group by a.Medida, b.NombreMedida, a.valor ) a pivot(sum(total) for NombreMedida in ([WAIST], [INSEAM])) as sec order by valor desc";

                    DataTable prod = DataAccess.Get_DataTable(query);
                    Session["intexR"] = prod;
                   // ASPxGridView1.DataSource = prod;
                    ASPxGridView1.DataBind();
                }
                else
                {
                    //las consultas en comentarios se dañaron por razones desconocidas 22-01-2021
                    //query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([INSEAM],'') INSEAM from (select a.Medida medidaTexto, ISNULL(b.total,0) total, b.NombreMedida, a.valor from "
                    //      + " (select idMedida, Medida, valor from tblMedidasIntex) a left join (select mp.medidaTexto, COUNT(mp.medidaTexto) total, (case when pm.NombreMedida = 'WAIST' then 'WAIST' else 'INSEAM' end) NombreMedida,"
                    //      + " mp.medidaFlotante from POrder p left join tbMedidasIntexDL mi on mi.idPorder = p.Id_Order join tbMedidasIntexP mp on mp.id = mi.id"
                    //      + " join tblPuntosMedida pm on pm.idPuntosM = mp.idPuntoMedida where p.Id_Planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date, mi.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                    //      + " group by mp.medidaTexto, pm.NombreMedida, mp.medidaFlotante) b on a.Medida = b.medidaTexto group by a.Medida, b.total, b.NombreMedida, a.valor) a pivot(sum(total) for NombreMedida in ([WAIST], [INSEAM])) as sec"
                    //      + " order by valor desc";

                    query = " select medidaTexto PuntoMedida, isnull([WAIST],'') WAIST, isnull([INSEAM],'') INSEAM"
                        + " from(select a.Medida as medidaTexto, ISNULL(b.total, 0) as total, b.NombreMedida, a.valor"
                        + " from(select idMedida, Medida, valor from tblMedidasIntex ) a"
                        + " left join"
                        + " (select c1.medidaTexto, sum(c1.total) as total, c1.NombreMedida, c1.medidaFlotante from"
                        + " (select mi.idPorder, mp.medidaTexto, COUNT(mp.medidaTexto) total, (case when pm.NombreMedida = 'WAIST' then 'WAIST' else 'INSEAM' end) NombreMedida, mp.medidaFlotante"
                        + " from tbMedidasIntexDL mi  join tbMedidasIntexP mp on mp.id = mi.id join tblPuntosMedida pm on pm.idPuntosM = mp.idPuntoMedida"
                        + " where CONVERT(date, mi.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "' group by mi.idPorder, mp.medidaTexto, pm.NombreMedida, mp.medidaFlotante"
                        + " ) as c1"
                        + " join"
                        + " (select po.Id_Order, po.Id_Planta from POrder po where po.Id_Planta = " + DropDownListPlant.SelectedValue + ") as c2 on c1.idPorder = c2.Id_Order"
                        + " group by c1.medidaTexto, c1.NombreMedida, c1.medidaFlotante"
                        + " ) b on a.Medida = b.medidaTexto"
                        + " ) a pivot(sum(total) for NombreMedida in ([WAIST], [INSEAM])) as sec order by valor desc";

                    DataTable prod = DataAccess.Get_DataTable(query);
                    Session["intexR"] = prod;
                    // ASPxGridView1.DataSource = prod;
                    ASPxGridView1.DataBind();
                }

                break;

            case "":
                resp = "fin";
                break;

            default:
                resp = "fin";
                break;
        }
    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView1.DataSource = Session["intexR"];
    }

    protected void ASPxGridView1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView1.DataBind();
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView1.DataBind();

            ASPxGridViewExporter1.GridViewID = "ASPxGridView1";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreatePageForEachLink();
            //compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Reporte-Frec-Intex";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=ReporteFrecuencia.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            throw;
        }
    }
}