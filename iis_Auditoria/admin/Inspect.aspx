﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Inspect.aspx.cs" Inherits="admin_Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2>
        <asp:Label ID="Label1" runat="server" Text="User Administration"></asp:Label></h2>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        #Grid {
            margin-top: 15px;
        }
    </style>
    <div id="Grid" style="width: 100%;">
        <center>
            <asp:GridView ID="UserGrid" runat="server" BackColor="White" OnRowCommand="UserGrid_RowCommand1" AutoGenerateColumns="false" BorderColor="#CCCCCC" BorderWidth="1px" 
            ForeColor="Black" GridLines="Horizontal" PageSize="500" CellSpacing="5" >                                
                 <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                <PagerSettings PageButtonCount="100" Position="Top" />
                <PagerStyle CssClass="PagerCS" BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
                    <Columns>
                     <asp:BoundField DataField="codigo" HeaderText="Codigo" />
                     <asp:BoundField DataField="Nombre" HeaderText="Nombre"/>                     
                     <asp:ButtonField ButtonType="Image" CommandName="Editbtn" HeaderText="Edit"
                     ImageUrl="~/img/edit.png" ShowHeader="True" Text="Edit" />
                     <asp:ButtonField ButtonType="Image" CommandName="Deletebtn" HeaderText="Delete"
                     ImageUrl="~/img/delete.png" ShowHeader="True" Text="Delete" />
                    </Columns>
            </asp:GridView>
        </center>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <style>
        #UserAdd {
            margin-top: 15px;
            margin-right: auto;
            margin-left: auto;
            height: 55px;
            width: 700px;
            background-color: #5882FA;
            text-align: center;
            border-radius: 10px 10px 10px 10px;
        }

            #UserAdd div {
                display: table-cell;
                vertical-align: middle;
            }

            #UserAdd span{
                font-size:18px;
                font-weight:bold;
            }

        #save {
            text-align: center;
            margin-top: 10px !important;
        }

    
    </style>
    <div id="UserAdd">
        <div id="name">
            <asp:Label Text="Nombre" runat="server">
                <asp:TextBox ID="TxtNombre" runat="server"></asp:TextBox>
            </asp:Label>
        </div>
        <div id="username">
            <asp:Label Text="Username" runat="server"></asp:Label>
            <asp:TextBox ID="TxtUsername" runat="server"></asp:TextBox>
        </div>
        <div id="rol">
            <asp:Label Text="Sección" runat="server"></asp:Label>
            <asp:DropDownList ID="Secdrop" runat="server"></asp:DropDownList>
        </div>
        <div id="pass">
            <asp:Label Text="Password" runat="server"></asp:Label>
            <input id="Password1" type="password" runat="server" />
        </div>
    </div>
    <div id="save">
        <asp:ImageButton ID="Savebtn" runat="server" ImageUrl="~/img/add_user.png" OnClick="Savebtn_Click" />
    </div>
</asp:Content>
