﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Reportes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void img_report1_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("Report1.aspx");
    }
    protected void lnk_report1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Report1.aspx");
    }
    protected void img_report2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("Report2.aspx");
    }
    protected void lnk_report2_Click(object sender, EventArgs e)
    {
        Response.Redirect("Report2.aspx");
    }
    protected void img_report3_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("SeccionReport.aspx");
    }
    protected void lnk_report3_Click(object sender, EventArgs e)
    {
        Response.Redirect("SeccionReport.aspx");
    }
}