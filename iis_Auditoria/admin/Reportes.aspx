﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Reportes.aspx.cs" Inherits="admin_Reportes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
    #search_panel
    {
    margin-top:50px !important;    
    }
    .box_search
    {
    background-color:#fff !important;   
    }
</style>
<script type="text/javascript" src="/scripts/jquery.blockUI.js"></script>   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
    <h3><asp:Label ID="Label1" runat="server" Text="Data Report Page"></asp:Label></h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
  <script type="text/javascript">
      function BlockUI(elementID) {
          var prm = Sys.WebForms.PageRequestManager.getInstance();
          prm.add_beginRequest(function () {
              $("#" + elementID).block({ message: '<table><tr><td>' + '<img src="/img/please_wait.gif"/></td></tr></table>',
                  css: {},
                  overlayCSS: { backgroundColor: '#FFFFFF', opacity: 0.6, border: '1px solid #000000' }
              });
          });
          prm.add_endRequest(function () {
              $("#" + elementID).unblock();
          });
      }
      $(document).ready(function () {
          BlockUI("search_panel");
          $.blockUI.defaults.css = {};
      });
</script>
<div id="search_panel" class="box_search">
        <div class="row">
            <div class="izq">
                    <div style="width:100%;text-align: center; padding:10px; display: table;">
                        <div style="display: table-cell;padding:10px;">
                         <asp:ImageButton ID="img_report1" runat="server" Width="96" 
                                ImageUrl="/img/quantity.png" onclick="img_report1_Click" />
                            <br />
                        <asp:LinkButton ID="lnk_report1" runat="server" onclick="lnk_report1_Click">Bundles Quantity Audit</asp:LinkButton>
                        </div>
                        <div style="display: table-cell;padding:10px;">
                        <asp:ImageButton ID="img_report2" runat="server" Width="72" 
                                ImageUrl="/img/checklist1.png" onclick="img_report2_Click"/><br />
                        <asp:LinkButton ID="lnk_report2" runat="server" onclick="lnk_report2_Click">Bundles Approved or Rejected</asp:LinkButton>
                        </div>
                        <div style="display: table-cell;padding:10px;">
                        <asp:ImageButton ID="img_report3" runat="server" Width="96" 
                                ImageUrl="/img/live.png" onclick="img_report3_Click"/><br />
                        <asp:LinkButton ID="lnk_report3" runat="server" onclick="lnk_report3_Click">Live Report by Auditor</asp:LinkButton>
                        </div>
                    </div>
             </div>
       </div>
</div>
</asp:Content>

