﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_IrregularesReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["irregulares2"] = null;
            Session["porcent2"] = null;
            Cargacombo();
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();
        }
    }

    void Cargacombo()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta, descripcion FROM Planta order by descripcion asc";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top 15 POrderClient from POrder p where POrderClient like '%" + pre + "%' group by POrderClient order by len(p.POrderClient) asc", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porder = dt.Rows[i][0].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porder { get; set; }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<estilosClass> GetStyle(string pref)
    {
        try
        {
            List<estilosClass> list = new List<estilosClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = "SELECT distinct top 15  Style,Id_Style FROM Style where Style like '%" + pref + "%' ";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                estilosClass obj = new estilosClass();

                obj.estilo = dt.Rows[i][0].ToString();
                obj.id = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class estilosClass
    {
        public string estilo { get; set; }
        public string id { get; set; }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            var consulta = new DataTable();
            var consultap = new DataTable();

            string sqlquery = "";
            string sqlqueryp = "";

            string opcion = rd1.SelectedValue;

            switch (opcion)
            {
                case "1"://Corte
                    if (txtPorder.Text != string.Empty)
                    {
                        //sqlquery = " select pl.descripcion Planta, 'Linea: ' + l.numero Linea, p.POrder Corte, p.Quantity, s.Style Estilo, i.talla Talla, i.Area, d.Defecto, po.Posicion, ISNULL(sum(i.unidades), 0) Cantidad"
                        //      + " from Irregulares i join POrder p on p.Id_Order = i.idOrder join Linea l on l.id_linea = p.Id_Linea join Style s on s.Id_Style = p.Id_Style"
                        //      + " join Planta pl on pl.id_planta = l.id_planta join tbDefectos d on d.idDefecto = i.idDefecto join tbPosicion po on po.idPosicion = i.idPosicion"
                        //      + " where p.Id_Order = " + hdnidporder.Value + " group by pl.descripcion, l.numero, p.POrder, p.Quantity, s.Style, i.talla, i.Area, d.Defecto, po.Posicion";

                        sqlquery = " select a.descripcion Planta, a.numero Linea, a.POrderClient Corte, a.Quantity Quantity, a.Estilo, b.talla, b.area, b.Defecto, b.Posicion, SUM(b.Cantidad) Cantidad"
                                 + " from(select pl.descripcion, l.numero, p.POrderClient, SUM(b.Quantity) Quantity, MIN(s.Style) Estilo from POrder p"
                                 + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Planta pl on pl.id_planta = l.id_planta"
                                 + " where p.POrderClient = '" + txtPorder.Text.Trim() + "' group by pl.descripcion, l.numero, p.POrderClient) a join(select orden POrderClient, i.talla, i.area, SUM(i.unidades) Cantidad, d.Defecto, po.Posicion"
                                 + " from IrregularesEnCrudo i join tbDefectos d on d.idDefecto = i.idDefecto join tbPosicion po on po.idPosicion = i.idPosicion"
                                 + " where i.orden = '" + txtPorder.Text.Trim() + "' group by orden, talla, area, d.Defecto, po.Posicion) b on a.POrderClient = b.POrderClient"
                                 + " group by a.descripcion, a.numero, a.POrderClient, a.Quantity, a.Estilo, b.talla, b.area, b.Defecto, b.Posicion";

                        consulta = DataAccess.Get_DataTable(sqlquery);
                        Session["irregulares2"] = consulta;
                        grdIrregular.DataBind();

                        //sqlqueryp = " select pl.descripcion Planta, 'Linea: ' + l.numero Linea, p.POrder Corte, p.Quantity, s.Style Estilo, d.Defecto, ISNULL(sum(i.unidades), 0) Cantidad,"
                        //                 + " CAST(CAST(ISNULL(sum(i.unidades), 0) as decimal(16, 2)) / CAST(p.Quantity as decimal(16, 2)) * 100 as decimal(16, 2)) Porcentaje from Irregulares i"
                        //                 + " join POrder p on p.Id_Order = i.idOrder join Linea l on l.id_linea = p.Id_Linea join Style s on s.Id_Style = p.Id_Style join Planta pl on pl.id_planta = l.id_planta"
                        //                 + " join tbDefectos d on d.idDefecto = i.idDefecto where p.Id_Order = " + hdnidporder.Value + ""
                        //                 + " group by pl.descripcion, l.numero, p.POrder, p.Quantity, s.Style, d.Defecto";

                        sqlqueryp = " select a.descripcion Planta, a.numero Linea, a.POrderClient Corte, a.Quantity Quantity, a.Estilo, b.Defecto, SUM(b.Cantidad) Cantidad,"
                                  + " CAST(CAST(ISNULL(sum(b.Cantidad), 0) as decimal(16, 2)) / CAST(a.Quantity as decimal(16, 2)) * 100 as decimal(16, 2)) Porcentaje"
                                  + " from(select pl.descripcion, l.numero, p.POrderClient, SUM(b.Quantity) Quantity, MIN(s.Style) Estilo from POrder p"
                                  + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Planta pl on pl.id_planta = l.id_planta"
                                  + " where p.POrderClient = '" + txtPorder.Text.Trim() + "' group by pl.descripcion, l.numero, p.POrderClient) a join(select orden POrderClient, i.talla, i.area, SUM(i.unidades) Cantidad, d.Defecto, po.Posicion"
                                  + " from IrregularesEnCrudo i join tbDefectos d on d.idDefecto = i.idDefecto join tbPosicion po on po.idPosicion = i.idPosicion"
                                  + " where i.orden = '" + txtPorder.Text.Trim() + "' group by orden, talla, area, d.Defecto, po.Posicion) b on a.POrderClient = b.POrderClient"
                                  + " group by a.descripcion, a.numero, a.POrderClient, a.Quantity, a.Estilo, b.Defecto";

                        consultap = DataAccess.Get_DataTable(sqlqueryp);
                        Session["porcent2"] = consultap;
                        grdporcent.DataBind();
                    }

                    break;

                case "2"://Estilo
                    if (txtstyle.Text != string.Empty && hdnidstyle.Value != string.Empty)
                    {
                        //sqlquery = " select pl.descripcion Planta, 'Linea: ' + l.numero Linea, p.POrder Corte, p.Quantity, s.Style Estilo, i.talla Talla, i.Area, d.Defecto, po.Posicion, ISNULL(sum(i.unidades), 0) Cantidad"
                        //      + " from Irregulares i join POrder p on p.Id_Order = i.idOrder join Linea l on l.id_linea = p.Id_Linea join Style s on s.Id_Style = p.Id_Style"
                        //      + " join Planta pl on pl.id_planta = l.id_planta join tbDefectos d on d.idDefecto = i.idDefecto join tbPosicion po on po.idPosicion = i.idPosicion"
                        //      + " where s.Id_Style = " + hdnidstyle.Value + " and CONVERT(date,i.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                        //      + " group by pl.descripcion, l.numero, p.POrder, p.Quantity, s.Style, i.talla, i.Area, d.Defecto, po.Posicion";

                        sqlquery = " select a.descripcion Planta, a.numero Linea, a.POrderClient Corte, a.Quantity Quantity, a.Estilo, b.talla, b.area, b.Defecto, b.Posicion, SUM(b.Cantidad) Cantidad from"
                                 + " (select a1.*from(select pl.descripcion, l.numero, p.POrderClient, SUM(b.Quantity) Quantity, MIN(s.Style) Estilo from POrder p"
                                 + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Planta pl on pl.id_planta = l.id_planta"
                                 + " join (select distinct orden from IrregularesEnCrudo i join POrder p on p.POrderClient = i.orden join Style s on s.Id_Style = p.Id_Style"
                                 + " where CONVERT(date, fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and s.Id_Style = " + hdnidstyle.Value + ") ir on ir.orden = p.POrderClient"
                                 + " where s.Id_Style = " + hdnidstyle.Value + " group by pl.descripcion, l.numero, p.POrderClient) a1) a join"
                                 + " (select p.POrderClient, i.talla, i.area, d.Defecto, po.Posicion, ISNULL(SUM(i.unidades),0) Cantidad from IrregularesEnCrudo i"
                                 + " join POrder p on p.POrderClient = i.orden join Style s on s.Id_Style = p.Id_Style join tbDefectos d on d.idDefecto = i.idDefecto join tbPosicion po on po.idPosicion = i.idPosicion"
                                 + " where s.Id_Style = " + hdnidstyle.Value + " and CONVERT(date, i.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.POrderClient, i.talla, i.area, d.Defecto, po.Posicion"
                                 + " ) b on a.POrderClient = b.POrderClient group by a.descripcion, a.numero, a.POrderClient, a.Quantity, a.Estilo, b.talla, b.area, b.Defecto, b.Posicion";

                        consulta = DataAccess.Get_DataTable(sqlquery);
                        Session["irregulares2"] = consulta;
                        grdIrregular.DataBind();

                        //sqlqueryp = " select pl.descripcion Planta, 'Linea: ' + l.numero Linea, p.POrder Corte, p.Quantity, s.Style Estilo, d.Defecto, ISNULL(sum(i.unidades), 0) Cantidad,"
                        //              + " CAST(CAST(ISNULL(sum(i.unidades), 0) as decimal(16, 2)) / CAST(p.Quantity as decimal(16, 2)) * 100 as decimal(16, 2)) Porcentaje from Irregulares i"
                        //              + " join POrder p on p.Id_Order = i.idOrder join Linea l on l.id_linea = p.Id_Linea join Style s on s.Id_Style = p.Id_Style join Planta pl on pl.id_planta = l.id_planta"
                        //              + " join tbDefectos d on d.idDefecto = i.idDefecto where s.Id_Style = " + hdnidstyle.Value + " and CONVERT(date, i.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                        //              + " group by pl.descripcion, l.numero, p.POrder, p.Quantity, s.Style, d.Defecto";

                        sqlqueryp = " select a.descripcion Planta, a.numero Linea, a.POrderClient Corte, a.Quantity Quantity, a.Estilo, b.Defecto, SUM(b.Cantidad) Cantidad,"
                                  + " CAST(CAST(ISNULL(sum(b.Cantidad), 0) as decimal(16, 2)) / CAST(a.Quantity as decimal(16, 2)) * 100 as decimal(16, 2)) Porcentaje from("
                                  + " select a1.*from(select pl.descripcion, l.numero, p.POrderClient, SUM(b.Quantity) Quantity, MIN(s.Style) Estilo from POrder p"
                                  + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Planta pl on pl.id_planta = l.id_planta"
                                  + " join (select distinct orden from IrregularesEnCrudo i join POrder p on p.POrderClient = i.orden join Style s on s.Id_Style = p.Id_Style"
                                  + " where CONVERT(date, fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and s.Id_Style = " + hdnidstyle.Value + ") ir on ir.orden = p.POrderClient"
                                  + " where s.Id_Style = " + hdnidstyle.Value + " group by pl.descripcion, l.numero, p.POrderClient) a1) a join"
                                  + " (select p.POrderClient, i.talla, i.area, d.Defecto, po.Posicion, ISNULL(SUM(i.unidades),0) Cantidad from IrregularesEnCrudo i"
                                  + " join POrder p on p.POrderClient = i.orden join Style s on s.Id_Style = p.Id_Style join tbDefectos d on d.idDefecto = i.idDefecto join tbPosicion po on po.idPosicion = i.idPosicion"
                                  + " where s.Id_Style = " + hdnidstyle.Value + " and CONVERT(date, i.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.POrderClient, i.talla, i.area, d.Defecto, po.Posicion) b"
                                  + " on a.POrderClient = b.POrderClient group by a.descripcion, a.numero, a.POrderClient, a.Quantity, a.Estilo, b.Defecto order by a.numero, a.POrderClient";

                        consultap = DataAccess.Get_DataTable(sqlqueryp);
                        Session["porcent2"] = consultap;

                        grdporcent.DataBind();

                    }

                    break;

                case "3"://Linea
                    if (DropDownListLine.SelectedItem.Text != "Select...")
                    {
                        //sqlquery = " select pl.descripcion Planta, 'Linea: ' + l.numero Linea, p.POrder Corte, p.Quantity, s.Style Estilo, i.talla Talla, i.Area, d.Defecto, po.Posicion, ISNULL(sum(i.unidades), 0) Cantidad"
                        //      + " from Irregulares i join POrder p on p.Id_Order = i.idOrder join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style"
                        //      + " join Planta pl on pl.id_planta = l.id_planta join tbDefectos d on d.idDefecto = i.idDefecto join tbPosicion po on po.idPosicion = i.idPosicion"
                        //      + " where l.id_linea = " + DropDownListLine.SelectedValue + " and CONVERT(date,i.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                        //      + " group by pl.descripcion, l.numero, p.POrder, p.Quantity, s.Style, i.talla, i.Area, d.Defecto, po.Posicion";

                        sqlquery = " select a.descripcion Planta, a.numero Linea, a.POrderClient Corte, a.Quantity Quantity, a.Estilo, b.talla, b.area, b.Defecto, b.Posicion, SUM(b.Cantidad) Cantidad from"
                                 + " (select a1.*from(select pl.descripcion, l.numero, p.POrderClient, SUM(b.Quantity) Quantity, MIN(s.Style) Estilo from POrder p"
                                 + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Planta pl on pl.id_planta = l.id_planta"
                                 + " join (select distinct orden from IrregularesEnCrudo i join POrder p on p.POrderClient = i.orden join Linea l on l.id_linea = p.Id_Linea2"
                                 + " where CONVERT(date, fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and l.id_linea = " + DropDownListLine.SelectedValue + ") ir on ir.orden = p.POrderClient"
                                 + " where l.id_linea = " + DropDownListLine.SelectedValue + " group by pl.descripcion, l.numero, p.POrderClient) a1) a join"
                                 + " (select p.POrderClient, i.talla, i.area, d.Defecto, po.Posicion, ISNULL(SUM(i.unidades),0) Cantidad from IrregularesEnCrudo i"
                                 + " join POrder p on p.POrderClient = i.orden join Style s on s.Id_Style = p.Id_Style join tbDefectos d on d.idDefecto = i.idDefecto join tbPosicion po on po.idPosicion = i.idPosicion"
                                 + " where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, i.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.POrderClient, i.talla, i.area, d.Defecto, po.Posicion) b"
                                 + " on a.POrderClient = b.POrderClient group by a.descripcion, a.numero, a.POrderClient, a.Quantity, a.Estilo, b.talla, b.area, b.Defecto, b.Posicion order by a.POrderClient";

                        consulta = DataAccess.Get_DataTable(sqlquery);
                        Session["irregulares2"] = consulta;
                        grdIrregular.DataBind();

                        //sqlqueryp = " select pl.descripcion Planta, 'Linea: ' + l.numero Linea, p.POrder Corte, p.Quantity, s.Style Estilo, d.Defecto, ISNULL(sum(i.unidades), 0) Cantidad,"
                        //          + " CAST(CAST(ISNULL(sum(i.unidades), 0) as decimal(16, 2)) / CAST(p.Quantity as decimal(16, 2)) * 100 as decimal(16, 2)) Porcentaje from Irregulares i"
                        //          + " join POrder p on p.Id_Order = i.idOrder join Linea l on l.id_linea = p.Id_Linea join Style s on s.Id_Style = p.Id_Style join Planta pl on pl.id_planta = l.id_planta"
                        //          + " join tbDefectos d on d.idDefecto = i.idDefecto where l.id_linea = " + DropDownListLine.SelectedValue + " and CONVERT(date, i.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                        //          + " group by pl.descripcion, l.numero, p.POrder, p.Quantity, s.Style, d.Defecto";

                        sqlqueryp = " select a.descripcion Planta, a.numero Linea, a.POrderClient Corte, a.Quantity Quantity, a.Estilo, b.Defecto, SUM(b.Cantidad) Cantidad,"
                                  + " CAST(CAST(ISNULL(sum(b.Cantidad), 0) as decimal(16, 2)) / CAST(a.Quantity as decimal(16, 2)) * 100 as decimal(16, 2)) Porcentaje from"
                                  + " (select a1.*from(select pl.descripcion, l.numero, p.POrderClient, SUM(b.Quantity) Quantity, MIN(s.Style) Estilo from POrder p"
                                  + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Planta pl on pl.id_planta = l.id_planta"
                                  + " join (select distinct orden from IrregularesEnCrudo i join POrder p on p.POrderClient = i.orden join Linea l on l.id_linea = p.Id_Linea2"
                                  + " where CONVERT(date, fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and l.id_linea = " + DropDownListLine.SelectedValue + ") ir on ir.orden = p.POrderClient where l.id_linea = " + DropDownListLine.SelectedValue + ""
                                  + " group by pl.descripcion, l.numero, p.POrderClient) a1) a join(select p.POrderClient, i.talla, i.area, d.Defecto, po.Posicion, ISNULL(SUM(i.unidades),0) Cantidad from IrregularesEnCrudo i"
                                  + " join POrder p on p.POrderClient = i.orden join Style s on s.Id_Style = p.Id_Style join tbDefectos d on d.idDefecto = i.idDefecto join tbPosicion po on po.idPosicion = i.idPosicion"
                                  + " where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, i.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.POrderClient, i.talla, i.area, d.Defecto, po.Posicion) b"
                                  + " on a.POrderClient = b.POrderClient group by a.descripcion, a.numero, a.POrderClient, a.Quantity, a.Estilo, b.Defecto order by a.POrderClient";

                        consultap = DataAccess.Get_DataTable(sqlqueryp);
                        Session["porcent2"] = consultap;

                        grdporcent.DataBind();

                    }

                    break;

                case "4"://Planta
                    if (DropDownListPlant.SelectedItem.Text != "Select...")
                    {
                        //sqlquery = " select pl.descripcion Planta, 'Linea: ' + l.numero Linea, p.POrder Corte, p.Quantity, s.Style Estilo, i.talla Talla, i.Area, d.Defecto, po.Posicion, ISNULL(sum(i.unidades), 0) Cantidad"
                        //      + " from Irregulares i join POrder p on p.Id_Order = i.idOrder join Linea l on l.id_linea = p.Id_Linea join Style s on s.Id_Style = p.Id_Style"
                        //      + " join Planta pl on pl.id_planta = l.id_planta join tbDefectos d on d.idDefecto = i.idDefecto join tbPosicion po on po.idPosicion = i.idPosicion"
                        //      + " where l.id_planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date,i.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                        //      + " group by pl.descripcion, l.numero, p.POrder, p.Quantity, s.Style, i.talla, i.Area, d.Defecto, po.Posicion";

                        sqlquery = " select a.descripcion Planta, a.numero Linea, a.POrderClient Corte, a.Quantity Quantity, a.Estilo, b.talla, b.area, b.Defecto, b.Posicion, SUM(b.Cantidad) Cantidad from"
                                 + " (select a1.*from(select pl.descripcion, l.numero, p.POrderClient, SUM(b.Quantity) Quantity, MIN(s.Style) Estilo from POrder p"
                                 + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Planta pl on pl.id_planta = l.id_planta"
                                 + " join (select distinct orden from IrregularesEnCrudo i join POrder p on p.POrderClient = i.orden join Linea l on l.id_linea = p.Id_Linea2"
                                 + " where CONVERT(date, fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and l.id_planta = " + DropDownListPlant.SelectedValue + ") ir on ir.orden = p.POrderClient where l.id_planta = " + DropDownListPlant.SelectedValue + ""
                                 + " group by pl.descripcion, l.numero, p.POrderClient) a1) a join(select p.POrderClient, i.talla, i.area, d.Defecto, po.Posicion, ISNULL(SUM(i.unidades),0) Cantidad from IrregularesEnCrudo i"
                                 + " join POrder p on p.POrderClient = i.orden join Style s on s.Id_Style = p.Id_Style join tbDefectos d on d.idDefecto = i.idDefecto join tbPosicion po on po.idPosicion = i.idPosicion"
                                 + " where p.Id_Planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date, i.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.POrderClient, i.talla, i.area, d.Defecto, po.Posicion) b on a.POrderClient = b.POrderClient"
                                 + " group by a.descripcion, a.numero, a.POrderClient, a.Quantity, a.Estilo, b.talla, b.area, b.Defecto, b.Posicion order by a.numero, a.POrderClient";

                        consulta = DataAccess.Get_DataTable(sqlquery);
                        Session["irregulares2"] = consulta;

                        grdIrregular.DataBind();

                        //sqlqueryp = " select pl.descripcion Planta, 'Linea: ' + l.numero Linea, p.POrder Corte, p.Quantity, s.Style Estilo, d.Defecto, ISNULL(sum(i.unidades), 0) Cantidad,"
                        //          + " CAST(CAST(ISNULL(sum(i.unidades), 0) as decimal(16, 2)) / CAST(p.Quantity as decimal(16, 2)) * 100 as decimal(16, 2)) Porcentaje from Irregulares i"
                        //          + " join POrder p on p.Id_Order = i.idOrder join Linea l on l.id_linea = p.Id_Linea join Style s on s.Id_Style = p.Id_Style join Planta pl on pl.id_planta = l.id_planta"
                        //          + " join tbDefectos d on d.idDefecto = i.idDefecto where l.id_planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date, i.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                        //          + " group by pl.descripcion, l.numero, p.POrder, p.Quantity, s.Style, d.Defecto";

                        sqlqueryp = " select a.descripcion Planta, a.numero Linea, a.POrderClient Corte, a.Quantity Quantity, a.Estilo, b.Defecto, SUM(b.Cantidad) Cantidad,"
                                  + " CAST(CAST(ISNULL(sum(b.Cantidad), 0) as decimal(16, 2)) / CAST(a.Quantity as decimal(16, 2)) * 100 as decimal(16, 2)) Porcentaje from(select a1.*from"
                                  + " (select pl.descripcion, l.numero, p.POrderClient, SUM(b.Quantity) Quantity, MIN(s.Style) Estilo from POrder p"
                                  + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Planta pl on pl.id_planta = l.id_planta"
                                  + " join (select distinct orden from IrregularesEnCrudo i join POrder p on p.POrderClient = i.orden join Linea l on l.id_linea = p.Id_Linea2"
                                  + " where CONVERT(date, fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and l.id_planta = " + DropDownListPlant.SelectedValue + ") ir on ir.orden = p.POrderClient where l.id_planta = " + DropDownListPlant.SelectedValue + ""
                                  + " group by pl.descripcion, l.numero, p.POrderClient) a1) a join(select p.POrderClient, i.talla, i.area, d.Defecto, po.Posicion, ISNULL(SUM(i.unidades),0) Cantidad"
                                  + " from IrregularesEnCrudo i join POrder p on p.POrderClient = i.orden join Style s on s.Id_Style = p.Id_Style join tbDefectos d on d.idDefecto = i.idDefecto"
                                  + " join tbPosicion po on po.idPosicion = i.idPosicion where p.Id_Planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date, i.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                  + " group by p.POrderClient, i.talla, i.area, d.Defecto, po.Posicion) b on a.POrderClient = b.POrderClient"
                                  + " group by a.descripcion, a.numero, a.POrderClient, a.Quantity, a.Estilo, b.Defecto order by a.numero, a.POrderClient";

                        consultap = DataAccess.Get_DataTable(sqlqueryp);
                        Session["porcent2"] = consultap;

                        grdporcent.DataBind();
                    }

                    break;

                default: break;
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin/IrregularesReport2.aspx");
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            export.GridViewID = "grdIrregular";
            exportPorcent.GridViewID = "grdporcent";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = export;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link2.Component = exportPorcent;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link2, link3 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Irregulares";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Irregulares_" + DateTime.Now.ToShortDateString() + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            throw;
        }
    }

    protected void grdIrregulares_DataBinding(object sender, EventArgs e)
    {
        grdIrregular.DataSource = Session["irregulares2"];
        //grdIrregular.GroupBy(grdIrregular.Columns["Planta"]);
        //grdIrregular.GroupBy(grdIrregular.Columns["Linea"]);
        //grdIrregular.ExpandAll();
    }

    protected void grdIrregulares_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdIrregular.DataBind();
    }

    protected void grdporcent_DataBinding(object sender, EventArgs e)
    {
        grdporcent.DataSource = Session["porcent2"];
    }

    protected void grdporcent_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdporcent.DataBind();
    }
}