﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportMasterRoc7.aspx.cs" Inherits="ReportMasterRoc7" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">

        <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

        <div class="panel panel-primary" style="margin-top:5px">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Measurement Report </strong></h3>
            </div>
            <div class="panel-body">


                <div class="col-lg-4">
                    Date 1
                      <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>
                    Date 2
                      <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>

                    Process
                      <asp:DropDownList ID="DropDownListProcess" CssClass="form-control" OnSelectedIndexChanged="DropDownListProcess_SelectedIndexChanged" AutoPostBack="true" runat="server">
                          <asp:ListItem Value="0"> Select...</asp:ListItem>
                          <asp:ListItem Value="1"> Produccion</asp:ListItem>
                          <asp:ListItem Value="2"> AfterWash</asp:ListItem>
                          <asp:ListItem Value="3"> Audit Final</asp:ListItem>
                      </asp:DropDownList>
                    <asp:Panel ID="Panel1" Visible="false" runat="server">
                        Planta
                      <asp:DropDownList ID="plantad" CssClass="form-control" AutoPostBack="false" runat="server">
                          <asp:ListItem Value="0"> Select...</asp:ListItem>
                          <asp:ListItem Value="1"> Rocedes 1</asp:ListItem>
                          <asp:ListItem Value="2"> Rocedes 3</asp:ListItem>
                          <asp:ListItem Value="3"> Rocedes 6</asp:ListItem>
                          <asp:ListItem Value="4"> All the Plants</asp:ListItem>
                      </asp:DropDownList>
                        Seccion 
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                            <asp:ListItem Value="bultoabulto">Bulto a bulto</asp:ListItem>
                            <asp:ListItem Value="inspInter">Inspeccion Final</asp:ListItem>
                        </asp:RadioButtonList>
                    </asp:Panel>
                </div>

                <div class="col-lg-12" style="padding-top: 3px">

                    <asp:Button ID="btnGenerar" OnClick="btnGenerar_Click" CssClass="btn btn-primary " Width="300px" runat="server" Text="Generate Report" />
                    <br />
                    <asp:Button ID="btnExport" OnClick="btnExport_Click" CssClass="btn btn-success " Width="300px" runat="server" Text="Export Excel" />
                    <hr />
                </div>

                <div class="col-lg-12">
                    <dxchartsui:WebChartControl ID="packet_chart" Width="1000px" Height="394px"
                        runat="server" AppearanceNameSerializable="Pastel Kit" CrosshairEnabled="True"
                        SeriesDataMember="NombreMedida">
                        <BorderOptions Visibility="False" />
                        <BorderOptions Visibility="False"></BorderOptions>
                        <DiagramSerializable>
                            <cc1:XYDiagram
                                RangeControlDateTimeGridOptions-GridMode="ChartGrid">
                                <AxisX VisibleInPanesSerializable="-1">
                                    <GridLines Visible="True">
                                    </GridLines>
                                </AxisX>
                                <AxisY VisibleInPanesSerializable="-1">
                                </AxisY>
                            </cc1:XYDiagram>
                        </DiagramSerializable>
                        <SeriesTemplate ArgumentDataMember="userName" LabelsVisibility="True" CrosshairLabelPattern="{S}:{V}"
                            ValueDataMembersSerializable="total">
                            <LabelSerializable>
                                <cc1:SideBySideBarSeriesLabel Position="Top" TextPattern="Qty:{V}"
                                    Indent="5" LineLength="10" LineVisibility="True"
                                    ResolveOverlappingMode="Default">
                                </cc1:SideBySideBarSeriesLabel>
                            </LabelSerializable>
                        </SeriesTemplate>
                        <Titles>
                            <cc1:ChartTitle Text="Report Measurement Of Line "></cc1:ChartTitle>
                        </Titles>
                    </dxchartsui:WebChartControl>
                </div>
                <div class="col-lg-12">

                    <dx:ASPxGridView ID="ASPxGridView2" OnDataBinding="ASPxGridView2_DataBinding" OnCustomCallback="ASPxGridView2_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="total" Caption="Puntos Medida" />
                            <dx:GridViewDataTextColumn FieldName="waist" Caption="Waist" />
                            <dx:GridViewDataTextColumn FieldName="inseam" Caption="Inseam" />
                            <dx:GridViewDataTextColumn FieldName="frontrise" Caption="Front Rise" />
                            <dx:GridViewDataTextColumn FieldName="backrise" Caption="Back Rise" />
                            <dx:GridViewDataTextColumn FieldName="hip" Caption="Hip" />
                        </Columns>
                        <%-- <Settings ShowFooter="True" />
                                <TotalSummary>
                                    <dx:ASPxSummaryItem FieldName="WAIST" ShowInGroupFooterColumn="WAIST" SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName="INSEAM" ShowInGroupFooterColumn="INSEAM" SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName="FRONT_RISE" ShowInGroupFooterColumn="FRONT_RISE" SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName="BACK_RISE" ShowInGroupFooterColumn="BACK_RISE" SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName="HIP" ShowInGroupFooterColumn="HIP" SummaryType="Sum" />
                                </TotalSummary>--%>
                    </dx:ASPxGridView>
                </div>
                <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

