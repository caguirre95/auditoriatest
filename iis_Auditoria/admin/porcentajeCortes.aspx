﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="porcentajeCortes.aspx.cs" Inherits="admin_porcentajeCortes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
           <%-- $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })--%>
        });
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong> Percent Measurement Report </strong></h3>
            </div>
            <div class="panel-body">


                <div class="col-lg-4">
                    Date 1
                      <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>
                   <%-- Date 2
                      <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>--%>
                   <%-- Process
                      <asp:DropDownList ID="DropDownListProcess" CssClass="form-control" OnSelectedIndexChanged="DropDownListProcess_SelectedIndexChanged" AutoPostBack="true" runat="server">
                          <asp:ListItem Value="0"> Select...</asp:ListItem>
                          <asp:ListItem Value="1"> Produccion</asp:ListItem>
                          <asp:ListItem Value="2"> AfterWash</asp:ListItem>
                          <asp:ListItem Value="3"> Audit Final</asp:ListItem>
                      </asp:DropDownList>
                    <asp:Panel ID="Panel1" Visible="false" runat="server">
                    Seccion 
                        <asp:RadioButtonList ID="RadioButtonList1"  runat="server">
                            <asp:ListItem Value="bultoabulto">Bulto a bulto</asp:ListItem>
                            <asp:ListItem Value="inspInter">Inspeccion Final</asp:ListItem>
                        </asp:RadioButtonList>
                    </asp:Panel>--%>
                </div>

                <div class="col-lg-12" style="padding-top:3px" >

                    <asp:Button ID="btnGenerar" OnClick="btnGenerar_Click" CssClass="btn btn-primary " Width="300px" runat="server" Text="Generate Report" />
                    <br />
                    <asp:Button ID="btnExport" OnClick="btnExport_Click" CssClass="btn btn-success "  Width="300px" runat="server" Text="Export Excel" />
                    <hr />
                </div>
            
                <div class="col-lg-12">                
                    <dx:ASPxGridView ID="ASPxGridView2" OnDataBinding="ASPxGridView2_DataBinding" OnCustomCallback="ASPxGridView2_CustomCallback" Width="100%" Theme="Metropolis" runat="server">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="porder" Caption="Porder" />
                                    <dx:GridViewDataTextColumn FieldName="porcent" Caption="percent " />                               
                                </Columns>                         
                    </dx:ASPxGridView>     
                </div>

                <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>

            </div>
        </div>
  </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

