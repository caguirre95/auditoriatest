﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        RegistrarRutas(RouteTable.Routes);
        // Código que se ejecuta al iniciarse la aplicación
    }

    void RegistrarRutas(RouteCollection rutas)
    {
        rutas.MapPageRoute("DashBoard", "DashBoard", "~/DashMedidasEmpaque.aspx");
        rutas.MapPageRoute("Muestra", "Muestra", "~/DashMedidasMuestra.aspx");
        rutas.MapPageRoute("Especificaciones", "Especificaciones", "~/AdministrarEstilosYTallas.aspx");
        rutas.MapPageRoute("Spec", "Spec", "~/AdminStyleSize.aspx");
        rutas.MapPageRoute("Reportes", "Reportes", "~/adminRepMedidasNuevos/ReporteMedidaMasterGuia.aspx");/*Guia reportes medida*/
        /*Ruta de reportes*/
        //rutas.MapPageRoute("ReportOne","ReportOne","~/adminRepMedidasNuevos/ReportMedidaEmpaque.aspx");
        //rutas.MapPageRoute("ReportTwo","ReportTwo","~/adminRepMedidasNuevos/DashboardRep.aspx");
        //rutas.MapPageRoute("ReportThree","ReportThree","~/adminRepMedidasNuevos/ReportLineMasterEmpaque.aspx");
        //rutas.MapPageRoute("ReportFour","ReportFour","~/adminRepMedidasNuevos/PorcentajeReporte.aspx");
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Código que se ejecuta al cerrarse la aplicación
    }

    void Application_Error(object sender, EventArgs e)
    {
        // Código que se ejecuta cuando se produce un error sin procesar      
    }

    void Session_Start(object sender, EventArgs e)
    {
        // Código que se ejecuta al iniciarse una nueva sesión
    }

    void Session_End(object sender, EventArgs e)
    {
        // Código que se ejecuta cuando finaliza una sesión. 
        // Nota: el evento Session_End se produce solamente con el modo sessionstate
        // se establece como InProc en el archivo Web.config. Si el modo de sesión se establece como StateServer
        // o SQLServer, el evento no se produce.
    }

</script>
