﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Style.aspx.cs" Inherits="Style" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v16.2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function oki() {
            swal({
                title: 'Exito!',
                text: "Registro Actualizado Correctamente",
                type: 'success'
            });
        }

        function oki_bad() {
            swal({
                title: 'Ups!',
                text: "No se Actualizo el Registro",
                type: 'warning'
            });
        }

        function oki_bad2() {
            swal({
                title: 'Ups!',
                text: "Ingrese los datos de manera correcta",
                type: 'warning'
            });
        }

        function oki_bad1(a) {
            swal({
                title: 'Ups!',
                text: "Ya existe el estilo : " + a,
                type: 'warning'
            });
        }

        function OnGetSelectedFieldValues1(selectedValues) {
            if (selectedValues.length == 0) return;
            for (i = 0; i < selectedValues.length; i++) {

                for (j = 0; j < selectedValues[i].length; j++) {
                    idStyle.Set('id', selectedValues[i][0]);
                    txtstyle.SetText(selectedValues[i][1]);
                    txtpricenew.SetText(selectedValues[i][2]);
                }
                PopupEditarStyle.Show();
            }
        }
        function ChangeType(s, newType) {
            s.GetInputElement().type = newType;
        }
    </script>
    <style>
        .mar {
            margin-top: 10px;
        }
    </style>

    <%--<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="/bootstrap/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato" />
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/main.css" rel="stylesheet" type="text/css" />

    <link href="/font-awesome/fonts.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css" />
    <script src="/sweetalert-master/dist/sweetalert.min.js"></script>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="container mar">
        <div class="panel panel-primary">
            <div class="panel-heading"><strong>Style</strong></div>
            <div class="panel-body">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <dx:ASPxGridView ID="gridStyle" runat="server" AutoGenerateColumns="False" KeyFieldName="id" Width="100%" Theme="Material" ClientInstanceName="gridStyle" ClientVisible="true">
                            <SettingsSearchPanel Visible="True" />
                            <SettingsBehavior AllowSelectSingleRowOnly="True" />
                            <Columns>
                                <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0" Caption="Id" Visible="false">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="false" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Style" VisibleIndex="2" Width="350px">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Price" VisibleIndex="3" Width="350px">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Wash" VisibleIndex="4" Width="350px">
                                    <DataItemTemplate>
                                        <dx:ASPxCheckBox ID="chkstylew" runat="server" Theme="Material" Text="Wash" Checked='<%# Bind("Washed") %>' ValueChecked="true" ValueUnchecked="false"></dx:ASPxCheckBox>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" />
                            <SettingsPager PageSize="5" />
                        </dx:ASPxGridView>

                        <div style="margin-top: 30px;">
                            <div class="col-lg-2">
                                <button id="btnaddStyle" runat="server" class="btn btn-primary" data-toggle="modal" data-target="#ventanamodal" style="width: 150px; height: 40px">Add Style</button>
                            </div>
                            <div class="col-lg-2">
                                <dx:ASPxButton ID="btnwash" runat="server" Text="Change Wash" CssClass="btn btn-primary" Width="150px" OnClick="btnestadoact_Click"></dx:ASPxButton>
                            </div>
                            <div class="col-lg-2">
                                <%--<dx:ASPxButton ID="btneditarstyle" runat="server" Text="Edit Style" CssClass="btn btn-primary" Width="150px" AutoPostBack="false" ClientInstanceName="btneditarstyle" ClientVisible="true" UseSubmitBehavior="False">
                                    <ClientSideEvents Click="function(s, e) {
                                gridStyle.GetSelectedFieldValues('id;Style;Wash', OnGetSelectedFieldValues1);
                            }" />
                                </dx:ASPxButton>--%>
                                <button id="Button1" runat="server" class="btn btn-primary" data-toggle="modal" data-target="#ventanaStyle" style="width: 150px; height: 40px">Edit Style</button>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--<dx:ASPxPopupControl ID="PopupEditarStyle" runat="server" EnableTheming="True" ClientInstanceName="PopupEditarStyle" HeaderText="Edit Style" Modal="True"
        Theme="MetropolisBlue" AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" CloseAction="CloseButton" Width="650px" MaxHeight="600px">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxPanel ID="PanelEditarStyle" runat="server">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                            <div class="col-lg-12">
                                <div class="col-lg-12" style="margin-top: 5px">
                                    <div class="col-lg-6">
                                        <dx:ASPxHiddenField ID="idStyle" runat="server" ClientInstanceName="idStyle"></dx:ASPxHiddenField>                                       
                                        <asp:Label ID="lblnm" runat="server" Text="" Visible="false"></asp:Label>
                                        <strong><span>Style</span></strong>
                                    </div>
                                    <div class="col-lg-6">
                                        <dx:ASPxTextBox ID="txtstyle" runat="server" NullText="Style" Width="280px" Theme="Mulberry" ClientInstanceName="txtstyle">
                                            <ClientSideEvents KeyUp="function(s, e) {
                                                        s.SetText(s.GetText().toUpperCase());
                                                        }" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </div>
                                <div class="col-lg-12" style="margin-top: 5px">
                                    <div class="col-lg-6">
                                        <strong><span>Price</span></strong>
                                    </div>
                                    <div class="col-lg-6">
                                        <dx:ASPxTextBox ID="txtpricenew" runat="server" NullText="Price" Width="280px" Theme="Mulberry" ClientInstanceName="txtpricenew">
                                            <ClientSideEvents KeyUp="function(s, e) {
                                                        s.SetText(s.GetText().toUpperCase());
                                                        }" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12" style="text-align: center; margin-top: 10px">
                                <dx:ASPxButton ID="btnStyle" runat="server" Text="Guardar" CssClass="btn btn-primary" Width="150px" OnClick="btnStyle_Click"></dx:ASPxButton>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>--%>

    <div class="modal fade" id="ventanamodal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Style</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Style</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxTextBox ID="txtstylenew" runat="server" NullText="Style..." Theme="Material" Width="180px" MaxLength="20" AutoCompleteType="Disabled">
                                        <ClientSideEvents KeyUp="function(s, e) {
                                                        s.SetText(s.GetText().toUpperCase());
                                                        }" />
                                    </dx:ASPxTextBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Price</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxTextBox ID="txtprice" runat="server" NullText="Price..." Theme="Material" Width="180px" MaxLength="6" AutoCompleteType="Disabled">
                                        <%-- <ClientSideEvents Init="function(s, e)
                                         {
                                          ChangeType(s, 'number');
                                         }" />--%>
                                    </dx:ASPxTextBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Wash</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxCheckBox ID="chkwash" runat="server" ValueChecked="true" ValueUnchecked="false" Theme="Material"></dx:ASPxCheckBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-12" style="text-align: center;">
                                    <dx:ASPxButton ID="btnagregarnewline" runat="server" Theme="Material" Text="Save" Width="150px" OnClick="btnagregarnewline_Click"></dx:ASPxButton>
                                </div>
                            </div>
                            <br />
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnagregarnewline" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ventanaStyle" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Style</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Choice Style</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxComboBox ID="cmbstyle" runat="server" NullText="Style" Theme="Material" Width="180px" OnSelectedIndexChanged="cmbstyle_SelectedIndexChanged" AutoPostBack="true"></dx:ASPxComboBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Style</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxTextBox ID="txtstilo" runat="server" NullText="Style..." Theme="Material" Width="180px" AutoCompleteType="Disabled">
                                        <ClientSideEvents KeyUp="function(s, e) {
                                                        s.SetText(s.GetText().toUpperCase());
                                                        }" />
                                    </dx:ASPxTextBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Price</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxTextBox ID="txtpriceedit" runat="server" NullText="Price..." Theme="Material" Width="180px" MaxLength="6" AutoCompleteType="Disabled">
                                    </dx:ASPxTextBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Wash</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxCheckBox ID="checkboxstyle" runat="server" ValueChecked="true" ValueUnchecked="false" Theme="Material"></dx:ASPxCheckBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-12" style="text-align: center;">
                                    <dx:ASPxButton ID="btnedit" runat="server" Theme="Material" Text="Save" Width="150px" OnClick="btnedit_Click"></dx:ASPxButton>
                                </div>
                            </div>
                            <br />
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnedit" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

