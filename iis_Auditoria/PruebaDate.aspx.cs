﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PruebaDate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtdate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }
    }

    void bioIng()
    {
        try
        {
            string biho = " select Line,"
                   + " Quantity,"
                   + " POrder,"
                   + " isnull([1],0) as '09:00am',"
                   + " isnull([2],0) as '11:00am',"
                   + " isnull([3],0) as '01:00pm',"
                   + " isnull([4],0) as '03:00pm',"
                   + " isnull([5],0) as '04:50pm'"
                   + " from"
                   + " ("
                   + " select l.numero as Line,"
                   + " p.Quantity,"
                   + " p.POrder,"
                   + " he.idBihorario,"
                   + " isnull(sum(he.cantidadagregada), 0) as Cantidad"
                   + " from tblhistoricoenvio he"
                   + " join tblEnviosbulto eb on eb.idEnvio = he.idEnvio"
                   + " left join Bundle b on b.Id_Bundle = eb.idBulto"
                   + " join POrder p on p.Id_Order = b.Id_Order"
                   + " join Linea l on l.id_linea = p.Id_Linea"
                   + " where isnull(he.idBihorario, 0) != 0"
                   //+ " and isnull(he.enviodia, 0) = 0"  
                   + " and CONVERT(date,he.fechaBihorario) = '" + txtdate.Text + "'"
                   + " group by p.POrder, he.idBihorario, l.numero, p.Quantity"
                   + " ) main"
                   + " pivot"
                   + " ("
                   + " sum(Cantidad) for idBihorario in "
                   + " ("
                   + " [1] , [2], [3], [4], [5]"
                   + " )"
                   + " ) as sec";
            DataTable bihor1 = DataAccess.Get_DataTable(biho);
            if (bihor1.Rows.Count > 0)
            {
                DataColumn def = new DataColumn("Deficit", typeof(string));
                bihor1.Columns.Add(def);
                string d = " select p.POrder,"
                         + " (isnull(sum(he.cantidadagregada),0) - p.Quantity) as Deficit"
                         + " from tblhistoricoenvio he"
                         + " join tblEnviosbulto eb on eb.idEnvio = he.idEnvio"
                         + " left join Bundle b on b.Id_Bundle = eb.idBulto"
                         + " join POrder p on p.Id_Order = b.Id_Order"
                         + " join Linea l on l.id_linea = p.Id_Linea"
                         + " where isnull(he.idBihorario,0) != 0"
                         //+ " and convert(date,he.fechaBihorario) = '" + txtdate.Text + "'"
                         + " group by p.POrder,l.numero, p.Quantity";
                DataTable df = DataAccess.Get_DataTable(d);

                for (int i = 0; i < df.Rows.Count; i++)
                {
                    for (int j = 0; j < bihor1.Rows.Count; j++)
                    {
                        if (df.Rows[i][0].ToString().Equals(bihor1.Rows[j][2].ToString()))
                        {
                            bihor1.Rows[j][8] = df.Rows[i][1];
                        }
                    }
                }
                grdbiho.DataSource = bihor1;
                grdbiho.DataBind();
                grdbiho.GroupBy(grdbiho.Columns["Line"]);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio('" + txtdate.Text + "');", true);
                grdbiho.DataSource = "";
                grdbiho.DataBind();
            }
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    void bioEnvio()
    {
        string biho = " select Line,"
                    + " Quantity,"
                    + " POrder,"
                    + " isnull([1],0) as '09:00am',"
                    + " isnull([2],0) as '11:00am',"
                    + " isnull([3],0) as '01:00pm',"
                    + " isnull([4],0) as '03:00pm',"
                    + " isnull([5],0) as '04:50pm'"
                    + " from"
                    + " ("
                    + " select l.numero as Line,"
                    + " p.Quantity,"
                    + " p.POrder,"
                    + " he.idBihorario,"
                    + " isnull(sum(he.cantidadagregada),0) as Cantidad"
                    + " from tblhistoricoenvio he"
                    + " join tblEnviosbulto eb on eb.idEnvio = he.idEnvio"
                    + " left join Bundle b on b.Id_Bundle = eb.idBulto"
                    + " join POrder p on p.Id_Order = b.Id_Order"
                    + " join Linea l on l.id_linea = p.Id_Linea"
                    + " where isnull(he.numEnvioCorte,0) != 0"
                    + " group by p.POrder, he.idBihorario, l.numero, p.Quantity"
                    + " ) main"
                    + " pivot"
                    + " ("
                    + " sum(Cantidad) for idBihorario in "
                    + " ("
                    + " [1] , [2], [3], [4], [5]"
                    + " )"
                    + " ) as sec";
        DataTable bihor1 = DataAccess.Get_DataTable(biho);
        grdbiho.DataSource = bihor1;
        grdbiho.DataBind();
    }

    protected void grdbiho_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "TotalBihorario")
        {
            int a1 = Convert.ToInt32(e.GetListSourceFieldValue("09:00am"));
            int a2 = Convert.ToInt32(e.GetListSourceFieldValue("11:00am"));
            int a3 = Convert.ToInt32(e.GetListSourceFieldValue("01:00pm"));
            int a4 = Convert.ToInt32(e.GetListSourceFieldValue("03:00pm"));
            int a5 = Convert.ToInt32(e.GetListSourceFieldValue("04:50pm"));
            //decimal price = (decimal)e.GetListSourceFieldValue("UnitPrice");
            //int quantity = Convert.ToInt32(e.GetListSourceFieldValue("Quantity"));
            e.Value = a1 + a2 + a3 + a4 + a5;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            if (drpop.SelectedValue == "0" || txtdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio1();", true);
            }
            else if (drpop.SelectedValue == "1")
            {
                bioIng();
            }
            else if (drpop.SelectedValue == "2")
            {
                bioEnvio();
            }
            exportar.GridViewID = "grdbiho";
            exportar.FileName = "Reporte de Bihorario " + DateTime.Now;
            exportar.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }

    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            if (drpop.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio1();", true);
            }
            else if (drpop.SelectedValue == "1")
            {
                bioIng();
            }
            else if (drpop.SelectedValue == "2")
            {
                bioEnvio();
            }
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void grdbiho_DataBinding(object sender, EventArgs e)
    {
        try
        {
            string biho = " select Line,"
                           + " Quantity,"
                           + " POrder,"
                           + " isnull([1],0) as '09:00am',"
                           + " isnull([2],0) as '11:00am',"
                           + " isnull([3],0) as '01:00pm',"
                           + " isnull([4],0) as '03:00pm',"
                           + " isnull([5],0) as '04:50pm'"
                           + " from"
                           + " ("
                           + " select l.numero as Line,"
                           + " p.Quantity,"
                           + " p.POrder,"
                           + " he.idBihorario,"
                           + " isnull(sum(he.cantidadagregada), 0) as Cantidad"
                           + " from tblhistoricoenvio he"
                           + " join tblEnviosbulto eb on eb.idEnvio = he.idEnvio"
                           + " left join Bundle b on b.Id_Bundle = eb.idBulto"
                           + " join POrder p on p.Id_Order = b.Id_Order"
                           + " join Linea l on l.id_linea = p.Id_Linea"
                           + " where isnull(he.idBihorario, 0) != 0"
                           //+ " and isnull(he.enviodia, 0) = 0"  
                           + " and CONVERT(date,he.fechaBihorario) = '" + txtdate.Text + "'"
                           + " group by p.POrder, he.idBihorario, l.numero, p.Quantity"
                           + " ) main"
                           + " pivot"
                           + " ("
                           + " sum(Cantidad) for idBihorario in "
                           + " ("
                           + " [1] , [2], [3], [4], [5]"
                           + " )"
                           + " ) as sec";
            DataTable bihor1 = DataAccess.Get_DataTable(biho);
            if (bihor1.Rows.Count > 0)
            {
                DataColumn def = new DataColumn("Deficit", typeof(string));
                bihor1.Columns.Add(def);
                string d = " select p.POrder,"
                         + " (isnull(sum(he.cantidadagregada),0) - p.Quantity) as Deficit"
                         + " from tblhistoricoenvio he"
                         + " join tblEnviosbulto eb on eb.idEnvio = he.idEnvio"
                         + " left join Bundle b on b.Id_Bundle = eb.idBulto"
                         + " join POrder p on p.Id_Order = b.Id_Order"
                         + " join Linea l on l.id_linea = p.Id_Linea"
                         + " where isnull(he.idBihorario,0) != 0"
                         //+ " and convert(date,he.fechaBihorario) = '" + txtdate.Text + "'"
                         + " group by p.POrder,l.numero, p.Quantity";
                DataTable df = DataAccess.Get_DataTable(d);

                for (int i = 0; i < df.Rows.Count; i++)
                {
                    for (int j = 0; j < bihor1.Rows.Count; j++)
                    {
                        if (df.Rows[i][0].ToString().Equals(bihor1.Rows[j][2].ToString()))
                        {
                            bihor1.Rows[j][8] = df.Rows[i][1];
                        }
                    }
                }
                grdbiho.DataSource = bihor1;
                grdbiho.GroupBy(grdbiho.Columns["Line"]);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio();", true);
            }
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void grdbiho_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdbiho.DataBind();
    }
}