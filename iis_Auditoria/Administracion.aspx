﻿<%@ Page Title="Admon" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Administracion.aspx.cs" Inherits="Administracion" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v16.2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">



    <script type="text/javascript">

        function test_close() {
            var d1 = document.getElementById('panel1');
            var d2 = document.getElementById('panel2');

            d1.style.display = 'none';
            d2.style.display = 'block';
        };

        function cerrar_modal() {
            $(".modal").hide();
            var d1 = document.getElementById('panel1');
            var d2 = document.getElementById('panel2');

            d1.style.display = 'block';
            d2.style.display = 'none';

        };

        function test1() {
            var d1 = document.getElementById('panel1');
            var d2 = document.getElementById('panel2');

            d1.style.display = 'block';
            d2.style.display = 'none';
        }

        function successalertelim1() {
            swal({
                title: 'Por Favor!',
                text: "Llene todos los campos!!",
                type: 'info'
            });
        }

        function successalert() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }

        function oki() {
            swal({
                title: 'Exito!',
                text: "Registro Actualizado Correctamente",
                type: 'success'
            });
        }

        function oki_bad() {
            swal({
                title: 'Ups!',
                text: "No se Actualizo el Registro",
                type: 'warning'
            });
        }

        function seccion_choice() {
            swal({
                title: 'Ups!',
                text: "Elija una seccion al inicio de la pagina",
                type: 'warning'
            });
        }

        function menor1() {
            swal({
                title: 'Ups!',
                text: "Ocurrio un error en la insercion, intentelo nuevamente!!",
                type: 'warning'
            });
        }

        function gridcampo() {
            swal({
                title: 'Por Favor!',
                text: "Seleccione Linea y Seccion!!",
                type: 'info'
            });
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <style>
        .mar {
            margin-top: 10px;
        }
    </style>

    <div class="container mar">
        <div class="panel panel-primary">
            <div class="panel-heading"><strong>Administración</strong></div>
            <div class="panel-body">

                <div class="col-md-12 form-group">

                    <asp:UpdatePanel ID="up" runat="server">
                        <ContentTemplate>
                            Line:                
                    <asp:DropDownList ID="linea" runat="server" Width="180" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="linea_SelectedIndexChanged"></asp:DropDownList>
                            <br />
                            Section:
                    <asp:DropDownList ID="seccion" runat="server" Width="180" CssClass="form-control" OnSelectedIndexChanged="seccion_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="0">Select...</asp:ListItem>
                    </asp:DropDownList>

                            <br />

                            <div style="margin-top: 25px;">
                                <dx:ASPxGridView ID="grid_operario" runat="server" KeyFieldName="id" Width="100%" Theme="Material" OnDataBinding="grid_operario_DataBinding1" OnCustomCallback="grid_operario_CustomCallback">
                                    <SettingsDataSecurity AllowEdit="true" AllowInsert="true" />
                                    <SettingsSearchPanel Visible="true" />
                                    <SettingsBehavior AllowSelectSingleRowOnly="true" />
                                    <Columns>
                                        <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="true" VisibleIndex="0" Caption="Id" Visible="false">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn FieldName="id" ReadOnly="true" Visible="false" VisibleIndex="1">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Seccion" VisibleIndex="2">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Operacion" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Nombre" VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Estado" Caption="Estado" VisibleIndex="4">
                                            <DataItemTemplate>
                                                <dx:ASPxCheckBox ID="chkactive" runat="server" Theme="Material" Text="Estado" Checked='<%# Bind("Estado") %>' ValueChecked="true" ValueUnchecked="false" Enabled="true"></dx:ASPxCheckBox>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Settings ShowGroupFooter="VisibleAlways" />
                                    <SettingsPager PageSize="7" />
                                </dx:ASPxGridView>
                            </div>

                            <div class="col-md-12" style="margin-top: 30px">
                                <div class="col-lg-2">

                                    <button id="btnadd" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="width: 150px; height: 40px">Add Operator</button>
                                </div>

                                <div class="col-lg-2">
                                    <dx:ASPxButton ID="btnaddoper" runat="server" CssClass="btn btn-primary" Text="Enable Operator" Width="150px" ClientInstanceName="btnaddoper" ClientVisible="true" OnClick="btnaddoper_Click">
                                    </dx:ASPxButton>
                                </div>

                                <div class="col-lg-2">
                                    <button id="btnchang" type="button" class="btn btn-primary" data-toggle="modal" data-target="#winAct" style="width: 150px; height: 40px">Change Line</button>
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

            </div>
        </div>
    </div>

    <%--Ventana Modal Prueba--%>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <input type="button" class="close" data-dismiss="modal" value="X" onclick="cerrar_modal();" />
                    <h4 class="modal-title">Add Operator</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div style="margin-top: 5px">
                                <div class="col-lg-3" style="font-weight: bold; text-align: -webkit-right; margin-top: 10px">
                                    <span>Codigo Operario</span>
                                </div>
                                <div class="col-lg-9" style="margin-top: 10px">
                                    <dx:ASPxTextBox ID="txtcodigo" runat="server" NullText="Code..." Theme="Material" Width="350px" AutoCompleteType="Disabled">
                                        <ClientSideEvents KeyUp="function(s, e) {
                                                        s.SetText(s.GetText().toUpperCase());
                                                        }" />
                                    </dx:ASPxTextBox>
                                </div>
                            </div>

                            <div style="margin-top: 15px">
                                <div class="col-lg-3" style="font-weight: bold; text-align: -webkit-right; margin-top: 10px">
                                    <span>Descripcion</span>
                                </div>
                                <div class="col-lg-9" style="margin-top: 10px">
                                    <dx:ASPxTextBox ID="txtdescrip" runat="server" NullText="Descripcion..." Theme="Material" Width="350px" AutoCompleteType="Disabled">
                                        <ClientSideEvents KeyUp="function(s, e) {
                                                        s.SetText(s.GetText().toUpperCase());
                                                        }" />
                                    </dx:ASPxTextBox>
                                </div>
                            </div>

                            <div style="margin-top: 10px">
                                <div class="col-lg-3" style="font-weight: bold; text-align: -webkit-right; margin-top: 20px">
                                    <span>Nombre Operario</span>
                                </div>
                                <div class="col-lg-9" style="margin-top: 20px">
                                    <dx:ASPxTextBox ID="txtnombre" runat="server" NullText="Name..." Theme="Material" Width="350px" AutoCompleteType="Disabled">
                                        <ClientSideEvents KeyUp="function(s, e) {
                                                        s.SetText(s.GetText().toUpperCase());
                                                        }" />
                                    </dx:ASPxTextBox>
                                </div>
                            </div>

                            <div style="margin-top: 15px">
                                <div class="col-lg-12" style="font-weight: bold; text-align: center; margin-top: 20px">
                                    <span>Activo</span>
                                    <dx:ASPxCheckBox ID="chkactiv" runat="server" ValueChecked="true" ValueUnchecked="false" Theme="Material"></dx:ASPxCheckBox>
                                </div>
                            </div>

                            <%--<asp:Panel ID="paneloper1" runat="server" Visible="true">--%>
                            <div id="panel1" style="display: block">

                                <div style="margin-top: 15px">
                                    <div class="col-lg-3" style="font-weight: bold; text-align: -webkit-right; margin-top: 20px">
                                        <span>Operacion</span>
                                    </div>
                                    <div class="col-lg-9" style="margin-top: 20px">
                                        <dx:ASPxComboBox ID="combooper" runat="server" ValueType="System.String" Theme="Material" NullText="Operation..." Width="420px" Enabled="true"></dx:ASPxComboBox>
                                    </div>
                                </div>

                                <div style="margin-top: 15px">
                                    <div class="col-lg-12" style="font-weight: bold; text-align: center; margin-top: 20px">
                                        <span>Estado</span>
                                        <dx:ASPxCheckBox ID="chkstad" runat="server" ValueChecked="true" ValueUnchecked="false" Theme="Material" Enabled="true"></dx:ASPxCheckBox>
                                    </div>
                                </div>

                                <div class="col-md-12" style="margin-top: 50px; text-align: center">
                                    <%--<asp:Button ID="aggoper" runat="server" Text="Add Operation" CssClass="btn-success" BorderStyle="Solid" OnClick="aggoper_Click" Visible="true" />--%>
                                    <input type="button" onclick="test_close();" value="Add Operation" class="btn btn-primary" />
                                </div>
                            </div>
                            <%-- </asp:Panel>--%>

                            <%-- <asp:Panel ID="paneloper2" runat="server" Visible="false">--%>
                            <div id="panel2" style="display: none">

                                <div class="col-md-12" style="margin-top: 50px; text-align: center">
                                    <%-- <asp:Button ID="btnback" runat="server" Text="Back" CssClass="btn-success" BorderStyle="Solid" OnClick="btnback_Click" Visible="false" />--%>
                                    <input type="button" onclick="test1();" value="Back" class="btn btn-primary" />
                                </div>

                                <div style="margin-top: 5px">
                                    <div class="col-lg-3" style="font-weight: bold; text-align: -webkit-right; margin-top: 10px">
                                        <span>Descripcion</span>
                                    </div>
                                    <div class="col-lg-9" style="margin-top: 10px">
                                        <dx:ASPxTextBox ID="txtdescripcion" runat="server" NullText="Description..." Theme="Material" Width="350px" ClientInstanceName="txtdescripcion" Enabled="true" AutoCompleteType="Disabled">
                                            <ClientSideEvents KeyUp="function(s, e) {
                                                        s.SetText(s.GetText().toUpperCase());
                                                        }" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </div>

                                <div style="margin-bottom: 15px">
                                    <div class="col-lg-3" style="font-weight: bold; text-align: -webkit-right; margin-top: 20px">
                                        <span>Nombre Operacion</span>
                                    </div>
                                    <div class="col-lg-9" style="margin-top: 20px">
                                        <dx:ASPxTextBox ID="txtnombre1" runat="server" NullText="Name Operation..." Theme="Material" Width="350px" ClientInstanceName="txtnombre1" Enabled="true" AutoCompleteType="Disabled">
                                            <ClientSideEvents KeyUp="function(s, e) {
                                                        s.SetText(s.GetText().toUpperCase());
                                                        }" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </div>

                                <div style="margin-bottom: 15px">
                                    <div class="col-lg-12" style="font-weight: bold; text-align: center; margin-top: 20px">
                                        <span>Estado</span>
                                        <dx:ASPxCheckBox ID="chkstado2" runat="server" ValueChecked="true" ValueUnchecked="false" Theme="Material" Enabled="true"></dx:ASPxCheckBox>
                                    </div>
                                </div>

                            </div>
                            <%-- </asp:Panel>--%>

                            <div style="margin-bottom: 15px">
                                <div class="col-lg-12" style="text-align: center; margin-top: 30px">
                                    <dx:ASPxButton ID="btnaceptar" runat="server" Theme="Material" Text="Add" Width="150px" OnClick="btnaceptar_Click"></dx:ASPxButton>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer" style="margin-top: -40px">
                </div>
            </div>
        </div>
    </div>
    <%--Fin ventana modal--%>

    <div class="modal fade" id="winAct" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Line Operator</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Operario</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxComboBox ID="cmboperario" runat="server" NullText="Operario" Theme="Material" Width="180px" AutoPostBack="true"></dx:ASPxComboBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Linea</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxComboBox ID="cmbline" runat="server" NullText="Line" Theme="Material" Width="180px" AutoPostBack="true" OnSelectedIndexChanged="cmbline_SelectedIndexChanged"></dx:ASPxComboBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Seccion</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxComboBox ID="cmbseccion" runat="server" NullText="Seccion" Theme="Material" Width="180px" AutoPostBack="true"></dx:ASPxComboBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Estado</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxCheckBox ID="chkestado" runat="server" ValueChecked="true" ValueUnchecked="false" Theme="Material"></dx:ASPxCheckBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-12" style="text-align: center;">
                                    <dx:ASPxButton ID="btnedit" runat="server" Theme="Material" Text="Save" Width="150px" OnClick="btnedit_Click"></dx:ASPxButton>
                                </div>
                            </div>
                            <br />
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnedit" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

