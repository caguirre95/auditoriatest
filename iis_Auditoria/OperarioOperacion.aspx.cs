﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OperarioOperacion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void btnoperario_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();

            string query = "select empno, firstname + ' ' +lastname as nombreCompleto, badgeno from employee";

            dt = DataAccessPervasive.GetDataTable(query);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string codig = dt.Rows[i][0].ToString();
                string nomb = dt.Rows[i][1].ToString();
                string barc = dt.Rows[i][2].ToString();
                string ok = OrderDetailDA.VincularOperario(codig, nomb, barc);

                if (ok == "OK")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                }
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();

            string query = "select distinct DESCR from oper order by char_length(descr) asc";

            dt = DataAccessPervasive.GetDataTable(query);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string desc = dt.Rows[i][0].ToString();
                string descm = dt.Rows[i][0].ToString();
                string ok = OrderDetailDA.VincularOperacion(desc,descm);

                if (ok == "OK")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                }
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }
}