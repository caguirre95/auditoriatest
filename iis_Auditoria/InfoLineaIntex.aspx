﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InfoLineaIntex.aspx.cs" Inherits="InfoLineaIntex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <%--<script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    var num = document.getElementById("<%= HiddenFielduser.ClientID %>").value;
                    $.ajax({
                        url: "InfoLineaIntex.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "','idlinea':'" + num + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);
                    $('#<%=txtstyle.ClientID%>').val(ui.item.style);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });       

    </script>--%>

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    var num = document.getElementById("<%= HiddenFielduser.ClientID %>").value;
                    $.ajax({
                        url: "InfoLineaIntex.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "','idlinea':'" + num + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=idporderAuxiliar.ClientID%>').val(ui.item.idorder);
                    $('#<%=txtstyle.ClientID%>').val(ui.item.style);
                    $('#<%=lblmensajepo.ClientID%>').html(ui.item.porder);

                    $('#myModal').modal('toggle')

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });

        $(document).ready(function () {

            $('#<%=Button1.ClientID%>').click(function (e) {
                e.preventDefault();
                $('#<%=txtPorder.ClientID%>').val('');
                $('#<%=hfidPorder.ClientID%>').val('');
                $('#<%=txtstyle.ClientID%>').val('');
                $('#<%=lblmensajepo.ClientID%>').html('');
                $('#myModal').modal('hide')
            })

            $('#<%=Button2.ClientID%>').click(function (e) {
                e.preventDefault();
                var idporder = $('#<%=idporderAuxiliar.ClientID%>').val();
                $('#<%=hfidPorder.ClientID%>').val(idporder);
                $('#<%=lblmensajepo.ClientID%>').html('');
                $('#myModal').modal('hide')
            })
        })

    </script>



    <%-- <script type="text/javascript">
        $(document).ready(function () {

            $('#<%=Button1.ClientID%>').click(function (e)
            {
                e.preventDefault();
                $('#<%=txtPorder.ClientID%>').val('');
                $('#<%=hfidPorder.ClientID%>').val('');
                $('#<%=txtstyle.ClientID%>').val('');
                $('#<%=lblmensajepo.ClientID%>').html('');
                $('#myModal').modal('hide')
            })

            $('#<%=Button2.ClientID%>').click(function (e)
            {
                e.preventDefault();
                var idporder = $('#<%=idporderAuxiliar.ClientID%>').val();
                $('#<%=hfidPorder.ClientID%>').val(idporder);
                $('#<%=lblmensajepo.ClientID%>').html('');
                $('#myModal').modal('hide')
            })
        })
    </script>--%>

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function alertexeso() {
            swal({
                title: 'El Acumulado Sobrepasa Las Unidades Enviadas Del Bulto Se Ha Corregido Automaticamente!',
                timer: 400,
                type: 'info'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Algunos Bultos No Se Guardaron Correctamente !',
                type: 'info'
            });
        }
    </script>

    <script>
        function key_up(objRef) {

            var row = objRef.parentNode.parentNode;

            if (row.hasChildNodes()) {
                var children = row.childNodes;

                alert(row.children[0].nodoName);

                for (var i = 0; i < children.length; i++) {
                    alert(children[i].childNodes[0].val);

                    // do something with each child as children[i]
                    // NOTE: List is live, adding or removing children will change the list
                }
            }
           //  txtunidades
           //  lblquantity
             //row.cell[0].text;
             //  if (objRef.checked) {
           //  var unidades = row.getElementById("<= txtunidades.ClientID %>").value;
           //  var quantity = row.getElementById("<= lblquantity.ClientID %>").value;
           //  var deficit = row.getElementById("<= txtdeficit.ClientID %>").value;

              //var num = document.getElementById("<%= HiddenFielduser.ClientID %>").value;

            row.style.backgroundColor = "#0090CB";
            // row.style.color = "white";
            //}
            //else {
            //    //If not checked change back to original color
            //    if (row.rowIndex % 2 == 0) {
            //        //Alternating Row Color
            //        row.style.backgroundColor = "white";
            //        row.style.color = "black";
            //    }
            //    else {
            //        row.style.backgroundColor = "white";
            //        row.style.color = "black";
            //    }
            //}
        }
    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }
    </script>

    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>
    <link href="jquery-ui.css" rel="stylesheet" />
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <style>
        .mar_t {
            margin-top: 20px;
            margin-bottom: 10px;
            margin-left: 5px;
            margin-right: 5px;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="">
            <div class="panel panel-primary" style="margin-top: 10px;">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Measurement suggestion </strong></h3>
                </div>
                <div class="panel-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="HiddenFielduser" runat="server" />
                            <%--    <div class="col-lg-12">--%>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    Corte:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" OnClick="btnbuscar_Click" CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" required="true" class="textboxAuto text-info form-control" AutoCompleteType="Disabled" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    Bultos:
                                     <asp:DropDownList ID="DropDownList2" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>

                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    Estilo:
                                     <asp:TextBox runat="server" CssClass="form-control" placeholder="Estilo" Font-Size="14px" ID="txtstyle" />

                                </div>
                                <div class="form-group">
                                    <asp:Button runat="server" ID="btnvertodos" OnClick="btnvertodos_Click" Text="Ver Todos" CssClass="btn btn-info pull-right mar_t" />
                                    <asp:Button runat="server" ID="btnlimpiar" OnClick="btnlimpiar_Click" Text="Limpiar" CssClass="btn btn-success  pull-right mar_t" />
                                    <asp:Button runat="server" ID="btnguardar" OnClick="btnguardar_Click" Text="Guardar Unidades" CssClass="btn btn-primary pull-right mar_t" />
                                    <asp:Button runat="server" ID="btncargar" OnClick="btncargar_Click" Text="Cargar Unidades" CssClass="btn btn-warning pull-right mar_t" />

                                </div>
                                <asp:HiddenField ID="hfidPorder" runat="server" />
                                <asp:HiddenField ID="hfiunidades" runat="server" />
                            </div>


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <%--   <div class="col-lg-10">--%>

                                <asp:GridView ID="gridbultos" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" DataKeyNames="Id_Bundle" CssClass="table table-responsive table-hover table-striped " OnRowCommand="gridbultos_RowCommand" OnRowDataBound="gridbultos_RowDataBound" GridLines="None" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <HeaderTemplate>
                                                Bulto 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblidbulto" runat="server" Font-Size="Medium" Text='<%#Bind("Id_Bundle") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Seq 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSeq" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("Seq") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Size 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblsize" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Size") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="false" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Quantity 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblquantity" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Quantity") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                CantidadEnviada 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblcantAE" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Cantidad") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Acumulado
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblacumulado" runat="server" Font-Size="Medium" CssClass="label label-info" Text='<%#Bind("Acumulado") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Cantidad
                                                        <asp:Button ID="btn" CommandName="reiniciar" Text="Reiniciar" CssClass="btn btn-default" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtunidades" Font-Size="Medium" onkeypress="return num(event);" CssClass="form-control" Text='' MaxLength="2" AutoCompleteType="Disabled" /><%--AutoPostBack="true" OnTextChanged="txtunidades_TextChanged"--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Deficit
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="txtdeficit" Font-Size="Medium" CssClass="label label-info" Text='<%#Bind("Deficit") %>' />
                                                <asp:LinkButton ID="btn" ForeColor="White" CommandName="agregar" Text="Reiniciar" CssClass="btn btn-default" runat="server">
                                                      <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                                <%--  </div>--%>
                            </div>

                            <%--  </div>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
        <div class="modal-dialog " role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Verificacion de Corte</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="display: block !important">
                        <asp:HiddenField ID="idporderAuxiliar" runat="server" />
                        <div class="col-lg-12 center-block">
                            <h1>
                                <asp:Label Text="text" ID="lblmensajepo" CssClass="label label-info center-block " runat="server" />
                            </h1>
                        </div>
                        <div class="col-lg-6 center-block">
                            <asp:Button ID="Button2" CssClass="btn btn-primary center-block" runat="server" Text="Aceptar" />
                        </div>

                        <div class="col-lg-6 center-block">
                            <asp:Button ID="Button1" CssClass="btn btn-default center-block" runat="server" Text="Cancelar" />
                        </div>

                    </div>

                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
