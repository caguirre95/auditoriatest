﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InspeccionEmpaque.aspx.cs" Inherits="InspeccionEmpaque" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function alertSeleccionarMedida(mess, tipo) {
            swal({
                title: "",
                text: mess,
                type: tipo
            });
        }
        function successalert() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
    </script>
    <script type="text/javascript">
        function warningalert() {
            swal({
                title: "¿Estas seguro?",
                text: "¿Deseas ingresar este registro?",
                type: "warning"
            })
        }
    </script>

    <link href="bootstrap-fileinput-master/css/fileinput.css" rel="stylesheet" />
    <link href="bootstrap-fileinput-master/css/fileinput.min.css" rel="stylesheet" />
    <script src="bootstrap-fileinput-master/js/fileinput.js"></script>
    <script src="bootstrap-fileinput-master/js/fileinput.min.js"></script>

    <link rel="stylesheet" href="datetimepicker/css/bootstrap-material-datetimepicker.css" />
    <script type="text/javascript" src="datetimepicker/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">

    <div style="margin-left: 10px;">

        <label id="frprododr">
            <asp:Label ID="lblorder1" Visible="false" runat="server" Text=""></asp:Label></label>

        <label id="toprododr">
            <asp:Label ID="lblorder2" runat="server" Visible="false" Text=""></asp:Label></label>

        <br />

    </div>

    <div class="container-fluid">
        <div class="col-lg-12 centrar">
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/img/system_search.png" OnClick="ImageButton2_Click" /><asp:LinkButton ID="lnkNew" runat="server" OnClick="lnkNew_Click">New Search</asp:LinkButton>

        </div>
    </div>

    <div class="container " style="padding-bottom: 5px;">
        <div class="col-lg-2">
            <strong>Customer:</strong>
            <asp:Label ID="lbclient" runat="server" CssClass="main">
                <asp:Label ID="lblcustomer" runat="server" Text=""></asp:Label></asp:Label>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container-fluid info_ord" style="margin-bottom: 0 !important">
        <div class="container" style="padding-bottom: 5px;">
            <div class="col-lg-2">
                <asp:Label ID="Label10" runat="server" Text="Corte#:"></asp:Label>
                <asp:TextBox ID="txtporder" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label28" runat="server" Text="Estilo"></asp:Label>
                <asp:TextBox ID="txtstyle" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label29" runat="server" Text="Descripcion"></asp:Label>
                <asp:TextBox ID="txtdescrip" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label30" runat="server" Text="Cantidad"></asp:Label>
                <asp:TextBox ID="txtcantidad" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label7" runat="server" Text="Color"></asp:Label>
                <asp:TextBox ID="txtcolor" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label1" runat="server" Text="Cantidad Defectos"></asp:Label>
                <asp:TextBox ID="txtpiezas" runat="server" CssClass="form-control" MaxLength="5" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>                                
            </div>
        </div>
    </div>

    <div class="container" style="padding-bottom: 5px; margin-top: 20px">
        <div class="panel panel-default">
            <div class="panel-heading">Ingreso de Datos</div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-8">
                        <div id="Grid" runat="server">
                            <asp:GridView ID="gridAddDefectos" runat="server" CssClass="table table-hover table-striped" GridLines="None" Width="100%" AutoGenerateColumns="False"
                                OnRowDataBound="gridAddDefectos_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Defecto">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddldefectos" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Operación">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlposicion" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cantidad">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtcantidaddef" CssClass="form-control" runat="server" Enabled="false" Text="1"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblno" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />

                        </div>
                    </div>
                    <div class="col-lg-4"> 
                        <div class="panel panel-info " >
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body form-inline">

                                <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success btn-block " Text="Guardar" OnClick="btnGuardar_Click" />

                                <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-danger btn-block" Text="Limpiar" OnClick="btnlimpiar_Click" />
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

