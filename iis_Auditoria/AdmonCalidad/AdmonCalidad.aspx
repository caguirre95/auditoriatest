﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdmonCalidad.aspx.cs" Inherits="AdmonCalidad_AdmonCalidad" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtuser.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "AdmonCalidad.aspx/GetUser",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    userid: item.userid,
                                    username: item.username,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtuser.ClientID%>').val(ui.item.username);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtuser.ClientID%>').val(ui.item.username);
                    $('#<%=hdiuserid.ClientID%>').val(ui.item.userid);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.username + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">      

        function desbloqueado() {
            swal({
                title: 'Exito!',
                text: "Usuario Desbloqueado!!!",
                type: 'success'
            });
        }

        function addlinea() {
            swal({
                title: 'Exito!',
                text: "Registro Agregado Correctamente!!!",
                type: 'success'
            });
        }

        function dsblq() {
            swal({
                title: 'Informacion!',
                text: "Usuario no esta Bloqueado!!!",
                type: 'info'
            });
        }

        function userUpd() {
            swal({
                title: 'Informacion!',
                text: "Usuario Actualizado!!!",
                type: 'success'
            });
        }

        function userNameUpd() {
            swal({
                title: 'Informacion!',
                text: "Nombre Usuario Actualizado!!!",
                type: 'success'
            });
        }

        function userRolUpd() {
            swal({
                title: 'Informacion!',
                text: "Rol Usuario Actualizado!!!",
                type: 'success'
            });
        }

        function error() {
            swal({
                title: 'Informacion!',
                text: "Llene los campos correctamente!!!",
                type: 'warning'
            });
        }

        function rgs() {
            swal({
                title: 'Informacion!',
                text: "Ya se Encuentra el registro!!!",
                type: 'info'
            });
        }

        function lnasig() {
            swal({
                title: 'Informacion!',
                text: "No se puede eliminar el registro, No esta asignado a este usuario!!!",
                type: 'info'
            });
        }

    </script>

    <script type="text/javascript" src="scripts/footable.min.js"></script>
    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <style>
        .mrto {
            margin-top: 10px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <br />

    <div class="container">

        <div class="panel panel-primary" style="margin-top: 5px">
            <div class="panel-heading">Administracion Usuarios Calidad</div>
            <div class="panel-body">

                <div class="col-lg-12">

                    <div class="col-lg-4">
                        Digite el nombre de Usuario
                        <asp:TextBox ID="txtuser" runat="server" placeholder="Usuario Membership" required="true" AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>
                        <asp:HiddenField ID="hdiuserid" runat="server" />

                        <asp:Button ID="btndata" runat="server" Text="Datos del Usuario" CssClass="btn btn-primary form-control mrto" OnClick="btndata_Click" />

                        <asp:Button ID="btnlimp" runat="server" Text="Limpiar Data" CssClass="btn btn-info form-control mrto" OnClick="btnlimp_Click" />
                    </div>

                </div>

                <div class="col-lg-12">
                    <hr />
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <asp:Label ID="lbl1" runat="server" Text="" CssClass="alert-info fa-bitcoin form-control"></asp:Label>
                                <hr />
                            </div>

                            <div class="col-lg-12">
                                <asp:Label ID="lbl2" runat="server" Text="" CssClass="alert-info mrto form-control"></asp:Label>
                                <hr />
                                <asp:Panel ID="p1" runat="server" Visible="true">
                                    <asp:UpdatePanel ID="upd1" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btndesb" runat="server" Text="Desbloquear Usuario" CssClass="btn btn-primary form-control" OnClick="btndesb_Click" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btndesb" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </asp:Panel>
                                <hr />
                            </div>

                            <div class="col-lg-12">
                                <asp:Label ID="lbl3" runat="server" Text="" CssClass="alert-info mrto form-control"></asp:Label>
                                <asp:Label ID="lbl6" runat="server" Text="" CssClass="alert-info mrto form-control"></asp:Label>

                                <hr />
                                <asp:Panel ID="p2" runat="server" Visible="true">

                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="col-lg-4">
                                                Digite el nuevo Nombre
                                                <asp:TextBox ID="txtnombre" runat="server" CssClass="form-control" placeholder="Nombre Usuario Nuevo" AutoCompleteType="Disabled"></asp:TextBox>
                                            </div>
                                            <div class="col-lg-4">
                                                Seleccione su Nuevo Rol
                                                <asp:DropDownList ID="drprol" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">Seleccione...</asp:ListItem>
                                                    <asp:ListItem Value="2">bultoabulto</asp:ListItem>
                                                    <asp:ListItem Value="7">embarque</asp:ListItem>
                                                    <asp:ListItem Value="8">final</asp:ListItem>
                                                    <asp:ListItem Value="9">inspfinal</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-lg-4">
                                                Seleccione la Linea 
                                                <asp:DropDownList ID="drpline1" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <div class="col-lg-12">
                                                <br />
                                                <asp:Button ID="btnupd" runat="server" Text="OK" CssClass="btn btn-primary form-control" OnClick="btnupd_Click" />
                                            </div>
                                        </div>
                                    </div>

                                </asp:Panel>
                                <hr />
                            </div>

                            <div class="col-lg-12">
                                <asp:Label ID="lbl4" runat="server" Text="" CssClass="alert-info mrto form-control"></asp:Label>
                                <asp:Label ID="lbl5" runat="server" Text="" CssClass="alert-info mrto form-control"></asp:Label>
                                <hr />
                                <asp:Panel ID="p3" runat="server" Visible="true">
                                    <div class="panel panel-default">
                                        <div class="panel-body">

                                            <div class="col-lg-6">

                                                <div class="col-lg-12">
                                                    Seleccione la Linea a Actualizar 

                                                    <asp:DropDownList ID="drpline" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>

                                                    <asp:HiddenField ID="nombre" runat="server" />
                                                    <asp:HiddenField ID="rol" runat="server" />
                                                </div>

                                                <div class="col-lg-12">
                                                    <asp:Button ID="btnupdl" runat="server" Text="Agregar Linea" CssClass="btn btn-primary form-control MTop" OnClick="btnupdl_Click" />

                                                    <asp:Button ID="btnelim" runat="server" Text="Eliminar Lineas Asignadas a ese Usuario" CssClass="btn btn-warning form-control MTop" OnClick="btnelim_Click" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </asp:Panel>
                                <hr />
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

