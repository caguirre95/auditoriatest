﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdmonCalidad_AdmonCalidad : System.Web.UI.Page
{
    string nombre1 = "";
    string rol1 = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CargaLinea();
            Session["nmb"] = null;
            Session["rol"] = null;
        }
    }

    void CargaLinea()
    {
        string line = " select id_linea as Id,'Linea - ' + numero as Linea from Linea order by CAST(numero as float) asc";
        DataTable li = DataAccess.Get_DataTable(line);

        drpline.DataSource = li;
        drpline.DataTextField = "Linea";
        drpline.DataValueField = "Id";
        drpline.DataBind();
        drpline.Items.Insert(0, "Select...");

        drpline1.DataSource = li;
        drpline1.DataTextField = "Linea";
        drpline1.DataValueField = "Id";
        drpline1.DataBind();
        drpline1.Items.Insert(0, "Select...");
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<usuarioClass> GetUser(string pre)
    {
        try
        {
            List<usuarioClass> list = new List<usuarioClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top 20 UserId, UserName from aspnet_Users where ApplicationId = 'C95A2209-0671-4303-ACA1-EF3EE97884AD' and UserName like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                usuarioClass obj = new usuarioClass();

                obj.userid = (dt.Rows[i][0].ToString());
                obj.username = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class usuarioClass
    {
        public string userid { get; set; }
        public string username { get; set; }
    }

    protected void btnlimp_Click(object sender, EventArgs e)
    {
        Response.Redirect("/AdmonCalidad/AdmonCalidad.aspx");
    }

    protected void btndata_Click(object sender, EventArgs e)
    {
        try
        {
            Cargadata();
        }
        catch (Exception)
        {

            throw;
        }
    }

    void Cargadata()
    {

        DataTable user = new DataTable();
        user = DataAccess.Get_DataTable("select UserId, UserName from aspnet_Users where UserId = '" + hdiuserid.Value.ToString().Trim() + "' and UserName = '" + txtuser.Text.Trim() + "'");
        if (user.Rows.Count > 0)
        {
            lbl1.Text = "Nombre Membership : " + user.Rows[0][1].ToString();
        }
        else
        {
            lbl1.Text = "No Data Result";
        }


        DataTable bloq = new DataTable();
        bloq = DataAccess.Get_DataTable(" select case when IsLockedOut = 1 then 'Bloqueado' else 'Desbloqueado' end PerfilUser from aspnet_Membership"
                                     + " where UserId = '" + hdiuserid.Value.ToString().Trim() + "'");
        if (bloq.Rows.Count > 0 && bloq.Rows[0][0].ToString().Trim().Equals("Bloqueado"))
        {
            lbl2.Text = "Perfil de Usuario : " + bloq.Rows[0][0].ToString();
        }
        else if (bloq.Rows.Count > 0 && bloq.Rows[0][0].ToString().Trim().Equals("Desbloqueado"))
        {
            lbl2.Text = "Perfil de Usuario : " + bloq.Rows[0][0].ToString();
        }
        else
        { lbl2.Text = "No Data Result"; }


        DataTable name_rol = new DataTable();
        name_rol = DataAccess.Get_DataTable(" select userName, rol from tblUserMedida where userms = '" + txtuser.Text.Trim() + "'");
        if (name_rol.Rows.Count > 0)
        {
            lbl3.Text = "Nombre del Usuario Auditor : " + name_rol.Rows[0][0].ToString() + ", Rol del Usuario : " + name_rol.Rows[0][1].ToString();
        }
        else
        {
            lbl3.Text = "No Data Result";
        }


        DataTable line_Audit = new DataTable();
        line_Audit = DataAccess.Get_DataTable(" DECLARE @str NVARCHAR(MAX) SELECT @str = COALESCE(@str + ', ', '') + l.numero FROM tblUserLineaMedida u join Linea l on l.id_linea = u.IdLinea"
                                            + " where nameMemberShip = '" + txtuser.Text.Trim() + "' order by CAST(l.numero as float) select @str 'Linea Auditor'");
        if (line_Audit.Rows.Count > 0)
        {
            lbl4.Text = "Auditor de las Lineas : " + line_Audit.Rows[0][0].ToString();
        }
        else
        {
            lbl4.Text = "No Data Result";
        }


        DataTable name_Insp = new DataTable();
        name_Insp = DataAccess.Get_DataTable("select nombre from Inspector where UserId = '" + hdiuserid.Value.ToString().Trim() + "'");
        if (name_Insp.Rows.Count > 0)
        {
            lbl6.Text = "Nombre del Usuario Inspector : " + name_Insp.Rows[0][0].ToString();
        }
        else
        {
            lbl6.Text = "No Data Result";
        }


        DataTable line_insp = new DataTable();
        line_insp = DataAccess.Get_DataTable(" DECLARE @str NVARCHAR(MAX) SELECT @str = COALESCE(@str + ', ', '') + l.numero from tbInspectorLinea u join Linea l on l.id_linea = u.IdLinea"
                                           + " join Inspector i on i.id_inspector = u.idInspector where codigo = '" + txtuser.Text.Trim() + "' order by CAST(l.numero as float)"
                                           + " select @str 'Linea Inspector'");
        if (line_insp.Rows.Count > 0)
        {
            lbl5.Text = "Inspector de las Lineas : " + line_insp.Rows[0][0].ToString();
        }
        else
        {
            lbl5.Text = "No Data Result";
        }

    }

    protected void btndesb_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable bloq = new DataTable();
            bloq = DataAccess.Get_DataTable(" select case when IsLockedOut = 1 then 'Bloqueado' else 'Desbloqueado' end PerfilUser from aspnet_Membership"
                                         + " where UserId = '" + hdiuserid.Value.ToString().Trim() + "'");
            if (bloq.Rows[0][0].ToString().Trim().Equals("Bloqueado"))
            {
                DataTable ds = new DataTable();
                ds = DataAccess.Get_DataTable(" update aspnet_Membership set IsLockedOut = 0, FailedPasswordAttemptCount = 0, LastLockoutDate = '1754-01-01 00:00:00.000',"
                                            + " FailedPasswordAttemptWindowStart = '1754-01-01 00:00:00.000' where UserId = '" + hdiuserid.Value.ToString().Trim() + "'");
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "desbloqueado();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "dsblq();", true);
            }

            Cargadata();
        }
        catch (Exception ex)
        {
            string mensaje = ex.Message;
            throw;
        }
    }

    protected void btnupd_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtnombre.Text != string.Empty || drprol.SelectedValue.ToString() != "0" || drpline1.SelectedValue.ToString() != "0")
            {
                DataTable a1 = new DataTable();
                DataTable a2 = new DataTable();
                DataTable a3 = new DataTable();

                a1 = DataAccess.Get_DataTable("select * from tblUserMedida where userms = '" + txtuser.Text.Trim() + "'");
                a2 = DataAccess.Get_DataTable("select * from tblUserLineaMedida where nameMemberShip = '" + txtuser.Text.Trim() + "'");
                a3 = DataAccess.Get_DataTable("select * from Inspector where codigo = '" + txtuser.Text.Trim() + "'");

                //tblusermedida
                if (a1.Rows.Count > 0)
                {
                    if (txtnombre.Text != string.Empty && drprol.SelectedValue.ToString() != "0" && drpline1.SelectedValue.ToString() != "0")
                    {
                        DataTable upd = new DataTable();
                        upd = DataAccess.Get_DataTable(" Update tblUserMedida set userName = '" + txtnombre.Text.Trim() + "'," +
                                                       " rol = '" + drprol.SelectedItem.Text.Trim() + "'," +
                                                       " linea = " + drpline1.SelectedValue + " where userms = '" + txtuser.Text.Trim() + "'");
                    }
                    else if (txtnombre.Text != string.Empty && drprol.SelectedValue.ToString() != "0")
                    {
                        DataTable upd = new DataTable();
                        upd = DataAccess.Get_DataTable(" Update tblUserMedida set userName = '" + txtnombre.Text.Trim() + "'," +
                                                       " rol = '" + drprol.SelectedItem.Text.Trim() + "' where userms = '" + txtuser.Text.Trim() + "'");
                    }
                    else if (txtnombre.Text != string.Empty)
                    {
                        DataTable upd = new DataTable();
                        upd = DataAccess.Get_DataTable(" Update tblUserMedida set userName = '" + txtnombre.Text.Trim() + "' where userms = '" + txtuser.Text.Trim() + "'");
                    }
                }
                else
                {
                    DataTable c1 = new DataTable();
                    c1 = DataAccess.Get_DataTable("insert into tblUserMedida values ('" + txtuser.Text.Trim() + "','"
                                                                                        + txtnombre.Text.Trim() + "','"
                                                                                        + drprol.SelectedItem.Text.Trim() + "',"
                                                                                        + drpline1.SelectedValue + ",1)");
                }

                //tbluserlineamedida
                if (a2.Rows.Count > 0)
                {
                    if (txtnombre.Text != string.Empty && drprol.SelectedValue.ToString() != "0")
                    {

                    }
                    else if (txtnombre.Text != string.Empty)
                    {

                    }
                }
                else
                {
                    DataTable c1 = new DataTable();
                    c1 = DataAccess.Get_DataTable("insert into tblUserLineaMedida values ('" + txtuser.Text.Trim() + "','"
                                                                                             + txtnombre.Text.Trim() + "','"
                                                                                             + drprol.SelectedItem.Text.Trim() + "',"
                                                                                             + drpline1.SelectedValue + ",1)");
                }

                //Inspector
                if (a3.Rows.Count > 0)
                {
                    if (txtnombre.Text != string.Empty && drprol.SelectedValue.ToString() != "0")
                    {
                        DataTable c1 = new DataTable();
                        c1 = DataAccess.Get_DataTable("Update Inspector set nombre = '" + txtnombre.Text.Trim() + "' where codigo = '" + txtuser.Text.Trim() + "'");
                        c1 = DataAccess.Get_DataTable("Update tbInspectorAuditoria set idAuditoria = (select idAuditoria from tbTipoAuditoria where idAuditoria = '" + drprol.SelectedValue + "')" +
                                                      " where idInspector = (select id_inspector from Inspector where codigo = '" + txtuser.Text.Trim() + "')");
                    }
                    else if (txtnombre.Text != string.Empty)
                    {
                        DataTable c1 = new DataTable();
                        c1 = DataAccess.Get_DataTable("Update Inspector set nombre = '" + txtnombre.Text.Trim() + "' where codigo = '" + txtuser.Text.Trim() + "'");
                    }

                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "userUpd();", true);
                }
                else
                {
                    DataTable c1 = new DataTable();
                    DataTable ver = new DataTable();
                    c1 = DataAccess.Get_DataTable("insert into Inspector values ('" + hdiuserid.Value + "','" + txtuser.Text.Trim() + "','" + txtnombre.Text.Trim() + "'");

                    ver = DataAccess.Get_DataTable("select * from Inspector where UserId = " + hdiuserid.Value);

                    if (ver.Rows.Count > 0)
                    {
                        c1 = DataAccess.Get_DataTable("insert into tbInspectorAuditoria values (" + ver.Rows[0][0].ToString() + "," + drprol.SelectedValue + ",1");

                        c1 = DataAccess.Get_DataTable("insert into tbInspectorLinea values (" + drpline1.SelectedValue + "," + ver.Rows[0][0].ToString() + ",1");
                    }

                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "addlinea();", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "error();", true);
            }

            Cargadata();
            CargaLinea();
            Limpiar();
        }
        catch (Exception ex)
        {
            string mensaje = ex.Message;
            //throw;
        }
    }

    void Limpiar()
    {
        txtnombre.Text = "";
        drprol.SelectedValue = "0";
    }

    protected void btnupdl_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtuser.Text != string.Empty)
            {
                DataTable confir2 = new DataTable();
                DataTable confir3 = new DataTable();

                confir2 = DataAccess.Get_DataTable(" select IdLinea from tblUserLineaMedida "
                                                 + " where nameMemberShip = '" + txtuser.Text.Trim() + "' and IdLinea = " + drpline.SelectedValue + "");

                confir3 = DataAccess.Get_DataTable(" select idLinea from tbInspectorLinea "
                                                 + " where idInspector = (select id_inspector from Inspector where codigo = '" + txtuser.Text.Trim() + "')"
                                                 + " and idLinea = " + drpline.SelectedValue + "");

                DataTable name_rol = new DataTable();
                name_rol = DataAccess.Get_DataTable(" select userName, rol from tblUserMedida where userms = '" + txtuser.Text.Trim() + "'");
                if (name_rol.Rows.Count > 0)
                {
                    nombre1 = name_rol.Rows[0][0].ToString();
                    rol1 = name_rol.Rows[0][1].ToString();
                }
                else
                {
                    nombre1 = "S/N";
                    rol1 = "S/R";
                }

                if (confir2.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "rgs();", true);
                }
                else
                {
                    DataTable c2 = new DataTable();
                    c2 = DataAccess.Get_DataTable("insert into tblUserLineaMedida values ('" + txtuser.Text.Trim() + "'"
                                                                                      + ",'" + nombre1 + "','" + rol1 + "'," + drpline.SelectedValue + ",1)");

                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "addlinea();", true);
                }

                if (confir3.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "rgs();", true);
                }
                else
                {
                    DataTable c3 = new DataTable();
                    c3 = DataAccess.Get_DataTable("insert into tbInspectorLinea values(" + drpline.SelectedValue + ","
                                                                                     + "(select id_inspector from Inspector where codigo = '" + txtuser.Text.Trim() + "'),1)");

                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "addlinea();", true);
                }

                Cargadata();
                CargaLinea();

            }
        }
        catch (Exception ex)
        {
            string mensaje = ex.Message;
            //throw;
        }
    }

    protected void btnelim_Click(object sender, EventArgs e)
    {
        try
        {
            if (drpline.SelectedValue.ToString() != "0")
            {
                DataTable confir = new DataTable();
                DataTable confir2 = new DataTable();

                confir = DataAccess.Get_DataTable("select * from tblUserLineaMedida where nameMemberShip = '" + txtuser.Text.Trim() + "' and IdLinea = " + drpline.SelectedValue + "");
                confir = DataAccess.Get_DataTable(" select * from tbInspectorLinea where idInspector = (select id_inspector from Inspector where codigo = '" + txtuser.Text.Trim() + "')"
                                                + " and idLinea = " + drpline.SelectedValue + "");

                if (confir.Rows.Count > 0 || confir2.Rows.Count > 0)
                {
                    DataTable elm = new DataTable();

                    elm = DataAccess.Get_DataTable(" delete from tblUserLineaMedida where nameMemberShip = '" + txtuser.Text.Trim() + "' and IdLinea = '" + drpline.SelectedValue + "'"
                                                 + " delete from tbInspectorLinea where idInspector = (select id_inspector from Inspector where codigo = '" + txtuser.Text.Trim() + "') "
                                                 + " and IdLinea ='" + drpline.SelectedValue + "'");


                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "addlinea();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "lnasig();", true);
                }

                Cargadata();
                CargaLinea();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "error();", true);
            }
        }
        catch (Exception ex)
        {
            string mensaje = ex.Message;
            //throw;
        }
    }
}