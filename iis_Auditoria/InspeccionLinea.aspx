﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InspeccionLinea.aspx.cs" Inherits="InspeccionLinea" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="scripts/footable.min.js"></script>
     <script type="text/javascript">
         function successalert() {
             swal({
                 title: 'Registro Ingresado Correctamente!',
                 type: 'success'
             });
         }
    </script>
    <script type="text/javascript">
        function pageLoad() { // this gets fired when the UpdatePanel.Update() completes
            ReBindMyStuff();
        }
        function ReBindMyStuff() { // create the rebinding logic in here
            $('[id*=Repeater]').footable();
        }
    </script>
    <script type="text/javascript">
        function ShowWindow() {
            AsignarDefectos.Show();
        }
        function HideWindow() {
            AsignarDefectos.Hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2>
        <asp:Label ID="lbl" runat="server" Text="In Line Audit"></asp:Label></h2>
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="header_div">
        <div class="col-xs-4" style="padding-top: 5px;">
            <div class="col-xs-12">
                <asp:Label ID="lblcustom" runat="server" Text="Customer:"></asp:Label>
                <asp:Label ID="Customer" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-xs-12">
                <asp:Label ID="Label1" runat="server" Text="Styles:"></asp:Label>
                <asp:Label ID="lblstyles" runat="server" Text="LP337"></asp:Label>
            </div>
        </div>

        <div class="col-xs-4" style="padding-top: 5px;">
            <div class="col-xs-12">
                <asp:Label ID="Fecha" runat="server" Text="Fecha:"></asp:Label>
                <asp:Label ID="lblfecha" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-xs-12">
                <asp:Label ID="Label3" runat="server" Text="Modulo:"></asp:Label>
                <asp:Label ID="lblmodulos" runat="server" Text="Rocedes 1"></asp:Label>
            </div>
        </div>
        <div class="col-xs-4" style="padding-top: 5px;">
            <div class="col-xs-12">
                <asp:Label ID="Label5" runat="server" Text="Linea/Sección:"></asp:Label>
                <asp:Label ID="lblseccion" runat="server" Text="L1 SECCION 1"></asp:Label>
            </div>
            <div class="col-xs-12">
                <asp:Label ID="Label7" runat="server" Text="Inspector:"></asp:Label>
                <asp:Label ID="lblinspector" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
    <center>
     <asp:GridView ID="grv_insp_linea" CssClass="grv_insp" runat="server" 
            CellPadding="4" ForeColor="#333333" ShowFooter="true"
            GridLines="None" AutoGenerateColumns="False" 
            onrowdatabound="grv_insp_linea_RowDataBound">
         <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
         <Columns>
             <asp:TemplateField HeaderText="No">
             <ItemTemplate>
                 <asp:Label ID="lblno" runat="server" Text='<%# Eval("id") %>'></asp:Label>
             </ItemTemplate>
             </asp:TemplateField>      
             <asp:TemplateField HeaderText="Codigo de Empleado" HeaderStyle-Width="60px">
             <ItemTemplate>
                 <asp:Label ID="lblcodigo" runat="server" Text='<%# Eval("codigo") %>'></asp:Label>
             </ItemTemplate>
             </asp:TemplateField>           
             <asp:TemplateField HeaderText="Nombre del Operario">
              <ItemTemplate>
                 <asp:Label ID="lbloperario" runat="server" Text='<%# Eval("nombre") %>'></asp:Label>
             </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Operacion">
                 <ItemTemplate>
                     <asp:Label ID="lbloperacion" runat="server" Text='<%# Eval("operacion") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Muestreo" HeaderStyle-CssClass="muestreo_col" ItemStyle-HorizontalAlign="Center">
                 <ItemTemplate>
                     <asp:Label ID="lblmuestreo" runat="server" Text='<%# Eval("muestreo") %>'></asp:Label>
                 </ItemTemplate>
                  <FooterTemplate>
				    <div>
                    <asp:Label ID="lblsample" Text="TTLMuestreo:" runat="server" />
				        <asp:Label ID="lblTotalqty" runat="server" />
				    </div>
			      </FooterTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Defectos">
                 <ItemTemplate>
                     <asp:TextBox ID="txtdefectos" runat="server" Width="80px" Text='<%# Eval("defectos") %>'></asp:TextBox>
                 </ItemTemplate>
                 <FooterTemplate>
				    <div>
                    <asp:Label ID="lblTtdef" Text="TTDEF:" runat="server" />
				        <asp:Label ID="lblTotaldefect" runat="server" />
				    </div>
			      </FooterTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="PxP">
              <ItemTemplate>                             
                  <asp:CheckBox ID="chk_PxP" runat="server" />                             
             </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="AUD">
              <ItemTemplate>
                  <asp:CheckBox ID="chk_Aud" runat="server" />                               
             </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="R-AUD">
              <ItemTemplate>                            
                  <asp:CheckBox ID="chk_raud" runat="server" />                            
             </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="H">
              <ItemTemplate>                               
                  <asp:CheckBox ID="chk_h" runat="server" />                               
             </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField>
              <ItemTemplate>                             
                  <dx:ASPxButton ID="ASPxButton8" runat="server" onclick="ASPxButton8_Click" 
                      Text="Asignar Defectos">
                  </dx:ASPxButton>                             
             </ItemTemplate>
             </asp:TemplateField>
         </Columns>
         <EditRowStyle BackColor="#999999" />
         <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
         <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" Height="30px" ForeColor="White" />
         <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
         <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
         <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
         <SortedAscendingCellStyle BackColor="#E9E7E2" />
         <SortedAscendingHeaderStyle BackColor="#506C8C" />
         <SortedDescendingCellStyle BackColor="#FFFDF8" />
         <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
     </asp:GridView><br /><br />
         </center>
    <dx:ASPxPopupControl ID="AsignarDefectos" runat="server" EnableTheming="True" ClientInstanceName="AsignarDefectos"
        HeaderText="Defectos Encontrados" Modal="True" Theme="MetropolisBlue"
        AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        Width="800px" CloseAction="CloseButton">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxPanel ID="ASPxPanel1" runat="server">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                            <table style="width: 100%">
                                <tr>
                                    <td colspan="1" style="text-align: right;">
                                        <asp:Label ID="Label20" runat="server" Text="Operario:" Style="display: inline-block; width: 120px; font-size: 18px; font-weight: bold; margin-right: 10px;"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbloperario" runat="server" Style="display: inline-block; font-size: 18px;"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                                            Width="100%" AllowPaging="True"
                                            PageSize="15" OnPageIndexChanging="GridView1_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <center>
                                   <asp:CheckBox ID="CheckBox17" Width="30px" runat="server" />
                                   </center>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Defectos" HeaderStyle-CssClass="header_gv">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelDefect" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtcantidad" Width="100px" Height="30px" BackColor="LightYellow" runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Area" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                                    <ItemTemplate>
                                                        <asp:DropDownList runat="server" Width="250px"  
                                                            DataTextField="Nombre" DataValueField="Id_Area">
                                                        </asp:DropDownList>
                                                       <%-- <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                                                            ConnectionString="<%$ ConnectionStrings:RocedesCS %>"
                                                            SelectCommand="SELECT [Id_Area], [Nombre] FROM [Area]"></asp:SqlDataSource>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" CssClass="paginador" Font-Bold="False"
                                                Font-Size="14px" Width="50px" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center;">
                                        <br />
                                        <asp:Button ID="btnsave" runat="server"
                                            Text=" SAVE " Width="200" OnClientClick="HideWindow();return false;"
                                            OnClick="btnsave_Click" />
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>

    </dx:ASPxPopupControl>
</asp:Content>
