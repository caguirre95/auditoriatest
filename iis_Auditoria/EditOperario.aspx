﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EditOperario.aspx.cs" Inherits="EditOperario" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2>
        <asp:Label ID="lbl" runat="server" Text="Traslado Operario"></asp:Label></h2>
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
 <%--   <style>
        .idgrid{
         display:none !important;
        }
    </style>--%>
    <div class="container">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="SqlDataSource1" EnableTheming="True" KeyFieldName="id_operario" 
            Theme="Moderno">
            <SettingsSearchPanel Visible="True" />
            <Columns>                
                <dx:GridViewDataTextColumn FieldName="id_operario" VisibleIndex="5" Width="70px" Visible="false">
                </dx:GridViewDataTextColumn>
                <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0" Width="10px">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="Codigo" FieldName="codigo" VisibleIndex="3" Width="30px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Nombre" FieldName="nombre" VisibleIndex="4" Width="70px">
                </dx:GridViewDataTextColumn>                
                <dx:GridViewDataTextColumn FieldName="Id_Seccion" VisibleIndex="5" Width="70px" Visible="false">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <center>
        <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Edit" OnClick="ASPxButton1_Click" Theme="Moderno"></dx:ASPxButton>
            </center>
    </div>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:RocedesCS %>" SelectCommand="select o.[id_operario], o.[codigo], o.[nombre],c.Id_Linea,c.Id_Seccion from Operario o inner join Catalogo c on o.id_operario=c.Id_operario"></asp:SqlDataSource>
     <dx:ASPxPopupControl ID="EditarOperario" runat="server" EnableTheming="True" ClientInstanceName="AsignarDefectos"
        HeaderText="Trasladar Operario" Modal="True" Theme="MetropolisBlue" Width="750px" 
        AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" 
         CloseAction="CloseButton" MaxHeight="600px" ScrollBars="Auto">
        <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <dx:ASPxPanel ID="ASPxPanel1" runat="server">
        <PanelCollection>
            <dx:PanelContent runat="server">
            <center> 
                  <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
                      <Columns>
                          <asp:BoundField HeaderText="Codigo" DataField="Codigo"  />
                          <asp:BoundField HeaderText="Nombre" DataField="Nombre"/>
                          <asp:TemplateField HeaderText="Seccion Actual">
                              <ItemTemplate>
                                  <asp:Label ID="textSecAct" runat="server" Text=""></asp:Label>
                              </ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Seccion Destino">
                              <ItemTemplate>
                                  <asp:DropDownList runat="server" id="drop_sec_dest" DataSourceID="SqlDataSource1" DataTextField="Modulo" DataValueField="id_seccion"></asp:DropDownList>
                                  <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:RocedesCS %>" SelectCommand="select * from Modulo"></asp:SqlDataSource>
                              </ItemTemplate>
                          </asp:TemplateField>
                            <asp:TemplateField HeaderText="Seccion Actual" HeaderStyle-CssClass="idgrid">
                              <ItemTemplate>
                                  <asp:Label ID="id" CssClass="idgrid" runat="server" Text=""></asp:Label>
                              </ItemTemplate>
                          </asp:TemplateField>
                      </Columns>
                  </asp:GridView>     
                  <asp:SqlDataSource ID="SqlDataSource2" runat="server"></asp:SqlDataSource>                 
                <br />
                    <dx:ASPxButton runat="server" Text="GUARDAR" Theme="Moderno" id="btnpop" OnClick="btnpop_click"></dx:ASPxButton>              
            </center>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
            </dx:PopupControlContentControl>
</ContentCollection>

    </dx:ASPxPopupControl>
</asp:Content>

