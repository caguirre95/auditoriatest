﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class enviosBultosSeccionados : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.User.Identity.IsAuthenticated == true)
        {
            if (Page.User.IsInRole("PackAdmon"))
            {
                if (!IsPostBack)
                {
                    HiddenFielduser.Value = HttpContext.Current.User.Identity.Name;
                    var id = (string)Request.QueryString["idorder"];
                    var porder = (string)Request.QueryString["porder"];


                    if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(porder))
                    {

                        txtPorder.Text = porder;
                        hfidPorder.Value = id;

                        bultoincompletoviewall(int.Parse(id));

                    }
                    //Response.Redirect("EnviosBultoXCorte.aspx?idorder=" + lblidorder.Text + "&porder=" + lblporder.Text);
                }

            }
            else
            {
                Response.Redirect("/Default.aspx");
            }

        }
        else
        {
            Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre, string user)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select top 10 p.Id_Order,p.POrder,s.Style"
                                           + " from POrder p join Style s on p.Id_Style = s.Id_Style"
                                           + " where p.POrder like '%" + pre + "%' and p.Id_Planta in"
                                           + " (select u.Id_Planta from UserPlanta u where u.nameMemberShip = '" + user + "')", cn);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][1].ToString();
                obj.style = dt.Rows[i][2].ToString();

                list.Add(obj);
            }
            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public int idorder { get; set; }
        public string porder { get; set; }
        public string style { get; set; }
    }

    void bultoincompletoviewall(int id)
    {
        try
        {
            if (hfidPorder.Value != "")
            {

                DataTable dt = OrderDetailDA.ListarBultosSeccionadosXcorte(id);

                gridEnvios.DataSource = dt;
                gridEnvios.DataBind();
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void lnkMostrarBultosIncompletos_Click(object sender, EventArgs e)
    {
        try
        {

            if (!string.IsNullOrEmpty(hfidPorder.Value) && !string.IsNullOrEmpty(txtPorder.Text))
            {

                bultoincompletoviewall(int.Parse(hfidPorder.Value));

            }
        }
        catch (Exception)
        {

            throw;
        }

    }

    //protected void btnver_Click(object sender, EventArgs e)
    //{
    //    try
    //    {

    //        if (drpEnvioDia.SelectedItem.Text != "Select...")
    //        {
    //            int cont = 0;

    //            int envioDia = int.Parse(drpEnvioDia.SelectedValue);

    //            foreach (GridViewRow item in gridEnvios.Rows)
    //            {
    //                var chk = (CheckBox)item.FindControl("checkEnvio");
    //                var lblidbundle = (Label)item.FindControl("lblidbundle");
    //                var txtenvioC = (TextBox)item.FindControl("txtenvio");

    //                if (chk.Checked && lblidbundle.Text != string.Empty && txtenvioC.Text != string.Empty)
    //                {

    //                    int envioCorte = int.Parse(txtenvioC.Text);

    //                    string resp = OrderDetailDA.enviarguardarBihorarioXBulto(int.Parse(lblidbundle.Text), envioCorte, envioDia, Context.User.Identity.Name);

    //                    if (resp != "OK")
    //                    {
    //                        cont++;

    //                    }

    //                }


    //            }

    //            if (cont == 0)
    //            {
    //                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
    //                limpiar();

    //                var id = (string)Request.QueryString["idorder"];
    //                var porder = (string)Request.QueryString["porder"];


    //                if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(porder))
    //                {

    //                    txtPorder.Text = porder;
    //                    hfidPorder.Value = id;

    //                    bultoincompletoviewall(int.Parse(id));

    //                }
    //            }
    //            else
    //            {
    //                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
    //            }
    //        }
    //    }
    //    catch (Exception)
    //    {
    //        throw;
    //    }
    //}

    void limpiar()
    {
        lblid.Text = string.Empty;
        txtAcumuladoM.Text = string.Empty;
        txtunidadesenviarM.Text = string.Empty;
        txtenviodiaM.Text = string.Empty;
        txtenvioCorteM.Text = string.Empty;
    }

    //protected void gridEnvios_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    try
    //    {
    //        if (drpEnvioDia.SelectedItem.Text != "Select...")
    //        {
    //            if (e.CommandName == "enviar")
    //            {
    //                GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
    //                int indice = row.RowIndex;
    //                string idenvio = gridEnvios.DataKeys[indice].Value.ToString();

    //                int envioDia = int.Parse(drpEnvioDia.SelectedValue);
    //                var txtenvioC = (TextBox)row.FindControl("txtenvio");

    //                string user = Context.User.Identity.Name;
    //                int envioCorte = int.Parse(txtenvioC.Text);


    //                string resp = "";//OrderDetailDA.guardarenvioBultosSeccionados(int.Parse(idenvio), envioCorte, envioDia, user);

    //                if (resp == "OK")
    //                {
    //                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
    //                    limpiar();

    //                    var id = (string)Request.QueryString["idorder"];
    //                    var porder = (string)Request.QueryString["porder"];


    //                    if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(porder))
    //                    {

    //                        txtPorder.Text = porder;
    //                        hfidPorder.Value = id;

    //                        bultoincompletoviewall(int.Parse(id));

    //                    }
    //                }
    //                else
    //                {
    //                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
    //                }
    //            }
    //        }
    //    }
    //    catch (Exception)
    //    {
    //        throw;
    //    }

    //}

    protected void lnkEnvio_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("EnviosBulto.aspx");
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void guardarEnvio_Click(object sender, EventArgs e)
    {
        try
        {
            var idenvio = lblid.Text;
            var unidadesRestantes = Convert.ToInt32(txtAcumuladoM.Text) - Convert.ToInt32(txtunidadesenviarM.Text);
            var unidadesAEnviar = Convert.ToInt32(txtunidadesenviarM.Text);
            var envioDia = Convert.ToInt16(txtenviodiaM.Text);
            var envioCorte = Convert.ToInt16(txtenvioCorteM.Text);

            string resp = OrderDetailDA.guardarenvioBultosSeccionados(int.Parse(idenvio), envioCorte, envioDia, HttpContext.Current.User.Identity.Name, txtjustificacionM.Text, unidadesAEnviar);

            if (resp == "OK")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                limpiar();

                var id = (string)Request.QueryString["idorder"];
                var porder = (string)Request.QueryString["porder"];


                if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(porder))
                {

                    txtPorder.Text = porder;
                    hfidPorder.Value = id;

                    bultoincompletoviewall(int.Parse(id));

                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
            }

        }
        catch (Exception ex)
        {
            string ee = ex.Message;
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
        }
    }
}