﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EnviosBultoExcepcion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.User.Identity.IsAuthenticated == true)
        {
            if (Page.User.IsInRole("PackAdmon"))
            {
                if (!IsPostBack)
                {
                    HiddenFielduser.Value = HttpContext.Current.User.Identity.Name;
                    bultoincompletoviewall();
                }

            }
            else
            {
                Response.Redirect("/Default.aspx");
            }

        }
        else
        {
            Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre, string user)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();
            //  string a = HttpContext.Current.User.Identity.Name;
            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select top 10 p.Id_Order,p.POrder,s.Style"
                                           + " from POrder p join Style s on p.Id_Style = s.Id_Style"
                                           + " where p.POrder like '%" + pre + "%' and p.Id_Planta in"
                                           + " (select u.Id_Planta from UserPlanta u where u.nameMemberShip = '" + user + "')", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][1].ToString();
                obj.style = dt.Rows[i][2].ToString();

                list.Add(obj);
            }
            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public int idorder { get; set; }
        public string porder { get; set; }
        public string style { get; set; }
    }

    void bultoincompletoviewall()
    {
        try
        {
            if (hfidPorder.Value == "")
            {

                string queryBundle = " select v.Linea,v.POrder,v.Id_Bundle,v.Size,v.Quantity,v.Cantidad,v.Envio"
                                   + " from vwgetbultosIncompletos v"
                                   + " join Bundle b on b.Id_Bundle = v.Id_Bundle"
                                   + " join POrder p on p.Id_Order = b.Id_Order"
                                   + " join Linea l on l.id_linea = p.Id_Linea"
                                   + " join UserPlanta u on u.Id_Planta = l.id_planta"
                                   + " where u.nombreUsuario = '" + HiddenFielduser.Value + "'"
                                   + " order by v.Linea asc";
                DataTable dt = DataAccess.Get_DataTable(queryBundle);

                gridEnvios.DataSource = dt;
                gridEnvios.DataBind();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    void cantidadNoLiberada()
    {
        try
        {
            if (hfidPorder.Value != "")
            {

                string queryBundle = "select v.Linea,v.POrder,v.Id_Bundle,v.Size,v.Quantity,v.Cantidad,v.Envio from vwgetbultosIncompletos v where v.Id_Order=" + hfidPorder.Value;
                DataTable dt = DataAccess.Get_DataTable(queryBundle);

                gridEnvios.DataSource = dt;
                gridEnvios.DataBind();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    void getBultos()
    {
        try
        {
            if (hfidPorder.Value != string.Empty)
            {
                string queryBundle = "Select b.Size as nom1,b.Size as nom2 from POrder p join Bundle b on p.Id_Order = b.Id_Order where p.Id_Order = " + hfidPorder.Value + " group by b.Size order by b.Size asc";
                DataTable dt_Bundle = DataAccess.Get_DataTable(queryBundle);

                DropDownList2.DataSource = dt_Bundle;
                DropDownList2.DataTextField = "nom1";
                DropDownList2.DataValueField = "nom2";
                DropDownList2.DataBind();
            }
        }
        catch (Exception ex)
        {
            Console.Write(ex);
        }
    }

    protected void lnkMostrarBultosIncompletos_Click(object sender, EventArgs e)
    {
        cantidadNoLiberada();
        getBultos();
    }

    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (hfidPorder.Value != "" && DropDownList2.SelectedItem.Text != "Select...")
            {

                string queryBundle = "select v.Linea,v.POrder,v.Id_Bundle,v.Size,v.Quantity,v.Cantidad,v.Envio from vwgetbultosIncompletos v where v.Id_Order=" + hfidPorder.Value + " and v.Size='" + DropDownList2.SelectedItem.Text + "'";
                DataTable dt = DataAccess.Get_DataTable(queryBundle);

                gridEnvios.DataSource = dt;
                gridEnvios.DataBind();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnver_Click(object sender, EventArgs e)
    {
        cantidadNoLiberada();
    }

    void limpiar()
    {
        drpEnvioDia.SelectedIndex = 0;
    }

    protected void gridEnvios_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (drpEnvioDia.SelectedItem.Text != "Select...")
            {
                if (e.CommandName == "enviar")
                {
                    GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    string idbulto = gridEnvios.DataKeys[indice].Value.ToString();

                    int envioDia = int.Parse(drpEnvioDia.SelectedValue);
                    var txtenvioC = (TextBox)row.FindControl("txtenvio");
                    var mess = (TextBox)row.FindControl("txtcomentario");
                    string user = Context.User.Identity.Name;
                    int envioCorte = int.Parse(txtenvioC.Text);


                    string resp = OrderDetailDA.enviarguardarBihorarioexeption(int.Parse(idbulto),  envioCorte, envioDia, mess.Text, user);

                    if (resp == "OK")
                    {
                        cantidadNoLiberada();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                        limpiar();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
                    }
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
        bultoincompletoviewall();
    }
}