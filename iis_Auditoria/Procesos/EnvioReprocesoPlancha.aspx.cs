﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Procesos_EnvioReprocesoPlancha : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.User.Identity.IsAuthenticated == true)
            {
                Ver();
                Session["rpcenv"] = null;
            }
            else
            {
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select Top 15 POrderClient from POrder p join Style s on s.Id_Style = p.Id_Style where POrderClient like '%" + pre + "%' and s.washed = 1 group by POrderClient", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porderclient = dt.Rows[i][0].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porderclient { get; set; }
    }

    void Ver()
    {
        DataTable dt;

        string query = " select a.corte, a.estilo, (ISNULL(a.Total, 0) - ISNULL(b.Envio, 0)) Total from (select rp.corte, rp.estilo, SUM(unidadesRecibidas) Total from reprocesoplancha rp"
                     + " join detailReprocesoPlancha drp on drp.idObjectReprocesoPlancha = rp.objectIdReprocesoPlancha where estado = 0 group by rp.corte, rp.estilo) a left join"
                     + " (select corte, ISNULL(SUM(unidadesEnviadas), 0) Envio from reprocesoPlanchaEnvio group by corte) b on a.corte = b.corte where (ISNULL(a.Total,0) - ISNULL(b.Envio,0)) != 0";

        dt = DataAccess.Get_DataTable(query);
        Session["rpcenv"] = dt;

        grdReproenv.DataSource = Session["rpcenv"];
        grdReproenv.DataBind();


    }

    void VerporCorte()
    {
        DataTable dt;

        string query = " select a.corte, a.estilo, (ISNULL(a.Total, 0) - ISNULL(b.Envio, 0)) Total from (select rp.corte, rp.estilo, SUM(unidadesRecibidas) Total from reprocesoplancha rp"
                     + " join detailReprocesoPlancha drp on drp.idObjectReprocesoPlancha = rp.objectIdReprocesoPlancha where estado = 0 and rp.corte = '" + txtcorte.Text + "' group by rp.corte, rp.estilo) a left join"
                     + " (select corte, ISNULL(SUM(unidadesEnviadas), 0) Envio from reprocesoPlanchaEnvio corte = '" + txtcorte.Text + "' group by corte) b on a.corte = b.corte where (ISNULL(a.Total,0) - ISNULL(b.Envio,0)) != 0";

        dt = DataAccess.Get_DataTable(query);
        Session["rpcenv"] = dt;

        grdReproenv.DataSource = Session["rpcenv"];
        grdReproenv.DataBind();
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        Ver();
    }

    protected void lkenviar_Click(object sender, EventArgs e)
    {
        try
        {
            int cont = 0;
            int cont2 = 0;

            foreach (GridViewRow item in grdReproenv.Rows)
            {
                string resp = "0";
                var corte = (Label)item.FindControl("lblcorte");
                var estilo = (Label)item.FindControl("lblestilo");
                var unidades = (TextBox)item.FindControl("txtunidades");
                var uniRecibidas = (TextBox)item.FindControl("txtunidadesrec");

                if (uniRecibidas.Text != string.Empty)
                {
                    if (Convert.ToInt16(unidades.Text) >= Convert.ToInt16(uniRecibidas.Text))
                    {
                        resp = OrderDetailDA.EnvioReprocesoPlancha(corte.Text, estilo.Text, int.Parse(uniRecibidas.Text), DateTime.Now, Context.User.Identity.Name, Convert.ToBoolean(1));

                        if (resp.Equals("OK"))
                        {
                            cont++;
                            cont2++;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "piezassobre();", true);
                    }
                }
            }

            if (cont == cont2)
            {
                Ver();

                if (cont2 > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void grdReproenv_DataBinding(object sender, EventArgs e)
    {
        grdReproenv.DataSource = Session["rpcenv"];
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Procesos/EnvioReprocesoPlancha.aspx");
    }
}