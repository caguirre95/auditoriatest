﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EnvioReprocesoPlancha.aspx.cs" Inherits="Procesos_EnvioReprocesoPlancha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "EnvioReprocesoPlancha.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porderclient: item.porderclient,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porderclient);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porderclient);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porderclient + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function alertMasterE(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }

        function limpiar() {

            $('#<%=txtcorte.ClientID%>').val('');

        }
    </script>

    <script type="text/javascript">

        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function piezassobre() {
            swal({
                title: 'No puede mayor la cantidad a lo liberado !',
                type: 'warning'
            });
        }

    </script>

    <link href="../jquery-ui.css" rel="stylesheet" />
    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>

    <style>
        .margin {
            margin-top: 19px;
        }

        .mar_t {
            margin: 10px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <br />
    <div class="container">
        <br />
        <div class="panel panel-primary">
            <div class="panel-heading"><span style="font-weight: bold">Envio Reproceso de Plancha</span></div>
            <div class="panel-body">

                <div class="row">

                    <div class="col-lg-6">
                        Ingreso del Corte
                        <asp:TextBox ID="txtcorte" runat="server" required="true" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Codigo Order" onfocus="limpiar()"></asp:TextBox>
                        <asp:HiddenField ID="idcorte" runat="server" />
                    </div>
                    <div class="col-lg-6">
                        <div class="col-lg-6">
                            <asp:Button ID="btnbuscar" runat="server" Text="Search" CssClass="btn btn-primary form-control margin" OnClick="btnbuscar_Click" />
                        </div>
                        <div class="col-lg-6" style="padding: 0">
                            <asp:Button ID="btnlimpiar" runat="server" Text="Limpiar" CssClass="btn btn-success form-control margin" OnClick="btnlimpiar_Click" />
                        </div>
                    </div>

                </div>

                <hr />

                <asp:GridView ID="grdReproenv" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" CssClass="table table-hover table-striped w" AutoGenerateColumns="false" GridLines="None"
                    OnDataBinding="grdReproenv_DataBinding">
                    <Columns>
                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                            <HeaderTemplate>
                                Corte 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblcorte" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("corte") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                            <HeaderTemplate>
                                Estilo
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblestilo" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("estilo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Total de Unidades
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtunidades" Enabled="false" Width="100px" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Total") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Cantidad a Enviar
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtunidadesrec" Width="100px" Font-Size="Medium" onkeypress="return num(event);" MaxLength="5" CssClass="form-control" Text='' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton Text="" ID="lkenviar" OnClick="lkenviar_Click" CssClass="btn btn-info" runat="server"><i class="glyphicon glyphicon-check"> </i> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

                <div class="row">
                    <hr />
                    <div class="col-lg-12">
                        <asp:Button ID="btntodos" runat="server" Text="Guardar Todos" CssClass="btn btn-primary form-control" OnClick="lkenviar_Click" Visible="true" />
                    </div>
                </div>
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

