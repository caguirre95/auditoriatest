﻿<%@ Page Title="" EnableEventValidation="true" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReprocesoEmpaque.aspx.cs" Inherits="Procesos_ReprocesoEmpaque" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../css/pagination.css" rel="stylesheet" />
    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>    
    <script src="../js/jquery.blockUI.js"></script>

    <script lang="javascript" type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            InitAutoComplCortes();
            InitAutoComplTallas();
        });

        function InitializeRequest(sender, args) {
        }

        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the Autocomplete
            InitAutoComplCortes();
            InitAutoComplTallas();
        }

        function InitAutoComplCortes() {
            $(function () {
                $("[id*='txtCorte']").autocomplete({
                    source: function (request, response) {
                        $("#spLoadingCorte").show(500);

                        $.ajax({
                            url: "../Procesos/ReprocesoEmpaque.aspx/GetPOrder",
                            data: "{'pre' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                var found = $.map(data.d, function (item) {
                                    return {
                                        idorder: item.idorder,
                                        porder: item.porder,
                                        style: item.style,
                                        json: item                                        
                                    }
                                });

                                if (found.length > 0) {
                                    $("#ulNoMatchCorte").hide(500);
                                } else {
                                    $("#ulNoMatchCorte").show(500);
                                }

                                $("#spLoadingCorte").hide(500);
                                response(found);
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $("[id*='txtCorte']").val(ui.item.porder);                    
                        return false;
                    },
                    select: function (event, ui) {
                        $("[id*='txtIdOrder']").val(ui.item.idorder);
                        $("[id*='txtCorte']").val(ui.item.porder);
                        $("[id*='txtEstilo']").val(ui.item.style);

                        $("[id*='txtTalla']").focus();
                        return false;
                    },
                    minLength: 2
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li>").append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
                };
            });
        }

        function InitAutoComplTallas() {
            $(function () {
                $("[id*='txtTalla']").autocomplete({
                    source: function (request, response) {
                        $("#spLoadingTalla").show(500);

                        $.ajax({
                            url: "../Procesos/ReprocesoEmpaque.aspx/GetTallas",
                            data: "{'pre' :'" + $("[id*='txtCorte']").val() + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                var found = $.map(data.d, function (item) {
                                    return {
                                        size: item.size,
                                        unidades: item.unidades,
                                        json: item
                                    }
                                });

                                $("#spLoadingTalla").hide(500);
                                response(found);
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $("[id*='txtTalla']").val(ui.item.size);
                        return false;
                    },
                    select: function (event, ui) {
                        $("[id*='txtTalla']").val(ui.item.size);
                        $("[id*='txtUnidades']").val(ui.item.unidades);

                        $("[id*='txtUnidades']").focus();
                        return false;
                    },
                    minLength: 1
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li>").append("<a class='text-info ' style='padding-left:30px;' >" + item.size + "</a>").appendTo(ul);
                };
            });
        }
    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function alertMasterE(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }
    </script>

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                timer: 1000,
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                timer: 1000,
                type: 'danger'
            });
        }
        function alertPlan(resp) {
            swal({
                title: 'Información: ' + resp,
                timer: 2000,
                type: 'info'
            });
        }
        function alertincompleto() {
            swal({
                title: 'La cantidad no puede ser Cero ni estar Vacio el Campo, y debe seleccionar el tipo de pieza (cancelacion o Nuevo) !',
                timer: 1000,
                type: 'warning'
            });
        }
        function visibleDiv(value) {           
            if (value) {
                document.getElementById('dvCortes').style.display = 'block';
                $("[id*='txtTalla']").focus();
            } else {
                document.getElementById('dvCortes').style.display = 'none';
                fnLimpiar();
            }
        }
        function fnLimpiar() {
            $("[id*='txtIdOrder']").val('');
            $("[id*='txtCorte']").val('');
            $("[id*='txtEstilo']").val('');
            $("[id*='txtTalla']").val('');
            $("[id*='txtUnidades']").val('');
            $("[id*='txtObservacion']").val('');
        }

        function mostrarLoader() {
            $('.blockMe').block({
                message: 'Cargando...<br /><img src="../images/loadingBar.gif" />',
                css: { 'padding-top': '5px' }
            });

            setTimeout(() => {
                $('.blockMe').unblock();
            }, 500);
        }

        function mostrarLoader2() {
            $('.blockMe2').block({
                message: 'Cargando...<br /><img src="../images/loadingBar.gif" width="75" />',
                css: { 'padding-top': '5px' }
            });

            setTimeout(() => {
                $('.blockMe2').unblock();
            }, 500);
        }

        function mostrarLoader3() {
            $('.blockMe3').block({
                message: 'Cargando...<br /><img src="../images/loadingBar.gif" width="75" />',
                css: { 'padding-top': '5px' }
            });

            setTimeout(() => {
                $('.blockMe3').unblock();
            }, 500);
        }
    </script>
    <style type="text/css">
          @media (min-width: 661px){
            .col-md-6 {
                width: 50%;
                float: left;
            }
        }
          
          td 
          {
            padding: 0 25px;
          }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <br />
    <div class="container">
        <br />        
        <div class="panel panel-primary">
            <div class="panel-heading"><span style="font-weight: bold">REPROCESO REPARACIÓN Y SEGUNDAS</span></div>
            <div class="panel-body">
                <div class="row">
                    <asp:UpdatePanel ID="upd1" runat="server">
                        <ContentTemplate>
                            <div class="row" id="dvCortes">                                
                                <div class="col-md-3 col-sm-3 col-xs-3" style="padding-left: 30px">              
                                    <asp:Label ID="lblCorte" runat="server" Text="CORTE" Font-Size="Smaller" Font-Bold="true"></asp:Label>
                                    <asp:TextBox ID="txtCorte" runat="server" CssClass="form-control" Font-Size="small" MaxLength="25" placeholder="Digite el número de corte"></asp:TextBox>
                                    <asp:HiddenField ID="txtIdOrder" runat="server" />
                                    <div class="spinner-border" role="status" id="spLoadingCorte" style="display: none">
                                        <span class="visually-hidden" style="color: crimson; font-weight: bold">Buscando...</span>
                                    </div>
                                    <ul id="ulNoMatchCorte"
                                        class="ui-autocomplete ui-menu ui-widget1 ui-widget1-content ui-corner-all"
                                        role="listbox"
                                        aria-activedescendant="ui-active-menuitem"
                                        style="z-index: 16; display: none; font-weight: bold; position: relative; color: red; font-size: 12px">
                                        <li class="ui-menu-item"
                                            role="menuitem">
                                            <a class="ui-corner-all" tabindex="-1" style="color: #fff; background-color: #d9534f">EL NUMERO DE CORTE NO EXISTE</a></li>
                                    </ul>
                                    <div class="spinner-border" role="status">
                                      <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblEstilo" runat="server" Text="ESTILO" Font-Size="Smaller" Font-Bold="true"></asp:Label>
                                    <asp:TextBox ID="txtEstilo" Enabled="false"  runat="server" CssClass="form-control" Font-Size="small" MaxLength="100"></asp:TextBox>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <asp:Label ID="lblTalla" runat="server" Text="TALLA" Font-Size="Smaller" Font-Bold="true"></asp:Label>
                                    <asp:TextBox ID="txtTalla" runat="server" CssClass="form-control" Font-Size="small" MaxLength="10"></asp:TextBox>
                                    <div class="spinner-border" role="status" id="spLoadingTalla" style="display: none">
                                        <span class="visually-hidden" style="color: crimson; font-weight: bold">Buscando...</span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3" style="padding-right: 30px;">
                                    <asp:Label ID="lblUnidades" runat="server" Text="UNIDADES" Font-Size="Smaller" Font-Bold="true"></asp:Label>
                                    <asp:TextBox ID="txtUnidades" runat="server" CssClass="form-control" MaxLength="6" onkeypress="return num(event);" Font-Size="small" TextMode="Number" placeholder="Cantidad reparada"></asp:TextBox>
                                </div>                              
                                
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-top: 10px; padding-left: 30px;">
                                    <asp:Label ID="lblPosicion" runat="server" Text="POSICIÓN" Font-Size="Smaller" Font-Bold="true"></asp:Label>
                                    <asp:DropDownList ID="ddlPosicion" runat="server" CssClass="form-control" Font-Size="small" Width="100%"></asp:DropDownList>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-top: 10px">
                                    <asp:Label ID="lblDefectos" runat="server" Text="DEFECTO" Font-Size="Smaller" Font-Bold="true"></asp:Label>
                                    <asp:DropDownList ID="ddlDefectos" runat="server" CssClass="form-control" Font-Size="small" Width="100%"></asp:DropDownList>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-right: 30px; padding-top: 10px">
                                    <asp:Label ID="lblObservacion" runat="server" Text="OBSERVACIÓN" Font-Size="Smaller" Font-Bold="true"></asp:Label>
                                    <asp:TextBox ID="txtObservacion" runat="server" CssClass="form-control" Font-Size="small" TextMode="MultiLine" placeholder="Observaciones sobre la reparación"></asp:TextBox>
                                </div>
                                <br />
                                <br />
                                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-right: 30px; padding-left: 30px; padding-top: 25px">
                                    <table style="width:500px;align-content:center;">
                                        <tbody>
                                            <tr>
                                                <td style="">
                                                    <asp:LinkButton ID="btnAgregar" runat="server" CssClass="form-control btn btn-success" OnClick="btnAgregar_Click"><i class="icon-plus icon-white" style="margin-right: 10px;width:150px;"></i>Agregar</asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="btnConsultar" runat="server" CssClass="form-control btn btn-success" OnClick="btnConsultar_Click"><i class="icon-plus icon-white" style="margin-right: 10px;width:150px;"></i>Consultar</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                </div>
                                
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-right: 30px; padding-left: 30px; padding-top: 5px">
                                    <asp:Label runat="server" Text="DETALLE DE REPARACIONES" Font-Bold="true" Font-Size="Small"></asp:Label>                                    
                                    <asp:GridView ID="grdReparaciones" 
                                        runat="server"
                                        DataKeyNames="Id" 
                                        AllowPaging="true"
                                        PageSize="5"
                                        ShowFooter="true"
                                        HeaderStyle-BackColor="#337ab7"
                                        HeaderStyle-ForeColor="White" 
                                        CssClass="table table-hover table-responsive table-striped blockMe"
                                        AutoGenerateColumns="False"
                                        GridLines="Vertical"
                                        OnPageIndexChanging="grdReparaciones_PageIndexChanging"
                                        OnRowCommand="grdReparaciones_RowCommand">
                                        <PagerStyle CssClass="pagination-ys" />
                                        <EmptyDataTemplate>
                                            <label style="color:#808080; font-size: 14px;">No hay datos agregados</label>
                                        </EmptyDataTemplate>
                                        <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Id" Visible="false" DataField="Id" />
                                            <asp:BoundField HeaderText="CORTE" DataField="Orden" >
                                                <HeaderStyle Font-Size="Smaller"/>
                                                <ControlStyle Font-Size="Small" />
                                            </asp:BoundField>

                                            <asp:BoundField HeaderText="DEFECTO" DataField="Defecto" >
                                                <HeaderStyle Font-Size="Smaller" />
                                                <ControlStyle Font-Size="Small" />
                                            </asp:BoundField>

                                            <asp:BoundField HeaderText="POSICIÓN" DataField="Posicion" >
                                                <HeaderStyle Font-Size="Smaller" />
                                                <ControlStyle Font-Size="Small" />
                                            </asp:BoundField>

                                            <asp:BoundField HeaderText="TALLA" 
                                                ControlStyle-Font-Size="Small" 
                                                DataField="Talla"
                                                ItemStyle-HorizontalAlign="Center" 
                                                FooterText="GRAN TOTAL:" 
                                                FooterStyle-Font-Size="Smaller" 
                                                FooterStyle-Font-Bold="true" 
                                                FooterStyle-BackColor="#cccccc" 
                                                FooterStyle-HorizontalAlign="Center" >
                                                <HeaderStyle CssClass="text-center" Font-Size="Smaller"/>
                                                <ControlStyle Font-Size="Small" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="UNIDADES" 
                                                DataField="Unidades" 
                                                ControlStyle-Font-Size="Small" 
                                                ItemStyle-HorizontalAlign="Center" 
                                                FooterStyle-HorizontalAlign="Center" 
                                                FooterStyle-BackColor="#cccccc" 
                                                FooterStyle-Font-Bold="true" 
                                                FooterStyle-Font-Size="Smaller"
                                                DataFormatString="{0:N0}">
                                                <HeaderStyle CssClass="text-center" Font-Size="Smaller"/>
                                                <ControlStyle Font-Size="Small" />
                                            </asp:BoundField>
                                            <%--<asp:BoundField HeaderText="COMENTARIOS" ControlStyle-Font-Size="Small" DataField="Observaciones" >
                                                <ControlStyle Font-Size="Small" />
                                            </asp:BoundField>--%>
                                            <asp:TemplateField HeaderText="ELIMINAR" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Smaller">
                                                <ItemTemplate>
                                                    <asp:LinkButton Text="" ID="lnkBorrar" CssClass="btn btn-danger" runat="server" CommandName="BorrarReg"><i class="glyphicon glyphicon-trash"> </i> </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#337AB7" ForeColor="White" />
                                    </asp:GridView>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                    <div class="col-md-6">
                                        <asp:Label runat="server" Text="SUMATORIA POR CORTES" Font-Bold="true" Font-Size="Small"></asp:Label>
                                        <asp:GridView ID="grdCortes" 
                                            runat="server" 
                                            AllowPaging="true"
                                            AllowSorting="true"                            
                                            PageSize="5"
                                            ShowFooter="true"
                                            HeaderStyle-BackColor="#337ab7" 
                                            HeaderStyle-ForeColor="White" 
                                            DataKeyNames="Corte"
                                            CssClass="table table-hover table-responsive table-striped blockMe2"
                                            AutoGenerateColumns="False"
                                            GridLines="Vertical" 
                                            OnPageIndexChanging="grdCortes_PageIndexChanging"
                                            >
                                            <PagerStyle CssClass="pagination-ys" />
                                            <EmptyDataTemplate>
                                                <label style="color:#808080; font-size: 14px;">Sin registros</label>
                                            </EmptyDataTemplate>
                                            <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <Columns>
                                                <asp:BoundField HeaderText="CORTE" 
                                                    DataField="Corte" 
                                                    FooterText="TOTAL:" 
                                                    FooterStyle-Font-Size="Small" 
                                                    FooterStyle-Font-Bold="true" 
                                                    FooterStyle-BackColor="#cccccc" 
                                                    FooterStyle-HorizontalAlign="Center" 
                                                    ControlStyle-Font-Size="Small" 
                                                    ItemStyle-HorizontalAlign="Center">
                                                    <HeaderStyle Font-Size="Smaller" CssClass="text-center"/>
                                                    <ItemStyle Font-Size="Small" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="UNIDADES" 
                                                    DataField="Unidades" 
                                                    ItemStyle-HorizontalAlign="Center"
                                                    FooterStyle-HorizontalAlign="Center" 
                                                    FooterStyle-BackColor="#cccccc"
                                                    FooterStyle-Font-Bold="true" 
                                                    FooterStyle-Font-Size="Small"
                                                    DataFormatString="{0:N0}">
                                                    <HeaderStyle Font-Size="Smaller" CssClass="text-center"/>
                                                    <ItemStyle Font-Size="Small" />
                                                </asp:BoundField>
                                            </Columns>
                                            <HeaderStyle BackColor="#337AB7" ForeColor="White" />
                                        </asp:GridView>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:Label runat="server" Text="SUMATORIA POR DEFECTOS" Font-Bold="true" Font-Size="Small"></asp:Label>
                                        <asp:GridView ID="grdDefectos"
                                            runat="server" 
                                            AllowPaging="true"
                                            AllowSorting="true"                            
                                            PageSize="5"
                                            ShowFooter="true"
                                            HeaderStyle-BackColor="#337ab7" 
                                            HeaderStyle-ForeColor="White" 
                                            DataKeyNames="Defecto"
                                            CssClass="table table-hover table-responsive table-striped blockMe3" 
                                            AutoGenerateColumns="False"
                                            GridLines="Vertical" OnPageIndexChanging="grdDefectos_PageIndexChanging" 

                                            >
                                            <PagerStyle CssClass="pagination-ys" />
                                            <EmptyDataTemplate>
                                                <label style="color:#808080; font-size: 14px;">Sin registros</label>
                                            </EmptyDataTemplate>
                                            <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <Columns>
                                                <asp:BoundField HeaderText="DEFECTO" 
                                                    DataField="Defecto" 
                                                    FooterText="TOTAL:" 
                                                    FooterStyle-Font-Size="Small" 
                                                    FooterStyle-Font-Bold="true" 
                                                    FooterStyle-BackColor="#cccccc" 
                                                    FooterStyle-HorizontalAlign="Center"
                                                    >
                                                    <HeaderStyle Font-Size="Smaller" CssClass="text-center"/>
                                                    <ItemStyle Font-Size="Small" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="UNIDADES" 
                                                    DataField="Unidades" 
                                                    ItemStyle-HorizontalAlign="Center"
                                                    FooterStyle-HorizontalAlign="Center" 
                                                    FooterStyle-BackColor="#cccccc"
                                                    FooterStyle-Font-Bold="true"
                                                    FooterStyle-Font-Size="Small"
                                                    DataFormatString="{0:N0}">
                                                    <HeaderStyle Font-Size="Smaller" CssClass="text-center"/>
                                                    <ItemStyle Font-Size="Small" />
                                                </asp:BoundField>
                                            </Columns>
                                            <HeaderStyle BackColor="#337AB7" ForeColor="White" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <hr />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

