﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Procesos_HTEnvios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.User.Identity.IsAuthenticated == true)
            {
                //Session["HT"] = null;
                envioHT();
            }
            else
            {
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select corte from accesorioHT where corte like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.corte = dt.Rows[i][0].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string corte { get; set; }
    }

    void envioHT()
    {
        try
        {
            DataTable dt;

            string query = " select a.corte, a.quantity, a.estilo, a.UnidadesAenviar, (a.quantity - ISNULL(b.enviadasI, 0)) Deficit from ("
                         + " select a1.corte, a1.quantity, a1.estilo, (ISNULL(a1.Total, 0) - ISNULL(b1.UnidadesAenviar, 0)) UnidadesAenviar from ("
                         + " select a.corte, a.quantity, a.estilo, SUM(ISNULL(ad.cantidad, 0)) Total from accesorioHT a join accesorioHTDetail ad on a.idObjectHT = ad.idObjectHT"
                         + " where ISNULL(a.estadoEnvio, 0) = 0 group by a.corte, a.quantity, a.estilo) a1 join"
                         + " (select a.corte, SUM(ISNULL(ae.unidadesEnviadas, 0)) UnidadesAenviar from accesorioHT a left join accesorioHTEnvio ae on ae.corte = a.corte"
                         + " where ISNULL(a.estadoEnvio, 0) = 0 group by a.corte) b1 on a1.corte = b1.corte) a left join (select corte, sum(ISNULL(unidadesEnviadas, 0)) enviadasI"
                         + " from accesorioHTEnvio group by corte) b on a.corte = b.corte";

            dt = DataAccess.Get_DataTable(query);
            Session["HT"] = dt;
            grdHTEnvios.DataSource = Session["HT"];
            grdHTEnvios.DataBind();

        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void lnkRecibir_Click(object sender, EventArgs e)
    {
        try
        {
            int cont = 0;
            int cont2 = 0;

            foreach (GridViewRow item in grdHTEnvios.Rows)
            {
                string resp = "0";
                var corte = (Label)item.FindControl("lblcorte");
                var quantity = (Label)item.FindControl("lblquantity");
                var estilo = (Label)item.FindControl("lblestilo");
                var unidadeEnv = (Label)item.FindControl("lblcantidadA");
                var unidadesRec = (TextBox)item.FindControl("txtunidadesEnv");
                var def = (TextBox)item.FindControl("txtdef");

                if (unidadesRec.Text != string.Empty)
                {
                    if (Convert.ToInt16(unidadeEnv.Text) >= Convert.ToInt16(unidadesRec.Text))
                    {
                        resp = OrderDetailDA.EnvioHT(corte.Text, estilo.Text, int.Parse(unidadesRec.Text), DateTime.Now, Context.User.Identity.Name, Convert.ToBoolean(1));
                        cont++;
                        cont2++;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "piezassobre();", true);
                    }
                }
            }

            if (cont == cont2)
            {
                envioHT();
                if (cont2 > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
            }

        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void grdHTEnvios_DataBinding(object sender, EventArgs e)
    {
        //grdHTEnvios.DataSource = Session["HT"];
    }

    protected void btnguardar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtcorte.Text != string.Empty && txtcant.Text != string.Empty)
            {
                string resp = OrderDetailDA.HeatTransferIrregulares(txtcorte.Text, int.Parse(txtcant.Text), DateTime.Now, txtobser.Text, Context.User.Identity.Name);

                if (resp.Equals("OK"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                    txtcorte.Text = "";
                    txtcant.Text = "";
                    txtobser.Text = "";
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
                }
            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }
}