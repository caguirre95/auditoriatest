﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HeatTransfer2.aspx.cs" Inherits="Procesos_HeatTransfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <link href="../jquery-ui.css" rel="stylesheet" />
    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>

    <script lang="javascript" type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            InitAutoComplCortes();
            InitAutoComplColaboradores();
        });

        function InitializeRequest(sender, args) {
        }

        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the Autocomplete
            InitAutoComplCortes();
            InitAutoComplColaboradores();
        }

        function InitAutoComplCortes() {
            $(function () {
                $("[id*='txtCorte']").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "../Procesos/HeatTransfer2.aspx/GetPOrder",
                            data: "{'pre' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                var found = $.map(data.d, function (item) {
                                    return {
                                        idorder: item.idorder,
                                        porder: item.porder,
                                        style: item.style,
                                        json: item
                                    }
                                });

                                if (found.length > 0) {
                                    $("#ulNoMatchCorte").hide();
                                } else {
                                    $("#ulNoMatchCorte").show();
                                }

                                response(found);
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $("[id*='txtCorte']").val(ui.item.porder);
                        return false;
                    },
                    select: function (event, ui) {
                        $("[id*='txtIdOrder']").val(ui.item.idorder);
                        $("[id*='txtCorte']").val(ui.item.porder);
                        $("[id*='txtEstilo']").val(ui.item.style);
                        $("[id*='txtStyle']").val(ui.item.style);

                        $("[id*='txtUnidades']").focus();
                        return false;
                    },
                    minLength: 2
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li>").append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
                };
            });
        }

        function InitAutoComplColaboradores() {
            $(function () {
                $("[id*='txtColaborador']").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "../Procesos/HeatTransfer2.aspx/GetColaboradores",
                            data: "{'pre' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                var found = $.map(data.d, function (item) {
                                    return {
                                        idresponsable: item.idresponsable,
                                        responsable: item.responsable,
                                        idcolaborador: item.idcolaborador,
                                        colaborador: item.colaborador,
                                        json: item
                                    }
                                });

                                if (found.length > 0) {
                                    $("#ulNoMatchColaborador").hide();
                                } else {
                                    $("#ulNoMatchColaborador").show();
                                }

                                response(found);
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $("[id*='txtColaborador']").val(ui.item.colaborador);

                        return false;
                    },
                    select: function (event, ui) {
                        $("#ulNoMatchColaborador").hide();
                        $("[id*='txtIdResponsable']").val(ui.item.idresponsable);
                        $("[id*='txtResponsable']").val(ui.item.responsable);
                        $("[id*='txtIdColaborador']").val(ui.item.idcolaborador);
                        $("[id*='txtColaborador']").val(ui.item.colaborador);

                        $("[id*='txtCorte']").focus();
                        return false;
                    },
                    minLength: 2
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li>").append("<a class='text-info ' style='padding-left:30px;' >" + item.colaborador + "</a>").appendTo(ul);
                };
            });
        }
    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function alertMasterE(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }
    </script>

    <script type="text/javascript">        
        function alertExitoso() {
            swal({
                title: 'Exito!',                
                text: 'Proceso exitoso',
                type: 'success'
            });
        }
        function alertPlan(msg) {
            swal({
                title: 'Error de validación',                
                text: msg,
                type: 'warning'
            });
        }
        function alertError(msg) {
            swal({
                title: 'Lo sentimos ha ocurrido un error',
                text: msg,
                type: 'error'
            });
        }
        function ocultarModal() {
            swal({
                title: 'Exito!',
                text: 'El colaborador fuer creado exitosamente',
                type: 'success'
            });

            // Cerramos el modal
            $("#ventanamodal").modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }
    </script>

    <style>
        .margin {
            margin-top: 20px;
        }

        .mt {
            margin-top: 10px;
        }

        .mt-2 {
            margin-top: 20px;
        }

        .pt {
            padding-top: 5px !important;
        }

        .p-1 {
            padding-left: 30px;
            padding-right: 30px;
        }

        .p-2 {
            padding-top: 24px;
        }

        @media (max-width: 660px){
            .col-md-12 {
                width: 100%;
            }
        }
        @media (min-width: 880px) {
          .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-6, .col-md-12 {
            float: left;
          }
          .col-md-12 {
            width: 100%;
          }
          .col-md-6 {
            width: 50%;
          }
          .col-md-5 {
            width: 41.66666667%;
          }
          .col-md-4 {
            width: 33.33333333%;
          }
          .col-md-3 {
            width: 25%;
          }
          .col-md-2 {
            width: 16.66666667%;
          }          
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">
        <br />
        <div class="panel panel-primary">
            <div class="panel-heading"><span style="font-weight: bold">PRODUCCIÓN ÁREA HEAT TRANSFER</span></div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <asp:Label runat="server" Text="OPERACIÓN" Font-Size="Small" Font-Bold="true"></asp:Label>
                            <asp:DropDownList ID="ddlOperacion" runat="server" CssClass="form-control mt" Font-Size="Small" AutoPostBack="true" OnSelectedIndexChanged="ddlOperacion_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-4 mt">
                            <asp:Label runat="server" Text="BUSCAR COLABORADOR" Font-Size="Small" Font-Bold="true"></asp:Label>
                            <asp:HiddenField ID="txtIdColaborador" runat="server" />
                            <asp:TextBox ID="txtColaborador" runat="server" required="true" CssClass="form-control mt" Font-Size="Small" AutoCompleteType="Disabled" placeholder="Escriba el nombre del Colaborador"></asp:TextBox>
                            <ul id="ulNoMatchColaborador"
                                class="ui-autocomplete ui-menu ui-widget1 ui-widget1-content ui-corner-all"
                                role="listbox"
                                aria-activedescendant="ui-active-menuitem"
                                style="z-index: 16; display: none; font-weight: bold; position: relative; color: red; font-size: 12px">
                                <li class="ui-menu-item"
                                    role="menuitem">
                                    <a class="ui-corner-all" tabindex="-1" style="color: #fff; background-color: #d9534f">EL COLABORADOR NO ESTÁ REGISTRADO</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3 mt">
                            <asp:LinkButton ID="btnAgregarColaborador" runat="server" CssClass="form-control btn btn-info"  data-toggle="modal" data-target="#ventanamodal"><i class="icon-plus icon-white" style="margin-right: 10px"></i>Agregar nuevo</asp:LinkButton>
                        </div>
                        <div class="col-md-3 mt">
                            <asp:Label runat="server" Text="RESPONSABLE" Font-Size="Small" Font-Bold="true"></asp:Label>
                            <asp:HiddenField ID="txtIdResponsable" runat="server" />
                            <asp:TextBox ID="txtResponsable" runat="server" required="true" CssClass="form-control mt" Font-Size="Small" Enabled="false"></asp:TextBox>
                        </div>
                    </div>                     
                    <div class="col-md-12 mt">
                        <div class="col-md-4">
                            <asp:Label runat="server" Text="INGRESE EL CORTE" Font-Size="Small" Font-Bold="true"></asp:Label>
                            <asp:TextBox ID="txtCorte" runat="server" required="true" CssClass="form-control mt" Font-Size="Small" AutoCompleteType="Disabled" placeholder="Codigo corte"></asp:TextBox>
                            <asp:HiddenField ID="txtIdOrder" runat="server" />
                            <ul id="ulNoMatchCorte"
                                class="ui-autocomplete ui-menu ui-widget1 ui-widget1-content ui-corner-all"
                                role="listbox"
                                aria-activedescendant="ui-active-menuitem"
                                style="z-index: 16; display: none; font-weight: bold; position: relative; color: red; font-size: 12px">
                                <li class="ui-menu-item"
                                    role="menuitem">
                                    <a class="ui-corner-all" tabindex="-1" style="color: #fff; background-color: #d9534f">EL NUMERO DE CORTE NO EXISTE</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 mt">
                            <asp:Label runat="server" Text="ESTILO" Font-Size="Small" Font-Bold="true"></asp:Label>
                            <asp:TextBox ID="txtEstilo" runat="server" CssClass="form-control mt" Font-Size="Small" placeholder="Estilo" Enabled="false"></asp:TextBox>
                            <asp:HiddenField ID="txtStyle" runat="server" />
                        </div>
                        <div class="col-md-4 mt">
                            <asp:Label runat="server" Text="AGREGAR UNIDADES" Font-Size="Small" Font-Bold="true"></asp:Label>
                            <asp:TextBox ID="txtUnidades" runat="server" required="true" CssClass="form-control mt" Font-Size="Small" textMode="Number" MaxLength="6" style="text-align: right"></asp:TextBox>                        
                        </div>
                    </div>
                </div>

                <hr />

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                             <asp:LinkButton ID="btnGuardar" runat="server" CssClass="form-control btn btn-success" OnClick="btnGuardar_Click"><i class="icon-save icon-white" style="margin-right: 10px"></i>Guardar</asp:LinkButton>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-md-12 p-1">
                        <asp:Label runat="server" Text="DETALLE DE PRODUCCIÓN" Font-Bold="true" Font-Size="Small"></asp:Label>
                        <asp:GridView ID="grdProduccion"
                            runat="server"
                            AllowPaging="true"
                            AllowSorting="true"
                            PageSize="5"
                            ShowFooter="true"
                            HeaderStyle-BackColor="#337ab7" 
                            HeaderStyle-ForeColor="White" 
                            DataKeyNames="IdProduccion"
                            CssClass="table table-hover table-responsive table-striped" 
                            AutoGenerateColumns="False"
                            GridLines="Vertical" 
                            OnPageIndexChanging="grdProduccion_PageIndexChanging" OnRowCommand="grdProduccion_RowCommand">
                            <EmptyDataTemplate>
                                <label style="color:#808080; font-size: 14px;">No hay datos agregados</label>
                            </EmptyDataTemplate>
                            <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <Columns>
                                <asp:BoundField HeaderText="ID" Visible="false" DataField="IdProduccion" />
                                <asp:BoundField HeaderText="TIPO DE OPERACIÓN" DataField="Operacion">
                                    <ItemStyle Font-Size="Smaller" CssClass="text-center"/>
                                    <HeaderStyle Font-Size="Smaller"/>
                                </asp:BoundField>
                                <asp:BoundField HeaderText="COLABORADOR" DataField="Colaborador">
                                    <HeaderStyle Font-Size="Smaller" />
                                    <ItemStyle Font-Size="Smaller" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="CORTE" DataField="IdCorte" ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle Font-Size="Smaller" CssClass="text-center"/>
                                    <ItemStyle Font-Size="Smaller" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="ESTILO" 
                                    DataField="IdStyle" 
                                    FooterText="GRAN TOTAL:" 
                                    FooterStyle-Font-Size="Smaller" 
                                    FooterStyle-Font-Bold="true" 
                                    FooterStyle-BackColor="#cccccc" 
                                    FooterStyle-HorizontalAlign="Center" 
                                    ControlStyle-Font-Size="Medium" 
                                    ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle Font-Size="Smaller" CssClass="text-center"/>
                                    <ItemStyle Font-Size="Smaller" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="UNIDADES" 
                                    DataField="Unidades" 
                                    ItemStyle-HorizontalAlign="Center" 
                                    FooterStyle-HorizontalAlign="Center" 
                                    FooterStyle-BackColor="#cccccc" 
                                    FooterStyle-Font-Bold="true" 
                                    FooterStyle-Font-Size="Smaller"
                                    DataFormatString="{0:N0}">
                                    <HeaderStyle Font-Size="Smaller" CssClass="text-center"/>
                                    <ItemStyle Font-Size="Smaller" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="ELIMINAR" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:LinkButton Text="" ID="lnkBorrar" CssClass="btn btn-danger" runat="server" CommandName="BorrarReg"><i class="glyphicon glyphicon-trash"> </i> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#337AB7" ForeColor="White" />
                        </asp:GridView>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-md-6 p-1">
                        <asp:Label runat="server" Text="SUMATORIA POR CORTES Y OPERACIÓN" Font-Bold="true" Font-Size="Small"></asp:Label>
                        <asp:GridView ID="grdCortes" 
                            runat="server" 
                            ShowFooter="true"
                            HeaderStyle-BackColor="#337ab7" 
                            HeaderStyle-ForeColor="White" 
                            DataKeyNames="IdCorte"
                            CssClass="table table-hover table-responsive table-striped" 
                            AutoGenerateColumns="False"
                            GridLines="Vertical">
                            <EmptyDataTemplate>
                                <label style="color:#808080; font-size: 14px;">No hay datos agregados</label>
                            </EmptyDataTemplate>
                            <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <Columns>
                                <asp:BoundField HeaderText="CORTE" 
                                    DataField="IdCorte" 
                                    FooterText="TOTAL:" 
                                    FooterStyle-Font-Size="Smaller" 
                                    FooterStyle-Font-Bold="true" 
                                    FooterStyle-BackColor="#cccccc" 
                                    FooterStyle-HorizontalAlign="Center" 
                                    ControlStyle-Font-Size="Small" 
                                    ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle Font-Size="Smaller" CssClass="text-center"/>
                                    <ItemStyle Font-Size="Smaller" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="UNIDADES" 
                                    DataField="Unidades" 
                                    ItemStyle-HorizontalAlign="Center"
                                    FooterStyle-HorizontalAlign="Center" 
                                    FooterStyle-BackColor="#cccccc"
                                    FooterStyle-Font-Bold="true" 
                                    FooterStyle-Font-Size="Smaller"
                                    DataFormatString="{0:N0}">
                                    <HeaderStyle Font-Size="Smaller" CssClass="text-center"/>
                                    <ItemStyle Font-Size="Smaller" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle BackColor="#337AB7" ForeColor="White" />
                        </asp:GridView>
                    </div>
                    <div class="col-md-6 p-1">
                        <asp:Label runat="server" Text="SUMATORIA POR BIORARIOS" Font-Bold="true" Font-Size="Small"></asp:Label>
                        <asp:GridView ID="grdBihorario"
                            runat="server" 
                            ShowFooter="true"
                            HeaderStyle-BackColor="#337ab7" 
                            HeaderStyle-ForeColor="White" 
                            DataKeyNames="Idbihorario"
                            CssClass="table table-hover table-responsive table-striped" 
                            AutoGenerateColumns="False"
                            GridLines="Vertical">
                            <EmptyDataTemplate>
                                <label style="color:#808080; font-size: 14px;">No hay datos agregados</label>
                            </EmptyDataTemplate>
                            <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <Columns>
                                <asp:BoundField HeaderText="BIHORARIO" 
                                    DataField="Descripcion" 
                                    FooterText="TOTAL:" 
                                    FooterStyle-Font-Size="Smaller" 
                                    FooterStyle-Font-Bold="true" 
                                    FooterStyle-BackColor="#cccccc" 
                                    FooterStyle-HorizontalAlign="Center" 
                                    ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle Font-Size="Smaller" CssClass="text-center"/>
                                    <ItemStyle Font-Size="Smaller" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="UNIDADES" 
                                    DataField="Unidades" 
                                    ItemStyle-HorizontalAlign="Center"
                                    FooterStyle-HorizontalAlign="Center" 
                                    FooterStyle-BackColor="#cccccc"
                                    FooterStyle-Font-Bold="true"
                                    FooterStyle-Font-Size="Smaller"
                                    DataFormatString="{0:N0}">
                                    <HeaderStyle Font-Size="Smaller" CssClass="text-center"/>
                                    <ItemStyle Font-Size="Smaller" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle BackColor="#337AB7" ForeColor="White" />
                        </asp:GridView>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
     <div class="modal fade" id="ventanamodal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Añadir un colaborador</h4>
                </div>
                <div class="modal-body" style="height: 220px">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>                         
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Label runat="server" Text="COLABORADOR" Font-Size="Small"></asp:Label>
                                    <asp:TextBox ID="txtAdColaborador" runat="server" required="true" CssClass="form-control mt" Font-Size="Small" placeholder="Escriba el nombre del Colaborador"></asp:TextBox>
                                </div>
                                <div class="col-md-12 mt">
                                    <asp:Label runat="server" Text="RESPONSABLE" Font-Size="Small"></asp:Label>
                                    <asp:DropDownList ID="ddlResponsable" runat="server" CssClass="form-control mt" Font-Size="Smaller"></asp:DropDownList>
                                </div>
                                <div class="col-md-6 p-2" style="float: right">
                                    <asp:LinkButton ID="btnGuardarModal" runat="server" CssClass="form-control btn btn-success" OnClick="btnGuardarModal_Click"><i class="icon-save icon-white" style="margin-right: 10px"></i>Guardar</asp:LinkButton>
                                </div>
                            </div>
                            <br />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>               
            </div>
        </div>
    </div>
</asp:Content>
