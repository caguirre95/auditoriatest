﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Procesos_ReprocesoEmpaque : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
            CargarDefectos();
            CargarPosicion();
            CargaInfoReparaciones();
        }
    }

    private void CargaInfoReparaciones()
    {
        try
        {
            // Detalle de Reparaciones
            int total = 0;
            DataTable dt;
            string query = @"SELECT r.objectIdReparaciones Id,
	                                        r.Orden,
	                                        td.Defecto,
	                                        tp.Posicion,
	                                        r.Talla,
	                                        r.Unidades,
	                                        r.Observaciones
                                        FROM Reparaciones r
	                                        INNER JOIN tbDefectos td ON r.idDefecto = td.idDefecto
	                                        INNER JOIN tbPosicion tp ON r.idPosicion = tp.idPosicion
                                        WHERE CAST(r.fechaRegistro AS DATE) = CAST(GETDATE() AS DATE)
                                        ORDER BY 1 DESC";

            dt = DataAccess.Get_DataTable(query);

            for (int item = 0; item < dt.Rows.Count; item++)
            {
                total += int.Parse(dt.Rows[item][5].ToString());
            }

            Session["DtReparaciones"] = dt;
            grdReparaciones.Columns[5].FooterText = string.Format("{0:n0}", total);
            grdReparaciones.DataSource = dt;
            grdReparaciones.DataBind();

            // Sumatoria total por cortes.
            string query2 = @"SELECT orden Corte, SUM(unidades) Unidades
                                FROM dbo.Reparaciones
                                WHERE CAST(fechaRegistro AS DATE) = CAST(GETDATE() AS DATE)
                                GROUP BY orden
                                ORDER BY unidades DESC";

            DataTable dt2 = DataAccess.Get_DataTable(query2);
            total = 0;
            for (int item = 0; item < dt2.Rows.Count; item++)
            {
                total += int.Parse(dt2.Rows[item][1].ToString());
            }

            Session["DtCortes"] = dt2;
            grdCortes.Columns[1].FooterText = string.Format("{0:n0}", total);
            grdCortes.DataSource = dt2;
            grdCortes.DataBind();


            // Sumatoria total por cortes.
            string query3 = @"SELECT td.Defecto, SUM(r.unidades) Unidades
                                FROM dbo.Reparaciones r
                                INNER JOIN dbo.tbDefectos td ON r.idDefecto = td.idDefecto
                                WHERE CAST(fechaRegistro AS DATE) = CAST(GETDATE() AS DATE)
                                GROUP BY td.Defecto
                                ORDER BY Unidades DESC";

            DataTable dt3 = DataAccess.Get_DataTable(query3);
            total = 0;
            for (int item = 0; item < dt2.Rows.Count; item++)
            {
                total += int.Parse(dt2.Rows[item][1].ToString());
            }

            Session["DtDefectos"] = dt3;
            grdDefectos.Columns[1].FooterText = string.Format("{0:n0}", total);
            grdDefectos.DataSource = dt3;
            grdDefectos.DataBind();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('" + ex.Message + "');", true);
        }
    }

    private void Limpiar()
    {
        ddlPosicion.SelectedIndex = 0;
        ddlDefectos.SelectedIndex = 0;
        txtObservacion.Text = "";
        txtTalla.Text = "";
        txtUnidades.Text = "";
    }

    private void CargarDefectos()
    {
        DataTable dt = DataAccess.Get_DataTable("SELECT IdDefecto, Defecto FROM tbDefectos WHERE Reparacion = 1 ORDER BY Defecto");

        ddlDefectos.DataSource = dt;
        ddlDefectos.DataTextField = "Defecto";
        ddlDefectos.DataValueField = "IdDefecto";
        ddlDefectos.DataBind();
        ddlDefectos.Items.Insert(0, new ListItem("Seleccione un defecto...", "0"));
    }

    private void CargarPosicion()
    {        
        DataTable dt = DataAccess.Get_DataTable("SELECT IdPosicion, Posicion FROM tbPosicion WHERE Estado = 1 ORDER BY Posicion");

        ddlPosicion.DataSource = dt;
        ddlPosicion.DataTextField = "Posicion";
        ddlPosicion.DataValueField = "IdPosicion";
        ddlPosicion.DataBind();
        ddlPosicion.Items.Insert(0, new ListItem("Seleccione una posición...", "0"));
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<pOrderClass> GetPOrder(string pre)
    {
        try
        {
            List<pOrderClass> list = new List<pOrderClass>();
            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = string.Format(@"SELECT TOP 10 p.Id_Order, p.POrder, s.Style 
                                            FROM POrder p 
                                            INNER JOIN Style s ON p.Id_Style = s.Id_Style 
                                            WHERE p.POrder like '%{0}%'
                                            ORDER BY len(p.POrder)", pre);
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                pOrderClass obj = new pOrderClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][1].ToString();
                obj.style = dt.Rows[i][2].ToString();

                list.Add(obj);
            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<TallaClass> GetTallas(string pre)
    {
        try
        {
            List<TallaClass> list = new List<TallaClass>();
            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = string.Format(@"SELECT REPLACE(b.Size,'*','X') as size, 
	                                        SUM(b.Quantity) unidades	
	                                        FROM POrder p 
	                                        INNER JOIN Bundle b on b.Id_Order=p.Id_Order 
	                                        WHERE p.POrder LIKE ltrim(rtrim('{0}'))
                                            GROUP BY b.Size", pre);

            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                TallaClass obj = new TallaClass();

                obj.size = dt.Rows[i][0].ToString();
                obj.unidades = int.Parse(dt.Rows[i][1].ToString());

                list.Add(obj);
            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

protected void btnConsultar_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("/Procesos/ReprocesoEmpaqueRPT.aspx", false);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('" + ex.Message + "');", true);            
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlDefectos.SelectedIndex == 0 || ddlPosicion.SelectedIndex == 0 || string.IsNullOrEmpty(txtCorte.Text) || string.IsNullOrEmpty(txtUnidades.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('Algunos campos están vacíos');", true);                
            }
            else
            {                
                string resp = OrderDetailDA.AgregarUnidadesReparaciones(Convert.ToInt32(txtIdOrder.Value), txtCorte.Text, Convert.ToInt32(ddlDefectos.SelectedValue), Convert.ToInt32(ddlPosicion.SelectedValue), txtTalla.Text, "2", Context.User.Identity.Name, Convert.ToInt32(txtUnidades.Text), txtObservacion.Text, txtEstilo.Text);

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "visibleDiv(true);", true);
                    CargaInfoReparaciones();
                    Limpiar();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('" + resp + "');", true);                    
                    Limpiar();
                }
            }            
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('" + ex.Message + "');", true);            
        }
    }

    protected void grdReparaciones_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("BorrarReg"))
            {
                GridViewRow item = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int indice = item.RowIndex;
                string IdReparacion = grdReparaciones.DataKeys[indice].Value.ToString();

                string respuesta = OrderDetailDA.EliminarReparacion(Convert.ToInt32(IdReparacion));

                if (respuesta.Equals("OK"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "visibleDiv(true);", true);
                    CargaInfoReparaciones();
                    Limpiar();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('" + respuesta + "');", true);
                }                                             
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('" + ex.Message + "');", true);
        }
    }

    protected void grdReparaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DataTable dt = Session["DtReparaciones"] as DataTable;

        if (dt != null)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "mostrarLoader();", true);
            grdReparaciones.PageIndex = e.NewPageIndex;
            grdReparaciones.DataSource = dt;
            grdReparaciones.DataBind();            
        }
    }

    protected void grdCortes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DataTable dt = Session["DtCortes"] as DataTable;

        if (dt != null)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "mostrarLoader2();", true);
            grdCortes.PageIndex = e.NewPageIndex;
            grdCortes.DataSource = dt;
            grdCortes.DataBind();            
        }
    }
    
    protected void grdDefectos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DataTable dt = Session["DtDefectos"] as DataTable;

        if (dt != null)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "mostrarLoader3();", true);
            grdDefectos.PageIndex = e.NewPageIndex;
            grdDefectos.DataSource = dt;
            grdDefectos.DataBind();            
        }
    }
    
    // Modelos.
    public class pOrderClass
    {
        public int idorder { get; set; }
        public string porder { get; set; }
        public string style { get; set; }
    }

    public class TallaClass
    {
        public string size { get; set; }
        public int unidades { get; set; }
    }
}