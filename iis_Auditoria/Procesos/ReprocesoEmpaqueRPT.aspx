﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReprocesoEmpaqueRPT.aspx.cs" Inherits="Procesos_ReprocesoEmpaqueRPT" %>
<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <script lang="javascript" type="text/javascript">        

        $(document).ready(function () {

            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });

        $(document).ready(function () {

            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });

    </script>

    <style>
        .mar {
            margin-bottom: 10px;
        }
    </style>

    <style>
        .m {
            margin-top: 25px;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
     <div class="container">
         <div class="panel panel-primary">
             <div class="panel-heading">Reporte Segundas</div>
             <div class="panel-body">
                 <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control  mar"></asp:SiteMapPath>

                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="panel-default">
                                <div class="panel-heading">Select </div>
                                <div class="panel-body">
                                    <asp:Panel runat="server" ID="panelfechas">
                                        Start Date
                                    <dx:ASPxDateEdit runat="server" ID="txtfecha1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx:ASPxDateEdit>
                                        End Date
                                    <dx:ASPxDateEdit runat="server" ID="txtfecha2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx:ASPxDateEdit>
                                    </asp:Panel>
                                    <div style="padding-top: 15px">
                                        <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" runat="server" OnClick="btngenerar_Click" />
                                    </div>
                                    <div style="padding-top: 15px">
                                        <asp:Button Text="Limpiar Campos" CssClass="btn btn-info form-control" ID="btnlimpiar" runat="server" OnClick="btnlimpiar_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <hr />
                        <dx:ASPxGridView ID="grdmiDD" 
                            runat="server" 
                            Width="100%" 
                            Theme="Metropolis" 
                            OnDataBinding="grdmiDD_DataBinding" 
                            OnCustomCallback="grdmiDD_CustomCallback"
                            Settings-HorizontalScrollBarMode="Auto" 
                            SettingsBehavior-AllowSort="false"
                            Styles-Cell-CssClass="textAlignLeft" 
                            ClientInstanceName="grid">
                            <SettingsBehavior AllowEllipsisInText="true" />
                            <SettingsResizing ColumnResizeMode="Control" />
                            <Settings ShowColumnHeaders="true" />
                        </dx:ASPxGridView>

                        <asp:Button ID="cmdExportDD" runat="server" CssClass="btn btn-success form-control m" Text="Export to Excel" OnClick="cmdExportDD_Click" />
                        <dx:ASPxGridViewExporter ID="exportDD" runat="server"></dx:ASPxGridViewExporter>

                    </div>
             </div>
         </div>
     </div>
</asp:Content>