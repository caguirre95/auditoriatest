﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HTEnvios.aspx.cs" Inherits="Procesos_HTEnvios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "HTEnvios.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    corte: item.corte,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.corte);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.corte);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.corte + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function alertMasterE(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }

        function limpiar() {

            $('#<%=grdHTEnvios.ClientID%>').empty();

        }
    </script>

    <script type="text/javascript">

        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function piezassobre() {
            swal({
                title: 'No puede mayor la cantidad a lo liberado !',
                type: 'warning'
            });
        }

    </script>

    <link href="../jquery-ui.css" rel="stylesheet" />
    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>

    <style>
        .margin {
            margin-top: 19px;
        }

        .mar_t {
            margin: 10px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <br />
    <div class="container">
        <br />
        <div class="panel panel-primary">
            <div class="panel-heading"><span style="font-weight: bold">Accesorio Heat Transfer</span></div>
            <div class="panel-body">

                <hr />

                <asp:GridView ID="grdHTEnvios" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" CssClass="table table-hover table-striped w" AutoGenerateColumns="false" GridLines="None" OnDataBinding="grdHTEnvios_DataBinding">
                    <Columns>
                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                            <HeaderTemplate>
                                Corte 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblcorte" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("corte") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField Visible="true">
                            <HeaderTemplate>
                                Quantity Cut
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblquantity" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("quantity") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                            <HeaderTemplate>
                                Estilo
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblestilo" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("estilo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField Visible="true">
                            <HeaderTemplate>
                                Unidades D/E
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblcantidadA" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("UnidadesAEnviar") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Piezas HT
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtunidadesEnv" Width="100px" Font-Size="Medium" onkeypress="return num(event);" MaxLength="5" CssClass="form-control" Text='' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Deficit
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtdef" Enabled="false" Width="100px" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("deficit") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton Text="" ID="lnkRecibir" OnClick="lnkRecibir_Click" CssClass="btn btn-info" runat="server"><i class="glyphicon glyphicon-check"> </i> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Irregulares Heat Transfer</div>
                            <div class="panel-body">                                
                                <div class="col-lg-12">
                                    <div class="col-lg-3" style="margin:0;padding:0">
                                        Corte
                                        <asp:TextBox ID="txtcorte" placeholder="digite el corte..." runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-3">
                                        Cantidad Irregulares
                                        <asp:TextBox ID="txtcant" placeholder="digite las irregulares..." MaxLength="3" onkeypress="return num(event);" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-4">
                                        Comentarios 
                                        <asp:TextBox ID="txtobser" placeholder="comentarios..." runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-2" style="margin:0;padding:0">
                                        <br />
                                        <asp:Button ID="btnguardar" runat="server" CssClass="btn btn-primary form-control" Text="Guardar" OnClick="btnguardar_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

