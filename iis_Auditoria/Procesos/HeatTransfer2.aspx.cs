﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Procesos_HeatTransfer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.User.Identity.IsAuthenticated == true)
            {
                CargarOperacion();
                CargarResponsable();
                CargarProduccion(0);
            }
            else
            {
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<pOrder> GetPOrder(string pre)
    {
        try
        {
            List<pOrder> list = new List<pOrder>();

            DataTable dt = new DataTable();
            string sql = string.Format(@"SELECT TOP 10 p.Id_Order, p.POrder, s.Style 
                                            FROM POrder p 
                                            INNER JOIN Style s ON p.Id_Style = s.Id_Style 
                                            WHERE p.POrder like '%{0}%'
                                            ORDER BY len(p.POrder)", pre);

            dt = DataAccess.Get_DataTable(sql);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                pOrder obj = new pOrder
                {
                    idorder = int.Parse(dt.Rows[i][0].ToString()),
                    porder = dt.Rows[i][1].ToString(),
                    style = dt.Rows[i][2].ToString()
                };

                list.Add(obj);
            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Colaboradores> GetColaboradores(string pre)
    {
        try
        {
            List<Colaboradores> list = new List<Colaboradores>();

            DataTable dt = new DataTable();
            string sql = string.Format(@"SELECT r.IdResponsable IdResponsable, r.Nombre Responsable,
	                                        c.IdColaborador, c.Nombre Colaborador
                                            FROM HT.Responsable r 
                                            INNER JOIN HT.Colaborador c ON c.IdResponsable = r.IdResponsable 
                                            WHERE c.Nombre LIKE '%{0}%' 
                                            ORDER BY 4", pre);

            dt = DataAccess.Get_DataTable(sql);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Colaboradores obj = new Colaboradores
                {
                    idresponsable = int.Parse(dt.Rows[i][0].ToString()),
                    responsable = dt.Rows[i][1].ToString(),
                    idcolaborador = int.Parse(dt.Rows[i][2].ToString()),
                    colaborador = dt.Rows[i][3].ToString()
                };

                list.Add(obj);
            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    private void CargarOperacion()
    {
        DataTable dt;
        string sql = "SELECT IdOperacion, Descripcion FROM HT.Operacion";

        dt = DataAccess.Get_DataTable(sql);

        ddlOperacion.DataSource = dt;
        ddlOperacion.DataTextField = "Descripcion";
        ddlOperacion.DataValueField = "IdOperacion";
        ddlOperacion.DataBind();        
    }

    private void CargarResponsable()
    {
        DataTable dt;
        string sql = "SELECT IdResponsable, Nombre FROM HT.Responsable";

        dt = DataAccess.Get_DataTable(sql);

        ddlResponsable.DataSource = dt;
        ddlResponsable.DataTextField = "Nombre";
        ddlResponsable.DataValueField = "IdResponsable";
        ddlResponsable.DataBind();
    }

    private void CargarProduccion(int idOp)
    {
        try
        {
            // Dealle de producción            
            int total = 0;
            DataTable dt = DataAccess.Get_DataTable(string.Format("EXEC HT.spCargarProduccion {0}", idOp));

            for (int item = 0; item < dt.Rows.Count; item++)
            {
                total += int.Parse(dt.Rows[item][5].ToString());
            }

            grdProduccion.Columns[5].FooterText = string.Format("{0:n0}", total);
            grdProduccion.DataSource = dt;
            grdProduccion.DataBind();

            // Sumatoria total por cortes.            
            string filtro = idOp == 0 ? string.Empty : "AND IdOperacion = " + idOp.ToString();
            string query2 = string.Format(@"SELECT IdCorte, SUM(Unidades) Unidades 
                                            FROM HT.Produccion 
                                            WHERE CAST(FechaProduccion AS DATE) = CAST(GETDATE() AS DATE)
                                            {0} GROUP BY IdCorte 
                                            ORDER BY IdCorte", filtro);

            DataTable dt2 = DataAccess.Get_DataTable(query2);
            total = 0;
            for (int item = 0; item < dt2.Rows.Count; item++)
            {
                total += int.Parse(dt2.Rows[item][1].ToString());
            }

            grdCortes.Columns[1].FooterText = string.Format("{0:n0}", total);
            grdCortes.DataSource = dt2;
            grdCortes.DataBind();

            // Sumatoria total por Bihorarios
            int idOp2 = 0;
            if (idOp == 0)
            {
                idOp = 1;
                idOp2 = 2;
            }
            DataTable dt3 = DataAccess.Get_DataTable(string.Format("SELECT * FROM HT.fnCargarProduccionXBihorario({0},{1})", idOp, idOp2));
            total = 0;
            for (int item = 0; item < dt3.Rows.Count; item++)
            {
                total += int.Parse(dt3.Rows[item][2].ToString());
            }

            grdBihorario.Columns[1].FooterText = string.Format("{0:n0}", total);
            grdBihorario.DataSource = dt3;
            grdBihorario.DataBind();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertError('" + ex.Message + "');", true);
        }
    }

    private void Limpiar()
    {
        txtCorte.Text = string.Empty;
        txtEstilo.Text = string.Empty;
        txtIdOrder.Value = string.Empty;
        txtIdResponsable.Value = string.Empty;
        txtIdColaborador.Value = string.Empty;
        txtColaborador.Text = string.Empty;
    }

    protected void ddlOperacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarProduccion(int.Parse(ddlOperacion.SelectedValue));
        if (ddlOperacion.SelectedIndex > 0)
        {
            txtColaborador.Focus();
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlOperacion.SelectedIndex > 0 && !string.IsNullOrEmpty(txtIdColaborador.Value) && !string.IsNullOrEmpty(txtIdOrder.Value))
            {
                Produccion model = new Produccion
                {
                    IdOperacion = int.Parse(ddlOperacion.SelectedValue),
                    IdColaborador = int.Parse(txtIdColaborador.Value),
                    IdResponsable = int.Parse(txtIdResponsable.Value),
                    IdOrder = int.Parse(txtIdOrder.Value),
                    IdCorte = txtCorte.Text,
                    IdStyle = txtStyle.Value,
                    Unidades = int.Parse(txtUnidades.Text),
                    Usuario = Context.User.Identity.Name
                };

                string resp = OrderDetailDA.GuardarProduccionDelDia(model);

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                    Limpiar();
                    CargarProduccion(int.Parse(ddlOperacion.SelectedValue));
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('" + resp.Replace("'", "") + "');", true);
                }
            }
            else
            {
                if (ddlOperacion.SelectedIndex == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('Seleccione una operación');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('Hay campos vacíos, revise por favor...');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertError('" + ex.Message + "');", true);            
        }
    }

    protected void grdProduccion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdProduccion.PageIndex = e.NewPageIndex;
        //grdProduccion.DataBind();
        CargarProduccion(int.Parse(ddlOperacion.SelectedValue));
    }

    protected void grdProduccion_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("BorrarReg"))
            {
                GridViewRow item = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int indice = item.RowIndex;
                string IdReparacion = grdProduccion.DataKeys[indice].Value.ToString();

                string respuesta = OrderDetailDA.EliminarLineaProduccion(int.Parse(IdReparacion));

                if (respuesta.Equals("OK"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                    CargarProduccion(int.Parse(ddlOperacion.SelectedValue));
                    Limpiar();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('" + respuesta + "');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertError('" + ex.Message + "');", true);
        }
    }

    protected void btnGuardarModal_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtAdColaborador.Text))
            {
                string resp = OrderDetailDA.CrearColaborador(int.Parse(ddlResponsable.SelectedValue), txtAdColaborador.Text);

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "ocultarModal();", true);
                    txtAdColaborador.Text = string.Empty;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('" + resp.Replace("'", "") + "');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertPlan('Escriba el nombre del colaborador');", true);
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertError('" + ex.Message + "');", true);
        }
    }
}