﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Procesos_ReprocesoPlanchaRec : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.User.Identity.IsAuthenticated == true)
            {

            }
            else
            {
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select Top 15 POrderClient from POrder p join Style s on s.Id_Style = p.Id_Style where POrderClient like '%" + pre + "%' and s.washed = 1 group by POrderClient", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porderclient = dt.Rows[i][0].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porderclient { get; set; }
    }

    void reprocesoplancha()
    {
        try
        {
            DataTable dt;

            string query = " select a.POrderClient, a.Style, ISNULL(a.WIS,0) WIS, ISNULL(a.Inseam,0) Inseam, ISNULL(a.Waist,0) Waist, (isnull(a.PiezasTotales,0) - ISNULL(b.TotalPiezas,0)) PiezasTotales from ("
                         + " SELECT POrderClient, Style, ISNULL(WIS, 0) WIS, ISNULL(InseamS, 0) Inseam, ISNULL(WaistS, 0) Waist, (ISNULL(WIS, 0) + ISNULL(InseamS, 0) + ISNULL(WaistS, 0)) PiezasTotales"
                         + " FROM (SELECT c1.POrderClient, Style, CASE WHEN isnull(c2.SmallW, 0) = 1 AND isnull(c3.SmallI, 0) = 1 THEN 'WIS' WHEN isnull(c2.SmallW, 0) = 0 AND isnull(c3.SmallI, 0) = 1 THEN 'InseamS'"
                         + " WHEN isnull(c2.SmallW, 0) = 1 AND isnull(c3.SmallI, 0) = 0 THEN 'WaistS' END AS Medida, SUM(c1.unidades) AS unidades FROM (SELECT p.POrderClient, s.Style, dl.idPorder, dl.id, 1 AS unidades"
                         + " FROM dbo.tbMedidasIntexDL AS dl INNER JOIN dbo.POrder AS p ON dl.idPorder = p.Id_Order INNER JOIN dbo.tbMedidasIntexP AS mi ON dl.id = mi.id inner join dbo.Style AS s ON s.Id_Style = p.Id_Style"
                         + " WHERE(dl.estado = 0) AND(mi.estado = 'small') GROUP BY p.POrderClient, s.Style, dl.idPorder, dl.id) AS c1 LEFT OUTER JOIN (SELECT dl.id, 1 AS SmallW FROM dbo.tbMedidasIntexDL AS dl"
                         + " INNER JOIN dbo.tbMedidasIntexP AS mi ON dl.id = mi.id WHERE(dl.estado = 0) AND(mi.estado = 'small') AND(mi.idPuntoMedida = 1)) AS c2 ON c1.id = c2.id LEFT OUTER JOIN (SELECT dl.id, 1 AS SmallI"
                         + " FROM dbo.tbMedidasIntexDL AS dl INNER JOIN dbo.tbMedidasIntexP AS mi ON dl.id = mi.id WHERE(dl.estado = 0) AND(mi.estado = 'small') AND(mi.idPuntoMedida IN(2, 300, 301, 302))) AS c3 ON c1.id = c3.id"
                         + " where c1.POrderClient = '" + txtcorte.Text + "' GROUP BY c1.POrderClient, c1.Style, CASE WHEN isnull(c2.SmallW, 0) = 1 AND isnull(c3.SmallI, 0) = 1 THEN 'WIS' WHEN isnull(c2.SmallW, 0) = 0 AND isnull(c3.SmallI, 0) = 1"
                         + " THEN 'InseamS' WHEN isnull(c2.SmallW, 0) = 1 AND isnull(c3.SmallI, 0) = 0 THEN 'WaistS' END) AS SourceTable PIVOT(SUM(unidades) FOR Medida IN([WIS], [InseamS], [WaistS])) AS PivotTable"
                         + " ) a left join (select rp.corte, ISNULL(SUM(drp.unidadesRecibidas),0) TotalPiezas from reprocesoPlancha rp join detailReprocesoPlancha drp on drp.idObjectReprocesoPlancha = rp.objectIdReprocesoPlancha"
                         + " where rp.corte = '" + txtcorte.Text + "' and drp.estado = 0 group by rp.corte) b on a.POrderClient = b.corte";

            dt = DataAccess.Get_DataTable(query);

            if (dt.Rows.Count > 0)
            {
                DataTable dt1;

                string query1 = " SELECT POrderClient, Style, ISNULL(mt1, '---') WAIST1, ISNULL(mt2, '---') INSEAM1, ISNULL(WIS, 0) WIS, ISNULL(WaistS, 0) Waist, ISNULL(InseamS, 0) Inseam, (ISNULL(WIS, 0) + ISNULL(InseamS, 0) + ISNULL(WaistS, 0)) PiezasTotales"
                              + " FROM (SELECT c1.POrderClient, Style, c2.medidaTexto mt1, c3.medidaTexto mt2, CASE WHEN isnull(c2.SmallW, 0) = 1 AND isnull(c3.SmallI, 0) = 1 THEN 'WIS' WHEN isnull(c2.SmallW, 0) = 0 AND isnull(c3.SmallI, 0) = 1 THEN 'InseamS'"
                              + " WHEN isnull(c2.SmallW, 0) = 1 AND isnull(c3.SmallI, 0) = 0 THEN 'WaistS' END AS Medida, SUM(c1.unidades) AS unidades FROM (SELECT p.POrderClient, s.Style, dl.idPorder, dl.id, 1 AS unidades FROM dbo.tbMedidasIntexDL AS dl"
                              + " INNER JOIN dbo.POrder AS p ON dl.idPorder = p.Id_Order INNER JOIN dbo.tbMedidasIntexP AS mi ON dl.id = mi.id inner join dbo.Style AS s ON s.Id_Style = p.Id_Style WHERE(dl.estado = 0) AND(mi.estado = 'small')"
                              + " GROUP BY p.POrderClient, s.Style, dl.idPorder, dl.id) AS c1 LEFT OUTER JOIN (SELECT mi.medidaTexto, dl.id, 1 AS SmallW FROM dbo.tbMedidasIntexDL AS dl INNER JOIN dbo.tbMedidasIntexP AS mi ON dl.id = mi.id"
                              + " WHERE(dl.estado = 0) AND(mi.estado = 'small') AND(mi.idPuntoMedida = 1)) AS c2 ON c1.id = c2.id LEFT OUTER JOIN (SELECT mi.medidaTexto, dl.id, 1 AS SmallI FROM dbo.tbMedidasIntexDL AS dl INNER JOIN dbo.tbMedidasIntexP AS mi ON dl.id = mi.id"
                              + " WHERE(dl.estado = 0) AND(mi.estado = 'small') AND(mi.idPuntoMedida IN(2, 300, 301, 302))) AS c3 ON c1.id = c3.id where c1.POrderClient = '" + txtcorte.Text + "' GROUP BY c1.POrderClient, c1.Style, c2.medidaTexto, c3.medidaTexto,"
                              + " CASE WHEN isnull(c2.SmallW, 0) = 1 AND isnull(c3.SmallI, 0) = 1 THEN 'WIS' WHEN isnull(c2.SmallW, 0) = 0 AND isnull(c3.SmallI, 0) = 1 THEN 'InseamS' WHEN isnull(c2.SmallW, 0) = 1 AND isnull(c3.SmallI, 0) = 0 THEN 'WaistS' END"
                              + " ) AS SourceTable PIVOT(SUM(unidades) FOR Medida IN([WIS], [InseamS], [WaistS])) AS PivotTable";


                dt1 = DataAccess.Get_DataTable(query1);

                grdinfo.DataSource = dt1;
                grdinfo.DataBind();
            }

            grdRepro.DataSource = dt;
            grdRepro.DataBind();
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            reprocesoplancha();
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void lnkRecibir_Click(object sender, EventArgs e)
    {
        try
        {
            int cont = 0;
            int cont2 = 0;

            if (txtcorte.Text != string.Empty)
            {
                foreach (GridViewRow item in grdRepro.Rows)
                {
                    string resp = "0";
                    string resp1 = "0";
                    var corte = (Label)item.FindControl("lblcorte");
                    var estilo = (Label)item.FindControl("lblestilo");
                    var waist = (Label)item.FindControl("lblpuntoW");
                    var inseam = (Label)item.FindControl("lblinseam");
                    var waIns = (Label)item.FindControl("lblwis");
                    var unidades = (TextBox)item.FindControl("txtunidades");
                    var uniRecibidas = (TextBox)item.FindControl("txtunidadesrec");

                    if (uniRecibidas.Text != string.Empty)
                    {
                        if (Convert.ToInt16(unidades.Text) >= Convert.ToInt16(uniRecibidas.Text))
                        {
                            DataTable dtr;
                            string consulta = "select corte from reprocesoPlancha where corte = " + corte.Text;

                            dtr = DataAccess.Get_DataTable(consulta);

                            if (dtr.Rows.Count > 0)
                            {
                                resp1 = OrderDetailDA.ReprocesoPlanchaRecibidoDet(corte.Text, int.Parse(uniRecibidas.Text), DateTime.Now, Context.User.Identity.Name, Convert.ToBoolean(0));

                                if (resp1.Equals("OK"))
                                {
                                    cont++;
                                    cont2++;
                                }
                            }
                            else
                            {
                                resp = OrderDetailDA.ReprocesoPlanchaRecibido(corte.Text, estilo.Text, Convert.ToBoolean(0), DateTime.Now, Context.User.Identity.Name);

                                if (int.Parse(resp) > 0)
                                {
                                    resp1 = OrderDetailDA.ReprocesoPlanchaRecibidoDet1(int.Parse(resp), int.Parse(uniRecibidas.Text), DateTime.Now, Context.User.Identity.Name, Convert.ToBoolean(0));

                                    if (resp1.Equals("OK"))
                                    {
                                        cont++;
                                        cont2++;
                                    }
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "piezassobre();", true);
                        }
                    }
                }

                if (cont == cont2)
                {
                    reprocesoplancha();
                    if (cont2 > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
                }
            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }
}