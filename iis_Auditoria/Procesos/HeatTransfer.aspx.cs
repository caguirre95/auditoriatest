﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Procesos_HeatTransfer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.User.Identity.IsAuthenticated == true)
            {
                Session["HT"] = null;
            }
            else
            {
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select top 15 p.POrderClient, s.Style, sum(p.Quantity) QuantityCorte from POrder p join Style s on s.Id_Style = p.Id_Style"
                                          + " join estiloAccesorio ea on ea.idEstilo = s.Id_Style where p.POrderClient like '%" + pre + "%' group by p.POrderClient, s.Style", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porderclient = dt.Rows[i][0].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porderclient { get; set; }
    }

    void HT()
    {
        try
        {
            DataTable dt;

            string query = " select a.POrderClient, a.Style, a.QuantityCorte, (ISNULL(b.PiezasLiberadas, 0) - ISNULL(c.Recibidas, 0)) Liberadas, (a.QuantityCorte - ISNULL(c.Recibidas, 0)) deficit from"
                         + " (select p.POrderClient, s.Style, sum(p.Quantity) QuantityCorte from POrder p join Style s on s.Id_Style = p.Id_Style join estiloAccesorio ea on ea.idEstilo = s.Id_Style"
                         + " group by p.POrderClient, s.Style) a join (select p.POrderClient, ISNULL(COUNT(mi.cantidad),0) PiezasLiberadas from POrder p join tbMedidasIntexDL mi on mi.idPorder = p.Id_Order"
                         + " group by p.POrderClient) b on a.POrderClient = b.POrderClient left join (select corte, ISNULL(SUM(cantidad),0) Recibidas, estado from accesorioHT ah"
                         + " join accesorioHTDetail ad on ad.idObjectHT = ah.idObjectHT group by corte, estado) c on a.POrderClient = c.corte where a.POrderClient = '" + txtcorte.Text + "'";

            dt = DataAccess.Get_DataTable(query);
            Session["HT"] = dt;
            grdHT.DataSource = Session["HT"];
            grdHT.DataBind();

        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            HT();
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void lnkRecibir_Click(object sender, EventArgs e)
    {
        try
        {
            int cont = 0;
            int cont2 = 0;

            if (txtcorte.Text != string.Empty)
            {
                foreach (GridViewRow item in grdHT.Rows)
                {
                    string resp = "0";
                    string resp1 = "0";
                    var corte = (Label)item.FindControl("lblcorte");
                    var estilo = (Label)item.FindControl("lblestilo");
                    var cantcor = (Label)item.FindControl("lblcantidadc");
                    var piezas = (Label)item.FindControl("lblpiezas");
                    var unidadesRec = (TextBox)item.FindControl("txtunidadesrec");
                    var def = (TextBox)item.FindControl("txtdef");

                    if (unidadesRec.Text != string.Empty && Convert.ToInt16(piezas.Text) >= Convert.ToInt16(unidadesRec.Text))
                    {
                        DataTable dtr;
                        string consulta = "select corte from accesorioHT where corte = " + corte.Text;

                        dtr = DataAccess.Get_DataTable(consulta);

                        if (dtr.Rows.Count > 0)
                        {
                            resp1 = OrderDetailDA.RecibidoDetailHT2(corte.Text, int.Parse(unidadesRec.Text), DateTime.Now, Context.User.Identity.Name, Convert.ToBoolean(1));

                            if (resp1.Equals("OK"))
                            {
                                cont++;
                                cont2++;
                            }
                        }
                        else
                        {
                            resp = OrderDetailDA.RecibidoHT(corte.Text, estilo.Text, int.Parse(cantcor.Text), DateTime.Now, Context.User.Identity.Name, Convert.ToBoolean(0));

                            if (int.Parse(resp) > 0)
                            {
                                cont2++;

                                resp1 = OrderDetailDA.RecibidoDetailHT(int.Parse(resp), int.Parse(unidadesRec.Text), DateTime.Now, Context.User.Identity.Name, Convert.ToBoolean(1));

                                if (resp1.Equals("OK"))
                                {
                                    cont++;
                                }
                            }
                        }                  
                    }
                    else
                    {
                        //Mensaje     
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "piezassobre();", true);
                    }
                }

                if (cont == cont2)
                {
                    HT();
                    if (cont2 > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                    }

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
                }
            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void grdHT_DataBinding(object sender, EventArgs e)
    {
        grdHT.DataSource = Session["HT"];
    }
}