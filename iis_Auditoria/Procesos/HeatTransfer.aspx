﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HeatTransfer.aspx.cs" Inherits="Procesos_HeatTransfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "HeatTransfer.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porderclient: item.porderclient,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porderclient);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porderclient);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porderclient + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function alertMasterE(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }

        function limpiar() {

            $('#<%=grdHT.ClientID%>').empty();

            $('#<%=txtcorte.ClientID%>').val('');

        }
    </script>

    <script type="text/javascript">
        
        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function piezassobre() {
            swal({
                title: 'No puede mayor la cantidad a lo liberado !',
                type: 'warning'
            });
        }
        
    </script>

    <link href="../jquery-ui.css" rel="stylesheet" />
    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>

    <style>
        .margin {
            margin-top: 19px;
        }

        .mar_t {
            margin: 10px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">
        <br />
        <div class="panel panel-primary">
            <div class="panel-heading"><span style="font-weight: bold">Accesorio Heat Transfer</span></div>
            <div class="panel-body">

                <div class="row">

                    <div class="col-lg-6">
                        Ingreso del Corte
                        <asp:TextBox ID="txtcorte" runat="server" required="true" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Codigo Order" onfocus="limpiar()"></asp:TextBox>
                        <asp:HiddenField ID="idcorte" runat="server" />
                    </div>
                    <div class="col-lg-6">
                        <asp:Button ID="btnbuscar" runat="server" Text="Search" CssClass="btn btn-primary form-control margin" OnClick="btnbuscar_Click" />
                    </div>

                </div>

                <hr />

                <asp:GridView ID="grdHT" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" CssClass="table table-hover table-striped w" AutoGenerateColumns="false" GridLines="None" OnDataBinding="grdHT_DataBinding">
                    <Columns>
                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                            <HeaderTemplate>
                                Corte 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblcorte" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("POrderClient") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                            <HeaderTemplate>
                                Estilo
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblestilo" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("Style") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField Visible="true">
                            <HeaderTemplate>
                                Cantidad Corte 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblcantidadc" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("QuantityCorte") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                            <HeaderTemplate>
                                Piezas Liberadas
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblpiezas" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Liberadas") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Cantidad Recibida
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtunidadesrec" Width="100px" Font-Size="Medium" onkeypress="return num(event);" MaxLength="5" CssClass="form-control" Text='' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Deficit
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtdef" Enabled="false" Width="100px" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("deficit") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>                        

                        <asp:TemplateField>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton Text="" ID="lnkRecibir" OnClick="lnkRecibir_Click" CssClass="btn btn-info" runat="server"><i class="glyphicon glyphicon-check"> </i> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

                <div class="row">
                    <hr />
                    <div class="col-lg-12">
                        <%--<asp:Button ID="btntodos" runat="server" Text="Guardar Todos" CssClass="btn btn-primary form-control" OnClick="lnkRecibir_Click" />--%>
                    </div>
                </div>
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

