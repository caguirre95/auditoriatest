﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Procesos_ReprocesoEmpaqueRPT : System.Web.UI.Page
{

    protected void grdmiDD_DataBinding(object sender, EventArgs e)
    {
        grdmiDD.DataSource = Session["tblSegundas_DD"];
    }


    protected void grdmiDD_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdmiDD.DataBind();
    }

    protected void cmdExportDD_Click(object sender, EventArgs e)
    {
        try
        {
            exportDD.GridViewID = "grdmiDD";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = exportDD;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link3 });
            compositeLink.CreatePageForEachLink();
            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Segundas Reportadas";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=SegundasReportadas-" + DateTime.Now.ToShortDateString() + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {}
    }
    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            var consultaDD = new DataTable();
            string sqlquery = "EXEC dbo.rpt_segundas @desde='" + txtfecha1.Text + "', @hasta='" + txtfecha2.Text + "';";
            consultaDD = DataAccess.Get_DataTable(sqlquery);
            Session["tblSegundas_DD"] = consultaDD;
            grdmiDD.DataBind();
        }
        catch (Exception)
        {
        }
    }


    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Procesos/ReprocesoEmpaqueRPT.aspx");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["tblSegundas_DD"] = null;
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();
        }
    }
}