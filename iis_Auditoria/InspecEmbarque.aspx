﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InspecEmbarque.aspx.cs" Inherits="InspecEmbarque" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function alertSeleccionarMedida(mess, tipo) {
            swal({
                title: "",
                text: mess,
                type: tipo
            });
        }
        function successalert() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
    </script>
    <script type="text/javascript">
        function warningalert() {
            swal({
                title: "¿Estas seguro?",
                text: "¿Deseas ingresar este registro?",
                type: "warning"
            })
        }
    </script>



    <link href="bootstrap-fileinput-master/css/fileinput.css" rel="stylesheet" />
    <link href="bootstrap-fileinput-master/css/fileinput.min.css" rel="stylesheet" />
    <script src="bootstrap-fileinput-master/js/fileinput.js"></script>
    <script src="bootstrap-fileinput-master/js/fileinput.min.js"></script>

    <link rel="stylesheet" href="datetimepicker/css/bootstrap-material-datetimepicker.css" />
    <script type="text/javascript" src="datetimepicker/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="datetimepicker/js/bootstrap-material-datetimepicker.js"></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <div style="margin-left: 10px;">

        <label id="frprododr">
            <asp:Label ID="lblorder1" Visible="false" runat="server" Text=""></asp:Label></label>

        <label id="toprododr">
            <asp:Label ID="lblorder2" runat="server" Visible="false" Text=""></asp:Label></label>

        <br />

    </div>

    <div class="container-fluid">
        <div class="col-lg-12 centrar">
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/img/system_search.png" OnClick="ImageButton2_Click" /><asp:LinkButton ID="lnkNew" runat="server" OnClick="lnkNew_Click">New Search</asp:LinkButton>

        </div>
    </div>

    <div class="container " style="padding-bottom: 5px;">
        <div class="col-lg-2">
            <strong>Customer:</strong>
            <asp:Label ID="lbclient" runat="server" CssClass="main">
                <asp:Label ID="lblcustomer" runat="server" Text=""></asp:Label></asp:Label>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container-fluid info_ord" style="margin-bottom: 0 !important">
        <div class="container" style="padding-bottom: 5px;">
            <div class="col-lg-2 col-lg-offset-1">
                <asp:Label ID="Label10" runat="server" Text="Corte#:"></asp:Label>
                <asp:TextBox ID="txtporder" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label28" runat="server" Text="Estilo"></asp:Label>
                <asp:TextBox ID="txtstyle" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label29" runat="server" Text="Descripcion"></asp:Label>
                <asp:TextBox ID="txtdescrip" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label30" runat="server" Text="Cantidad"></asp:Label>
                <asp:TextBox ID="txtcantidad" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label7" runat="server" Text="Color"></asp:Label>
                <asp:TextBox ID="txtcolor" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
        </div>
    </div>

    <div class="container-fluid info_ord">
        <div class="container " style="padding-bottom: 5px;">
            <div class="col-lg-2 col-lg-offset-1">
                <asp:Label ID="Label2" runat="server" Text="Muestra:"></asp:Label>
                <asp:TextBox ID="txtmuestra" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label3" runat="server" Text="Acepta"></asp:Label>
                <asp:TextBox ID="txtacepta" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label4" runat="server" Text="Rechaza"></asp:Label>
                <asp:TextBox ID="txtrechaza" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label11" runat="server" Text="Codigo"></asp:Label>
                <asp:TextBox ID="txtcodigo" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label12" runat="server" Text="Auditor"></asp:Label>
                <asp:TextBox ID="txtaudit" Font-Size="Small" runat="server" CssClass="form-control" Enabled="true" MaxLength="25"></asp:TextBox>
            </div>
        </div>
    </div>

    <div class="container" style="padding-bottom: 5px;">
        <div class="panel panel-default">
            <div class="panel-heading">Ingreso de Datos</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-2 ">
                        <asp:Label ID="Label5" runat="server" Text="Nivel:"></asp:Label>
                        <asp:DropDownList ID="drpniveles" runat="server" CssClass="form-control" OnSelectedIndexChanged="drpniveles_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="col-lg-2">
                        <asp:Label ID="Label6" runat="server" Text="Nivel AQL:"></asp:Label>
                        <asp:DropDownList ID="drpnivelaql" runat="server" CssClass="form-control" OnSelectedIndexChanged="drpnivelaql_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="col-lg-2">
                        <asp:Label ID="Label1" runat="server" Text="Lote:"></asp:Label>
                        <asp:TextBox ID="txtlote" Font-Size="Small" runat="server" OnTextChanged="txtlote_TextChanged" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                    </div>

                    <div class="col-lg-2">
                        <asp:Label ID="Label31" runat="server" Text="Piezas Defectuosas"></asp:Label>
                        <asp:TextBox ID="txtpiezasdefect" Font-Size="Small" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-lg-2">

                        <asp:Label ID="Label8" runat="server" Text="Hora Inicio"></asp:Label>
                        <asp:TextBox ID="txthoraI" autocomplete="off" AutoCompleteType="None" Font-Size="Small" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-2">
                        <asp:Label ID="label9" runat="server" Text="Hora Final"></asp:Label>
                        <asp:TextBox ID="txthoraF" autocomplete="off" AutoCompleteType="None" Font-Size="Small" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>


                <div class="col-lg-8 form-group">
                    <label>Comentario</label>
                    <asp:TextBox ID="txtcomentario" onfocus="limpiar(this);" CssClass="form-control" onkeypress="return num(event);" runat="server"></asp:TextBox>
                </div>

                <div class="col-lg-4 form-group">
                    <label>Deficit Corte</label>
                    <asp:TextBox ID="txtdef" CssClass="form-control" runat="server" Font-Size="Small" Enabled="false"></asp:TextBox>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="panel panel-info " style="margin: 0">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body form-inline">

                                <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success btn-block " Text="Guardar" OnClick="btnGuardar_Click" />
                                <a class="btn btn-info btn-block" data-toggle="modal" data-target="#myModal">Visualizar</a>
                                 <a href="#ventanaG" data-toggle="modal" style="margin-top:5px" class="btn btn-warning btn-block"><span class="glyphicon glyphicon-align-justify"></span> Ver Defectos</a>
                                <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-danger btn-block" Text="Limpiar" OnClick="btnlimpiar_Click" />
                            </div>
                        </div>

                        <%--<div class="panel panel-default">
                            <div class="panel-heading ">
                                <h1>Subir Imagenes
                                </h1>
                            </div>
                            <div class="panel-body">
                                <%--<asp:FileUpload ID="FileUpload1" runat="server" name="input2[]" class="file" multiple="multiple" data-show-upload="false" data-show-caption="true" />--%>

                                <%--<div runat="server" id="ms">
                                </div>
                            </div>
                        </div>--%>
                    </div>
                    <div class="col-lg-9"> <%--table table-hover table-striped--%>
                        <div id="Grid" runat="server">
                            <asp:GridView ID="gridAddDefectos" runat="server" CssClass="table" GridLines="None" Width="100%" AutoGenerateColumns="False"
                                OnRowDataBound="gridAddDefectos_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Codigo Defecto" HeaderStyle-Width="250px">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddldefectos" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Codigo Operacion" HeaderStyle-Width="250px">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlposicion" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Width="15px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtcantidaddef" CssClass="form-control" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Subir Imagenes">
                                        <ItemTemplate>
                                            <asp:FileUpload ID="FileUpload2" runat="server" name="input2[]" class="file" multiple="multiple" data-show-upload="false" data-show-caption="true" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblno" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />

                        </div>
                    </div>
                </div>
            </div>
    </div>
    </div>


    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Auditorias del Corte</h4>
                </div>
                <div class="modal-body">
                    <asp:GridView ID="GridViewAuditado" runat="server" CssClass="table table-hover table-striped" GridLines="None" Width="100%" AutoGenerateColumns="False"
                        OnRowCommand="GridViewAuditado_RowCommand" OnRowDataBound="GridViewAuditado_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Corte">
                                <ItemTemplate>
                                    <asp:Label ID="lblcorte" runat="server" Text='<%# Eval("POrder") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Codigo Lote">
                                <ItemTemplate>
                                    <asp:Label ID="lblcodigo" runat="server" Text='<%# Eval("codigoLote") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unidades Lote">
                                <ItemTemplate>
                                    <asp:Label ID="lblLote" runat="server" Text='<%# Eval("lote") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblno" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Estado" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblestado" runat="server" Text='<%# Eval("estadoAudit") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Auditorias" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblsec" runat="server" Text='<%# Eval("secuencia") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="btnReauditar" runat="server" Text="Reauditar" CommandName="Reauditar" CssClass="btn btn-default" />
                                </ItemTemplate>

                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <style>
                        .label-grid {
                            color: aliceblue !important;
                        }

                        .label-grid-inv {
                            color: #5c5c5c !important;
                        }
                    </style>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>   

    <aside class="modal fade"  id="ventanaG">
        <div class="modal-dialog">
            <div class="modal-content">
                <article class="modal-header">
                    <button id="cerrarG" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                    <h4>Defectos del Corte (Embarque) </h4>
                </article>
                <article class="modal-body">
                    <dx1:ASPxGridView ID="grddefecto" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grddefecto_DataBinding" OnCustomCallback="grddefecto_CustomCallback">
                            <SettingsSearchPanel Visible="true" />                            
                            <Settings VerticalScrollBarMode="Visible" />
                            <SettingsPager PageSize="100"></SettingsPager>
                        </dx1:ASPxGridView>
                    <%--<asp:GridView runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White"  CssClass="table  table-hover table-striped " ID="grddefecto">

                    </asp:GridView>--%>
                </article>
            </div>
        </div>
    </aside>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=txthoraI.ClientID%>').bootstrapMaterialDatePicker
                ({
                    date: false,
                    shortTime: false,
                    format: 'HH:mm'
                });

            $('#<%=txthoraF.ClientID%>').bootstrapMaterialDatePicker
                ({
                    date: false,
                    shortTime: false,
                    format: 'HH:mm'
                });

            $.material.init()
        });
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <h2>
        <asp:Label ID="lbl" runat="server" ForeColor="White" Text="Bundles Check Off"></asp:Label>
    </h2>
    <br />
</asp:Content>

