﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BultoaBultoInspFinal.aspx.cs" Inherits="BultoaBultoInspFinal" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v16.2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="scripts/footable.min.js"></script>

    <script type="text/javascript">
        function alertSeleccionarMedida(mess, tipo) {
            swal({
                title: "",
                text: mess,
                type: tipo
            });
        }
        function successalert() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
    </script>
    <script type="text/javascript">
        function warningalert() {
            swal({
                title: "¿Estas seguro?",
                text: "¿Deseas ingresar este registro?",
                type: "warning"
            })
        }
    </script>
    <script type="text/javascript">
        function pageLoad() { // this gets fired when the UpdatePanel.Update() completes
            ReBindMyStuff();
        }
        function ReBindMyStuff() { // create the rebinding logic in here
            $('[id*=Repeater]').footable();
        }


        function getUrlVars() {            
            var hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            hash = hashes[0].split('=');
            var idorder = hash[1];

            var porder = document.getElementById("<%= txtporder.ClientID %>").value;
            var style = document.getElementById("<%= txtstyle.ClientID %>").value;

            window.open("MedidaAL.aspx?idPorder=" + idorder + "&Porder=" + porder + "&Estilo=" + style, '_blank');
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <div style="margin-left: 10px;">

        <label id="frprododr">
            <asp:Label ID="lblorder1" Visible="false" runat="server" Text=""></asp:Label></label>

        <label id="toprododr">
            <asp:Label ID="lblorder2" runat="server" Visible="false" Text=""></asp:Label></label>

        <br />

    </div>

    <div class="container-fluid">
        <div class="col-lg-12 centrar">
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/img/system_search.png" OnClick="ImageButton2_Click" /><asp:LinkButton ID="lnkNew" runat="server" OnClick="lnkNew_Click">New Search</asp:LinkButton>
            | <a onclick="getUrlVars(this);" class="alert-link">Medidas</a>
        </div>
    </div>

    <div class="container " style="padding-bottom: 5px;">
        <div class="col-lg-2">
            <strong>Customer:</strong>
            <asp:Label ID="lbclient" runat="server" CssClass="main">
                <asp:Label ID="lblcustomer" runat="server" Text=""></asp:Label></asp:Label>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container-fluid info_ord">
        <div class="container " style="padding-bottom: 5px;">
            <div class="col-lg-2">
                <asp:Label ID="Label10" runat="server" Text="Corte#:"></asp:Label>
                <asp:TextBox ID="txtporder" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label28" runat="server" Text="Estilo"></asp:Label>
                <asp:TextBox ID="txtstyle" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label29" runat="server" Text="Descripcion"></asp:Label>
                <asp:TextBox ID="txtdescrip" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label30" runat="server" Text="Cantidad"></asp:Label>
                <asp:TextBox ID="txtcantidad" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label31" runat="server" Text="Bultos"></asp:Label>
                <asp:TextBox ID="txtbultos" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="lbl32" runat="server" Text="Deficit"></asp:Label>
                <asp:TextBox ID="txtdeficit" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-12">
                <a href="#ventanaG" data-toggle="modal" style="margin-top: 5px" class="btn btn-warning btn-block"><span class="glyphicon glyphicon-align-justify"></span>Ver Defectos</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="Grid" runat="server">
            <asp:GridView ID="GridBultos" OnRowCommand="GridBultos_RowCommand" OnRowDataBound="GridBultos_RowDataBound" AutoGenerateColumns="false" DataKeyNames="Id_Bundle"
                CssClass="table table-hover table-striped" GridLines="None" Width="100%" runat="server">
                <Columns>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblidbundle" runat="server" Text='<%#Bind("Id_Bundle") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="NSeq" HeaderText="Seq" />
                    <asp:BoundField DataField="Bld" HeaderText="Bulto" />
                    <asp:BoundField DataField="Size" HeaderText="Talla" />
                    <asp:BoundField DataField="Color" HeaderText="Color" />
                    <asp:BoundField DataField="Quantity" HeaderText="Cantidad" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                <asp:ListItem Value="0" Text="Rechazado"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Aprobado"></asp:ListItem>
                            </asp:RadioButtonList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Button ID="btnGuardarTodos" OnClick="btnGuardarTodos_Click" CssClass="btn btn-primary" runat="server" Text="Aprobar Todos" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton CommandName="Editar" ID="comando" CssClass="btn btn-default" runat="server">
                                     <i class="fa fa-check-circle marApRe" aria-hidden="true"></i>  
                                             
                                     <i class="fa fa-times-circle" aria-hidden="true"></i>
                            </asp:LinkButton>
                            <asp:LinkButton CommandName="Reparar" ID="comandoRep" CssClass="btn btn-success" runat="server">
                                     <i class="glyphicon glyphicon-wrench" aria-hidden="true"></i> Reparar
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <h4 style="text-align: center">Sin datos para mostrar</h4>
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <asp:Label ID="lblnoRows" runat="server" Text=""></asp:Label>
        </div>
    </div>

    <aside class="modal fade" id="ventanaG">
        <div class="modal-dialog">
            <div class="modal-content">
                <article class="modal-header">
                    <button id="cerrarG" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                    <h4>Defectos del Corte (Bulto a Bulto) </h4>
                </article>
                <article class="modal-body">
                    <dx:ASPxGridView ID="grddefecto" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grddefecto_DataBinding" OnCustomCallback="grddefecto_CustomCallback">
                        <SettingsSearchPanel Visible="true" />
                        <Settings VerticalScrollBarMode="Visible" />
                        <SettingsPager PageSize="100"></SettingsPager>
                    </dx:ASPxGridView>
                </article>
            </div>
        </div>
    </aside>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <h2>
        <asp:Label ID="lbl" runat="server" ForeColor="White" Text="Bundles Check Off"></asp:Label></h2>
    <br />
</asp:Content>

