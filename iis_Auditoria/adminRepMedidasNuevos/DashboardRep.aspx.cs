﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Export;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using SistemaAuditores.DataAccess;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Web.Script.Services;
using System.Web.Services;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;

public partial class adminRepMedidasNuevos_DashboardRep : System.Web.UI.Page
{
    public class estilosClass
    {
        public string estilo { get; set; }
        public string id { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();

            cargagridestilos();

            Session["rep"] = null;
            Session["rep1"] = null;
            Session["rep2"] = null;
            Session["rep3"] = null;

        }
    }

    void carga()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);
        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string query = " select right(c.id, 13) as Hours,  p.POrder, s.style as Style, t.Talla , pm.NombreMedida as POM, cast(e.Valor as decimal(5, 2)) as Spec,cast(m.valor as decimal(5, 2)) as Measurements"
                       + " ,m.texto as Comments"
                       + " from tbCajasInspeccion c"
                       + " join tbMuestrasMedidas m on c.id = m.id"
                       + " join tbEspecificacion e on m.idespecificacion = e.idEspecificacion"
                       + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                       + " join tbEstiloTalla st on c.idEstiloTalla = st.idEstiloTalla"
                       + " join Style s on st.idEstilo = s.Id_Style"
                       + " join tbTalla t on t.idTalla = st.idTalla"
                       + " join POrder p on p.Id_Style = s.Id_Style"
                       + " where CONVERT(date,c.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                       + " order by c.fecha asc";
        DataTable general = DataAccess.Get_DataTable(query);
        Session["rep"] = general;
        grid1.DataSource = Session["rep"];
        grid1.DataBind();
    }

    void cargaStyle()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);
        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string query = " select right(c.id, 13) as Hours, p.POrder, s.style as Style, t.Talla , pm.NombreMedida as POM, cast(e.Valor as decimal(5, 2)) as Spec,cast(m.valor as decimal(5, 2)) as Measurements"
                       + " ,m.texto as Comments"
                       + " from tbCajasInspeccion c"
                       + " join tbMuestrasMedidas m on c.id = m.id"
                       + " join tbEspecificacion e on m.idespecificacion = e.idEspecificacion"
                       + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                       + " join tbEstiloTalla st on c.idEstiloTalla = st.idEstiloTalla"
                       + " join Style s on st.idEstilo = s.Id_Style"
                       + " join tbTalla t on t.idTalla = st.idTalla"
                       + " join POrder p on p.Id_Style = s.Id_Style"
                       + " where s.Style = '" + txtstyle.Text + "' and CONVERT(date,c.fecha) between '"+ dtime.ToString("yyyy-MM-dd") + "' and '"+ dtime1.ToString("yyyy-MM-dd") + "'"
                       + " order by c.fecha asc";
        DataTable general = DataAccess.Get_DataTable(query);
        Session["rep"] = general;
        grid1.DataSource = Session["rep"];
        grid1.DataBind();
    }

    void cargaCorte()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);
        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string query = " select right(c.id, 13) as Hours, p.POrder, s.style as Style, t.Talla , pm.NombreMedida as POM, cast(e.Valor as decimal(5, 2)) as Spec,cast(m.valor as decimal(5, 2)) as Measurements"
                       + " ,m.texto as Comments"
                       + " from tbCajasInspeccion c"
                       + " join tbMuestrasMedidas m on c.id = m.id"
                       + " join tbEspecificacion e on m.idespecificacion = e.idEspecificacion"
                       + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                       + " join tbEstiloTalla st on c.idEstiloTalla = st.idEstiloTalla"
                       + " join Style s on st.idEstilo = s.Id_Style"
                       + " join tbTalla t on t.idTalla = st.idTalla"
                       + " join POrder p on p.Id_Style = s.Id_Style"
                       + " where p.Id_Order = '" + hdnidcorte.Value + "'"
                       + " order by c.fecha asc";
        DataTable general = DataAccess.Get_DataTable(query);
        Session["rep"] = general;
        grid1.DataSource = Session["rep"];
        grid1.DataBind();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<estilosClass> GetStyle(string pref)
    {
        try
        {
            List<estilosClass> list = new List<estilosClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = "SELECT distinct top 10  Style,p.Id_Style FROM Style s join POrder p on s.Id_Style=p.Id_Style  where Style like '%" + pref + "%'";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                estilosClass obj = new estilosClass();

                obj.estilo = dt.Rows[i][0].ToString();
                obj.id = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    //void cargacombo()
    //{
    //    string sqline = "select id_linea,numero from Linea l where l.numero not like '%-%' and l.numero not like '%A%' order by CONVERT(float,numero) asc";

    //    DataTable dt_Line = DataAccess.Get_DataTable(sqline);

    //    DropDownListLine.DataSource = dt_Line;
    //    DropDownListLine.DataTextField = "numero";
    //    DropDownListLine.DataValueField = "id_linea";
    //    DropDownListLine.DataBind();
    //    DropDownListLine.Items.Insert(0, "Select...");

    //    string sqlplant = "SELECT id_planta,descripcion FROM Planta order by descripcion";

    //    DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

    //    DropDownListPlant.DataSource = dt_plant;
    //    DropDownListPlant.DataTextField = "descripcion";
    //    DropDownListPlant.DataValueField = "id_planta";
    //    DropDownListPlant.DataBind();
    //    DropDownListPlant.Items.Insert(0, "Select...");
    //    DropDownListPlant.Items.Insert(6, "All Plant");
    //}

    protected void LinkButton1_Click(object sender, EventArgs e)
    {

    }

    protected void lnkCorte_Click(object sender, EventArgs e)
    {
        try
        {
            //if (txtstyle.Text == string.Empty)
            //{
            //    carga();
            //}
            //else
            //{
            //    cargaStyle();
            //}
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdnidcorte.Value != "" && txtcorte.Text != "")
            {
                cargaCorte();
            }
            else
            if (txtstyle.Text == string.Empty)
            {
                carga();
            }
            else
            {
                cargaStyle();
            }
        }
        catch (Exception)
        {
            throw;
        }        
    }

    void cargagridestilos()
    {
        string sqline = " select s.Style"
                       + " from Style s"
                       + " join POrder p on s.Id_Style = p.Id_Style"
                       + " join tbCajasInspeccion ci on ci.idPorder = p.Id_Order"
                       + " join tbEstiloTalla et on et.idEstiloTalla = ci.idEstiloTalla"
                       + " join tbEspecificacion e on e.idEstiloTalla = et.idEstiloTalla"
                       + " join tblPuntosMedida pm on pm.idPuntosM = e.idPuntoDeMedida"
                       + " where datepart(wk, ci.fecha) = DATEPART(WK, GETDATE())"
                       + " group by s.Style";

        DataTable dtEstilos = DataAccess.Get_DataTable(sqline);

        grvestilos.DataSource = dtEstilos;
        grvestilos.DataBind();
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridViewExporter1.GridViewID = "grid1";           

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;                       

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreatePageForEachLink();
            //compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Report_Empaque - "+ DateTime.Now.ToShortDateString();
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=ReportMeasurement.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {
            throw;
        }
    }    

    public class listatotal
    {
        public string r { get; set; }
        public int totalw { get; set; }
        public string procenW { get; set; }
        public int totalI { get; set; }
        public string procenI { get; set; }
        public int totalF { get; set; }
        public string procenF { get; set; }
        public int totalB { get; set; }
        public string procenB { get; set; }
        public int totalM { get; set; }
        public string procenM { get; set; }
        public int totalH { get; set; }
        public string procenH { get; set; }
    }

    protected void btnclean_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/adminRepMedidasNuevos/DashboardRep.aspx");
    }

    protected void grid1_DataBinding(object sender, EventArgs e)
    {
        grid1.DataSource = Session["rep"];
    }

    protected void grid1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grid1.DataBind();
    }
}