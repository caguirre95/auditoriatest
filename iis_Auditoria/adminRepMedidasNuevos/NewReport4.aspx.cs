﻿using DevExpress.Export;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;
using System.Windows.Forms;

public partial class adminRepMedidasNuevos_NewReport4 : System.Web.UI.Page
{
    public class estilosClass
    {
        public string estilo { get; set; }
        public string id { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();

            cargacombo();

            Session["repPorcent"] = null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<estilosClass> GetStyle(string pref)
    {
        try
        {
            List<estilosClass> list = new List<estilosClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = "SELECT distinct top 10  Style,p.Id_Style FROM Style s join POrder p on s.Id_Style=p.Id_Style  where Style like '%" + pref + "%' and p.Id_Cliente=11";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                estilosClass obj = new estilosClass();

                obj.estilo = dt.Rows[i][0].ToString();
                obj.id = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    void cargacombo()
    {
        string sqline = "select id_linea,numero from Linea l where l.numero not like '%-%' and l.numero not like '%A%' order by CONVERT(float,numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
    }

    void cargagrid()
    {
        DateTime dt1 = DateTime.Parse(txtfecha1.Text);
        DateTime dt2 = DateTime.Parse(txtfecha2.Text);

        if (txtcorte.Text != string.Empty && hdnidcorte.Value != string.Empty)
        {
            string query = " select b.NombreMedida, b.Big, b.Spec, b.Small, b.TotalPiezas,"
                         + " CONCAT(CAST(CAST(ISNULL(b.Big, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Big',"
                         + " CONCAT(CAST(CAST(ISNULL(b.Spec, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Spec',"
                         + " CONCAT(CAST(CAST(ISNULL(b.Small, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Small'"
                         + " from (select NombreMedida, ISNULL([Big], 0) Big, ISNULL([In Spec],0) Spec, ISNULL([Small],0) Small, ISNULL([Big],0)+ISNULL([In Spec],0)+ISNULL([Small],0) TotalPiezas"
                         + " from (select pm.NombreMedida, ep.estado, COUNT(ep.estado) Total from tbMedidasEmbarqueAL eal"
                         + " join tbMedidasEmbarqueP ep on ep.id = eal.id join POrder p on p.Id_Order = eal.idPorder join Style s on s.Id_Style = eal.idEstilo join Linea l on l.id_linea = p.Id_Linea2"
                         + " join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida where p.Id_Order = " + hdnidcorte.Value + " group by pm.NombreMedida, ep.estado) a"
                         + " pivot(sum(Total) for estado in ([Big],[In Spec],[Small])) as pvt) b";

            DataTable querystyle = DataAccess.Get_DataTable(query);
            Session["repPorcent"] = querystyle;
            gridporcent.DataSource = Session["repPorcent"];
            gridporcent.DataBind();
        }
        else
        if (txtstyle.Text != string.Empty)
        {
            string query = " select b.NombreMedida, b.Big, b.Spec, b.Small, b.TotalPiezas,"
                         + " CONCAT(CAST(CAST(ISNULL(b.Big, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Big',"
                         + " CONCAT(CAST(CAST(ISNULL(b.Spec, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Spec',"
                         + " CONCAT(CAST(CAST(ISNULL(b.Small, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Small'"
                         + " from (select NombreMedida, ISNULL([Big], 0) Big, ISNULL([In Spec],0) Spec, ISNULL([Small],0) Small, ISNULL([Big],0)+ISNULL([In Spec],0)+ISNULL([Small],0) TotalPiezas"
                         + " from (select pm.NombreMedida, ep.estado, COUNT(ep.estado) Total from tbMedidasEmbarqueAL eal"
                         + " join tbMedidasEmbarqueP ep on ep.id = eal.id join POrder p on p.Id_Order = eal.idPorder join Style s on s.Id_Style = eal.idEstilo join Linea l on l.id_linea = p.Id_Linea2"
                         + " join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida "
                         + " where s.Style = '" + txtstyle.Text + "' and CONVERT(date, fechaRegistro) between '" + dt1.ToString("yyyy-MM-dd") + "' and '" + dt2.ToString("yyyy-MM-dd") + "'"
                         + " group by pm.NombreMedida, ep.estado) a pivot(sum(Total) for estado in ([Big],[In Spec],[Small])) as pvt) b";

            DataTable querystyle = DataAccess.Get_DataTable(query);
            Session["repPorcent"] = querystyle;
            gridporcent.DataSource = Session["repPorcent"];
            gridporcent.DataBind();
        }
        else
        if (DropDownListPlant.SelectedValue != "0")
        {
            string query = " select b.NombreMedida, b.Big, b.Spec, b.Small, b.TotalPiezas,"
                         + " CONCAT(CAST(CAST(ISNULL(b.Big, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Big',"
                         + " CONCAT(CAST(CAST(ISNULL(b.Spec, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Spec',"
                         + " CONCAT(CAST(CAST(ISNULL(b.Small, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Small'"
                         + " from (select NombreMedida, ISNULL([Big], 0) Big, ISNULL([In Spec],0) Spec, ISNULL([Small],0) Small, ISNULL([Big],0)+ISNULL([In Spec],0)+ISNULL([Small],0) TotalPiezas"
                         + " from (select pm.NombreMedida, ep.estado, COUNT(ep.estado) Total from tbMedidasEmbarqueAL eal join tbMedidasEmbarqueP ep on ep.id = eal.id"
                         + " join POrder p on p.Id_Order = eal.idPorder join Style s on s.Id_Style = eal.idEstilo join Linea l on l.id_linea = p.Id_Linea2 join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida"
                         + " where p.Id_Planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date, fechaRegistro) between '" + dt1.ToString("yyyy-MM-dd") + "' and '" + dt2.ToString("yyyy-MM-dd") + "' group by pm.NombreMedida, ep.estado) a pivot(sum(Total) for estado in ([Big],[In Spec],[Small])) as pvt) b";

            DataTable querystyle = DataAccess.Get_DataTable(query);
            Session["repPorcent"] = querystyle;
            gridporcent.DataSource = Session["repPorcent"];
            gridporcent.DataBind();
        }
        else
        if (DropDownListLine.SelectedValue != "0")
        {
            string query = " select b.NombreMedida, b.Big, b.Spec, b.Small, b.TotalPiezas,"
                         + " CONCAT(CAST(CAST(ISNULL(b.Big, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Big',"
                         + " CONCAT(CAST(CAST(ISNULL(b.Spec, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Spec',"
                         + " CONCAT(CAST(CAST(ISNULL(b.Small, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Small'"
                         + " from (select NombreMedida, ISNULL([Big], 0) Big, ISNULL([In Spec],0) Spec, ISNULL([Small],0) Small, ISNULL([Big],0)+ISNULL([In Spec],0)+ISNULL([Small],0) TotalPiezas"
                         + " from (select pm.NombreMedida, ep.estado, COUNT(ep.estado) Total from tbMedidasEmbarqueAL eal join tbMedidasEmbarqueP ep on ep.id = eal.id"
                         + " join POrder p on p.Id_Order = eal.idPorder join Style s on s.Id_Style = eal.idEstilo join Linea l on l.id_linea = p.Id_Linea2 join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida"
                         + " where l.id_linea = " + DropDownListLine.SelectedValue + " and CONVERT(date, fechaRegistro) between '" + dt1.ToString("yyyy-MM-dd") + "' and '" + dt2.ToString("yyyy-MM-dd") + "' group by pm.NombreMedida, ep.estado) a pivot(sum(Total) for estado in ([Big],[In Spec],[Small])) as pvt) b";

            DataTable querystyle = DataAccess.Get_DataTable(query);
            Session["repPorcent"] = querystyle;
            gridporcent.DataSource = Session["repPorcent"];
            gridporcent.DataBind();
        }
        else
        {
            string query = " select b.NombreMedida, b.Big, b.Spec, b.Small, b.TotalPiezas,"
                         + " CONCAT(CAST(CAST(ISNULL(b.Big, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Big',"
                         + " CONCAT(CAST(CAST(ISNULL(b.Spec, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Spec',"
                         + " CONCAT(CAST(CAST(ISNULL(b.Small, 0) as decimal(6, 3)) / CAST(ISNULL(b.TotalPiezas, 0) as decimal(6, 3)) * 100 as decimal(6, 3)), ' %') as '%Small'"
                         + " from (select NombreMedida, ISNULL([Big], 0) Big, ISNULL([In Spec],0) Spec, ISNULL([Small],0) Small, ISNULL([Big],0)+ISNULL([In Spec],0)+ISNULL([Small],0) TotalPiezas"
                         + " from (select pm.NombreMedida, ep.estado, COUNT(ep.estado) Total from tbMedidasEmbarqueAL eal"
                         + " join tbMedidasEmbarqueP ep on ep.id = eal.id join POrder p on p.Id_Order = eal.idPorder join Style s on s.Id_Style = eal.idEstilo join Linea l on l.id_linea = p.Id_Linea2"
                         + " join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida where CONVERT(date, fechaRegistro) between '" + dt1.ToString("yyyy-MM-dd") + "' and '" + dt2.ToString("yyyy-MM-dd") + "' group by pm.NombreMedida, ep.estado) a"
                         + " pivot(sum(Total) for estado in ([Big],[In Spec],[Small])) as pvt) b";

            DataTable querystyle = DataAccess.Get_DataTable(query);
            Session["repPorcent"] = querystyle;
            gridporcent.DataSource = Session["repPorcent"];
            gridporcent.DataBind();
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            cargagrid();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridViewExporter1.GridViewID = "gridporcent";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreatePageForEachLink();
            //compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Report_Empaque - " + DateTime.Now.ToShortDateString();
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=ReportMeasurement.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnclean_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/adminRepMedidasNuevos/NewReport4.aspx");
    }

    protected void gridporcent_DataBinding(object sender, EventArgs e)
    {
        gridporcent.DataSource = Session["repPorcent"];
    }

    protected void gridporcent_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridporcent.DataBind();
    }

    protected void lnkCorte_Click(object sender, EventArgs e)
    {
        try
        {
            cargagrid();
        }
        catch (Exception)
        {
            throw;
        }
    }
}