﻿using DevExpress.Export;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;

public partial class adminRepMedidasNuevos_NewReport1 : System.Web.UI.Page
{
    public class estilosClass
    {
        public string estilo { get; set; }
        public string id { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SideBySideBarSeriesLabel label = (SideBySideBarSeriesLabel)WebChartControl1.SeriesTemplate.Label;


        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();

            visible();
            cargacombo();
            cargagridestilos();

            Session["rep"] = null;
            Session["rep1"] = null;
            Session["rep2"] = null;
            Session["rep3"] = null;


            chbShowLabels.Checked = WebChartControl1.SeriesTemplate.LabelsVisibility == DefaultBoolean.True;
            spnLabelIndent.Value = label.Indent;

        }

        bool indentEnabled = chbShowLabels.Checked;
        spnLabelIndent.ClientEnabled = indentEnabled;
        WebChartControl1.CrosshairOptions.CrosshairLabelMode = CrosshairLabelMode.ShowForNearestSeries;
    }

    void PerformShowLabelsAction()
    {
        WebChartControl1.SeriesTemplate.LabelsVisibility = chbShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False;
        WebChartControl1.CrosshairEnabled = chbShowLabels.Checked ? DefaultBoolean.False : DefaultBoolean.True;
    }

    void PerformLabelIndentAction()
    {
        ((BarSeriesLabel)WebChartControl1.SeriesTemplate.Label).Indent = Convert.ToInt32(spnLabelIndent.Value);
    }

    protected void WebChartControl1_CustomCallback(object sender, CustomCallbackEventArgs e)
    {
        if (e.Parameter == "ShowLabels")
            PerformShowLabelsAction();
        else if (e.Parameter == "LabelIndent")
            PerformLabelIndentAction();

        WebChartControl1.DataBind();
    }

    void visible()
    {
        btnexport1.Visible = false;
        btnexport.Visible = true;
    }

    void nvisible()
    {
        btnexport1.Visible = true;
        btnexport.Visible = false;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<estilosClass> GetStyle(string pref)
    {
        try
        {
            List<estilosClass> list = new List<estilosClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = "SELECT distinct top 10  Style,p.Id_Style FROM Style s join POrder p on s.Id_Style=p.Id_Style  where Style like '%" + pref + "%'";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                estilosClass obj = new estilosClass();

                obj.estilo = dt.Rows[i][0].ToString();
                obj.id = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    void cargacombo()
    {
        string sqline = "select id_linea,numero from Linea l where l.numero not like '%-%' and l.numero not like '%A%' order by CONVERT(float,numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta,descripcion FROM Planta order by descripcion";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
        DropDownListPlant.Items.Insert(6, "All Plant");

    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        visible();

        WebChartControl1.DataBind();
    }

    void cargagridestilos()
    {//Cambio
        string sqline = "select s.Style from tbMedidasEmbarqueAL eal join Style s on s.Id_Style = eal.idEstilo where DATEPART(WK, eal.fechaRegistro) = DATEPART(WK, GETDATE()) group by s.Style";

        DataTable dtEstilos = DataAccess.Get_DataTable(sqline);
        grvestilos.DataSource = dtEstilos;
        grvestilos.DataBind();
    }

    DataTable funcionCarga2()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);

        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string resp = RadioButtonList1.SelectedValue;

        string query = "";

        switch (resp)
        {
            case "1"://cut
                if (txtPorder.Text != string.Empty)
                {
                    query = " select pm.NombreMedida, ep.estado, COUNT(ep.estado) Total"
                          + " from tbMedidasEmbarqueAL eal"
                          + " left join tbMedidasEmbarqueP ep on eal.id = ep.id left join POrder p on p.Id_Order = eal.idPorder left join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida"
                          + " where p.POrder='" + txtPorder.Text + "'"
                          + " group by pm.NombreMedida, ep.estado";
                }
                else
                {
                    resp = "fin";
                }
                break;

            case "2"://style
                if (txtstyle.Text != string.Empty)
                {
                    query = " select pm.NombreMedida, ep.estado, COUNT(ep.estado) Total from tbMedidasEmbarqueAL eal"
                          + " join tbMedidasEmbarqueP ep on eal.id = ep.id join POrder p on p.Id_Order = eal.idPorder join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida join Style s on s.Id_Style = p.Id_Style"                         
                          + " where s.Style = '" + txtstyle.Text + "' and CONVERT(date, eal.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                          + " group by pm.NombreMedida, ep.estado";
                }
                else
                {
                    resp = "fin";
                }
                break;

            case "3"://line
                if (DropDownListLine.SelectedItem.Text != "Select...")
                {//Tiro
                    query = " select pm.NombreMedida, ep.estado, COUNT(ep.estado) Total from tbMedidasEmbarqueAL eal"
                          + " join tbMedidasEmbarqueP ep on eal.id = ep.id join POrder p on p.Id_Order = eal.idPorder join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida"                         
                          + " where p.Id_Linea2 = '" + DropDownListLine.SelectedValue + "' and  CONVERT(date,eal.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                          + " group by pm.NombreMedida, ep.estado, pm.idPuntosM"
                          + " order by pm.idPuntosM";   
                }
                else
                {
                    resp = "fin";
                }
                break;

            case "4"://plant
                if (DropDownListPlant.SelectedItem.Text == "Select...")
                {
                    resp = "fin";
                }
                else if (DropDownListPlant.SelectedItem.Text == "All Plant")
                {

                    query = " select pm.NombreMedida, ep.estado, COUNT(ep.estado) Total from tbMedidasEmbarqueAL eal"
                          + " join tbMedidasEmbarqueP ep on eal.id = ep.id join POrder p on p.Id_Order = eal.idPorder join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida"
                          + " where CONVERT(date,eal.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                          + " group by pm.NombreMedida, ep.estado, pm.idPuntosM"
                          + " order by pm.idPuntosM";
                }
                else
                {
                    query = " select pm.NombreMedida, ep.estado, COUNT(ep.estado) Total from tbMedidasEmbarqueAL eal"
                          + " join tbMedidasEmbarqueP ep on eal.id = ep.id join POrder p on p.Id_Order = eal.idPorder join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida"                          
                          + " where p.Id_Planta = " + DropDownListPlant.SelectedValue + " and  CONVERT(date,eal.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                          + " group by pm.NombreMedida, ep.estado, pm.idPuntosM"
                          + " order by pm.idPuntosM";
                }
                break;

            default:
                resp = "fin";
                break;
        }

        DataTable dt = new DataTable();
        if (resp != "fin")
        {
            if (txtPorder.Text != "")
            {
                DataTable dt2 = new DataTable();
                dt2 = DataAccess.Get_DataTable("select p.comments from POrder p where p.POrder='" + txtPorder.Text + "'");

                foreach (DataRow item in dt2.Rows)
                {
                    memo.Text = item[0].ToString();
                }

            }

            dt = new DataTable();
            dt = DataAccess.Get_DataTable(query);

            return dt;
        }
        else
        {
            //debe seleccionar una opcion
            dt = null;
            return dt;
        }
    }

    protected void btnclean_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/adminRepMedidasNuevos/ReportMedidaEmpaque.aspx");
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void WebChartControl1_DataBinding(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();

        dt = funcionCarga2();

        WebChartControl1.DataSource = dt;

        Session["rep"] = dt;

    }

    private void Link_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
    {
        string resp = RadioButtonList1.SelectedValue;
        string mess = "";
        if (resp == "1")
        {
            mess = txtPorder.Text + "\n" + memo.Text;
        }
        else
        {
            mess = memo.Text;
        }

        TextBrick brick;
        brick = e.Graph.DrawString(mess, Color.Navy, new RectangleF(0, 0, 700, 50), DevExpress.XtraPrinting.BorderSide.None);
        brick.Font = new Font("Times New Roman", 12);
        brick.StringFormat = new BrickStringFormat(StringAlignment.Center);
    }

    protected void btnexport1_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridViewExporter1.GridViewID = "ASPxGridView2";
            ASPxGridViewExporter1.FileName = "Report " + DateTime.Now.ToShortDateString();
            ASPxGridViewExporter1.WriteXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {

        try
        {
            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

            WebChartControl1.DataSource = Session["rep"];
            WebChartControl1.DataBind();
            link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)WebChartControl1).Chart;           

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link2 });

            compositeLink.CreatePageForEachLink();
            //compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Report_Empaque";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=ReportMeasurement.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            throw;
        }

    }
}