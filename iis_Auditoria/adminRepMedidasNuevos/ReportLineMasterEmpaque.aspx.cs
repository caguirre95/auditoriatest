﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepMedidasNuevos_ReportLineMasterEmpaque : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtfecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }
    }

    protected void btnGenerar_Click(object sender, EventArgs e)
    {
        if (drpplan.SelectedValue != "0")
        {
            string query = " select isnull(b.Total,0) as Total, b.NombreMedida,'Line '+ a.numero as Line from " 
                      + " (  select l.numero, l.id_linea from Linea l where l.id_planta = " + drpplan.SelectedValue + " and numero not like '%-%' and numero not like '%A%' ) as a"
                       + " join"
                       + " ( select COUNT(mm.texto)as Total, pm.NombreMedida, 'Linea ' + l.numero as Linea, l.id_linea from tbCajasInspeccion ci"
                       + " join tbMuestrasMedidas mm on mm.id = ci.id"
                       + " join POrder p on p.Id_Order = ci.idPorder"
                       + " join Linea l on l.id_linea = p.Id_Linea"
                       + " join tbEspecificacion e on e.idEspecificacion = mm.idespecificacion"
                       + " join tblPuntosMedida pm on pm.idPuntosM = e.idPuntoDeMedida"                                            
                       + " where l.id_planta = " + drpplan.SelectedValue
                       + " and CONVERT(date, ci.Fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                       + " group by pm.NombreMedida, l.numero, l.id_linea ) as b"
                       + " on a.id_linea = b.id_linea"
                       + " order by b.linea desc";

            DataTable dt = new DataTable();
            dt = DataAccess.Get_DataTable(query);

            packet_chart.DataSource = dt;
            packet_chart.DataBind();
        }
        else
        {

        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (drpplan.SelectedValue != "0")
        {
            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

            string query = " select isnull(b.Total,0) as Total, b.NombreMedida,'Line '+ a.numero as Line from "
                      + " (  select l.numero, l.id_linea from Linea l where l.id_planta = " + drpplan.SelectedValue + " and numero not like '%-%' and numero not like '%A%' ) as a"
                       + " join"
                       + " ( select COUNT(mm.texto)as Total, pm.NombreMedida, 'Linea ' + l.numero as Linea, l.id_linea from tbCajasInspeccion ci"
                       + " join tbMuestrasMedidas mm on mm.id = ci.id"
                       + " join POrder p on p.Id_Order = ci.idPorder"
                       + " join Linea l on l.id_linea = p.Id_Linea"
                       + " join tbEspecificacion e on e.idEspecificacion = mm.idespecificacion"
                       + " join tblPuntosMedida pm on pm.idPuntosM = e.idPuntoDeMedida"
                       + " where l.id_planta = " + drpplan.SelectedValue
                       + " and CONVERT(date, ci.Fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                       + " group by pm.NombreMedida, l.numero, l.id_linea ) as b"
                       + " on a.id_linea = b.id_linea"
                       + " order by b.linea desc";

            DataTable dt = new DataTable();
            dt = DataAccess.Get_DataTable(query);

            packet_chart.DataSource = dt;
            packet_chart.DataBind();

            link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)packet_chart).Chart;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link2 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Master_Report_Line";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Report-Plant.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        else
        {

        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/adminRepMedidasNuevos/ReportLineMasterEmpaque.aspx");
    }
}