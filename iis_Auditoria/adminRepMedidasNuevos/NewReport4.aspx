﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NewReport4.aspx.cs" Inherits="adminRepMedidasNuevos_NewReport4" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porder);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porder);
                    $('#<%=hdnidcorte.ClientID%>').val(ui.item.idorder);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtstyle.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ReportMedidaEmpaque.aspx/GetStyle",
                        data: "{'pref' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    estilo: item.estilo,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })
            })

        });

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <link href="../css/font-awesome.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Measurement Report </strong></h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <asp:HiddenField runat="server" ID="hdnidcorte" />
                        Cut:
                         <div class="input-group">
                             <span class="input-group-btn">
                                 <asp:LinkButton Text="" OnClick="lnkCorte_Click" CssClass="btn btn-default" ID="lnkCorte" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                             </span>
                             <div class="ui-widget" style="text-align: left;">
                                 <asp:TextBox ID="txtcorte" placeholder="Cut" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                             </div>
                         </div>
                        Style:
                         <div class="input-group">
                             <span class="input-group-btn">
                                 <asp:LinkButton Text="" OnClick="LinkButton1_Click" CssClass="btn btn-default" ID="LinkButton2" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                             </span>
                             <div class="ui-widget" style="text-align: left;">
                                 <asp:TextBox ID="txtstyle" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                             </div>
                         </div>
                        Plant:
                         <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server">
                             <asp:ListItem Value="0">Select...</asp:ListItem>
                             <asp:ListItem Value="1">Roc1</asp:ListItem>
                             <asp:ListItem Value="3">Roc6</asp:ListItem>
                             <asp:ListItem Value="4">Roc7</asp:ListItem>
                         </asp:DropDownList>
                        Line:
                         <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="padding-top: 15px">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select search filter</div>
                            <div class="panel-body">
                                Start Date
                            <div class="input-group">
                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                                End Date                          
                                <div class="input-group">
                                    <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                    <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div style="padding-top: 15px">
                            <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" OnClick="btngenerar_Click" runat="server" />
                        </div>
                        <div style="padding-top: 15px">
                            <asp:Button ID="btnexport1" CssClass="btn btn-success form-control" OnClick="btnexport_Click" runat="server" Text="Export Excel" />
                        </div>
                        <div style="padding-top: 15px">
                            <asp:Button Text="Limpiar Formulario" CssClass=" btn btn-info form-control" ID="btnclean" OnClick="btnclean_Click" runat="server" />
                        </div>

                        <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                    </div>

                </div>

                <div class="col-lg-12" style="margin-top: 5px">
                    <dx:ASPxGridView ID="gridporcent" runat="server" Width="100%" Theme="Metropolis" AutoGenerateColumns="False" OnDataBinding="gridporcent_DataBinding" OnCustomCallback="gridporcent_CustomCallback">
                        <Columns>
                            <dx:GridViewDataDateColumn FieldName="NombreMedida" Width="100" Caption="Nombre Medida" />
                            <dx:GridViewDataDateColumn FieldName="Big" Width="100" Caption="Big" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <dx:GridViewDataDateColumn FieldName="Spec" Width="100" Caption="In Spec" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <dx:GridViewDataTextColumn FieldName="Small" Width="100" Caption="Small" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <dx:GridViewDataTextColumn FieldName="TotalPiezas" Width="100" Caption="Total de Piezas Medidas" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <dx:GridViewDataTextColumn FieldName="%Big" Width="100" Caption="% Big" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <dx:GridViewDataTextColumn FieldName="%Spec" Width="120" Caption="% In Spec" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <dx:GridViewDataTextColumn FieldName="%Small" Width="120" Caption="% Small" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                        </Columns>
                    </dx:ASPxGridView>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

