﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteMedidaMasterEmpaque.aspx.cs" Inherits="adminRepMedidasNuevos_ReporteMedidaMasterEmpaque" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraReports.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtstyle.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ReporteMedidaMasterEmpaque.aspx/GetStyle",
                        data: "{'pref' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    estilo: item.estilo,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
            };
        });
    </script>s

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../StyleSheetAuto.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">
        <%-- <div class="col-lg-12">--%>

        <div class="col-lg-10 col-lg-6 col-lg-4 col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Select Date</h3>
                </div>
                <div class="panel-body">
                    <dx:ASPxDateEdit ID="txtfecha1" Theme="Metropolis" Height="35px" runat="server" EditFormat="Custom" Width="100%">
                    </dx:ASPxDateEdit>

                    <br />
                    <dx:ASPxDateEdit ID="txtfecha2" Theme="Metropolis" Height="35px" runat="server" EditFormat="Custom" Width="100%">
                    </dx:ASPxDateEdit>
                    <br />

                    <hr />
                    <asp:Button ID="btnGenerar" OnClick="btnGenerar_Click" runat="server" Text="Generate Report" CssClass="btn btn-primary form-control" />
                </div>
            </div>
        </div>

        <div class="col-lg-10 col-lg-6 col-lg-4 col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Select Date</h3>
                </div>
                <div class="panel-body">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                        <asp:ListItem Text="All Style" Value="0" />
                        <asp:ListItem Text="By Style" Value="1" />
                    </asp:RadioButtonList>
                    <br />

                    <div class="input-group">
                        <span class="input-group-btn">
                            <asp:LinkButton CssClass="btn btn-default" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                        </span>
                        <div class="ui-widget" style="text-align: left;">
                            <asp:TextBox ID="txtstyle" placeholder="Style" class=" form-control" Font-Size="14px" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                            <%--<ajaxToolkit:AutoCompleteExtender ID="txtstyle_AutoCompleteExtender" runat="server" BehaviorID="txtstyle_AutoCompleteExtender"
                                ServicePath="ReportMedidaMasterEmpaque.aspx" ServiceMethod="GetStyle" TargetControlID="txtstyle"
                                CompletionInterval="10" Enabled="True"
                                MinimumPrefixLength="1"
                                CompletionListCssClass="CompletionList"
                                CompletionListHighlightedItemCssClass="CompletionListHighlightedItem"
                                CompletionListItemCssClass="CompletionListItem"
                                DelimiterCharacters="">
                            </ajaxToolkit:AutoCompleteExtender>--%>
                        </div>
                    </div>

                    <br />

                    <%-- <asp:UpdatePanel ID="update" runat="server">
                        <ContentTemplate>

                            <asp:DropDownList ID="DropDownListProcess" OnSelectedIndexChanged="DropDownListProcess_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                <asp:ListItem Value="0"> Select...</asp:ListItem>
                                <asp:ListItem Value="1"> Production</asp:ListItem>
                                <asp:ListItem Value="2"> After Wash</asp:ListItem>
                                <asp:ListItem Value="3"> Audit Final</asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <asp:Panel ID="Panel1" Visible="false" runat="server">
                                Seccion 
                        <asp:RadioButtonList ID="RadioButtonList2" runat="server">
                            <asp:ListItem Value="bultoabulto">Before Wash Final Audit</asp:ListItem>
                            <asp:ListItem Value="inspInter">Before Wash In Line</asp:ListItem>
                        </asp:RadioButtonList>
                            </asp:Panel>

                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
            </div>
        </div>
        <%--</div>--%>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Report</h3>
                </div>
                <div class="panel-body">
                    <dx:ASPxWebDocumentViewer ID="ASPxWebDocumentViewer1" Visible="true" runat="server">
                    </dx:ASPxWebDocumentViewer>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

