﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepMedidasNuevos_ReporteMedidaMasterGuia : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void LinkButton15_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("~/adminRepMedidasNuevos/ReportMedidaEmpaque.aspx");
            //Response.Redirect("~/ReportOne");
        }
        catch (Exception ex)
        {
            throw;
        }        
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("~/adminRepMedidasNuevos/DashboardRep.aspx");
            //Response.Redirect("~/ReportTwo");
        }
        catch (Exception ex)
        {

            throw;
        }        
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("~/adminRepMedidasNuevos/ReportLineMasterEmpaque.aspx");
            //Response.Redirect("~/ReportThree");
        }
        catch (Exception ex)
        {

            throw;
        }        
    }

    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("~/adminRepMedidasNuevos/PorcentajeReporte.aspx");
            //Response.Redirect("~/ReportFour");
        }
        catch (Exception ex)
        {

            throw;
        }        
    }

    protected void lnknew1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/adminRepMedidasNuevos/NewReport1.aspx");
    }

    protected void lnknew2_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/adminRepMedidasNuevos/NewReport2.aspx");
    }

    protected void lnknew3_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/adminRepMedidasNuevos/NewReport3.aspx");
    }

    protected void lnknew4_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/adminRepMedidasNuevos/NewReport4.aspx");
    }
}