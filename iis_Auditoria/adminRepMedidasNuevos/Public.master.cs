﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepMedidasNuevos_Public : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (Page.User.Identity.IsAuthenticated == true)//
            {
                cargaClientes();
            }
            else
            {
                FormsAuthentication.SignOut();
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
    }

    void cargaClientes()
    {

        DataTable dt1 = DataAccess.Get_DataTable("select Id_Cliente,Cliente from Cliente where Estado=1 order by Cliente");
        DropDownListClientes.DataSource = dt1;
        DropDownListClientes.DataTextField = "Cliente";
        DropDownListClientes.DataValueField = "Id_Cliente";
        DropDownListClientes.DataBind();
        DropDownListClientes.Items.Insert(0,"Select...");

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            FormsAuthentication.SignOut();
            Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
//            Response.Redirect("~/Dashboard", false);
        }
        catch (Exception) 
        {

        }
    }

    protected void prc_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Reportes");
    }

    protected void DropDownListClientes_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (DropDownListClientes.SelectedItem.Text!="Select...")
            {
                Response.Redirect("/DashBoard?idcliente=" + DropDownListClientes.SelectedValue, false);
            }

        }
        catch (Exception)
        {

            throw;
        }
    }
}
