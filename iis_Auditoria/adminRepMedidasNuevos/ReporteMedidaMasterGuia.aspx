﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteMedidaMasterGuia.aspx.cs" Inherits="adminRepMedidasNuevos_ReporteMedidaMasterGuia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .box_effect {
            height: 250px;
            width: 180px;
            padding-top: 15px;
            text-align: center;
            border-width: 160px;
            margin: auto;
            margin-bottom: 15px !important;
            opacity: 0.8;
            border: 1px solid rgba(0,0,0,0.2);
        }

        .te {
            text-decoration: none;
        }

        .im {
            z-index: -1;
        }

        .mty {
            margin-left: 50px;
            margin-top: 15px;
        }

        .te:hover {
            color: chocolate !important;
            text-decoration: none;
            outline-style: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <br />
    <div class="container">
        <div class="row">
            <div class="col-lg-4 ">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton15" runat="server" CssClass="te" OnClick="LinkButton15_Click">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Measurement Report </span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
            <div class="col-lg-4 ">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="te" OnClick="LinkButton2_Click">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Records by Style Report</span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4 ">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="te" OnClick="LinkButton1_Click">
                    <div>                        
                        <img class=" mt" src="/img/medidas.jpg" alt="Bundle by Bundle" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">Measurements by Plant Report</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-4 ">
            </div>

            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton3" runat="server" CssClass="te" OnClick="LinkButton3_Click">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">Summary Report </span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4">
            </div>

        </div>

        <div class="row">


            <div class="col-lg-3">
                <div class="box_effect">
                    <asp:LinkButton ID="lnknew1" runat="server" CssClass="te" OnClick="lnknew1_Click">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">New Report 1 </span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="box_effect">
                    <asp:LinkButton ID="lnknew2" runat="server" CssClass="te" OnClick="lnknew2_Click">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">New Report 2 </span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="box_effect">
                    <asp:LinkButton ID="lnknew3" runat="server" CssClass="te" OnClick="lnknew3_Click">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">New Report 3 </span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="box_effect">
                    <asp:LinkButton ID="lnknew4" runat="server" CssClass="te" OnClick="lnknew4_Click">
                        <div>                        
                            <img src="/img/medidas.jpg" alt="Inspection" class="mt" />
                        </div>
                        <div style="margin-top:20px">
                            <span style="font-weight: bold;">New Report 4 </span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

        </div>
    </div>
</asp:Content>

