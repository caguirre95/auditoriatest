﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepMedidasNuevos_ReporteMedidaMasterEmpaque : System.Web.UI.Page
{

    public class estilosClass
    {
        public string estilo { get; set; }
        public string id { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Date = DateTime.Now;
            txtfecha2.Date = DateTime.Now;
            RadioButtonList1.SelectedValue = "0";
        }
    }

    protected void btnGenerar_Click(object sender, EventArgs e)
    {
        #region
        //if (RadioButtonList1.SelectedValue == "0")
        //{
        //    if (DropDownListProcess.SelectedValue == "1")
        //    {
        //        if (RadioButtonList2.SelectedIndex > -1)
        //        {
        //            XtraReport1 rep = new XtraReport1();

        //            rep.Parameters[0].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
        //            rep.Parameters[1].Value = txtfecha2.Date.ToString("yyyy-MM-dd");
        //            rep.Parameters[2].Value = RadioButtonList2.SelectedValue;

        //            ASPxWebDocumentViewer1.OpenReport(rep);
        //        }
        //    }
        //    else if (DropDownListProcess.SelectedValue == "2")
        //    {
        //        XtraReport1Intex rep = new XtraReport1Intex();

        //        rep.Parameters[0].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
        //        rep.Parameters[1].Value = txtfecha2.Date.ToString("yyyy-MM-dd");
        //        // rep.Parameters[2].Value = "inspInter";

        //        ASPxWebDocumentViewer1.OpenReport(rep);
        //    }
        //    else if (DropDownListProcess.SelectedValue == "3")
        //    {
        //        XtraReport1After rep = new XtraReport1After();

        //        rep.Parameters[0].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
        //        rep.Parameters[1].Value = txtfecha2.Date.ToString("yyyy-MM-dd");
        //        //  rep.Parameters[2].Value = "inspInter";

        //        ASPxWebDocumentViewer1.OpenReport(rep);
        //    }
        //}
        //else
        //{
        //    if (DropDownListProcess.SelectedValue == "1")
        //    {
        //        if (RadioButtonList2.SelectedIndex > -1)
        //        {
        //            XtraReportXStyleMaster rep = new XtraReportXStyleMaster();

        //            rep.Parameters[1].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
        //            rep.Parameters[2].Value = txtfecha2.Date.ToString("yyyy-MM-dd");
        //            rep.Parameters[3].Value = RadioButtonList2.SelectedValue;
        //            rep.Parameters[0].Value = txtstyle.Text;

        //            ASPxWebDocumentViewer1.OpenReport(rep);
        //        }

        //    }
        //    else if (DropDownListProcess.SelectedValue == "2")
        //    {
        //        XtraReportXStyleMasterInt rep = new XtraReportXStyleMasterInt();

        //        rep.Parameters[0].Value = txtstyle.Text;
        //        rep.Parameters[1].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
        //        rep.Parameters[2].Value = txtfecha2.Date.ToString("yyyy-MM-dd");

        //        ASPxWebDocumentViewer1.OpenReport(rep);
        //    }
        //    else if (DropDownListProcess.SelectedValue == "3")
        //    {
        //        XtraReportXStyleMasterAf rep = new XtraReportXStyleMasterAf();

        //        rep.Parameters[0].Value = txtstyle.Text;
        //        rep.Parameters[1].Value = txtfecha1.Date.ToString("yyyy-MM-dd");
        //        rep.Parameters[2].Value = txtfecha2.Date.ToString("yyyy-MM-dd");

        //        ASPxWebDocumentViewer1.OpenReport(rep);
        //    }

        //}
        #endregion

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<estilosClass> GetStyle(string pref)
    {
        try
        {
            List<estilosClass> list = new List<estilosClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = "SELECT distinct top 10  Style,p.Id_Style FROM Style s join POrder p on s.Id_Style=p.Id_Style  where Style like '%" + pref + "%' and p.Id_Cliente=11";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                estilosClass obj = new estilosClass();

                obj.estilo = dt.Rows[i][0].ToString();
                obj.id = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }
}