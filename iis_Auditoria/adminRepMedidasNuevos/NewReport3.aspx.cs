﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepMedidasNuevos_NewReport3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtfecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }
    }

    protected void btnGenerar_Click(object sender, EventArgs e)
    {
        if (drpplan.SelectedValue != "0")
        {
            string query = " select l.numero Linea, pm.NombreMedida, COUNT(eal.estado) Total"
                         + " from tbMedidasEmbarqueAL eal"
                         + " join tbMedidasEmbarqueP ep on ep.id = eal.id"
                         + " join POrder p on p.Id_Order = eal.idPorder"
                         + " join Linea l on l.id_linea = p.Id_Linea2"
                         + " join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida"
                         + " where p.Id_Planta = " + drpplan.SelectedValue + " and CONVERT(date, eal.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                         + " group by l.numero, pm.NombreMedida order by l.numero asc";

            DataTable dt = new DataTable();
            dt = DataAccess.Get_DataTable(query);

            packet_chart.DataSource = dt;
            packet_chart.DataBind();
        }
        else
        {

        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (drpplan.SelectedValue != "0")
        {
            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

            string query = " select l.numero Linea, pm.NombreMedida, COUNT(eal.estado) Total"
                         + " from tbMedidasEmbarqueAL eal"
                         + " join tbMedidasEmbarqueP ep on ep.id = eal.id"
                         + " join POrder p on p.Id_Order = eal.idPorder"
                         + " join Linea l on l.id_linea = p.Id_Linea2"
                         + " join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida"
                         + " where p.Id_Planta = " + drpplan.SelectedValue + " and CONVERT(date, eal.fechaRegistro) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                         + " group by l.numero, pm.NombreMedida order by l.numero asc";

            DataTable dt = new DataTable();
            dt = DataAccess.Get_DataTable(query);

            packet_chart.DataSource = dt;
            packet_chart.DataBind();

            link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)packet_chart).Chart;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link2 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Master_Report_Line";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Report-Plant.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        else
        {

        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/adminRepMedidasNuevos/NewReport3.aspx");
    }
}