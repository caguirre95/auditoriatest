﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Export;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using SistemaAuditores.DataAccess;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Web.Script.Services;
using System.Web.Services;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;

public partial class adminRepMedidasNuevos_NewReport2 : System.Web.UI.Page
{
    public class estilosClass
    {
        public string estilo { get; set; }
        public string id { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();

            cargagridestilos();

            Session["rep"] = null;
            Session["rep1"] = null;
            Session["rep2"] = null;
            Session["rep3"] = null;

        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<estilosClass> GetStyle(string pref)
    {
        try
        {
            List<estilosClass> list = new List<estilosClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = "SELECT distinct top 10  Style,p.Id_Style FROM Style s join POrder p on s.Id_Style=p.Id_Style  where Style like '%" + pref + "%'";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                estilosClass obj = new estilosClass();

                obj.estilo = dt.Rows[i][0].ToString();
                obj.id = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    void cargagridestilos()
    {
        string sqline = "select s.Style from tbMedidasEmbarqueAL eal join Style s on s.Id_Style = eal.idEstilo where DATEPART(WK, eal.fechaRegistro) = DATEPART(WK, GETDATE()) group by s.Style";

        DataTable dtEstilos = DataAccess.Get_DataTable(sqline);
        grvestilos.DataSource = dtEstilos;
        grvestilos.DataBind();
    }

    void Corte()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);
        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string query = " select p.POrder, s.Style, eal.size Talla, pm.NombreMedida, (case when ep.estado = ep.estado2 then ep.estado else (ep.estado + '-' + ep.estado2) end) Estatus"
                     + " from tbMedidasEmbarqueAL eal"
                     + " join tbMedidasEmbarqueP ep on ep.id = eal.id"
                     + " join POrder p on p.Id_Order = eal.idPorder"
                     + " join Style s on s.Id_Style = eal.idEstilo"
                     + " join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida"
                     + " where p.Id_Order = " + hdnidcorte.Value + " and CONVERT(date,eal.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                     + " order by eal.fechaRegistro asc";        

        DataTable general = DataAccess.Get_DataTable(query);
        Session["rep"] = general;
        grid1.DataSource = Session["rep"];
        grid1.DataBind();
    }

    void Estilo()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);
        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string query = " select p.POrder, s.Style, eal.size Talla, pm.NombreMedida, (case when ep.estado = ep.estado2 then ep.estado else (ep.estado + '-' + ep.estado2) end) Estatus"
                     + " from tbMedidasEmbarqueAL eal"
                     + " join tbMedidasEmbarqueP ep on ep.id = eal.id"
                     + " join POrder p on p.Id_Order = eal.idPorder"
                     + " join Style s on s.Id_Style = eal.idEstilo"
                     + " join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida"
                     + " where s.Style = " + txtstyle.Text.Trim() + " and CONVERT(date,eal.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                     + " order by eal.fechaRegistro asc";

        DataTable general = DataAccess.Get_DataTable(query);
        Session["rep"] = general;
        grid1.DataSource = Session["rep"];
        grid1.DataBind();
    }

    void Fecha()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);
        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string query = " select p.POrder, s.Style, eal.size Talla, pm.NombreMedida, (case when ep.estado = ep.estado2 then ep.estado else (ep.estado + '-' + ep.estado2) end) Estatus"
                     + " from tbMedidasEmbarqueAL eal"
                     + " join tbMedidasEmbarqueP ep on ep.id = eal.id"
                     + " join POrder p on p.Id_Order = eal.idPorder"
                     + " join Style s on s.Id_Style = eal.idEstilo"
                     + " join tblPuntosMedida pm on pm.idPuntosM = ep.idPuntoMedida"
                     + " where CONVERT(date,eal.fechaRegistro) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                     + " order by eal.fechaRegistro asc";

        DataTable general = DataAccess.Get_DataTable(query);
        Session["rep"] = general;
        grid1.DataSource = Session["rep"];
        grid1.DataBind();
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdnidcorte.Value != "" && txtcorte.Text != "")
            {
                Corte();
            }
            else
            if (txtstyle.Text == string.Empty)
            {
                Fecha();
            }
            else
            {
                Estilo();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnclean_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/adminRepMedidasNuevos/NewReport2.aspx");
    }

    protected void grid1_DataBinding(object sender, EventArgs e)
    {
        grid1.DataSource = Session["rep"];
    }

    protected void grid1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grid1.DataBind();
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridViewExporter1.GridViewID = "grid1";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreatePageForEachLink();
            //compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Report_Empaque - " + DateTime.Now.ToShortDateString();
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=ReportMeasurement.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {

    }

    protected void lnkCorte_Click(object sender, EventArgs e)
    {
        try
        {
            //if (txtstyle.Text == string.Empty)
            //{
            //    carga();
            //}
            //else
            //{
            //    cargaStyle();
            //}
        }
        catch (Exception)
        {
            throw;
        }
    }
}