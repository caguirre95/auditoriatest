﻿using DevExpress.Export;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;

public partial class adminRepMedidasNuevos_ReportMedidaEmpaque : System.Web.UI.Page
{
    public class estilosClass
    {
        public string estilo { get; set; }
        public string id { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        SideBySideBarSeriesLabel label = (SideBySideBarSeriesLabel)WebChartControl1.SeriesTemplate.Label;


        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();

            visible();
            cargacombo();
            cargagridestilos();

            Session["rep"] = null;
            Session["rep1"] = null;
            Session["rep2"] = null;
            Session["rep3"] = null;


            chbShowLabels.Checked = WebChartControl1.SeriesTemplate.LabelsVisibility == DefaultBoolean.True;
            spnLabelIndent.Value = label.Indent;

        }

        bool indentEnabled = chbShowLabels.Checked;
        spnLabelIndent.ClientEnabled = indentEnabled;
        WebChartControl1.CrosshairOptions.CrosshairLabelMode = CrosshairLabelMode.ShowForNearestSeries;

    }



    void PerformShowLabelsAction()
    {
        WebChartControl1.SeriesTemplate.LabelsVisibility = chbShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False;
        WebChartControl1.CrosshairEnabled = chbShowLabels.Checked ? DefaultBoolean.False : DefaultBoolean.True;
    }

    void PerformLabelIndentAction()
    {
        ((BarSeriesLabel)WebChartControl1.SeriesTemplate.Label).Indent = Convert.ToInt32(spnLabelIndent.Value);
    }

    protected void WebChartControl1_CustomCallback(object sender, CustomCallbackEventArgs e)
    {
        if (e.Parameter == "ShowLabels")
            PerformShowLabelsAction();
        else if (e.Parameter == "LabelIndent")
            PerformLabelIndentAction();

        WebChartControl1.DataBind();
    }

    void visible()
    {
        //  ASPxGridView2.Visible = false;
        btnexport1.Visible = false;

        //ASPxGridView1.Visible = true;
        btnexport.Visible = true;

    }

    void nvisible()
    {
        //   ASPxGridView2.Visible = true;
        btnexport1.Visible = true;

        //ASPxGridView1.Visible = false;
        btnexport.Visible = false;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<estilosClass> GetStyle(string pref)
    {
        try
        {
            List<estilosClass> list = new List<estilosClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = "SELECT distinct top 10  Style,p.Id_Style FROM Style s join POrder p on s.Id_Style=p.Id_Style  where Style like '%" + pref + "%'";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                estilosClass obj = new estilosClass();

                obj.estilo = dt.Rows[i][0].ToString();
                obj.id = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    void cargacombo()
    {
        string sqline = "select id_linea,numero from Linea l where l.numero not like '%-%' and l.numero not like '%A%' order by CONVERT(float,numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta,descripcion FROM Planta order by descripcion";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
        DropDownListPlant.Items.Insert(6, "All Plant");

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        visible();

        WebChartControl1.DataBind();

        //ASPxGridView1.DataBind();
    }

    void cargagridestilos()
    {//Cambio
        string sqline = " select s.Style"
                       + " from Style s"
                       + " join POrder p on s.Id_Style = p.Id_Style"
                       + " join tbCajasInspeccion ci on ci.idPorder = p.Id_Order"
                       + " join tbEstiloTalla et on et.idEstiloTalla = ci.idEstiloTalla"
                       + " join tbEspecificacion e on e.idEstiloTalla = et.idEstiloTalla"
                       + " join tblPuntosMedida pm on pm.idPuntosM = e.idPuntoDeMedida"
                       + " where datepart(wk, ci.fecha) = DATEPART(WK, GETDATE())"
                       + " group by s.Style";

        DataTable dtEstilos = DataAccess.Get_DataTable(sqline);

        grvestilos.DataSource = dtEstilos;
        grvestilos.DataBind();
    }

    DataTable funcionCarga()
    {//Formulario crear 
        DateTime dtime = DateTime.Parse(txtfecha1.Text);

        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string resp = RadioButtonList1.SelectedValue;

        string query = "";

        switch (resp)
        {
            case "1"://cut
                if (txtPorder.Text != string.Empty)
                {//Cambio

                    query = "select pm.NombreMedida,case mm.texto when 'Big' then mm.valor-e.TolMax when 'Small' then mm.valor-e.Tolmin when 'In Spec' then 0 END as valor,count(case mm.texto when 'Big' then 1 when 'Small' then 1 when 'In Spec' then 1 END) as total"
                         + " from tbCajasInspeccion ci"
                         + " join tbMuestrasMedidas mm on ci.id = mm.id"
                         + " join tbEspecificacion e on mm.idespecificacion = e.idEspecificacion"
                         + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                         + " join POrder p on ci.idPorder = p.Id_Order"
                         + " where p.POrder = '" + txtPorder.Text.TrimEnd() + "'"
                         + " group by pm.NombreMedida ,case mm.texto when 'Big' then mm.valor - e.TolMax when 'Small' then mm.valor - e.Tolmin when 'In Spec' then 0 END"
                         + " order by  case mm.texto when 'Big' then mm.valor - e.TolMax when 'Small' then mm.valor - e.Tolmin when 'In Spec' then 0 END desc";
                }
                else
                {
                    resp = "fin";
                }
                break;
            case "2"://style
                if (txtstyle.Text != string.Empty)
                {//Cambio
                    //query = "select pm.NombreMedida,case mm.texto when 'Big' then mm.valor-e.TolMax when 'Small' then mm.valor-e.Tolmin when 'In Speck' then 0 END as valor,count(case mm.texto when 'Big' then 1 when 'Small' then 1 when 'In Speck' then 1 END) as total"
                    //     + " from tbCajasInspeccion ci"
                    //     + " join tbMuestrasMedidas mm on ci.id = mm.id"
                    //     + " join tbEspecificacion e on mm.idespecificacion = e.idEspecificacion"
                    //     + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                    //     + " join POrder p on ci.idPorder = p.Id_Order"
                    //     + " where p.Id_Style = '" + txtstyle.Text + "' and CONVERT(date, ci.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                    //     + " group by pm.NombreMedida ,case mm.texto when 'Big' then mm.valor - e.TolMax when 'Small' then mm.valor - e.Tolmin when 'In Speck' then 0 END"
                    //     + " order by  case mm.texto when 'Big' then mm.valor - e.TolMax when 'Small' then mm.valor - e.Tolmin when 'In Speck' then 0 END desc";
                    query = " select pm.NombreMedida,case mm.texto when 'Big' then mm.valor-e.TolMax when 'Small' then mm.valor-e.Tolmin when 'In Spec' then 0 END as valor,count(case mm.texto when 'Big' then 1 when 'Small' then 1 when 'In Spec' then 1 END) as total"
                           + " from tbCajasInspeccion ci"
                           + " join tbMuestrasMedidas mm on ci.id = mm.id"
                           + " join tbEspecificacion e on mm.idespecificacion = e.idEspecificacion"
                           + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                           + " join POrder p on ci.idPorder = p.Id_Order"
                           + " join Style s on s.Id_Style = p.Id_Style"
                           + " where s.Style = '" + txtstyle.Text + "' and CONVERT(date, ci.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                           + " group by pm.NombreMedida ,case mm.texto when 'Big' then mm.valor - e.TolMax when 'Small' then mm.valor - e.Tolmin when 'In Spec' then 0 END"
                           + " order by  case mm.texto when 'Big' then mm.valor - e.TolMax when 'Small' then mm.valor - e.Tolmin when 'In Spec' then 0 END desc";
                }
                else
                {
                    resp = "fin";
                }
                break;
            case "3"://line
                if (DropDownListLine.SelectedItem.Text != "Select...")
                {//Tiro
                    query = " select pm.NombreMedida,case mm.texto when 'Big' then mm.valor-e.TolMax when 'Small' then mm.valor-e.Tolmin when 'In Speck' then 0 END as valor,count(case mm.texto when 'Big' then 1 when 'Small' then 1 when 'In Speck' then 1 END) as total"
                         + " from tbCajasInspeccion ci"
                         + " join tbMuestrasMedidas mm on ci.id = mm.id"
                         + " join tbEspecificacion e on mm.idespecificacion = e.idEspecificacion"
                         + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                         + " join POrder p on ci.idPorder = p.Id_Order"
                         + " where p.Id_Linea = " + DropDownListLine.SelectedValue + " and CONVERT(date, ci.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                         + " group by pm.NombreMedida ,case mm.texto when 'Big' then mm.valor - e.TolMax when 'Small' then mm.valor - e.Tolmin when 'In Speck' then 0 END"
                         + " order by  case mm.texto when 'Big' then mm.valor - e.TolMax when 'Small' then mm.valor - e.Tolmin when 'In Speck' then 0 END desc";

                }
                else
                {
                    resp = "fin";
                }
                break;
            case "4"://plant
                if (DropDownListPlant.SelectedItem.Text == "Select...")
                {
                    resp = "fin";
                }
                else if (DropDownListPlant.SelectedItem.Text == "All Plant")
                {
                    query = "select pm.NombreMedida,case mm.texto when 'Big' then mm.valor-e.TolMax when 'Small' then mm.valor-e.Tolmin when 'In Speck' then 0 END as valor,"
                          + " count(case mm.texto when 'Big' then 1 when 'Small' then 1 when 'In Speck' then 1 END) as total"
                          + " from tbCajasInspeccion ci"
                          + " join tbMuestrasMedidas mm on ci.id = mm.id"
                          + " join tbEspecificacion e on mm.idespecificacion = e.idEspecificacion"
                          + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                          + " join POrder p on ci.idPorder = p.Id_Order"
                          + " join Linea l on p.Id_Linea = l.id_linea"
                          + " where CONVERT(date, ci.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                          + " group by pm.NombreMedida ,case mm.texto when 'Big' then mm.valor - e.TolMax when 'Small' then mm.valor - e.Tolmin when 'In Speck' then 0 END"
                          + " order by  case mm.texto when 'Big' then mm.valor - e.TolMax when 'Small' then mm.valor - e.Tolmin when 'In Speck' then 0 END desc";
                } 
                else
                {
                    query =   " select pm.NombreMedida,case mm.texto when 'Big' then mm.valor-e.TolMax when 'Small' then mm.valor-e.Tolmin when 'In Speck' then 0 END as valor,count(case mm.texto when 'Big' then 1 when 'Small' then 1 when 'In Speck' then 1 END) as total"
                            + " from tbCajasInspeccion ci"
                            + " join tbMuestrasMedidas mm on ci.id = mm.id"
                            + " join tbEspecificacion e on mm.idespecificacion = e.idEspecificacion"
                            + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                            + " join POrder p on ci.idPorder = p.Id_Order"
                            + " join Linea l on p.Id_Linea = l.id_linea"
                            + " where l.id_planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date, ci.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                            + " group by pm.NombreMedida ,case mm.texto when 'Big' then mm.valor - e.TolMax when 'Small' then mm.valor - e.Tolmin when 'In Speck' then 0 END"
                            + " order by  case mm.texto when 'Big' then mm.valor - e.TolMax when 'Small' then mm.valor - e.Tolmin when 'In Speck' then 0 END desc";
                }
                break;
            case "":
                resp = "fin";
                break;
            default:
                resp = "fin";
                break;
        }

        DataTable dt = new DataTable();
        if (resp != "fin")
        {
            //carga comentario del corte
            if (txtPorder.Text != "")
            {
                DataTable dt2 = new DataTable();
                dt2 = DataAccess.Get_DataTable("select p.comments from POrder p where p.POrder='" + txtPorder.Text + "'");

                foreach (DataRow item in dt2.Rows)
                {
                    memo.Text = item[0].ToString();
                }

            }

            //Extrae la informacion de medidas del cut
            dt = new DataTable();
            dt = DataAccess.Get_DataTable(query);

            return dt;
        }
        else
        {
            //debe seleccionar una opcion
            dt = null;
            return dt;
        }

    }

    protected void ASPxGridView1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        //ASPxGridView1.DataBind();
    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        DataTable dt_seccion = new DataTable();

        dt_seccion.Columns.Add("PUNTOS_MEDIDA", typeof(string));
        dt_seccion.Columns.Add("WAIST", typeof(int));
        dt_seccion.Columns.Add("%WAIST", typeof(string));
        dt_seccion.Columns.Add("INSEAM", typeof(int));
        dt_seccion.Columns.Add("%INSEAM", typeof(string));
        dt_seccion.Columns.Add("FRONT_RISE", typeof(int));
        dt_seccion.Columns.Add("%FRONT_RISE", typeof(string));
        dt_seccion.Columns.Add("BACK_RISE", typeof(int));
        dt_seccion.Columns.Add("%BACK_RISE", typeof(string));
        dt_seccion.Columns.Add("HI_HIP", typeof(int));
        dt_seccion.Columns.Add("%HI_HIP", typeof(string));
        dt_seccion.Columns.Add("MUSLO", typeof(int));
        dt_seccion.Columns.Add("%MUSLO", typeof(string));
        //dt_seccion.Columns.Add("LOW_HIP", typeof(int));
        //dt_seccion.Columns.Add("%LOW_HIP", typeof(string));


        DataTable dt = new DataTable();

        string queryM = "select medida from tblMedidas order by valor desc";

        dt = DataAccess.Get_DataTable(queryM);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            DataRow row2 = dt_seccion.NewRow();

            row2[0] = dt.Rows[i][0];
            dt_seccion.Rows.Add(row2);

        }

        dt = new DataTable();
        dt = funcionCarga();

        int n1 = 0, n2 = 0, n3 = 0, n4 = 0, n5 = 0, n6 = 0, n7 = 0;

        List<listatotal> list = new List<listatotal>();
        listatotal obj = new listatotal();
        listatotal obj1 = new listatotal();
        listatotal obj2 = new listatotal();

        List<listatotal> list3_4 = new List<listatotal>();
        listatotal obj3_4 = new listatotal();
        listatotal obj3_41 = new listatotal();
        listatotal obj3_42 = new listatotal();

        List<listatotal> list3_8 = new List<listatotal>();
        listatotal obj3_8 = new listatotal();
        listatotal obj3_81 = new listatotal();
        listatotal obj3_82 = new listatotal();

        List<listatotal> list1_2 = new List<listatotal>();
        listatotal obj1_2 = new listatotal();
        listatotal obj1_21 = new listatotal();
        listatotal obj1_22 = new listatotal();

        obj.r = "Over 1''";
        obj1.r = "In Speck";
        obj2.r = "Below -1'' ";

        obj3_4.r = "Over 3/4''";
        obj3_41.r = "In Speck";
        obj3_42.r = "Below -3/4''";

        obj3_8.r = "Over 3/8''";
        obj3_81.r = "In Speck";
        obj3_82.r = "Below -3/8''";

        obj1_2.r = "Over 1/2''";
        obj1_21.r = "In Speck";
        obj1_22.r = "Below -1/2''";

        if (dt != null)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][2].Equals("WAIST"))
                {
                    n1 = n1 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalw = obj.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalw = obj1.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalw = obj2.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalw = obj3_4.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalw = obj3_41.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalw = obj3_42.totalw + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("INSEAM"))
                {
                    n2 = n2 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalI = obj.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalI = obj1.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalI = obj2.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.5)
                    {
                        obj1_2.totalI = obj1_2.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.5 && float.Parse(dt.Rows[i][3].ToString()) >= -0.5)
                    {
                        obj1_21.totalI = obj1_21.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.5)
                    {
                        obj1_22.totalI = obj1_22.totalI + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("FRONT RISE"))
                {
                    n3 = n3 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalF = obj.totalF + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalF = obj1.totalF + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalF = obj2.totalF + Convert.ToInt32(dt.Rows[i][1]);

                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.375)
                    {
                        obj3_8.totalF = obj3_8.totalF + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.375 && float.Parse(dt.Rows[i][3].ToString()) >= -0.375)
                    {
                        obj3_81.totalF = obj3_81.totalF + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.375)
                    {
                        obj3_82.totalF = obj3_82.totalF + Convert.ToInt32(dt.Rows[i][1]);
                    }

                }
                else if (dt.Rows[i][2].Equals("BACK RISE"))
                {
                    n4 = n4 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalB = obj.totalB + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalB = obj1.totalB + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalB = obj2.totalB + Convert.ToInt32(dt.Rows[i][1]);

                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.375)
                    {
                        obj3_8.totalB = obj3_8.totalB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.375 && float.Parse(dt.Rows[i][3].ToString()) >= -0.375)
                    {
                        obj3_81.totalB = obj3_81.totalB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.375)
                    {
                        obj3_82.totalB = obj3_82.totalB + Convert.ToInt32(dt.Rows[i][1]);
                    }
                }
                else if (dt.Rows[i][2].Equals("HI HIP"))
                {
                    n5 = n5 + Convert.ToInt32(dt.Rows[i][1]);

                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalH = obj.totalH + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalH = obj1.totalH + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalH = obj2.totalH + Convert.ToInt32(dt.Rows[i][1]);

                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                    {
                        obj3_4.totalH = obj3_4.totalH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                    {
                        obj3_41.totalH = obj3_41.totalH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                    {
                        obj3_42.totalH = obj3_42.totalH + Convert.ToInt32(dt.Rows[i][1]);
                    }
                }
                else if (dt.Rows[i][2].Equals("MUSLO"))
                {
                    n6 = n6 + Convert.ToInt32(dt.Rows[i][1]);
                    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                    {
                        obj.totalM = obj.totalM + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                    {
                        obj1.totalM = obj1.totalM + Convert.ToInt32(dt.Rows[i][1]);

                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                    {
                        obj2.totalM = obj2.totalM + Convert.ToInt32(dt.Rows[i][1]);

                    }

                    if (float.Parse(dt.Rows[i][3].ToString()) > 0.375)
                    {
                        obj3_8.totalM = obj3_8.totalM + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.375 && float.Parse(dt.Rows[i][3].ToString()) >= -0.375)
                    {
                        obj3_81.totalM = obj3_81.totalM + Convert.ToInt32(dt.Rows[i][1]);
                    }
                    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.375)
                    {
                        obj3_82.totalM = obj3_82.totalM + Convert.ToInt32(dt.Rows[i][1]);
                    }
                }
                //else if (dt.Rows[i][2].Equals("LOW HIP"))
                //{
                //    n7 = n7 + Convert.ToInt32(dt.Rows[i][1]);

                //    if (float.Parse(dt.Rows[i][3].ToString()) > 1)
                //    {
                //        obj.totalH2 = obj.totalH2 + Convert.ToInt32(dt.Rows[i][1]);

                //    }
                //    else if (float.Parse(dt.Rows[i][3].ToString()) <= 1 && float.Parse(dt.Rows[i][3].ToString()) >= -1)
                //    {
                //        obj1.totalH2 = obj1.totalH2 + Convert.ToInt32(dt.Rows[i][1]);

                //    }
                //    else if (float.Parse(dt.Rows[i][3].ToString()) < -1)
                //    {
                //        obj2.totalH2 = obj2.totalH2 + Convert.ToInt32(dt.Rows[i][1]);

                //    }

                //    if (float.Parse(dt.Rows[i][3].ToString()) > 0.75)
                //    {
                //        obj3_4.totalH2 = obj3_4.totalH2 + Convert.ToInt32(dt.Rows[i][1]);
                //    }
                //    else if (float.Parse(dt.Rows[i][3].ToString()) <= 0.75 && float.Parse(dt.Rows[i][3].ToString()) >= -0.75)
                //    {
                //        obj3_41.totalH2 = obj3_41.totalH2 + Convert.ToInt32(dt.Rows[i][1]);
                //    }
                //    else if (float.Parse(dt.Rows[i][3].ToString()) < -0.75)
                //    {
                //        obj3_42.totalH2 = obj3_42.totalH2 + Convert.ToInt32(dt.Rows[i][1]);
                //    }

                //}
            }

            obj.procenW = ((Convert.ToSingle(obj.totalw) / n1) * 100).ToString("0.00") + "% ";
            obj1.procenW = ((Convert.ToSingle(obj1.totalw) / n1) * 100).ToString("0.00") + "% ";
            obj2.procenW = ((Convert.ToSingle(obj2.totalw) / n1) * 100).ToString("0.00") + "% ";

            obj.procenI = ((Convert.ToSingle(obj.totalI) / n2) * 100).ToString("0.00") + "% ";
            obj1.procenI = ((Convert.ToSingle(obj1.totalI) / n2) * 100).ToString("0.00") + "% ";
            obj2.procenI = ((Convert.ToSingle(obj2.totalI) / n2) * 100).ToString("0.00") + "% ";

            obj.procenF = ((Convert.ToSingle(obj.totalF) / n3) * 100).ToString("0.00") + "% ";
            obj1.procenF = ((Convert.ToSingle(obj1.totalF) / n3) * 100).ToString("0.00") + "% ";
            obj2.procenF = ((Convert.ToSingle(obj2.totalF) / n3) * 100).ToString("0.00") + "% ";

            obj.procenB = ((Convert.ToSingle(obj.totalB) / n4) * 100).ToString("0.00") + "%";
            obj1.procenB = ((Convert.ToSingle(obj1.totalB) / n4) * 100).ToString("0.00") + "% ";
            obj2.procenB = ((Convert.ToSingle(obj2.totalB) / n4) * 100).ToString("0.00") + "% ";

            obj.procenH = ((Convert.ToSingle(obj.totalH) / n5) * 100).ToString("0.00") + "% ";
            obj1.procenH = ((Convert.ToSingle(obj1.totalH) / n5) * 100).ToString("0.00") + "% ";
            obj2.procenH = ((Convert.ToSingle(obj2.totalH) / n5) * 100).ToString("0.00") + "% ";

            obj.procenM = ((Convert.ToSingle(obj.totalM) / n6) * 100).ToString("0.00") + "% ";
            obj1.procenM = ((Convert.ToSingle(obj1.totalM) / n6) * 100).ToString("0.00") + "% ";
            obj2.procenM = ((Convert.ToSingle(obj2.totalM) / n6) * 100).ToString("0.00") + "% ";

            //obj.procenH2 = ((Convert.ToSingle(obj.totalH2) / n7) * 100).ToString("0.00") + "% ";
            //obj1.procenH2 = ((Convert.ToSingle(obj1.totalH2) / n7) * 100).ToString("0.00") + "% ";
            //obj2.procenH2 = ((Convert.ToSingle(obj2.totalH2) / n7) * 100).ToString("0.00") + "% ";

            list.Add(obj);

            list.Add(obj1);

            list.Add(obj2);


            obj3_4.procenW = ((Convert.ToSingle(obj3_4.totalw) / n1) * 100).ToString("0.00") + "% ";
            obj3_41.procenW = ((Convert.ToSingle(obj3_41.totalw) / n1) * 100).ToString("0.00") + "% ";
            obj3_42.procenW = ((Convert.ToSingle(obj3_42.totalw) / n1) * 100).ToString("0.00") + "% ";

            obj1_2.procenI = ((Convert.ToSingle(obj1_2.totalI) / n2) * 100).ToString("0.00") + "% ";
            obj1_21.procenI = ((Convert.ToSingle(obj1_21.totalI) / n2) * 100).ToString("0.00") + "% ";
            obj1_22.procenI = ((Convert.ToSingle(obj1_22.totalI) / n2) * 100).ToString("0.00") + "% ";

            obj3_8.procenF = ((Convert.ToSingle(obj3_8.totalF) / n3) * 100).ToString("0.00") + "% ";
            obj3_81.procenF = ((Convert.ToSingle(obj3_81.totalF) / n3) * 100).ToString("0.00") + "% ";
            obj3_82.procenF = ((Convert.ToSingle(obj3_82.totalF) / n3) * 100).ToString("0.00") + "% ";

            obj3_8.procenB = ((Convert.ToSingle(obj3_8.totalB) / n4) * 100).ToString("0.00") + "% ";
            obj3_81.procenB = ((Convert.ToSingle(obj3_81.totalB) / n4) * 100).ToString("0.00") + "% ";
            obj3_82.procenB = ((Convert.ToSingle(obj3_82.totalB) / n4) * 100).ToString("0.00") + "% ";

            obj3_8.procenM = ((Convert.ToSingle(obj3_8.totalM) / n6) * 100).ToString("0.00") + "% ";
            obj3_81.procenM = ((Convert.ToSingle(obj3_81.totalM) / n6) * 100).ToString("0.00") + "% ";
            obj3_82.procenM = ((Convert.ToSingle(obj3_82.totalM) / n6) * 100).ToString("0.00") + "% ";

            obj3_4.procenH = ((Convert.ToSingle(obj3_4.totalH) / n5) * 100).ToString("0.00") + "% ";
            obj3_41.procenH = ((Convert.ToSingle(obj3_41.totalH) / n5) * 100).ToString("0.00") + "% ";
            obj3_42.procenH = ((Convert.ToSingle(obj3_42.totalH) / n5) * 100).ToString("0.00") + "% ";

            //obj3_4.procenH2 = ((Convert.ToSingle(obj3_4.totalH2) / n7) * 100).ToString("0.00") + "% ";
            //obj3_41.procenH2 = ((Convert.ToSingle(obj3_41.totalH2) / n7) * 100).ToString("0.00") + "% ";
            //obj3_42.procenH2 = ((Convert.ToSingle(obj3_42.totalH2) / n7) * 100).ToString("0.00") + "% ";

            list3_4.Add(obj3_4);
            list3_4.Add(obj3_41);
            list3_4.Add(obj3_42);

            list1_2.Add(obj1_2);
            list1_2.Add(obj1_21);
            list1_2.Add(obj1_22);

            list3_8.Add(obj3_8);
            list3_8.Add(obj3_81);
            list3_8.Add(obj3_82);


            //carga grid Pincipal con Porcentajes
            //for (int i = 0; i < dt_seccion.Rows.Count; i++)
            //{
            //    for (int j = 0; j < dt.Rows.Count; j++)
            //    {
            //        if (dt.Rows[j][2].Equals("WAIST"))
            //        {
            //            if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
            //            {
            //                dt_seccion.Rows[i][1] = dt.Rows[j][1];
            //                dt_seccion.Rows[i][2] = ((float.Parse(dt.Rows[j][1].ToString()) / n1) * 100).ToString("0.00") + "%";
            //            }
            //        }
            //        else if (dt.Rows[j][2].Equals("INSEAM"))
            //        {
            //            if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
            //            {
            //                dt_seccion.Rows[i][3] = dt.Rows[j][1];
            //                dt_seccion.Rows[i][4] = ((float.Parse(dt.Rows[j][1].ToString()) / n2) * 100).ToString("0.00") + "%";
            //            }
            //        }
            //        else if (dt.Rows[j][2].Equals("FRONT RISE"))
            //        {
            //            if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
            //            {
            //                dt_seccion.Rows[i][5] = dt.Rows[j][1];
            //                dt_seccion.Rows[i][6] = ((float.Parse(dt.Rows[j][1].ToString()) / n3) * 100).ToString("0.00") + "%";
            //            }
            //        }
            //        else if (dt.Rows[j][2].Equals("BACK RISE"))
            //        {
            //            if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
            //            {
            //                dt_seccion.Rows[i][7] = dt.Rows[j][1];
            //                dt_seccion.Rows[i][8] = ((float.Parse(dt.Rows[j][1].ToString()) / n4) * 100).ToString("0.00") + "%";
            //            }
            //        }
            //        else if (dt.Rows[j][2].Equals("HI HIP"))
            //        {
            //            if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
            //            {
            //                dt_seccion.Rows[i][9] = dt.Rows[j][1];
            //                dt_seccion.Rows[i][10] = ((float.Parse(dt.Rows[j][1].ToString()) / n5) * 100).ToString("0.00") + "%";
            //            }
            //        }
            //        else if (dt.Rows[j][2].Equals("MUSLO"))
            //        {
            //            if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
            //            {
            //                dt_seccion.Rows[i][11] = dt.Rows[j][1];
            //                dt_seccion.Rows[i][12] = ((float.Parse(dt.Rows[j][1].ToString()) / n6) * 100).ToString("0.00") + "%";
            //            }
            //        }
            //        //else if (dt.Rows[j][2].Equals("LOW HIP"))
            //        //{
            //        //    if (dt_seccion.Rows[i][0].Equals(dt.Rows[j][0]))
            //        //    {
            //        //        dt_seccion.Rows[i][13] = dt.Rows[j][1];
            //        //        dt_seccion.Rows[i][14] = ((float.Parse(dt.Rows[j][1].ToString()) / n7) * 100).ToString("0.00") + "%";
            //        //    }
            //        //}
            //    }
            //}


        }

        WebChartControl1.DataBind();

        // ASPxGridView1.DataSource = dt_seccion;

        Session["rep"] = list;
        Session["rep1"] = list1_2;
        Session["rep2"] = list3_4;
        Session["rep3"] = list3_8;

        ASPxGridView3.DataBind();
        ASPxGridView4.DataBind();
        ASPxGridView2.DataBind();
        ASPxGridView5.DataBind();

    }

    protected void btnexport_Click(object sender, EventArgs e)
    {

        try
        {
            //ASPxGridView1.DataBind();

            ASPxGridViewExporter1.GridViewID = "ASPxGridView1";
            ASPxGridViewExporter2.GridViewID = "ASPxGridView3";
            ASPxGridViewExporter3.GridViewID = "ASPxGridView4";
            ASPxGridViewExporter4.GridViewID = "ASPxGridView2";
            ASPxGridViewExporter5.GridViewID = "ASPxGridView5";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;
            link1.CreateMarginalHeaderArea += new CreateAreaEventHandler(Link_CreateMarginalHeaderArea);

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link2.Component = ASPxGridViewExporter2;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = ASPxGridViewExporter3;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link4 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link4.Component = ASPxGridViewExporter4;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link5 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link5.Component = ASPxGridViewExporter5;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1/*, link2, link3, link4, link5*/ });

            compositeLink.CreatePageForEachLink();
            //compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Report_Empaque";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=ReportMeasurement.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            throw;
        }

    }

    private void Link_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
    {
        string resp = RadioButtonList1.SelectedValue;
        string mess = "";
        if (resp == "1")
        {
            mess = txtPorder.Text + "\n" + memo.Text;
        }
        else
        {
            mess = memo.Text;
        }

        TextBrick brick;
        brick = e.Graph.DrawString(mess, Color.Navy, new RectangleF(0, 0, 700, 50), DevExpress.XtraPrinting.BorderSide.None);
        brick.Font = new Font("Times New Roman", 12);
        brick.StringFormat = new BrickStringFormat(StringAlignment.Center);

        // string reps="31313131311sdsfsdf";
        //PageInfoBrick brick = e.Graph.DrawPageInfo(PageInfo., "", Color.DarkBlue,new RectangleF(0, 0, 100, 20), BorderSide.None);

        //brick.LineAlignment = BrickAlignment.Center;

        //brick.Alignment = BrickAlignment.Center;

        //brick.AutoWidth = true;

    }

    protected void btnexport1_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridViewExporter1.GridViewID = "ASPxGridView2";
            ASPxGridViewExporter1.FileName = "Report " + DateTime.Now.ToShortDateString();

            ASPxGridViewExporter1.WriteXlsToResponse(new XlsExportOptionsEx { ExportType = ExportType.WYSIWYG });

        }
        catch (Exception)
        {

            throw;
        }
    }

    public class listatotal
    {
        public string r { get; set; }
        public int totalw { get; set; }
        public string procenW { get; set; }
        public int totalI { get; set; }
        public string procenI { get; set; }
        public int totalF { get; set; }
        public string procenF { get; set; }
        public int totalB { get; set; }
        public string procenB { get; set; }
        public int totalM { get; set; }
        public string procenM { get; set; }
        public int totalH { get; set; }
        public string procenH { get; set; }
        //public int totalH2 { get; set; }
        //public string procenH2 { get; set; }

    }

    protected void ASPxGridView3_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView3.DataSource = (List<listatotal>)Session["rep"];
    }

    protected void ASPxGridView3_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView3.DataBind();
    }

    protected void ASPxGridView4_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView4.DataSource = (List<listatotal>)Session["rep2"];
    }

    protected void ASPxGridView4_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView4.DataBind();
    }

    protected void ASPxGridView2_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView2.DataSource = (List<listatotal>)Session["rep3"];
    }

    protected void ASPxGridView2_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView2.DataBind();
    }

    protected void ASPxGridView5_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView5.DataSource = (List<listatotal>)Session["rep1"];
    }

    protected void ASPxGridView5_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView5.DataBind();
    }

    protected void WebChartControl1_DataBinding(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();

        dt = funcionCarga2();

        WebChartControl1.DataSource = dt;

    }

    DataTable funcionCarga2()
    {
        DateTime dtime = DateTime.Parse(txtfecha1.Text);

        DateTime dtime1 = DateTime.Parse(txtfecha2.Text);

        string resp = RadioButtonList1.SelectedValue;

        string query = "";

        switch (resp)
        {
            case "1"://cut
                if (txtPorder.Text != string.Empty)
                {
                    query = "select pm.NombreMedida,m.texto,COUNT(m.texto) as total"
                            + " from tbCajasInspeccion c"
                            + " join tbMuestrasMedidas m on c.id = m.id"
                            + " join tbEspecificacion e on m.idespecificacion = e.idEspecificacion"
                            + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                            + " join POrder p on p.Id_Order = c.idPorder"
                            + " where p.POrder='" + txtPorder.Text + "'"
                            + " group by pm.idPuntosM , pm.NombreMedida,m.texto order by pm.idPuntosM";
                }
                else
                {
                    resp = "fin";
                }
                break;
            case "2"://style
                if (txtstyle.Text != string.Empty)
                {
                    query = "select pm.NombreMedida,m.texto,COUNT(m.texto)as total"
                            + " from tbCajasInspeccion c"
                            + " join tbMuestrasMedidas m on c.id = m.id"
                            + " join tbEspecificacion e on m.idespecificacion = e.idEspecificacion"
                            + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                            + " join POrder p on p.Id_Order = c.idPorder"
                            + " join Style s on p.Id_Style = s.Id_Style"
                            + " where s.Style = '" + txtstyle.Text + "' and CONVERT(date, c.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                            + " group by pm.NombreMedida,m.texto";

                }
                else
                {
                    resp = "fin";
                }
                break;
            case "3"://line
                if (DropDownListLine.SelectedItem.Text != "Select...")
                {//Tiro
                    query = "select pm.NombreMedida,m.texto,COUNT(m.texto)as total"
                            + " from tbCajasInspeccion c"
                            + " join tbMuestrasMedidas m on c.id = m.id"
                            + " join tbEspecificacion e on m.idespecificacion = e.idEspecificacion"
                            + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                            + " join POrder p on p.Id_Order = c.idPorder"
                            + " where p.Id_Linea = '" + DropDownListLine.SelectedValue + "' and  CONVERT(date,c.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                            + " group by pm.idPuntosM , pm.NombreMedida,m.texto"
                            + " order by pm.idPuntosM";

                }
                else
                {
                    resp = "fin";
                }
                break;
            case "4"://plant
                if (DropDownListPlant.SelectedItem.Text == "Select...")
                {
                    resp = "fin";
                }
                else if (DropDownListPlant.SelectedItem.Text == "All Plant")
                {

                    query = "select pm.NombreMedida,m.texto,COUNT(m.texto) as total"
                            + " from tbCajasInspeccion c"
                            + " join tbMuestrasMedidas m on c.id = m.id"
                            + " join tbEspecificacion e on m.idespecificacion = e.idEspecificacion"
                            + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                            + " join POrder p on p.Id_Order = c.idPorder"
                            + " where CONVERT(date,c.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                            + " group by pm.idPuntosM , pm.NombreMedida,m.texto"
                            + " order by pm.idPuntosM";
                }
                else
                {
                    query = "select pm.NombreMedida,m.texto,COUNT(m.texto)as total"
                            + " from tbCajasInspeccion c"
                            + " join tbMuestrasMedidas m on c.id = m.id"
                            + " join tbEspecificacion e on m.idespecificacion = e.idEspecificacion"
                            + " join tblPuntosMedida pm on e.idPuntoDeMedida = pm.idPuntosM"
                            + " join POrder p on p.Id_Order = c.idPorder"
                            + " join Linea l on p.Id_Linea = l.id_linea"
                            + " where l.id_planta =" + DropDownListPlant.SelectedValue + " and  CONVERT(date,c.fecha) between '" + dtime.ToString("yyyy-MM-dd") + "' and '" + dtime1.ToString("yyyy-MM-dd") + "'"
                            + " group by pm.idPuntosM , pm.NombreMedida,m.texto"
                            + " order by pm.idPuntosM ";
                }
                break;
            case "":
                resp = "fin";
                break;
            default:
                resp = "fin";
                break;
        }

        DataTable dt = new DataTable();
        if (resp != "fin")
        {
            if (txtPorder.Text != "")
            {
                DataTable dt2 = new DataTable();
                dt2 = DataAccess.Get_DataTable("select p.comments from POrder p where p.POrder='" + txtPorder.Text + "'");

                foreach (DataRow item in dt2.Rows)
                {
                    memo.Text = item[0].ToString();
                }

            }

            dt = new DataTable();
            dt = DataAccess.Get_DataTable(query);

            return dt;
        }
        else
        {
            //debe seleccionar una opcion
            dt = null;
            return dt;
        }
    }

    protected void btnclean_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/adminRepMedidasNuevos/ReportMedidaEmpaque.aspx");
    }
}