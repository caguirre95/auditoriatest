﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using SistemaAuditores.DataAccess;
using System.Text.RegularExpressions;

public partial class Search : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtpo.Focus();
        }
    }
    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
        DataTable DT_Estilos = new DataTable();
        DataTable DT_Linea = new DataTable();
        string inspectorUN = Page.User.Identity.Name;
        DataTable dt_UserName = DataAccess.Get_DataTable("select UserId from aspnet_Users where UserName='" + inspectorUN + "'");
        string UserId = Convert.ToString(dt_UserName.Rows[0][0]);
        string id_seccion = (string) Session["idSeccion"].ToString() ?? "NA";
        DT_Estilos = DataAccess.Get_DataTable("Select Id_Style from Style where style='" + txtestilo.Text + "'");
        DT_Linea = DataAccess.Get_DataTable("select id_linea from Inspector where UserId='" + UserId + "' and id_seccion=" + id_seccion);
            int id_linea = 0;
            if (DT_Linea != null)
            {
                 id_linea = Convert.ToInt16(DT_Linea.Rows[0][0]);
            }
            else
            {
                string error = "alert('No !')";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
                return;
            }
      
        if(DT_Estilos.Rows.Count>0)
         {
                DataTable POrder_dt = new DataTable();
                //string query = "select Id_Order, POrder, Id_Style from POrder where POrder='" + txtpo.Text + "' and Id_Style ='" + DT_Estilos.Rows[0][0] + "'";
				string query = "select Id_Order, POrder, Id_Style from POrder where Id_Order=(select max(Id_Order) from POrder where POrder='" + txtpo.Text + "' and Id_Style ='" + DT_Estilos.Rows[0][0] + "')";
                POrder_dt = DataAccess.Get_DataTable(query);
                if (POrder_dt.Rows.Count > 0)
                {
                    Response.Redirect("BultoxBulto.aspx?id_order=" + POrder_dt.Rows[0][0].ToString() + "&id_seccion=" + id_seccion + "&id_linea=" + id_linea, false);
                }
                else
                {
                    txtestilo.Text = string.Empty;
                    txtpo.Text = string.Empty;
                    txtpo.Focus();
                    string error = "alert('No se encontro esta Orden, Intente Nuevamente!')";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
                    //return;
                }
         }
        else
         {
                txtestilo.Text = string.Empty;
                txtestilo.Focus();
                string error = "alert('No se encontro este Estilo, Intente Nuevamente!')";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
                //return;
         }        
        }
        catch (Exception ex)
        {
            string error = "alert('Error 300 " + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
        }
    }
}