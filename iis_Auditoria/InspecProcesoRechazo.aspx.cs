﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using SistemaAuditores.DataAccess;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using DevExpress.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;


public partial class InspecProcesoRechazo : System.Web.UI.Page
{
    int total = 0, totaldef = 0;
    double porcentaje = 0;

    /// <summary>
    /// evento 0 del formulario
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (User.Identity.IsAuthenticated == true)
            {
                string idorder = (string)Request.QueryString["idOrder"];
                string idbulto = (string)Request.QueryString["idBulto"];
                string idauditoria = (string)Request.QueryString["idAuditoria"];
                string idlinea = (string)(Request.QueryString["idLinea"]);
                string idmodulo = (string)Request.QueryString["idModulo"];

                lblcustomerval.Text = (string)Request.QueryString["customer"];
                lblstylesval.Text = (string)Request.QueryString["style"];
                lblcut.Text = (string)Request.QueryString["cut"];
                lblfechaval.Text = DateTime.Now.ToShortDateString();

                var guid = Guid.NewGuid();
                hdnguid.Value = guid.ToString();
                Session["guid"] = guid.ToString();

                if (idorder == "")
                {
                    Response.Redirect("Secciones.aspx");
                }
                Load_Data(idlinea, idmodulo, idbulto);
                cargaControl();
            }
        }
    }

    /// <summary>
    /// evento 1
    /// </summary>
    void cargaControl()
    {
        DataTable defectos_dt = new DataTable();
        defectos_dt = DataAccess.Get_DataTable("select idDefecto as idDefecto,Defecto as Defecto from tbDefectos WHERE Tipo='Sewing' and Cliente=92 order by Defecto  asc");

        DefectosEncontradosC.DataSource = defectos_dt;
        DefectosEncontradosC.DataBind();

        GridViewdefectosC.DataSource = defectos_dt;
        GridViewdefectosC.DataBind();

        string idlinea = (string)(Request.QueryString["idLinea"]);

        DataTable linea_dt = new DataTable();
        linea_dt = DataAccess.Get_DataTable(" select id_linea,numero"
                                          + " from linea l join (select l.id_planta from Linea l join planta p on l.id_planta = p.id_planta where l.id_linea = " + idlinea + ") as p"
                                          + " on l.id_planta = p.id_planta where l.estado = 'True' order by CONVERT(numeric, numero) asc");

        drplinea.DataSource = linea_dt;
        drplinea.DataTextField = "numero";
        drplinea.DataValueField = "id_linea";
        drplinea.DataBind();

    }

    /// <summary>
    /// envento 2
    /// </summary>
    public void Load_Data(string idlinea, string idmodulo, string idbulto)
    {
        btnsave.Enabled = true;

        DataTable gridInspecLinea = new DataTable();

        string query = " select row_number() OVER(ORDER BY oper.descripcion_completa) AS id,Op.codigo AS Codigo, Op.nombre as Nombre, Oper.descripcion_completa as Operacion,operop.idoperarioOpe,op.id_operario,oper.id_operacion"
                      + " from Seccion s"
                      + " join OperacionOperario operOp on s.id_seccion = operOp.id_seccion"
                      + " join Operacion oper on operOp.id_operacion = oper.id_operacion"
                      + " join Operario op on operOp.id_operario = op.id_operario"
                      + " where s.id_linea = " + idlinea + " and s.id_seccion = " + idmodulo + " and op.activo = 'true'"
                      + " order by oper.descripcion_completa asc";

        gridInspecLinea = DataAccess.Get_DataTable(query);

        DataTable dtporder = new DataTable();
        dtporder = DataAccess.Get_DataTable("select p.POrder,b.Quantity from Bundle b join POrder p on b.Id_Order=p.Id_Order where b.Id_Bundle=" + idbulto);


        var quantity = int.Parse(dtporder.Rows[0][1].ToString());
        if (quantity < 25)
        {
            txtMuestreo.Text = "5";
        }
        else if (quantity > 24)
        {
            txtMuestreo.Text = "8";
        }

        DataColumn CantidadDefectos = new DataColumn("CantidadDefectos", typeof(string));
        gridInspecLinea.Columns.Add(CantidadDefectos);

        DataTable dt_bundlerejected = new DataTable();

        for (int i = 0; i <= gridInspecLinea.Rows.Count - 1; i++)
        {
            string querysum = " select isnull(sum(ipdt.CantidadDefectos),0) as cantidad_defectos  from tbInspecProcRechazado ipr"
                             + " join tbInspecProcRechazadoDet ipdt on ipr.idBBRechazado=ipdt.idBBRechazado "
                             + " where ipr.idbulto=" + idbulto + " and ipr.idModulo=" + idmodulo + " and ipdt.idOperarioOpe=" + gridInspecLinea.Rows[i][4].ToString();

            dt_bundlerejected = DataAccess.Get_DataTable(querysum);

            if (dt_bundlerejected.Rows.Count > 0)
            {
                gridInspecLinea.Rows[i][7] = dt_bundlerejected.Rows[0][0];
            }
            else
            {
                gridInspecLinea.Rows[i][7] = 0;
            }

        }

        grv_insp_linea.DataSource = gridInspecLinea;
        grv_insp_linea.DataBind();

    }

    /// <summary>
    /// evento 3
    /// </summary>
    protected void ASPxButton8_Click(object sender, EventArgs e)
    {

        Button btn = (Button)sender;
        GridViewRow gv = (GridViewRow)(btn.NamingContainer);
        Label lbloper = (Label)gv.FindControl("lbloperario");
        Label lblcodigo = (Label)gv.FindControl("lblcodigo");
        Label lbldescripcion = (Label)gv.FindControl("lbloperacion");
        Label lbloperacionOperario = (Label)gv.FindControl("lbloperacionOperario");
        Label lblidoperacion = (Label)gv.FindControl("lblidoperacion");
        Label lblidoperario = (Label)gv.FindControl("lblidoperario");

        hdnidoperacion.Value = lblidoperacion.Text;
        hdnidoperario.Value = lblidoperario.Text;
        hdnidoperacionope.Value = lbloperacionOperario.Text;

        lbloperario.Text = lbloper.Text;
        hdnCodigo.Value = lblcodigo.Text;
        hdnOperacion.Value = lbldescripcion.Text;

        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "showModal();", true);

    }

    /// <summary>
    /// evento 4 guarda registro rechazado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            guardaRechazo(DefectosEncontradosC, false);
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "')", true);
        }
    }

    /// <summary>
    /// evento 4.. funcion guarda rechazo
    /// </summary>
    /// <param name="DefectosEncontrados"></param>
    /// <param name="ayuda"></param>
    
    void guardaRechazo(GridView DefectosEncontrados, bool ayuda)
    {
        string guid = (string)Session["guid"];
        if (hdnguid.Value == guid)
        {

            string idorder = (string)Request.QueryString["idOrder"];
            string idbulto = (string)Request.QueryString["idBulto"];
            string idauditoria = (string)Request.QueryString["idAuditoria"];
            string idlinea = (string)(Request.QueryString["idLinea"]);
            string idmodulo = (string)Request.QueryString["idModulo"];

            int idoperario, idoperacion, idopeop;

            if (ayuda)
            {
                if (hdnidoperacion2.Value!=string.Empty && hdnidoperario2.Value!= string.Empty)
                {
                    idoperacion = Convert.ToInt32(hdnidoperacion2.Value);
                    idoperario = Convert.ToInt32(hdnidoperario2.Value);
                    idopeop = 0;
                }
                else
                {
                    string mess = "Debe Cargar Correctamente el Operario ó la Operación!!";
                    string tipo = "info";
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "')", true);
                    return;
                }
            }
            else
            {
                idoperacion = Convert.ToInt32(hdnidoperacion.Value);
                idoperario = Convert.ToInt32(hdnidoperario.Value);
                idopeop = Convert.ToInt32(hdnidoperacionope.Value);

            }

            MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
            string userId = mu.ProviderUserKey.ToString();

            DataTable dt_Inspector = DataAccess.Get_DataTable("select i.id_inspector from  Inspector i  where i.UserId='" + userId + "'");
            int idinspector = Convert.ToInt16(dt_Inspector.Rows[0][0]);

            var resp = OrderDetailDA.SaveIPRejected(Convert.ToInt32(idbulto), idinspector, Convert.ToInt16(idlinea), Convert.ToInt16(idmodulo), Convert.ToInt16(txtMuestreo.Text), guid);// hdnguid.Value);

            int numAudi = 1;

            if (CheckBox1.Checked && resp > 0)
            {
                DataTable dtnumAudi = DataAccess.Get_DataTable("select isnull(MAX(dt.NumAuditoria),0) from tbInspecProcRechazadoDet dt where dt.idBBRechazado=" + resp);
                numAudi = Convert.ToInt16(dtnumAudi.Rows[0][0]) + 1;
            }

            int cont = 0;

            for (int i = 0; i <= DefectosEncontrados.Rows.Count - 1; i++)
            {
                TextBox txtcantidad = (TextBox)DefectosEncontrados.Rows[i].FindControl("txtcantidad");
                if (txtcantidad.Text != string.Empty)
                {
                    Label lbliddef = (Label)DefectosEncontrados.Rows[i].FindControl("labelIddefecto");
                    int iddefecto = Convert.ToInt16(lbliddef.Text);


                    if (resp > 0)
                    {
                        var r = OrderDetailDA.SaveIPRejectedDetail(resp, iddefecto, Convert.ToInt16(0),idopeop, idoperario, idoperacion, Convert.ToInt16(txtcantidad.Text), numAudi);

                        if (r != "OK")
                        {
                            cont++;
                        }

                    }

                }
            }


            if (cont == 0)
            {
                Load_Data(idlinea, idmodulo, idbulto);
                cargaControl();
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "hideModal();", true);

                var _guid = Guid.NewGuid();
                Session["guid"] = _guid.ToString();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "Popup", "Erroalert()", true);
            }


        }
    }

    /// <summary>
    /// evento 4.2 guarda registro rechazado de ayuda
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSaveAyuda_Click(object sender, EventArgs e)
    {
        try
        {
            guardaRechazo(GridViewdefectosC, true);
        }
        catch (Exception)
        {

            // throw;
        }
    }

    /// <summary>
    /// evento 5 reparar bulto 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            var idbulto = (string)Request.QueryString["idBulto"];
            var idmodulo = (string)Request.QueryString["idModulo"];

            var resp = OrderDetailDA.SaveIPRepair(Convert.ToInt32(idbulto), Convert.ToInt16(idmodulo));

            if (resp == "OK")
            {
                string message = "Exito!!!";
                string tipo = "success";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + message + "','" + tipo + "');", true);
                redirect(1);
            }
            else
            {
                string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
                string tipo = "error";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + message + "','" + tipo + "');", true);
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void grv_insp_linea_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            TextBox lblqdefect = (TextBox)e.Row.FindControl("txtdefectos");
            int qty1 = Int32.Parse(lblqdefect.Text);
            totaldef = qty1 + totaldef;

        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotalDef = (Label)e.Row.FindControl("lblTotaldefect");
            lblTotalDef.Text = totaldef.ToString();

        }
    }

    void redirect(int n)
    {
        string idorder = (string)Request.QueryString["idOrder"];
        string idauditoria = (string)Request.QueryString["idAuditoria"];
        string idlinea = (string)(Request.QueryString["idLinea"]);
        string idmodulo = (string)Request.QueryString["idModulo"];

        if (n == 1)
        {
            Response.Redirect("InspecProcesoAprovados.aspx?idOrder=" + idorder + "&idAuditoria=" + idauditoria + "&idLinea=" + idlinea + "&idModulo=" + idmodulo, false);
        }
        else if (n == 2)
        {
            Response.Redirect("SearchIL.aspx?idAuditoria=" + idauditoria + "&idLinea=0&idSeccion=" + idmodulo);
        }


    }

    /// <summary>
    /// redireccona a una nueva buskeda
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkNew_Click(object sender, EventArgs e)
    {
        redirect(2);
    }

    /// <summary>
    /// redirecciona a la hoja de corte
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        redirect(1);
    }


    /// <summary>
    /// guarda defectos no asignados a operarios
    /// </summary>
    /// <param name="cantidad"></param>
    /// <param name="idarea"></param>
    /// <param name="iddefecto"></param>
    /// <param name="idbulto"></param>
    /// <param name="idseccion"></param>
    /// <param name="idbiho"></param>
    /// <returns></returns>
    [WebMethod]
    public static string GuardarRechazo(int cantidad, int idarea, int iddefecto, int idbulto, int idseccion, int idbiho)
    {
        try
        {

            //int i = 0;
            //if (idbulto != 0)
            //{
            //    string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

            //    int Id_Biohorario = 0;
            //    DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio, bihorario FROM Biohorario WHERE @input between rango1 and rango2");
            //    if (Bihorarios.Rows.Count > 0)
            //    {
            //        Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);
            //    }

            //    MembershipUser mu = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            //    string userId = mu.ProviderUserKey.ToString();

            //    DataTable dt_Inspector = DataAccess.Get_DataTable("select si.idseccionInspector from  seccionInspector si  join Inspector i on si.idInspector=i.id_inspector where si.idseccion=" + idseccion + " and i.UserId='" + userId + "'");
            //    int idseccionInspector = Convert.ToInt16(dt_Inspector.Rows[0][0]);

            //    //OrderDetailDA.SaveBundlesRejected(idbulto, Convert.ToInt16(Id_Biohorario), IdAreaxDef, IdoperarioOP, idseccionInspector, Id_Muestreo, Convert.ToDateTime(lbldaterechazo.Text), Convert.ToString(lblhorarechazo.Text), Convert.ToInt16(txtcantidad.Text), ayuda);

            //    string queryUpdate = "insert AreaDefectoRechazo (idbundle,idbio,iddefecto,idarea,idseccionInsp,fechaRecibido,Cantidad,idseccion) values(" + idbulto + "," + Id_Biohorario + "," + iddefecto + ","
            //                          + idarea + "," + idseccionInspector + ",'" + DateTime.Now.ToString() + "'," + cantidad + "," + idseccion + ")";

            //    SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            //    cn.Open();
            //    SqlCommand cmd = new SqlCommand(queryUpdate, cn);
            //    i = cmd.ExecuteNonQuery();
            //    cn.Close();

            //}

            //if (i == 0)
            //{
            //    return "false";
            //}

            return "true";
        }
        catch (Exception)
        {
            return "false";
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ardef> getarea(string pre)
    {
        try
        {
            List<ardef> list = new List<ardef>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top(10) idPosicion,Posicion from tbPosicion p where p.Posicion like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ardef obj = new ardef();

                obj.Id_Area = int.Parse(dt.Rows[i][0].ToString());
                obj.Nombre = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ardef> getdefecto(string pre)
    {
        try
        {
            List<ardef> list = new List<ardef>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();

            SqlCommand cmd = new SqlCommand("select top(10) idDefecto,Defecto from tbDefectos  d where d.Defecto like '%" + pre + "%' order by Defecto asc", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ardef obj = new ardef();

                obj.Id_Defecto = int.Parse(dt.Rows[i][0].ToString());
                obj.Nombre = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ardef> getoperacion(string pre)
    {
        try
        {
            List<ardef> list = new List<ardef>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top(10) id_operacion,descripcion_completa from operacion p where p.descripcion_completa like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ardef obj = new ardef();

                obj.Id_Area = int.Parse(dt.Rows[i][0].ToString());
                obj.Nombre = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class ardef
    {
        public int Id_Area { get; set; }
        public string Nombre { get; set; }
        public int Id_Defecto { get; set; }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Oper> GetOperario(string pre, int idlinea)
    {
        try
        {
            List<Oper> list = new List<Oper>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            //SqlCommand cmd = new SqlCommand("  select distinct Top(10)  op.id_operario,op.nombre"
            //                              + "  from Operario op"
            //                              + "  join OperacionOperario opOp on opOp.id_operario = op.id_operario"
            //                              + "  join Seccion s on opOp.id_seccion = s.id_seccion"
            //                              + "  join Linea l on s.id_linea = l.id_linea"
            //                              + "  where op.nombre like '%" + pre + "%' and l.id_linea =" + idlinea, cn);

            SqlCommand cmd = new SqlCommand("  select distinct Top(10)  op.id_operario,op.nombre"
                                         + "  from Operario op"
                                         + "  join OperacionOperario opOp on opOp.id_operario = op.id_operario"
                                         + "  join Seccion s on opOp.id_seccion = s.id_seccion"
                                         + "  join Linea l on s.id_linea = l.id_linea"
                                         + "  where op.nombre like '%" + pre + "%'", cn);


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Oper obj = new Oper();

                obj.idoperacionOpe = int.Parse(dt.Rows[i][0].ToString());
                obj.operario = dt.Rows[i][1].ToString();
                //  obj.operacion = dt.Rows[i][2].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class Oper
    {
        public int idoperacionOpe { get; set; }
        public string operario { get; set; }
        public string operacion { get; set; }
    }

}