﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CargaEspecificaciones.aspx.cs" Inherits="CargaEspecificaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript">
        function alertSeleccionarMedida(mess, tipo) {
            swal({
                title: "",
                text: mess,
                type: tipo
            });
        }
        function successalert() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
    </script>
    <script type="text/javascript">
        function warningalert() {
            swal({
                title: "¿Estas seguro?",
                text: "¿Deseas ingresar este registro?",
                type: "warning"
            })
        }
    </script>

 

    <link href="bootstrap-fileinput-master/css/fileinput.css" rel="stylesheet" />
    <link href="bootstrap-fileinput-master/css/fileinput.min.css" rel="stylesheet" />
    <script src="bootstrap-fileinput-master/js/fileinput.js"></script>
    <script src="bootstrap-fileinput-master/js/fileinput.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="container" style="margin-top:10px;">
        
                    <div class="col-lg-6 ">
                        <div class="panel panel-info " style="margin: 0">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body form-inline">

                                <asp:DropDownList runat="server" ID="drpEstilos" CssClass="form-control btn-block" >                               
                                </asp:DropDownList>

                                <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success btn-block " Text="Guardar" OnClick="btnGuardar_Click" />
                                <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-danger btn-block" Text="Limpiar"  />
                            </div>
                        </div>

                     
                    </div>

        <div class="col-lg-6 ">
               <div class="panel panel-default">
                            <div class="panel-heading ">
                                <h3>Seleccionar Archivo Excel de Especificaciones
                                </h3>
                            </div>
                            <div class="panel-body">
                                <asp:FileUpload ID="FileUpload1" runat="server" name="input2[]" class="file" multiple="multiple" data-show-upload="false" data-show-caption="true" />

                                <div runat="server" id="ms">
                                </div>
                            </div>
                        </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

