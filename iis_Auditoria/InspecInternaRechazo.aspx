﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InspecInternaRechazo.aspx.cs" Inherits="InspecInternaRechazo" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v16.2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <script type="text/javascript">
        function alertaMess(Mess) {
            swal({
                text: Mess,
                type: 'error'
            });
        }

         function alertSeleccionarMedida(mes,tipo) {
             swal({
                 title: mes,
                 type: tipo
            });
        }
    </script>

      <link href="jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2>
        <asp:Label ID="lbl" runat="server" ForeColor="White" Text="Bundle by bundle Audit"></asp:Label>
    </h2>
    <br />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="container-fluid">
        <div class="col-lg-12 centrar">

            <asp:ImageButton ID="ImageButton2" runat="server" CssClass="marB" ImageUrl="~/img/system_search.png" OnClick="ImageButton2_Click" />
            <asp:LinkButton ID="lnkNew" runat="server" OnClick="lnkNew_Click">New Search</asp:LinkButton>

            <asp:ImageButton ID="ImageButton1" runat="server" CssClass="marB" ImageUrl="~/img/report.png" OnClick="ImageButton1_Click" />
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Hoja de Corte</asp:LinkButton>

        </div>
    </div>


    <div class="container-fluid info_ord">
        <div class="container  " style="padding-bottom: 5px;">
            <div class="col-lg-4">
                <div class="col-lg-12 centrar">
                    <asp:Label ID="lblcustom" runat="server" Text="Cliente:"></asp:Label>
                    <asp:Label ID="lblcustomerval" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-lg-12 centrar">
                    <asp:Label ID="Label1" runat="server" Text="Corte:"></asp:Label>
                    <asp:Label ID="lblCorteval" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="col-lg-12 centrar">
                    <asp:Label ID="Fecha" runat="server" Text="Fecha:"></asp:Label>
                    <asp:Label ID="lblfechaval" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-lg-12 centrar">
                    <asp:Label ID="Label3" runat="server" Text="Estilo:"></asp:Label>
                    <asp:Label ID="lblestiloval" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="col-lg-12 centrar">
                    <asp:Label ID="Label5" runat="server" Text="Linea:"></asp:Label>
                    <asp:Label ID="lineaval" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblseccionval" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-lg-12 centrar">
                    <asp:Label ID="Label7" runat="server" Text="Inspector:"></asp:Label>
                    <asp:Label ID="lblinspectorval" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-8 col-md-offset-2">
        <div class="col-lg-4 col-lg-offset-4">
            <asp:Button Text="REPARAR BULTO" ID="btnreparar" OnClick="btnreparar_Click" CssClass="btn btn-success form-control marB" runat="server" />

        </div>
    </div>

    <div class="col-lg-12 centrar" style="height: 35px">
        <h4>
           
           
        </h4>
    </div>

    <div class="container">
        <div class="col-lg-6 col-lg-offset-3 panel panel-primary">
            <div class="col-lg-6">
                <div class="form-group">
                    Reauditoria
                    <asp:CheckBox ID="CheckBox1" Text="Activo" Checked="false" CssClass="form-control" runat="server" />
                </div>
            </div>
            <div class="col-lg-6">
                  Cantidad de Muestra
                  <asp:TextBox ID="txtMuestreo" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
             
           
        </div>
        <div class="col-lg-12">

            <asp:GridView ID="gridAddDefectos" runat="server" CssClass="table table-hover table-striped" GridLines="None" Width="100%" AutoGenerateColumns="False"
                OnRowDataBound="gridAddDefectos_RowDataBound" OnRowCommand="gridAddDefectos_RowCommand">
                <Columns>
                    <asp:TemplateField HeaderText="Cantidad">
                        <ItemTemplate>
                            <asp:TextBox ID="txtcantidaDefectos" Type="Number" CssClass="form-control" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Codigo Defecto">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddldefectos" CssClass="form-control" runat="server"></asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Codigo Operacion">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlposicion" CssClass="form-control" runat="server"></asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="No" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblno" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btnrechazar" runat="server" Text="Rechazar" CommandName="Guardar" CssClass="btn btn-warning" />
                        </ItemTemplate>
                        <HeaderTemplate>
                            <asp:Button ID="btnrechazar1" runat="server" Text="Guardar Todos" CommandName="GuardarTodos"  CssClass="btn btn-primary" />
                        </HeaderTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

        </div>

    </div>
    <asp:HiddenField runat="server" ID="hdfidinspector" />
    <asp:HiddenField runat="server" ID="hdnguid" />

    <br />

</asp:Content>

