﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Production.aspx.cs" Inherits="Production" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <script type="text/javascript">
         function erroralert() {
             swal({
                 title: 'Acceso Denegado!',
                 type: 'error'
             });
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2>
        <asp:Label ID="lbl" runat="server" Text="Production Audit"></asp:Label></h2>
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    
         <div class="col-lg-8 col-lg-offset-2" style="text-align:center; margin-top:50px;">
    <div class="col-md-3">
    <asp:ImageButton ID="imgBack" runat="server" ImageUrl="img/trasero.png" 
        OnClick="imgBack_Click" /><br />
    <asp:LinkButton ID="lnkback" runat="server" OnClick="lnkback_Click">Trasero</asp:LinkButton>
    </div>
        <div class="col-md-3">
        <asp:ImageButton ID="imgFront" runat="server" ImageUrl="img/delantero.png"  Width="128px"
            OnClick="imgFront_Click"/><br />
          <asp:LinkButton ID="lnkFront" runat="server" OnClick="lnkFront_Click">Delantero</asp:LinkButton>
        </div>
        <div class="col-md-3">
            <asp:ImageButton ID="imgE1" runat="server" ImageUrl="img/E1.png" 
            OnClick="imgE1_Click"/><br />
         <asp:LinkButton ID="lnkE1" runat="server" OnClick="lnkE1_Click">Ensamble I</asp:LinkButton>
         </div>
          <div class="col-md-3">
            <asp:ImageButton ID="imgE2" runat="server" ImageUrl="img/pants.png" Width="128px"
           OnClick="imgE2_Click"/><br />
         <asp:LinkButton ID="lnkE2" runat="server" OnClick="lnkE2_Click">Ensamble II</asp:LinkButton>
         </div>
     </div>
     <asp:Button ID="Button4" runat="server" Text="Help Audit" Width="150" 
        Height="150" Visible="False"/>
    

    <dx:ASPxPopupControl ID="CantidadProduccion" runat="server" EnableTheming="True" ClientInstanceName="AsignarDefectos"
        HeaderText="Produccion Operarios" Modal="True" Theme="MetropolisBlue" 
        AllowDragging="True" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" CloseAction="CloseButton"
        Width="850px" MaxHeight="600px" ScrollBars="Auto">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxPanel ID="ASPxPanel1" runat="server">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <center>
                                                    <asp:CheckBox ID="chkdefectos" Width="30px" runat="server" />
                                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="id" ItemStyle-HorizontalAlign="Center" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="id" runat="server" Text='<%# Eval("id") %>' Font-Size="Large" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Codigo" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbcodigo" runat="server" Text='<%# Eval("Codigo") %>' Font-Size="Large"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nombre" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbnombre" runat="server" Text='<%# Eval("Nombre") %>' Font-Size="Large"></asp:Label>
                                        </ItemTemplate>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtcantidad" Width="100px" Height="30px" BackColor="LightYellow" runat="server" Font-Size="Large"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="header_gv"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>                                   
                                </Columns>
                            </asp:GridView>
                            <br />
                            <center>
                                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="SAVE" CssClass="nephritis-flat-button1" OnClick="ASPxButton1_Click"></dx:ASPxButton>
                            </center>                          
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

</asp:Content>

