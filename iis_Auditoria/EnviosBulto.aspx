﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EnviosBulto.aspx.cs" Inherits="EnviosBulto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Algunos Bultos No Se Guardaron Correctamente !',
                type: 'info'
            });
        }
        function alertClave() {
            swal({
                title: 'Clave Incorrecta !',
                type: 'warning'
            });
        }
        function ClaveCorrect() {
            swal({
                title: 'Clave Correcta, Accion Realizada !',
                type: 'info'
            });
        }

    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function alertMasterE(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }
    </script>

    <style>
        .mar_t {
            margin: 10px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="">
            <div class="panel panel-primary" style="margin-top: 10px;">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Bihorario de Envio </strong></h3>
                </div>
                <div class="panel-body">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:MultiView runat="server" ID="multiview">
                                <asp:View runat="server">

                                    <asp:HiddenField ID="HiddenFielduser" runat="server" />

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        Envio Dia
                                    <asp:DropDownList ID="drpEnvioDia" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Select..." Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 Envio" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 Envio" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 Envio" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 Envio" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5 Envio" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="6 Envio" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="7 Envio" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="8 Envio" Value="8"></asp:ListItem>
                                    </asp:DropDownList>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        Linea
                                         <asp:DropDownList runat="server" ID="drpLinea" OnSelectedIndexChanged="drpLinea_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                         </asp:DropDownList>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6" style="margin-top: 10px">

                                        <div class="col-lg-3">
                                            <asp:Button runat="server" ID="Button1" OnClick="Button1_Click" Text="Bihorario" CssClass="btn btn-default  mar_t" />
                                        </div>
                                        <div class="col-lg-9">
                                            <asp:Panel ID="verB" runat="server" Visible="false">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">Opcion de Bloqueo</div>
                                                    <div class="panel-body">
                                                        <asp:TextBox ID="txtblo" runat="server" placeholder="Ingrese el Codigo" CssClass="form-control" MaxLength="8" onkeypress="return num(event);" AutoCompleteType="Disabled"></asp:TextBox>
                                                        <asp:Button ID="btnbloq" runat="server" CssClass="btn btn-primary mar_t" Text="Aceptar" OnClick="btnbloq_Click" />
                                                        <asp:Label ID="lbld" runat="server" Text="" ForeColor="#ff6600" Font-Bold="true"></asp:Label>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <asp:Button runat="server" ID="btnguardarEnviar" OnClick="btnguardarEnviar_Click" Text="Guardar Envio" CssClass="btn btn-primary  mar_t" />

                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <asp:Button runat="server" ID="btnVertodos" OnClick="btnVertodos_Click" Text="Ver Todos " CssClass="btn btn-info  mar_t" />
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="overflow-x: auto">
                                        <style>
                                            .w {
                                                width: 100%;
                                                min-width: 700px;
                                            }
                                        </style>
                                        <asp:GridView ID="gridEnvios" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" CssClass="table table-hover table-striped w" AutoGenerateColumns="false" GridLines="None">
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        Bulto 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblidorder" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("Id_Order") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        Linea 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbllinea" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("Linea") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        Porder
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPorder" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("Porder") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        Quantity 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblquantity" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Quantity") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        Unidades
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtunidades" Enabled="false" Width="100px" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Cantidad") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        Envio Corte
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtenvio" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Envio") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton Text="" ID="lnkEnvio" OnClick="lnkEnvio_Click" CssClass="btn btn-info" runat="server"><i class="glyphicon glyphicon-indent-left"> </i> </asp:LinkButton>
                                                        <asp:CheckBox ID="checkEnvio" Text="Enviar" Checked="true" CssClass="btn btn-default" runat="server" />
                                                        <asp:LinkButton Text="" ID="lnkseccionado" OnClick="lnkseccionado_Click" CssClass="btn btn-success" runat="server"><i class="glyphicon glyphicon-object-align-bottom"> </i> </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                        <%-- </div>--%>
                                    </div>

                                </asp:View>
                                <asp:View runat="server">
                                </asp:View>
                            </asp:MultiView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

