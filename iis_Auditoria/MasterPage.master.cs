﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Data.Sql;
using SistemaAuditores.DataAccess;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.User.Identity.IsAuthenticated == true)//
            {
                lbldate.Text = DateTime.Now.ToLongDateString();

                string us = Page.User.Identity.Name.ToString().ToLower();

                DataTable dt = DataAccess.Get_DataTable("select userName from tblUserMedida where userms = '" + us + "'");

                if (dt.Rows.Count > 0)
                    lblUName.Text = dt.Rows[0][0].ToString();
                else
                {
                    DataTable dt1 = DataAccess.Get_DataTable("select nombre from Inspector where codigo='" + us + "'");

                    if (dt1.Rows.Count > 0)
                        lblUName.Text = dt1.Rows[0][0].ToString();
                    else
                        lblUName.Text = Page.User.Identity.Name.ToString();
                }

                link();

                if (us.Trim() == "empaquesup" || us.Trim() == "jsilva")
                {
                    lnkpck.Visible = true;
                    lnkpckXfecha.Visible = true;
                    lnkpack.Visible = true;
                    lnkreportC.Visible = true;
                    lnkgrafico.Visible = true;
                }

                if (Page.User.IsInRole("Administrator") || Page.User.IsInRole("Reports"))
                {
                    LinkButton2.Visible = true;
                    lnkreportes.Visible = true;
                    adminLink.Visible = true;
                    LinkButton5.Visible = true;
                    lnkreportes.Visible = true;
                    LinkButton3.Visible = true;
                    LinkButton7.Visible = true;
                    LinkButton8.Visible = true;
                    lnkEspecificacion.Visible = true;
                    lnkEspecificacion2.Visible = true;
                    LinkLine.Visible = true;
                    lnkweek.Visible = true;
                    lnkpack.Visible = true;
                    lnkpck.Visible = true;
                    lnkpckXfecha.Visible = true;
                    lnkreproceso.Visible = true;
                    lnkentallado.Visible = true;
                    lnktalla.Visible = true;

                    //usuarios con permiso de cambiar linea
                    if (us == "roc6" || us == "planta03" || us == "planta1" || us == "administrator")
                    {
                        lnkUserLine.Visible = true;
                    }

                    if (us == "reportemod3")
                    {
                        lavanderiaL.Visible = true;
                        intexl.Visible = true;
                    }
                }
                else if (Page.User.IsInRole("Inspector"))
                {
                    LinkButton2.Visible = true;
                    LinkButton6.Visible = true;
                    if(us == "supervisor4")
                    {
                        pro.Visible = true;                        
                        LinkButton6.Visible = false;
                        lnkproc2.Visible = true;
                    }
                }                
                else if (Page.User.IsInRole("PackList"))
                {
                    lnkinfo2.Visible = true;
                }
                else if (Page.User.IsInRole("PackAdmon"))
                {
                    LinkButton7.Visible = true;
                    lnkEnvios.Visible = true;
                    lnkEnviosXBulto.Visible = true;
                    lnkExcepcion.Visible = true;
                    lnkbio.Visible = true;
                    lnkpl.Visible = true;
                    lnkweek.Visible = true;
                    lavanderiaL.Visible = true;
                    intexl.Visible = true;
                    lnkentallado.Visible = true;
                    lnkEnviosSeccionados.Visible = true;
                    lnk1.Visible = true;
                    lnk4.Visible = true;
                    lnk5.Visible = true;
                    lnk6.Visible = true;
                    lnk7.Visible = true;
                    lnkcommrep.Visible = true;
                    lnkHistoricoPross.Visible = true;
                    lnkdev.Visible = true;
                    lnkemabrque.Visible = true;
                    lnkcom.Visible = true;
                    lnkintex1.Visible = true;
                    lnkintex2.Visible = true;
                    lnkintex3.Visible = true;
                    lnkhist.Visible = true;
                    openCutint.Visible = true;
                    linkAdminIntex.Visible = true;
                    lnktalla.Visible = true;
                    lnkprod.Visible = true;
                    Li1.Visible = true;
                    lnkrepsec.Visible = true;
                    lnkbs.Visible = true;
                    lnkbihoprod.Visible = true;
                    lnkauditsecado.Visible = true;
                    lnkenviosecado.Visible = true;
                    lnkprocesos.Visible = true;
                    lnkbihoraroenviado.Visible = true;
                    lnkpacklistIntex.Visible = true;
                    lnkinvIntex.Visible = true;
                    lnkupdAudit.Visible = true;

                    if (us == "adminsnr" || us == "rocedesl")
                    {
                        lnkExceso.Visible = true;
                        lnkpack.Visible = true;
                        pro.Visible = true;
                        lnkreportC.Visible = true;
                        lnkcontenedor.Visible = true;
                        rci.Visible = true;
                        // Nuevo 2022-10-25                        
                        lnkproc2.Visible = true;
                        //lnkenv2.Visible = true;
                    }

                    if (us == "rocedesl")
                    {
                        LinkButton7.Visible = false;
                        lnkEnvios.Visible = false;
                        Li1.Visible = false;
                        lnkreportC.Visible = true;
                    }

                    if (us == "rjose" || us == "ezamora")
                    {
                        lnkpack.Visible = true;
                        lnkpck.Visible = true;
                        lnkpckXfecha.Visible = true;
                        lnkreportC.Visible = true;
                    }
                }
            }
            else
            {
                FormsAuthentication.SignOut();
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            FormsAuthentication.SignOut();
            Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
        }
        catch (Exception)
        {

            //  throw;
        }

    }

    void link()
    {
        LinkButton2.Visible = false;
        LinkButton4.Visible = false;
        lnkreportes.Visible = false;
        LinkButton6.Visible = false;
        LinkButton7.Visible = false;
        LinkButton8.Visible = false;
        lnkPackList.Visible = false;
        lnkEnvios.Visible = false;
        lnkinfo2.Visible = false;

        Li1.Visible = false;
        //
        lnkEnviosXBulto.Visible = false;
        lnkEnviosSeccionados.Visible = false;
        lnkExcepcion.Visible = false;
        lnkcom.Visible = false;
        lnkExceso.Visible = false;
        lnkcontenedor.Visible = false;

        lnkprod.Visible = false;
        //
        lnkbio.Visible = false;
        lnkpl.Visible = false;
        lnkweek.Visible = false;
        lnkbihoprod.Visible = false;

        adminLink.Visible = false;
        //
        LinkButton5.Visible = false;
        LinkButton3.Visible = false;
        lnkUserLine.Visible = false;
        LinkLine.Visible = false;
        lnkEspecificacion.Visible = false;
        lnkEspecificacion2.Visible = false;
        lnk.Visible = false;
        lnk2.Visible = false;

        lavanderiaL.Visible = false;
        //
        lnk1.Visible = false;
        lnk4.Visible = false;
        lnk5.Visible = false;
        lnk6.Visible = false;
        lnk7.Visible = false;
        lnkcommrep.Visible = false;
        lnkHistoricoPross.Visible = false;
        lnkdev.Visible = false;
        lnkemabrque.Visible = false;

        intexl.Visible = false;
        //
        lnkintex1.Visible = false;
        lnkintex2.Visible = false;
        lnkintex3.Visible = false;
        lnkhist.Visible = false;
        openCutint.Visible = false;
        linkAdminIntex.Visible = false;
        lnkrepsec.Visible = false;
        lnkbs.Visible = false;
        lnkauditsecado.Visible = false;
        lnkenviosecado.Visible = false;
        lnkprocesos.Visible = false;
        lnkbihoraroenviado.Visible = false;
        lnkpacklistIntex.Visible = false;
        lnkinvIntex.Visible = false;
        lnkupdAudit.Visible = false;

        lnkentallado.Visible = false;
        //
        lnktalla.Visible = false;

        lnkpack.Visible = false;
        //
        lnkpck.Visible = false;
        lnkpckXfecha.Visible = false;
        lnkadmins.Visible = false;
        lnkreportC.Visible = false;
        lnkgrafico.Visible = false;
        lnkreproceso.Visible = false;

        pro.Visible = false;
        //
        lnkproc1.Visible = false;
        lnkproc2.Visible = false;
        lnkenv1.Visible = false;
        lnkenv2.Visible = false;
        rci.Visible = false;
    }

    protected void LinkButton5_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Administracion.aspx");
    }

    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Search.aspx");
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Default.aspx");
    }

    protected void lnkreportes_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin/SelectReportType.aspx");
    }

    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Style.aspx");
    }

    protected void LinkButton6_Click(object sender, EventArgs e)
    {
        string user = HttpContext.Current.User.Identity.Name;
        // string query = "select rol from tblUserLineaMedida where nameMemberShip ='" + user + "'";
        string query = "select rol from tblusermedida where userms ='" + user + "'";

        DataTable dt_rol = DataAccess.Get_DataTable(query);

        if (dt_rol.Rows.Count > 0)
        {
            //else if (dt_rol.Rows[0][0].ToString().Contains("especial2"))
            //{
            //    Response.Redirect("/MedidaEspecial2.aspx");
            //}
            if (dt_rol.Rows[0][0].ToString().Contains("modulo4"))
            {
                Response.Redirect("/Medida.aspx");
            }
            else if (dt_rol.Rows[0][0].ToString().Contains("especial1"))
            {
                Response.Redirect("/MedidaEspecial1.aspx");
            }
            else if (dt_rol.Rows[0][0].ToString().Contains("especial2"))
            {
                Response.Redirect("/MedidaNew.aspx", false);
            }
            else if (dt_rol.Rows[0][0].ToString().Contains("intex"))
            {
                Response.Redirect("/MedidasNewIntex1.aspx");
            }
            else if (dt_rol.Rows[0][0].ToString().Contains("muestra"))
            {
                Response.Redirect("/MedidaMuestra.aspx");
            }
            else if (dt_rol.Rows[0][0].ToString().Contains("bultoabulto"))
            {
                Response.Redirect("/MedidasNewBultoaBulto.aspx");
            }
            else if (dt_rol.Rows[0][0].ToString().Contains("final"))
            {
                Response.Redirect("/MedidasNewFinal.aspx");
            }
            else if (dt_rol.Rows[0][0].ToString().Contains("embarque"))
            {
                Response.Redirect("/MedidasNewEmbarque.aspx");
            }
            else if (dt_rol.Rows[0][0].ToString().Contains("iSecado"))
            {
                Response.Redirect("/SalidaSecadora.aspx");
            }
            else if (dt_rol.Rows[0][0].ToString().Contains("AuditoriaS"))
            {
                Response.Redirect("/Intex/AuditoriaSecadoIntex.aspx");
            }
        }
        else
        {
            Response.Redirect("/Default.aspx");
        }

    }

    protected void LinkButton7_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Asignacion.aspx");
    }

    protected void LinkButton8_Click(object sender, EventArgs e)
    {
        Response.Redirect("/AsignarComentario.aspx");
    }

    protected void lnkUserLine_Click(object sender, EventArgs e)
    {
        Response.Redirect("/UserLine.aspx");
    }

    protected void lnkPackList_Click(object sender, EventArgs e)
    {
        string user = HttpContext.Current.User.Identity.Name;
        string query = " select modulo from tblUserEnvio where nameMemberShip = '" + user + "'";
        DataTable acceso = DataAccess.Get_DataTable(query);
        if (acceso.Rows.Count > 0)
        {
            if (acceso.Rows[0][0].ToString().Equals("Linea"))
            {
                Response.Redirect("/InfoLinea.aspx");
            }
            else if (acceso.Rows[0][0].ToString().Equals("Intex"))
            {
                Response.Redirect("/InfoLineaIntex.aspx");
            }
        }
        else
        {
            Response.Redirect("/Default.aspx");
        }
    }

    protected void lnkExcepcion_Click(object sender, EventArgs e)
    {
        int i = activarUrl();

        if (i == 1)
        {
            Response.Redirect("/EnviosBultoExcepcion.aspx");
        }
        else if (i == 2)
        {
            Response.Redirect("/EnviosBultoExcepcionIntex.aspx");
        }
        else if (i == 0)
        {
            Response.Redirect("/Default.aspx");
        }


    }

    protected void lnkEnvios_Click(object sender, EventArgs e)
    {
        int i = activarUrl();

        if (i == 1)
        {
            Response.Redirect("/EnviosBulto.aspx");
        }
        else if (i == 2)
        {
            Response.Redirect("/EnviosBultoIntex.aspx");
        }
        else if (i == 0)
        {
            Response.Redirect("/Default.aspx");
        }


    }

    protected void lnkbio_Click(object sender, EventArgs e)
    {
        int i = activarUrl();

        if (i == 1)
        {
            Response.Redirect("/adminReport/ReportBio.aspx");
        }
        else if (i == 2)
        {
            Response.Redirect("/adminReport/ReportBioIntex.aspx");
        }
    }

    protected void lnkpl_Click(object sender, EventArgs e)
    {
        int i = activarUrl();

        if (i == 1)
        {
            Response.Redirect("/adminReport/ReportPackingList.aspx");
            // Response.Redirect("/adminReport/PackingList.aspx");
        }
        else if (i == 2)
        {
            Response.Redirect("/adminReport/PackingListIntex.aspx");
        }
    }

    protected void lnkweek_Click(object sender, EventArgs e)
    {
        int i = activarUrl();

        if (i == 1)
        {
            Response.Redirect("/adminReport/ReportWeek.aspx");
        }
        else if (i == 2)
        {
            Response.Redirect("/adminReport/ReportWeekIntex.aspx");
        }
    }

    int activarUrl()
    {
        int url = 0;

        DataTable dt = DataAccess.Get_DataTable("select u.Id_Planta from UserPlanta u where u.nombreUsuario = '" + Page.User.Identity.Name.ToString() + "'");

        if (dt.Rows.Count > 0)
        {
            int i = Convert.ToInt32(dt.Rows[0][0]);

            switch (i)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                    url = 1;
                    break;
                case 0:
                    url = 2;
                    break;
                default:
                    break;
            }

        }

        return url;
    }

    protected void LinkLine_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Line.aspx");
    }

    protected void lnkEnviosXBulto_Click(object sender, EventArgs e)
    {
        int i = activarUrl();

        if (i == 1)
        {
            Response.Redirect("/EnviosBultoXCorte.aspx?idorder=&porder=");
        }
        else if (i == 2)
        {
            Response.Redirect("/EnviosBultoXCorteIntex.aspx?idorder=&porder=");
        }
    }

    protected void lnkEspecificacion_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Especificaciones");
    }

    protected void lnk1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/PLgeneral.aspx");
    }

    protected void lnk4_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/OutSewNotSent.aspx");
    }

    protected void lnk5_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReporteEnNoEn.aspx");
    }

    protected void lnk6_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/PackingListRepair.aspx");
    }

    protected void lnkEnviosSeccionados_Click(object sender, EventArgs e)
    {
        int i = activarUrl();

        if (i == 1)
        {
            Response.Redirect("/enviosBultosSeccionados.aspx");
        }
        else if (i == 2)
        {
            Response.Redirect("/enviosBultosSeccionadosIntex.aspx");
        }
    }

    protected void lnkcom_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/HistInPlant.aspx");
    }

    protected void lnkcommrep_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportHistInPlant.aspx");
    }

    protected void lnk7_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportWeekSNS.aspx");
    }

    protected void lnkHistoricoPross_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/HistorialCorte.aspx");
    }

    protected void lnkintex1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportBioIntex.aspx");
    }

    protected void lnkintex2_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/PackingListIntex.aspx");
    }

    protected void lnkintex3_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportWeekIntex.aspx");
    }

    protected void lnktalla_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/HistorialCorteTalla.aspx");
    }

    protected void lnkhist_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/HistorialCorteIntex.aspx");
    }

    protected void lnkExceso_Click(object sender, EventArgs e)
    {
        Response.Redirect("/RegistroExceso.aspx");
    }

    protected void lnkcontenedor_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ContenedorEmb.aspx");
    }

    protected void lnkemabrque_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReporteContenedorEmb.aspx");
    }

    protected void lnkpck_Click(object sender, EventArgs e)
    {
        //Response.Redirect("/ReporteEmpaque/PackingListF.aspx");
        Response.Redirect("/ReporteEmpaque/BoxReportBihorario.aspx");
    }

    protected void lnkEspecificacion2_Click(object sender, EventArgs e)
    {
        Response.Redirect("/CargaEspecificaciones.aspx");
    }

    protected void lnk_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/Ejemplo.aspx");
    }

    protected void openCutint_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/EnviadoNoenviadoL.aspx");
    }

    protected void lnkpckXfecha_Click(object sender, EventArgs e)
    {
        //Response.Redirect("/ReporteEmpaque/PackingListGeneral.aspx");
        Response.Redirect("/ReporteEmpaque/BoxReport.aspx");
    }

    protected void lnk2_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/Ejemplo2.aspx");
    }

    protected void linkAdminIntex_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/PackingListRepairIntex.aspx");
    }

    protected void lnkdev_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReporteDevoluciones.aspx");
    }

    protected void lnkadmins_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/AdministracionEmpaque.aspx");
    }

    protected void lnkreportC_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/ReporteEmpacado.aspx");
    }

    protected void lnkproc1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Procesos/ReprocesoPlanchaRec.aspx");
    }

    protected void lnkproc2_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Procesos/HeatTransfer2.aspx");
    }

    protected void lnkenv1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Procesos/EnvioReprocesoPlancha.aspx");
    }

    protected void lnkenv2_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Procesos/HTEnvios.aspx");
    }

    protected void lnkgrafico_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/GraficoEmpacado.aspx");
    }

    protected void rci_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Intex/RecepcionIntex.aspx");
    }

    protected void lnkrepsec_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReparacionSecadoIntex.aspx");
    }

    protected void lnkbs_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportBihorarioSecado.aspx");
    }

    protected void lnkinfo2_Click(object sender, EventArgs e)
    {
        Response.Redirect("/InfoLineaProd.aspx");
    }

    protected void lnkbihoprod_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportBioProd.aspx");
    }

    protected void lnkreproceso_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteEmpaque/ReporteBihorarioReproceso.aspx");
    }

    protected void lnkauditsecado_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Intex/ReporteAuditadoSecadoIntex.aspx");
    }

    protected void lnkenviosecado_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Intex/EnviosProcesoSecado.aspx");
    }

    protected void lnkprocesos_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Intex/ReporteProcesosIntex.aspx");
    }

    protected void lnkbihoraroenviado_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Intex/ReporteBihorarioSecadoEnviado.aspx");
    }

    protected void lnkpacklistIntex_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Intex/PackingListEnvioSecado.aspx");
    }

    protected void lnkinvIntex_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReporteIntexInv.aspx");
    }

    protected void lnkupdAudit_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Intex/ReparacionAuditoriaIntex.aspx");
    }
}