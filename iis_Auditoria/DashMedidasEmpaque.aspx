﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DashMedidasEmpaque.aspx.cs" Inherits="DashMedidasEmpaque" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx1" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Sistema de Control Auditores</title>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width" />

    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="/bootstrap/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>

    <%--   <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato" />
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/main.css" rel="stylesheet" type="text/css" />--%>

    <%-- <link href="/font-awesome/fonts.css" rel="stylesheet" type="text/css" />--%>
    <%--  <link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css" />
    <script src="/sweetalert-master/dist/sweetalert.min.js"></script>--%>

    <link rel="/img/" href="AuditIco.ico" />
    <link rel="shortcut icon" type="image/x-icon" href="/img/AuditIco.ico" />
</head>
<body>
    <style>
        body {
            background: #e4e4e4;
        }
    </style>
    <form id="form1" runat="server">

        <div class="container-fluid" style="margin-top: 20px">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="row" style="padding-right:5px;">
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <img src="img/Logo-Roc-New-BIG.JPG"  height="230" />
                        </div>
                        <asp:HiddenField ID="week" runat="server" />
                        <div class="col-lg-3 col-md-6 col-sm-6">
                             <h2 style="font-size: 3em">
                                   <asp:Label Text="" runat="server" ID="lblClienteN" />  
                            </h2>
                            <%-- <h1 style="font-size: 4em">Packing Station 01--  </h1>%>
                                <%--<asp:DropDownList CssClass="form-control" ID="DropDownList1" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>--%>
                           
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12" style="border: 1px solid #a1a1a1; border-radius: 10px 10px 10px 10px;padding:0">
                            <div class="col-lg-12 col-lg-12 col-sm-12 col-xs-12">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" " style="font-size: 2em;padding-top:4%">
                                    <p style="margin: 0px !important;">
                                        <label>Processed</label>
                                    </p>
                                    <p style="margin: 0px !important;">
                                        <label>Today</label>
                                    </p>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="font-size: 7em">
                                    <p style="margin: 0px !important;">
                                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                                    </p>
                                </div>
                                 <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="font-size: 1.2em">
                                    <p style="margin: 0px !important;">
                                        <asp:Label ID="Label6" runat="server" Text="AQL GOAL"></asp:Label>
                                    </p>
                                     <p style="margin: 0px !important;">
                                        <asp:Label ID="lblCalculoAQL" runat="server" Text="4%"></asp:Label>
                                    </p>
                                      <p style="margin: 0px !important;">
                                        <asp:Label ID="Label9" runat="server" Text="Status"></asp:Label>
                                    </p>
                                      <p style="margin: 0px !important;">
                                        <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-12 col-lg-12 col-sm-12 col-xs-12" style="border-top: 1px solid #a1a1a1">

                                <div class="col-lg-12 text-success" style="font-size: 2em">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                        <asp:Label ID="Label5" Style="" class="pull-right" runat="server" Text="Passed"></asp:Label>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="Label4" Style="" class="pull-right" runat="server" Text="Label"></asp:Label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="border-left: 1px solid #519851">
                                        <asp:Label ID="Label2" Style="" class="pull-right" runat="server" Text="Label"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-lg-12 text-danger" style="font-size: 2em">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                        <asp:Label ID="Label8" class="pull-right" runat="server" Text="Fail"></asp:Label>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="Label7" class="pull-right" runat="server" Text="Label"></asp:Label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="border-left: 1px solid #96302d">
                                        <asp:Label ID="Label3" class="pull-right" runat="server" Text="Label"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 30px">
                        <div class="col-lg-12">
                             
                        </div>

                    </div>
                    <div class="row" style="margin-top: 5px" >
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" >
                            <h4>Live View</h4>
                            <%-- <iframe width="350" height="250" src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>--%>
                       <%--  <iframe src="http://ipcamlive.com/player/player.php?alias=5d3b5fed7bcd9&autoplay=true" width="350" height="260" frameborder="0" autostart="yes" allowfullscreen></iframe>--%>
                            <iframe src="rtsp://admin:tech2014@152.231.39.180/streaming/channels/201" width="350" height="260" frameborder="0" autostart="yes" allowfullscreen></iframe>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                            <h4>Weekly Production</h4>
                            <dx1:WebChartControl ID="WebChartControl1" runat="server" Width="360px" Height="260px"
                                CrosshairEnabled="True"
                                SeriesDataMember="Day">
                                <BorderOptions Visibility="False" />
                                <BorderOptions Visibility="False"></BorderOptions>
                                <DiagramSerializable>
                                    <cc1:XYDiagram
                                        RangeControlDateTimeGridOptions-GridMode="ChartGrid">
                                        <AxisX VisibleInPanesSerializable="-1">
                                            <GridLines Visible="True">
                                            </GridLines>
                                        </AxisX>
                                        <AxisY VisibleInPanesSerializable="-1">
                                        </AxisY>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <Titles>
                                    <cc1:ChartTitle Text="" ></cc1:ChartTitle>
                                </Titles>
                                <SeriesTemplate ArgumentDataMember="Day" LabelsVisibility="True" CrosshairLabelPattern="{S}:{V}"
                                    ValueDataMembersSerializable="valor">
                                    <LabelSerializable>
                                        <cc1:SideBySideBarSeriesLabel Position="Top" TextPattern="Qty:{V}"
                                            Indent="5" LineLength="10" LineVisibility="True"
                                            ResolveOverlappingMode="Default">
                                        </cc1:SideBySideBarSeriesLabel>
                                    </LabelSerializable>
                                </SeriesTemplate>
                            </dx1:WebChartControl>

                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                            <h4>
                                Passed / Fail
                            </h4>
                            <%--<strong style="font-family:inherit;font-size:20px"></strong> --%>
                            <dx1:WebChartControl ID="WebChartControl2" runat="server" Width="360px" Height="260px"
                                CrosshairEnabled="True"
                                SeriesDataMember="estado">
                                <BorderOptions Visibility="False" />
                                <BorderOptions Visibility="False"></BorderOptions>
                                <DiagramSerializable>
                                    <cc1:XYDiagram
                                        RangeControlDateTimeGridOptions-GridMode="ChartGrid">
                                        <AxisX VisibleInPanesSerializable="-1">
                                            <GridLines Visible="True">
                                            </GridLines>
                                        </AxisX>
                                        <AxisY VisibleInPanesSerializable="-1">
                                        </AxisY>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <Titles>
                                    <cc1:ChartTitle Text=""></cc1:ChartTitle>
                                </Titles>
                                <SeriesTemplate ArgumentDataMember="Day" LabelsVisibility="True" CrosshairLabelPattern="{S}:{V}"
                                    ValueDataMembersSerializable="valor">
                                    <LabelSerializable>
                                        <cc1:SideBySideBarSeriesLabel Position="Top" TextPattern="Qty:{V}"
                                            Indent="5" LineLength="10" LineVisibility="True"
                                            ResolveOverlappingMode="Default">
                                        </cc1:SideBySideBarSeriesLabel>
                                    </LabelSerializable>
                                </SeriesTemplate>
                            </dx1:WebChartControl>
                        </div>

                    </div>
                    <div class="row" style="margin-top: 40px">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                           <%-- <iframe src="http://ipcamlive.com/player/player.php?alias=5d3b600e34e42&autoplay=true" width="350" height="260" autostart="yes"  frameborder="0" allowfullscreen></iframe>--%>
                            <iframe src="rtsp://admin:tech2014@152.231.39.180/streaming/channels/101" width="350" height="260" autostart="yes"  frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                           
                            <dx1:WebChartControl ID="WebChartControl3" runat="server" Width="360px" Height="260px"
                                CrosshairEnabled="True"
                                SeriesDataMember="Day">
                                <BorderOptions Visibility="False" />
                                <BorderOptions Visibility="False"></BorderOptions>
                                <DiagramSerializable>
                                    <cc1:XYDiagram
                                        RangeControlDateTimeGridOptions-GridMode="ChartGrid">
                                        <AxisX VisibleInPanesSerializable="-1">
                                            <GridLines Visible="True">
                                            </GridLines>
                                        </AxisX>
                                        <AxisY VisibleInPanesSerializable="-1">
                                        </AxisY>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <Titles>
                                    <cc1:ChartTitle Text=""></cc1:ChartTitle>
                                </Titles>
                                <SeriesTemplate ArgumentDataMember="Day" LabelsVisibility="True" CrosshairLabelPattern="{S}:{V}"
                                    ValueDataMembersSerializable="valor">
                                    <LabelSerializable>
                                        <cc1:SideBySideBarSeriesLabel Position="Top" TextPattern="Qty:{V}"
                                            Indent="5" LineLength="10" LineVisibility="True"
                                            ResolveOverlappingMode="Default">
                                        </cc1:SideBySideBarSeriesLabel>
                                    </LabelSerializable>
                                </SeriesTemplate>
                            </dx1:WebChartControl>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                             
                            <dx1:WebChartControl ID="WebChartControl4" runat="server" Width="360px" Height="260px"
                                CrosshairEnabled="True"
                                SeriesDataMember="estado">
                                <BorderOptions Visibility="False" />
                                <BorderOptions Visibility="False"></BorderOptions>
                                <DiagramSerializable>
                                    <cc1:XYDiagram
                                        RangeControlDateTimeGridOptions-GridMode="ChartGrid">
                                        <AxisX VisibleInPanesSerializable="-1">
                                            <GridLines Visible="True">
                                            </GridLines>
                                        </AxisX>
                                        <AxisY VisibleInPanesSerializable="-1">
                                        </AxisY>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <Titles>
                                    <cc1:ChartTitle Text=""></cc1:ChartTitle>
                                </Titles>
                                <SeriesTemplate ArgumentDataMember="Day" LabelsVisibility="True" CrosshairLabelPattern="{S}:{V}"
                                    ValueDataMembersSerializable="valor">
                                    <LabelSerializable>
                                        <cc1:SideBySideBarSeriesLabel Position="Top" TextPattern="Qty:{V}"
                                            Indent="5" LineLength="10" LineVisibility="True"
                                            ResolveOverlappingMode="Default">
                                        </cc1:SideBySideBarSeriesLabel>
                                    </LabelSerializable>
                                </SeriesTemplate>
                            </dx1:WebChartControl>
                        </div>
                    </div>
            </div>

            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12" style="border-left: 2px solid #a1a1a1">
                <div class="col-lg-12" >
                    <h3>Ongoing Log              <a class="alert-link" style="float:right" href="../Reportes">Reports</a> </h3>
                    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" Theme="Metropolis" runat="server" OnDataBinding="ASPxGridView1_DataBinding" 
                        Width="100%" AutoGenerateColumns="False" OnCustomCallback="ASPxGridView1_CustomCallback" Settings-ShowColumnHeaders="true" OnHtmlRowPrepared="ASPxGridView1_HtmlRowPrepared">
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="tiempo" Caption="Hours" Width="100px" />
                            <dx:GridViewDataTextColumn FieldName="style" Caption="Style" />
                            <dx:GridViewDataTextColumn FieldName="NombreMedida" Caption="POM" />
                            <dx:GridViewDataTextColumn FieldName="spec" Caption="Spec" />
                            <dx:GridViewDataTextColumn FieldName="valor" Caption="Measurements"  />
                            <dx:GridViewDataTextColumn Caption="Comment">
                                <DataItemTemplate>
                                    <dx:ASPxLabel ID="lbltexto" runat="server" Text='<%# Eval("texto") %>'>
                                    </dx:ASPxLabel>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                       <Settings VerticalScrollBarMode="Visible" VerticalScrollableHeight="350" />
        
        <SettingsPager Mode="ShowAllRecords" />
                    </dx:ASPxGridView>

                </div>
                <div class="col-lg-12">
                    <h3>Styles Log</h3>
                    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" Theme="Metropolis" runat="server" OnDataBinding="grid_DataBinding"
                        Width="100%" AutoGenerateColumns="False" OnCustomCallback="grid_CustomCallback">
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="Style" SortIndex="0" />
                            <dx:GridViewDataTextColumn FieldName="NombreMedida" Caption="Measurement Points" Settings-AllowCellMerge="False" />
                            <dx:GridViewDataTextColumn FieldName="Passed" Settings-AllowCellMerge="False" />
                            <dx:GridViewDataTextColumn FieldName="Fail" Settings-AllowCellMerge="False" />
                            <dx:GridViewDataTextColumn FieldName="Passedp" Caption="Passed %" CellStyle-ForeColor="Green" Settings-AllowCellMerge="False" />
                            <dx:GridViewDataTextColumn FieldName="Failp" Caption="Fail %" CellStyle-ForeColor="Red" Settings-AllowCellMerge="False" />
                        </Columns>
                        <SettingsPager PageSize="15" />
                        <SettingsBehavior AllowCellMerge="true" />
                    </dx:ASPxGridView>
                </div>
            </div>

        </div>
        </div>
    </form>
</body>
</html>
