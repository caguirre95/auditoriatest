﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ControlAuditoriasEmb.aspx.cs" Inherits="adminRepDefectos_ControlAuditoriasEmb" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "ControlAuditoriasEmb.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porderclient: item.porderclient,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porderclient);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porderclient);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porderclient + "</a>").appendTo(ul);
            };
        });
    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtestilo.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "ControlAuditoriasEmb.aspx/GetStyle",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    estilo: item.estilo,
                                    id: item.id,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtestilo.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtestilo.ClientID%>').val(ui.item.estilo);
                    $('#<%=hdnidstyle.ClientID%>').val(ui.item.id);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });

        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <br />

    <div class="container">

        <div class="col-lg-12">

            <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

            <div class="panel panel-primary">
                <div class="panel-heading">Control de Auditorias</div>
                <div class="panel-body">

                    <div class="col-lg-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="rdl1" runat="server">
                                    <asp:ListItem Value="1">Linea</asp:ListItem>
                                    <asp:ListItem Value="2">Corte</asp:ListItem>
                                    <asp:ListItem Value="3">Estilo</asp:ListItem>
                                    <asp:ListItem Value="4">Status</asp:ListItem>
                                    <asp:ListItem Value="5">Fecha</asp:ListItem>
                                    <%--<asp:ListItem Value="6">Corte-Status</asp:ListItem>--%>
                                    <asp:ListItem Value="6">Line-Status</asp:ListItem>
                                    <asp:ListItem Value="7">Estilo-Status</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <asp:Panel ID="panelLinea" runat="server">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    Linea
                                    <asp:DropDownList ID="drpLinea" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="panelCorte" runat="server">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    Corte
                                    <asp:TextBox ID="txtcorte" runat="server" CssClass="form-control" placeholder="Digite el Corte..."></asp:TextBox>
                                </div>
                            </div>
                        </asp:Panel>

                    </div>
                    <div class="col-lg-3">
                        <asp:Panel ID="panelEstilo" runat="server">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    Estilo
                                    <asp:TextBox ID="txtestilo" runat="server" CssClass="form-control" placeholder="Digite el Estilo..."></asp:TextBox>
                                    <asp:HiddenField ID="hdnidstyle" runat="server" />
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="panelStatus" runat="server">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    Status
                                    <asp:DropDownList ID="drpstatus" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">Select...</asp:ListItem>
                                        <asp:ListItem Value="1">A</asp:ListItem>
                                        <asp:ListItem Value="2">R</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <asp:Panel ID="panelFechas" runat="server">
                                    Fecha Inicial
                                    <asp:TextBox ID="txtfecha1" runat="server" CssClass="form-control"></asp:TextBox>
                                    Fecha Final
                                    <asp:TextBox ID="txtfecha2" runat="server" CssClass="form-control"></asp:TextBox>
                                    <hr />
                                    <div class="col-lg-6" style="padding-left: 0">
                                        <asp:Button ID="btnbuscar" runat="server" Text="Buscar" CssClass="btn btn-primary form-control" OnClick="btnbuscar_Click" />
                                    </div>
                                    <div class="col-lg-6" style="padding-right: 0">
                                        <asp:Button ID="btnlimpiar" runat="server" Text="Limpiar" CssClass="btn btn-success form-control" OnClick="btnlimpiar_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <hr />
                        <div style="text-align: center">
                            <h4><span style="font-weight: bold; color: dodgerblue">CONTROL DE AUDITORIAS</span></h4>
                        </div>
                        <hr />
                        <dx1:ASPxGridView ID="grdcontrolAudit" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdcontrolAudit_DataBinding" OnCustomCallback="grdcontrolAudit_CustomCallback">                          
                            <SettingsSearchPanel Visible="true" />                            
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Control Auditorias" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="Linea" Caption="Linea" Settings-AllowCellMerge="True" CellStyle-HorizontalAlign="Center"
                                            CellStyle-VerticalAlign="Top" CellStyle-Font-Bold="true" CellStyle-BackColor="#6699ff" CellStyle-ForeColor="#ccffcc">
                                        </dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Corte" Caption="Corte" Settings-AllowCellMerge="True" CellStyle-HorizontalAlign="Center"
                                            CellStyle-VerticalAlign="Top" CellStyle-Font-Bold="true" CellStyle-BackColor="#ccccff">
                                        </dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo" Settings-AllowCellMerge="True" CellStyle-HorizontalAlign="Center"
                                            CellStyle-VerticalAlign="Top" CellStyle-Font-Bold="true">
                                        </dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Color" Caption="Color" Settings-AllowCellMerge="True" CellStyle-HorizontalAlign="Center"
                                            CellStyle-VerticalAlign="Top" CellStyle-Font-Bold="true" CellStyle-BackColor="#ccccff">
                                        </dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="codigolote" Caption="Codigo del Lote"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Status" Caption="Status">
                                        </dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="secuencia" Caption="Secuencia"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Operacion" Caption="Operacion" CellStyle-BackColor="#ccccff"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Defectos" Caption="Defectos Encontrados"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Cantidad" Caption="Cantidad Defectos"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewCommandColumn ShowClearFilterButton="true" ShowApplyFilterButton="true" />
                                    </Columns>
                                </dx1:GridViewBandColumn>
                            </Columns>                            
                            <Settings VerticalScrollBarMode="Visible" ShowGroupFooter="VisibleAlways" ShowFooter="true" ShowFilterRow="true" />
                            <SettingsPager PageSize="200"></SettingsPager>
                            <TotalSummary>
                                <dx1:ASPxSummaryItem FieldName="Defectos" SummaryType="Count" DisplayFormat="Cant. Defectos: {0}" />
                                <dx1:ASPxSummaryItem FieldName="Cantidad" SummaryType="Sum" DisplayFormat="Total Defectos: {0}" />
                            </TotalSummary>
                        </dx1:ASPxGridView>
                        <dx1:ASPxGridViewExporter ID="export1" runat="server"></dx1:ASPxGridViewExporter>
                        <br />
                        <asp:Button ID="btnexportar" runat="server" CssClass="btn btn-success form-control" Text="Exportar Información" OnClick="btnexportar_Click" />
                    </div>

                </div>
            </div>

        </div>

    </div>

</asp:Content>

