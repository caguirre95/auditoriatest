﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OperarioDef.aspx.cs" Inherits="adminRepDefectos_OperarioDef" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hdnidporder.ClientID%>').val(ui.item.idorder);
                    // $('#<=txtstyle.ClientID%>').val(ui.item.style);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        })
    </script>
    <script lang="javascript" type="text/javascript">
        function autoestilo() {

            $(function () {
                $('#<%=txtstyle.ClientID%>').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "AqlOql.aspx/GetStyle",
                            data: "{'pref' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        estilo: item.estilo,
                                        id: item.id,
                                        json: item
                                    }
                                }))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                        return false;
                    },
                    select: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);
                        $('#<%=hdnidstyle.ClientID%>').val(ui.item.id);

                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li  >")
                        .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
                };
            })
        }
    </script>

    <script type="text/javascript">
        function date1() {
            $('#<%=txtfecha1.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

        function date2() {
            $('#<%=txtfecha2.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">

        <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

        <div class="panel panel-primary" style="margin-top: 5px">
            <div class="panel-heading">Inspección de Bultos Aprovados y Rechazados</div>
            <div class="panel-body">

                <div class="row">

                    <div class="col-lg-2">
                        <div class="panel-default">
                            <div class="panel-heading">Bultos</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="rdbult" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rdbult_SelectedIndexChanged">
                                    <asp:ListItem Value="1">Bultos Aprovados</asp:ListItem>
                                    <asp:ListItem Value="2">Bultos Rechazados</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel-default">
                            <div class="panel-heading">Auditorias</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="rdaudit" runat="server">
                                    <%--<asp:ListItem Value="1">Auditoria Interna</asp:ListItem>--%>
                                    <%--<asp:ListItem Value="2">Auditoria Externa</asp:ListItem>--%>
                                    <%--<asp:ListItem Value="3">Auditoria Bulto a Bulto</asp:ListItem>--%>
                                    <asp:ListItem Value="4">Auditoria Inspeccion - Proceso</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>

                    <asp:UpdatePanel ID="upd1" runat="server">
                        <ContentTemplate>
                            <div class="col-lg-2">
                                <div class="panel-default">
                                    <div class="panel-heading">Filtros</div>
                                    <div class="panel-body">

                                        <asp:RadioButtonList ID="rd1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rd1_SelectedIndexChanged">
                                            <asp:ListItem Value="1">Cut</asp:ListItem>
                                            <asp:ListItem Value="2">Style</asp:ListItem>
                                            <asp:ListItem Value="3">Line</asp:ListItem>
                                            <asp:ListItem Value="4">Plant</asp:ListItem>
                                        </asp:RadioButtonList>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="panel-default">
                                    <div class="panel-heading">Opciones</div>
                                    <div class="panel-body">
                                        <asp:Panel runat="server" ID="panelCorte">
                                            Cut:                                   
                                    <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" AutoCompleteType="Disabled" Font-Size="14px" runat="server"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdnidporder" />
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="panelLinea">
                                            Line:
                                     <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="panelEstilo">
                                            Style:                                   
                                    <asp:TextBox ID="txtstyle" onkeypress="autoestilo()" AutoCompleteType="Disabled" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdnidstyle" />
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="panelPlanta">
                                            Plant:
                                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="col-lg-3">
                        <div class="panel-default">
                            <div class="panel-heading">Select filter Date</div>
                            <div class="panel-body">
                                <asp:Panel runat="server" ID="panelfechas">
                                    Start Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                    End Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>

                                </asp:Panel>
                                <div style="padding-top: 15px">
                                    <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" runat="server" OnClick="btngenerar_Click" />
                                </div>
                                <div style="padding-top: 15px">
                                    <asp:Button Text="Limpiar Campos" CssClass="btn btn-info form-control" ID="btnlimpiar" runat="server" OnClick="btnlimpiar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <hr />
                        <asp:Panel ID="aprovado_p" runat="server">
                            <dx1:ASPxGridView ID="grddef" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grddef_DataBinding" OnCustomCallback="grddef_CustomCallback">
                                <Columns>
                                    <dx1:GridViewDataTextColumn FieldName="Corte" Caption="Corte"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Bulto" Caption="#Bulto"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Inspector" Caption="Inspector"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Linea" Caption="Linea"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Modulo" Caption="Modulo"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha"></dx1:GridViewDataTextColumn>
                                </Columns>
                            </dx1:ASPxGridView>

                            <div style="padding-top: 20px">
                                <asp:Button ID="btnexcel" runat="server" Text="Export to Excel" CssClass="btn btn-success form-control" OnClick="btnexcel_Click" />
                            </div>
                            <dx1:ASPxGridViewExporter ID="export" runat="server"></dx1:ASPxGridViewExporter>
                        </asp:Panel>
                        <br />
                        <asp:Panel ID="rechazado_p" runat="server">
                            <dx1:ASPxGridView ID="grdrec" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdrec_DataBinding" OnCustomCallback="grdrec_CustomCallback">
                                <Columns>
                                    <dx1:GridViewDataTextColumn FieldName="Corte" Caption="Corte"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Bulto" Caption="Bulto"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Inspector" Caption="Inspector"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Linea" Caption="Linea"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Defecto" Caption="Defecto"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Modulo" Caption="Modulo"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Operario" Caption="Operario"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Operacion" Caption="Operacion"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="TotalDef" Caption="#Defecto"></dx1:GridViewDataTextColumn>
                                </Columns>
                            </dx1:ASPxGridView>

                            <div style="padding-top: 20px">
                                <asp:Button ID="btne" runat="server" Text="Export to Excel" CssClass="btn btn-success form-control" OnClick="btne_Click" />
                            </div>
                            <dx1:ASPxGridViewExporter ID="export1" runat="server"></dx1:ASPxGridViewExporter>
                        </asp:Panel>

                    </div>

                </div>

            </div>

        </div>

    </div>

</asp:Content>

