﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AqlOqlAI.aspx.cs" Inherits="adminRepDefectos_AqlOqlAI" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hdnidporder.ClientID%>').val(ui.item.idorder);
                    // $('#<=txtstyle.ClientID%>').val(ui.item.style);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        })
    </script>
    <script lang="javascript" type="text/javascript">
        function autoestilo() {

            $(function () {
                $('#<%=txtstyle.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "AqlOql.aspx/GetStyle",
                        data: "{'pref' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    estilo: item.estilo,
                                    id: item.id,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);
                    $('#<%=hdnidstyle.ClientID%>').val(ui.item.id);

                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li  >")
                        .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
                };
            })
        }
    </script>

    <script type="text/javascript">
        function date1() {

           <%-- $('#<%=txtfecha1.ClientID%>').val("");--%>

            $('#<%=txtfecha1.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });

        }

        function date2() {

            $('#<%=txtfecha2.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });


        }

       <%-- $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

         

        });--%>

</script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <%--<link href="../css/bootstrap.min.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Measurement Report </strong></h3>
            </div>
            <div class="panel-body">
                <div class="col-lg-12">
                    <div class="col-lg-8">
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">Select search filter</div>
                                <div class="panel-body">
                                    <asp:RadioButtonList ID="RadioButtonList1" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" runat="server">
                                        <asp:ListItem Value="1">Cut</asp:ListItem>
                                        <asp:ListItem Value="2">Style</asp:ListItem>
                                        <asp:ListItem Value="3">Line</asp:ListItem>
                                        <asp:ListItem Value="4">Plant</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <asp:Panel runat="server" ID="panelCorte">
                                Cut:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></span>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                        <asp:HiddenField runat="server" ID="hdnidporder" />
                                    </div>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="panelLinea">
                                Line:
                                     <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="panelEstilo">
                                Style:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></span>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtstyle" onkeypress="autoestilo()" AutoCompleteType="None" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                <asp:HiddenField runat="server" ID="hdnidstyle" />
                            </asp:Panel>
                            <asp:Panel runat="server" ID="panelPlanta">
                                Plant:
                                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>
                            </asp:Panel>


                        </div>

                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select filter Date</div>
                            <div class="panel-body">
                                <asp:Panel runat="server" ID="panelfechas">
                                    Start Date
                                    <dx1:ASPxDateEdit  runat="server" ID="txtfecha1"  Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%" ></dx1:ASPxDateEdit>    
                                    <%--<div class="input-group">

                                        <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                        <asp:TextBox ID="txtfecha1" onfocus="date1();"  CssClass="form-control" runat="server"></asp:TextBox>

                                    </div>--%>
                                    End Date
                                    <dx1:ASPxDateEdit  runat="server" ID="txtfecha2"  Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%" ></dx1:ASPxDateEdit>
                                   <%-- <div class="input-group">

                                        <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                        <asp:TextBox ID="txtfecha2" onfocus="date2();" CssClass="form-control" runat="server"></asp:TextBox>

                                    </div>--%>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="col-lg-4" style="padding-top: 10px; padding-bottom: 10px">
                        <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" OnClick="btngenerar_Click" runat="server" />
                    </div>
                   <%-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6" style="padding-top: 10px; padding-bottom: 10px">
                        <a href="#ventanaG" data-toggle="modal" class="btn btn-md btn-success"><span class="glyphicon glyphicon-align-justify"></span>Processed Styles In The Week </a>
                    </div>--%>
                </div>

                <div class="col-lg-6">
                    <dx:WebChartControl ID="WebChartAql" runat="server" Height="400px"
                        Width="500px" AppearanceName="Pastel Kit" PaletteName="Metro"
                        ClientInstanceName="chart"
                        ToolTipEnabled="False" CrosshairEnabled="True">
                        <Legend Name="Default Legend"></Legend>
                        <SeriesSerializable>
                            <cc1:Series Name="AQL" ArgumentDataMember="aq" ValueDataMembersSerializable="valora" LegendTextPattern="{A}">

                                <ViewSerializable>
                                    <cc1:DoughnutSeriesView Rotation="90" ExplodeMode="All" RuntimeExploding="True">
                                    </cc1:DoughnutSeriesView>
                                </ViewSerializable>
                                <LabelSerializable>

                                    <cc1:DoughnutSeriesLabel Position="TwoColumns" ColumnIndent="20" TextColor="Black" BackColor="Transparent" Font="Tahoma, 8pt, style=Bold" TextPattern="{VP:P0}">
                                        <Border Visibility="False"></Border>
                                    </cc1:DoughnutSeriesLabel>
                                </LabelSerializable>
                            </cc1:Series>
                        </SeriesSerializable>

                        <BorderOptions Visibility="False" />
                        <Titles>
                            <cc1:ChartTitle Text="AQL"></cc1:ChartTitle>
                            <cc1:ChartTitle Dock="Bottom" Alignment="Far" Text="Rocedes" Font="Tahoma, 8pt" TextColor="Gray"></cc1:ChartTitle>
                        </Titles>
                        <DiagramSerializable>
                            <cc1:SimpleDiagram></cc1:SimpleDiagram>
                        </DiagramSerializable>
                    </dx:WebChartControl>
                </div>

                <div class="col-lg-6">

                    <dx:WebChartControl ID="WebChartOql" runat="server" Height="400px"
                        Width="500px" AppearanceName="Pastel Kit" PaletteName="Metro"
                        ClientInstanceName="chart"
                        ToolTipEnabled="False" CrosshairEnabled="True">
                        <Legend Name="Default Legend"></Legend>
                        <SeriesSerializable>
                            <cc1:Series Name="OQL" ArgumentDataMember="oq" ValueDataMembersSerializable="valoro" LegendTextPattern="{A}">

                                <ViewSerializable>
                                    <cc1:DoughnutSeriesView Rotation="90" ExplodeMode="All" RuntimeExploding="True">
                                    </cc1:DoughnutSeriesView>
                                </ViewSerializable>
                                <LabelSerializable>
                                    <cc1:DoughnutSeriesLabel Position="TwoColumns" ColumnIndent="20" TextColor="Black" BackColor="Transparent" Font="Tahoma, 8pt, style=Bold" TextPattern="{VP:P0}">
                                        <Border Visibility="False"></Border>
                                    </cc1:DoughnutSeriesLabel>
                                </LabelSerializable>
                            </cc1:Series>
                        </SeriesSerializable>

                        <BorderOptions Visibility="False" />
                        <Titles>
                            <cc1:ChartTitle Text="OQL"></cc1:ChartTitle>
                            <cc1:ChartTitle Dock="Bottom" Alignment="Far" Text="Rocedes" Font="Tahoma, 8pt" TextColor="Gray"></cc1:ChartTitle>
                        </Titles>
                        <DiagramSerializable>
                            <cc1:SimpleDiagram></cc1:SimpleDiagram>
                        </DiagramSerializable>
                    </dx:WebChartControl>


                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

