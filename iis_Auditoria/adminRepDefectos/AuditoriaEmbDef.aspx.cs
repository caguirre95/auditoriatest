﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_AuditoriaEmbDef : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["embarque"] = null;
            Session["embarquedef"] = null;
            cargacombo();
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();
        }
    }

    void cargacombo()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta, descripcion FROM Planta order by descripcion asc";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            var embarque1 = new DataTable();

            string query = "";

            string opcionfi = rd1.SelectedValue;

            switch (opcionfi)
            {
                case "1"://Corte
                    if (hdnidporder.Value != string.Empty)
                    {
                        panel1.Visible = true;
                        panel2.Visible = false;

                        query = " select a.Planta, b.Auditor, a.Corte, a.Quantity, a.Estilo, a.Color, a.Linea, b.codigoLote, b.Aud, b.Estado, b.lote, b.muestra, b.Codigo, b.Defecto, b.unidad, b.Posicion Operacion,"
                             + " b.comentario Observacion, b.Fecha from (select pl.descripcion Planta, l.numero Linea, p.Id_Order, p.POrder Corte, p.Quantity, s.Style Estilo, MIN(b.Color) Color from POrder p"
                             + " join Bundle b on b.Id_Order = p.Id_Order join Style s on s.Id_Style = p.Id_Style join Linea l on l.id_linea = p.Id_Linea2 join Planta pl on pl.id_planta = l.id_planta"
                             + " where p.Id_Order = " + hdnidporder.Value + " group by pl.descripcion, l.numero, p.Id_Order, p.POrder, p.Quantity, s.Style) a join"
                             + " (select p.Id_Order, p.POrder, na.nombreAuditor Auditor, ae.codigoLote, na.secuencia Aud, case when na.estadoAudit = 0 then 'Rechazado' else 'Aprobado' end Estado, ae.lote, ae.muestra,"
                             + " ISNULL(SUM(ed.unidad), 0) CantDefectos, d.Codigo, d.Defecto, ed.unidad, po.Posicion, convert(date, na.HoraFinal) Fecha, ae.comentario from tbAuditEmbarque ae"
                             + " join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join tbAuditEmbarqueDefectos ed on ed.idAuditEmbarque = na.id"
                             + " join tbDefectos d on d.idDefecto = ed.idDefecto join tbPosicion po on po.idPosicion = ed.idPosicion where p.Id_Order = " + hdnidporder.Value + ""
                             + " and convert(date, na.horaFinal) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Order, p.POrder, na.nombreAuditor, ae.codigoLote, na.secuencia, na.estadoAudit,"
                             + " ae.lote, ae.muestra, d.Codigo, d.Defecto, ed.unidad, po.Posicion, convert(date, na.HoraFinal), ae.comentario) b on a.Id_Order = b.Id_Order group by a.Planta, b.Auditor, a.Corte,"
                             + " a.Quantity, a.Estilo, a.Color, a.Linea, b.codigoLote, b.Aud, b.Estado, b.lote, b.muestra, b.Codigo, b.Defecto, b.unidad, b.Posicion, b.comentario, b.Fecha";

                        embarque1 = DataAccess.Get_DataTable(query);
                        Session["embarque"] = embarque1;
                        grdembarque.DataSource = embarque1;
                        grdembarque.DataBind();
                    }

                    break;

                case "3"://Linea
                    if (DropDownListLine.SelectedItem.Text != "Select...")
                    {
                        panel1.Visible = true;
                        panel2.Visible = false;

                        query = " select a.Planta, b.Auditor, a.Corte, a.Quantity, a.Estilo, a.Color, a.Linea, b.codigoLote, b.Aud, b.Estado, b.lote, b.muestra, b.Codigo, b.Defecto, b.unidad, b.Posicion Operacion,"
                              + " b.comentario Observacion, b.Fecha from (select pl.descripcion Planta, l.numero Linea, p.Id_Order, p.POrder Corte, p.Quantity, s.Style Estilo, MIN(b.Color) Color from POrder p"
                              + " join Bundle b on b.Id_Order = p.Id_Order join Style s on s.Id_Style = p.Id_Style join Linea l on l.id_linea = p.Id_Linea2 join Planta pl on pl.id_planta = l.id_planta"
                              + " where l.id_linea = " + DropDownListLine.SelectedValue + " group by pl.descripcion, l.numero, p.Id_Order, p.POrder, p.Quantity, s.Style) a join"
                              + " (select p.Id_Order, p.POrder, na.nombreAuditor Auditor, ae.codigoLote, na.secuencia Aud, case when na.estadoAudit = 0 then 'Rechazado' else 'Aprobado' end Estado, ae.lote, ae.muestra,"
                              + " ISNULL(SUM(ed.unidad), 0) CantDefectos, d.Codigo, d.Defecto, ed.unidad, po.Posicion, convert(date, na.HoraFinal) Fecha, ae.comentario from tbAuditEmbarque ae"
                              + " join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join tbAuditEmbarqueDefectos ed on ed.idAuditEmbarque = na.id"
                              + " join tbDefectos d on d.idDefecto = ed.idDefecto join tbPosicion po on po.idPosicion = ed.idPosicion where p.Id_Linea2 = " + DropDownListLine.SelectedValue + ""
                              + " and convert(date, na.horaFinal) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Order, p.POrder, na.nombreAuditor, ae.codigoLote, na.secuencia, na.estadoAudit,"
                              + " ae.lote, ae.muestra, d.Codigo, d.Defecto, ed.unidad, po.Posicion, convert(date, na.HoraFinal), ae.comentario) b on a.Id_Order = b.Id_Order group by a.Planta, b.Auditor, a.Corte,"
                              + " a.Quantity, a.Estilo, a.Color, a.Linea, b.codigoLote, b.Aud, b.Estado, b.lote, b.muestra, b.Codigo, b.Defecto, b.unidad, b.Posicion, b.comentario, b.Fecha";

                        embarque1 = DataAccess.Get_DataTable(query);
                        Session["embarque"] = embarque1;
                        grdembarque.DataSource = embarque1;
                        grdembarque.DataBind();
                    }

                    break;

                case "4"://Planta
                    if (DropDownListPlant.SelectedItem.Text != "Select...")
                    {
                        panel1.Visible = true;
                        panel2.Visible = false;

                        //query = " select a.Planta, b.Auditor, a.Corte, a.Quantity, a.Estilo, a.Color, a.Linea, b.codigoLote, b.Aud, b.Estado, b.lote, b.muestra, b.Codigo, b.Defecto, b.unidad, b.Posicion Operacion,"
                        //      + " b.comentario Observacion, b.Fecha from (select pl.descripcion Planta, l.numero Linea, p.Id_Order, p.POrder Corte, p.Quantity, s.Style Estilo, MIN(b.Color) Color from POrder p"
                        //      + " join Bundle b on b.Id_Order = p.Id_Order join Style s on s.Id_Style = p.Id_Style join Linea l on l.id_linea = p.Id_Linea2 join Planta pl on pl.id_planta = l.id_planta"
                        //      + " where pl.id_planta = " + DropDownListPlant.SelectedValue + " group by pl.descripcion, l.numero, p.Id_Order, p.POrder, p.Quantity, s.Style) a join"
                        //      + " (select p.Id_Order, p.POrder, na.nombreAuditor Auditor, ae.codigoLote, na.secuencia Aud, case when na.estadoAudit = 0 then 'Rechazado' else 'Aprobado' end Estado, ae.lote, ae.muestra,"
                        //      + " ISNULL(SUM(ed.unidad), 0) CantDefectos, d.Codigo, d.Defecto, ed.unidad, po.Posicion, convert(date, na.HoraFinal) Fecha, ae.comentario from tbAuditEmbarque ae"
                        //      + " join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join tbAuditEmbarqueDefectos ed on ed.idAuditEmbarque = na.id"
                        //      + " join tbDefectos d on d.idDefecto = ed.idDefecto join tbPosicion po on po.idPosicion = ed.idPosicion where p.Id_Planta = " + DropDownListPlant.SelectedValue + ""
                        //      + " and convert(date, na.horaFinal) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Order, p.POrder, na.nombreAuditor, ae.codigoLote, na.secuencia, na.estadoAudit,"
                        //      + " ae.lote, ae.muestra, d.Codigo, d.Defecto, ed.unidad, po.Posicion, convert(date, na.HoraFinal), ae.comentario) b on a.Id_Order = b.Id_Order group by a.Planta, b.Auditor, a.Corte,"
                        //      + " a.Quantity, a.Estilo, a.Color, a.Linea, b.codigoLote, b.Aud, b.Estado, b.lote, b.muestra, b.Codigo, b.Defecto, b.unidad, b.Posicion, b.comentario, b.Fecha";
                        query = string.Format(@"SELECT a.Planta, b.Auditor, a.Corte, a.Quantity, a.Estilo, a.Color, a.Linea, b.codigoLote, b.Aud, b.Estado, b.lote, b.muestra, b.Codigo, b.Defecto, b.unidad, b.Posicion Operacion,
	                                            b.comentario Observacion, b.Fecha, CONVERT(VARCHAR(8),b.horaInicial, 108) HoraInicial, CONVERT(VARCHAR(8), b.horaFinal, 108) HoraFinal
                                                FROM (
		                                            SELECT pl.descripcion Planta, l.numero Linea, p.Id_Order, p.POrder Corte, p.Quantity, s.Style Estilo, MIN(b.Color) Color 
			                                            FROM POrder p
			                                            join Bundle b on b.Id_Order = p.Id_Order join Style s on s.Id_Style = p.Id_Style join Linea l on l.id_linea = p.Id_Linea2 
			                                            join Planta pl on pl.id_planta = l.id_planta
			                                            where pl.id_planta =  {0}
			                                            group by pl.descripcion, l.numero, p.Id_Order, p.POrder, p.Quantity, s.Style
	                                            ) a join
	                                            (
		                                            SELECT p.Id_Order, p.POrder, na.nombreAuditor Auditor, ae.codigoLote, na.secuencia Aud, CASE WHEN na.estadoAudit = 0 then 'Rechazado' else 'Aprobado' end Estado, ae.lote, ae.muestra,
			                                            ISNULL(SUM(ed.unidad), 0) CantDefectos, d.Codigo, d.Defecto, ed.unidad, po.Posicion, convert(date, na.HoraFinal) Fecha, ae.comentario, na.horaInicial, na.horaFinal
			                                            FROM tbAuditEmbarque ae
			                                            join POrder p on p.Id_Order = ae.idcorte
			                                            join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join tbAuditEmbarqueDefectos ed on ed.idAuditEmbarque = na.id
			                                            join tbDefectos d on d.idDefecto = ed.idDefecto join tbPosicion po on po.idPosicion = ed.idPosicion 
			                                            WHERE p.Id_Planta =  {0}
			                                            AND convert(date, na.horaFinal) BETWEEN '{1}' AND '{2}'
			                                            GROUP BY p.Id_Order, p.POrder, na.nombreAuditor, ae.codigoLote, na.secuencia, na.estadoAudit, ae.lote, ae.muestra, d.Codigo, d.Defecto, ed.unidad, po.Posicion, convert(date, na.HoraFinal), ae.comentario,
			                                            na.horaInicial, na.horaFinal
	                                            ) b on a.Id_Order = b.Id_Order
	                                            group by a.Planta, b.Auditor, a.Corte, a.Quantity, a.Estilo, a.Color, a.Linea, b.codigoLote, b.Aud, b.Estado, b.lote, b.muestra, b.Codigo, b.Defecto, b.unidad, b.Posicion, b.comentario, b.Fecha, b.horaInicial, b.horaFinal", DropDownListPlant.SelectedValue, txtfecha1.Text, txtfecha2.Text);


                        embarque1 = DataAccess.Get_DataTable(query);

                        Session["embarque"] = embarque1;
                        grdembarque.DataSource = embarque1;
                        grdembarque.DataBind();
                    }

                    break;

                case "5"://Deficit Embarque
                    if (DropDownListPlant.SelectedItem.Text != "Select...")
                    {
                        panel1.Visible = false;
                        panel2.Visible = true;

                        query = " select a.POrder, a.Quantity, isnull(a.AuditadoEnFechas,0) AuditadoFechas, isnull(b.TotalLote,0) TotalAuditado, (a.Quantity - isnull(b.TotalLote,0)) Deficit"
                          + " from (select p.POrder, isnull(p.Quantity, 0) Quantity, ISNULL(SUM(ae.lote),0) AuditadoEnFechas from tbAuditEmbarque ae join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                          + " join POrder p on p.Id_Order = ae.idcorte where na.secuencia = 1 and p.Id_Planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date,na.horaFinal) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                          + " group by p.POrder, p.Quantity) a join (select p.POrder, isnull(SUM(lote), 0) TotalLote from tbAuditEmbarque ae join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                          + " join POrder p on p.Id_Order = ae.idcorte where na.secuencia = 1 and p.Id_Planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date,na.horaFinal) <= '" + txtfecha2.Text + "'"
                          + " group by p.POrder, p.Quantity) b on a.POrder = b.POrder order by a.POrder";

                        embarque1 = DataAccess.Get_DataTable(query);
                        Session["embarquedef"] = embarque1;
                        griddeficit.DataSource = embarque1;
                        griddeficit.DataBind();
                    }

                    break;

                default: break;
            }

        }
        catch (Exception)
        {
            //throw;
        }
    }

    protected void grdembarque_DataBinding(object sender, EventArgs e)
    {
        grdembarque.DataSource = Session["embarque"];
    }

    protected void grdembarque_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdembarque.DataBind();
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/AuditoriaEmbDef.aspx");
    }

    protected void btnexp_Click(object sender, EventArgs e)
    {
        try
        {
            export.GridViewID = "grdembarque";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = export;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Auditoria Embarque";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Embarque" + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btn1_Click(object sender, EventArgs e)
    {
        try
        {
            export1.GridViewID = "griddeficit";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link2.Component = export1;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link2 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Auditoria Embarque";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Embarque" + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void griddeficit_DataBinding(object sender, EventArgs e)
    {
        griddeficit.DataSource = Session["embarquedef"];
    }

    protected void griddeficit_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        griddeficit.DataBind();
    }
}