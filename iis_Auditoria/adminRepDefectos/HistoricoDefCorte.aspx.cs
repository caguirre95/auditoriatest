﻿using DevExpress.Web;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_HistoricoDefCorte : System.Web.UI.Page
{
    class ClassArrm
    {
        public int id { get; set; }
        public string semana { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Session["grdembdef"] = null;               

                getgrid();
            }

            gridpack.SettingsDetail.ExportMode = GridViewDetailExportMode.Expanded;// Enum.Parse(typeof(GridViewDetailExportMode), ddlExportMode.Text);
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void gridpack_DataBinding(object sender, EventArgs e)
    {
        try
        {
            gridpack.DataSource = Session["grdembdef"];
            gridpack.GroupBy(gridpack.Columns["Planta"]);
            gridpack.GroupBy(gridpack.Columns["Linea"]);

        }
        catch (Exception ex)
        {
            string j = ex.Message;
        }
    }

    protected void gridpack_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridpack.DataBind();
    }

    void getgrid()
    {
        if (drpplanta.SelectedItem.Text != "Select...")
        {
            Session["grdembdef"] = null;

            var dt = OrderDetailDA.ReporteDefectocorteProceso(int.Parse(drpplanta.SelectedValue));

            Session["grdembdef"] = dt;
            gridpack.DataBind();
            gridpack.ExpandAll();

        }

    }

    protected void btnExport_Click(object sender, EventArgs e)
    {

        ASPxGridViewExporter1.GridViewID = "gridpack";

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link1.Component = ASPxGridViewExporter1;


        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link1 });

        compositeLink.CreatePageForEachLink();

        using (MemoryStream stream = new MemoryStream())
        {

            XlsxExportOptions options = new XlsxExportOptions();

            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
            options.SheetName = "Historial_de_CorteDef";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=Historial de Corte_Def.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        ps.Dispose();

    }

    protected void grid_Load(object sender, EventArgs e)
    {

    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            getgrid();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void grdlote_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        int id = (int)detailGrid.GetMasterRowKeyValue();

        var dt = OrderDetailDA.ReporteDefectoLote(id);//Lote

        detailGrid.DataSource = dt;
    }

    protected void grddefecto_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        int id = (int)detailGrid.GetMasterRowKeyValue();

        var dt = OrderDetailDA.ReporteDefectoHistorico(id);//Defecto

        detailGrid.DataSource = dt;
    }

    protected void grddfimg_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        int id = (int)detailGrid.GetMasterRowKeyValue();

        var dt = OrderDetailDA.ReporteDefectoImagen(id);//Img

        detailGrid.DataSource = dt;
    }    

    protected void LinkButton1_Click(object sender, EventArgs e)
    {

        if (HiddenFieldCortesId.Value != "")
        {

            Session["grdembdef"] = null;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            string json = "[" + HiddenFieldCortesId.Value + "]";

            var result = json_serializer.Deserialize<List<order>>(json);


            var r = result.Select(x => x.idorder).ToArray();

            var cadena = string.Join(",", r);

            //Console.WriteLine(cadena);

            var dt = OrderDetailDA.ReportProcesoDeCorte(cadena);
            HiddenFieldCortesId.Value = "";
            Session["grdembdef"] = dt;
            gridpack.DataBind();
            gridpack.ExpandAll();
            gridpack.DetailRows.ExpandAllRows();
        }
    }

    public class order
    {
        public int idorder { get; set; }
        public string porder { get; set; }
    }

    public void img_estado_Init(object sender, EventArgs e)
    {
        ASPxImage img_estado = (ASPxImage)sender;
        GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)img_estado.NamingContainer;
        string value = (string)DataBinder.Eval(container.DataItem, "ruta");
    }
    public string GetImageUrl(string ruta)
    {
        ruta = ruta.Replace("C:\\inetpub\\wwwroot\\LOCALWEB", "~");
        string url = ruta.Replace("\\", "//");
        return url;        
    }    
}