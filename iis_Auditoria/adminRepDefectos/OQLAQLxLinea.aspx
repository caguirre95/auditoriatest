﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OQLAQLxLinea.aspx.cs" Inherits="adminRepDefectos_OQLAQLxLinea" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <link href="../css/font-awesome.css" rel="stylesheet" />

    <style>
        .s {
            margin-top: 10px;
        }

        .ne {
            font-weight: bold;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <br />
    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading">Measurement Report </div>
            <div class="panel-body">

                <div class="col-lg-12">

                    <div class="col-lg-4">
                        <div class="panel-default">
                            <div class="panel-heading">Filtros</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="rd1" runat="server">
                                    <asp:ListItem Value="1">Auditoria Interna</asp:ListItem>
                                    <asp:ListItem Value="2">Auditoria Externa</asp:ListItem>
                                    <asp:ListItem Value="3">Auditoria Bulto a Bulto</asp:ListItem>
                                    <asp:ListItem Value="4">Auditoria Inspeccion - Proceso</asp:ListItem>
                                </asp:RadioButtonList>
                                <br />
                                Linea
                                <asp:DropDownList ID="drplinea" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                                <%--Linea
                                <asp:DropDownList ID="drpline" runat="server" CssClass="form-control"></asp:DropDownList>--%>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel-default">
                            <div class="panel-heading">Fechas</div>
                            <div class="panel-body">
                                Start Date
                                <%--<asp:TextBox ID="txtfecha1" runat="server" AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>--%>
                                <dx1:ASPxDateEdit ID="txtfecha1" runat="server" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                Final Date
                                <%--<asp:TextBox ID="txtfecha2" runat="server" AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>--%>
                                <dx1:ASPxDateEdit ID="txtfecha2" runat="server" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <div class="col-lg-12">
                                    <asp:Button ID="btnbuscar" runat="server" Text="Buscar" CssClass="btn btn-primary form-control s" OnClick="btnbuscar_Click" />

                                    <asp:Button ID="btnexcel" runat="server" Text="Exportar" CssClass="btn btn-success form-control s" OnClick="btnexcel_Click" />

                                    <asp:Button ID="btnclean" runat="server" Text="Limpiar" CssClass="btn btn-info form-control s" OnClick="btnclean_Click" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-lg-12">
                                    <asp:Label ID="lblplan" runat="server" Text="" CssClass="ne"></asp:Label>
                                </div>

                                <div class="col-lg-12">
                                    <asp:Label ID="lblprocess" runat="server" Text="" CssClass="ne"></asp:Label>
                                </div>

                                <dx:WebChartControl ID="WebChartControl2" runat="server" Width="1020px" Height="360px" CrosshairEnabled="True" SeriesDataMember="quality">
                                    <BorderOptions Visibility="False" />
                                    <BorderOptions Visibility="False"></BorderOptions>
                                    <DiagramSerializable>
                                        <cc1:XYDiagram RangeControlDateTimeGridOptions-GridMode="ChartGrid">                                            
                                            <AxisX VisibleInPanesSerializable="-1">
                                                <GridLines Visible="True">
                                                </GridLines>
                                            </AxisX>
                                            <AxisY VisibleInPanesSerializable="-1">
                                            </AxisY>
                                        </cc1:XYDiagram>
                                    </DiagramSerializable>
                                    <Titles>
                                        <cc1:ChartTitle Text="AQL / OQL Line"></cc1:ChartTitle>
                                    </Titles>
                                    <SeriesTemplate ArgumentDataMember="Linea" LabelsVisibility="True" CrosshairLabelPattern="{S} : {V} %" ValueDataMembersSerializable="porcentaje">
                                        <LabelSerializable>
                                            <cc1:SideBySideBarSeriesLabel Position="Top" TextPattern="Qty:{V} %" Indent="5" LineLength="10" LineVisibility="True" ResolveOverlappingMode="Default">
                                            </cc1:SideBySideBarSeriesLabel>
                                        </LabelSerializable>
                                    </SeriesTemplate>
                                </dx:WebChartControl>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

</asp:Content>

