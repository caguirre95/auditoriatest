﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_TopDefectos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                cargacombo();
                Session["infobb"] = null;
                Session["infoeb"] = null;
                
                txtfecha1.Text = DateTime.Now.ToShortDateString();
                txtfecha2.Text = DateTime.Now.ToShortDateString();

                ASPxDateEdit1.Text = DateTime.Now.ToShortDateString();
                ASPxDateEdit2.Text = DateTime.Now.ToShortDateString();

                Session["rep1"] = null;
                Session["repChr"] = null;
                Session["Todas"] = null;
                lblaudit.Text = "";
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void rd1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string id = rd1.SelectedValue;

            switch (id)
            {
                case "1":
                    panelCorte.Visible = true;
                    panelEstilo.Visible = false;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = false;
                    break;
                case "2":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = true;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;
                case "3":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = false;
                    panelLinea.Visible = true;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;
                case "4":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = false;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = true;
                    panelfechas.Visible = true;
                    break;

                case "5":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = true;
                    panelLinea.Visible = true;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    void VerInfo()
    {
        string sqlQuery = " select a.nombre, a.POrder, a.Aprobado Estatus from (select i.nombre, p.POrder, 'Aprobado' 'Aprobado' from POrder p"
                        + " join Bundle b on b.Id_Order = p.Id_Order join tbBultoaBultoAprovados ba on ba.IdBulto = b.Id_Bundle join inspector i on i.id_inspector = ba.idInspector"
                        + " where convert(date,ba.FechaInspeccion) between '" + ASPxDateEdit1.Text + "' and '" + ASPxDateEdit2.Text + "'"
                        + " group by p.POrder, i.nombre union select i.nombre, p.POrder, 'Rechazado' 'Rechazado' from POrder p join Bundle b on b.Id_Order = p.Id_Order"
                        + " join tbBultoaBultoRechazado br on br.IdBulto = b.Id_Bundle join inspector i on i.id_inspector = br.idInspector"
                        + " where convert(date,br.FechaInspeccion) between '" + ASPxDateEdit1.Text + "' and '" + ASPxDateEdit2.Text + "'"
                        + " group by p.POrder, i.nombre) a order by a.Aprobado, a.nombre";

        DataTable dt_bb = DataAccess.Get_DataTable(sqlQuery);
        Session["infobb"] = dt_bb;
        grdpiezas.DataSource = Session["infobb"];
        grdpiezas.DataBind();

        string sqlQuery1 = " select na.nombreAuditor, p.POrder from POrder p join tbAuditEmbarque a on a.idcorte = p.Id_Order join tbNumeroAuditoria na on na.idAuditoriaEmbarque = a.id"
                         + " where convert(date,na.horaInicial) between '" + ASPxDateEdit1.Text + "' and '" + ASPxDateEdit2.Text + "'";

        DataTable dt_eb = DataAccess.Get_DataTable(sqlQuery1);
        Session["infoeb"] = dt_eb;
        grdpiezas2.DataSource = Session["infoeb"];
        grdpiezas2.DataBind();
    }

    void cargacombo()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta, descripcion FROM Planta order by descripcion asc";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/TopDefectos.aspx");
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            panel1.Visible = true;
            panel2.Visible = false;
            var defect = new DataTable();
            var defectG = new DataTable();
            string query = "";
            string query1 = "";
            int n = 0;

            string op1 = rdaudit.SelectedValue;

            string opcionfi = rd1.SelectedValue;

            switch (op1)
            {
                case "1"://Auditoria Interna
                    switch (opcionfi)
                    {
                        case "1":
                            if (txtPorder.Text != "" && hdnidporder.Value != "")
                            {
                                int idorder = Convert.ToInt32(hdnidporder.Value);
                                query = " select top(5) pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbAuditInterRechazadoDet bb"
                                      + " join tbAuditInterRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado join tbDefectos d on d.idDefecto = bb.idDefectos"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion"
                                      + " join Bundle b on b.Id_Bundle = bbr.idBulto"
                                      + " join POrder p on p.Id_Order = b.Id_Order"
                                      + " where p.Id_Order = " + idorder + " and bb.NumAuditoria=1"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "2":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "")
                            {
                                int idstyle = Convert.ToInt32(hdnidstyle.Value);
                                query = " select top(5) pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbAuditInterRechazadoDet bb"
                                      + " join tbAuditInterRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join tbPosicion pos on pos.idPosicion = bb.idPosicion"
                                      + " join Bundle b on b.Id_Bundle = bbr.idBulto join POrder p on p.Id_Order = b.Id_Order"
                                      + " join Style s on s.Id_Style = p.Id_Style where s.Id_Style = " + idstyle
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "3":
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idlinea = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select top(5) pos.Posicion as Posicion, d.Defecto as Defecto, sum(bb.CantidadDefectos) as Total from tbAuditInterRechazadoDet bb"
                                      + " join tbAuditInterRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion where l.id_linea = " + idlinea + " and bb.NumAuditoria=1"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "4":
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                int idplant = Convert.ToInt32(DropDownListPlant.SelectedValue);
                                query = " select top(5) pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbAuditInterRechazadoDet bb"
                                      + " join tbAuditInterRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.Id_Planta = " + idplant + " and bb.NumAuditoria=1"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "5":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "" && DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idstilo = Convert.ToInt32(hdnidstyle.Value);
                                int idlinea = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select top(5) pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbAuditInterRechazadoDet bb"
                                      + " join tbAuditInterRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion join Bundle b on b.Id_Bundle = bbr.idBulto"
                                      + " join POrder p on p.Id_Order = b.Id_Order join Style s on s.Id_Style = p.Id_Style"
                                      + " where s.Id_Style = " + idstilo + " and l.id_linea = " + idlinea + " and bb.NumAuditoria=1"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        default:
                            break;
                    }
                    break;

                case "2"://Auditoria Externa
                    switch (opcionfi)
                    {
                        case "1":
                            if (txtPorder.Text != "" && hdnidporder.Value != "")
                            {
                                int idorder = Convert.ToInt32(hdnidporder.Value);
                                query = " select top(5) pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbAuditExterRechazadoDet bb"
                                      + " join tbAuditExterRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado join tbDefectos d on d.idDefecto = bb.idDefectos"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion"
                                      + " join Bundle b on b.Id_Bundle = bbr.idBulto"
                                      + " join POrder p on p.Id_Order = b.Id_Order"
                                      + " where p.Id_Order = " + idorder + " and bb.NumAuditoria=1"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "2":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "")
                            {
                                int idstyle = Convert.ToInt32(hdnidstyle.Value);
                                query = " select top(5) pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbAuditExterRechazadoDet bb"
                                      + " join tbAuditExterRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join tbPosicion pos on pos.idPosicion = bb.idPosicion"
                                      + " join Bundle b on b.Id_Bundle = bbr.idBulto join POrder p on p.Id_Order = b.Id_Order"
                                      + " join Style s on s.Id_Style = p.Id_Style where s.Id_Style = " + idstyle
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "3":
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idlinea = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select top(5) pos.Posicion as Posicion, d.Defecto as Defecto, sum(bb.CantidadDefectos) as Total from tbAuditExterRechazadoDet bb"
                                      + " join tbAuditExterRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion where l.id_linea = " + idlinea + " and bb.NumAuditoria=1"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "4":
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                int idplant = Convert.ToInt32(DropDownListPlant.SelectedValue);
                                query = " select top(5) pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbAuditExterRechazadoDet bb"
                                      + " join tbAuditExterRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.Id_Planta = " + idplant + " and bb.NumAuditoria=1"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "5":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "" && DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idstilo = Convert.ToInt32(hdnidstyle.Value);
                                int idlinea = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select top(5) pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbAuditExterRechazadoDet bb"
                                      + " join tbAuditExterRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion join Bundle b on b.Id_Bundle = bbr.idBulto"
                                      + " join POrder p on p.Id_Order = b.Id_Order join Style s on s.Id_Style = p.Id_Style"
                                      + " where s.Id_Style = " + idstilo + " and l.id_linea = " + idlinea + " and bb.NumAuditoria=1"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        default:
                            break;
                    }
                    break;

                case "3"://Auditoria Bulto a Bulto
                    switch (opcionfi)
                    {
                        case "1":
                            if (txtPorder.Text != "" && hdnidporder.Value != "")
                            {
                                int idorder = Convert.ToInt32(hdnidporder.Value);
                                query = " select top(5) pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbBultoaBultoRechazadoDet bb"
                                      + " join tbBultoaBultoRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado join tbDefectos d on d.idDefecto = bb.idDefectos"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion"
                                      + " join Bundle b on b.Id_Bundle = bbr.idBulto"
                                      + " join POrder p on p.Id_Order = b.Id_Order"
                                      + " where p.Id_Order = " + idorder + " and bb.NumAuditoria=1"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);

                                query1 = " select left(CONVERT(date,bbr.FechaInspeccion),10) Fecha, l.numero Linea, p.POrder, pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbBultoaBultoRechazadoDet bb"
                                      + " join tbBultoaBultoRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado join tbDefectos d on d.idDefecto = bb.idDefectos"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion"
                                      + " join Bundle b on b.Id_Bundle = bbr.idBulto"
                                      + " join POrder p on p.Id_Order = b.Id_Order"
                                      + " join Linea l on l.id_linea = bbr.idLinea"
                                      + " where p.Id_Order = " + idorder + " and bb.NumAuditoria=1"
                                      + " group by CONVERT(date,bbr.FechaInspeccion), l.numero, p.POrder, pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defectG = DataAccess.Get_DataTable(query1);


                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "2":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "")
                            {
                                int idstyle = Convert.ToInt32(hdnidstyle.Value);
                                query = " select top(5) pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbBultoaBultoRechazadoDet bb"
                                      + " join tbBultoaBultoRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join tbPosicion pos on pos.idPosicion = bb.idPosicion"
                                      + " join Bundle b on b.Id_Bundle = bbr.idBulto join POrder p on p.Id_Order = b.Id_Order"
                                      + " join Style s on s.Id_Style = p.Id_Style where s.Id_Style = " + idstyle + " and CONVERT(date,bb.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);

                                query1 = " select left(CONVERT(date,bbr.FechaInspeccion),10) Fecha, l.numero Linea, p.POrder, pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbBultoaBultoRechazadoDet bb"
                                      + " join tbBultoaBultoRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join tbPosicion pos on pos.idPosicion = bb.idPosicion"
                                      + " join Bundle b on b.Id_Bundle = bbr.idBulto join POrder p on p.Id_Order = b.Id_Order" + " join Linea l on l.id_linea = bbr.idLinea"
                                      + " join Style s on s.Id_Style = p.Id_Style where s.Id_Style = " + idstyle + " and CONVERT(date,bb.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by CONVERT(date,bbr.FechaInspeccion), l.numero, p.POrder, pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defectG = DataAccess.Get_DataTable(query1);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "3":
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idlinea = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select top(5) pos.Posicion as Posicion, d.Defecto as Defecto, sum(bb.CantidadDefectos) as Total from tbBultoaBultoRechazadoDet bb"
                                      + " join tbBultoaBultoRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion where l.id_linea = " + idlinea + " and bb.NumAuditoria=1 and CONVERT(date,bb.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);

                                query1 = " select left(CONVERT(date,bbr.FechaInspeccion),10) Fecha, l.numero Linea, p.POrder, pos.Posicion as Posicion, d.Defecto as Defecto, sum(bb.CantidadDefectos) as Total from tbBultoaBultoRechazadoDet bb"
                                      + " join tbBultoaBultoRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join Bundle b on b.Id_Bundle = bbr.idBulto join POrder p on p.Id_Order = b.Id_Order "
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion where l.id_linea = " + idlinea + " and bb.NumAuditoria=1 and CONVERT(date,bb.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by CONVERT(date,bbr.FechaInspeccion), l.numero, p.POrder, pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defectG = DataAccess.Get_DataTable(query1);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "4":
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                int idplant = Convert.ToInt32(DropDownListPlant.SelectedValue);
                                query = " select top(5) pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbBultoaBultoRechazadoDet bb"
                                      + " join tbBultoaBultoRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.Id_Planta = " + idplant + " and bb.NumAuditoria=1 and CONVERT(date,bb.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);

                                query1 = " select left(CONVERT(date,bbr.FechaInspeccion),10) Fecha, l.numero Linea, p.POrder, pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbBultoaBultoRechazadoDet bb"
                                      + " join tbBultoaBultoRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join Bundle b on b.Id_Bundle = bbr.idBulto join POrder p on p.Id_Order = b.Id_Order"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.Id_Planta = " + idplant + " and bb.NumAuditoria=1 and CONVERT(date,bb.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by CONVERT(date,bbr.FechaInspeccion), l.numero, p.POrder, pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defectG = DataAccess.Get_DataTable(query1);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "5":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "" && DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idstilo = Convert.ToInt32(hdnidstyle.Value);
                                int idlinea = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select top(5) pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbBultoaBultoRechazadoDet bb"
                                      + " join tbBultoaBultoRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion join Bundle b on b.Id_Bundle = bbr.idBulto"
                                      + " join POrder p on p.Id_Order = b.Id_Order join Style s on s.Id_Style = p.Id_Style"
                                      + " where s.Id_Style = " + idstilo + " and l.id_linea = " + idlinea + " and bb.NumAuditoria=1 and CONVERT(date,bb.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);

                                query1 = " select left(CONVERT(date,bbr.FechaInspeccion),10) Fecha, l.numero Linea, p.POrder, pos.Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbBultoaBultoRechazadoDet bb"
                                      + " join tbBultoaBultoRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join tbPosicion pos on pos.idPosicion = bb.idPosicion join Bundle b on b.Id_Bundle = bbr.idBulto"
                                      + " join POrder p on p.Id_Order = b.Id_Order join Style s on s.Id_Style = p.Id_Style"
                                      + " where s.Id_Style = " + idstilo + " and l.id_linea = " + idlinea + " and bb.NumAuditoria=1 and CONVERT(date,bb.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by CONVERT(date,bbr.FechaInspeccion) Fecha, l.numero Linea, p.POrder, pos.Posicion, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defectG = DataAccess.Get_DataTable(query1);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        default:
                            break;
                    }
                    break;

                case "4"://Auditoria Inspeccion Proceso
                    switch (opcionfi)
                    {
                        case "1":
                            if (txtPorder.Text != "" && hdnidporder.Value != "")
                            {
                                int idorder = Convert.ToInt32(hdnidporder.Value);
                                query = " select top(5) op.descripcion_completa as Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbInspecProcRechazadoDet bb"
                                      + " join tbInspecProcRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado join tbDefectos d on d.idDefecto = bb.idDefectos"
                                      + " join Operacion op on op.id_operacion = bb.idOperacion"
                                      + " join Bundle b on b.Id_Bundle = bbr.idBulto"
                                      + " join POrder p on p.Id_Order = b.Id_Order"
                                      + " where p.Id_Order = " + idorder + " and bb.NumAuditoria=1"
                                      + " group by op.descripcion_completa, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "2":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "")
                            {
                                int idstyle = Convert.ToInt32(hdnidstyle.Value);
                                query = " select top(5) op.descripcion_completa as Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbInspecProcRechazadoDet bb"
                                      + " join tbInspecProcRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Operacion op on op.id_operacion = bb.idOperacion"
                                      + " join Bundle b on b.Id_Bundle = bbr.idBulto join POrder p on p.Id_Order = b.Id_Order"
                                      + " join Style s on s.Id_Style = p.Id_Style where s.Id_Style = " + idstyle + " and CONVERT(date,bb.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by op.descripcion_completa, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "3":
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idlinea = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select top(5) op.descripcion_completa as Posicion, d.Defecto as Defecto, sum(bb.CantidadDefectos) as Total from tbInspecProcRechazadoDet bb"
                                      + " join tbInspecProcRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join Operacion op on op.id_operacion = bb.idOperacion where l.id_linea = " + idlinea + " and bb.NumAuditoria=1 and CONVERT(date,bb.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by op.descripcion_completa, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "4":
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                int idplant = Convert.ToInt32(DropDownListPlant.SelectedValue);
                                query = " select top(5) op.descripcion_completa as Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbInspecProcRechazadoDet bb"
                                      + " join tbInspecProcRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join Operacion op on op.id_operacion = bb.idOperacion join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.Id_Planta = " + idplant + " and bb.NumAuditoria=1 and CONVERT(date,bb.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by op.descripcion_completa, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "5":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "" && DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idstilo = Convert.ToInt32(hdnidstyle.Value);
                                int idlinea = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select top(5) op.descripcion_completa as Posicion, d.Defecto, sum(bb.CantidadDefectos) as Total from tbInspecProcRechazadoDet bb"
                                      + " join tbInspecProcRechazado bbr on bbr.idBBRechazado = bb.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = bb.idDefectos join Linea l on l.id_linea = bbr.idLinea"
                                      + " join Operacion op on op.id_operacion = bb.idOperacion join Bundle b on b.Id_Bundle = bbr.idBulto"
                                      + " join POrder p on p.Id_Order = b.Id_Order join Style s on s.Id_Style = p.Id_Style"
                                      + " where s.Id_Style = " + idstilo + " and l.id_linea = " + idlinea + " and bb.NumAuditoria=1 and CONVERT(date,bb.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by op.descripcion_completa, d.Defecto order by sum(bb.CantidadDefectos) desc";
                                defect = DataAccess.Get_DataTable(query);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        default:
                            break;
                    }
                    break;

                case "5"://Auditoria Embarque, reparar
                    switch (opcionfi)
                    {
                        case "1":
                            if (txtPorder.Text != "" && hdnidporder.Value != "")
                            {
                                int idorder = Convert.ToInt32(hdnidporder.Value);
                                query = " select top(5) po.Posicion, d.Defecto, sum(ISNULL(aed.unidad,0)) as Total from tbAuditEmbarque ae"
                                      + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                                      + " join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                                      + " left join tbDefectos d on d.idDefecto = aed.idDefecto"
                                      + " left join tbPosicion po on po.idPosicion = aed.idPosicion"
                                      + " join POrder p on p.Id_Order = ae.idcorte"
                                      + " where p.Id_Order = " + idorder + ""
                                      + " group by po.Posicion, d.Defecto order by sum(ISNULL(aed.unidad,0)) desc";
                                defect = DataAccess.Get_DataTable(query);

                                query1 = " select left(CONVERT(date,na.horaInicial),10) Fecha, l.numero Linea, p.POrder, po.Posicion, d.Defecto, sum(ISNULL(aed.unidad,0)) as Total from tbAuditEmbarque ae"
                                      + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                                      + " join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                                      + " left join tbDefectos d on d.idDefecto = aed.idDefecto"
                                      + " left join tbPosicion po on po.idPosicion = aed.idPosicion"
                                      + " join POrder p on p.Id_Order = ae.idcorte"
                                      + " join Linea l on l.id_linea = p.Id_Linea2"
                                      + " where p.Id_Order = " + idorder + ""
                                      + " group by CONVERT(date,na.horaInicial), l.numero, p.POrder, po.Posicion, d.Defecto order by sum(ISNULL(aed.unidad,0)) desc";
                                defectG = DataAccess.Get_DataTable(query1);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "2"://Listo
                            if (txtstyle.Text != "" && hdnidstyle.Value != "")
                            {
                                int idstyle = Convert.ToInt32(hdnidstyle.Value);
                                query = " select top(5) po.Posicion, d.Defecto, sum(ISNULL(aed.unidad, 0)) as Total from tbAuditEmbarque ae"
                                      + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                                      + " left join tbDefectos d on d.idDefecto = aed.idDefecto left join tbPosicion po on po.idPosicion = aed.idPosicion join POrder p on p.Id_Order = ae.idcorte"
                                      + " where p.Id_Style = " + idstyle + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha1.Text + "' group by po.Posicion, d.Defecto"
                                      + " order by sum(ISNULL(aed.unidad,0)) desc";

                                defect = DataAccess.Get_DataTable(query);

                                query1 = " select left(CONVERT(date,na.horaInicial),10) Fecha, l.numero Linea, p.POrder, po.Posicion, d.Defecto, sum(ISNULL(aed.unidad, 0)) as Total from tbAuditEmbarque ae"
                                       + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                                       + " left join tbDefectos d on d.idDefecto = aed.idDefecto left join tbPosicion po on po.idPosicion = aed.idPosicion join POrder p on p.Id_Order = ae.idcorte"
                                       + " join Linea l on l.id_linea = p.Id_Linea2"
                                       + " where p.Id_Style = " + idstyle + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha1.Text + "'"
                                       + " group by CONVERT(date,na.horaInicial), l.numero, p.POrder, po.Posicion, d.Defecto"
                                       + " order by sum(ISNULL(aed.unidad,0)) desc";

                                defectG = DataAccess.Get_DataTable(query1);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "3"://Listo
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idlinea = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select top(5) po.Posicion, d.Defecto, sum(ISNULL(aed.unidad,0)) as Total from tbAuditEmbarque ae"
                                      + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                                      + " join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                                      + " left join tbDefectos d on d.idDefecto = aed.idDefecto"
                                      + " left join tbPosicion po on po.idPosicion = aed.idPosicion"
                                      + " join POrder p on p.Id_Order = ae.idcorte"
                                      + " join Linea l on l.id_linea = p.Id_Linea2"
                                      + " where l.id_linea = " + idlinea + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by po.Posicion, d.Defecto order by sum(ISNULL(aed.unidad,0)) desc";

                                defect = DataAccess.Get_DataTable(query);

                                query1 = " select left(CONVERT(date,na.horaInicial),10) Fecha, l.numero Linea, p.POrder, po.Posicion, d.Defecto, sum(ISNULL(aed.unidad, 0)) as Total from tbAuditEmbarque ae"
                                       + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                                       + " join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                                       + " left join tbDefectos d on d.idDefecto = aed.idDefecto"
                                       + " left join tbPosicion po on po.idPosicion = aed.idPosicion"
                                       + " join POrder p on p.Id_Order = ae.idcorte"
                                       + " join Linea l on l.id_linea = p.Id_Linea2"
                                       + " where l.id_linea = " + idlinea + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                       + " group by CONVERT(date,na.horaInicial), l.numero, p.POrder, po.Posicion, d.Defecto order by sum(ISNULL(aed.unidad,0)) desc";

                                defectG = DataAccess.Get_DataTable(query1);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "4"://Listo
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                int idplant = Convert.ToInt32(DropDownListPlant.SelectedValue);
                                query = " select top(5) po.Posicion, d.Defecto, sum(ISNULL(aed.unidad, 0)) as Total from tbAuditEmbarque ae"
                                      + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                                      + " join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                                      + " left join tbDefectos d on d.idDefecto = aed.idDefecto"
                                      + " left join tbPosicion po on po.idPosicion = aed.idPosicion join POrder p on p.Id_Order = ae.idcorte"
                                      + " join Linea l on l.id_linea = p.Id_Linea2 join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.id_planta = " + idplant + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by po.Posicion, d.Defecto order by sum(ISNULL(aed.unidad,0)) desc";

                                defect = DataAccess.Get_DataTable(query);

                                query1 = " select left(CONVERT(date,na.horaInicial),10) Fecha, l.numero Linea, p.POrder, po.Posicion, d.Defecto, sum(ISNULL(aed.unidad, 0)) as Total from tbAuditEmbarque ae"
                                      + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                                      + " join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                                      + " left join tbDefectos d on d.idDefecto = aed.idDefecto"
                                      + " left join tbPosicion po on po.idPosicion = aed.idPosicion join POrder p on p.Id_Order = ae.idcorte"
                                      + " join Linea l on l.id_linea = p.Id_Linea2 join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.id_planta = " + idplant + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by CONVERT(date,na.horaInicial), l.numero, p.POrder, po.Posicion, d.Defecto order by sum(ISNULL(aed.unidad,0)) desc";

                                defectG = DataAccess.Get_DataTable(query1);
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        default:
                            break;
                    }
                    break;

                case "6"://Todas las auditorias
                    switch (opcionfi)
                    {
                        case "1":
                            if (txtPorder.Text != "" && hdnidporder.Value != "")
                            {
                                panel1.Visible = false;
                                panel2.Visible = true;
                                int idorder = Convert.ToInt32(hdnidporder.Value);
                                query = " select Defecto, Auditorias, Total from (select a.Defecto, ISNULL(b.Total, 0) BultoaBulto, ISNULL(c.Total, 0) Embarque from (select idDefecto, Defecto from tbDefectos) a"
                                      + " left join (select d.idDefecto, d.Defecto, SUM(bt.CantidadDefectos) Total from tbDefectos d join tbBultoaBultoRechazadoDet bt on bt.idDefectos = d.idDefecto join tbBultoaBultoRechazado br on br.idBBRechazado = bt.idBBRechazado join Bundle b on b.Id_Bundle = br.idBulto"
                                      + " join POrder p on p.Id_Order = b.Id_Order where p.Id_Order = " + idorder + " group by d.idDefecto, d.Defecto) b on a.idDefecto = b.idDefecto left join"
                                      + " (Select d.idDefecto, d.Defecto, SUM(ISNULL(na.UnidadesDefect, 0)) Total1, SUM(ISNULL(aed.unidad, 0)) Total from tbDefectos d left join tbAuditEmbarqueDefectos aed on aed.idDefecto = d.idDefecto join tbNumeroAuditoria na on na.id = aed.idAuditEmbarque join tbAuditEmbarque ae on ae.id = na.idAuditoriaEmbarque"
                                      + " where ae.idcorte = " + idorder + " group by d.idDefecto, d.Defecto) c on a.idDefecto = c.idDefecto) main unpivot(Total for Auditorias in ([BultoaBulto],[Embarque])) as ma";
                                defectG = DataAccess.Get_DataTable(query);
                                Session["Todas"] = defectG;
                                g1.DataSource = defectG;
                                g1.DataBind();
                                grddeft.DataSource = defectG;
                                grddeft.DataBind();
                                n = 1;
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "2":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "")
                            {
                                panel1.Visible = false;
                                panel2.Visible = true;
                                query = " select Defecto, Auditorias, Total from (select a.Defecto, ISNULL(b.Total, 0) BultoaBulto, ISNULL(c.Total, 0) Embarque from (select idDefecto, Defecto from tbDefectos) a left join"
                                      + " (select d.idDefecto, d.Defecto, SUM(bt.CantidadDefectos) Total from tbDefectos d join tbBultoaBultoRechazadoDet bt on bt.idDefectos = d.idDefecto join tbBultoaBultoRechazado br on br.idBBRechazado = bt.idBBRechazado"
                                      + " join Bundle b on b.Id_Bundle = br.idBulto join POrder p on p.Id_Order = b.Id_Order where p.Id_Style = " + hdnidstyle.Value + " and CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by d.idDefecto, d.Defecto) b on a.idDefecto = b.idDefecto left join (Select d.idDefecto, d.Defecto, SUM(ISNULL(na.UnidadesDefect, 0)) Total1, SUM(ISNULL(aed.unidad, 0)) Total from tbDefectos d"
                                      + " left join tbAuditEmbarqueDefectos aed on aed.idDefecto = d.idDefecto join tbNumeroAuditoria na on na.id = aed.idAuditEmbarque join tbAuditEmbarque ae on ae.id = na.idAuditoriaEmbarque join POrder p on p.Id_Order = ae.idcorte"
                                      + " where p.Id_Style = " + hdnidstyle.Value + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by d.idDefecto, d.Defecto) c on a.idDefecto = c.idDefecto) main unpivot(Total for Auditorias in ([BultoaBulto],[Embarque])) as ma where Total != 0";

                                defectG = DataAccess.Get_DataTable(query);
                                Session["Todas"] = defectG;
                                g1.DataSource = defectG;
                                g1.DataBind();
                                grddeft.DataSource = defectG;
                                grddeft.DataBind();
                                n = 1;
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "3":
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                panel1.Visible = false;
                                panel2.Visible = true;
                                query = " select Defecto, Auditorias, Total from (select a.Defecto, ISNULL(b.Total, 0) BultoaBulto, ISNULL(c.Total, 0) Embarque from (select idDefecto, Defecto from tbDefectos) a"
                                      + " left join (select d.idDefecto, d.Defecto, SUM(bt.CantidadDefectos) Total from tbDefectos d join tbBultoaBultoRechazadoDet bt on bt.idDefectos = d.idDefecto"
                                      + " join tbBultoaBultoRechazado br on br.idBBRechazado = bt.idBBRechazado join Bundle b on b.Id_Bundle = br.idBulto join POrder p on p.Id_Order = b.Id_Order where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date,br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' "
                                      + " group by d.idDefecto, d.Defecto) b on a.idDefecto = b.idDefecto left join (Select d.idDefecto, d.Defecto, SUM(ISNULL(na.UnidadesDefect, 0)) Total1, SUM(ISNULL(aed.unidad, 0)) Total from tbDefectos d"
                                      + " left join tbAuditEmbarqueDefectos aed on aed.idDefecto = d.idDefecto join tbNumeroAuditoria na on na.id = aed.idAuditEmbarque join tbAuditEmbarque ae on ae.id = na.idAuditoriaEmbarque"
                                      + " join POrder p on p.Id_Order = ae.idcorte where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date,na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by d.idDefecto, d.Defecto) c on a.idDefecto = c.idDefecto) main unpivot(Total for Auditorias in ([BultoaBulto],[Embarque])) as ma where Total != 0";

                                defectG = DataAccess.Get_DataTable(query);
                                Session["Todas"] = defectG;
                                g1.DataSource = defectG;
                                g1.DataBind();
                                grddeft.DataSource = defectG;
                                grddeft.DataBind();
                                n = 1;

                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        case "4":
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                panel1.Visible = false;
                                panel2.Visible = true;
                                query = " select Defecto, Auditorias, Total from (select a.Defecto, ISNULL(b.Total, 0) BultoaBulto, ISNULL(c.Total, 0) Embarque from (select idDefecto, Defecto from tbDefectos) a left join"
                                      + " (select d.idDefecto, d.Defecto, SUM(bt.CantidadDefectos) Total from tbDefectos d join tbBultoaBultoRechazadoDet bt on bt.idDefectos = d.idDefecto join tbBultoaBultoRechazado br on br.idBBRechazado = bt.idBBRechazado"
                                      + " join Bundle b on b.Id_Bundle = br.idBulto join POrder p on p.Id_Order = b.Id_Order where p.Id_Planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by d.idDefecto, d.Defecto) b "
                                      + " on a.idDefecto = b.idDefecto left join (Select d.idDefecto, d.Defecto, SUM(ISNULL(na.UnidadesDefect, 0)) Total1, SUM(ISNULL(aed.unidad, 0)) Total from tbDefectos d join tbAuditEmbarqueDefectos aed on aed.idDefecto = d.idDefecto "
                                      + " join tbNumeroAuditoria na on na.id = aed.idAuditEmbarque join tbAuditEmbarque ae on ae.id = na.idAuditoriaEmbarque left join POrder p on p.Id_Order = ae.idcorte where p.Id_Planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by d.idDefecto, d.Defecto) c on a.idDefecto = c.idDefecto) main unpivot(Total for Auditorias in ([BultoaBulto],[Embarque])) as ma where Total != 0";

                                defectG = DataAccess.Get_DataTable(query);
                                Session["Todas"] = defectG;
                                g1.DataSource = defectG;
                                g1.DataBind();
                                grddeft.DataSource = defectG;
                                grddeft.DataBind();
                                n = 1;
                            }
                            else
                            {
                                n = 1;
                            }
                            break;

                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }

            if (n == 0)
            {
                Session["rep1"] = defectG;
                dft.DataSource = defectG;
                dft.DataBind();
                Session["repChr"] = defect;
                chart.DataSource = defect;
                chart.DataBind();
                lblaudit.Text = rdaudit.SelectedItem.Text;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void dft_DataBinding(object sender, EventArgs e)
    {
        dft.DataSource = Session["rep1"];
    }

    protected void dft_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        dft.DataBind();
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

            chart.DataSource = Session["rep1"];
            chart.DataBind();

            export.GridViewID = "dft";

            link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)chart).Chart;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = export;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link2, link3 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Top-Defectos-";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=TopDefectos-" + lblaudit.Text + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void chart_DataBinding(object sender, EventArgs e)
    {
        chart.DataSource = Session["repChr"];
    }

    protected void chart_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    {
        chart.DataBind();
    }

    protected void g1_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    {
        g1.DataBind();
    }

    protected void g1_DataBinding(object sender, EventArgs e)
    {
        g1.DataSource = Session["Todas"];
    }

    protected void exp_Click(object sender, EventArgs e)
    {
        try
        {
            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

            g1.DataSource = Session["Todas"];
            g1.DataBind();

            expdft.GridViewID = "grddeft";

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link3.Component = expdft;

            link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)g1).Chart;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link2, link3 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Top-Defectos-";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=TopDefectos" + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void grddeft_DataBinding(object sender, EventArgs e)
    {
        grddeft.DataSource = Session["Todas"];
    }

    protected void grddeft_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grddeft.DataBind();
    }

    protected void grdpiezas_DataBinding(object sender, EventArgs e)
    {
        grdpiezas.DataSource = Session["infobb"];
    }

    protected void grdpiezas_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdpiezas.DataBind();
    }

    protected void grdpiezas2_DataBinding(object sender, EventArgs e)
    {
        grdpiezas2.DataSource = Session["infoeb"];
    }

    protected void grdpiezas2_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdpiezas2.DataBind();
    }

    protected void btninfo_Click(object sender, EventArgs e)
    {
        try
        {
            VerInfo();
        }
        catch (Exception)
        {

           //throw;
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Session["infobb"] = null;
        grdpiezas.DataSource = Session["infobb"];
        grdpiezas.DataBind();

        Session["infoeb"] = null;
        grdpiezas2.DataSource = Session["infoeb"];
        grdpiezas2.DataBind();

        ASPxDateEdit1.Text = DateTime.Now.ToShortDateString();
        ASPxDateEdit2.Text = DateTime.Now.ToShortDateString();
    }
}