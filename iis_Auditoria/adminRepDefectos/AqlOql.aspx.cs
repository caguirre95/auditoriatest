﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_AqlOql : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                cargacombo();
                txtfecha1.Text = DateTime.Now.ToShortDateString();
                txtfecha2.Text = DateTime.Now.ToShortDateString();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    void cargacombo()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta, descripcion FROM Planta order by descripcion asc";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
        DropDownListPlant.Items.Insert(6, "All Plant");

    }

    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string id = RadioButtonList1.SelectedValue;

            switch (id)
            {
                case "1":
                    panelCorte.Visible = true;
                    panelEstilo.Visible = false;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = false;
                    break;
                case "2":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = true;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;
                case "3":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = false;
                    panelLinea.Visible = true;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;
                case "4":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = false;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = true;
                    panelfechas.Visible = true;
                    break;

                case "5":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = true;
                    panelLinea.Visible = true;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            var dt = new DataTable();

            string id= RadioButtonList1.SelectedValue;
            bool resp = false;

            switch (id)
            {
                case "1":
                    if (hdnidporder.Value!=string.Empty)
                    {
                        dt = OrderDetailDA.spdRepBBAqlOqlXCorte(Convert.ToInt32(hdnidporder.Value));
                    }
                    else
                    {
                        resp = true;
                    }
                    break;
                case "2":
                    if (hdnidstyle.Value != string.Empty)
                    {
                        dt = OrderDetailDA.spdRepBBAqlOqlXEstilo(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt32(hdnidstyle.Value));
                    }
                    else
                    {
                        resp = true;
                    }
                    break;
                case "3":
                    if (DropDownListLine.SelectedItem.Text != "Select...")
                    {
                        dt = OrderDetailDA.spdRepBBAqlOqlXLinea(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListLine.SelectedValue));
                    }
                    else
                    {
                        resp = true;
                    }
                    break;
                case "4":
                    if (DropDownListPlant.SelectedItem.Text != "Select...")
                    {
                        if (DropDownListPlant.SelectedItem.Value == "All Plant")
                        {
                            dt = OrderDetailDA.spdRepBBAqlOqlAll(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));
                        }
                        else
                        {
                            dt = OrderDetailDA.spdRepBBAqlOqlXPlanta(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListPlant.SelectedValue));
                        }
                    }
                    else
                    {
                        resp = true;
                    }
                    break;

                case "5":
                    if (DropDownListLine.SelectedValue != "0" && hdnidstyle.Value != "")
                    {
                        dt = OrderDetailDA.spdRepBBAqlOqlXLineaStyle(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt32(DropDownListLine.SelectedValue), Convert.ToInt32(hdnidstyle.Value));
                    }
                    else
                    {
                        resp = true;
                    }
                    break;

                default:
                    break;
            }

            if (!resp)
            {
                List<oqlaql> lista = new List<oqlaql>();


                var obj = new oqlaql
                {
                    aq = "% Accepted",
                    valora = Math.Round(Convert.ToDecimal(dt.Rows[0][1].ToString()), 2),
                    oq = "% Accepted",
                    valoro = Math.Round(Convert.ToDecimal(dt.Rows[0][3].ToString()), 2)
                };

                lista.Add(obj);

                obj = new oqlaql
                {
                aq = "% Rejected",
                valora =Math.Round(Convert.ToDecimal(dt.Rows[0][0].ToString()),2),
                    oq= "% Rejected",
                    valoro = Math.Round(Convert.ToDecimal(dt.Rows[0][2].ToString()), 2)
                };

                lista.Add(obj);


             
               WebChartAql.DataSource = lista;
               WebChartAql.DataBind();

               WebChartOql.DataSource = lista;
               WebChartOql.DataBind();

            }
        }
        catch (Exception)
        {
            //throw;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<estilosClass> GetStyle(string pref)
    {
        try
        {
            List<estilosClass> list = new List<estilosClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            string sql = "SELECT distinct top 10  Style,Id_Style FROM Style where Style like '%" + pref + "%' ";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                estilosClass obj = new estilosClass();

                obj.estilo = dt.Rows[i][0].ToString();
                obj.id = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    class oqlaql
    {
        public string oq { get; set; }
        public decimal valoro { get; set; }
        public string aq { get; set; }
        public decimal valora { get; set; }
    }

     public class estilosClass
    {
        public string estilo { get; set; }
        public string id { get; set; }
    }
}