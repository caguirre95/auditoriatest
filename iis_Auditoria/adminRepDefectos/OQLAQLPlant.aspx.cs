﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_OQLAQLPlant : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                txtfecha1.Text = DateTime.Now.ToShortDateString();
                txtfecha2.Text = DateTime.Now.ToShortDateString();
                lblplan.Text = "";
                lblprocess.Text = "";
                cargacombo();
                Session["plantaO"] = null;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    //protected void drplant_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        string linea = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero"
    //                     + " from ( select l.id_linea, l.numero from Linea l where l.Estado = 1 and id_planta = " + drplant.SelectedValue + " ) as c1 order by CONVERT(float, c1.numero) asc";
    //        DataTable numeroLine = DataAccess.Get_DataTable(linea);
    //        drpline.DataSource = numeroLine;
    //        drpline.DataTextField = "numero";
    //        drpline.DataValueField = "id_linea";
    //        drpline.DataBind();
    //        drpline.Items.Insert(0, "Select...");
    //    }
    //    catch (Exception)
    //    {
    //        throw;
    //    }
    //}

    protected void rdb1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string id = rdb1.SelectedValue;

            switch (id)
            {
                case "1":
                    panelEstilo.Visible = true;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;
                case "2":
                    panelEstilo.Visible = false;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = true;
                    panelfechas.Visible = true;
                    break;
                case "3":
                    panelEstilo.Visible = true;
                    panelLinea.Visible = true;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    void cargacombo()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta, descripcion FROM Planta order by descripcion asc";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
    }

    protected void btnclean_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/OQLAQLPlant.aspx");
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            var aqloql = new DataTable();
            string query = "";
            int pass = 0;

            string audit = rd1.SelectedValue;
            string filt = rdb1.SelectedValue;


            switch (audit)
            {
                case "1"://Auditoria Interna

                    switch (filt)
                    {
                        case "1":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "")
                            {
                                int idst = Convert.ToInt32(hdnidstyle.Value);
                                query = " select 'Style: ' + Style as Linea, estado, valor"
                                      + " from"
                                      + " (select a.Style, CAST(cast(a.Total as decimal(16, 3)) / (CAST(a.Total as decimal (16, 3)) + CAST(b.Total as decimal (16, 3))) * 100 as decimal (16, 3)) as AQL"
                                      + " , cast(((((CAST(a.Total as decimal(16, 3)) + cast(b.Total as decimal(16, 3))) - CAST(b.TotalDefectos as decimal(16, 3))) / CAST(b.rechazadas as decimal(16, 3))) * 100) as decimal(16, 3)) as OQL"
                                      + " from"
                                      + " (select s.Style as Style, COUNT(ba.idBBAprovado) as Total, sum(UnidadesAuditadas) as aceptadas"
                                      + " from tbAuditInterAprovados ba"
                                      + " left Join Bundle b on b.Id_Bundle = ba.IdBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " where s.Id_Style = " + idst
                                      + " and convert(date, FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by s.Style) as a,"
                                      + " (select COUNT(c.idBBRechazado) as Total, sum(c.Muestreo) as rechazadas, COUNT(c.idDefectos) as TotalDefectos"
                                      + " from"
                                      + " (select ar.idBBRechazado, Muestreo, dt.idDefectos"
                                      + " from tbAuditInterRechazadoDet dt"
                                      + " left join tbAuditInterRechazado ar on ar.idBBRechazado = dt.idBBRechazado"
                                      + " left join Bundle b on b.Id_Bundle = ar.idBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " where dt.NumAuditoria = 1 and  s.Id_Style = " + idst
                                      + " and convert(date, dt.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by ar.idBBRechazado, Muestreo, dt.idDefectos) as c) as b"
                                      + " ) pvt unpivot(valor for estado in (AQL, OQL) ) as pv";
                                aqloql = DataAccess.Get_DataTable(query);
                            }
                            else
                            { pass = 1; }
                            break;

                        case "2":
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                query = " select 'Line: '+ Linea as Linea, estado, valor " 
                                      + " from ( select a.Linea, CAST(cast(a.Total as decimal(16, 3)) / (CAST(a.Total as decimal (16, 3)) + CAST(b.Total as decimal (16, 3))) * 100 as decimal (16, 3)) as AQL"
                                      + " ,cast(((((CAST(a.Total as decimal(16,3)) + cast(b.Total as decimal(16,3))) - CAST(b.TotalDefectos as decimal(16,3))) / CAST(b.rechazadas as decimal(16,3))) * 100)as decimal(16,3))  as OQL"
                                      + " from "
                                      + " ( select l.numero as Linea, COUNT(idBBAprovado) as Total, sum(UnidadesAuditadas) as aceptadas " 
                                      +  " from Planta pl left join linea l on pl.id_planta = l.id_planta"
                                      + " left join tbAuditInterAprovados ba on l.id_linea = ba.idLinea where pl.id_planta = " + DropDownListPlant.SelectedValue
                                      + " and convert(date, FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by l.numero ) as a,"
                                      + " (select COUNT(c.idBBRechazado) as Total,sum(c.Muestreo) as rechazadas, COUNT(c.idDefectos) as TotalDefectos "
                                      + " from ( select br.idBBRechazado, Muestreo, dt.idDefectos"
                                      + " from Planta pl left join linea l on pl.id_planta = l.id_planta "
                                      + " left join tbAuditInterRechazado br on l.id_linea = br.idLinea"
                                      + " left join tbAuditInterRechazadoDet dt on br.idBBRechazado = dt.idBBRechazado "
                                      + " where dt.NumAuditoria = 1 and  pl.id_planta = " + DropDownListPlant.SelectedValue
                                      + " and convert(date, dt.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by br.idBBRechazado, Muestreo, l.numero, dt.idDefectos ) as c )as b"
                                      + " ) pvt unpivot ( valor for estado in (AQL, OQL) ) as pv";
                                aqloql = DataAccess.Get_DataTable(query);
                            }
                            else
                            { pass = 1; }
                            break;

                        case "3":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "" && DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idst = Convert.ToInt32(hdnidstyle.Value);
                                int idl = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select 'Style: '+ Style + ' Linea: ' + Line as Linea, estado, valor"
                                      + " from"
                                      + " (select a.Style, a.Line, CAST(cast(a.Total as decimal(16, 3)) / (CAST(a.Total as decimal (16, 3)) + CAST(b.Total as decimal (16, 3))) * 100 as decimal (16, 3)) as AQL"
                                      + " , cast(((((CAST(a.Total as decimal(16, 3)) + cast(b.Total as decimal(16, 3))) - CAST(b.TotalDefectos as decimal(16, 3))) / CAST(b.rechazadas as decimal(16, 3))) * 100) as decimal(16, 3)) as OQL"
                                      + " from"
                                      + " (select s.Style as Style, l.numero as Line, COUNT(ba.idBBAprovado) as Total, sum(UnidadesAuditadas) as aceptadas"
                                      + " from tbAuditInterAprovados ba"
                                      + " left Join Bundle b on b.Id_Bundle = ba.IdBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " left join Linea l on l.Id_Linea = ba.idLinea"
                                      + " where s.Id_Style = " + idst 
                                      + " and l.id_linea = " + idl
                                      + " and convert(date, FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by s.Style, l.numero) as a,"
                                      + " (select COUNT(c.idBBRechazado) as Total, sum(c.Muestreo) as rechazadas, COUNT(c.idDefectos) as TotalDefectos"
                                      + " from"
                                      + " (select ar.idBBRechazado, Muestreo, dt.idDefectos"
                                      + " from tbAuditInterRechazadoDet dt"
                                      + " left join tbAuditInterRechazado ar on ar.idBBRechazado = dt.idBBRechazado"
                                      + " left join Bundle b on b.Id_Bundle = ar.idBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " left join Linea l on l.id_linea = ar.idLinea"
                                      + " where dt.NumAuditoria = 1 and  s.Id_Style = " + idst
                                      + " and l.id_linea = " + idl
                                      + " and convert(date, dt.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by ar.idBBRechazado, Muestreo, dt.idDefectos) as c) as b"
                                      + " ) pvt unpivot(valor for estado in (AQL, OQL) ) as pv";
                                aqloql = DataAccess.Get_DataTable(query);
                            }
                            else
                            { pass = 1; }
                            break;

                        default: break;
                    }
                    break;

                case "2"://Auditoria Externa
                    switch (filt)
                    {
                        case "1":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "")
                            {
                                int idst = Convert.ToInt32(hdnidstyle.Value);
                                query = " select 'Style: ' + Style as Linea, estado, valor"
                                      + " from"
                                      + " (select a.Style, CAST(cast(a.Total as decimal(16, 3)) / (CAST(a.Total as decimal (16, 3)) + CAST(b.Total as decimal (16, 3))) * 100 as decimal (16, 3)) as AQL"
                                      + " , cast(((((CAST(a.Total as decimal(16, 3)) + cast(b.Total as decimal(16, 3))) - CAST(b.TotalDefectos as decimal(16, 3))) / CAST(b.rechazadas as decimal(16, 3))) * 100) as decimal(16, 3)) as OQL"
                                      + " from"
                                      + " (select s.Style as Style, COUNT(ba.idBBAprovado) as Total, sum(UnidadesAuditadas) as aceptadas"
                                      + " from tbAuditExterAprovados ba"
                                      + " left Join Bundle b on b.Id_Bundle = ba.IdBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " where s.Id_Style = " + idst
                                      + " and convert(date, FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by s.Style) as a,"
                                      + " (select COUNT(c.idBBRechazado) as Total, sum(c.Muestreo) as rechazadas, COUNT(c.idDefectos) as TotalDefectos"
                                      + " from"
                                      + " (select ar.idBBRechazado, Muestreo, dt.idDefectos"
                                      + " from tbAuditExterRechazadoDet dt"
                                      + " left join tbAuditExterRechazado ar on ar.idBBRechazado = dt.idBBRechazado"
                                      + " left join Bundle b on b.Id_Bundle = ar.idBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " where dt.NumAuditoria = 1 and  s.Id_Style = " + idst
                                      + " and convert(date, dt.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by ar.idBBRechazado, Muestreo, dt.idDefectos) as c) as b"
                                      + " ) pvt unpivot(valor for estado in (AQL, OQL) ) as pv";
                                aqloql = DataAccess.Get_DataTable(query);
                            }
                            else
                            { pass = 1; }
                            break;

                        case "2":
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                query = " select 'Line: '+ Linea as Linea, estado, valor from ( select a.Linea, CAST(cast(a.Total as decimal(16, 3)) / (CAST(a.Total as decimal (16, 3)) + CAST(b.Total as decimal (16, 3))) * 100 as decimal (16, 3)) as AQL"
                                      + " ,cast(((((CAST(a.Total as decimal(16,3)) + cast(b.Total as decimal(16,3))) - CAST(b.TotalDefectos as decimal(16,3))) / CAST(b.rechazadas as decimal(16,3))) * 100)as decimal(16,3))  as OQL"
                                      + " from ( select l.numero as Linea, COUNT(idBBAprovado) as Total, sum(UnidadesAuditadas) as aceptadas from Planta pl left join linea l on pl.id_planta = l.id_planta"
                                      + " left join tbAuditExterAprovados ba on l.id_linea = ba.idLinea where pl.id_planta = " + DropDownListPlant.SelectedValue
                                      + " and convert(date, FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by l.numero ) as a,"
                                      + " ( select COUNT(c.idBBRechazado) as Total,sum(c.Muestreo) as rechazadas, COUNT(c.idDefectos) as TotalDefectos from ( select br.idBBRechazado, Muestreo, dt.idDefectos"
                                      + " from Planta pl left join linea l on pl.id_planta = l.id_planta left join tbAuditExterRechazado br on l.id_linea = br.idLinea"
                                      + " left join tbAuditExterRechazadoDet dt on br.idBBRechazado = dt.idBBRechazado where dt.NumAuditoria = 1 and  pl.id_planta = " + DropDownListPlant.SelectedValue
                                      + " and convert(date, dt.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by br.idBBRechazado, Muestreo, l.numero, dt.idDefectos ) as c )as b"
                                      + " ) pvt unpivot ( valor for estado in (AQL, OQL) ) as pv";
                                aqloql = DataAccess.Get_DataTable(query);
                            }
                            else
                            { pass = 1; }
                            break;

                        case "3":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "" && DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idst = Convert.ToInt32(hdnidstyle.Value);
                                int idl = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select 'Style: '+ Style + ' Linea: ' + Line as Linea, estado, valor"
                                      + " from"
                                      + " (select a.Style, a.Line, CAST(cast(a.Total as decimal(16, 3)) / (CAST(a.Total as decimal (16, 3)) + CAST(b.Total as decimal (16, 3))) * 100 as decimal (16, 3)) as AQL"
                                      + " , cast(((((CAST(a.Total as decimal(16, 3)) + cast(b.Total as decimal(16, 3))) - CAST(b.TotalDefectos as decimal(16, 3))) / CAST(b.rechazadas as decimal(16, 3))) * 100) as decimal(16, 3)) as OQL"
                                      + " from"
                                      + " (select s.Style as Style, l.numero as Line, COUNT(ba.idBBAprovado) as Total, sum(UnidadesAuditadas) as aceptadas"
                                      + " from tbAuditExterAprovados ba"
                                      + " left Join Bundle b on b.Id_Bundle = ba.IdBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " left join Linea l on l.Id_Linea = ba.idLinea"
                                      + " where s.Id_Style = " + idst
                                      + " and l.id_linea = " + idl
                                      + " and convert(date, FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by s.Style, l.numero) as a,"
                                      + " (select COUNT(c.idBBRechazado) as Total, sum(c.Muestreo) as rechazadas, COUNT(c.idDefectos) as TotalDefectos"
                                      + " from"
                                      + " (select ar.idBBRechazado, Muestreo, dt.idDefectos"
                                      + " from tbAuditExterRechazadoDet dt"
                                      + " left join tbAuditExterRechazado ar on ar.idBBRechazado = dt.idBBRechazado"
                                      + " left join Bundle b on b.Id_Bundle = ar.idBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " left join Linea l on l.id_linea = ar.idLinea"
                                      + " where dt.NumAuditoria = 1 and  s.Id_Style = " + idst
                                      + " and l.id_linea = " + idl
                                      + " and convert(date, dt.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by ar.idBBRechazado, Muestreo, dt.idDefectos) as c) as b"
                                      + " ) pvt unpivot(valor for estado in (AQL, OQL) ) as pv";
                                aqloql = DataAccess.Get_DataTable(query);
                            }
                            else
                            { pass = 1; }
                            break;

                        default: break;
                    }
                    break;

                case "3"://Auditoria Bulto a Bulto
                    switch (filt)
                    {
                        case "1":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "")
                            {
                                int idst = Convert.ToInt32(hdnidstyle.Value);
                                query = " select 'Style: ' + Style as Linea, estado, valor"
                                      + " from"
                                      + " (select a.Style, CAST(cast(a.Total as decimal(16, 3)) / (CAST(a.Total as decimal (16, 3)) + CAST(b.Total as decimal (16, 3))) * 100 as decimal (16, 3)) as AQL"
                                      + " , cast(((((CAST(a.Total as decimal(16, 3)) + cast(b.Total as decimal(16, 3))) - CAST(b.TotalDefectos as decimal(16, 3))) / CAST(b.rechazadas as decimal(16, 3))) * 100) as decimal(16, 3)) as OQL"
                                      + " from"
                                      + " (select s.Style as Style, COUNT(ba.idBBAprovado) as Total, sum(UnidadesAuditadas) as aceptadas"
                                      + " from tbBultoaBultoAprovados ba"
                                      + " left Join Bundle b on b.Id_Bundle = ba.IdBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " where s.Id_Style = " + idst
                                      + " and convert(date, FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by s.Style) as a,"
                                      + " (select COUNT(c.idBBRechazado) as Total, sum(c.Muestreo) as rechazadas, COUNT(c.idDefectos) as TotalDefectos"
                                      + " from"
                                      + " (select ar.idBBRechazado, Muestreo, dt.idDefectos"
                                      + " from tbBultoaBultoRechazadoDet dt"
                                      + " left join tbBultoaBultoRechazado ar on ar.idBBRechazado = dt.idBBRechazado"
                                      + " left join Bundle b on b.Id_Bundle = ar.idBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " where dt.NumAuditoria = 1 and  s.Id_Style = " + idst
                                      + " and convert(date, dt.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by ar.idBBRechazado, Muestreo, dt.idDefectos) as c) as b"
                                      + " ) pvt unpivot(valor for estado in (AQL, OQL) ) as pv";
                                aqloql = DataAccess.Get_DataTable(query);
                            }
                            else
                            { pass = 1; }
                            break;

                        case "2":
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                query = " select 'Line: '+ Linea as Linea, estado, valor from ( select a.Linea, CAST(cast(a.Total as decimal(16, 3)) / (CAST(a.Total as decimal (16, 3)) + CAST(b.Total as decimal (16, 3))) * 100 as decimal (16, 3)) as AQL"
                                      + " ,cast(((((CAST(a.Total as decimal(16,3)) + cast(b.Total as decimal(16,3))) - CAST(b.TotalDefectos as decimal(16,3))) / CAST(b.rechazadas as decimal(16,3))) * 100)as decimal(16,3))  as OQL"
                                      + " from ( select l.numero as Linea, COUNT(idBBAprovado) as Total, sum(UnidadesAuditadas) as aceptadas from Planta pl left join linea l on pl.id_planta = l.id_planta"
                                      + " left join tbBultoaBultoAprovados ba on l.id_linea = ba.idLinea where pl.id_planta = " + DropDownListPlant.SelectedValue
                                      + " and convert(date, FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by l.numero ) as a,"
                                      + " ( select COUNT(c.idBBRechazado) as Total,sum(c.Muestreo) as rechazadas, COUNT(c.idDefectos) as TotalDefectos from ( select br.idBBRechazado, Muestreo, dt.idDefectos"
                                      + " from Planta pl left join linea l on pl.id_planta = l.id_planta left join tbBultoaBultoRechazado br on l.id_linea = br.idLinea"
                                      + " left join tbBultoaBultoRechazadoDet dt on br.idBBRechazado = dt.idBBRechazado where dt.NumAuditoria = 1 and  pl.id_planta = " + DropDownListPlant.SelectedValue
                                      + " and convert(date, dt.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by br.idBBRechazado, Muestreo, l.numero, dt.idDefectos ) as c )as b"
                                      + " ) pvt unpivot ( valor for estado in (AQL, OQL) ) as pv";
                                aqloql = DataAccess.Get_DataTable(query);
                            }
                            else
                            { pass = 1; }
                            break;

                        case "3":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "" && DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idst = Convert.ToInt32(hdnidstyle.Value);
                                int idl = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select 'Style: '+ Style + ' Linea: ' + Line as Linea, estado, valor"
                                      + " from"
                                      + " (select a.Style, a.Line, CAST(cast(a.Total as decimal(16, 3)) / (CAST(a.Total as decimal (16, 3)) + CAST(b.Total as decimal (16, 3))) * 100 as decimal (16, 3)) as AQL"
                                      + " ,cast(((((CAST(a.Total as decimal(16, 3)) + cast(b.Total as decimal(16, 3))) - CAST(b.TotalDefectos as decimal(16, 3))) / CAST(b.rechazadas as decimal(16, 3))) * 100) as decimal(16, 3)) as OQL"
                                      + " from"
                                      + " (select s.Style as Style, l.numero as Line, COUNT(ba.idBBAprovado) as Total, sum(UnidadesAuditadas) as aceptadas"
                                      + " from tbBultoaBultoAprovados ba"
                                      + " left Join Bundle b on b.Id_Bundle = ba.IdBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " left join Linea l on l.Id_Linea = ba.idLinea"
                                      + " where s.Id_Style = " + idst
                                      + " and l.id_linea = " + idl
                                      + " and convert(date, FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by s.Style, l.numero) as a,"
                                      + " (select COUNT(c.idBBRechazado) as Total, sum(c.Muestreo) as rechazadas, COUNT(c.idDefectos) as TotalDefectos"
                                      + " from"
                                      + " (select ar.idBBRechazado, Muestreo, dt.idDefectos"
                                      + " from tbBultoaBultoRechazadoDet dt"
                                      + " left join tbBultoaBultoRechazado ar on ar.idBBRechazado = dt.idBBRechazado"
                                      + " left join Bundle b on b.Id_Bundle = ar.idBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " left join Linea l on l.id_linea = ar.idLinea"
                                      + " where dt.NumAuditoria = 1 and  s.Id_Style = " + idst
                                      + " and l.id_linea = " + idl
                                      + " and convert(date, dt.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by ar.idBBRechazado, Muestreo, dt.idDefectos) as c) as b"
                                      + " ) pvt unpivot(valor for estado in (AQL, OQL) ) as pv";
                                aqloql = DataAccess.Get_DataTable(query);
                            }
                            else
                            { pass = 1; }
                            break;

                        default: break;
                    }
                    break;

                case "4"://Auditoria Inspeccion Proceso
                    switch (filt)
                    {
                        case "1":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "")
                            {
                                int idst = Convert.ToInt32(hdnidstyle.Value);
                                query = " select 'Style: ' + Style as Linea, estado, valor"
                                      + " from"
                                      + " (select a.Style, CAST(cast(a.Total as decimal(16, 3)) / (CAST(a.Total as decimal (16, 3)) + CAST(b.Total as decimal (16, 3))) * 100 as decimal (16, 3)) as AQL"
                                      + " , cast(((((CAST(a.Total as decimal(16, 3)) + cast(b.Total as decimal(16, 3))) - CAST(b.TotalDefectos as decimal(16, 3))) / CAST(b.rechazadas as decimal(16, 3))) * 100) as decimal(16, 3)) as OQL"
                                      + " from"
                                      + " (select s.Style as Style, COUNT(ba.idBBAprovado) as Total, sum(UnidadesAuditadas) as aceptadas"
                                      + " from tbInspecProcAprovados ba"
                                      + " left Join Bundle b on b.Id_Bundle = ba.IdBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " where s.Id_Style = " + idst
                                      + " and convert(date, FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by s.Style) as a,"
                                      + " (select COUNT(c.idBBRechazado) as Total, sum(c.Muestreo) as rechazadas, COUNT(c.idDefectos) as TotalDefectos"
                                      + " from"
                                      + " (select ar.idBBRechazado, Muestreo, dt.idDefectos"
                                      + " from tbInspecProcRechazadoDet dt"
                                      + " left join tbInspecProcRechazado ar on ar.idBBRechazado = dt.idBBRechazado"
                                      + " left join Bundle b on b.Id_Bundle = ar.idBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " where dt.NumAuditoria = 1 and  s.Id_Style = " + idst
                                      + " and convert(date, dt.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by ar.idBBRechazado, Muestreo, dt.idDefectos) as c) as b"
                                      + " ) pvt unpivot(valor for estado in (AQL, OQL) ) as pv";
                                aqloql = DataAccess.Get_DataTable(query);
                            }
                            else
                            { pass = 1; }
                            break;

                        case "2":
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                query = " select 'Line: '+ Linea as Linea, estado, valor from ( select a.Linea, CAST(cast(a.Total as decimal(16, 3)) / (CAST(a.Total as decimal (16, 3)) + CAST(b.Total as decimal (16, 3))) * 100 as decimal (16, 3)) as AQL"
                                      + " ,cast(((((CAST(a.Total as decimal(16,3)) + cast(b.Total as decimal(16,3))) - CAST(b.TotalDefectos as decimal(16,3))) / CAST(b.rechazadas as decimal(16,3))) * 100)as decimal(16,3))  as OQL"
                                      + " from ( select l.numero as Linea, COUNT(idBBAprovado) as Total, sum(UnidadesAuditadas) as aceptadas from Planta pl left join linea l on pl.id_planta = l.id_planta"
                                      + " left join tbInspecProcAprovados ba on l.id_linea = ba.idLinea where pl.id_planta = " + DropDownListPlant.SelectedValue
                                      + " and convert(date, FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by l.numero ) as a,"
                                      + " ( select COUNT(c.idBBRechazado) as Total,sum(c.Muestreo) as rechazadas, COUNT(c.idDefectos) as TotalDefectos from ( select br.idBBRechazado, Muestreo, dt.idDefectos"
                                      + " from Planta pl left join linea l on pl.id_planta = l.id_planta left join tbInspecProcRechazado br on l.id_linea = br.idLinea"
                                      + " left join tbInspecProcRechazadoDet dt on br.idBBRechazado = dt.idBBRechazado where dt.NumAuditoria = 1 and  pl.id_planta = " + DropDownListPlant.SelectedValue
                                      + " and convert(date, dt.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by br.idBBRechazado, Muestreo, l.numero, dt.idDefectos ) as c )as b"
                                      + " ) pvt unpivot ( valor for estado in (AQL, OQL) ) as pv";
                                aqloql = DataAccess.Get_DataTable(query);
                            }
                            else
                            { pass = 1; }
                            break;

                        case "3":
                            if (txtstyle.Text != "" && hdnidstyle.Value != "" && DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                int idst = Convert.ToInt32(hdnidstyle.Value);
                                int idl = Convert.ToInt32(DropDownListLine.SelectedValue);
                                query = " select 'Style: '+ Style + ' Linea: ' + Line as Linea, estado, valor"
                                      + " from"
                                      + " (select a.Style, a.Line, CAST(cast(a.Total as decimal(16, 3)) / (CAST(a.Total as decimal (16, 3)) + CAST(b.Total as decimal (16, 3))) * 100 as decimal (16, 3)) as AQL"
                                      + " , cast(((((CAST(a.Total as decimal(16, 3)) + cast(b.Total as decimal(16, 3))) - CAST(b.TotalDefectos as decimal(16, 3))) / CAST(b.rechazadas as decimal(16, 3))) * 100) as decimal(16, 3)) as OQL"
                                      + " from"
                                      + " (select s.Style as Style, l.numero as Line, COUNT(ba.idBBAprovado) as Total, sum(UnidadesAuditadas) as aceptadas"
                                      + " from tbInspecProcAprovados ba"
                                      + " left Join Bundle b on b.Id_Bundle = ba.IdBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " left join Linea l on l.Id_Linea = ba.idLinea"
                                      + " where s.Id_Style = " + idst
                                      + " and l.id_linea = " + idl
                                      + " and convert(date, FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by s.Style, l.numero) as a,"
                                      + " (select COUNT(c.idBBRechazado) as Total, sum(c.Muestreo) as rechazadas, COUNT(c.idDefectos) as TotalDefectos"
                                      + " from"
                                      + " (select ar.idBBRechazado, Muestreo, dt.idDefectos"
                                      + " from tbInspecProcRechazadoDet dt"
                                      + " left join tbInspecProcRechazado ar on ar.idBBRechazado = dt.idBBRechazado"
                                      + " left join Bundle b on b.Id_Bundle = ar.idBulto"
                                      + " left join POrder p on p.Id_Order = b.Id_Order"
                                      + " left join Style s on s.Id_Style = p.Id_Style"
                                      + " left join Linea l on l.id_linea = ar.idLinea"
                                      + " where dt.NumAuditoria = 1 and  s.Id_Style = " + idst
                                      + " and l.id_linea = " + idl
                                      + " and convert(date, dt.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by ar.idBBRechazado, Muestreo, dt.idDefectos) as c) as b"
                                      + " ) pvt unpivot(valor for estado in (AQL, OQL) ) as pv";
                                aqloql = DataAccess.Get_DataTable(query);
                            }
                            else
                            { pass = 1; }
                            break;

                        default: break;
                    }
                    break;

                default: break;
            }

            if (pass == 0)
            {
                Session["plantaO"] = aqloql;
                WebChartControl2.DataSource = aqloql;
                WebChartControl2.DataBind();
                //lblplan.Text = DropDownListPlant.SelectedItem.Text;
                lblprocess.Text = rd1.SelectedItem.Text;
            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

            WebChartControl2.DataSource = Session["plantaO"];
            WebChartControl2.DataBind();

            link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)WebChartControl2).Chart;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link2 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "AQL-OQL-";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Oql-Aql-" + lblprocess.Text + "-" + lblplan.Text + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {
            throw;
        }
    }
}