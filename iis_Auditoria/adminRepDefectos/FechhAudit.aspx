﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FechhAudit.aspx.cs" Inherits="adminRepDefectos_FechhAudit" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">
        <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

        <div class="panel panel-primary" style="margin-top: 5px">
            <div class="panel-heading">Auditorias Fech</div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <asp:Button ID="btngenerar" runat="server" Text="Generar Informacion" CssClass="btn btn-primary form-control" OnClick="btngenerar_Click" />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <asp:Button ID="btnexcel" runat="server" Text="Exportar Informacion" CssClass="btn btn-success form-control" OnClick="btnexcel_Click" />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <asp:Button ID="btnlimpiar" runat="server" Text="Limpiar" CssClass="btn btn-info form-control" OnClick="btnlimpiar_Click" />
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <hr />
                    <dx1:ASPxGridView ID="grdcortes" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdcortes_DataBinding" OnCustomCallback="grdcortes_CustomCallback">
                        <Columns>
                            <dx1:GridViewDataTextColumn FieldName="MPOrder" Caption="MP Order"></dx1:GridViewDataTextColumn>
                            <dx1:GridViewDataTextColumn FieldName="StyleNo." Caption="Style No."></dx1:GridViewDataTextColumn>
                            <dx1:GridViewDataTextColumn FieldName="Line"></dx1:GridViewDataTextColumn>
                            <dx1:GridViewDataTextColumn FieldName="QTY"></dx1:GridViewDataTextColumn>
                            <dx1:GridViewDataTextColumn FieldName="Client"></dx1:GridViewDataTextColumn>
                            <dx1:GridViewDataTextColumn FieldName="Garment"></dx1:GridViewDataTextColumn>
                            <dx1:GridViewDataTextColumn FieldName="Modulo"></dx1:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowFooter="true" VerticalScrollBarMode="Visible" ShowGroupFooter="VisibleAlways" />
                        <SettingsPager PageSize="250"></SettingsPager>
                    </dx1:ASPxGridView>
                    <hr />
                    <dx1:ASPxGridView ID="grdauditorias" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdauditorias_DataBinding" OnCustomCallback="grdauditorias_CustomCallback">
                        <Columns>
                            <dx1:GridViewDataTextColumn FieldName="Fecha"></dx1:GridViewDataTextColumn>
                            <dx1:GridViewDataTextColumn FieldName="Semana"></dx1:GridViewDataTextColumn>
                            <dx1:GridViewDataTextColumn FieldName="MPOrder" Caption="MP Order"></dx1:GridViewDataTextColumn>
                            <dx1:GridViewDataTextColumn FieldName="QTYAudit" Caption="QTY Audit"></dx1:GridViewDataTextColumn>
                            <dx1:GridViewDataTextColumn FieldName="Estatus"></dx1:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowFooter="true" VerticalScrollBarMode="Visible" ShowGroupFooter="VisibleAlways" />
                        <SettingsPager PageSize="250"></SettingsPager>
                    </dx1:ASPxGridView>
                </div>
                <dx1:ASPxGridViewExporter ID="export" runat="server"></dx1:ASPxGridViewExporter>
                <dx1:ASPxGridViewExporter ID="export2" runat="server"></dx1:ASPxGridViewExporter>
            </div>
        </div>


    </div>



</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

