﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_HistorialDefecto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                cargacombo();
                txtfecha1.Text = DateTime.Now.ToShortDateString();
                txtfecha2.Text = DateTime.Now.ToShortDateString();

                Session["defectorep"] = null;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    void cargacombo()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta, descripcion FROM Planta order by descripcion asc";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/HistorialDefecto.aspx");
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            var defecto = new DataTable();
            string query = "";
            int n = 0;

            string op1 = rdaudit.SelectedValue;
            string opc1 = rd1.SelectedValue;

            switch (op1)
            {
                case "1"://Bulto a Bulto
                    switch (opc1)
                    {
                        case "1"://Corte
                            if (txtPorder.Text != "" && hdnidporder.Value != "")
                            {
                                query = " select l.numero Linea, p.Id_Order, p.POrder Corte, p.Quantity Cantidad, s.Style Estilo, b.NSeq Bulto, d.Codigo, d.Defecto, po.Posicion"
                                      + " from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on brd.idBBRechazado = br.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = brd.idDefectos left join tbPosicion po on po.idPosicion = brd.idPosicion join Bundle b on b.Id_Bundle = br.idBulto join POrder p on p.Id_Order = b.Id_Order"
                                      + " join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style where p.Id_Order = " + hdnidporder.Value + " order by l.numero asc, p.POrder asc, b.NSeq asc";

                                defecto = DataAccess.Get_DataTable(query);

                                n = 1;
                            }

                            break;

                        case "2"://Linea
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                query = " select l.numero Linea, p.Id_Order, p.POrder Corte, p.Quantity Cantidad, s.Style Estilo, b.NSeq Bulto, d.Codigo, d.Defecto, po.Posicion"
                                      + " from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on brd.idBBRechazado = br.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = brd.idDefectos left join tbPosicion po on po.idPosicion = brd.idPosicion join Bundle b on b.Id_Bundle = br.idBulto join POrder p on p.Id_Order = b.Id_Order"
                                      + " join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style where l.id_linea = " + DropDownListLine.SelectedValue + " and"
                                      + " CONVERT(date,brd.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' order by l.numero asc, p.POrder asc, b.NSeq asc";

                                defecto = DataAccess.Get_DataTable(query);

                                n = 1;
                            }

                            break;

                        case "3"://Estilo
                            if (txtstyle.Text != "" && hdnidstyle.Value != "")
                            {
                                query = " select l.numero Linea, p.Id_Order, p.POrder Corte, p.Quantity Cantidad, s.Style Estilo, b.NSeq Bulto, d.Codigo, d.Defecto, po.Posicion"
                                      + " from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on brd.idBBRechazado = br.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = brd.idDefectos left join tbPosicion po on po.idPosicion = brd.idPosicion join Bundle b on b.Id_Bundle = br.idBulto join POrder p on p.Id_Order = b.Id_Order"
                                      + " join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style where s.Id_Style = " + hdnidstyle.Value + " and"
                                      + " CONVERT(date,brd.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' order by l.numero asc, p.POrder asc, b.NSeq asc";

                                defecto = DataAccess.Get_DataTable(query);

                                n = 1;
                            }

                            break;

                        case "4"://Planta
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                query = " select l.numero Linea, p.Id_Order, p.POrder Corte, p.Quantity Cantidad, s.Style Estilo, b.NSeq Bulto, d.Codigo, d.Defecto, po.Posicion"
                                      + " from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on brd.idBBRechazado = br.idBBRechazado"
                                      + " join tbDefectos d on d.idDefecto = brd.idDefectos left join tbPosicion po on po.idPosicion = brd.idPosicion join Bundle b on b.Id_Bundle = br.idBulto join POrder p on p.Id_Order = b.Id_Order"
                                      + " join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style where p.Id_Planta = " + DropDownListPlant.SelectedValue + " and"
                                      + " CONVERT(date,brd.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' order by l.numero asc, p.POrder asc, b.NSeq asc";

                                defecto = DataAccess.Get_DataTable(query);

                                n = 1;
                            }

                            break;

                        default: break;
                    }
                    break;

                case "2"://Embarque
                    switch (opc1)
                    {
                        case "1"://Corte
                            if (txtPorder.Text != "" && hdnidporder.Value != "")
                            {
                                query = " select l.numero Linea, p.Id_Order, p.POrder Corte, p.Quantity Cantidad, s.Style Estilo, ae.codigoLote Bulto, d.Codigo, d.Defecto, po.Posicion from tbAuditEmbarque ae"
                                      + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id join tbDefectos d on d.idDefecto = aed.idDefecto"
                                      + " left join tbPosicion po on po.idPosicion = aed.idPosicion join POrder p on p.Id_Order = ae.idcorte join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style"
                                      + " where p.Id_Order = " + hdnidporder.Value + " order by l.numero asc, p.POrder asc, ae.lote asc";

                                defecto = DataAccess.Get_DataTable(query);

                                n = 1;
                            }

                            break;

                        case "2"://Linea
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                query = " select l.numero Linea, p.Id_Order, p.POrder Corte, p.Quantity Cantidad, s.Style Estilo, ae.codigoLote Bulto, d.Codigo, d.Defecto, po.Posicion from tbAuditEmbarque ae"
                                      + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id  join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id join tbDefectos d on d.idDefecto = aed.idDefecto"
                                      + " left join tbPosicion po on po.idPosicion = aed.idPosicion join POrder p on p.Id_Order = ae.idcorte join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style"
                                      + " where l.id_linea = " + DropDownListLine.SelectedValue + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " order by l.numero asc, p.POrder asc, ae.lote asc";

                                defecto = DataAccess.Get_DataTable(query);

                                n = 1;
                            }

                            break;

                        case "3"://Estilo
                            if (txtstyle.Text != "" && hdnidstyle.Value != "")
                            {
                                query = " select l.numero Linea, p.Id_Order, p.POrder Corte, p.Quantity Cantidad, s.Style Estilo, ae.codigoLote Bulto, d.Codigo, d.Defecto, po.Posicion from tbAuditEmbarque ae"
                                      + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id  join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id join tbDefectos d on d.idDefecto = aed.idDefecto"
                                      + " left join tbPosicion po on po.idPosicion = aed.idPosicion join POrder p on p.Id_Order = ae.idcorte join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style"
                                      + " where s.Id_Style = " + hdnidstyle.Value + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " order by l.numero asc, p.POrder asc, ae.lote asc";

                                defecto = DataAccess.Get_DataTable(query);

                                n = 1;
                            }

                            break;

                        case "4"://Planta
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                query = " select l.numero Linea, p.Id_Order, p.POrder Corte, p.Quantity Cantidad, s.Style Estilo, ae.codigoLote Bulto, d.Codigo, d.Defecto, po.Posicion from tbAuditEmbarque ae"
                                      + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id  join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id join tbDefectos d on d.idDefecto = aed.idDefecto"
                                      + " left join tbPosicion po on po.idPosicion = aed.idPosicion join POrder p on p.Id_Order = ae.idcorte join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style"
                                      + " where p.Id_Planta = " + DropDownListPlant.SelectedValue + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " order by l.numero asc, p.POrder asc, ae.lote asc";

                                defecto = DataAccess.Get_DataTable(query);

                                n = 1;
                            }

                            break;

                        default: break;
                    }
                    break;

                default: break;

            }

            if (n == 1)
            {
                Session["defectorep"] = defecto;
                griddefecto.DataBind();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void griddefecto_DataBinding(object sender, EventArgs e)
    {
        griddefecto.DataSource = Session["defectorep"];
    }

    protected void griddefecto_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        griddefecto.DataBind();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            exportar.GridViewID = "griddefecto";

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = exportar;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Historial-Defecto";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Defecto" + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            throw;
        }
    }
}