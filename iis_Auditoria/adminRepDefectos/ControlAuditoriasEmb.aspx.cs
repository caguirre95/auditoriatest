﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data.SqlClient;
using System.IO;
using DevExpress.XtraPrinting;

public partial class adminRepDefectos_ControlAuditoriasEmb : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();
            linea();
            Session["controlau"] = null;
        }
    }

    void linea()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        drpLinea.DataSource = dt_Line;
        drpLinea.DataTextField = "numero";
        drpLinea.DataValueField = "id_linea";
        drpLinea.DataBind();
        drpLinea.Items.Insert(0, "Select...");
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select Top 15 POrderClient from POrder p where POrderClient like '%" + pre + "%' group by POrderClient", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porderclient = dt.Rows[i][0].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porderclient { get; set; }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<style> GetStyle(string pre)
    {
        try
        {
            List<style> list = new List<style>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top 15 Id_Style, Style from Style where Style like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                style obj = new style();

                obj.id = int.Parse(dt.Rows[i][0].ToString());
                obj.estilo = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class style
    {
        public int id { get; set; }
        public string estilo { get; set; }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/ControlAuditoriasEmb.aspx");
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            Carga();
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    void Carga()
    {
        DataTable dt;
        string query = "";
        string opcion = rdl1.SelectedValue;

        switch (opcion)
        {
            case "1"://Linea - Fecha
                if (drpLinea.SelectedItem.Text != "Select...")
                {
                    query = " select a1.Linea, a1.POrderClient Corte, a1.Style Estilo, a1.Color, c1.codigolote, c1.Status Status, c1.secuencia, isnull(c1.Posicion, '') Operacion, ISNULL(c1.Defecto, '') Defectos,"
                          + " ISNULL(c1.unidad, 0) Cantidad from (select p.POrderClient, s.Style, Min(b.Color) Color, sum(b.Quantity) Quantity, l.numero Linea from POrder p join Style s on s.Id_Style = p.Id_Style"
                          + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join tbAuditEmbarque ae on ae.idcorte = p.Id_Order join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                          + " where p.Id_Linea2 = " + drpLinea.SelectedValue + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.POrderClient, s.Style, l.numero) a1 left join"
                          + " (select p.POrderClient, ae.codigoLote, case when na.estadoAudit = 0 then 'R' else 'A' end Status, na.secuencia, po.Posicion, d.Defecto,aed.unidad from tbAuditEmbarque ae"
                          + " join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id left join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                          + " left join tbDefectos d on d.idDefecto = aed.idDefecto left join tbPosicion po on po.idPosicion = aed.idPosicion where p.Id_Linea2 = " + drpLinea.SelectedValue + ""
                          + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "') c1 on a1.POrderClient = c1.POrderClient order by a1.Linea, c1.codigolote, c1.secuencia asc";

                    dt = DataAccess.Get_DataTable(query);
                    Session["controlau"] = dt;

                    grdcontrolAudit.DataSource = Session["controlau"];
                    grdcontrolAudit.DataBind();
                }
                break;

            case "2"://Corte
                if (txtcorte.Text != string.Empty)
                {
                    query = " select a1.Linea, a1.POrderClient Corte, a1.Style Estilo, a1.Color, c1.codigolote, c1.Status Status, c1.secuencia, isnull(c1.Posicion, '') Operacion, ISNULL(c1.Defecto, '') Defectos,"
                          + " ISNULL(c1.unidad, 0) Cantidad from (select p.POrderClient, s.Style, Min(b.Color) Color, sum(b.Quantity) Quantity, l.numero Linea from POrder p join Style s on s.Id_Style = p.Id_Style"
                          + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 where p.POrderClient = '" + txtcorte.Text + "' group by p.POrderClient, s.Style, l.numero) a1 left join"
                          + " (select p.POrderClient, ae.codigoLote, case when na.estadoAudit = 0 then 'R' else 'A' end Status, na.secuencia, po.Posicion, d.Defecto,aed.unidad from tbAuditEmbarque ae"
                          + " join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id left join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                          + " left join tbDefectos d on d.idDefecto = aed.idDefecto left join tbPosicion po on po.idPosicion = aed.idPosicion where p.POrderClient = '" + txtcorte.Text + "') c1 on a1.POrderClient = c1.POrderClient"
                          + " order by c1.codigolote, c1.secuencia asc";

                    dt = DataAccess.Get_DataTable(query);
                    Session["controlau"] = dt;

                    grdcontrolAudit.DataSource = Session["controlau"];
                    grdcontrolAudit.DataBind();
                }
                break;

            case "3"://Estilo - Fecha
                if (txtestilo.Text != string.Empty && hdnidstyle.Value != string.Empty)
                {
                    query = " select a1.Linea, a1.POrderClient Corte, a1.Style Estilo, a1.Color, c1.codigolote, c1.Status Status, c1.secuencia, isnull(c1.Posicion, '') Operacion, ISNULL(c1.Defecto, '') Defectos,"
                          + " ISNULL(c1.unidad, 0) Cantidad from (select p.POrderClient, s.Style, Min(b.Color) Color, sum(b.Quantity) Quantity, l.numero Linea from POrder p join Style s on s.Id_Style = p.Id_Style"
                          + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join tbAuditEmbarque ae on ae.idcorte = p.Id_Order join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                          + " where p.Id_Style = " + hdnidstyle.Value + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.POrderClient, s.Style, l.numero) a1 left join"
                          + " (select p.POrderClient, ae.codigoLote, case when na.estadoAudit = 0 then 'R' else 'A' end Status, na.secuencia, po.Posicion, d.Defecto,aed.unidad from tbAuditEmbarque ae"
                          + " join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id left join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                          + " left join tbDefectos d on d.idDefecto = aed.idDefecto left join tbPosicion po on po.idPosicion = aed.idPosicion where p.Id_Style = " + hdnidstyle.Value + ""
                          + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "') c1 on a1.POrderClient = c1.POrderClient order by a1.Linea, c1.codigolote, c1.secuencia asc";

                    dt = DataAccess.Get_DataTable(query);
                    Session["controlau"] = dt;

                    grdcontrolAudit.DataSource = Session["controlau"];
                    grdcontrolAudit.DataBind();
                }
                break;

            case "4"://Status - Fecha
                if (drpstatus.SelectedItem.Text != "Select...")
                {
                    query = " select a1.Linea, a1.POrderClient Corte, a1.Style Estilo, a1.Color, c1.codigolote, c1.Status Status, c1.secuencia, isnull(c1.Posicion, '') Operacion, ISNULL(c1.Defecto, '') Defectos,"
                          + " ISNULL(c1.unidad, 0) Cantidad from (select p.POrderClient, s.Style, Min(b.Color) Color, sum(b.Quantity) Quantity, l.numero Linea from POrder p join Style s on s.Id_Style = p.Id_Style"
                          + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join tbAuditEmbarque ae on ae.idcorte = p.Id_Order join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                          + " where CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.POrderClient, s.Style, l.numero) a1 left join"
                          + " (select p.POrderClient, ae.codigoLote, case when na.estadoAudit = 0 then 'R' else 'A' end Status, na.secuencia, po.Posicion, d.Defecto,aed.unidad from tbAuditEmbarque ae"
                          + " join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id left join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                          + " left join tbDefectos d on d.idDefecto = aed.idDefecto left join tbPosicion po on po.idPosicion = aed.idPosicion where"
                          + " CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "') c1 on a1.POrderClient = c1.POrderClient where c1.Status = '" + drpstatus.SelectedItem.Text.Trim() + "' order by a1.Linea, c1.codigolote, c1.secuencia asc";

                    dt = DataAccess.Get_DataTable(query);
                    Session["controlau"] = dt;

                    grdcontrolAudit.DataSource = Session["controlau"];
                    grdcontrolAudit.DataBind();
                }
                break;

            case "5"://Fechas
                query = " select a1.Linea, a1.POrderClient Corte, a1.Style Estilo, a1.Color, c1.codigolote, c1.Status Status, c1.secuencia, isnull(c1.Posicion, '') Operacion, ISNULL(c1.Defecto, '') Defectos,"
                          + " ISNULL(c1.unidad, 0) Cantidad from (select p.POrderClient, s.Style, Min(b.Color) Color, sum(b.Quantity) Quantity, l.numero Linea from POrder p join Style s on s.Id_Style = p.Id_Style"
                          + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join tbAuditEmbarque ae on ae.idcorte = p.Id_Order join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                          + " where CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.POrderClient, s.Style, l.numero) a1 left join"
                          + " (select p.POrderClient, ae.codigoLote, case when na.estadoAudit = 0 then 'R' else 'A' end Status, na.secuencia, po.Posicion, d.Defecto,aed.unidad from tbAuditEmbarque ae"
                          + " join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id left join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                          + " left join tbDefectos d on d.idDefecto = aed.idDefecto left join tbPosicion po on po.idPosicion = aed.idPosicion where"
                          + " CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "') c1 on a1.POrderClient = c1.POrderClient order by a1.Linea, c1.codigolote, c1.secuencia asc";

                dt = DataAccess.Get_DataTable(query);
                Session["controlau"] = dt;

                grdcontrolAudit.DataSource = Session["controlau"];
                grdcontrolAudit.DataBind();
                break;

            //case "6"://Corte - Status
            //    if (txtcorte.Text != string.Empty && drpstatus.SelectedItem.Text != "Select...")
            //    {

            //    }
            //    break;

            case "6"://Linea - Status - Fecha
                if (drpLinea.SelectedItem.Text != "Select..." && drpstatus.SelectedItem.Text != "Select...")
                {
                    query = " select a1.Linea, a1.POrderClient Corte, a1.Style Estilo, a1.Color, c1.codigolote, c1.Status Status, c1.secuencia, isnull(c1.Posicion, '') Operacion, ISNULL(c1.Defecto, '') Defectos,"
                          + " ISNULL(c1.unidad, 0) Cantidad from (select p.POrderClient, s.Style, Min(b.Color) Color, sum(b.Quantity) Quantity, l.numero Linea from POrder p join Style s on s.Id_Style = p.Id_Style"
                          + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join tbAuditEmbarque ae on ae.idcorte = p.Id_Order join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                          + " where p.Id_Linea2 = " + drpLinea.SelectedValue + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.POrderClient, s.Style, l.numero) a1 left join"
                          + " (select p.POrderClient, ae.codigoLote, case when na.estadoAudit = 0 then 'R' else 'A' end Status, na.secuencia, po.Posicion, d.Defecto,aed.unidad from tbAuditEmbarque ae"
                          + " join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id left join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                          + " left join tbDefectos d on d.idDefecto = aed.idDefecto left join tbPosicion po on po.idPosicion = aed.idPosicion where p.Id_Linea2 = " + drpLinea.SelectedValue + ""
                          + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "') c1 on a1.POrderClient = c1.POrderClient where c1.Status = '"+drpstatus.SelectedItem.Text.Trim()+"'"
                          + " order by a1.Linea, c1.codigolote, c1.secuencia asc";

                    dt = DataAccess.Get_DataTable(query);
                    Session["controlau"] = dt;

                    grdcontrolAudit.DataSource = Session["controlau"];
                    grdcontrolAudit.DataBind();
                }
                break;

            case "7"://Estilo - Status - Fechas
                if (txtestilo.Text != string.Empty && hdnidstyle.Value != string.Empty && drpstatus.SelectedItem.Text != "Select...")
                {
                    query = " select a1.Linea, a1.POrderClient Corte, a1.Style Estilo, a1.Color, c1.codigolote, c1.Status Status, c1.secuencia, isnull(c1.Posicion, '') Operacion, ISNULL(c1.Defecto, '') Defectos,"
                          + " ISNULL(c1.unidad, 0) Cantidad from (select p.POrderClient, s.Style, Min(b.Color) Color, sum(b.Quantity) Quantity, l.numero Linea from POrder p join Style s on s.Id_Style = p.Id_Style"
                          + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join tbAuditEmbarque ae on ae.idcorte = p.Id_Order join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                          + " where p.Id_Style = " + hdnidstyle.Value + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.POrderClient, s.Style, l.numero) a1 left join"
                          + " (select p.POrderClient, ae.codigoLote, case when na.estadoAudit = 0 then 'R' else 'A' end Status, na.secuencia, po.Posicion, d.Defecto,aed.unidad from tbAuditEmbarque ae"
                          + " join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id left join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                          + " left join tbDefectos d on d.idDefecto = aed.idDefecto left join tbPosicion po on po.idPosicion = aed.idPosicion where p.Id_Style = " + hdnidstyle.Value + ""
                          + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "') c1 on a1.POrderClient = c1.POrderClient where c1.Status = '"+drpstatus.SelectedItem.Text.Trim()+"'"
                          + " order by a1.Linea, c1.codigolote, c1.secuencia asc";

                    dt = DataAccess.Get_DataTable(query);
                    Session["controlau"] = dt;

                    grdcontrolAudit.DataSource = Session["controlau"];
                    grdcontrolAudit.DataBind();
                }
                break;

            default: break;

        }
    }

    protected void btnexportar_Click(object sender, EventArgs e)
    {
        try
        {
            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            export1.GridViewID = "grdcontrolAudit";

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = export1;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "ControlAudit";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=ControlAudit" + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void grdcontrolAudit_DataBinding(object sender, EventArgs e)
    {
        grdcontrolAudit.DataSource = Session["controlau"];
    }

    protected void grdcontrolAudit_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdcontrolAudit.DataBind();
    }
}