﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_Ejemplo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["Produccion"] = null;
            Session["Produccion2"] = null;
            Session["Produccion3"] = null;
            Session["Produccion4"] = null;
            if (drplinea.SelectedItem.Text != "Select...")
            {
                load_data();
            }
            else
            {
                cargaLinea();
            }
            txtfecha.Text = DateTime.Now.ToShortDateString();
        }
    }

    void cargaLinea()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        drplinea.DataSource = dt_Line;
        drplinea.DataTextField = "numero";
        drplinea.DataValueField = "id_linea";
        drplinea.DataBind();
        drplinea.Items.Insert(0, "Select...");
    }

    void load_data()
    {
        try
        {
            string query = " select descOperacion, isnull([1],0) as '09:00am', isnull([2],0) as '11:00am', isnull([3],0) as '01:00pm', isnull([4],0) as '03:00pm', isnull([5],0) as '04:50pm',"
                         + " isnull([6],0) as '06:50pm',(isnull([1], 0) + isnull([2],0) +isnull([3],0) +isnull([4],0) +isnull([5],0) +isnull([6],0)) Total from (select pt.descOperacion, sum(pt.BundleUni) Total, pt.bihorario bihorario from tbProduccionTickets pt join Operario op on op.id_operario = pt.operarioId"
                         + " where convert(date, fechaRegistro) = '" + txtfecha.Text + "' and pt.lineaConfeccion = " + drplinea.SelectedValue + " group by pt.descOperacion, pt.bihorario) main pivot(sum(Total) for bihorario in ([1],[2],[3],[4],[5],[6])) as Sec"
                         + " order by descOperacion";

            DataTable prod = DataAccess.Get_DataTable(query);
            Session["Produccion"] = prod;
            grdproduccion.DataSource = prod;
            grdproduccion.DataBind();

            string query3 = " select descOperacion, isnull([1],0) as '09:00am', isnull([2],0) as '11:00am', isnull([3],0) as '01:00pm', isnull([4],0) as '03:00pm', isnull([5],0) as '04:50pm', isnull([6],0) as '06:50pm'"
                          + " ,(isnull([1], 0) + isnull([2],0) +isnull([3],0) +isnull([4],0) +isnull([5],0) +isnull([6],0)) Total"
                          + " from (select descOperacion, sum(BundleUni) as Total, bihorario from tbProduccionTickets where descOperacion in ('PEGAR BOLSA TRASERA','PEGAR BOLSA DELANTERA','HERMANADO','CERRAR COSTADO',"
                          + " 'PEGAR PRETINA','HACER PUNTAS','PEGAR STOP','HACER RUEDO','PEGAR CARGADOR') AND convert(date, fechaRegistro) = '" + txtfecha.Text + "' and lineaConfeccion = " + drplinea.SelectedValue + " group by descOperacion, bihorario) main1"
                          + " pivot(sum(Total) for bihorario in ([1],[2],[3],[4],[5],[6])) as Sec order by descOperacion";

            DataTable prod3 = DataAccess.Get_DataTable(query3);
            Session["Produccion3"] = prod3;
            grdoper.DataSource = prod3;
            grdoper.DataBind();

            string query2 = " select pt.descOperacion, case when pt.bihorario = 1 then 'Bihorario 1' else case when pt.bihorario = 2 then 'Bihorario 2' else case when pt.bihorario = 3 then 'Bihorario 3' else"
                          + " case when pt.bihorario = 4 then 'Bihorario 4' else case when pt.bihorario = 5 then 'Bihorario 5' else case when pt.bihorario = 6 then 'Bihorario 6' end end end end end end"
                          + " bihorario, sum(pt.BundleUni) Total from tbProduccionTickets pt join Operario op on op.id_operario = pt.operarioId"
                          + " where descOperacion in ('PEGAR BOLSA TRASERA', 'PEGAR BOLSA DELANTERA', 'HERMANADO', 'CERRAR COSTADO', 'PEGAR PRETINA', 'HACER PUNTAS', 'PEGAR STOP', 'HACER RUEDO', 'PEGAR CARGADOR')"
                          + " and convert(date, fechaRegistro) = '" + txtfecha.Text + "' and pt.lineaConfeccion = " + drplinea.SelectedValue + " group by pt.descOperacion, pt.bihorario order by pt.bihorario, pt.descOperacion desc";

            DataTable prod2 = DataAccess.Get_DataTable(query2);
            Session["Produccion2"] = prod2;
            g1.DataSource = prod2;
            g1.DataBind();
            //w1.DataSource = prod2;
            //w1.DataBind();
            //w2.DataSource = prod2;
            //w2.DataBind();

            string query4 = " select lineaConfeccion Linea, descOperacion, Operario, isnull([1],0) as '09:00am', isnull([2],0) as '11:00am', isnull([3],0) as '01:00pm', isnull([4],0) as '03:00pm', isnull([5],0) as '04:50pm',"
                          + " isnull([6],0) as '06:50pm',(isnull([1], 0) + isnull([2],0) +isnull([3],0) +isnull([4],0) +isnull([5],0) +isnull([6],0)) Total from ("
                          + " select pt.lineaConfeccion, pt.descOperacion, op.nombre Operario, sum(pt.BundleUni) Total, pt.bihorario bihorario from tbProduccionTickets pt join Operario op on op.id_operario = pt.operarioId"
                          + " where convert(date, fechaRegistro) = '" + txtfecha.Text + "' and pt.lineaConfeccion = " + drplinea.SelectedValue + " group by pt.lineaConfeccion, pt.descOperacion, op.nombre, pt.bihorario) main"
                          + " pivot(sum(Total) for bihorario in ([1],[2],[3],[4],[5],[6])) as Sec order by descOperacion";

            DataTable prod4 = DataAccess.Get_DataTable(query4);
            Session["Produccion4"] = prod4;
            g1.DataSource = prod4;
            g1.DataBind();
            gridoper.GroupBy(gridoper.Columns["Linea"]);
            gridoper.GroupBy(gridoper.Columns["descOperacion"]);
            gridoper.ExpandAll();
        }
        catch (Exception)
        {

        }
    }

    protected void btnreporte_Click(object sender, EventArgs e)
    {
        if (drplinea.SelectedItem.Text != "Select...")
        {
            load_data();
            lbllin.Text = "Linea : " + drplinea.SelectedItem.Text + " - " + txtfecha.Text;
        }
    }

    protected void grdproduccion_DataBinding(object sender, EventArgs e)
    {
        grdproduccion.DataSource = Session["Produccion"];
        //grdproduccion.GroupBy(grdproduccion.Columns["corte"]);
    }

    protected void grdproduccion_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdproduccion.DataBind();
    }

    protected void g1_DataBinding(object sender, EventArgs e)
    {
        g1.DataSource = Session["Produccion2"];
    }

    protected void g1_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    {
        g1.DataBind();
    }

    protected void grdoper_DataBinding(object sender, EventArgs e)
    {
        grdoper.DataSource = Session["Produccion3"];
    }

    protected void grdoper_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdoper.DataBind();
    }

    protected void btnlim_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/Ejemplo.aspx");
    }

    protected void gridoper_DataBinding(object sender, EventArgs e)
    {
        gridoper.DataSource = Session["Produccion4"];
        gridoper.GroupBy(gridoper.Columns["Linea"]);
        gridoper.GroupBy(gridoper.Columns["descOperacion"]);
        gridoper.ExpandAll();
    }

    protected void gridoper_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridoper.DataBind();
    }

    //protected void w1_DataBinding(object sender, EventArgs e)
    //{
    //    w1.DataSource = Session["Produccion2"];
    //}

    //protected void w1_CustomCallback1(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    //{
    //    w1.DataBind();
    //}

    //protected void w2_CustomDrawSeriesPoint(object sender, DevExpress.XtraCharts.CustomDrawSeriesPointEventArgs e)
    //{
    //    if (w2.Series.Count > 0 && !ReferenceEquals(w2.Series[0], e.Series))
    //        e.LegendText = "";
    //}

    //protected void w2_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    //{
    //    w2.DataBind();
    //}

    //protected void w2_DataBinding(object sender, EventArgs e)
    //{
    //    w2.DataSource = Session["Produccion2"];
    //}
}