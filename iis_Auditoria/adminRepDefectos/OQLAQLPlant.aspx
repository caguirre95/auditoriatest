﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OQLAQLPlant.aspx.cs" Inherits="adminRepDefectos_OQLAQLPlant" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        function autoestilo() {

            $(function () {
                $('#<%=txtstyle.ClientID%>').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "AqlOql.aspx/GetStyle",
                            data: "{'pref' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        estilo: item.estilo,
                                        id: item.id,
                                        json: item
                                    }
                                }))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                        return false;
                    },
                    select: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);
                        $('#<%=hdnidstyle.ClientID%>').val(ui.item.id);

                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li  >")
                        .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
                };
            })
        }
    </script>

    <script type="text/javascript">
        function date1() {
            $('#<%=txtfecha1.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

        function date2() {
            $('#<%=txtfecha2.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <link href="../css/font-awesome.css" rel="stylesheet" />

    <style>
        .s {
            margin-top: 5px;
        }

        .ne {
            font-weight: bold;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <br />
    <div class="container">

        <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

        <div class="panel panel-primary" style="margin-top:5px">
            <div class="panel-heading">Measurement Report </div>
            <div class="panel-body">

                <div class="col-lg-12">

                    <div class="col-lg-3">
                        <div class="panel-default">
                            <div class="panel-heading">Auditorias</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="rd1" runat="server">
                                    <asp:ListItem Value="1">Auditoria Interna</asp:ListItem>
                                    <asp:ListItem Value="2">Auditoria Externa</asp:ListItem>
                                    <asp:ListItem Value="3">Auditoria Bulto a Bulto</asp:ListItem>
                                    <asp:ListItem Value="4">Auditoria Inspeccion - Proceso</asp:ListItem>
                                </asp:RadioButtonList>
                                <br />
                                <%--Linea
                                <asp:DropDownList ID="drpline" runat="server" CssClass="form-control"></asp:DropDownList>--%>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel-default">
                            <div class="panel-heading">Filtros</div>
                            <div class="panel-body">

                                <asp:RadioButtonList ID="rdb1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rdb1_SelectedIndexChanged">
                                    <asp:ListItem Value="1">Style</asp:ListItem>
                                    <asp:ListItem Value="2">Plant</asp:ListItem>
                                    <asp:ListItem Value="3">Style - Line</asp:ListItem>
                                </asp:RadioButtonList>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <asp:Panel runat="server" ID="panelLinea">
                                    Line:
                                     <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="panelEstilo">
                                    Style:                                   
                                    <asp:TextBox ID="txtstyle" onkeypress="autoestilo()" AutoCompleteType="Disabled" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdnidstyle" />
                                </asp:Panel>
                                <asp:Panel runat="server" ID="panelPlanta">
                                    Plant:
                                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <asp:Panel ID="panelfechas" runat="server">
                                    Start Date
                                <%--<asp:TextBox ID="txtfecha1" runat="server" AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>--%>
                                    <dx1:ASPxDateEdit ID="txtfecha1" runat="server" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                    Final Date
                                <%--<asp:TextBox ID="txtfecha2" runat="server" AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>--%>
                                    <dx1:ASPxDateEdit ID="txtfecha2" runat="server" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 pull-right">
                        <div class="col-lg-2 pull-right">
                            <asp:Button ID="btnbuscar" runat="server" Text="Buscar" CssClass="btn btn-primary form-control s" OnClick="btnbuscar_Click" />
                        </div>
                        <div class="col-lg-2 pull-right">
                            <asp:Button ID="btnexcel" runat="server" Text="Exportar" CssClass="btn btn-success form-control s" OnClick="btnexcel_Click" />
                        </div>
                        <div class="col-lg-2 pull-right">
                            <asp:Button ID="btnclean" runat="server" Text="Limpiar" CssClass="btn btn-info form-control s" OnClick="btnclean_Click" />
                        </div>
                    </div>

                    <div class="col-lg-12" style="padding-top: 20px">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-lg-12">
                                    <asp:Label ID="lblplan" runat="server" Text="" CssClass="ne"></asp:Label>
                                </div>

                                <div class="col-lg-12">
                                    <asp:Label ID="lblprocess" runat="server" Text="" CssClass="ne"></asp:Label>
                                </div>

                                <dx:WebChartControl ID="WebChartControl2" runat="server" Width="1020px" Height="360px" CrosshairEnabled="True" SeriesDataMember="estado">
                                    <BorderOptions Visibility="False" />
                                    <BorderOptions Visibility="False"></BorderOptions>
                                    <DiagramSerializable>
                                        <cc1:XYDiagram RangeControlDateTimeGridOptions-GridMode="ChartGrid">
                                            <%--<AxisX VisibleInPanesSerializable="-1" Label-TextPattern="Line : {A}">--%>
                                            <AxisX VisibleInPanesSerializable="-1">
                                                <GridLines Visible="True">
                                                </GridLines>
                                            </AxisX>
                                            <AxisY VisibleInPanesSerializable="-1">
                                            </AxisY>
                                        </cc1:XYDiagram>
                                    </DiagramSerializable>
                                    <Titles>
                                        <cc1:ChartTitle Text="AQL / OQL"></cc1:ChartTitle>
                                    </Titles>
                                    <SeriesTemplate ArgumentDataMember="Linea" LabelsVisibility="True" CrosshairLabelPattern="{S} : {V} %" ValueDataMembersSerializable="valor">
                                        <LabelSerializable>
                                            <cc1:SideBySideBarSeriesLabel Position="Top" TextPattern="Qty:{V} %" Indent="5" LineLength="10" LineVisibility="True" ResolveOverlappingMode="Default">
                                            </cc1:SideBySideBarSeriesLabel>
                                        </LabelSerializable>
                                    </SeriesTemplate>
                                </dx:WebChartControl>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>
</asp:Content>

