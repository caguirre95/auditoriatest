﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AqlOqlCorte.aspx.cs" Inherits="adminRepDefectos_AqlOqlCorte" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hdnidporder.ClientID%>').val(ui.item.idorder);
                    // $('#<=txtstyle.ClientID%>').val(ui.item.style);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        })
    </script>

    <script lang="javascript" type="text/javascript">
        function autoestilo() {

            $(function () {
                $('#<%=txtstyle.ClientID%>').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "AqlOql.aspx/GetStyle",
                            data: "{'pref' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        estilo: item.estilo,
                                        id: item.id,
                                        json: item
                                    }
                                }))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                        return false;
                    },
                    select: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);
                        $('#<%=hdnidstyle.ClientID%>').val(ui.item.id);

                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li  >")
                        .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
                };
            })
        }
    </script>

    <script type="text/javascript">
        function date1() {
            $('#<%=txtfecha1.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

        function date2() {
            $('#<%=txtfecha2.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

    </script>

    <style>
        .m {
            margin-top: 25px;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <br />
    <div class="container">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

            <div class="panel panel-primary" style="margin-top: 5px">
                <div class="panel-heading">AQL - OQL</div>
                <div class="panel-body">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="panel-default">
                                <div class="panel-heading">Auditorias</div>
                                <div class="panel-body">
                                    <asp:RadioButtonList ID="rdaudit" runat="server">
                                        <%--<asp:ListItem Value="1">Auditoria Interna</asp:ListItem>
                                        <asp:ListItem Value="2">Auditoria Externa</asp:ListItem>--%>
                                        <asp:ListItem Value="3">Auditoria Bulto a Bulto</asp:ListItem>
                                        <%--<asp:ListItem Value="4">Auditoria Inspeccion - Proceso</asp:ListItem>--%>
                                        <asp:ListItem Value="5">Auditoria Embarque</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="panel-default">
                                <div class="panel-heading">Filtros</div>
                                <div class="panel-body">

                                    <asp:RadioButtonList ID="rd1" runat="server">
                                        <asp:ListItem Value="1">Cut</asp:ListItem>
                                        <%--<asp:ListItem Value="2">Style</asp:ListItem>--%>
                                        <asp:ListItem Value="3">Line</asp:ListItem>
                                        <%--<asp:ListItem Value="4">Plant</asp:ListItem>--%>
                                        <%-- <asp:ListItem Value="5">Style - Line</asp:ListItem>--%>
                                        <asp:ListItem Value="6">Line - Week</asp:ListItem>
                                        <asp:ListItem Value="7">Plant - Week</asp:ListItem>
                                        <asp:ListItem Value="9">Global - Line</asp:ListItem>
                                        <asp:ListItem Value="8">Global - Plant</asp:ListItem>
                                    </asp:RadioButtonList>

                                </div>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="panel-default">
                                <div class="panel-heading">Opciones</div>
                                <div class="panel-body">
                                    <asp:Panel runat="server" ID="panelCorte">
                                        Cut:                                   
                                    <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        <asp:HiddenField runat="server" ID="hdnidporder" />
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="panelEstilo" Visible="false">
                                        Style:                                   
                                    <asp:TextBox ID="txtstyle" onkeypress="autoestilo()" AutoCompleteType="Disabled" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        <asp:HiddenField runat="server" ID="hdnidstyle" />
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="panelLinea">
                                        Line:
                                     <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="panelPlanta">
                                        Plant:
                                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="panel-default">
                                <div class="panel-heading">Select </div>
                                <div class="panel-body">
                                    <asp:Panel runat="server" ID="panelfechas">
                                        Start Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                        End Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                        <strong>Semana</strong>
                                        <asp:DropDownList ID="drpSemana" CssClass="form-control" runat="server" Width="200px" />
                                    </asp:Panel>
                                    <div style="padding-top: 15px">
                                        <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" runat="server" OnClick="btngenerar_Click" />
                                    </div>
                                    <div style="padding-top: 15px">
                                        <asp:Button Text="Limpiar Campos" CssClass="btn btn-info form-control" ID="btnlimpiar" runat="server" OnClick="btnlimpiar_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <asp:Panel ID="panelCut" runat="server">
                            <dx1:ASPxGridView ID="grd1" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grd1_DataBinding" OnCustomCallback="grd1_CustomCallback">
                                <Columns>
                                    <dx1:GridViewDataTextColumn FieldName="Id_Order" Caption="Id_Order" Visible="false"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="POrder" Caption="POrder" Visible="false"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Bld" Caption="Bulto" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="CantidadDefectos" Caption="Cantidad de Defectos"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Codigo" Caption="Codigo"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Posicion" Caption="Posicion"></dx1:GridViewDataTextColumn>
                                </Columns>
                            </dx1:ASPxGridView>
                            <hr />
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <dx1:ASPxGridView ID="AQl" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="AQl_DataBinding" OnCustomCallback="AQl_CustomCallback">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="Id_Order" Caption="Id_Order" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="POrder" Caption="POrder" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="BultosAudit" Caption="Bulto Audit"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="BultoAprovados" Caption="Bultos Aprobados"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="BultoRechazados" Caption="Bultos Rechazados"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="AQL" Caption="% AQL"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:ASPxGridView>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <dx1:ASPxGridView ID="OQL" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="OQL_DataBinding" OnCustomCallback="OQL_CustomCallback">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="Id_Order" Caption="Id_Order" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="POrder" Caption="POrder" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="PiezasAudit" Caption="Piezas Audit" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="PiezasAceptadas" Caption="Piezas Aceptadas"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="PiezasRechazadas" Caption="Piezas Rechazadas"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="OQL" Caption="% OQL"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:ASPxGridView>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="panellin" runat="server">
                            <dx1:ASPxGridView ID="gridlinea" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="gridlinea_DataBinding" OnCustomCallback="gridlinea_CustomCallback">
                                <Columns>
                                    <dx1:GridViewDataTextColumn FieldName="Id_Order" Caption="Id_Order" Visible="false"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha" PropertiesTextEdit-DisplayFormatString="yyyy-MM-dd"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="POrder" Caption="POrder"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Linea" Caption="Linea"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="BultosAudit" Caption="Bulto Auditados"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="BultoAprovados" Caption="Bultos Aprobados"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="BultoRechazados" Caption="Bultos Rechazados"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="PiezasAudit" Caption="Piezas Auditadas"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="PiezasAceptadas" Caption="Piezas Aceptadas"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="PiezasRechazadas" Caption="Piezas Rechazadas"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="AQL" Caption="% AQL"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="OQL" Caption="% OQL"></dx1:GridViewDataTextColumn>
                                </Columns>
                            </dx1:ASPxGridView>
                            <hr />
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <dx1:ASPxGridView ID="aqllinea" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="aqllinea_DataBinding" OnCustomCallback="aqllinea_CustomCallback">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="id_linea" Caption="id_linea" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha" PropertiesTextEdit-DisplayFormatString="yyyy-MM-dd"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="numero" Caption="Linea" Visible="true"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="BultosAudit" Caption="Bulto Audit"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="BultoAprovados" Caption="Bultos Aprobados"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="BultoRechazados" Caption="Bultos Rechazados"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="AQL" Caption="% AQL"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:ASPxGridView>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <dx1:ASPxGridView ID="oqllinea" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="oqllinea_DataBinding" OnCustomCallback="oqllinea_CustomCallback">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="id_linea" Caption="id_linea" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha" PropertiesTextEdit-DisplayFormatString="yyyy-MM-dd"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="numero" Caption="Linea" Visible="true"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="PiezasAudit" Caption="Piezas Audit" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="PiezasAceptadas" Caption="Piezas Aceptadas"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="PiezasRechazadas" Caption="Piezas Rechazadas"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="OQL" Caption="% OQL"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:ASPxGridView>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="pnlineaemb" runat="server">
                            <dx1:ASPxGridView ID="grdlinemb" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdlinemb_DataBinding" OnCustomCallback="grdlinemb_CustomCallback">
                                <Columns>
                                    <dx1:GridViewDataTextColumn FieldName="Id_Order" Caption="Id_Order" Visible="false"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha" PropertiesTextEdit-DisplayFormatString="yyyy-MM-dd"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="POrder" Caption="POrder"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="LotesAuditados" Caption="Lotes Auditados"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="LoteAprobados" Caption="Lotes Aprobados"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="LoteRechazado" Caption="Lotes Rechazados"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="PiezasAuditadas" Caption="Piezas Auditadas"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="PiezasAceptadas" Caption="Piezas Aceptadas"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="PiezasRechazadas" Caption="Piezas Rechazadas"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="AQL" Caption="% AQL"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="OQL" Caption="% OQL"></dx1:GridViewDataTextColumn>
                                </Columns>
                            </dx1:ASPxGridView>
                            <hr />
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <dx1:ASPxGridView ID="aqlemb" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="aqlemb_DataBinding" OnCustomCallback="aqlemb_CustomCallback">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="id_linea" Caption="id_linea" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha" PropertiesTextEdit-DisplayFormatString="yyyy-MM-dd"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="numero" Caption="Linea" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="LotesAuditados" Caption="Lote Audit"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="LoteAprobados" Caption="Lotes Aprobados"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="LoteRechazado" Caption="Lotes Rechazados"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="AQL" Caption="% AQL"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:ASPxGridView>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <dx1:ASPxGridView ID="oqlemb" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="oqlemb_DataBinding" OnCustomCallback="oqlemb_CustomCallback">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="id_linea" Caption="id_linea" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha" PropertiesTextEdit-DisplayFormatString="yyyy-MM-dd"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="numero" Caption="Linea" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="PiezasAudit" Caption="Piezas Audit" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="PiezasAceptadas" Caption="Piezas Aceptadas"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="PiezasRechazadas" Caption="Piezas Rechazadas"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="OQL" Caption="% OQL"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:ASPxGridView>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="panelgrafico" runat="server" Visible="false">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center">
                                <h3>% AQL, % OQL  Linea - Semana</h3>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <dx:WebChartControl ID="g1" runat="server" Height="400px" Width="500px" RenderFormat="Svg" OnDataBinding="g1_DataBinding" OnCustomCallback="g1_CustomCallback">
                                    <SeriesSerializable>
                                        <dx:Series Name="AQL" ArgumentDataMember="Dia" ValueDataMembersSerializable="AQL" CrosshairLabelPattern="{V:F2} %" LabelsVisibility="True" CrosshairTextOptions-TextColor="Green">
                                            <ViewSerializable>
                                                <dx:SideBySideBarSeriesView></dx:SideBySideBarSeriesView>
                                            </ViewSerializable>
                                        </dx:Series>
                                    </SeriesSerializable>
                                    <BorderOptions Visibility="False" />
                                    <Titles>
                                        <dx:ChartTitle Alignment="Near" Text="" Font="Tahoma, 10pt"></dx:ChartTitle>
                                        <dx:ChartTitle Text="AQL %"></dx:ChartTitle>
                                    </Titles>
                                    <DiagramSerializable>
                                        <dx:XYDiagram>
                                            <AxisX VisibleInPanesSerializable="-1">
                                                <Label Angle="-30"></Label>
                                            </AxisX>
                                            <AxisY Title-Text="AQL" Title-Visibility="True" VisibleInPanesSerializable="-1" Interlaced="True">
                                            </AxisY>
                                        </dx:XYDiagram>
                                    </DiagramSerializable>
                                </dx:WebChartControl>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <dx:WebChartControl ID="g2" runat="server" Height="400px" Width="500px" RenderFormat="Svg" OnDataBinding="g2_DataBinding" OnCustomCallback="g2_CustomCallback">
                                    <SeriesSerializable>
                                        <dx:Series Name="OQL" ArgumentDataMember="Dia" ValueDataMembersSerializable="OQL" CrosshairLabelPattern="{V:F2} %" LabelsVisibility="True" CrosshairTextOptions-TextColor="Red">
                                            <ViewSerializable>
                                                <dx:SideBySideBarSeriesView></dx:SideBySideBarSeriesView>
                                            </ViewSerializable>
                                        </dx:Series>
                                    </SeriesSerializable>
                                    <BorderOptions Visibility="False" />
                                    <Titles>
                                        <dx:ChartTitle Alignment="Near" Text="" Font="Tahoma, 10pt"></dx:ChartTitle>
                                        <dx:ChartTitle Text="OQL %"></dx:ChartTitle>
                                    </Titles>
                                    <DiagramSerializable>
                                        <dx:XYDiagram>
                                            <AxisX VisibleInPanesSerializable="-1">
                                                <Label Angle="-30"></Label>
                                            </AxisX>
                                            <AxisY Title-Text="OQL" Title-Visibility="True" VisibleInPanesSerializable="-1" Interlaced="True">
                                            </AxisY>
                                        </dx:XYDiagram>
                                    </DiagramSerializable>
                                </dx:WebChartControl>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <hr />

                            </div>
                        </asp:Panel>

                        <asp:Panel ID="panelgrafico2" runat="server" Visible="false">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center; font-family: Calibri">
                                <h3>% AQL, % OQL  Planta - Semana</h3>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <dx:WebChartControl ID="g3" runat="server" Height="400px" Width="500px" RenderFormat="Svg" OnDataBinding="g3_DataBinding" OnCustomCallback="g3_CustomCallback">
                                    <SeriesSerializable>
                                        <dx:Series Name="AQL" ArgumentDataMember="Dia" ValueDataMembersSerializable="AQL" CrosshairLabelPattern="{V:F2} %" LegendTextPattern="{VP}"
                                            LabelsVisibility="True" CrosshairTextOptions-TextColor="Green" CrosshairTextOptions-Font="Calibri, 9pt">
                                            <ViewSerializable>
                                                <dx:SideBySideBarSeriesView Color="Green"></dx:SideBySideBarSeriesView>
                                            </ViewSerializable>
                                        </dx:Series>
                                    </SeriesSerializable>
                                    <BorderOptions Visibility="False" />
                                    <Titles>
                                        <dx:ChartTitle Alignment="Near" Text="" Font="Calibri, 10pt"></dx:ChartTitle>
                                        <dx:ChartTitle Text="AQL %"></dx:ChartTitle>
                                    </Titles>
                                    <DiagramSerializable>
                                        <dx:XYDiagram>
                                            <AxisX VisibleInPanesSerializable="-1">
                                                <Label Angle="-30"></Label>
                                            </AxisX>
                                            <AxisY Title-Text="AQL %" Title-Visibility="True" VisibleInPanesSerializable="-1" Interlaced="True" CrosshairAxisLabelOptions-Font="Calibri, 9pt">
                                            </AxisY>
                                        </dx:XYDiagram>
                                    </DiagramSerializable>
                                </dx:WebChartControl>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <dx:WebChartControl ID="g4" runat="server" Height="400px" Width="500px" RenderFormat="Svg" OnDataBinding="g4_DataBinding" OnCustomCallback="g4_CustomCallback">
                                    <SeriesSerializable>
                                        <dx:Series Name="OQL" ArgumentDataMember="Dia" ValueDataMembersSerializable="OQL" CrosshairLabelPattern="{V:F2} %" LegendTextPattern="{V:F2} %" LabelsVisibility="True"
                                            CrosshairTextOptions-TextColor="Red" CrosshairTextOptions-Font="Calibri, 9pt">
                                            <ViewSerializable>
                                                <dx:SideBySideBarSeriesView Color="Red"></dx:SideBySideBarSeriesView>
                                            </ViewSerializable>
                                        </dx:Series>
                                    </SeriesSerializable>
                                    <SeriesTemplate>
                                        <LabelSerializable>
                                            <dx:SideBySideBarSeriesLabel TextPattern="{V:F2}%"></dx:SideBySideBarSeriesLabel>
                                        </LabelSerializable>
                                    </SeriesTemplate>
                                    <BorderOptions Visibility="False" />
                                    <Titles>
                                        <dx:ChartTitle Alignment="Near" Text="" Font="Calibri, 10pt"></dx:ChartTitle>
                                        <dx:ChartTitle Text="OQL %"></dx:ChartTitle>
                                    </Titles>
                                    <DiagramSerializable>
                                        <dx:XYDiagram>
                                            <AxisX VisibleInPanesSerializable="-1">
                                                <Label Angle="-30"></Label>
                                            </AxisX>
                                            <AxisY Title-Text="OQL %" Title-Visibility="True" VisibleInPanesSerializable="-1" Interlaced="True" CrosshairAxisLabelOptions-Font="Calibri, 9pt">
                                            </AxisY>
                                        </dx:XYDiagram>
                                    </DiagramSerializable>
                                </dx:WebChartControl>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <hr />
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="panelGrafico3" runat="server" Visible="false">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center">
                                <h3>
                                    <asp:Label ID="lbl1" runat="server" Text=""></asp:Label>
                                </h3>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <dx:WebChartControl ID="grfAql" runat="server" Height="400px" Width="500px" RenderFormat="Svg" OnDataBinding="grfAql_DataBinding" OnCustomCallback="grfAql_CustomCallback">
                                    <SeriesSerializable>
                                        <dx:Series Name="AQL" ArgumentDataMember="Semana" ValueDataMembersSerializable="AQL" CrosshairLabelPattern="{V:F2} %"
                                            LabelsVisibility="True" CrosshairTextOptions-TextColor="Green" CrosshairTextOptions-Font="Calibri, 9pt">
                                            <ViewSerializable>
                                                <dx:SideBySideBarSeriesView Color="Green" FillStyle-FillMode="Gradient"></dx:SideBySideBarSeriesView>
                                            </ViewSerializable>
                                        </dx:Series>
                                    </SeriesSerializable>
                                    <BorderOptions Visibility="False" />
                                    <Titles>
                                        <dx:ChartTitle Alignment="Near" Text="" Font="Tahoma, 10pt"></dx:ChartTitle>
                                        <dx:ChartTitle Text="AQL %"></dx:ChartTitle>
                                    </Titles>
                                    <DiagramSerializable>
                                        <dx:XYDiagram>
                                            <AxisX VisibleInPanesSerializable="-1">
                                                <Label Angle="-30"></Label>
                                            </AxisX>
                                            <AxisY Title-Text="AQL %" Title-Visibility="True" VisibleInPanesSerializable="-1" Interlaced="True">
                                            </AxisY>
                                        </dx:XYDiagram>
                                    </DiagramSerializable>
                                </dx:WebChartControl>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <dx:WebChartControl ID="grfOql" runat="server" Height="400px" Width="500px" RenderFormat="Svg" OnDataBinding="grfOql_DataBinding" OnCustomCallback="grfOql_CustomCallback">
                                    <SeriesSerializable>
                                        <dx:Series Name="OQL" ArgumentDataMember="Semana" ValueDataMembersSerializable="OQL" CrosshairLabelPattern="{V:F2} %" LabelsVisibility="True"
                                            CrosshairTextOptions-TextColor="Red" CrosshairTextOptions-Font="Calibri, 9pt">
                                            <ViewSerializable>
                                                <dx:SideBySideBarSeriesView Color="Red" FillStyle-FillMode="Gradient"></dx:SideBySideBarSeriesView>
                                            </ViewSerializable>
                                        </dx:Series>
                                    </SeriesSerializable>
                                    <BorderOptions Visibility="False" />
                                    <Titles>
                                        <dx:ChartTitle Alignment="Near" Text="" Font="Tahoma, 10pt"></dx:ChartTitle>
                                        <dx:ChartTitle Text="OQL %"></dx:ChartTitle>
                                    </Titles>
                                    <DiagramSerializable>
                                        <dx:XYDiagram>
                                            <AxisX VisibleInPanesSerializable="-1">
                                                <Label Angle="-30"></Label>
                                            </AxisX>
                                            <AxisY Title-Text="OQL %" Title-Visibility="True" VisibleInPanesSerializable="-1" Interlaced="True">
                                            </AxisY>
                                        </dx:XYDiagram>
                                    </DiagramSerializable>
                                </dx:WebChartControl>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <hr />

                            </div>
                        </asp:Panel>

                        <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control m" Text="Export to Excel" OnClick="btnexcel_Click" />
                        <dx1:ASPxGridViewExporter ID="export" runat="server"></dx1:ASPxGridViewExporter>
                        <dx1:ASPxGridViewExporter ID="export2" runat="server"></dx1:ASPxGridViewExporter>
                        <dx1:ASPxGridViewExporter ID="export3" runat="server"></dx1:ASPxGridViewExporter>

                    </div>

                </div>
            </div>



        </div>
    </div>

</asp:Content>

