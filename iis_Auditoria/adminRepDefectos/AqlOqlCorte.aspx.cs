﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_AqlOqlCorte : System.Web.UI.Page
{
    class ClassArrm
    {
        public int id { get; set; }
        public string semana { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["CorteRep"] = null;
            Session["CorteRep1"] = null;
            Session["CorteRep2"] = null;
            Session["Grafico1"] = null;
            Session["Grafico2"] = null;
            Session["grf3"] = null;
            Session["grf31"] = null;

            cargacombo();
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();

            panelgrafico2.Visible = false;
            panelgrafico.Visible = false;
            panelCut.Visible = false;
            panellin.Visible = false;
            pnlineaemb.Visible = false;
            panelGrafico3.Visible = false;

            System.Globalization.CultureInfo norwCulture = System.Globalization.CultureInfo.CreateSpecificCulture("es");
            System.Globalization.Calendar cal = norwCulture.Calendar;
            int x = cal.GetWeekOfYear(DateTime.Now, norwCulture.DateTimeFormat.CalendarWeekRule, norwCulture.DateTimeFormat.FirstDayOfWeek);

            var list = new List<ClassArrm>();
            for (int i = 1; i <= x; i++)
            {
                ClassArrm obj = new ClassArrm();
                obj.id = i;
                obj.semana = "Semana " + i;
                list.Add(obj);
            }

            drpSemana.DataSource = list;
            drpSemana.DataTextField = "semana";
            drpSemana.DataValueField = "id";
            drpSemana.DataBind();

            drpSemana.SelectedValue = (x).ToString();
        }
    }

    void cargacombo()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta, descripcion FROM Planta order by descripcion asc";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            panelgrafico2.Visible = false;
            panelgrafico.Visible = false;
            panelCut.Visible = false;
            panellin.Visible = false;
            pnlineaemb.Visible = false;
            panelGrafico3.Visible = false;
            lbl1.Text = "";

            var grd11 = new DataTable();
            var AQL = new DataTable();
            var OQL1 = new DataTable();
            var grafico = new DataTable();
            var grafico2 = new DataTable();
            var grafico3 = new DataTable();
            string query = "";
            string queryaql = "";
            string queryoql = "";

            string op1 = rdaudit.SelectedValue;

            string opcionfi = rd1.SelectedValue;

            switch (op1)
            {
                case "1"://Auditoria Interna

                    break;

                case "2"://Auditoria Externa

                    break;

                case "3"://Auditoria Bulto a Bulto
                    switch (opcionfi)
                    {
                        case "1"://Corte                            
                            if (txtPorder.Text != string.Empty && hdnidporder.Value != string.Empty)
                            {
                                panelCut.Visible = true;

                                query = " select a.Id_Order, a.POrder, a.Bld, ISNULL(b.CantidadDefectos, 0) CantidadDefectos, ISNULL(b.Codigo, '') Codigo, ISNULL(b.Posicion, 'S/P') Posicion from"
                                      + " (select p.Id_Order, p.POrder, b.Bld from POrder p join Bundle b on b.Id_Order = p.Id_Order join Style s on s.Id_Style = p.Id_Style where p.Id_Order = " + hdnidporder.Value + ") a"
                                      + " left join (select p.Id_Order, b.Bld, brd.CantidadDefectos, d.Codigo, ps.Posicion, brd.idBBRechazadoDet from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on br.idBBRechazado = brd.idBBRechazado"
                                      + " left join tbDefectos d on d.idDefecto = brd.idDefectos join tbPosicion ps on ps.idPosicion = brd.idPosicion join Bundle b on b.Id_Bundle = br.idBulto join POrder p on p.Id_Order = b.Id_Order"
                                      + " where p.Id_Order = " + hdnidporder.Value + ") b on a.Id_Order = b.Id_Order and a.Bld = b.Bld"
                                      + " order by a.Bld";

                                grd11 = DataAccess.Get_DataTable(query);
                                Session["CorteRep"] = grd11;
                                grd1.DataSource = grd11;
                                grd1.DataBind();

                                queryaql = " select a.Id_Order, a.POrder, a.Style, a.Quantity,(ISNULL(b.BultoAprovados,0) + ISNULL(c.BultoRechazados,0)) BultosAudit, ISNULL(b.BultoAprovados,0) BultoAprovados, ISNULL(c.BultoRechazados,0) BultoRechazados"
                                    + " ,CONCAT(CAST(CAST(ISNULL(b.BultoAprovados,0) as decimal(16, 3)) / CAST((CAST(ISNULL(b.BultoAprovados,0) as decimal(16, 3)) + CAST(ISNULL(c.BultoRechazados,0) as decimal(16, 3))) as decimal(16, 3)) * 100 as decimal(16, 3)), ' %') AQL"
                                    + " from ( select p.Id_Order, l.numero Linea, p.POrder, s.Style, p.Quantity from POrder p join Style s on s.Id_Style = p.Id_Style join Linea l on l.id_linea = p.Id_Linea where p.Id_Order = " + hdnidporder.Value + ") a"
                                    + " left join (select p.Id_Order, COUNT(IdBulto) BultoAprovados from tbBultoaBultoAprovados ba join Bundle b on b.Id_Bundle = ba.IdBulto join POrder p on p.Id_Order = b.Id_Order where p.Id_Order = " + hdnidporder.Value + " group by p.Id_Order"
                                    + " ) b on a.Id_Order = b.Id_Order left join ( select p.Id_Order, COUNT(IdBulto) BultoRechazados from tbBultoaBultoRechazado br join Bundle b on b.Id_Bundle = br.IdBulto join POrder p on p.Id_Order = b.Id_Order"
                                    + " where p.Id_Order = " + hdnidporder.Value + " group by p.Id_Order ) c on c.Id_Order = a.Id_Order";

                                AQL = DataAccess.Get_DataTable(queryaql);
                                Session["CorteRep1"] = AQL;
                                AQl.DataSource = AQL;
                                AQl.DataBind();

                                queryoql = " select a.Id_Order, a.POrder, ISNULL(b.BultoAprovados,0) BultoAprovados, ISNULL(c.BultoRechazados,0) BultoRechazados, (ISNULL(Muestra_Ap,0) + ISNULL(MuestraRe,0)) PiezasAudit, ((ISNULL(Muestra_Ap,0) + ISNULL(MuestraRe,0)) - ISNULL(rechazo,0)) PiezasAceptadas, ISNULL(rechazo,0) PiezasRechazadas,"
                                    + " CONCAT(CAST((CAST(ISNULL(rechazo,0) as decimal(16, 3))) / (CAST(ISNULL(Muestra_Ap,0) as decimal(16, 3)) + CAST(ISNULL(MuestraRe,0) as decimal(16, 3))) * 100 as decimal(16, 3)), ' %') OQL"
                                    + " from (select p.Id_Order, l.numero Linea, p.POrder, s.Style, p.Quantity from POrder p  join Style s on s.Id_Style = p.Id_Style join Linea l on l.id_linea = p.Id_Linea where p.Id_Order = " + hdnidporder.Value + " ) a"
                                    + " left join (select p.Id_Order, COUNT(IdBulto) BultoAprovados, SUM(UnidadesAuditadas) Muestra_Ap from tbBultoaBultoAprovados ba join Bundle b on b.Id_Bundle = ba.IdBulto join POrder p on p.Id_Order = b.Id_Order where p.Id_Order = " + hdnidporder.Value + " group by p.Id_Order ) b"
                                    + " on a.Id_Order = b.Id_Order left join (select p.Id_Order, COUNT(IdBulto) BultoRechazados, SUM(Muestreo) MuestraRe, SUM(ISNULL(PiezasRechazadas, 0)) rechazo from tbBultoaBultoRechazado br join Bundle b on b.Id_Bundle = br.IdBulto"
                                    + " join POrder p on p.Id_Order = b.Id_Order where p.Id_Order = " + hdnidporder.Value + " group by p.Id_Order ) c on c.Id_Order = a.Id_Order";

                                OQL1 = DataAccess.Get_DataTable(queryoql);
                                Session["CorteRep2"] = OQL1;
                                OQL.DataSource = OQL1;
                                OQL.DataBind();

                            }

                            break;

                        case "2"://Estilo

                            break;

                        case "3"://Linea
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                panellin.Visible = true;

                                //query = " select a.Id_Order, a.POrder, a.Style, a.Quantity,(isnull(b.BultoAprovados, 0) + ISNULL(c.BultoRechazados, 0)) BultosAudit, ISNULL(b.BultoAprovados, 0) BultoAprovados, ISNULL(c.BultoRechazados, 0) BultoRechazados,"
                                //    + " (isnull(Muestra_Ap, 0) + ISNULL(MuestraRe, 0)) PiezasAudit, ((ISNULL(Muestra_Ap, 0) + ISNULL(MuestraRe, 0)) - ISNULL(rechazo, 0)) PiezasAceptadas, ISNULL(rechazo, 0) PiezasRechazadas , CONCAT(CAST(CAST(ISNULL(b.BultoAprovados, 0) as decimal(5, 3)) /"
                                //    + " CAST((CAST(isnull(b.BultoAprovados, 0) as decimal(5, 3)) + CAST(isnull(c.BultoRechazados, 0) as decimal(5, 3))) as decimal(5, 3)) * 100 as decimal(5, 3)), ' %') AQL, CONCAT(CAST((CAST(isnull(rechazo, 0) as decimal(16, 3))) /"
                                //    + " (CAST(isnull(Muestra_Ap, 0) as decimal(16, 3)) + CAST(ISNULL(MuestraRe, 0) as decimal(16, 3))) * 100 as decimal(16, 3)), ' %') OQL from (select p.Id_Order, l.numero Linea, p.POrder, s.Style, p.Quantity from POrder p join Style s on s.Id_Style = p.Id_Style"
                                //    + " join Linea l on l.id_linea = p.Id_Linea where p.id_Linea2 = " + DropDownListLine.SelectedValue + " ) a left join (select p.Id_Order, COUNT(IdBulto) BultoAprovados, SUM(UnidadesAuditadas) Muestra_Ap from tbBultoaBultoAprovados ba join Bundle b on b.Id_Bundle = ba.IdBulto"
                                //    + " join POrder p on p.Id_Order = b.Id_Order where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, ba.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Order ) b on a.Id_Order = b.Id_Order right join (select p.Id_Order, COUNT(IdBulto) BultoRechazados, SUM(Muestreo) MuestraRe,"
                                //    + " SUM(ISNULL(PiezasRechazadas, 0)) rechazo from tbBultoaBultoRechazado br join Bundle b on b.Id_Bundle = br.IdBulto join POrder p on p.Id_Order = b.Id_Order where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Order"
                                //    + " ) c on a.Id_Order = c.Id_Order";

                                query = " select a.Id_Order, b.Fecha, a.POrder, a.Linea, sum(isnull(b.BultoAprovados, 0)) BultosAudit, sum(case when b.apro = 1 then b.BultoAprovados else 0 end) BultoAprovados,"
                                      + " sum(case when b.apro = 2 then b.BultoAprovados else 0 end) BultoRechazados, sum(isnull(Muestra_Ap, 0)) PiezasAudit, sum(b.Muestra_Ap)-sum(b.rechazo) PiezasAceptadas,"
                                      + " sum(case when b.apro=2 then b.rechazo else 0 end) PiezasRechazadas, concat(CAST(cast(sum(case when b.apro = 1 then b.BultoAprovados else 0 end) as decimal(5, 2)) /"
                                      + " cast(sum(isnull(b.BultoAprovados, 0)) as decimal(5, 2)) * 100 as decimal(5, 2)), ' %') as AQL, concat(CAST(cast(sum(b.rechazo) as decimal(5, 2)) /"
                                      + " cast(sum(isnull(b.Muestra_Ap, 0)) as decimal(5, 2)) * 100 as decimal(5, 2)), ' %') as OQL from ("
                                      + " select p.Id_Order, MAX(CONVERT(date,FechaInspeccion)) Fecha, COUNT(IdBulto) BultoAprovados, SUM(UnidadesAuditadas) Muestra_Ap, 1 as apro, 0 as rechazo from tbBultoaBultoAprovados ba join Bundle b on b.Id_Bundle = ba.IdBulto"
                                      + " join POrder p on p.Id_Order = b.Id_Order where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, ba.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Order union"
                                      + " select p.Id_Order, MAX(CONVERT(date,FechaInspeccion)) Fecha, COUNT(IdBulto) BultoRechazados, SUM(Muestreo) MuestraRe, 2 as rech, SUM(ISNULL(PiezasRechazadas, 0)) rechazo from tbBultoaBultoRechazado br"
                                      + " join Bundle b on b.Id_Bundle = br.IdBulto join POrder p on p.Id_Order = b.Id_Order where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                                      + " group by p.Id_Order) b join(select p.Id_Order, l.numero Linea, p.POrder, s.Style, p.Quantity from POrder p join Style s on s.Id_Style = p.Id_Style join Linea l on l.id_linea = p.Id_Linea2"
                                      + " where p.id_Linea2 = " + DropDownListLine.SelectedValue + ") a on b.Id_Order = a.Id_Order group by a.Id_Order, a.POrder, a.Linea, b.Fecha";

                                grd11 = DataAccess.Get_DataTable(query);
                                Session["CorteRep"] = grd11;
                                gridlinea.DataSource = grd11;
                                gridlinea.DataBind();

                                queryaql = " select case when b.Fecha > c.Fecha then b.Fecha else c.Fecha end Fecha, a.id_linea, a.numero, (SUM(isnull(b.BultoAprovados, 0)) + sum(isnull(c.BultoRechazados, 0))) BultosAudit, sum(ISNULL(b.BultoAprovados, 0)) BultoAprovados, sum(ISNULL(c.BultoRechazados, 0)) BultoRechazados,"
                                         + " CONCAT(CAST(CAST(SUM(ISNULL(b.BultoAprovados, 0)) as decimal(16, 3)) / CAST((CAST(SUM(ISNULL(b.BultoAprovados, 0)) as decimal(16, 3)) + CAST(sum(isnull(c.BultoRechazados, 0)) as decimal(16, 3))) as decimal(16, 3)) * 100 as decimal(5, 3)), ' %') AQL"
                                         + " from (select l.id_linea, l.numero from Linea l where l.id_linea = " + DropDownListLine.SelectedValue + " ) a left join (select MAX(CONVERT(date,ba.FechaInspeccion)) Fecha, p.Id_Linea2, COUNT(IdBulto) BultoAprovados from tbBultoaBultoAprovados ba join Bundle b on b.Id_Bundle = ba.IdBulto join POrder p on p.Id_Order = b.Id_Order"
                                         + " where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, ba.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Linea2 ) b on a.id_linea = b.Id_Linea2 right join (select MAX(CONVERT(date,br.FechaInspeccion)) Fecha, p.Id_Linea2, COUNT(IdBulto) BultoRechazados from tbBultoaBultoRechazado br"
                                         + " join Bundle b on b.Id_Bundle = br.IdBulto join POrder p on p.Id_Order = b.Id_Order where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Linea2 ) c on c.Id_Linea2 = a.id_linea"
                                         + " group by a.id_linea, a.numero, b.Fecha, c.Fecha";

                                AQL = DataAccess.Get_DataTable(queryaql);
                                Session["CorteRep1"] = AQL;
                                aqllinea.DataSource = AQL;
                                aqllinea.DataBind();

                                queryoql = " select case when b.Fecha > c.Fecha then b.Fecha else c.Fecha end Fecha, a.id_linea, a.numero, (SUM(isnull(Muestra_Ap, 0)) + SUM(ISNULL(MuestraRe, 0))) PiezasAudit, ((SUM(ISNULL(Muestra_Ap, 0)) + SUM(ISNULL(MuestraRe, 0))) - SUM(ISNULL(rechazo, 0))) PiezasAceptadas, sum(ISNULL(rechazo, 0)) PiezasRechazadas,"
                                         + " CONCAT(CAST((CAST(SUM(isnull(rechazo, 0)) as decimal(16, 3))) / (CAST(SUM(ISNULL(Muestra_Ap, 0)) as decimal(16, 3)) + CAST(sum(isnull(MuestraRe, 0)) as decimal(16, 3))) * 100 as decimal(16, 3)), ' %') OQL from (select l.id_linea, l.numero "
                                         + " from Linea l where l.id_linea = " + DropDownListLine.SelectedValue + ") a left join (select MAX(CONVERT(date,ba.FechaInspeccion)) Fecha, p.Id_Linea2, SUM(UnidadesAuditadas) Muestra_Ap from tbBultoaBultoAprovados ba join Bundle b on b.Id_Bundle = ba.IdBulto join POrder p on p.Id_Order = b.Id_Order where p.Id_Linea2 = " + DropDownListLine.SelectedValue + ""
                                         + " and CONVERT(date, ba.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Linea2 ) b on a.id_linea = b.Id_Linea2 right join (select MAX(CONVERT(date,br.FechaInspeccion)) Fecha, p.Id_Linea2, SUM(Muestreo) MuestraRe, SUM(ISNULL(PiezasRechazadas, 0)) rechazo from tbBultoaBultoRechazado br"
                                         + " join Bundle b on b.Id_Bundle = br.IdBulto join POrder p on p.Id_Order = b.Id_Order where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Linea2 ) c on c.Id_Linea2 = a.id_linea"
                                         + " group by a.id_linea, a.numero, b.Fecha, c.Fecha";

                                OQL1 = DataAccess.Get_DataTable(queryoql);
                                Session["CorteRep2"] = OQL1;
                                oqllinea.DataSource = OQL1;
                                oqllinea.DataBind();
                            }

                            break;

                        case "4"://Planta

                            break;

                        case "6"://Linea - Semana
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                panelgrafico.Visible = true;

                                query = " select a.id_linea, a.numero Linea, c.Dia, CAST(CAST(ISNULL(b.BultosAprovados, 0) as decimal(16, 3)) / CAST((CAST(isnull(b.BultosAprovados, 0) as decimal(16, 3)) + CAST(isnull(c.BultoRechazados, 0) as decimal(16, 3))) as decimal(16, 3)) * 100 as decimal(16, 3)) AQL,"
                                      + " CAST((CAST(isnull(rechazo, 0) as decimal(16, 3))) / (CAST(isnull(Muestra_Ap, 0) as decimal(16, 3)) + CAST(ISNULL(MuestraRe, 0) as decimal(16, 3))) * 100 as decimal(16, 3)) OQL"
                                      + " from (select l.id_linea, l.numero from Linea l where l.id_linea = " + DropDownListLine.SelectedValue + ") a left join(select l.id_linea,"
                                      + " case when DATEPART(DW, CONVERT(date, ba.FechaInspeccion)) = 1 then 'Lunes' else case when DATEPART(DW, CONVERT(date, ba.FechaInspeccion)) = 2 then 'Martes' else"
                                      + " case when DATEPART(DW, CONVERT(date, ba.FechaInspeccion)) = 3 then 'Miercoles' else case when DATEPART(DW, CONVERT(date, ba.FechaInspeccion)) = 4 then 'Jueves' else"
                                      + " case when DATEPART(DW, CONVERT(date, ba.FechaInspeccion)) = 5 then 'Viernes' else case when DATEPART(DW, CONVERT(date, ba.FechaInspeccion)) = 6 then 'Sabado'"
                                      + " end end end end end end Dia, COUNT(IdBulto) BultosAprovados , SUM(ISNULL(ba.UnidadesAuditadas, 0)) Muestra_Ap from tbBultoaBultoAprovados ba"
                                      + " join Linea l on l.id_linea = ba.idLinea where l.id_linea = " + DropDownListLine.SelectedValue + " and DATEPART(Week, CONVERT(date, ba.FechaInspeccion)) = " + drpSemana.SelectedValue + " group by l.id_linea, DATEPART(DW, CONVERT(date, ba.FechaInspeccion))"
                                      + " ) b on a.id_linea = b.id_linea right join(select l.id_linea, case when DATEPART(DW, CONVERT(date, br.FechaInspeccion)) = 1 then 'Lunes' else case when DATEPART(DW, CONVERT(date, br.FechaInspeccion)) = 2 then 'Martes' else"
                                      + " case when DATEPART(DW, CONVERT(date, br.FechaInspeccion)) = 3 then 'Miercoles' else case when DATEPART(DW, CONVERT(date, br.FechaInspeccion)) = 4 then 'Jueves' else"
                                      + " case when DATEPART(DW, CONVERT(date, br.FechaInspeccion)) = 5 then 'Viernes' else case when DATEPART(DW, CONVERT(date, br.FechaInspeccion)) = 6 then 'Sabado'"
                                      + " end end end end end end Dia, COUNT(idBulto) BultoRechazados, SUM(Muestreo) MuestraRe, SUM(ISNULL(PiezasRechazadas, 0)) rechazo from tbBultoaBultoRechazado br"
                                      + " join Linea l on l.id_linea = br.idLinea where l.id_linea = " + DropDownListLine.SelectedValue + " and DATEPART(Week, CONVERT(date, br.FechaInspeccion)) = " + drpSemana.SelectedValue + " group by l.id_linea, DATEPART(DW, CONVERT(date, br.FechaInspeccion))) c on a.id_linea = c.id_linea and b.Dia = c.Dia";

                                grafico = DataAccess.Get_DataTable(query);
                                Session["Grafico1"] = grafico;
                                g1.DataSource = grafico;
                                g1.DataBind();

                                g2.DataSource = grafico;
                                g2.DataBind();
                            }
                            break;

                        case "7"://Planta - Semana
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                panelgrafico2.Visible = true;

                                query = " select a.id_planta, a.descripcion Planta, c.Dia, CAST(CAST(ISNULL(b.BultosAprovados, 0) as decimal(16, 3)) / CAST((CAST(isnull(b.BultosAprovados, 0) as decimal(16, 3)) + CAST(isnull(c.BultoRechazados, 0) as decimal(16, 3))) as decimal(16, 3)) * 100 as decimal(16, 3)) AQL,"
                                      + " CAST((CAST(isnull(rechazo, 0) as decimal(16, 3))) / (CAST(isnull(Muestra_Ap, 0) as decimal(16, 3)) + CAST(ISNULL(MuestraRe, 0) as decimal(16, 3))) * 100 as decimal(16, 3)) OQL from"
                                      + " (select pl.id_planta, pl.descripcion from Linea l join Planta pl on pl.id_planta = l.id_linea where pl.id_planta = " + DropDownListPlant.SelectedValue + ") a left join"
                                      + " (select pl.id_planta, case when DATEPART(DW, CONVERT(date, ba.FechaInspeccion)) = 1 then 'Lunes' else case when DATEPART(DW, CONVERT(date, ba.FechaInspeccion)) = 2 then 'Martes' else case when DATEPART(DW, CONVERT(date, ba.FechaInspeccion)) = 3 then 'Miercoles' else case when DATEPART(DW, CONVERT(date, ba.FechaInspeccion)) = 4 then 'Jueves' else "
                                      + " case when DATEPART(DW, CONVERT(date, ba.FechaInspeccion)) = 5 then 'Viernes' else case when DATEPART(DW, CONVERT(date, ba.FechaInspeccion)) = 6 then 'Sabado' end end end end end end Dia, COUNT(IdBulto) BultosAprovados , SUM(ISNULL(ba.UnidadesAuditadas, 0)) Muestra_Ap"
                                      + " from tbBultoaBultoAprovados ba join Linea l on l.id_linea = ba.idLinea join Planta pl on pl.id_planta = l.id_planta where pl.id_planta = " + DropDownListPlant.SelectedValue + " and DATEPART(Week, CONVERT(date, ba.FechaInspeccion)) = " + drpSemana.SelectedValue + " group by pl.id_planta, DATEPART(DW, CONVERT(date, ba.FechaInspeccion))"
                                      + " ) b on a.id_planta = b.id_planta right join(select pl.id_planta, case when DATEPART(DW, CONVERT(date, br.FechaInspeccion)) = 1 then 'Lunes' else case when DATEPART(DW, CONVERT(date, br.FechaInspeccion)) = 2 then 'Martes' else"
                                      + " case when DATEPART(DW, CONVERT(date, br.FechaInspeccion)) = 3 then 'Miercoles' else case when DATEPART(DW, CONVERT(date, br.FechaInspeccion)) = 4 then 'Jueves' else"
                                      + " case when DATEPART(DW, CONVERT(date, br.FechaInspeccion)) = 5 then 'Viernes' else case when DATEPART(DW, CONVERT(date, br.FechaInspeccion)) = 6 then 'Sabado' end end end end end end Dia,"
                                      + " COUNT(idBulto) BultoRechazados, SUM(Muestreo) MuestraRe, SUM(ISNULL(PiezasRechazadas, 0)) rechazo from tbBultoaBultoRechazado br join Linea l on l.id_linea = br.idLinea join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.id_planta = " + DropDownListPlant.SelectedValue + " and DATEPART(Week, CONVERT(date, br.FechaInspeccion)) = " + drpSemana.SelectedValue + " group by pl.id_planta, DATEPART(DW, CONVERT(date, br.FechaInspeccion))) c on a.id_planta = c.id_planta and b.Dia = c.Dia";

                                grafico2 = DataAccess.Get_DataTable(query);
                                Session["Grafico2"] = grafico2;
                                g3.DataSource = grafico2;
                                g3.DataBind();

                                g4.DataSource = grafico2;
                                g4.DataBind();
                            }
                            break;

                        case "8":
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                lbl1.Text = "% AQL - % OQL, Global - Planta";
                                int semana1 = 0;
                                int semana2 = 0;

                                panelGrafico3.Visible = true;

                                semana1 = Convert.ToInt16(drpSemana.SelectedValue) - 3;
                                semana2 = Convert.ToInt16(drpSemana.SelectedValue);

                                query = " select a.id_planta, a.descripcion Planta, 'Semana: ' + Convert(varchar, a.Semana) Semana, CAST(CAST(ISNULL(b.BultosAprovados, 0) as decimal(16, 3)) /"
                                      + " CAST((CAST(isnull(b.BultosAprovados, 0) as decimal(16, 3)) + CAST(isnull(c.BultoRechazados, 0) as decimal(16, 3))) as decimal(16, 3)) * 100 as decimal(16, 3)) AQL,"
                                      + " CAST((CAST(isnull(rechazo, 0) as decimal(16, 3))) / (CAST(isnull(Muestra_Ap, 0) as decimal(16, 3)) + CAST(ISNULL(MuestraRe, 0) as decimal(16, 3))) * 100 as decimal(16, 3)) OQL"
                                      + " from (select pl.id_planta, pl.descripcion, DATEPART(WEEK, CONVERT(date, ba.FechaInspeccion)) Semana from tbBultoaBultoAprovados ba"
                                      + " join Linea l on l.id_linea = ba.idLinea join Planta pl on pl.id_planta = l.id_planta "
                                      + " where pl.id_planta = " + DropDownListPlant.SelectedValue + " and DATEPART(Week, CONVERT(date, ba.FechaInspeccion)) between " + semana1 + " and " + semana2 + " group by pl.id_planta, pl.descripcion, DATEPART(WEEK, CONVERT(date, ba.FechaInspeccion))"
                                      + " union"
                                      + " select pl.id_planta, pl.descripcion, DATEPART(WEEK, CONVERT(date, br.FechaInspeccion)) Semana from tbBultoaBultoRechazado br"
                                      + " join Linea l on l.id_linea = br.idLinea join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.id_planta = " + DropDownListPlant.SelectedValue + " and DATEPART(Week, CONVERT(date, br.FechaInspeccion)) between " + semana1 + " and " + semana2 + " group by pl.id_planta, pl.descripcion, DATEPART(WEEK, CONVERT(date, br.FechaInspeccion))"
                                      + " ) a right join (select pl.id_planta, DATEPART(WEEK, CONVERT(date, ba.FechaInspeccion)) Semana, COUNT(IdBulto) BultosAprovados, SUM(ISNULL(ba.UnidadesAuditadas, 0)) Muestra_Ap"
                                      + " from tbBultoaBultoAprovados ba join Linea l on l.id_linea = ba.idLinea join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.id_planta = " + DropDownListPlant.SelectedValue + " and DATEPART(Week, CONVERT(date, ba.FechaInspeccion)) between " + semana1 + " and " + semana2 + " group by pl.id_planta, DATEPART(WEEK, CONVERT(date, ba.FechaInspeccion))"
                                      + " ) b on a.id_planta = b.id_planta and a.Semana = b.Semana right join (select pl.id_planta, DATEPART(WEEK, CONVERT(date, br.FechaInspeccion)) Semana,"
                                      + " COUNT(idBulto) BultoRechazados, SUM(Muestreo) MuestraRe, SUM(ISNULL(PiezasRechazadas, 0)) rechazo from tbBultoaBultoRechazado br"
                                      + " join Linea l on l.id_linea = br.idLinea join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.id_planta = " + DropDownListPlant.SelectedValue + " and DATEPART(Week, CONVERT(date, br.FechaInspeccion)) between " + semana1 + " and " + semana2 + " group by pl.id_planta, DATEPART(WEEK, CONVERT(date, br.FechaInspeccion))"
                                      + " ) c on a.id_planta = c.id_planta and a.Semana = c.Semana";

                                grafico3 = DataAccess.Get_DataTable(query);
                                Session["grf3"] = grafico3;

                                grfAql.DataSource = grafico3;
                                grfAql.DataBind();

                                grfOql.DataSource = grafico3;
                                grfOql.DataBind();

                            }
                            break;

                        case "9":
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                lbl1.Text = "% AQL - % OQL, Global - Linea";
                                int semana1 = 0;
                                int semana2 = 0;

                                panelGrafico3.Visible = true;

                                semana1 = Convert.ToInt16(drpSemana.SelectedValue) - 3;
                                semana2 = Convert.ToInt16(drpSemana.SelectedValue);

                                query = " select a.id_linea id, a.numero Linea, 'Semana: ' + Convert(varchar, a.Semana) Semana,"
                                      + " CAST(CAST(ISNULL(b.BultosAprovados, 0) as decimal(16, 3)) /"
                                      + " CAST((CAST(isnull(b.BultosAprovados, 0) as decimal(16, 3)) + CAST(isnull(c.BultoRechazados, 0) as decimal(16, 3))) as decimal(16, 3)) * 100 as decimal(16, 3)) AQL,"
                                      + " CAST((CAST(isnull(rechazo, 0) as decimal(16, 3))) / (CAST(isnull(Muestra_Ap, 0) as decimal(16, 3)) + CAST(ISNULL(MuestraRe, 0) as decimal(16, 3))) * 100 as decimal(16, 3)) OQL"
                                      + " from (select l.id_linea, l.numero, DATEPART(WEEK, CONVERT(date, ba.FechaInspeccion)) Semana from tbBultoaBultoAprovados ba"
                                      + " join Linea l on l.id_linea = ba.idLinea where l.id_linea = " + DropDownListLine.SelectedValue + " and DATEPART(Week, CONVERT(date, ba.FechaInspeccion))"
                                      + " between " + semana1 + " and " + semana2 + " group by l.id_linea, l.numero, DATEPART(WEEK, CONVERT(date, ba.FechaInspeccion))"
                                      + " union select l.id_linea, l.numero, DATEPART(WEEK, CONVERT(date, br.FechaInspeccion)) Semana from tbBultoaBultoRechazado br"
                                      + " join Linea l on l.id_linea = br.idLinea where l.id_linea = " + DropDownListLine.SelectedValue + " and DATEPART(Week, CONVERT(date, br.FechaInspeccion)) between " + semana1 + " and " + semana2 + ""
                                      + " group by l.id_linea, l.numero, DATEPART(WEEK, CONVERT(date, br.FechaInspeccion))) a right join ("
                                      + " select l.id_linea, DATEPART(WEEK, CONVERT(date, ba.FechaInspeccion)) Semana, COUNT(IdBulto) BultosAprovados, SUM(ISNULL(ba.UnidadesAuditadas, 0)) Muestra_Ap"
                                      + " from tbBultoaBultoAprovados ba join Linea l on l.id_linea = ba.idLinea where l.id_linea = " + DropDownListLine.SelectedValue + " and "
                                      + " DATEPART(Week, CONVERT(date, ba.FechaInspeccion)) between " + semana1 + " and " + semana2 + ""
                                      + " group by l.id_linea, DATEPART(WEEK, CONVERT(date, ba.FechaInspeccion))) b on a.id_linea = b.id_linea and a.Semana = b.Semana"
                                      + " right join (select l.id_linea, DATEPART(WEEK, CONVERT(date, br.FechaInspeccion)) Semana, COUNT(idBulto) BultoRechazados, SUM(Muestreo) MuestraRe, SUM(ISNULL(PiezasRechazadas, 0)) rechazo"
                                      + " from tbBultoaBultoRechazado br join Linea l on l.id_linea = br.idLinea where l.id_linea = " + DropDownListLine.SelectedValue + " and "
                                      + " DATEPART(Week, CONVERT(date, br.FechaInspeccion)) between " + semana1 + " and " + semana2 + ""
                                      + " group by l.id_linea, DATEPART(WEEK, CONVERT(date, br.FechaInspeccion))) c on a.id_linea = c.id_linea and a.Semana = c.Semana";

                                grafico3 = DataAccess.Get_DataTable(query);
                                Session["grf3"] = grafico3;

                                grfAql.DataSource = grafico3;
                                grfAql.DataBind();

                                grfOql.DataSource = grafico3;
                                grfOql.DataBind();
                            }
                            break;

                        default: break;
                    }

                    break;

                case "4"://Auditoria Inspeccion Proceso

                    break;

                case "5"://Auditoria Embarque
                    switch (opcionfi)
                    {
                        case "1":

                            break;

                        case "2":

                            break;

                        case "3"://Linea
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                pnlineaemb.Visible = true;

                                query = " select MAX(CONVERT(date,na.horaInicial)) Fecha, p.POrder, idcorte, COUNT(codigoLote) LotesAuditados, ISNULL(case when na.estadoAudit = 1 then COUNT(ae.codigoLote) end,0) LoteAprobados,"
                                      + " ISNULL(case when na.estadoAudit = 0 then ISNULL(COUNT(ae.codigoLote),0) end,0) LoteRechazado, SUM(ISNULL(ae.muestra, 0)) PiezasAuditadas,"
                                      + " SUM(ISNULL(ae.muestra, 0)) - SUM(ISNULL(na.UnidadesDefect, 0)) PiezasAceptadas, SUM(ISNULL(na.UnidadesDefect, 0)) PiezasRechazadas,"
                                      + " CONCAT(CAST(CAST(ISNULL(case when na.estadoAudit = 1 then COUNT(ae.codigoLote) end,0) as decimal(16, 3)) / CAST(ISNULL(COUNT(codigoLote), 0) as decimal(16, 3)) * 100 as decimal(16, 3)),' %') AQL,"
                                      + " CONCAT(CAST(CAST(SUM(ISNULL(na.UnidadesDefect, 0)) as decimal(16, 3)) / CAST(SUM(ISNULL(ae.muestra, 0)) as decimal(16, 3)) * 100 as decimal(16, 3)), ' %') OQL"
                                      + " from tbAuditEmbarque ae join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join POrder p on p.Id_Order = ae.idcorte"
                                      + " where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and na.secuencia = 1 group by p.POrder, idcorte, na.estadoAudit";
                                grd11 = DataAccess.Get_DataTable(query);
                                Session["CorteRep"] = grd11;
                                grdlinemb.DataSource = grd11;
                                grdlinemb.DataBind();

                                queryaql = " select a.Fecha, a.id_linea, a.numero, SUM(ISNULL(a.LotesAuditados, 0)) LotesAuditados, SUM(ISNULL(a.LoteAprobados, 0)) LoteAprobados, SUM(ISNULL(a.LoteRechazado, 0)) LoteRechazado,"
                                         + " CONCAT(CAST(CAST(SUM(ISNULL(a.LoteAprobados, 0)) as decimal(7, 3)) / CAST(ISNULL(SUM(ISNULL(a.LotesAuditados, 0)), 0) as decimal(7, 3)) * 100 as decimal(6, 3)), ' %') AQL"
                                         + " from(select MAX(CONVERT(date,na.horainicial)) Fecha, l.id_linea, l.numero, COUNT(codigoLote) LotesAuditados, ISNULL(case when na.estadoAudit = 1 then COUNT(ae.codigoLote) end,0) LoteAprobados,"
                                         + " ISNULL(case when na.estadoAudit = 0 then ISNULL(COUNT(ae.codigoLote),0) end,0) LoteRechazado from tbAuditEmbarque ae"
                                         + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join POrder p on p.Id_Order = ae.idcorte join Linea l on l.id_linea = p.Id_Linea2"
                                         + " where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and na.secuencia = 1"
                                         + " group by l.id_linea, l.numero, na.estadoAudit) a group by a.id_linea, a.numero, a.Fecha";
                                AQL = DataAccess.Get_DataTable(queryaql);
                                Session["CorteRep1"] = AQL;
                                aqlemb.DataSource = AQL;
                                aqlemb.DataBind();

                                queryoql = " select MAX(CONVERT(date,na.horaInicial)) Fecha, l.id_linea, l.numero, SUM(ISNULL(ae.muestra, 0)) PiezasAudit, SUM(ISNULL(ae.muestra, 0)) - SUM(ISNULL(na.UnidadesDefect, 0)) PiezasAceptadas,"
                                         + " SUM(ISNULL(na.UnidadesDefect, 0)) PiezasRechazadas, CONCAT(CAST(CAST(SUM(ISNULL(na.UnidadesDefect, 0)) as decimal(7, 3)) / CAST(SUM(ISNULL(ae.muestra, 0)) as decimal(7, 3)) * 100 as decimal(6, 3)), ' %') OQL"
                                         + " from tbAuditEmbarque ae join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join POrder p on p.Id_Order = ae.idcorte join Linea l on l.id_linea = p.Id_Linea2"
                                         + " where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and CONVERT(date, na.horaInicial) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' and na.secuencia = 1 group by l.id_linea, l.numero";
                                OQL1 = DataAccess.Get_DataTable(queryoql);
                                Session["CorteRep2"] = OQL1;
                                oqlemb.DataSource = OQL1;
                                oqlemb.DataBind();
                            }

                            break;

                        case "4":

                            break;

                        case "5":

                            break;

                        case "6"://Linea - Semana
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                panelgrafico.Visible = true;

                                query = " select a.id_linea, a.numero, a.D, a.Dia, (CAST((CAST(SUM(ISNULL(LoteAprobados,0)) as decimal(7,3)) / CAST(SUM(ISNULL(LotesAuditados,0)) as decimal(7,3))) * 100 as decimal(6,3))) AQL,"
                                      + " (CAST((CAST(SUM(ISNULL(PiezasRechazadas,0)) as decimal(7,3)) / CAST(SUM(ISNULL(PiezasAuditadas,0)) as decimal(7,3))) * 100 as decimal(6,3))) OQL from (select l.id_linea, l.numero, DATEPART(DW,CONVERT(date,na.horaInicial)) D,"
                                      + " case when DATEPART(DW, CONVERT(date, na.horaInicial)) = 1 then 'Lunes' else case when DATEPART(DW, CONVERT(date, na.horaInicial)) = 2 then 'Martes' else"
                                      + " case when DATEPART(DW, CONVERT(date, na.horaInicial)) = 3 then 'Miercoles' else case when DATEPART(DW, CONVERT(date, na.horaInicial)) = 4 then 'Jueves' else"
                                      + " case when DATEPART(DW, CONVERT(date, na.horaInicial)) = 5 then 'Viernes' else case when DATEPART(DW, CONVERT(date, na.horaInicial)) = 6 then 'Sabado' end end end end end end Dia,"
                                      + " COUNT(codigoLote) LotesAuditados, ISNULL(case when na.estadoAudit = 1 then COUNT(ae.codigoLote) end,0) LoteAprobados, ISNULL(case when na.estadoAudit = 0 then ISNULL(COUNT(ae.codigoLote),0) end,0) LoteRechazado,"
                                      + " SUM(ISNULL(ae.muestra, 0)) PiezasAuditadas, SUM(ISNULL(ae.muestra, 0)) - SUM(ISNULL(na.UnidadesDefect, 0)) PiezasAceptadas, SUM(ISNULL(na.UnidadesDefect, 0)) PiezasRechazadas"
                                      + " from tbAuditEmbarque ae join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join POrder p on p.Id_Order = ae.idcorte join Linea l on l.id_linea = p.Id_Linea2"
                                      + " where p.Id_Linea2 = " + DropDownListLine.SelectedValue + " and DATEPART(WEEK, CONVERT(date, na.horaInicial)) = " + drpSemana.SelectedValue + " and na.secuencia = 1 group by l.id_linea, l.numero, na.estadoAudit, DATEPART(DW, CONVERT(date, na.horaInicial))) a group by a.id_linea, a.numero, a.Dia, a.D order by a.D";

                                grafico = DataAccess.Get_DataTable(query);
                                Session["Grafico1"] = grafico;

                                g1.DataSource = grafico;
                                g1.DataBind();

                                g2.DataSource = grafico;
                                g2.DataBind();
                            }

                            break;
                        //14.54 ip actual
                        case "7"://Planta - Semana
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                panelgrafico2.Visible = true;

                                query = " select a.id_planta, a.descripcion, a.D, a.Dia, (CAST((CAST(SUM(ISNULL(LoteAprobados, 0)) as decimal(7, 3)) / CAST(SUM(ISNULL(LotesAuditados, 0)) as decimal(7, 3))) * 100 as decimal(6, 3))) AQL,"
                                      + " (CAST((CAST(SUM(ISNULL(PiezasRechazadas, 0)) as decimal(7, 3)) / CAST(SUM(ISNULL(PiezasAuditadas, 0)) as decimal(7, 3))) * 100 as decimal(6, 3))) OQL from ("
                                      + " select pl.id_planta, pl.descripcion, DATEPART(DW, CONVERT(date, na.horaInicial)) D,"
                                      + " case when DATEPART(DW, CONVERT(date, na.horaInicial)) = 1 then 'Lunes' else case when DATEPART(DW, CONVERT(date, na.horaInicial)) = 2 then 'Martes' else"
                                      + " case when DATEPART(DW, CONVERT(date, na.horaInicial)) = 3 then 'Miercoles' else case when DATEPART(DW, CONVERT(date, na.horaInicial)) = 4 then 'Jueves' else"
                                      + " case when DATEPART(DW, CONVERT(date, na.horaInicial)) = 5 then 'Viernes' else case when DATEPART(DW, CONVERT(date, na.horaInicial)) = 6 then 'Sabado' end end end end end end Dia,"
                                      + " COUNT(codigoLote) LotesAuditados, ISNULL(case when na.estadoAudit = 1 then COUNT(ae.codigoLote) end,0) LoteAprobados, ISNULL(case when na.estadoAudit = 0 then ISNULL(COUNT(ae.codigoLote),0) end,0) LoteRechazado,"
                                      + " SUM(ISNULL(ae.muestra, 0)) PiezasAuditadas, SUM(ISNULL(ae.muestra, 0)) - SUM(ISNULL(na.UnidadesDefect, 0)) PiezasAceptadas, SUM(ISNULL(na.UnidadesDefect, 0)) PiezasRechazadas from tbAuditEmbarque ae"
                                      + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join POrder p on p.Id_Order = ae.idcorte join Linea l on l.id_linea = p.Id_Linea2 join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.id_planta = " + DropDownListPlant.SelectedValue + " and DATEPART(WEEK, CONVERT(date, na.horaInicial)) = " + drpSemana.SelectedValue + " and na.secuencia = 1 group by pl.id_planta, pl.descripcion, na.estadoAudit, DATEPART(DW, CONVERT(date, na.horaInicial))) a"
                                      + " group by a.id_planta, a.descripcion, a.Dia, a.D order by a.D";

                                grafico2 = DataAccess.Get_DataTable(query);
                                Session["Grafico2"] = grafico2;

                                g3.DataSource = grafico2;
                                g3.DataBind();

                                g4.DataSource = grafico2;
                                g4.DataBind();
                            }

                            break;

                        case "8":
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                lbl1.Text = "% AQL - % OQL, Global - Planta, Embarque";
                                panelGrafico3.Visible = true;

                                int semana1 = Convert.ToInt16(drpSemana.SelectedValue) - 3;
                                int semana2 = Convert.ToInt16(drpSemana.SelectedValue);

                                query = " select a.id_planta, a.descripcion, 'Semana: ' + Convert(varchar, a.Semana) Semana,"
                                      + " (CAST((CAST(SUM(ISNULL(LoteAprobados, 0)) as decimal(7, 3)) / CAST(SUM(ISNULL(LotesAuditados, 0)) as decimal(7, 3))) * 100 as decimal(6, 3))) AQL,"
                                      + " (CAST((CAST(SUM(ISNULL(PiezasRechazadas, 0)) as decimal(7, 3)) / CAST(SUM(ISNULL(PiezasAuditadas, 0)) as decimal(7, 3))) * 100 as decimal(6, 3))) OQL"
                                      + " from (select pl.id_planta, pl.descripcion, DATEPART(WEEK, CONVERT(date, na.horaInicial)) Semana,"
                                      + " COUNT(codigoLote) LotesAuditados, ISNULL(case when na.estadoAudit = 1 then COUNT(ae.codigoLote) end,0) LoteAprobados,"
                                      + " ISNULL(case when na.estadoAudit = 0 then ISNULL(COUNT(ae.codigoLote),0) end,0) LoteRechazado, SUM(ISNULL(ae.muestra, 0)) PiezasAuditadas,"
                                      + " SUM(ISNULL(ae.muestra, 0)) - SUM(ISNULL(na.UnidadesDefect, 0)) PiezasAceptadas, SUM(ISNULL(na.UnidadesDefect, 0)) PiezasRechazadas"
                                      + " from tbAuditEmbarque ae join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join POrder p on p.Id_Order = ae.idcorte "
                                      + " join Linea l on l.id_linea = p.Id_Linea2 join Planta pl on pl.id_planta = l.id_planta"
                                      + " where pl.id_planta = " + DropDownListPlant.SelectedValue + " and DATEPART(WEEK, CONVERT(date, na.horaInicial)) between " + semana1 + " and " + semana2 + " and na.secuencia = 1 group by pl.id_planta, pl.descripcion,"
                                      + " na.estadoAudit, DATEPART(WEEK, CONVERT(date, na.horaInicial))"
                                      + " ) a group by a.id_planta, a.descripcion, a.Semana order by a.Semana";

                                grafico3 = DataAccess.Get_DataTable(query);
                                Session["grf3"] = grafico3;

                                grfAql.DataSource = grafico3;
                                grfAql.DataBind();

                                grfOql.DataSource = grafico3;
                                grfOql.DataBind();
                            }
                            break;

                        case "9":
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                lbl1.Text = "% AQL - % OQL, Global - Linea, Embarque";

                                panelGrafico3.Visible = true;

                                int semana1 = Convert.ToInt16(drpSemana.SelectedValue) - 3;
                                int semana2 = Convert.ToInt16(drpSemana.SelectedValue);

                                query = " select a.id_linea, a.numero Linea, 'Semana: ' + Convert(varchar, a.Semana) Semana,"
                                      + " (CAST((CAST(SUM(ISNULL(LoteAprobados, 0)) as decimal(7, 3)) / CAST(SUM(ISNULL(LotesAuditados, 0)) as decimal(7, 3))) * 100 as decimal(6, 3))) AQL,"
                                      + " (CAST((CAST(SUM(ISNULL(PiezasRechazadas, 0)) as decimal(7, 3)) / CAST(SUM(ISNULL(PiezasAuditadas, 0)) as decimal(7, 3))) * 100 as decimal(6, 3))) OQL"
                                      + " from (select l.id_linea, l.numero, DATEPART(WEEK, CONVERT(date, na.horaInicial)) Semana,"
                                      + " COUNT(codigoLote) LotesAuditados, ISNULL(case when na.estadoAudit = 1 then COUNT(ae.codigoLote) end,0) LoteAprobados,"
                                      + " ISNULL(case when na.estadoAudit = 0 then ISNULL(COUNT(ae.codigoLote),0) end,0) LoteRechazado, SUM(ISNULL(ae.muestra, 0)) PiezasAuditadas,"
                                      + " SUM(ISNULL(ae.muestra, 0)) - SUM(ISNULL(na.UnidadesDefect, 0)) PiezasAceptadas, SUM(ISNULL(na.UnidadesDefect, 0)) PiezasRechazadas"
                                      + " from tbAuditEmbarque ae join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join POrder p on p.Id_Order = ae.idcorte join Linea l on l.id_linea = p.Id_Linea2"
                                      + " where l.id_linea = " + DropDownListLine.SelectedValue + " and DATEPART(WEEK, CONVERT(date, na.horaInicial)) between " + semana1 + " and " + semana2 + " and na.secuencia = 1"
                                      + " group by l.id_linea, l.numero, na.estadoAudit, DATEPART(WEEK, CONVERT(date, na.horaInicial))) a group by a.id_linea, a.numero, a.Semana order by a.Semana";

                                grafico3 = DataAccess.Get_DataTable(query);
                                Session["grf3"] = grafico3;

                                grfAql.DataSource = grafico3;
                                grfAql.DataBind();

                                grfOql.DataSource = grafico3;
                                grfOql.DataBind();
                            }
                            break;
                    }
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/AqlOqlCorte.aspx");
    }

    protected void grd1_DataBinding(object sender, EventArgs e)
    {
        grd1.DataSource = Session["CorteRep"];
    }

    protected void grd1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grd1.DataBind();
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            string op1 = rdaudit.SelectedValue;

            string opcionfi = rd1.SelectedValue;

            switch (op1)
            {
                case "1":
                    break;

                case "2":
                    break;

                case "3"://Bulto a Bulto
                    switch (opcionfi)
                    {
                        case "1"://Corte
                            if (hdnidporder.Value != "")
                            {
                                export.GridViewID = "grd1";
                                export2.GridViewID = "AQl";
                                export3.GridViewID = "OQL";

                                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                                link3.Component = export;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                                link1.Component = export2;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                                link2.Component = export3;

                                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                                compositeLink.Links.AddRange(new object[] { link1, link2, link3 });

                                compositeLink.CreatePageForEachLink();

                                using (MemoryStream stream = new MemoryStream())
                                {
                                    XlsxExportOptions options = new XlsxExportOptions();
                                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                    options.SheetName = "OQL/AQL-Corte";
                                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                    Response.Clear();
                                    Response.Buffer = false;
                                    Response.AppendHeader("Content-Type", "application/xlsx");
                                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                    Response.AppendHeader("Content-Disposition", "attachment; filename=OQL-AQL-Corte" + ".xlsx");
                                    Response.BinaryWrite(stream.ToArray());
                                    Response.End();
                                }
                                ps.Dispose();
                            }

                            break;

                        case "2":
                            break;

                        case "3"://Linea
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                export.GridViewID = "gridlinea";
                                export2.GridViewID = "aqllinea";
                                export3.GridViewID = "oqllinea";

                                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                                link3.Component = export;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                                link1.Component = export2;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                                link2.Component = export3;

                                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                                compositeLink.Links.AddRange(new object[] { link1, link2, link3 });

                                compositeLink.CreatePageForEachLink();

                                using (MemoryStream stream = new MemoryStream())
                                {
                                    XlsxExportOptions options = new XlsxExportOptions();
                                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                    options.SheetName = "OQL/AQL-Linea";
                                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                    Response.Clear();
                                    Response.Buffer = false;
                                    Response.AppendHeader("Content-Type", "application/xlsx");
                                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                    Response.AppendHeader("Content-Disposition", "attachment; filename=OQL-AQL-Linea" + ".xlsx");
                                    Response.BinaryWrite(stream.ToArray());
                                    Response.End();
                                }
                                ps.Dispose();
                            }
                            break;

                        case "4":
                            break;

                        case "5":
                            break;

                        case "6"://Linea  - Semana
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                g1.DataSource = Session["Grafico1"];
                                g1.DataBind();
                                link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)g1).Chart;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                g2.DataSource = Session["Grafico1"];
                                g2.DataBind();
                                link3.Component = ((DevExpress.XtraCharts.Native.IChartContainer)g2).Chart;

                                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                                compositeLink.Links.AddRange(new object[] { link2, link3 });

                                compositeLink.CreatePageForEachLink();

                                using (MemoryStream stream = new MemoryStream())
                                {
                                    XlsxExportOptions options = new XlsxExportOptions();
                                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                    options.SheetName = "AQL-OQL-LineWeek";
                                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                    Response.Clear();
                                    Response.Buffer = false;
                                    Response.AppendHeader("Content-Type", "application/xlsx");
                                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                    Response.AppendHeader("Content-Disposition", "attachment; filename=AQL-OQL-LineWeek" + ".xlsx");
                                    Response.BinaryWrite(stream.ToArray());
                                    Response.End();
                                }
                                ps.Dispose();
                            }
                            break;

                        case "7"://Planta - Semana
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                g3.DataSource = Session["Grafico2"];
                                g3.DataBind();
                                link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)g3).Chart;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                g4.DataSource = Session["Grafico2"];
                                g4.DataBind();
                                link3.Component = ((DevExpress.XtraCharts.Native.IChartContainer)g4).Chart;

                                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                                compositeLink.Links.AddRange(new object[] { link2, link3 });

                                compositeLink.CreatePageForEachLink();

                                using (MemoryStream stream = new MemoryStream())
                                {
                                    XlsxExportOptions options = new XlsxExportOptions();
                                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                    options.SheetName = "AQL-OQL-PlantWeek";
                                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                    Response.Clear();
                                    Response.Buffer = false;
                                    Response.AppendHeader("Content-Type", "application/xlsx");
                                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                    Response.AppendHeader("Content-Disposition", "attachment; filename=AQL-OQL-PlantWeek" + ".xlsx");
                                    Response.BinaryWrite(stream.ToArray());
                                    Response.End();
                                }
                                ps.Dispose();
                            }
                            break;

                        case "8"://Global Planta
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                grfAql.DataSource = Session["grf3"];
                                grfAql.DataBind();
                                link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)grfAql).Chart;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                grfOql.DataSource = Session["grf3"];
                                grfOql.DataBind();
                                link3.Component = ((DevExpress.XtraCharts.Native.IChartContainer)grfOql).Chart;

                                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                                compositeLink.Links.AddRange(new object[] { link2, link3 });

                                compositeLink.CreatePageForEachLink();

                                using (MemoryStream stream = new MemoryStream())
                                {
                                    XlsxExportOptions options = new XlsxExportOptions();
                                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                    options.SheetName = "AQL-OQL-PlantGlobal";
                                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                    Response.Clear();
                                    Response.Buffer = false;
                                    Response.AppendHeader("Content-Type", "application/xlsx");
                                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                    Response.AppendHeader("Content-Disposition", "attachment; filename=AQL-OQL-PlantGlobal" + ".xlsx");
                                    Response.BinaryWrite(stream.ToArray());
                                    Response.End();
                                }
                                ps.Dispose();
                            }
                            break;

                        case "9"://Global Line
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                grfAql.DataSource = Session["grf3"];
                                grfAql.DataBind();
                                link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)grfAql).Chart;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                grfOql.DataSource = Session["grf3"];
                                grfOql.DataBind();
                                link3.Component = ((DevExpress.XtraCharts.Native.IChartContainer)grfOql).Chart;

                                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                                compositeLink.Links.AddRange(new object[] { link2, link3 });

                                compositeLink.CreatePageForEachLink();

                                using (MemoryStream stream = new MemoryStream())
                                {
                                    XlsxExportOptions options = new XlsxExportOptions();
                                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                    options.SheetName = "AQL-OQL-LineGlobal";
                                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                    Response.Clear();
                                    Response.Buffer = false;
                                    Response.AppendHeader("Content-Type", "application/xlsx");
                                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                    Response.AppendHeader("Content-Disposition", "attachment; filename=AQL-OQL-LineGlobal" + ".xlsx");
                                    Response.BinaryWrite(stream.ToArray());
                                    Response.End();
                                }
                                ps.Dispose();
                            }
                            break;

                        default: break;
                    }

                    break;

                case "4":
                    break;

                case "5"://Embarque
                    switch (opcionfi)
                    {
                        case "1"://Corte
                            if (hdnidporder.Value != "")
                            {
                                export.GridViewID = "grd1";
                                export2.GridViewID = "AQl";
                                export3.GridViewID = "OQL";

                                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                                link3.Component = export;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                                link1.Component = export2;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                                link2.Component = export3;

                                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                                compositeLink.Links.AddRange(new object[] { link1, link2, link3 });

                                compositeLink.CreatePageForEachLink();

                                using (MemoryStream stream = new MemoryStream())
                                {
                                    XlsxExportOptions options = new XlsxExportOptions();
                                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                    options.SheetName = "OQL/AQL-Corte";
                                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                    Response.Clear();
                                    Response.Buffer = false;
                                    Response.AppendHeader("Content-Type", "application/xlsx");
                                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                    Response.AppendHeader("Content-Disposition", "attachment; filename=OQL-AQL-Corte" + ".xlsx");
                                    Response.BinaryWrite(stream.ToArray());
                                    Response.End();
                                }
                                ps.Dispose();
                            }

                            break;

                        case "2":
                            break;

                        case "3"://Linea
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                export.GridViewID = "grdlinemb";
                                export2.GridViewID = "aqlemb";
                                export3.GridViewID = "oqlemb";

                                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                                link3.Component = export;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                                link1.Component = export2;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                                link2.Component = export3;

                                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                                compositeLink.Links.AddRange(new object[] { link1, link2, link3 });

                                compositeLink.CreatePageForEachLink();

                                using (MemoryStream stream = new MemoryStream())
                                {
                                    XlsxExportOptions options = new XlsxExportOptions();
                                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                    options.SheetName = "OQL/AQL-Linea";
                                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                    Response.Clear();
                                    Response.Buffer = false;
                                    Response.AppendHeader("Content-Type", "application/xlsx");
                                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                    Response.AppendHeader("Content-Disposition", "attachment; filename=OQL-AQL-Linea" + ".xlsx");
                                    Response.BinaryWrite(stream.ToArray());
                                    Response.End();
                                }
                                ps.Dispose();
                            }
                            break;

                        case "4":
                            break;

                        case "5":
                            break;

                        case "6"://Linea  - Semana
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                g1.DataSource = Session["Grafico1"];
                                g1.DataBind();
                                link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)g1).Chart;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                g2.DataSource = Session["Grafico1"];
                                g2.DataBind();
                                link3.Component = ((DevExpress.XtraCharts.Native.IChartContainer)g2).Chart;

                                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                                compositeLink.Links.AddRange(new object[] { link2, link3 });

                                compositeLink.CreatePageForEachLink();

                                using (MemoryStream stream = new MemoryStream())
                                {
                                    XlsxExportOptions options = new XlsxExportOptions();
                                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                    options.SheetName = "AQL-OQL-LineWeek";
                                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                    Response.Clear();
                                    Response.Buffer = false;
                                    Response.AppendHeader("Content-Type", "application/xlsx");
                                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                    Response.AppendHeader("Content-Disposition", "attachment; filename=AQL-OQL-LineWeek" + ".xlsx");
                                    Response.BinaryWrite(stream.ToArray());
                                    Response.End();
                                }
                                ps.Dispose();
                            }
                            break;

                        case "7"://Planta - Semana
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                g3.DataSource = Session["Grafico2"];
                                g3.DataBind();
                                link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)g3).Chart;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                g4.DataSource = Session["Grafico2"];
                                g4.DataBind();
                                link3.Component = ((DevExpress.XtraCharts.Native.IChartContainer)g4).Chart;

                                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                                compositeLink.Links.AddRange(new object[] { link2, link3 });

                                compositeLink.CreatePageForEachLink();

                                using (MemoryStream stream = new MemoryStream())
                                {
                                    XlsxExportOptions options = new XlsxExportOptions();
                                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                    options.SheetName = "AQL-OQL-PlantWeek";
                                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                    Response.Clear();
                                    Response.Buffer = false;
                                    Response.AppendHeader("Content-Type", "application/xlsx");
                                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                    Response.AppendHeader("Content-Disposition", "attachment; filename=AQL-OQL-PlantWeek" + ".xlsx");
                                    Response.BinaryWrite(stream.ToArray());
                                    Response.End();
                                }
                                ps.Dispose();
                            }
                            break;

                        case "8"://Global Planta
                            if (DropDownListPlant.SelectedItem.Text != "Select...")
                            {
                                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                grfAql.DataSource = Session["grf3"];
                                grfAql.DataBind();
                                link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)grfAql).Chart;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                grfOql.DataSource = Session["grf3"];
                                grfOql.DataBind();
                                link3.Component = ((DevExpress.XtraCharts.Native.IChartContainer)grfOql).Chart;

                                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                                compositeLink.Links.AddRange(new object[] { link2, link3 });

                                compositeLink.CreatePageForEachLink();

                                using (MemoryStream stream = new MemoryStream())
                                {
                                    XlsxExportOptions options = new XlsxExportOptions();
                                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                    options.SheetName = "AQL-OQL-PlantGlobal";
                                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                    Response.Clear();
                                    Response.Buffer = false;
                                    Response.AppendHeader("Content-Type", "application/xlsx");
                                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                    Response.AppendHeader("Content-Disposition", "attachment; filename=AQL-OQL-PlantGlobal" + ".xlsx");
                                    Response.BinaryWrite(stream.ToArray());
                                    Response.End();
                                }
                                ps.Dispose();
                            }
                            break;

                        case "9"://Global Line
                            if (DropDownListLine.SelectedItem.Text != "Select...")
                            {
                                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                grfAql.DataSource = Session["grf3"];
                                grfAql.DataBind();
                                link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)grfAql).Chart;

                                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

                                grfOql.DataSource = Session["grf3"];
                                grfOql.DataBind();
                                link3.Component = ((DevExpress.XtraCharts.Native.IChartContainer)grfOql).Chart;

                                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                                compositeLink.Links.AddRange(new object[] { link2, link3 });

                                compositeLink.CreatePageForEachLink();

                                using (MemoryStream stream = new MemoryStream())
                                {
                                    XlsxExportOptions options = new XlsxExportOptions();
                                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                    options.SheetName = "AQL-OQL-LineGlobal";
                                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                                    Response.Clear();
                                    Response.Buffer = false;
                                    Response.AppendHeader("Content-Type", "application/xlsx");
                                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                                    Response.AppendHeader("Content-Disposition", "attachment; filename=AQL-OQL-LineGlobal" + ".xlsx");
                                    Response.BinaryWrite(stream.ToArray());
                                    Response.End();
                                }
                                ps.Dispose();
                            }
                            break;

                        default: break;
                    }
                    break;

                default: break;
            }

        }
        catch (Exception ex)
        {
            string a = ex.Message;
        }
    }

    protected void AQl_DataBinding(object sender, EventArgs e)
    {
        AQl.DataSource = Session["CorteRep1"];
    }

    protected void AQl_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        AQl.DataBind();
    }

    protected void OQL_DataBinding(object sender, EventArgs e)
    {
        OQL.DataSource = Session["CorteRep2"];
    }

    protected void OQL_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        OQL.DataBind();
    }

    protected void gridlinea_DataBinding(object sender, EventArgs e)
    {
        gridlinea.DataSource = Session["CorteRep"];
    }

    protected void gridlinea_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridlinea.DataBind();
    }

    protected void aqllinea_DataBinding(object sender, EventArgs e)
    {
        aqllinea.DataSource = Session["CorteRep1"];
    }

    protected void aqllinea_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        aqllinea.DataBind();
    }

    protected void oqllinea_DataBinding(object sender, EventArgs e)
    {
        oqllinea.DataSource = Session["CorteRep2"];
    }

    protected void oqllinea_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        oqllinea.DataBind();
    }

    protected void g1_DataBinding(object sender, EventArgs e)
    {
        g1.DataSource = Session["Grafico1"];
    }

    protected void g1_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    {
        g1.DataBind();
    }

    protected void g2_DataBinding(object sender, EventArgs e)
    {
        g2.DataSource = Session["Grafico1"];
    }

    protected void g2_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    {
        g2.DataBind();
    }

    protected void g3_DataBinding(object sender, EventArgs e)
    {
        g3.DataSource = Session["Grafico2"];
    }

    protected void g3_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    {
        g3.DataBind();
    }

    protected void g4_DataBinding(object sender, EventArgs e)
    {
        g4.DataSource = Session["Grafico2"];
    }

    protected void g4_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    {
        g4.DataBind();
    }

    protected void grdlinemb_DataBinding(object sender, EventArgs e)
    {
        grdlinemb.DataSource = Session["CorteRep"];
    }

    protected void grdlinemb_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdlinemb.DataBind();
    }

    protected void aqlemb_DataBinding(object sender, EventArgs e)
    {
        aqlemb.DataSource = Session["CorteRep1"];
    }

    protected void aqlemb_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        aqlemb.DataBind();
    }

    protected void oqlemb_DataBinding(object sender, EventArgs e)
    {
        oqlemb.DataSource = Session["CorteRep2"];
    }

    protected void oqlemb_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        oqlemb.DataBind();
    }

    protected void grfAql_DataBinding(object sender, EventArgs e)
    {
        grfAql.DataSource = Session["grf3"];
    }

    protected void grfAql_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    {
        grfAql.DataBind();
    }

    protected void grfOql_DataBinding(object sender, EventArgs e)
    {
        grfOql.DataSource = Session["grf3"];
    }

    protected void grfOql_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    {
        grfOql.DataBind();
    }
}