﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Ejemplo.aspx.cs" Inherits="adminRepDefectos_Ejemplo" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function actualizar() {
            location.reload(true);
        }
        //Función para actualizar cada 6 segundos(4000 milisegundos)
        setInterval("actualizar()", 10000);
    </script>

    <script type="text/javascript">
        function date1() {
            $('#<%=txtfecha.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

    </script>

    <style>
        .m {
            margin-top: 15px;
        }

        .st {
            font-size: x-large;
            font-weight: bold;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <br />
    <div class="container">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary" style="margin-top: 5px">
                <div class="panel-heading">Produccion por Bihorario</div>
                <div class="panel-body">

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        Linea
                    <asp:DropDownList ID="drplinea" runat="server" CssClass="form-control">
                        <asp:ListItem Value="0">Select...</asp:ListItem>
                        <asp:ListItem Value="1">Linea 1</asp:ListItem>
                        <asp:ListItem Value="2">Linea 2</asp:ListItem>
                        <asp:ListItem Value="3">Linea 3</asp:ListItem>
                        <asp:ListItem Value="4">Linea 4</asp:ListItem>
                    </asp:DropDownList>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        Fecha
                    <dx1:ASPxDateEdit runat="server" ID="txtfecha" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        Reporte
                    <asp:Button ID="btnreporte" runat="server" Text="Generar Reporte" CssClass="btn btn-primary form-control" OnClick="btnreporte_Click" />
                        <asp:Button ID="btnlim" runat="server" Text="Limpiar Pantalla" CssClass="btn btn-info form-control m" OnClick="btnlim_Click" />
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center">
                        <hr />
                        <asp:Label ID="lbllin" runat="server" Text="" CssClass="st"></asp:Label>
                        <hr />
                        <dx:WebChartControl ID="g1" runat="server" Height="400px" Width="700px" OnCustomCallback="g1_CustomCallback" OnDataBinding="g1_DataBinding"
                            CrosshairEnabled="False" ToolTipEnabled="True" SeriesDataMember="bihorario" RenderFormat="Svg">
                            <SeriesTemplate ArgumentDataMember="descOperacion" ValueDataMembersSerializable="Total" LabelsVisibility="True" CrosshairLabelPattern="{V:0}">
                                <ViewSerializable>
                                    <dx:StackedBarSeriesView></dx:StackedBarSeriesView>
                                </ViewSerializable>
                                <LabelSerializable>
                                    <dx:StackedBarSeriesLabel Font="Tahoma, 8pt, style=Bold" TextPattern="{V:0}">
                                    </dx:StackedBarSeriesLabel>
                                </LabelSerializable>
                            </SeriesTemplate>
                            <Legend Direction="BottomToTop"></Legend>
                            <BorderOptions Visibility="False" />
                            <Titles>
                                <dx:ChartTitle Text="Operaciones por Bihorario"></dx:ChartTitle>
                                <dx:ChartTitle Dock="Bottom" Alignment="Far" Text="From Rocedes" Font="Tahoma, 8pt" TextColor="Gray"></dx:ChartTitle>
                            </Titles>
                            <DiagramSerializable>
                                <dx:XYDiagram Rotated="True">
                                    <AxisX VisibleInPanesSerializable="-1">
                                    </AxisX>
                                    <AxisY Title-Text="Unidades" Title-Visibility="True" VisibleInPanesSerializable="-1">
                                        <Label TextPattern="{V:0}" />
                                        <GridLines MinorVisible="True"></GridLines>
                                    </AxisY>
                                    <DefaultPane>
                                        <%--<stackedbartotallabel textpattern="Total {V:0}" visible="True"></stackedbartotallabel>--%>
                                    </DefaultPane>
                                </dx:XYDiagram>
                            </DiagramSerializable>
                        </dx:WebChartControl>
                        <hr />
                    </div>

                    <%--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center">

                        <dx:WebChartControl ID="w1" runat="server" Height="400px" Width="700px" RenderFormat="Svg" ToolTipEnabled="False" OnDataBinding="w1_DataBinding" OnCustomCallback="w1_CustomCallback1" CrosshairEnabled="True" SeriesDataMember="bihorario">
                            <SeriesTemplate ArgumentDataMember="descOperacion" ValueDataMembersSerializable="Total" CrosshairLabelPattern="{S}: {V:0}" ArgumentScaleType="Qualitative">
                                <ViewSerializable>
                                    <dx:SideBySideBarSeriesView />
                                </ViewSerializable>
                            </SeriesTemplate>
                            <Legend Direction="LeftToRight" AlignmentHorizontal="Center" AlignmentVertical="BottomOutside" />
                            <BorderOptions Visibility="False" />
                            <Titles>
                                <dx:ChartTitle Text="Bihorario" />
                            </Titles>
                            <DiagramSerializable>
                                <dx:XYDiagram>
                                    <AxisX VisibleInPanesSerializable="-1" />
                                    <AxisY Title-Text="Operaciones por Bihorario" Title-Visibility="True" VisibleInPanesSerializable="-1" Interlaced="True" />
                                </dx:XYDiagram>
                            </DiagramSerializable>
                        </dx:WebChartControl>
                        <hr />
                    </div>--%>

                    <%--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center">
                        <dx:WebChartControl ID="w2" runat="server" Height="400px" Width="700px" RenderFormat="Svg" OnCustomDrawSeriesPoint="w2_CustomDrawSeriesPoint" OnCustomCallback="w2_CustomCallback"
                            AlternateText="DevAV Sales Mix by Region chart." ToolTipEnabled="True" SeriesDataMember="descOperacion" OnDataBinding="w2_DataBinding">
                            <SeriesTemplate ArgumentDataMember="bihorario" ValueDataMembersSerializable="Total" LegendTextPattern="{A:0}"
                                LabelsVisibility="False">
                                <ViewSerializable>
                                    <dx:PieSeriesView RuntimeExploding="False">
                                        <Titles>
                                            <dx:SeriesTitle></dx:SeriesTitle>
                                        </Titles>
                                    </dx:PieSeriesView>
                                </ViewSerializable>
                                <LabelSerializable>
                                    <dx:PieSeriesLabel Font="Tahoma, 8.25pt" TextPattern="{V:F1}">
                                    </dx:PieSeriesLabel>
                                </LabelSerializable>
                            </SeriesTemplate>
                            <Legend AlignmentHorizontal="Center" AlignmentVertical="BottomOutside" Direction="LeftToRight" MaxHorizontalPercentage="70"></Legend>
                            <BorderOptions Visibility="False" />
                            <Titles>
                                <dx:ChartTitle Text="DevAV Sales Mix by Region"></dx:ChartTitle>
                            </Titles>
                            <DiagramSerializable>
                                <dx:SimpleDiagram></dx:SimpleDiagram>
                            </DiagramSerializable>
                        </dx:WebChartControl>
                        <hr />  
                    </div>--%>



                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <dx1:ASPxGridView ID="grdoper" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdoper_DataBinding" OnCustomCallback="grdoper_CustomCallback">
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Operaciones Criticas por Linea" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="descOperacion" Caption="Operación"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="09:00am" Caption="09:00am"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="11:00am" Caption="11:00am"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="01:00pm" Caption="01:00pm"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="03:00pm" Caption="03:00pm"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="04:50pm" Caption="04:50pm"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="06:50pm" Caption="06:50pm"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Total" Caption="Total Por Operacion"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:GridViewBandColumn>
                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="false" />
                            <Settings ShowFooter="true" HorizontalScrollBarMode="Hidden" />
                            <SettingsPager PageSize="100" />
                            <TotalSummary>
                                <dx1:ASPxSummaryItem FieldName="descOperacion" SummaryType="Count" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="09:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="11:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="01:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="03:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="04:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="06:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="Total" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <GroupSummary>
                            </GroupSummary>
                        </dx1:ASPxGridView>
                        <hr />
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <dx1:ASPxGridView ID="grdproduccion" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdproduccion_DataBinding" OnCustomCallback="grdproduccion_CustomCallback">
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Operaciones Generales por Linea" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="descOperacion" Caption="Operación"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="09:00am" Caption="09:00am"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="11:00am" Caption="11:00am"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="01:00pm" Caption="01:00pm"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="03:00pm" Caption="03:00pm"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="04:50pm" Caption="04:50pm"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="06:50pm" Caption="06:50pm"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Total" Caption="Total Por Operacion"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:GridViewBandColumn>
                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="false" />
                            <Settings ShowFooter="true" HorizontalScrollBarMode="Hidden" />
                            <SettingsPager PageSize="15" />
                            <TotalSummary>
                                <dx1:ASPxSummaryItem FieldName="descOperacion" SummaryType="Count" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="09:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="11:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="01:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="03:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="04:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="06:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="Total" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <GroupSummary>
                            </GroupSummary>
                        </dx1:ASPxGridView>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <hr />
                        <dx1:ASPxGridView ID="gridoper" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="gridoper_DataBinding" OnCustomCallback="gridoper_CustomCallback">
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Operacion - Operario Generales por Linea" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="Linea" Caption="Linea"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="descOperacion" Caption="Operación"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Operario" Caption="Operario"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="09:00am" Caption="09:00am"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="11:00am" Caption="11:00am"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="01:00pm" Caption="01:00pm"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="03:00pm" Caption="03:00pm"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="04:50pm" Caption="04:50pm"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="06:50pm" Caption="06:50pm"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Total" Caption="Total Por Operacion"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:GridViewBandColumn>
                            </Columns>
                            <Settings ShowFooter="true" ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                            <SettingsPager PageSize="20" />
                            <TotalSummary>
                                <dx1:ASPxSummaryItem FieldName="descOperacion" SummaryType="Count" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="Operario" SummaryType="Count" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="09:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="11:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="01:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="03:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="04:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="06:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="Total" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <GroupSummary>
                                <dx1:ASPxSummaryItem FieldName="Operario" ShowInGroupFooterColumn="Operario" SummaryType="Count" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="09:00am" ShowInGroupFooterColumn="09:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="11:00am" ShowInGroupFooterColumn="11:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="01:00pm" ShowInGroupFooterColumn="01:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="03:00pm" ShowInGroupFooterColumn="03:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="04:50pm" ShowInGroupFooterColumn="04:50am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="06:50pm" ShowInGroupFooterColumn="06:50am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx1:ASPxSummaryItem FieldName="Total" ShowInGroupFooterColumn="Total" SummaryType="Sum" DisplayFormat="{0}" />
                            </GroupSummary>
                        </dx1:ASPxGridView>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

