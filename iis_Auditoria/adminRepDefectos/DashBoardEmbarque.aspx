﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DashBoardEmbarque.aspx.cs" Inherits="adminRepDefectos_DashBoardEmbarque" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "DashBoardEmbarque.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porderclient: item.porderclient,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porderclient);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porderclient);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porderclient + "</a>").appendTo(ul);
            };
        });
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <br />

    <div class="container">
        <div class="col-lg-12">

            <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

            <div class="panel panel-primary">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <div class="col-lg-6">
                        Corte
                        <asp:TextBox ID="txtcorte" runat="server" CssClass="form-control" placeholder="Digite el corte"></asp:TextBox>
                    </div>
                    <div class="col-lg-6">
                        <br />
                        <div class="col-lg-6">
                            <asp:Button ID="btnbuscar" runat="server" Text="Buscar" CssClass="btn btn-primary form-control" OnClick="btnbuscar_Click" />
                        </div>
                        <div class="col-lg-6" style="margin:0;padding:0">
                            <asp:Button ID="btnlim" runat="server" Text="Limpiar" CssClass="btn btn-success form-control" OnClick="btnlim_Click" />
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <hr />
                        <dx1:ASPxGridView ID="grdr1" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdr1_DataBinding" OnCustomCallback="grdr1_CustomCallback">                            
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Auditoria" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="POrderClient" Caption="Corte"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Style" Caption="Estilo"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Color" Caption="Color"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Quantity" Caption="Total Unidades"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Linea" Caption="Linea"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="UnidadesAceptadas" Caption="Unidades Aceptadas"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Pendientes" Caption="Pendientes"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Defectos" Caption="Total Defectos"></dx1:GridViewDataTextColumn>                                        
                                    </Columns>
                                </dx1:GridViewBandColumn>
                            </Columns>
                        </dx1:ASPxGridView>
                        <dx1:ASPxGridViewExporter ID="export1" runat="server"></dx1:ASPxGridViewExporter>

                        <hr />

                        <dx1:ASPxGridView ID="grdr2" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdr2_DataBinding" OnCustomCallback="grdr2_CustomCallback">
                            <SettingsSearchPanel Visible="true" />
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Defectos" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="POrderClient" Caption="Corte" Settings-AllowCellMerge="True" CellStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Top" CellStyle-Font-Bold="true"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="unidad" Caption="Unidades"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Defecto" Caption="Defectos Encontrados"></dx1:GridViewDataTextColumn>                                        
                                    </Columns>
                                </dx1:GridViewBandColumn>
                            </Columns>
                            <Settings VerticalScrollBarMode="Visible" ShowGroupFooter="VisibleAlways" ShowFooter="true" />
                            <SettingsPager PageSize="200"></SettingsPager>
                            <TotalSummary>                                
                                <dx1:ASPxSummaryItem FieldName="unidad" SummaryType="Sum" DisplayFormat="Total Defectos: {0}" />
                                <dx1:ASPxSummaryItem FieldName="Defecto" SummaryType="Count" DisplayFormat="Defectos Encontrados : {0}" />                               
                            </TotalSummary>
                        </dx1:ASPxGridView>
                        <dx1:ASPxGridViewExporter ID="export2" runat="server"></dx1:ASPxGridViewExporter>
                        <hr />
                        <asp:Button ID="btnexportar" runat="server" CssClass="btn btn-success form-control" Text="Exportar Información" OnClick="btnexportar_Click" />
                    </div>
                </div>
            </div>

        </div>
    </div>

</asp:Content>

