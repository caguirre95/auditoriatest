﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AuditoriaEmbDef.aspx.cs" Inherits="adminRepDefectos_AuditoriaEmbDef" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hdnidporder.ClientID%>').val(ui.item.idorder);
                    // $('#<=txtstyle.ClientID%>').val(ui.item.style);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        })
    </script>

    <script lang="javascript" type="text/javascript">
        function autoestilo() {

            $(function () {
                $('#<%=txtstyle.ClientID%>').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "AqlOql.aspx/GetStyle",
                            data: "{'pref' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        estilo: item.estilo,
                                        id: item.id,
                                        json: item
                                    }
                                }))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                        return false;
                    },
                    select: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);
                        $('#<%=hdnidstyle.ClientID%>').val(ui.item.id);

                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li  >")
                        .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
                };
            })
        }
    </script>

    <script type="text/javascript">
        function date1() {
            $('#<%=txtfecha1.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

        function date2() {
            $('#<%=txtfecha2.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

    </script>

    <style>
        .m {
            margin-top: 25px;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <br />
    <div class="container">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

            <div class="panel panel-primary" style="margin-top: 5px">
                <div class="panel-heading">Embarque</div>
                <div class="panel-body">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <asp:Panel ID="panelemba" runat="server">
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="panel-default">
                                    <div class="panel-heading">Filtros</div>
                                    <div class="panel-body">

                                        <asp:RadioButtonList ID="rd1" runat="server">
                                            <asp:ListItem Value="1">Cut</asp:ListItem>
                                            <%-- <asp:ListItem Value="2">Style</asp:ListItem>--%>
                                            <asp:ListItem Value="3">Line</asp:ListItem>
                                            <asp:ListItem Value="4">Plant</asp:ListItem>
                                            <asp:ListItem Value="5">Deficit Auditado - Planta</asp:ListItem>
                                        </asp:RadioButtonList>

                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="panel-default">
                                    <div class="panel-heading">Opciones</div>
                                    <div class="panel-body">
                                        <asp:Panel runat="server" ID="panelCorte">
                                            Cut:                                   
                                    <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdnidporder" />

                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="panelEstilo" Visible="false">
                                            Style:                                   
                                    <asp:TextBox ID="txtstyle" onkeypress="autoestilo()" AutoCompleteType="Disabled" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdnidstyle" />
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="panelLinea">
                                            Line:
                                     <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="panelPlanta">
                                            Plant:
                                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div class="panel-default">
                                    <div class="panel-heading">Select </div>
                                    <div class="panel-body">
                                        <asp:Panel runat="server" ID="panelfechas">
                                            Start Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                            End Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>

                                        </asp:Panel>
                                        <div style="padding-top: 15px">
                                            <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" runat="server" OnClick="btngenerar_Click" />
                                        </div>
                                        <div style="padding-top: 15px">
                                            <asp:Button Text="Limpiar Campos" CssClass="btn btn-info form-control" ID="btnlimpiar" runat="server" OnClick="btnlimpiar_Click" />
                                        </div>
                                        <div style="padding-top: 15px">
                                            <asp:Button Text="Exportar a Excel" CssClass="btn btn-success form-control" ID="btnexp" runat="server" OnClick="btnexp_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <%--                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="panel-default">
                                <div class="panel-heading">Auditorias</div>
                                <div class="panel-body">
                                    <asp:RadioButtonList ID="rdaudit" runat="server">--%>
                        <%--<asp:ListItem Value="1">Auditoria Interna</asp:ListItem>
                                        <asp:ListItem Value="2">Auditoria Externa</asp:ListItem>--%>
                        <%--<asp:ListItem Value="3">Auditoria Bulto a Bulto</asp:ListItem>--%>
                        <%--<asp:ListItem Value="4">Auditoria Inspeccion - Proceso</asp:ListItem>--%>
                        <%-- <asp:ListItem Value="5">Auditoria Embarque</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>--%>


                        <%--<asp:Panel ID="panelc" runat="server" Visible="true">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <asp:Image ID="img1" runat="server" ImageUrl="~/img/const.jpg" Width="700px" Height="500px" />
                            </div>
                        </asp:Panel>--%>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <hr />
                            <asp:Panel ID="panel1" runat="server" Visible="false">
                                <dx1:ASPxGridView ID="grdembarque" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdembarque_DataBinding" OnCustomCallback="grdembarque_CustomCallback">
                                    <Columns>
                                        <dx1:GridViewBandColumn Caption="Auditoria de Embarque" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx1:GridViewDataTextColumn FieldName="Planta" Caption="Planta" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Auditor" Caption="Auditor" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Corte" Caption="Corte"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Quantity" Caption="Cantidad del Corte"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Color" Caption="Color"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Linea" Caption="Linea"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="codigoLote" Caption="Codigo del Lote"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Aud" Caption="AUD"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Estado" Caption="Estado" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="lote" Caption="Cantidad del Lote"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="muestra" Caption="Unidades Auditadas"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Codigo" Caption="COD del Defecto"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Defecto" Caption="Defecto"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="unidad" Caption="Cantidad de Defectos"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Operacion" Caption="Operacion"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Observacion" Caption="Observaciones"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha" PropertiesTextEdit-DisplayFormatString="dd-MM-yyyy"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="HoraInicial" Caption="Hora Inicio" PropertiesTextEdit-DisplayFormatString="hh:mm"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="HoraFinal" Caption="Hora Final" PropertiesTextEdit-DisplayFormatString="hh:mm"></dx1:GridViewDataTextColumn>
                                            </Columns>
                                        </dx1:GridViewBandColumn>
                                    </Columns>
                                    <Settings ShowFooter="true" HorizontalScrollBarMode="Visible" VerticalScrollBarMode="Visible" />
                                    <SettingsPager PageSize="10"></SettingsPager>
                                    <TotalSummary>
                                        <dx1:ASPxSummaryItem FieldName="unidad" SummaryType="Sum" DisplayFormat="Total Defecto:{0}" />
                                    </TotalSummary>
                                </dx1:ASPxGridView>
                                <dx1:ASPxGridViewExporter ID="export" runat="server"></dx1:ASPxGridViewExporter>
                            </asp:Panel>

                            <hr />

                            <asp:Panel ID="panel2" runat="server" Visible="false">
                                <dx1:ASPxGridView ID="griddeficit" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="griddeficit_DataBinding" OnCustomCallback="griddeficit_CustomCallback">
                                    <SettingsSearchPanel Visible="true" />
                                    <Columns>
                                        <dx1:GridViewBandColumn Caption="Deficit Auditoria de Embarque" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx1:GridViewDataTextColumn FieldName="POrder" Caption="Corte" Settings-AllowCellMerge="True" BatchEditModifiedCellStyle-HorizontalAlign="Center"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Quantity" Caption="Cantidad del Corte"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="AuditadoFechas" Caption="Auditado Entre Fechas"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="TotalAuditado" Caption="Total Auditado del Corte"></dx1:GridViewDataTextColumn>
                                                <dx1:GridViewDataTextColumn FieldName="Deficit" Caption="Deficit"></dx1:GridViewDataTextColumn>
                                            </Columns>
                                        </dx1:GridViewBandColumn>
                                    </Columns>
                                    <Settings VerticalScrollBarMode="Visible" ShowGroupFooter="VisibleAlways" ShowFooter="true" />
                                    <SettingsPager PageSize="200"></SettingsPager>
                                    <TotalSummary>
                                        <dx1:ASPxSummaryItem FieldName="POrder" SummaryType="Count" DisplayFormat="{0}" />
                                        <dx1:ASPxSummaryItem FieldName="Quantity" SummaryType="Sum" DisplayFormat="{0}" />
                                        <dx1:ASPxSummaryItem FieldName="AuditadoFechas" SummaryType="Sum" DisplayFormat="{0}" />
                                        <dx1:ASPxSummaryItem FieldName="TotalAuditado" SummaryType="Sum" DisplayFormat="{0}" />
                                    </TotalSummary>
                                </dx1:ASPxGridView>
                                <dx1:ASPxGridViewExporter ID="export1" runat="server"></dx1:ASPxGridViewExporter>
                            </asp:Panel>

                            <br />
                            <asp:Button Text="Exportar Deficit a Excel" CssClass="btn btn-success form-control" ID="btn1" runat="server" OnClick="btn1_Click" />
                        </div>

                    </div>

                </div>
            </div>



        </div>
    </div>

</asp:Content>

