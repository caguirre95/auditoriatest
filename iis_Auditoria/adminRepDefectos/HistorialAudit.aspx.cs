﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_HistorialAudit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                cargacombo();
                txtfecha1.Text = DateTime.Now.ToShortDateString();
                txtfecha2.Text = DateTime.Now.ToShortDateString();

                Session["aprobado"] = null;
                Session["rechazado"] = null;
                Session["pendiente"] = null;
                Session["deficitbb"] = null;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    void cargacombo()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta, descripcion FROM Planta order by descripcion asc";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/HistorialAudit.aspx");
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            var audit = new DataTable();
            var auditR = new DataTable();
            var auditP = new DataTable();
            var def = new DataTable();
            string query = "";
            string queryR = "";
            string queryP = "";
            int a = 0;

            string opcionfi = rd1.SelectedValue;

            switch (opcionfi)
            {
                case "1"://Corte
                    if (txtPorder.Text != "" && hdnidporder.Value != "")
                    {
                        query = " select p.Id_Order, p.POrder Corte, s.Style Estilo, l.numero Linea, b.NSeq Bulto, CONVERT(date, bba.FechaInspeccion) FechaInspeccion, RIGHT(bba.FechaInspeccion, 7) HoraInspeccion, i.nombre Inspector"
                              + " from tbBultoaBultoAprovados bba join Bundle b on b.Id_Bundle = bba.IdBulto join POrder p on p.Id_Order = b.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style"
                              + " join Inspector i on i.id_inspector = bba.idInspector where p.Id_Order = " + hdnidporder.Value + " UNION"
                              + " select p.Id_Order, p.POrder Corte, s.Style Estilo, l.numero Linea, b.NSeq Bulto, CONVERT(date, br.FechaInspeccion) FechaInspeccion, RIGHT(br.FechaInspeccion, 7) HoraInspeccion, i.nombre Inspector"
                              + " from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on brd.idBBRechazado = br.idBBRechazado join Bundle b on b.Id_Bundle = br.idBulto join POrder p on p.Id_Order = b.Id_Order"
                              + " join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Inspector i on i.id_inspector = br.idInspector where p.Id_Order = " + hdnidporder.Value + " and br.Estado = 0"
                              + " group by p.Id_Order, p.POrder, s.Style, l.numero, b.NSeq, CONVERT(date, br.FechaInspeccion), RIGHT(br.FechaInspeccion, 7), i.nombre, br.Estado, brd.NumAuditoria order by b.NSeq";

                        audit = DataAccess.Get_DataTable(query);

                        queryR = " select p.Id_Order, p.POrder Corte, s.Style Estilo, l.numero Linea, b.NSeq Bulto, CONVERT(date, br.FechaInspeccion) FechaInspeccion, RIGHT(br.FechaInspeccion, 7) HoraInspeccion, i.nombre Inspector,"
                               + " br.Estado, brd.NumAuditoria from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on brd.idBBRechazado = br.idBBRechazado join Bundle b on b.Id_Bundle = br.idBulto"
                               + " join POrder p on p.Id_Order = b.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Inspector i on i.id_inspector = br.idInspector where p.Id_Order = " + hdnidporder.Value + " and br.Estado = 1"
                               + " group by p.Id_Order, p.POrder, s.Style, l.numero, b.NSeq, CONVERT(date, br.FechaInspeccion), RIGHT(br.FechaInspeccion, 7), i.nombre, br.Estado, brd.NumAuditoria order by b.NSeq";

                        auditR = DataAccess.Get_DataTable(queryR);

                        queryP = " select l.numero, s.Style, p.POrder, b.Bld from Bundle b join POrder p on p.Id_Order = b.Id_Order join Style s on s.Id_Style = p.Id_Style join Linea l on l.id_linea = p.Id_Linea2"
                               + " where Id_Bundle not in (select b.Id_Bundle from tbBultoaBultoAprovados bba join Bundle b on b.Id_Bundle = bba.IdBulto join POrder p on p.Id_Order = b.Id_Order where p.Id_Order = " + hdnidporder.Value + ""
                               + " group by b.Id_Bundle union select b.Id_Bundle from tbBultoaBultoRechazado bbr join Bundle b on b.Id_Bundle = bbr.IdBulto join POrder p on p.Id_Order = b.Id_Order where p.Id_Order = " + hdnidporder.Value + ""
                               + " group by b.Id_Bundle) and p.Id_Order = " + hdnidporder.Value + " order by l.numero asc, p.POrder asc, b.Bld asc";

                        auditP = DataAccess.Get_DataTable(queryP);

                        a = 1;
                    }
                    break;

                case "2"://Estilo
                    if (txtstyle.Text != "" && hdnidstyle.Value != "")
                    {
                        query = " select p.Id_Order, p.POrder Corte, s.Style Estilo, l.numero Linea, b.NSeq Bulto, CONVERT(date, bba.FechaInspeccion) FechaInspeccion, RIGHT(bba.FechaInspeccion, 7) HoraInspeccion, i.nombre Inspector"
                              + " from tbBultoaBultoAprovados bba join Bundle b on b.Id_Bundle = bba.IdBulto join POrder p on p.Id_Order = b.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style"
                              + " join Inspector i on i.id_inspector = bba.idInspector where s.Style = '" + txtstyle.Text.Trim() + "' and CONVERT(date, bba.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' UNION"
                              + " select p.Id_Order, p.POrder Corte, s.Style Estilo, l.numero Linea, b.NSeq Bulto, CONVERT(date, br.FechaInspeccion) FechaInspeccion, RIGHT(br.FechaInspeccion, 7) HoraInspeccion, i.nombre Inspector"
                              + " from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on brd.idBBRechazado = br.idBBRechazado join Bundle b on b.Id_Bundle = br.idBulto join POrder p on p.Id_Order = b.Id_Order"
                              + " join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Inspector i on i.id_inspector = br.idInspector"
                              + " where s.Style = '" + txtstyle.Text.Trim() + "' and br.Estado = 0 and CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                              + " group by p.Id_Order, p.POrder, s.Style, l.numero, b.NSeq, CONVERT(date, br.FechaInspeccion), RIGHT(br.FechaInspeccion, 7), i.nombre, br.Estado, brd.NumAuditoria order by b.NSeq";

                        audit = DataAccess.Get_DataTable(query);

                        queryR = " select p.Id_Order, p.POrder Corte, s.Style Estilo, l.numero Linea, b.NSeq Bulto, CONVERT(date, br.FechaInspeccion) FechaInspeccion, RIGHT(br.FechaInspeccion, 7) HoraInspeccion, i.nombre Inspector,"
                               + " br.Estado, brd.NumAuditoria from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on brd.idBBRechazado = br.idBBRechazado join Bundle b on b.Id_Bundle = br.idBulto"
                               + " join POrder p on p.Id_Order = b.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Inspector i on i.id_inspector = br.idInspector"
                               + " where s.Style = '" + txtstyle.Text.Trim() + "' and br.Estado = 1 and CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                               + " group by p.Id_Order, p.POrder, s.Style, l.numero, b.NSeq, CONVERT(date, br.FechaInspeccion), RIGHT(br.FechaInspeccion, 7), i.nombre, br.Estado, brd.NumAuditoria order by b.NSeq";

                        auditR = DataAccess.Get_DataTable(queryR);

                        queryP = " select l.numero, s.Style, p.POrder, b.Bld from Bundle b join POrder p on p.Id_Order = b.Id_Order join Style s on s.Id_Style = p.Id_Style join Linea l on l.id_linea = p.Id_Linea2"
                               + " where s.Style = '" + txtstyle.Text.Trim() + "' and b.Id_Order in (select p.Id_Order from tbBultoaBultoAprovados ba join Bundle b on b.Id_Bundle = ba.IdBulto join POrder p on p.Id_Order = b.Id_Order"
                               + " where CONVERT(date, ba.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Order union"
                               + " select p.Id_Order from tbBultoaBultoRechazado br join Bundle b on b.Id_Bundle = br.IdBulto join POrder p on p.Id_Order = b.Id_Order"
                               + " where CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Order ) and Id_Bundle not in ("
                               + " select idBulto from tbBultoaBultoAprovados ba union select idBulto from tbBultoaBultoRechazado br) order by l.numero asc, p.POrder asc, b.NSeq asc";

                        auditP = DataAccess.Get_DataTable(queryP);

                        a = 1;
                    }
                    break;

                case "3"://Linea
                    if (DropDownListLine.SelectedItem.Text != "Select...")
                    {
                        query = " select p.Id_Order, p.POrder Corte, s.Style Estilo, l.numero Linea, b.NSeq Bulto, CONVERT(date, bba.FechaInspeccion) FechaInspeccion, RIGHT(bba.FechaInspeccion, 7) HoraInspeccion, i.nombre Inspector"
                              + " from tbBultoaBultoAprovados bba join Bundle b on b.Id_Bundle = bba.IdBulto join POrder p on p.Id_Order = b.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style"
                              + " join Inspector i on i.id_inspector = bba.idInspector where l.id_linea = '" + DropDownListLine.SelectedValue + "' and CONVERT(date, bba.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' UNION"
                              + " select p.Id_Order, p.POrder Corte, s.Style Estilo, l.numero Linea, b.NSeq Bulto, CONVERT(date, br.FechaInspeccion) FechaInspeccion, RIGHT(br.FechaInspeccion, 7) HoraInspeccion, i.nombre Inspector"
                              + " from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on brd.idBBRechazado = br.idBBRechazado join Bundle b on b.Id_Bundle = br.idBulto join POrder p on p.Id_Order = b.Id_Order"
                              + " join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Inspector i on i.id_inspector = br.idInspector"
                              + " where l.id_linea = '" + DropDownListLine.SelectedValue + "' and br.Estado = 0 and CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                              + " group by p.Id_Order, p.POrder, s.Style, l.numero, b.NSeq, CONVERT(date, br.FechaInspeccion), RIGHT(br.FechaInspeccion, 7), i.nombre, br.Estado, brd.NumAuditoria order by p.POrder asc, NSeq asc";

                        audit = DataAccess.Get_DataTable(query);

                        queryR = " select p.Id_Order, p.POrder Corte, s.Style Estilo, l.numero Linea, b.NSeq Bulto, CONVERT(date, br.FechaInspeccion) FechaInspeccion, RIGHT(br.FechaInspeccion, 7) HoraInspeccion, i.nombre Inspector,"
                               + " br.Estado, brd.NumAuditoria from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on brd.idBBRechazado = br.idBBRechazado join Bundle b on b.Id_Bundle = br.idBulto"
                               + " join POrder p on p.Id_Order = b.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Inspector i on i.id_inspector = br.idInspector"
                               + " where l.id_linea = '" + DropDownListLine.SelectedValue + "' and br.Estado = 1 and CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                               + " group by p.Id_Order, p.POrder, s.Style, l.numero, b.NSeq, CONVERT(date, br.FechaInspeccion), RIGHT(br.FechaInspeccion, 7), i.nombre, br.Estado, brd.NumAuditoria order by p.POrder asc, NSeq asc";

                        auditR = DataAccess.Get_DataTable(queryR);

                        queryP = " select l.numero, s.Style, p.POrder, b.Bld from Bundle b join POrder p on p.Id_Order = b.Id_Order join Style s on s.Id_Style = p.Id_Style join Linea l on l.id_linea = p.Id_Linea2"
                               + " where l.id_linea = '" + DropDownListLine.SelectedValue + "' and b.Id_Order in (select p.Id_Order from tbBultoaBultoAprovados ba join Bundle b on b.Id_Bundle = ba.IdBulto join POrder p on p.Id_Order = b.Id_Order"
                               + " where CONVERT(date, ba.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Order union"
                               + " select p.Id_Order from tbBultoaBultoRechazado br join Bundle b on b.Id_Bundle = br.IdBulto join POrder p on p.Id_Order = b.Id_Order"
                               + " where CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Order ) and Id_Bundle not in ("
                               + " select idBulto from tbBultoaBultoAprovados ba union select idBulto from tbBultoaBultoRechazado br) order by l.numero asc, p.POrder asc, b.NSeq asc";

                        auditP = DataAccess.Get_DataTable(queryP);

                        a = 1;
                    }
                    break;

                case "4"://Planta
                    if (DropDownListPlant.SelectedItem.Text != "Select...")
                    {
                        query = " select p.Id_Order, p.POrder Corte, s.Style Estilo, l.numero Linea, b.NSeq Bulto, CONVERT(date, bba.FechaInspeccion) FechaInspeccion, RIGHT(bba.FechaInspeccion, 7) HoraInspeccion, i.nombre Inspector"
                              + " from tbBultoaBultoAprovados bba join Bundle b on b.Id_Bundle = bba.IdBulto join POrder p on p.Id_Order = b.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style"
                              + " join Inspector i on i.id_inspector = bba.idInspector where l.id_planta = '" + DropDownListPlant.SelectedValue + "' and CONVERT(date, bba.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' UNION"
                              + " select p.Id_Order, p.POrder Corte, s.Style Estilo, l.numero Linea, b.NSeq Bulto, CONVERT(date, br.FechaInspeccion) FechaInspeccion, RIGHT(br.FechaInspeccion, 7) HoraInspeccion, i.nombre Inspector"
                              + " from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on brd.idBBRechazado = br.idBBRechazado join Bundle b on b.Id_Bundle = br.idBulto join POrder p on p.Id_Order = b.Id_Order"
                              + " join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Inspector i on i.id_inspector = br.idInspector"
                              + " where l.id_planta = '" + DropDownListPlant.SelectedValue + "' and br.Estado = 0 and CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                              + " group by p.Id_Order, p.POrder, s.Style, l.numero, b.NSeq, CONVERT(date, br.FechaInspeccion), RIGHT(br.FechaInspeccion, 7), i.nombre, br.Estado, brd.NumAuditoria order by l.numero asc, p.POrder asc, NSeq asc";

                        audit = DataAccess.Get_DataTable(query);

                        queryR = " select p.Id_Order, p.POrder Corte, s.Style Estilo, l.numero Linea, b.NSeq Bulto, CONVERT(date, br.FechaInspeccion) FechaInspeccion, RIGHT(br.FechaInspeccion, 7) HoraInspeccion, i.nombre Inspector,"
                               + " br.Estado, brd.NumAuditoria from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet brd on brd.idBBRechazado = br.idBBRechazado join Bundle b on b.Id_Bundle = br.idBulto"
                               + " join POrder p on p.Id_Order = b.Id_Order join Linea l on l.id_linea = p.Id_Linea2 join Style s on s.Id_Style = p.Id_Style join Inspector i on i.id_inspector = br.idInspector"
                               + " where l.id_planta = '" + DropDownListPlant.SelectedValue + "' and br.Estado = 1 and CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "'"
                               + " group by p.Id_Order, p.POrder, s.Style, l.numero, b.NSeq, CONVERT(date, br.FechaInspeccion), RIGHT(br.FechaInspeccion, 7), i.nombre, br.Estado, brd.NumAuditoria order by l.numero asc, p.POrder asc, NSeq asc";

                        auditR = DataAccess.Get_DataTable(queryR);

                        queryP = " select l.numero, s.Style, p.POrder, b.Bld from Bundle b join POrder p on p.Id_Order = b.Id_Order join Style s on s.Id_Style = p.Id_Style join Linea l on l.id_linea = p.Id_Linea2"
                               + " where l.id_planta = '" + DropDownListPlant.SelectedValue + "' and b.Id_Order in (select p.Id_Order from tbBultoaBultoAprovados ba join Bundle b on b.Id_Bundle = ba.IdBulto join POrder p on p.Id_Order = b.Id_Order"
                               + " where CONVERT(date, ba.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Order union"
                               + " select p.Id_Order from tbBultoaBultoRechazado br join Bundle b on b.Id_Bundle = br.IdBulto join POrder p on p.Id_Order = b.Id_Order"
                               + " where CONVERT(date, br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by p.Id_Order ) and Id_Bundle not in ("
                               + " select idBulto from tbBultoaBultoAprovados ba union select idBulto from tbBultoaBultoRechazado br) order by l.numero asc, p.POrder asc, b.NSeq asc";

                        auditP = DataAccess.Get_DataTable(queryP);

                        a = 1;
                    }
                    break;

                case "5"://Deficit bulto - Planta
                    if (DropDownListPlant.SelectedItem.Text != "Select...")
                    {
                        query = " select a.Id_Order, a.POrder, a.Quantity, ISNULL(sum(a.UnidadesAuditadas), 0) UnidadesAuditadas, (a.Quantity - ISNULL(SUM(a.UnidadesAuditadas), 0)) Deficit from"
                              + " (select p.Id_Order, p.POrder, p.Quantity, SUM(b.Quantity) UnidadesAuditadas from tbBultoaBultoAprovados bba join Bundle b on b.Id_Bundle = bba.IdBulto"
                              + " join POrder p on p.Id_Order = b.Id_Order where p.Id_Planta = " + DropDownListPlant.SelectedValue + " group by p.Id_Order, p.POrder, p.Quantity union"
                              + " select p.Id_Order, p.POrder, p.Quantity, SUM(b.Quantity) UnidadesAuditadas from tbBultoaBultoRechazado bbr join Bundle b on b.Id_Bundle = bbr.IdBulto"
                              + " join POrder p on p.Id_Order = b.Id_Order where p.Id_Planta = " + DropDownListPlant.SelectedValue + " group by p.Id_Order, p.POrder, p.Quantity) a"
                              + " group by a.Id_Order, a.POrder, a.Quantity order by a.Id_Order";

                        def = DataAccess.Get_DataTable(query);

                        a = 2;
                    }
                    break;

                default:
                    break;
            }

            if (a == 1)
            {
                Session["aprobado"] = audit;
                Session["rechazado"] = auditR;
                Session["pendiente"] = auditP;
                grdaprobado.DataBind();
                grdrechazado.DataBind();
                gridpending.DataBind();
            }
            else if (a == 2)
            {
                Session["deficitbb"] = def;
                griddef.DataBind();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }


    protected void grdaprobado_DataBinding(object sender, EventArgs e)
    {
        grdaprobado.DataSource = Session["aprobado"];
    }

    protected void grdaprobado_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdaprobado.DataBind();
    }

    protected void grdrechazado_DataBinding(object sender, EventArgs e)
    {
        grdrechazado.DataSource = Session["rechazado"];
    }

    protected void grdrechazado_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdrechazado.DataBind();
    }

    protected void gridpending_DataBinding(object sender, EventArgs e)
    {
        gridpending.DataSource = Session["pendiente"];
    }

    protected void gridpending_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridpending.DataBind();
    }

    protected void griddef_DataBinding(object sender, EventArgs e)
    {
        griddef.DataSource = Session["deficitbb"];
    }

    protected void griddef_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        griddef.DataBind();
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        try
        {


            string opcionfi = rd1.SelectedValue;

            switch (opcionfi)
            {
                case "1":
                case "2":
                case "3":
                case "4":
                    DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                    export.GridViewID = "grdaprobado";
                    export2.GridViewID = "grdrechazado";
                    export4.GridViewID = "gridpending";

                    DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                    link1.Component = export;

                    DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                    link2.Component = export2;

                    DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link4 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                    link4.Component = export4;

                    DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                    compositeLink.Links.AddRange(new object[] { link1, link2, link4 });

                    compositeLink.CreatePageForEachLink();

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XlsxExportOptions options = new XlsxExportOptions();
                        options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        options.SheetName = "Historial";
                        compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/xlsx");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", "attachment; filename=Historial" + ".xlsx");
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }
                    ps.Dispose();

                    break;

                case "5":
                    DevExpress.XtraPrinting.PrintingSystemBase ps1 = new DevExpress.XtraPrinting.PrintingSystemBase();

                    export3.GridViewID = "griddef";

                    DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps1);
                    link3.Component = export3;

                    DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps1);
                    compositeLink1.Links.AddRange(new object[] { link3 });

                    compositeLink1.CreatePageForEachLink();

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XlsxExportOptions options = new XlsxExportOptions();
                        options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        options.SheetName = "Historial";
                        compositeLink1.PrintingSystemBase.ExportToXlsx(stream, options);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/xlsx");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", "attachment; filename=Historial" + ".xlsx");
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }
                    ps1.Dispose();

                    break;

                default:
                    break;

            }

        }
        catch (Exception)
        {

            throw;
        }
    }
}