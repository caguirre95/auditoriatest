﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HistoricoDefCorte.aspx.cs" Inherits="adminRepDefectos_HistoricoDefCorte" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val('');
              
                    var cont = 0;

                    $("#cortes li").each(function () {
                         if ($(this).text() == ui.item.porder.trim()) 
                      
                         cont++;
                    });

                    if (cont == 0) {
                        $("#cortes").append("<li onclick='sel(this)'>" + ui.item.porder + "<i class='glyphicon glyphicon-trash' style='margin-left:5px' ></i></li>");

                        var cadenapricipal="";
                        var cadena = document.getElementById("<%= HiddenFieldCortesId.ClientID %>").value;

                        myObj = { 'idorder':  ui.item.idorder , 'porder': ui.item.porder };
                        myJSON = JSON.stringify(myObj);
                     
                        if (cadena.length == 0) {
                            cadenapricipal = myJSON;                       
                        }
                        else {
                            cadenapricipal= cadena + "," + myJSON ;                          
                        }

                        $('#<%=HiddenFieldCortesId.ClientID%>').val(cadenapricipal);
                     
                    }
                    
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

    <style>
        .a {
            margin-top: 5px;
        }
    </style>

    <script>

        function sel(e) {
            var element = e.innerText;
                   
            e.parentNode.removeChild(e);

            var cadena ="[" + document.getElementById("<%= HiddenFieldCortesId.ClientID %>").value +"]";
            var arr = JSON.parse(cadena);

            var pos= arr.indexOf(arr.find(x => x.porder == element));

            arr.splice(pos, 1);
          
            var cadenapricipal = JSON.stringify(arr);
            console.log(cadenapricipal);

            ult = cadenapricipal.length;
         
            cadenapricipal = cadenapricipal.substr(1,ult-2);
       

            $('#<%=HiddenFieldCortesId.ClientID%>').val(cadenapricipal);
        }

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Defectos Embarque</strong></div>
            <div class="panel-body">

                <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                     <asp:HiddenField runat="server" ID="HiddenFieldCortesId" />
                    <strong>Corte Agregados</strong>
                    <div >
                      <ul id="cortes">

                      </ul>
                    </div>
                    <div class="input-group">
                        <span class="input-group-btn">
                            <asp:LinkButton Text="" OnClick="LinkButton1_Click" CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                        </span>
                        <div class="ui-widget" style="text-align: left;">
                            <asp:TextBox ID="txtPorder" placeholder="Codigo de Corte"  class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hfidPorder" />
                        </div>
                    </div>
                    <strong>Planta</strong>
                    <asp:DropDownList ID="drpplanta" runat="server" CssClass="form-control" Width="200px">
                        <asp:ListItem Value="0">Select...</asp:ListItem>
                        <asp:ListItem Value="1">Planta 1</asp:ListItem>
                        <asp:ListItem Value="2">Planta 3</asp:ListItem>
                        <asp:ListItem Value="3">Planta 6</asp:ListItem>
                        <asp:ListItem Value="4">Planta 7</asp:ListItem>
                    </asp:DropDownList>

                    <asp:DropDownList ID="drpSemana" CssClass="form-control" Visible="false" runat="server" Width="200px" />
                    <br />
                    <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control" Text="Generar" Width="200px" OnClick="btngenerar_Click" />

                    <asp:Button ID="btnExport" OnClick="btnExport_Click" CssClass="btn btn-success form-control a" runat="server" Text="Export Excel" Width="200px" />
                    <hr />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow-x: auto">
                    <dx:ASPxGridView ID="gridpack" runat="server" KeyFieldName="Id_Order" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnDataBinding="gridpack_DataBinding" OnCustomCallback="gridpack_CustomCallback">
                        <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="Id_Order" Visible="false" Caption="id" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Planta" Caption="Planta" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Linea" Caption="Linea" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Corte" Caption="Corte" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>                            
                            <dx:GridViewDataTextColumn FieldName="Cantidad" Caption="Cantidad Del Corte" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>                            
                        </Columns>
                        <Templates>
                            <DetailRow>
                                <div style="padding: 3px 3px 2px 3px">
                                    <dx:ASPxPageControl runat="server" ID="pageControl" Width="100%" EnableCallBacks="true">
                                        <TabPages>

                                            <dx:TabPage Text="Lote" Visible="true">
                                                <ContentCollection>
                                                    <dx:ContentControl runat="server">
                                                        <dx:ASPxGridView ID="grdlote" runat="server" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnBeforePerformDataSelect="grdlote_BeforePerformDataSelect" KeyFieldName="Id_Order" 
                                                            Width="100%">
                                                            <Columns>
                                                                <dx:GridViewDataColumn FieldName="Id_Order" Caption="Id" Visible="false" />
                                                                <dx:GridViewDataColumn FieldName="Codigo" Caption="Lote" />
                                                                <dx:GridViewDataColumn FieldName="Lote" Caption="Cantidad del Lote" />
                                                                <dx:GridViewDataColumn FieldName="Muestra" Caption="Muestra del Lote" />
                                                                <dx:GridViewDataColumn FieldName="Auditor" Caption="Auditor" />
                                                                <dx:GridViewDataColumn FieldName="Observacion" Caption="Observaciones" />                                               
                                                                <dx:GridViewDataColumn FieldName="Auditoria" Caption="Auditoria" />                                                                                                                              
                                                            </Columns>
                                                            <SettingsPager EnableAdaptivity="true" />
                                                            <Settings ShowFooter="true" />                                                           
                                                        </dx:ASPxGridView>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>

                                            <dx:TabPage Text="Defecto" Visible="true">
                                                <ContentCollection>
                                                    <dx:ContentControl runat="server">
                                                        <dx:ASPxGridView ID="grddefecto" runat="server" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnBeforePerformDataSelect="grddefecto_BeforePerformDataSelect"
                                                            KeyFieldName="Id_Order" Width="100%">
                                                            <Columns>
                                                                <%--<dx:GridViewDataColumn FieldName="Id_Order" Caption="Id" Visible="false" /> 
                                                                <dx:GridViewDataColumn FieldName="Lote" Caption="Lote" Settings-AllowCellMerge="True" />
                                                                <dx:GridViewDataColumn FieldName="UnidadesDefect" Caption="Unidades con Defectos" Settings-AllowCellMerge="True" Visible="false" />
                                                                <dx:GridViewDataColumn FieldName="Unidades" Caption="Numero de Defectos" Settings-AllowCellMerge="False" />--%>
                                                                <dx:GridViewDataColumn FieldName="Defecto" Caption="Defecto" Settings-AllowCellMerge="False" />
                                                                <dx:GridViewDataColumn FieldName="Posicion" Caption="Posicion" Settings-AllowCellMerge="False" />  
                                                                 <dx:GridViewDataTextColumn FieldName="ruta">
                                                                     <DataItemTemplate>
                                                                        <dx:ASPxImage ID="ASPxImage1" runat="server" ShowLoadingImage="true" ImageUrl='<%# GetImageUrl(Container.Text )%>' ></dx:ASPxImage>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <SettingsPager EnableAdaptivity="true" />
                                                            <Settings ShowFooter="true" />
                                                            <TotalSummary>                                                                
                                                                <dx:ASPxSummaryItem FieldName="Unidades" SummaryType="Sum" DisplayFormat="{0}" />
                                                            </TotalSummary>
                                                        </dx:ASPxGridView>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>

                                            <dx:TabPage Text="Defecto Imagen" Visible="false">
                                                <ContentCollection>
                                                    <dx:ContentControl runat="server">
                                                        <dx:ASPxGridView ID="grddfimg" runat="server" Theme="Metropolis" SettingsBehavior-AllowSort="false" KeyFieldName="Id_Order" OnBeforePerformDataSelect="grddfimg_BeforePerformDataSelect"
                                                            Width="100%">
                                                            <Columns>
                                                                <dx:GridViewDataColumn FieldName="Id_Order" Caption="Id" Settings-AllowCellMerge="True" Visible="false" />                                                                
                                                                <dx:GridViewDataColumn FieldName="Defecto" Caption="Defecto" Settings-AllowCellMerge="False" />
                                                                <dx:GridViewDataColumn FieldName="Posicion" Caption="Posicion" Settings-AllowCellMerge="False" />                                                                
                                                                <%--Imagen--%>                                                               
                                                            </Columns>
                                                            <SettingsBehavior AllowCellMerge="true" />
                                                            <SettingsPager EnableAdaptivity="true" />
                                                            <Settings ShowFooter="true"  />                                                            
                                                        </dx:ASPxGridView>
                                                    </dx:ContentControl>
                                                </ContentCollection>
                                            </dx:TabPage>

                                        </TabPages>
                                    </dx:ASPxPageControl>
                                </div>
                            </DetailRow>
                        </Templates>
                        <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                        <Settings ShowFooter="true" />
                        <SettingsPager PageSize="100" />
                        <SettingsBehavior EnableCustomizationWindow="true" />
                        <SettingsDetail ShowDetailRow="true" />
                    </dx:ASPxGridView>
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                </div>
            </div>

        </div>

    </div>

</asp:Content>

