﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_OQLAQLxLinea : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                txtfecha1.Text = DateTime.Now.ToShortDateString();
                txtfecha2.Text = DateTime.Now.ToShortDateString();
                lblplan.Text = "";
                lblprocess.Text = "";
                cargaL();
                Session["extra1"] = null;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    void cargaL()
    {
        string linea = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";
        DataTable dt_linea = DataAccess.Get_DataTable(linea);
        drplinea.DataSource = dt_linea;
        drplinea.DataTextField = "numero";
        drplinea.DataValueField = "id_linea";
        drplinea.DataBind();
        drplinea.Items.Insert(0, "Select...");
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            var aqloql = new DataTable();
            string query = "";
            string c = rd1.SelectedValue;
            int n = 0;
            switch (c)
            {
                case "1":
                    if (drplinea.SelectedItem.Text != "Select...")
                    {
                        query = " select 'Line: ' + Linea as Linea, quality, porcentaje from ( select a.Linea, CAST((CAST(a.Total as decimal (16,3)) / (CAST(a.Total as decimal (16,3)) + CAST(b.Total as decimal (16,3)))) * 100 as decimal (16,3)) as AQL,"
                              + " cast(((((CAST(a.Total as decimal(16,3)) + cast(b.Total as decimal(16,3))) - cast(b.TotalDef as decimal(16,3))) / CAST(b.rechazadas as decimal(16,3))) * 100) as decimal(16,3)) as OQL"
                              + " from ( select l.numero as Linea, COUNT(idBBAprovado)as Total,sum(UnidadesAuditadas)as aceptadas from tbAuditInterAprovados bb join Linea l on l.id_linea = bb.idLinea"
                              + " where idLinea= " + drplinea.SelectedValue + " and convert(date,FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by l.numero )as a, ( select COUNT(c.idBBRechazado)as Total,sum(c.Muestreo)as rechazadas, COUNT(c.idDefectos) as TotalDef"
                              + " from ( select br.idBBRechazado,Muestreo, dt.idDefectos from tbAuditInterRechazado br join tbAuditInterRechazadoDet dt on br.idBBRechazado=dt.idBBRechazado"
                              + " where dt.NumAuditoria=1 and idLinea= " + drplinea.SelectedValue + " and convert(date,br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by br.idBBRechazado,Muestreo, dt.idDefectos ) as c )as b"
                              + " ) pvt unpivot (porcentaje for quality in (AQL,OQL)) as pv";
                        aqloql = DataAccess.Get_DataTable(query);
                    }
                    else
                    {
                        n = 1;
                    }
                    break;

                case "2":
                    if (drplinea.SelectedItem.Text != "Select...")
                    {
                        query = " select 'Line: ' + Linea as Linea, quality, porcentaje from ( select a.Linea, CAST((CAST(a.Total as decimal (16,3)) / (CAST(a.Total as decimal (16,3)) + CAST(b.Total as decimal (16,3)))) * 100 as decimal (16,3)) as AQL,"
                              + " cast(((((CAST(a.Total as decimal(16,3)) + cast(b.Total as decimal(16,3))) - cast(b.TotalDef as decimal(16,3))) / CAST(b.rechazadas as decimal(16,3))) * 100) as decimal(16,3)) as OQL"
                              + " from ( select l.numero as Linea, COUNT(idBBAprovado)as Total,sum(UnidadesAuditadas)as aceptadas from tbAuditExterAprovados bb join Linea l on l.id_linea = bb.idLinea"
                              + " where idLinea= " + drplinea.SelectedValue + " and convert(date,FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by l.numero )as a, ( select COUNT(c.idBBRechazado)as Total,sum(c.Muestreo)as rechazadas, COUNT(c.idDefectos) as TotalDef"
                              + " from ( select br.idBBRechazado,Muestreo, dt.idDefectos from tbAuditExterRechazado br join tbAuditExterRechazadoDet dt on br.idBBRechazado=dt.idBBRechazado"
                              + " where dt.NumAuditoria=1 and idLinea= " + drplinea.SelectedValue + " and convert(date,br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by br.idBBRechazado,Muestreo, dt.idDefectos ) as c )as b"
                              + " ) pvt unpivot (porcentaje for quality in (AQL,OQL)) as pv";
                        aqloql = DataAccess.Get_DataTable(query);
                    }
                    else
                    {
                        n = 1;
                    }
                    break;

                case "3":
                    if (drplinea.SelectedItem.Text != "Select...")
                    {
                        query = " select 'Line: ' + Linea as Linea, quality, porcentaje from ( select a.Linea, CAST((CAST(a.Total as decimal (16,3)) / (CAST(a.Total as decimal (16,3)) + CAST(b.Total as decimal (16,3)))) * 100 as decimal (16,3)) as AQL,"
                              + " cast(((((CAST(a.Total as decimal(16,3)) + cast(b.Total as decimal(16,3))) - cast(b.TotalDef as decimal(16,3))) / CAST(b.rechazadas as decimal(16,3))) * 100) as decimal(16,3)) as OQL"
                              + " from ( select l.numero as Linea, COUNT(idBBAprovado)as Total,sum(UnidadesAuditadas)as aceptadas from tbBultoaBultoAprovados bb join Linea l on l.id_linea = bb.idLinea"
                              + " where idLinea= " + drplinea.SelectedValue + " and convert(date,FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by l.numero )as a, ( select COUNT(c.idBBRechazado)as Total,sum(c.Muestreo)as rechazadas, COUNT(c.idDefectos) as TotalDef"
                              + " from ( select br.idBBRechazado,Muestreo, dt.idDefectos from tbBultoaBultoRechazado br join tbBultoaBultoRechazadoDet dt on br.idBBRechazado=dt.idBBRechazado"
                              + " where dt.NumAuditoria=1 and idLinea= " + drplinea.SelectedValue + " and convert(date,br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by br.idBBRechazado,Muestreo, dt.idDefectos ) as c )as b"
                              + " ) pvt unpivot (porcentaje for quality in (AQL,OQL)) as pv";
                        aqloql = DataAccess.Get_DataTable(query);
                    }
                    else
                    {
                        n = 1;
                    }
                    break;

                case "4":
                    if (drplinea.SelectedItem.Text != "Select...")
                    {
                        query = " select 'Line: ' + Linea as Linea, quality, porcentaje from ( select a.Linea, CAST((CAST(a.Total as decimal (16,3)) / (CAST(a.Total as decimal (16,3)) + CAST(b.Total as decimal (16,3)))) * 100 as decimal (16,3)) as AQL,"
                              + " cast(((((CAST(a.Total as decimal(16,3)) + cast(b.Total as decimal(16,3))) - cast(b.TotalDef as decimal(16,3))) / CAST(b.rechazadas as decimal(16,3))) * 100) as decimal(16,3)) as OQL"
                              + " from ( select l.numero as Linea, COUNT(idBBAprovado)as Total,sum(UnidadesAuditadas)as aceptadas from tbInspecProcAprovados bb join Linea l on l.id_linea = bb.idLinea"
                              + " where idLinea= " + drplinea.SelectedValue + " and convert(date,FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by l.numero )as a, ( select COUNT(c.idBBRechazado)as Total,sum(c.Muestreo)as rechazadas, COUNT(c.idDefectos) as TotalDef"
                              + " from ( select br.idBBRechazado,Muestreo, dt.idDefectos from tbInspecProcRechazado br join tbInspecProcRechazadoDet dt on br.idBBRechazado=dt.idBBRechazado"
                              + " where dt.NumAuditoria=1 and idLinea= " + drplinea.SelectedValue + " and convert(date,br.FechaInspeccion) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by br.idBBRechazado,Muestreo, dt.idDefectos ) as c )as b"
                              + " ) pvt unpivot (porcentaje for quality in (AQL,OQL)) as pv";
                        aqloql = DataAccess.Get_DataTable(query);
                    }
                    else
                    {
                        n = 1;
                    }
                    break;

                default: break;

            }

            if (n == 0)
            {
                Session["extra1"] = aqloql;
                WebChartControl2.DataSource = aqloql;
                WebChartControl2.DataBind();
                lblplan.Text = "Line: " + drplinea.SelectedItem.Text;
                lblprocess.Text = rd1.SelectedItem.Text;
            }           

        }
        catch (Exception ex)
        {
            string a = ex.Message;
            throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();
            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);

            WebChartControl2.DataSource = Session["extra1"];
            WebChartControl2.DataBind();

            link2.Component = ((DevExpress.XtraCharts.Native.IChartContainer)WebChartControl2).Chart;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link2 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "AQL-OQL-by-Plant-";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Oql-Aql-" + lblprocess.Text + "-" + lblplan.Text + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnclean_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/OQLAQLxLinea.aspx");
    }
}