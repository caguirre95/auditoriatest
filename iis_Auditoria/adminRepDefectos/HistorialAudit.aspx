﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HistorialAudit.aspx.cs" Inherits="adminRepDefectos_HistorialAudit" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hdnidporder.ClientID%>').val(ui.item.idorder);
                    // $('#<=txtstyle.ClientID%>').val(ui.item.style);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        })
    </script>

    <script lang="javascript" type="text/javascript">
        function autoestilo() {

            $(function () {
                $('#<%=txtstyle.ClientID%>').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "AqlOql.aspx/GetStyle",
                            data: "{'pref' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        estilo: item.estilo,
                                        id: item.id,
                                        json: item
                                    }
                                }))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                        return false;
                    },
                    select: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);
                        $('#<%=hdnidstyle.ClientID%>').val(ui.item.id);

                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li  >")
                        .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
                };
            })
        }
    </script>

    <script type="text/javascript">
        function date1() {
            $('#<%=txtfecha1.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

        function date2() {
            $('#<%=txtfecha2.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <br />
    <div class="container">

        <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

        <div class="panel panel-primary" style="margin-top: 5px">
            <div class="panel-heading">Historial Bulto a Bulto</div>
            <div class="panel-body">

                <div class="col-lg-12">

                    <%--<div class="col-lg-3">
                        <div class="panel-default">
                            <div class="panel-heading">Auditorias</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="rdaudit" runat="server">
                                    <asp:ListItem Value="3">Auditoria Bulto a Bulto</asp:ListItem>
                                    <asp:ListItem Value="5">Auditoria Embarque</asp:ListItem>
                                    <asp:ListItem Value="6">Todas las Auditorias</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>--%>

                    <div class="col-lg-3">
                        <div class="panel-default">
                            <div class="panel-heading">Filtros</div>
                            <div class="panel-body">

                                <asp:RadioButtonList ID="rd1" runat="server">
                                    <asp:ListItem Value="1">Cut</asp:ListItem>
                                    <asp:ListItem Value="2">Style</asp:ListItem>
                                    <asp:ListItem Value="3">Line</asp:ListItem>
                                    <asp:ListItem Value="4">Plant</asp:ListItem>
                                    <asp:ListItem Value="5">Deficit</asp:ListItem>
                                </asp:RadioButtonList>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <asp:Panel runat="server" ID="panelCorte">
                                    Cut:                                   
                                    <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdnidporder" />

                                </asp:Panel>
                                <asp:Panel runat="server" ID="panelLinea">
                                    Line:
                                     <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="panelEstilo">
                                    Style:                                   
                                    <asp:TextBox ID="txtstyle" onkeypress="autoestilo()" AutoCompleteType="Disabled" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdnidstyle" />
                                </asp:Panel>
                                <asp:Panel runat="server" ID="panelPlanta">
                                    Plant:
                                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-5">
                        <div class="panel-default">
                            <div class="panel-heading">Select filter Date</div>
                            <div class="panel-body">
                                <asp:Panel runat="server" ID="panelfechas">
                                    Start Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                    End Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>

                                </asp:Panel>
                                <div style="padding-top: 15px">
                                    <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" runat="server" OnClick="btngenerar_Click" />
                                </div>
                                <div style="padding-top: 15px">
                                    <asp:Button Text="Limpiar Campos" CssClass="btn btn-info form-control" ID="btnlimpiar" runat="server" OnClick="btnlimpiar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <hr />
                    <div class="col-lg-12">
                        <hr />
                        <dx1:ASPxGridView ID="gridpending" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="gridpending_DataBinding" OnCustomCallback="gridpending_CustomCallback">
                            <SettingsSearchPanel Visible="true" />
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Auditoria Bulto a Bulto - Pendientes" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="numero" Caption="Linea" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Style" Caption="Estilo" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <%--<dx1:GridViewDataTextColumn FieldName="Id_Order" Caption="Id" Visible="false"></dx1:GridViewDataTextColumn>--%>
                                        <dx1:GridViewDataTextColumn FieldName="POrder" Caption="Corte" Settings-AllowCellMerge="True" BatchEditModifiedCellStyle-HorizontalAlign="Center"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Bld" Caption="# Bulto"></dx1:GridViewDataTextColumn>                                       
                                    </Columns>
                                </dx1:GridViewBandColumn>
                            </Columns>
                            <Settings VerticalScrollBarMode="Visible" />
                            <SettingsPager PageSize="1000"></SettingsPager>
                        </dx1:ASPxGridView>
                        <dx1:ASPxGridViewExporter ID="export4" runat="server"></dx1:ASPxGridViewExporter>
                        <hr />
                    </div>                    
                    <div class="col-lg-6">
                        <dx1:ASPxGridView ID="grdaprobado" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdaprobado_DataBinding" OnCustomCallback="grdaprobado_CustomCallback">
                            <SettingsSearchPanel Visible="true" />
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Auditoria Bulto a Bulto - Aprobado" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="Linea" Caption="Linea" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Id_Order" Caption="Id" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Corte" Caption="Corte" Settings-AllowCellMerge="True" BatchEditModifiedCellStyle-HorizontalAlign="Center"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Bulto" Caption="# Bulto"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="FechaInspeccion" Caption="Fecha Inspeccion" PropertiesTextEdit-DisplayFormatString="dd-MM-yyyy"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="HoraInspeccion" Caption="Hora de Inspeccion"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Inspector" Caption="Auditado Por"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:GridViewBandColumn>
                            </Columns>
                            <Settings VerticalScrollBarMode="Visible" HorizontalScrollBarMode="Visible" />
                            <SettingsPager PageSize="1000"></SettingsPager>
                        </dx1:ASPxGridView>
                        <dx1:ASPxGridViewExporter ID="export" runat="server"></dx1:ASPxGridViewExporter>
                    </div>
                    <div class="col-lg-6">
                        <%--<hr />--%>
                        <dx1:ASPxGridView ID="grdrechazado" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdrechazado_DataBinding" OnCustomCallback="grdrechazado_CustomCallback">
                            <SettingsSearchPanel Visible="true" />
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Auditoria Bulto a Bulto - Rechazado" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="Linea" Caption="Linea" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Id_Order" Caption="Id" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Corte" Caption="Corte" Settings-AllowCellMerge="True" BatchEditModifiedCellStyle-HorizontalAlign="Center"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Bulto" Caption="# Bulto"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="FechaInspeccion" Caption="Fecha Inspeccion" PropertiesTextEdit-DisplayFormatString="dd-MM-yyyy"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="HoraInspeccion" Caption="Hora de Inspeccion"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Inspector" Caption="Auditado Por"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:GridViewBandColumn>
                            </Columns>
                            <Settings VerticalScrollBarMode="Visible" HorizontalScrollBarMode="Visible" />
                            <SettingsPager PageSize="1000"></SettingsPager>
                        </dx1:ASPxGridView>
                        <dx1:ASPxGridViewExporter ID="export2" runat="server"></dx1:ASPxGridViewExporter>
                    </div>                    
                    <div class="col-lg-12">
                        <hr />
                        <dx1:ASPxGridView ID="griddef" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="griddef_DataBinding" OnCustomCallback="griddef_CustomCallback">
                            <SettingsSearchPanel Visible="true" />
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Auditoria Bulto a Bulto - Deficit" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="Id_Order" Caption="Id" Visible="false"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="POrder" Caption="Corte" BatchEditModifiedCellStyle-HorizontalAlign="Center"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Quantity" Caption="Cantidad del Corte"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="UnidadesAuditadas" Caption="Unidades Auditadas"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Deficit" Caption="Deficit"></dx1:GridViewDataTextColumn>
                                    </Columns>
                                </dx1:GridViewBandColumn>
                            </Columns>
                            <Settings VerticalScrollBarMode="Visible" />
                            <SettingsPager PageSize="1000"></SettingsPager>
                        </dx1:ASPxGridView>
                        <dx1:ASPxGridViewExporter ID="export3" runat="server"></dx1:ASPxGridViewExporter>
                    </div>
                    <div class="col-lg-12">
                        <br />
                        <asp:Button Text="Exportar Reporte" CssClass="btn btn-success form-control" ID="btnexport" runat="server" OnClick="btnexport_Click" />
                    </div>
                </div>
            </div>

        </div>
    </div>

</asp:Content>

