﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_FechhAudit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["crt"] = null;
            Session["aud"] = null;
        }
    }

    void Corte()
    {
        try
        {
            DataTable dt;            

            string queryCortes = " select p.POrder 'MPOrder', s.Style 'StyleNo.', l.numero Line, p.Quantity QTY, c.Cliente Client, "
                               + " case when p.Describir = 'SHI' then 'Shirts' else case when p.Describir = 'P' then 'Pants' else 'S/D' end end Garment, pl.descripcion Modulo"
                               + " from POrder p left join Asignacionlinea al on al.idorder = p.Id_Order left join Style s on s.Id_Style = p.Id_Style left join Linea l on l.id_linea = p.Id_Linea2"
                               + " left join Cliente c on c.Id_Cliente = p.Id_Cliente left join Planta pl on pl.id_planta = l.id_planta where pl.id_planta = 4"
                               + " and DATEPART(YEAR, al.date1) = DATEPART(year, GETDATE()) and c.Id_Cliente = 92"
                               + " group by p.POrder, s.Style, l.numero, p.Quantity, c.Cliente, p.Describir, pl.descripcion";

            dt = DataAccess.Get_DataTable(queryCortes);

            Session["crt"] = dt;
            grdcortes.DataSource = Session["crt"];
            grdcortes.DataBind();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    void Auditoria()
    {
        try
        {
            DataTable dt;

            string queryAudit = " select CONVERT(date,na.horaInicial) Fecha, DATEPART(WEEK, horaInicial) Semana, p.POrder 'MPOrder', ae.lote 'QTYAudit',"
                              + " case when na.estadoAudit = 1 then 'PASS' else case when na.estadoAudit = 0 then 'FAIL' else 'S/D' end end Estatus from tbAuditEmbarque ae"
                              + " join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id where na.secuencia = 1 and p.Id_Cliente = 92"
                              + " and DATEPART(YEAR, na.horaInicial) = DATEPART(year, GETDATE()) order by DATEPART(WEEK, horaInicial) asc, CONVERT(date,na.horaInicial) asc";

            dt = DataAccess.Get_DataTable(queryAudit);

            Session["aud"] = dt;
            grdauditorias.DataSource = Session["aud"];
            grdauditorias.DataBind();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            Corte();
            Auditoria();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            export.GridViewID = "grdcortes";
            export2.GridViewID = "grdauditorias";            

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link.Component = export;

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link2.Component = export2;            

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link, link2});

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Auditorias";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=AuditoriasMP" + ".xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/FechhAudit.aspx");
    }

    protected void grdcortes_DataBinding(object sender, EventArgs e)
    {
        grdcortes.DataSource = Session["crt"];        
    }

    protected void grdcortes_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdcortes.DataBind();
    }

    protected void grdauditorias_DataBinding(object sender, EventArgs e)
    {
        grdauditorias.DataSource = Session["aud"];
    }

    protected void grdauditorias_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdauditorias.DataBind();
    }
}