﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_OperarioDef : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                txtfecha1.Text = DateTime.Now.ToShortDateString();
                txtfecha2.Text = DateTime.Now.ToShortDateString();
                cargacombo();
                Session["oper"] = null;
                Session["operdef"] = null;
                aprovado_p.Visible = false;
                rechazado_p.Visible = false;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    void cargacombo()
    {
        try
        {
            string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                          + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                          + " order by CONVERT(float, c1.numero) asc";

            DataTable dt_Line = DataAccess.Get_DataTable(sqline);

            DropDownListLine.DataSource = dt_Line;
            DropDownListLine.DataTextField = "numero";
            DropDownListLine.DataValueField = "id_linea";
            DropDownListLine.DataBind();
            DropDownListLine.Items.Insert(0, "Select...");

            string sqlplant = "SELECT id_planta, descripcion FROM Planta order by descripcion asc";

            DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

            DropDownListPlant.DataSource = dt_plant;
            DropDownListPlant.DataTextField = "descripcion";
            DropDownListPlant.DataValueField = "id_planta";
            DropDownListPlant.DataBind();
            DropDownListPlant.Items.Insert(0, "Select...");
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void rdbult_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string id1 = rdbult.SelectedValue;

            switch (id1)
            {
                case "1":
                    aprovado_p.Visible = true;
                    rechazado_p.Visible = false;
                    rdaudit.SelectedValue = "4";
                    break;

                case "2":
                    aprovado_p.Visible = false;
                    rechazado_p.Visible = true;
                    rdaudit.SelectedValue = "4";
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void rd1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string id = rd1.SelectedValue;

            switch (id)
            {
                case "1":
                    panelCorte.Visible = true;
                    panelEstilo.Visible = false;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = false;
                    break;

                case "2":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = true;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;

                case "3":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = false;
                    panelLinea.Visible = true;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;

                case "4":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = false;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = true;
                    panelfechas.Visible = true;
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    void defecto()
    {
        try
        {
            int idporder;
            string estilo;
            int idline;
            int idplant;

            if (hdnidporder.Value != "") { idporder = Convert.ToInt32(hdnidporder.Value); } else { idporder = 0; }

            if (txtstyle.Text != "") { estilo = txtstyle.Text; } else { estilo = ""; }

            if (DropDownListLine.SelectedValue != "Select...") { idline = Convert.ToInt16(DropDownListLine.SelectedValue); } else { idline = 0; }

            if (DropDownListPlant.SelectedValue != "Select...") { idplant = Convert.ToInt16(DropDownListPlant.SelectedValue); } else { idplant = 0; }

            var df = OrderDetailDA.defectoOper(idporder, estilo, idline, idplant, Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));

            Session["oper"] = df;
            grddef.DataSource = Session["oper"];
            grddef.DataBind();
        }
        catch (Exception)
        {
            throw;
        }
    }

    void defectorec()
    {
        try
        {
            int idporder;
            string estilo;
            int idline;
            int idplant;

            if (hdnidporder.Value != "") { idporder = Convert.ToInt32(hdnidporder.Value); } else { idporder = 0; }

            if (txtstyle.Text != "") { estilo = txtstyle.Text; } else { estilo = ""; }

            if (DropDownListLine.SelectedValue != "Select...") { idline = Convert.ToInt16(DropDownListLine.SelectedValue); } else { idline = 0; }

            if (DropDownListPlant.SelectedValue != "Select...") { idplant = Convert.ToInt16(DropDownListPlant.SelectedValue); } else { idplant = 0; }

            var df = OrderDetailDA.defectoOperrec(idporder, estilo, idline, idplant, Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));

            Session["operdef"] = df;
            grdrec.DataSource = Session["operdef"];
            grdrec.DataBind();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            string opb = rdbult.SelectedValue;

            string op1 = rdaudit.SelectedValue;

            string opcionfi = rd1.SelectedValue;

            switch (opb)
            {
                case "1":
                    switch (op1)
                    {
                        case "1"://Auditoria interna
                            switch (opcionfi)
                            {
                                case "1":

                                    break;

                                case "2":

                                    break;

                                case "3":

                                    break;

                                case "4":

                                    break;

                                default:
                                    break;
                            }
                            break;

                        case "2"://Auditoria Externa
                            switch (opcionfi)
                            {
                                case "1":

                                    break;

                                case "2":

                                    break;

                                case "3":

                                    break;

                                case "4":

                                    break;

                                default:
                                    break;
                            }
                            break;

                        case "3"://Auditoria Bulto a Bulto
                            switch (opcionfi)
                            {
                                case "1":

                                    break;

                                case "2":

                                    break;

                                case "3":

                                    break;

                                case "4":

                                    break;

                                default:
                                    break;
                            }
                            break;

                        case "4"://Auditoria Inspeccion Proceso
                            switch (opcionfi)
                            {
                                case "1":
                                    defecto();
                                    break;

                                case "2":
                                    defecto();
                                    break;

                                case "3":
                                    defecto();
                                    break;

                                case "4":
                                    defecto();
                                    break;

                                default:
                                    break;
                            }
                            break;

                        default:
                            break;
                    }
                    break;

                case "2":
                    switch (op1)
                    {
                        case "1"://Auditoria Interna
                            switch (opcionfi)
                            {
                                case "1":

                                    break;

                                case "2":

                                    break;

                                case "3":

                                    break;

                                case "4":

                                    break;

                                default:
                                    break;
                            }
                            break;

                        case "2"://Auditoria Externa
                            switch (opcionfi)
                            {
                                case "1":

                                    break;

                                case "2":

                                    break;

                                case "3":

                                    break;

                                case "4":

                                    break;

                                default:
                                    break;
                            }
                            break;

                        case "3"://Auditoria Bulto a Bulto
                            switch (opcionfi)
                            {
                                case "1":

                                    break;

                                case "2":

                                    break;

                                case "3":

                                    break;

                                case "4":

                                    break;

                                default:
                                    break;
                            }
                            break;

                        case "4"://Auditoria Inspeccion Proceso
                            switch (opcionfi)
                            {
                                case "1":
                                    defectorec();
                                    break;

                                case "2":
                                    defectorec();
                                    break;

                                case "3":
                                    defectorec();
                                    break;

                                case "4":
                                    defectorec();
                                    break;

                                default:
                                    break;
                            }
                            break;

                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/OperarioDef.aspx");
    }

    protected void grddef_DataBinding(object sender, EventArgs e)
    {
        if (Session["oper"] != null)
        {
            grddef.DataSource = Session["oper"];
        }
    }

    protected void grddef_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grddef.DataBind();
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            export.GridViewID = "grddef";
            export.FileName = "Reporte Aprovados " + DateTime.Now;
            export.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void grdrec_DataBinding(object sender, EventArgs e)
    {
        if (Session["operdef"] != null)
        {
            grdrec.DataSource = Session["operdef"];
        }
    }

    protected void grdrec_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdrec.DataBind();
    }

    protected void btne_Click(object sender, EventArgs e)
    {
        try
        {
            export1.GridViewID = "grdrec";
            export1.FileName = "Reporte Rechazos " + DateTime.Now;
            export1.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
        catch (Exception)
        {
            throw;
        }
    }
}