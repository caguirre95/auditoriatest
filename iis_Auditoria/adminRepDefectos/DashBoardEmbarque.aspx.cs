﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_DashBoardEmbarque : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["r1"] = null;
            Session["r2"] = null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select Top 15 POrderClient from POrder p where POrderClient like '%" + pre + "%' group by POrderClient", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porderclient = dt.Rows[i][0].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porderclient { get; set; }
    }

    void R1()
    {
        DataTable dt;

        string query = " select a1.POrderClient, a1.Style, a1.Color, a1.Quantity, a1.Linea, ISNULL(b1.TotalA, 0) UnidadesAceptadas, (ISNULL(b1.TotalA, 0) - a1.Quantity) Pendientes, c1.Defectos from"
                     + " (select p.POrderClient, s.Style, Min(b.Color) Color, sum(b.Quantity) Quantity, l.numero Linea from POrder p join Style s on s.Id_Style = p.Id_Style "
                     + " join Bundle b on b.Id_Order = p.Id_Order join Linea l on l.id_linea = p.Id_Linea2 where p.POrderClient = '" + txtcorte.Text + "' group by p.POrderClient, s.Style, l.numero) a1 left join"
                     + " (select a.POrderClient, SUM(a.lote) TotalA from (select p.POrderClient, ae.codigoLote, ae.lote from tbAuditEmbarque ae join POrder p on p.Id_Order = ae.idcorte"
                     + " join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id where p.POrderClient = '" + txtcorte.Text + "' and na.estadoAudit = 1 group by p.POrderClient, ae.codigoLote, ae.lote"
                     + " ) a group by a.POrderClient) b1 on a1.POrderClient = b1.POrderClient left join (select p.POrderClient, ISNULL(SUM(aed.unidad),0) Defectos from tbAuditEmbarque ae"
                     + " join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id"
                     + " left join tbDefectos d on d.idDefecto = aed.idDefecto left join tbPosicion po on po.idPosicion = aed.idPosicion where p.POrderClient = '" + txtcorte.Text + "' group by p.POrderClient"
                     + " ) c1 on c1.POrderClient = a1.POrderClient";

        dt = DataAccess.Get_DataTable(query);
        Session["r1"] = dt;

        grdr1.DataSource = Session["rpcenv"];
        grdr1.DataBind();
    }

    void R2()
    {
        DataTable dt;

        string query = " select p.POrderClient, aed.unidad, d.Defecto from tbAuditEmbarque ae join POrder p on p.Id_Order = ae.idcorte join tbNumeroAuditoria na on na.idAuditoriaEmbarque = ae.id"
                     + " join tbAuditEmbarqueDefectos aed on aed.idAuditEmbarque = na.id left join tbDefectos d on d.idDefecto = aed.idDefecto left join tbPosicion po on po.idPosicion = aed.idPosicion"
                     + " where p.POrderClient = '" + txtcorte.Text + "'";

        dt = DataAccess.Get_DataTable(query);
        Session["r2"] = dt;

        grdr2.DataSource = Session["rpcenv"];
        grdr2.DataBind();
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtcorte.Text != string.Empty)
            {
                R1();
                R2();
            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void btnlim_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminRepDefectos/DashBoardEmbarque.aspx");
    }

    protected void grdr1_DataBinding(object sender, EventArgs e)
    {
        grdr1.DataSource = Session["r1"];
    }

    protected void grdr1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdr1.DataBind();
    }

    protected void grdr2_DataBinding(object sender, EventArgs e)
    {
        grdr2.DataSource = Session["r2"];
    }

    protected void grdr2_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdr2.DataBind();
    }

    protected void btnexportar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtcorte.Text != string.Empty)
            {
                DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                export1.GridViewID = "grdr1";
                export2.GridViewID = "grdr2";

                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                link1.Component = export1;

                DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                link2.Component = export2;

                DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                compositeLink.Links.AddRange(new object[] { link1, link2 });

                compositeLink.CreatePageForEachLink();

                using (MemoryStream stream = new MemoryStream())
                {
                    XlsxExportOptions options = new XlsxExportOptions();
                    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                    options.SheetName = "Dashboard";
                    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                    Response.Clear();
                    Response.Buffer = false;
                    Response.AppendHeader("Content-Type", "application/xlsx");
                    Response.AppendHeader("Content-Transfer-Encoding", "binary");
                    Response.AppendHeader("Content-Disposition", "attachment; filename=Dashboard" + ".xlsx");
                    Response.BinaryWrite(stream.ToArray());
                    Response.End();
                }
                ps.Dispose();
            }            
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }    
}