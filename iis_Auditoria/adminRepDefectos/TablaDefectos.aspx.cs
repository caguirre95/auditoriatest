﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_TablaDefectos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                cargacombo();
                txtfecha1.Text = DateTime.Now.ToShortDateString();
                txtfecha2.Text = DateTime.Now.ToShortDateString();

                Session["rep"] = null;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    void cargacombo()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta, descripcion FROM Planta order by descripcion asc";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");
        DropDownListPlant.Items.Insert(6, "All Plant");

    }

    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string id = RadioButtonList1.SelectedValue;

            switch (id)
            {
                case "1":
                    panelCorte.Visible = true;
                    panelEstilo.Visible = false;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = false;
                    break;
                case "2":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = true;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;
                case "3":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = false;
                    panelLinea.Visible = true;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;
                case "4":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = false;
                    panelLinea.Visible = false;
                    panelPlanta.Visible = true;
                    panelfechas.Visible = true;
                    break;

                case "5":
                    panelCorte.Visible = false;
                    panelEstilo.Visible = true;
                    panelLinea.Visible = true;
                    panelPlanta.Visible = false;
                    panelfechas.Visible = true;
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView1.DataSource = (DataTable)Session["rep"];
    }

    protected void ASPxGridView1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView1.DataBind();
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView1.Columns.Clear();

            var dt = new DataTable();

            string id = RadioButtonList1.SelectedValue;
            bool resp = false;

            int idAudit =Convert.ToInt16(RadioButtonList2.SelectedValue);

            switch (id)
            {
                case "1":
                    if (hdnidporder.Value != string.Empty)
                    {
                        dt = tablaXCortes(idAudit);
                    }
                    else
                    {
                        resp = true;
                    }
                    break;
                case "2":
                    if (hdnidstyle.Value != string.Empty)
                    {
                        dt = tablaXEstilo(idAudit);
                    }
                    else
                    {
                        resp = true;
                    }
                    break;
                case "3":
                    if (DropDownListLine.SelectedItem.Text != "Select...")
                    {
                        dt = tablaXLinea(idAudit);
                    }
                    else
                    {
                        resp = true;
                    }
                    break;
                case "4":
                    if (DropDownListPlant.SelectedItem.Text != "Select...")
                    {
                        if (DropDownListPlant.SelectedItem.Value == "All Plant")
                        {
                            dt = tablaXPlantaAll(idAudit); 
                        }
                        else
                        {
                            dt = tablaXPlanta(idAudit);
                        }
                    }
                    else
                    {
                        resp = true;
                    }
                    break;

                case "5":
                    if (DropDownListLine.SelectedValue != "0" && hdnidstyle.Value != "")
                    {
                        dt = tablaXLineaEstilo(idAudit);
                    }
                    else
                    {
                        resp = true;
                    }
                    break;

                default:
                    break;
            }

            if (!resp)
            {
                Session["rep"] = dt;

                ASPxGridView1.AutoGenerateColumns = true;

                ASPxGridView1.DataBind();

            }
        }
        catch (Exception)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "Alerta();", true);
            //throw;
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView1.DataBind();

            ASPxGridViewExporter1.GridViewID = "ASPxGridView1";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = ASPxGridViewExporter1;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreatePageForEachLink();
            //compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Posicio_Defectos";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=TablaDefectos.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }

            ps.Dispose();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    #region Funciones
    DataTable tablaXCortes(int idAudit)
    {
        var dt = new DataTable();

        if (idAudit == 1)
        {
            dt = OrderDetailDA.spdRepIPTablaDefectosXCorte(Convert.ToInt32(hdnidporder.Value));
        }
        else if (idAudit == 2)
        {
            dt = OrderDetailDA.spdRepBBTablaDefectosXCorte(Convert.ToInt32(hdnidporder.Value));
        }
        else if (idAudit == 3)
        {
            dt = OrderDetailDA.spdRepAITablaDefectosXCorte(Convert.ToInt32(hdnidporder.Value));
        }
        else if (idAudit == 4)
        {
            dt = OrderDetailDA.spdRepAETablaDefectosXCorte(Convert.ToInt32(hdnidporder.Value));
        }

        return dt;
    }

    DataTable tablaXEstilo(int idAudit)
    {
        var dt = new DataTable();

        if (idAudit == 1)
        {
            dt = OrderDetailDA.spdRepIPTablaDefectosXEstilo(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt32(hdnidstyle.Value));
        }
        else if (idAudit == 2)
        {
            dt = OrderDetailDA.spdRepBBTablaDefectosXEstilo(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt32(hdnidstyle.Value));
        }
        else if (idAudit == 3)
        {
            dt = OrderDetailDA.spdRepAITablaDefectosXEstilo(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt32(hdnidstyle.Value));
        }
        else if (idAudit == 4)
        {
            dt = OrderDetailDA.spdRepAETablaDefectosXEstilo(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt32(hdnidstyle.Value));
        }

        return dt;
    }

    DataTable tablaXLinea(int idAudit)
    {
        var dt = new DataTable();

        if (idAudit == 1)
        {
            dt = OrderDetailDA.spdRepIPTablaDefectosXLinea(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListLine.SelectedValue));
        }
        else if (idAudit == 2)
        {
            dt = OrderDetailDA.spdRepBBTablaDefectosXLinea(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListLine.SelectedValue));
        }
        else if (idAudit == 3)
        {
            dt = OrderDetailDA.spdRepAITablaDefectosXLinea(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListLine.SelectedValue));
        }
        else if (idAudit == 4)
        {
            dt = OrderDetailDA.spdRepAETablaDefectosXLinea(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListLine.SelectedValue));
        }

        return dt;
    }

    DataTable tablaXPlanta(int idAudit)
    {
        var dt = new DataTable();

        if (idAudit == 1)
        {
            dt = OrderDetailDA.spdRepIPTablaDefectosXPlanta(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListPlant.SelectedValue));
        }
        else if (idAudit == 2)
        {
            dt = OrderDetailDA.spdRepBBTablaDefectosXPlanta(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListPlant.SelectedValue));
        }
        else if (idAudit == 3)
        {
            dt = OrderDetailDA.spdRepAITablaDefectosXPlanta(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListPlant.SelectedValue));
        }
        else if (idAudit == 4)
        {
            dt = OrderDetailDA.spdRepAETablaDefectosXPlanta(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt16(DropDownListPlant.SelectedValue));
        }

        return dt;
    }

    DataTable tablaXPlantaAll(int idAudit)
    {
        var dt = new DataTable();

        if (idAudit == 1)
        {
            dt = OrderDetailDA.spdRepIPTablaDefectosAll(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));
        }
        else if (idAudit == 2)
        {
            dt = OrderDetailDA.spdRepBBTablaDefectosAll(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));
        }
        else if (idAudit == 3)
        {
            dt = OrderDetailDA.spdRepAITablaDefectosAll(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));
        }
        else if (idAudit == 4)
        {
            dt = OrderDetailDA.spdRepAETablaDefectosAll(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));
        }

        return dt;
    }

    DataTable tablaXLineaEstilo(int idAudit)
    {
        var dt = new DataTable();

        if (idAudit == 1)
        {
            dt = OrderDetailDA.spdRepIPTablaDefectosXLineaStyle(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt32(DropDownListLine.SelectedValue), Convert.ToInt32(hdnidstyle.Value));
        }
        else if (idAudit == 2)
        {
            dt = OrderDetailDA.spdRepBBTablaDefectosXLineaStyle(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt32(DropDownListLine.SelectedValue), Convert.ToInt32(hdnidstyle.Value));
        }
        else if (idAudit == 3)
        {
            dt = OrderDetailDA.spdRepAITablaDefectosXLineaStyle(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt32(DropDownListLine.SelectedValue), Convert.ToInt32(hdnidstyle.Value));
        }
        else if (idAudit == 4)
        {
            dt = OrderDetailDA.spdRepAETablaDefectosXLineaStyle(Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text), Convert.ToInt32(DropDownListLine.SelectedValue), Convert.ToInt32(hdnidstyle.Value));
        }

        return dt;
    }
    #endregion
}