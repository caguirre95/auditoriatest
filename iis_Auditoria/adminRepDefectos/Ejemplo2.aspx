﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Ejemplo2.aspx.cs" Inherits="adminRepDefectos_Ejemplo2" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script type="text/javascript">
        function actualizar() {
            location.reload(true);
        }
        //Función para actualizar cada 6 segundos(4000 milisegundos)
        setInterval("actualizar()", 100000);
    </script>

    <script type="text/javascript">
        function date1() {
            $('#<%=txtfecha.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

    </script>

    <style>
        .m {
            margin-top: 15px;
        }

        .st {
            font-size: x-large;
            font-weight: bold;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <div class="container-fluid">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary" style="margin-top: 5px">
                <div class="panel-heading">Produccion por Bihorario</div>
                <div class="panel-body">

                       <div class="col-lg-12">
                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                             Fecha
                            <dx1:ASPxDateEdit runat="server" ID="txtfecha" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                           </div>
                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                             Reporte
                             <asp:Button ID="btnreporte" runat="server" Text="Generar Reporte" CssClass="btn btn-primary form-control" OnClick="btnreporte_Click" />
                      
                           </div>
                       </div>
                       
                        <div class="col-lg-12" style="margin-top:5px">
                             <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <dx1:ASPxGridView ID="grdoper" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdoper_DataBinding" OnCustomCallback="grdoper_CustomCallback">
                                <Columns>
                                    <dx1:GridViewBandColumn Caption=" Linea 1" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx1:GridViewDataTextColumn FieldName="descOperacion" Caption="Operación"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="09:00am" Caption="09:00am"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="11:00am" Caption="11:00am"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="01:00pm" Caption="01:00pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="03:00pm" Caption="03:00pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="04:50pm" Caption="04:50pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="06:50pm" Caption="06:50pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Total" Caption="Total Por Operacion"></dx1:GridViewDataTextColumn>
                                        </Columns>
                                    </dx1:GridViewBandColumn>
                                </Columns>
                                <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="false" />
                                <Settings ShowFooter="true" HorizontalScrollBarMode="Hidden" />
                                <SettingsPager PageSize="100" />
                                <TotalSummary>
                                    <dx1:ASPxSummaryItem FieldName="descOperacion" SummaryType="Count" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="09:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="11:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="01:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="03:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="04:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="06:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="Total" SummaryType="Sum" DisplayFormat="{0}" />
                                </TotalSummary>
                                <GroupSummary>
                                </GroupSummary>
                            </dx1:ASPxGridView>
                      
                        </div>
                             <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <dx1:ASPxGridView ID="grdoper2" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdoper2_DataBinding" OnCustomCallback="grdoper2_CustomCallback">
                                <Columns>
                                    <dx1:GridViewBandColumn Caption=" Linea 2" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx1:GridViewDataTextColumn FieldName="descOperacion" Caption="Operación"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="09:00am" Caption="09:00am"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="11:00am" Caption="11:00am"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="01:00pm" Caption="01:00pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="03:00pm" Caption="03:00pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="04:50pm" Caption="04:50pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="06:50pm" Caption="06:50pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Total" Caption="Total Por Operacion"></dx1:GridViewDataTextColumn>
                                        </Columns>
                                    </dx1:GridViewBandColumn>
                                </Columns>
                                <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="false" />
                                <Settings ShowFooter="true" HorizontalScrollBarMode="Hidden" />
                                <SettingsPager PageSize="100" />
                                <TotalSummary>
                                    <dx1:ASPxSummaryItem FieldName="descOperacion" SummaryType="Count" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="09:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="11:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="01:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="03:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="04:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="06:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="Total" SummaryType="Sum" DisplayFormat="{0}" />
                                </TotalSummary>
                                <GroupSummary>
                                </GroupSummary>
                            </dx1:ASPxGridView>
                        
                        </div>
                       
                        </div>
                       
                       <div class="col-lg-12" style="margin-top:5px">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <dx1:ASPxGridView ID="grdoper3" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdoper3_DataBinding" OnCustomCallback="grdoper3_CustomCallback">
                                <Columns>
                                    <dx1:GridViewBandColumn Caption=" Linea 3" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx1:GridViewDataTextColumn FieldName="descOperacion" Caption="Operación"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="09:00am" Caption="09:00am"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="11:00am" Caption="11:00am"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="01:00pm" Caption="01:00pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="03:00pm" Caption="03:00pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="04:50pm" Caption="04:50pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="06:50pm" Caption="06:50pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Total" Caption="Total Por Operacion"></dx1:GridViewDataTextColumn>
                                        </Columns>
                                    </dx1:GridViewBandColumn>
                                </Columns>
                                <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="false" />
                                <Settings ShowFooter="true" HorizontalScrollBarMode="Hidden" />
                                <SettingsPager PageSize="100" />
                                <TotalSummary>
                                    <dx1:ASPxSummaryItem FieldName="descOperacion" SummaryType="Count" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="09:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="11:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="01:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="03:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="04:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="06:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="Total" SummaryType="Sum" DisplayFormat="{0}" />
                                </TotalSummary>
                                <GroupSummary>
                                </GroupSummary>
                            </dx1:ASPxGridView>
                       
                        </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <dx1:ASPxGridView ID="grdoper4" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdoper4_DataBinding" OnCustomCallback="grdoper4_CustomCallback">
                                <Columns>
                                    <dx1:GridViewBandColumn Caption=" Linea 4" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx1:GridViewDataTextColumn FieldName="descOperacion" Caption="Operación"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="09:00am" Caption="09:00am"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="11:00am" Caption="11:00am"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="01:00pm" Caption="01:00pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="03:00pm" Caption="03:00pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="04:50pm" Caption="04:50pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="06:50pm" Caption="06:50pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Total" Caption="Total Por Operacion"></dx1:GridViewDataTextColumn>
                                        </Columns>
                                    </dx1:GridViewBandColumn>
                                </Columns>
                                <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="false" />
                                <Settings ShowFooter="true" HorizontalScrollBarMode="Hidden" />
                                <SettingsPager PageSize="100" />
                                <TotalSummary>
                                    <dx1:ASPxSummaryItem FieldName="descOperacion" SummaryType="Count" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="09:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="11:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="01:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="03:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="04:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="06:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="Total" SummaryType="Sum" DisplayFormat="{0}" />
                                </TotalSummary>
                                <GroupSummary>
                                </GroupSummary>
                            </dx1:ASPxGridView>
                       
                        </div>
                       </div>
                       
                       <div class="col-lg-12" style="margin-top:5px">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-12">
                            <dx1:ASPxGridView ID="ASPxGridView1" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="ASPxGridView1_DataBinding" OnCustomCallback="ASPxGridView1_CustomCallback">
                                <Columns>
                                    <dx1:GridViewBandColumn Caption=" PLANTA 1" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx1:GridViewDataTextColumn FieldName="descOperacion" Caption="Operación"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="09:00am" Caption="09:00am"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="11:00am" Caption="11:00am"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="01:00pm" Caption="01:00pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="03:00pm" Caption="03:00pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="04:50pm" Caption="04:50pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="06:50pm" Caption="06:50pm"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Total" Caption="Total Por Operacion"></dx1:GridViewDataTextColumn>
                                        </Columns>
                                    </dx1:GridViewBandColumn>
                                </Columns>
                                <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="false" />
                                <Settings ShowFooter="true" HorizontalScrollBarMode="Hidden" />
                                <SettingsPager PageSize="100" />
                                <TotalSummary>
                                    <dx1:ASPxSummaryItem FieldName="descOperacion" SummaryType="Count" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="09:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="11:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="01:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="03:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="04:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="06:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dx1:ASPxSummaryItem FieldName="Total" SummaryType="Sum" DisplayFormat="{0}" />
                                </TotalSummary>
                                <GroupSummary>
                                </GroupSummary>
                            </dx1:ASPxGridView>
                       
                        </div>
                       </div>
                 
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

