﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TopDefectos.aspx.cs" Inherits="adminRepDefectos_TopDefectos" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hdnidporder.ClientID%>').val(ui.item.idorder);
                    // $('#<=txtstyle.ClientID%>').val(ui.item.style);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        })
    </script>
    <script lang="javascript" type="text/javascript">
        function autoestilo() {

            $(function () {
                $('#<%=txtstyle.ClientID%>').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "AqlOql.aspx/GetStyle",
                            data: "{'pref' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        estilo: item.estilo,
                                        id: item.id,
                                        json: item
                                    }
                                }))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                        return false;
                    },
                    select: function (event, ui) {
                        $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);
                        $('#<%=hdnidstyle.ClientID%>').val(ui.item.id);

                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li  >")
                        .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
                };
            })
        }
    </script>

    <script type="text/javascript">
        function date1() {
            $('#<%=txtfecha1.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

        function date2() {
            $('#<%=txtfecha2.ClientID%>').datepicker({
                dateFormat: "dd/mm/yy"
            });
        }

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <br />
    <div class="container">

        <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

        <div class="panel panel-primary" style="margin-top: 5px">
            <div class="panel-heading">Top 5 Defectos</div>
            <div class="panel-body">

                <div class="col-lg-12">

                    <div class="col-lg-3">
                        <div class="panel-default">
                            <div class="panel-heading">Auditorias</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="rdaudit" runat="server">
                                    <%--<asp:ListItem Value="1">Auditoria Interna</asp:ListItem>
                                    <asp:ListItem Value="2">Auditoria Externa</asp:ListItem>--%>
                                    <asp:ListItem Value="3">Auditoria Bulto a Bulto</asp:ListItem>
                                    <%--<asp:ListItem Value="4">Auditoria Inspeccion - Proceso</asp:ListItem>--%>
                                    <asp:ListItem Value="5">Auditoria Embarque</asp:ListItem>
                                    <asp:ListItem Value="6">Todas las Auditorias</asp:ListItem>
                                </asp:RadioButtonList>

                                <hr />
                                <a href="#ventanaG" data-toggle="modal" style="width: 161px" class="btn btn-md btn-success"><span class="glyphicon glyphicon-align-justify"></span>Ver Información</a>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel-default">
                            <div class="panel-heading">Filtros</div>
                            <div class="panel-body">

                                <asp:RadioButtonList ID="rd1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rd1_SelectedIndexChanged">
                                    <asp:ListItem Value="1">Cut</asp:ListItem>
                                    <asp:ListItem Value="2">Style</asp:ListItem>
                                    <asp:ListItem Value="3">Line</asp:ListItem>
                                    <asp:ListItem Value="4">Plant</asp:ListItem>
                                    <%--<asp:ListItem Value="5">Style - Line</asp:ListItem>--%>
                                </asp:RadioButtonList>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <asp:Panel runat="server" ID="panelCorte">
                                    Cut:                                   
                                    <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdnidporder" />

                                </asp:Panel>
                                <asp:Panel runat="server" ID="panelLinea">
                                    Line:
                                     <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="panelEstilo">
                                    Style:                                   
                                    <asp:TextBox ID="txtstyle" onkeypress="autoestilo()" AutoCompleteType="Disabled" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdnidstyle" />
                                </asp:Panel>
                                <asp:Panel runat="server" ID="panelPlanta">
                                    Plant:
                                     <asp:DropDownList ID="DropDownListPlant" CssClass="form-control" runat="server"></asp:DropDownList>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel-default">
                            <div class="panel-heading">Select filter Date</div>
                            <div class="panel-body">
                                <asp:Panel runat="server" ID="panelfechas">
                                    Start Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                    End Date
                                    <dx1:ASPxDateEdit runat="server" ID="txtfecha2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>

                                </asp:Panel>
                                <div style="padding-top: 15px">
                                    <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="btngenerar" runat="server" OnClick="btngenerar_Click" />
                                </div>
                                <div style="padding-top: 15px">
                                    <asp:Button Text="Limpiar Campos" CssClass="btn btn-info form-control" ID="btnlimpiar" runat="server" OnClick="btnlimpiar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <asp:Panel ID="panel1" runat="server" Visible="true">
                        <hr />
                        <asp:Label ID="lblaudit" runat="server" Text=""></asp:Label>
                        <div class="col-lg-12">
                            <dx:WebChartControl ID="chart" runat="server" Height="300px" Width="500px" CrosshairEnabled="False" SeriesDataMember="Defecto" AlternateText="Serie de Defectos" OnDataBinding="chart_DataBinding" OnCustomCallback="chart_CustomCallback">
                                <Titles>
                                    <dx:ChartTitle Text="Serie de Defectos"></dx:ChartTitle>
                                </Titles>
                                <SeriesTemplate LabelsVisibility="True" ArgumentDataMember="Posicion" ValueDataMembersSerializable="Total" CrosshairLabelPattern="{S} : {V:F3}">
                                    <ViewSerializable>
                                        <dx:SideBySideBarSeriesView></dx:SideBySideBarSeriesView>
                                    </ViewSerializable>
                                    <LabelSerializable>
                                        <dx:SideBySideBarSeriesLabel TextPattern="{V}">
                                        </dx:SideBySideBarSeriesLabel>
                                    </LabelSerializable>
                                </SeriesTemplate>
                                <DiagramSerializable>
                                    <dx:XYDiagram>
                                        <AxisX VisibleInPanesSerializable="-1">
                                            <Label Staggered="True"></Label>
                                        </AxisX>
                                        <AxisY VisibleInPanesSerializable="-1">
                                            <GridLines MinorVisible="True"></GridLines>
                                        </AxisY>
                                    </dx:XYDiagram>
                                </DiagramSerializable>
                                <BorderOptions Visibility="False" />
                                <CrosshairOptions>
                                    <CrosshairLabelTextOptions Font="Tahoma, 10pt" />
                                    <GroupHeaderTextOptions Font="Tahoma, 10pt, style=Bold" />
                                </CrosshairOptions>
                            </dx:WebChartControl>
                        </div>

                        <div class="col-lg-12" style="padding-top: 49px">
                            <dx1:ASPxGridView ID="dft" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="dft_DataBinding" OnCustomCallback="dft_CustomCallback">
                                <Columns>
                                    <dx1:GridViewDataTextColumn FieldName="Fecha" Caption="Fecha" PropertiesTextEdit-DisplayFormatString="MM/dd/yy"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Linea" Caption="Linea"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="POrder" Caption="Corte"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Posicion" Caption="Posicion"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Defecto" Caption="Defecto"></dx1:GridViewDataTextColumn>
                                    <dx1:GridViewDataTextColumn FieldName="Total" Caption="Total"></dx1:GridViewDataTextColumn>
                                </Columns>
                                <Settings ShowFooter="true" />
                                <TotalSummary>
                                    <dx1:ASPxSummaryItem FieldName="Total" SummaryType="Sum" ValueDisplayFormat="{0}" />
                                </TotalSummary>
                            </dx1:ASPxGridView>
                            <div style="padding-top: 20px">
                                <asp:Button ID="btnexcel" runat="server" Text="Export to Excel" CssClass="btn btn-success form-control" OnClick="btnexcel_Click" />
                            </div>
                            <dx1:ASPxGridViewExporter ID="export" runat="server"></dx1:ASPxGridViewExporter>
                        </div>
                    </asp:Panel>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <asp:Panel ID="panel2" runat="server" Visible="false">
                        <hr />
                        <dx:WebChartControl ID="g1" runat="server" Height="700px" Width="800px" OnCustomCallback="g1_CustomCallback" OnDataBinding="g1_DataBinding"
                            CrosshairEnabled="False" ToolTipEnabled="True" SeriesDataMember="Auditorias" RenderFormat="Svg">
                            <SeriesTemplate ArgumentDataMember="Defecto" ValueDataMembersSerializable="Total" LabelsVisibility="True" CrosshairLabelPattern="{V:0}">
                                <ViewSerializable>
                                    <dx:StackedBarSeriesView></dx:StackedBarSeriesView>
                                </ViewSerializable>
                                <LabelSerializable>
                                    <dx:StackedBarSeriesLabel Font="Tahoma, 8pt, style=Bold" TextPattern="{V:0}">
                                    </dx:StackedBarSeriesLabel>
                                </LabelSerializable>
                            </SeriesTemplate>
                            <Legend Direction="BottomToTop"></Legend>
                            <BorderOptions Visibility="False" />
                            <Titles>
                                <dx:ChartTitle Text="Defectos por Auditoria"></dx:ChartTitle>
                                <dx:ChartTitle Dock="Bottom" Alignment="Far" Text="From Rocedes" Font="Tahoma, 8pt" TextColor="Gray"></dx:ChartTitle>
                            </Titles>
                            <DiagramSerializable>
                                <dx:XYDiagram Rotated="True">
                                    <AxisX VisibleInPanesSerializable="-1">
                                    </AxisX>
                                    <AxisY Title-Text="Defectos" Title-Visibility="True" VisibleInPanesSerializable="-1">
                                        <Label TextPattern="{V:0}" />
                                        <GridLines MinorVisible="True"></GridLines>
                                    </AxisY>
                                    <DefaultPane>
                                        <%--<stackedbartotallabel textpattern="Total {V:0}" visible="True"></stackedbartotallabel>--%>
                                    </DefaultPane>
                                </dx:XYDiagram>
                            </DiagramSerializable>
                        </dx:WebChartControl>
                        <hr />
                        <dx1:ASPxGridView ID="grddeft" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grddeft_DataBinding" OnCustomCallback="grddeft_CustomCallback">
                            <Columns>
                                <dx1:GridViewDataTextColumn FieldName="Defecto" Caption="Defecto" Settings-AllowCellMerge="True"></dx1:GridViewDataTextColumn>
                                <dx1:GridViewDataTextColumn FieldName="Auditorias" Caption="Auditoria"></dx1:GridViewDataTextColumn>
                                <dx1:GridViewDataTextColumn FieldName="Total" Caption="Total"></dx1:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowFooter="true" />
                            <TotalSummary>
                                <dx1:ASPxSummaryItem FieldName="Total" SummaryType="Sum" ValueDisplayFormat="{0}" />
                            </TotalSummary>
                        </dx1:ASPxGridView>
                        <dx1:ASPxGridViewExporter ID="expdft" runat="server"></dx1:ASPxGridViewExporter>
                        <hr />
                        <asp:Button ID="exp" runat="server" CssClass="btn btn-success form-control" Text="Export to Excel" OnClick="exp_Click" />
                    </asp:Panel>
                </div>

            </div>

        </div>
    </div>

    <aside class="modal fade" id="ventanaG">
        <div class="modal-dialog">
            <div class="modal-content">
                <article class="modal-header">
                    <button id="cerrarG" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                    <h4>Ver Información </h4>
                </article>
                <article class="modal-body">
                    <asp:UpdatePanel ID="upd1" runat="server">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="panel3">
                                Start Date
                                    <dx1:ASPxDateEdit runat="server" ID="ASPxDateEdit1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                                End Date
                                    <dx1:ASPxDateEdit runat="server" ID="ASPxDateEdit2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                            </asp:Panel>
                            <hr />
                            <asp:Button ID="btninfo" runat="server" Text="Ver" CssClass="form-control btn btn-info" OnClick="btninfo_Click" />
                            <hr />
                            <asp:Button Text="Limpiar" CssClass="btn btn-primary form-control" ID="Button2" runat="server" OnClick="Button2_Click" />
                            <hr />

                            <dx1:ASPxGridView ID="grdpiezas" runat="server" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnDataBinding="grdpiezas_DataBinding" OnCustomCallback="grdpiezas_CustomCallback">
                                <Columns>
                                    <dx1:GridViewBandColumn Caption="Ver Informacion Bulto a Bulto" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx1:GridViewDataTextColumn FieldName="nombre" Caption="Nombre"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="POrder" Caption="Corte"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="Estatus" Caption="Estatus"></dx1:GridViewDataTextColumn>
                                        </Columns>
                                    </dx1:GridViewBandColumn>
                                </Columns>
                                <Settings ShowFooter="true" VerticalScrollBarMode="Visible" ShowGroupFooter="VisibleAlways" />
                                <SettingsPager PageSize="20"></SettingsPager>
                                <SettingsSearchPanel Visible="true" />
                            </dx1:ASPxGridView>
                            <hr />
                            <dx1:ASPxGridView ID="grdpiezas2" runat="server" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnDataBinding="grdpiezas2_DataBinding" OnCustomCallback="grdpiezas2_CustomCallback">
                                <Columns>
                                    <dx1:GridViewBandColumn Caption="Ver Informacion Embarque" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx1:GridViewDataTextColumn FieldName="nombreAuditor" Caption="Nombre"></dx1:GridViewDataTextColumn>
                                            <dx1:GridViewDataTextColumn FieldName="POrder" Caption="Corte"></dx1:GridViewDataTextColumn>
                                        </Columns>
                                    </dx1:GridViewBandColumn>
                                </Columns>
                                <Settings ShowFooter="true" VerticalScrollBarMode="Visible" ShowGroupFooter="VisibleAlways" />
                                <SettingsPager PageSize="20"></SettingsPager>
                                <SettingsSearchPanel Visible="true" />
                            </dx1:ASPxGridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </article>
            </div>
        </div>
    </aside>

</asp:Content>

