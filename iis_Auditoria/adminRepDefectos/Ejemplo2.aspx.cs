﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminRepDefectos_Ejemplo2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["Linea1"] = null;
            Session["Linea2"] = null;
            Session["Linea3"] = null;
            Session["Linea4"] = null;
            Session["LineaTOTAL"] = null; 

            txtfecha.Text = DateTime.Now.ToShortDateString();

            load_data();
         
           
        }
    }

    void load_data()
    {
        try
        {
            
            string query1 = " select descOperacion, isnull([1],0) as '09:00am', isnull([2],0) as '11:00am', isnull([3],0) as '01:00pm', isnull([4],0) as '03:00pm', isnull([5],0) as '04:50pm', isnull([6],0) as '06:50pm'"
                          + " ,(isnull([1], 0) + isnull([2],0) +isnull([3],0) +isnull([4],0) +isnull([5],0) +isnull([6],0)) Total"
                          + " from (select descOperacion, sum(BundleUni) as Total, bihorario from tbProduccionTickets where convert(date, fechaRegistro) = '" + txtfecha.Text + "' and lineaConfeccion = 1 group by descOperacion, bihorario) main1"
                          + " pivot(sum(Total) for bihorario in ([1],[2],[3],[4],[5],[6])) as Sec order by descOperacion";

            DataTable prod1 = DataAccess.Get_DataTable(query1);

            Session["Linea1"] = prod1;
            grdoper.DataSource = prod1;
            grdoper.DataBind();

            query1 = " select descOperacion, isnull([1],0) as '09:00am', isnull([2],0) as '11:00am', isnull([3],0) as '01:00pm', isnull([4],0) as '03:00pm', isnull([5],0) as '04:50pm', isnull([6],0) as '06:50pm'"
                          + " ,(isnull([1], 0) + isnull([2],0) +isnull([3],0) +isnull([4],0) +isnull([5],0) +isnull([6],0)) Total"
                          + " from (select descOperacion, sum(BundleUni) as Total, bihorario from tbProduccionTickets where  convert(date, fechaRegistro) = '" + txtfecha.Text + "' and lineaConfeccion = 2 group by descOperacion, bihorario) main1"
                          + " pivot(sum(Total) for bihorario in ([1],[2],[3],[4],[5],[6])) as Sec order by descOperacion";

            prod1 = DataAccess.Get_DataTable(query1);

            Session["Linea2"] = prod1;
            grdoper2.DataSource = prod1;
            grdoper2.DataBind();


            query1 = " select descOperacion, isnull([1],0) as '09:00am', isnull([2],0) as '11:00am', isnull([3],0) as '01:00pm', isnull([4],0) as '03:00pm', isnull([5],0) as '04:50pm', isnull([6],0) as '06:50pm'"
                         + " ,(isnull([1], 0) + isnull([2],0) +isnull([3],0) +isnull([4],0) +isnull([5],0) +isnull([6],0)) Total"
                         + " from (select descOperacion, sum(BundleUni) as Total, bihorario from tbProduccionTickets where convert(date, fechaRegistro) = '" + txtfecha.Text + "' and lineaConfeccion = 3 group by descOperacion, bihorario) main1"
                         + " pivot(sum(Total) for bihorario in ([1],[2],[3],[4],[5],[6])) as Sec order by descOperacion";

            prod1 = DataAccess.Get_DataTable(query1);

            Session["Linea3"] = prod1;
            grdoper3.DataSource = prod1;
            grdoper3.DataBind();

            query1 = " select descOperacion, isnull([1],0) as '09:00am', isnull([2],0) as '11:00am', isnull([3],0) as '01:00pm', isnull([4],0) as '03:00pm', isnull([5],0) as '04:50pm', isnull([6],0) as '06:50pm'"
                         + " ,(isnull([1], 0) + isnull([2],0) +isnull([3],0) +isnull([4],0) +isnull([5],0) +isnull([6],0)) Total"
                         + " from (select descOperacion, sum(BundleUni) as Total, bihorario from tbProduccionTickets where convert(date, fechaRegistro) = '" + txtfecha.Text + "' and lineaConfeccion = 4 group by descOperacion, bihorario) main1"
                         + " pivot(sum(Total) for bihorario in ([1],[2],[3],[4],[5],[6])) as Sec order by descOperacion";

            prod1 = DataAccess.Get_DataTable(query1);

            Session["Linea4"] = prod1;
            grdoper4.DataSource = prod1;
            grdoper4.DataBind();


            query1 = " select descOperacion, isnull([1],0) as '09:00am', isnull([2],0) as '11:00am', isnull([3],0) as '01:00pm', isnull([4],0) as '03:00pm', isnull([5],0) as '04:50pm', isnull([6],0) as '06:50pm'"
                        + " ,(isnull([1], 0) + isnull([2],0) +isnull([3],0) +isnull([4],0) +isnull([5],0) +isnull([6],0)) Total"
                        + " from (select descOperacion, sum(BundleUni) as Total, bihorario from tbProduccionTickets where convert(date, fechaRegistro) = '" + txtfecha.Text + "'  group by descOperacion, bihorario) main1"
                        + " pivot(sum(Total) for bihorario in ([1],[2],[3],[4],[5],[6])) as Sec order by descOperacion";

            prod1 = DataAccess.Get_DataTable(query1);

            Session["LineaTOTAL"] = prod1;
            ASPxGridView1.DataSource = prod1;
            ASPxGridView1.DataBind();
        }
        catch (Exception)
        {

        }
    }

    protected void btnreporte_Click(object sender, EventArgs e)
    {        
            load_data();     
    }

    protected void grdoper_DataBinding(object sender, EventArgs e)
    {
        grdoper.DataSource = Session["Linea1"];
    }

    protected void grdoper_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdoper.DataBind();
    }

    protected void grdoper2_DataBinding(object sender, EventArgs e)
    {
        grdoper2.DataSource = Session["Linea2"];
    }

    protected void grdoper2_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdoper2.DataBind();
    }

    protected void grdoper3_DataBinding(object sender, EventArgs e)
    {
        grdoper3.DataSource = Session["Linea3"];
    }

    protected void grdoper3_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdoper3.DataBind();
    }

    protected void grdoper4_DataBinding(object sender, EventArgs e)
    {
        grdoper4.DataSource = Session["Linea4"];
    }

    protected void grdoper4_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdoper4.DataBind();
    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView1.DataSource = Session["LineaTOTAL"];
    }

    protected void ASPxGridView1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView1.DataBind();
    }
}