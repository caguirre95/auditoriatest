﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="SelectAudit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2 class="quitPadH">
        <asp:Label ID="lbl" runat="server" Text="Sistema de Auditoria"></asp:Label>
    </h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <style type="text/css">
        .box_effect {
            height: 250px;
            width: 180px;
            padding-top: 15px;
            /*-webkit-transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
            transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);*/
            /*color: darkgray !important;*/
            text-align: center;
            margin: auto;
            padding: 5px;
            /*border: 2px solid #eee;*/
            border: 1px solid rgba(0,0,0,0.2);
            /*-webkit-box-shadow: 7px 7px 7px rgba(0,0,0,0.2);
            -moz-box-shadow: 7px 7px 7px rgba(0,0,0,0.2);
            box-shadow: 7px 7px 7px rgba(0,0,0,0.2);
            -webkit-transition: all 0.5s ease-out;
            -moz-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;*/
        }

        /*.box_effect:hover {
                -webkit-transform: scale(1.25, 1.25);
                transform: scale(1.25, 1.25);
                background-color: #6E6E6E;
                /*color: white !important;*
                text-decoration: none;
                margin-top: 2px;
                -webkit-transform: rotate(-7deg);
                -moz-transform: rotate(-7deg);
                -o-transform: rotate(-7deg);
                z-index: 1000;
            }*/

        .te {
            text-decoration: none;
        }

        /*.im {
            z-index: -1;
        }*/

        .mt {
            margin-left: 15px;
            margin-top: 15px;
        }

        .mt:hover {
            z-index: -1;
        }

        .te:hover {
            color: white !important;
            text-decoration: none;
            outline-style: none;
        }
    </style>


    <div class="container">

        <div class="row" runat="server" id="MESS">
            <div class="col-lg-8 col-lg-offset-2">
                 <img src="img/ddd/Header-1-01-12.jpg" style="width:100%; margin-bottom:5px"  />
            </div>
        </div>
        <asp:HiddenField runat="server" ID="hdnidinspector" />
        <div class="row">
            
            <div class="col-lg-4" runat="server" id="ip">
                <div class="box_effect">

                    <asp:LinkButton ID="lnk" runat="server" CssClass="te" OnClick="lnk_Click">
                   
                    <div>                        
                        <img src="img/line_iinspection.png" alt="Inspection" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;color:rgba(0,0,0,0.6);">PROCESS INSPECTION</span>
                    </div>
                          
                    </asp:LinkButton>

                </div>
            </div>

            <div class="col-lg-4 " runat="server" id="bb">
                <div class="box_effect">
                    <asp:LinkButton ID="lnnkbundle" runat="server" CssClass="te" OnClick="lnnkbundle_Click">
                    <div>                        
                        <img class=" mt" src="img/canvas_Calidad.png" alt="Bundle by Bundle" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;color:rgba(0,0,0,0.6);">BUNDLE BY BUNDLE</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4" runat="server" id="ai">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkship" runat="server" CssClass="te" OnClick="lnkship_Click">
                    <div>                        
                        <img src="img/shippin_audit.png" alt="Shipping Audit" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;color:rgba(0,0,0,0.6);">INTERNAL AUDIT</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4" runat="server" id="ae">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="te" OnClick="LinkButton1_Click">
                    <div>                        
                        <img src="img/shippin_audit.png" alt="Shipping Audit" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;color:rgba(0,0,0,0.6);">EXTERNAL AUDIT</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4" runat="server" id="ac">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="te" OnClick="LinkButton2_Click">
                    <div>                        
                        <img src="img/shippin_audit.png" alt="Shipping Audit" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;color:rgba(0,0,0,0.6);">CUTTING AUDIT</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>

             <div class="col-lg-4" runat="server" id="aem">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton3" runat="server" CssClass="te" OnClick="LinkButton3_Click">
                    <div>                        
                        <img src="img/shippin_audit.png" alt="Shipping Audit" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;color:rgba(0,0,0,0.6);">SHIPPING AUDIT</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>

             <div class="col-lg-4" runat="server" id="af">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton4" runat="server" CssClass="te" OnClick="LinkButton4_Click">
                    <div>                        
                        <img src="img/shippin_audit.png" alt="Shipping Audit" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;color:rgba(0,0,0,0.6);">FINAL AUDIT</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>

             <div class="col-lg-4" runat="server" id="mn">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkmedidas" runat="server" CssClass="te" OnClick="lnkmedidas_Click">
                    <div>                        
                        <img src="img/Measurements.png" alt="measurement" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;color:rgba(0,0,0,0.6);">MEASUREMENT</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4" runat="server" id="emp">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkemp" runat="server" CssClass="te" OnClick="lnkemp_Click">
                    <div>                        
                        <img src="img/bulto_a_bulto.png" alt="bultoemp" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;color:rgba(0,0,0,0.6);">DEFECTOS EMPAQUE</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4" runat="server" id="insf">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkinspf" runat="server" CssClass="te" OnClick="lnkinspf_Click">
                    <div>                        
                        <img src="img/bundle_by_bundle.png" alt="bultoemp" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;color:rgba(0,0,0,0.6);">INSPECCION FINAL</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4" runat="server" id="aei">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkaein" runat="server" CssClass="te" OnClick="lnkaein_Click">
                    <div>                        
                        <img src="img/shippin_audit.png" alt="Embarque" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;color:rgba(0,0,0,0.6);">EMBARQUE INTEX</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>

        </div>
    </div>


</asp:Content>

