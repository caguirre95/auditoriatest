﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Correo.aspx.cs" Inherits="Correo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">

        function oki(a) {
            swal({
                title: 'Exito!',
                text: ""+a,
                type: 'success'
            });
        }
    </script>

    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/pagination.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="js/bootstrap.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container" style="margin-top: 20px">
        <div class="panel panel-default">
            <div class="panel-heading">Enviar Correo</div>
            <div class="panel-body">
                <div class="col-lg-12">
                    <asp:UpdatePanel ID="pnUpdateFile" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:PostBackTrigger ControlID="cmdAddFile" />
                        </Triggers>
                        <ContentTemplate>
                            <div class="col-lg-5">
                                <asp:FileUpload ID="fUpload" runat="server" CssClass="form-control" AllowMultiple="true"/>

                                <asp:Button ID="cmdAddFile"
                                    runat="server"
                                    Text="+"
                                    ToolTip="Añade el fichero a la lista"
                                    OnClick="cmdAddFile_Click" CssClass="form-control" Width="50px" />
                            </div>
                            <div class="col-lg-5">
                                <asp:ListBox ID="lstFiles" runat="server" CssClass="form-control" Width="300px" Height="35px"></asp:ListBox>

                                <asp:Button ID="cmdDelFile"
                                    runat="server"
                                    Text="-"
                                    ToolTip="Elimina el fichero seleccionado de la lista" CssClass="form-control" Width="50px" />
                            </div>
                            <div class="col-lg-2" style="margin: 0px; padding-left: 0px">
                                <asp:Button ID="btn" runat="server" Text="Enviar" CssClass="btn btn-info" OnClick="btn_Click" Width="200px" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                </div>

            </div>
        </div>
    </div>

</asp:Content>

