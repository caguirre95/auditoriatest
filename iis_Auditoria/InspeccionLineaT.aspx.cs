﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using SistemaAuditores.DataAccess;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using DevExpress.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;

public partial class InspeccionLineaT : System.Web.UI.Page
{
    int total = 0, totaldef = 0;
    double porcentaje = 0;

    /// <summary>
    /// evento 0 del formulario
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (User.Identity.IsAuthenticated == true)
            {
                string idOrder = (string)Request.QueryString["idOrder"];
                if (idOrder == "")
                {
                    Response.Redirect("Secciones.aspx");
                }
                Load_Data();
                cargaControl();
            }
        }

        // btnsave.Attributes.Add("onclick", " this.disabled = true; " + ClientScript.GetPostBackEventReference(btnsave, null) + ";");
        //Button1.Attributes.Add("onclick", " this.disabled = true; " + ClientScript.GetPostBackEventReference(Button1, null) + ";");
    }

    /// <summary>
    /// evento 1
    /// </summary>
    void cargaControl()
    {
        DataTable defectos_dt = new DataTable();
        defectos_dt = DataAccess.Get_DataTable("select idDefecto as idDefecto,Defecto as Defecto from tbDefectos WHERE Tipo='Sewing' and Cliente=92 order by Defecto  asc");

        GridViewdefectosC.DataSource = defectos_dt;
        GridViewdefectosC.DataBind();

        string Id_Linea = (string)Request.QueryString["id_linea"];

        DataTable linea_dt = new DataTable();
        linea_dt = DataAccess.Get_DataTable(" select id_linea,numero"
                                          + " from linea l join (select l.id_planta from Linea l join planta p on l.id_planta = p.id_planta where l.id_linea = "+ Id_Linea + ") as p"
                                          + " on l.id_planta = p.id_planta where l.estado = 'True' order by CONVERT(numeric, numero) asc");
            
        drplinea.DataSource = linea_dt;
        drplinea.DataTextField = "numero";
        drplinea.DataValueField = "id_linea";
        drplinea.DataBind();
    }

    /// <summary>
    /// envento 2
    /// </summary>
    public void Load_Data()
    {
        btnsave.Enabled = true;
        Button1.Enabled = true; //BOTON SALVAR BUNDLE REJECTED

        string Id_Order = (string)Request.QueryString["IdOrder"];
        lblcustomerval.Text = (string)Request.QueryString["customer"];
        lblstylesval.Text = (string)Request.QueryString["style"];
        int Id_Linea = Convert.ToInt16(Request.QueryString["id_linea"]);
        int Id_Seccion = Convert.ToInt16(Request.QueryString["id_seccion"]);
        string IdBundle = Request.QueryString["IdBundle"] ?? "0";


        lblfechaval.Text = DateTime.Now.ToShortDateString();

        if (Id_Order == "")
        {
            Response.Redirect("Secciones.aspx");
        }

        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
        string UserId = mu.ProviderUserKey.ToString();

        DataTable dt_Inspector = DataAccess.Get_DataTable("select nombre from Inspector where UserId='" + UserId + "'");
        string NombreInspector = Convert.ToString(dt_Inspector.Rows[0][0]);
        DataTable dt_Seccion = DataAccess.Get_DataTable("select m.Modulo from Seccion s join Modulo m on s.idModulo=m.id_modulo where s.id_seccion=" + Id_Seccion);
        string Seccion = Convert.ToString(dt_Seccion.Rows[0][0]);

        lblseccionval.Text = Seccion;
        lblinspectorval.Text = NombreInspector;

        DataTable dt_Linea = DataAccess.Get_DataTable("select l.numero,p.descripcion from Linea l join Planta p  on l.id_planta=p.id_planta where id_linea=" + Id_Linea);
        int NumeroLinea = Convert.ToInt16(dt_Linea.Rows[0][0]);
        lineaval.Text = Convert.ToString(dt_Linea.Rows[0][1]);

        //modificar X jefe de linea
        //DataTable dt_Supervisor = DataAccess.Get_DataTable("select distinct s.nombre from OperacionOperario op join OperOperSup ops on op.id_operarioOpe=ops.idOperacionOpe join Supervisor s on ops.idsupervisor=s.Id_Supervisor where op.id_seccion=" + Id_Seccion);
        //if (dt_Supervisor.Rows.Count > 0)
        //{
        //    lblsupervalue.Text = Convert.ToString(dt_Supervisor.Rows[0][0]);
        //}

        string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));
        DateTime fecha_actual = DateTime.Now;
        string sqlfecha_actual = fecha_actual.ToString("yyyy-MM-dd");

        int Id_Biohorario = 0;
        DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio, bihorario FROM Biohorario WHERE @input between rango1 and rango2");
        if (Bihorarios.Rows.Count > 0)
        {
            Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);
            lblbio.Text = "Biohorario " + Convert.ToString(Bihorarios.Rows[0][1]);
        }

        DataTable defectos_dt = new DataTable();
        defectos_dt = DataAccess.Get_DataTable("select id_defectos as idDefecto,nombre as Defecto from Defectos WHERE Categoria='C' and Cliente=92 order by nombre  asc");

        DefectosEncontradosC.DataSource = defectos_dt;
        DefectosEncontradosC.DataBind();


        DataTable gridInspecLinea = new DataTable();

        string query = " select row_number() OVER(ORDER BY Op.id_operario) AS id,Op.codigo AS Codigo, Op.nombre as Nombre, Oper.descripcion_completa as Operacion"
                      + " from OperacionOperario operOp join Operacion oper on operOp.id_operacion=oper.id_operacion join Operario op on operOp.id_operario=op.id_operario"
                      + " where operOp.id_seccion=" + Id_Seccion + " and op.activo='true'";

        gridInspecLinea = DataAccess.Get_DataTable(query);
        DataColumn Muestreo = new DataColumn("Muestreo", typeof(string));

        gridInspecLinea.Columns.Add(Muestreo);

        string Id_Bundle = Request.QueryString["IdBundle"];
        DataTable dt_Bundle = DataAccess.Get_DataTable("select Quantity from Bundle where Id_Bundle=" + Id_Bundle);
        int Cantidad_Bundle = Convert.ToInt16(dt_Bundle.Rows[0][0]);
        DataTable dt_Muestreo = DataAccess.Get_DataTable("declare @input float =" + Cantidad_Bundle + " SELECT sample_size FROM Muestreo WHERE @input between rango_incial and rango_final and nivel_insp = (Select levelaql from AQL)");
        int Muestra = Convert.ToInt16(dt_Muestreo.Rows[0][0]);

        for (int i = 0; i <= gridInspecLinea.Rows.Count - 1; i++)
        {
            gridInspecLinea.Rows[i][4] = Muestra;
        }

        DataColumn CantidadDefectos = new DataColumn("CantidadDefectos", typeof(string));
        gridInspecLinea.Columns.Add(CantidadDefectos);

        DataTable dt_bundlerejected = new DataTable();

        for (int i = 0; i <= gridInspecLinea.Rows.Count - 1; i++)
        {
            //for (int j = 0; j <= gridInspecLinea.Columns.Count - 1; j++)
            //{
            //    if (j == 5)
            //    {
            string querysum = "select sum(cantidad_defectos) as cantidad_defectos from BundleRejected where Id_Bundle=" + IdBundle + " and id_operarioOpe in ( select operop.id_operarioOpe"
                             + " from OperacionOperario operop"
                             + " join Operacion oper on operop.id_operacion = oper.id_operacion"
                             + " join Operario op on operop.id_operario = op.id_operario"
                             + " where op.codigo = '" + Convert.ToString(gridInspecLinea.Rows[i][1]) + "' and operop.id_seccion = " + Id_Seccion + " and oper.descripcion_completa = '" + Convert.ToString(gridInspecLinea.Rows[i][3]) + "') group by id_operarioOpe";
            dt_bundlerejected = DataAccess.Get_DataTable(querysum);
            if (dt_bundlerejected.Rows.Count > 0)
            {
                gridInspecLinea.Rows[i][5] = dt_bundlerejected.Rows[0][0];
            }
            else
            {
                gridInspecLinea.Rows[i][5] = 0;
            }
            //    }
            //}
        }

        grv_insp_linea.DataSource = gridInspecLinea;
        grv_insp_linea.DataBind();

    }

    /// <summary>
    /// evento 3
    /// </summary>
    protected void ASPxButton8_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        GridViewRow gv = (GridViewRow)(btn.NamingContainer);
        TextBox txtdefectos = (TextBox)gv.FindControl("txtdefectos");
        Label lbloper = (Label)gv.FindControl("lbloperario");
        Label lblcodigo = (Label)gv.FindControl("lblcodigo");
        Label lbldescripcion = (Label)gv.FindControl("lbloperacion");
        lbloperario.Text = lbloper.Text;
        lbldaterechazo.Text = DateTime.Now.ToShortDateString();
        lblhorarechazo.Text = DateTime.Now.ToShortTimeString();
        lbllinearechazo.Text = "L" + lineaval.Text + " " + lblseccionval.Text;
        hdnCodigo.Value = lblcodigo.Text;
        hdnOperacion.Value = lbldescripcion.Text;
        this.AsignarDefectos.ShowOnPageLoad = true;
    }

    /// <summary>
    /// evento 4 guarda registro rechazado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            guardaRechazo(DefectosEncontradosC, false);
        }
        catch (Exception)
        {

            throw;
        }
    }

    /// <summary>
    /// evento 4.. funcion guarda rechazo
    /// </summary>
    /// <param name="DefectosEncontrados"></param>
    /// <param name="ayuda"></param>
    void guardaRechazo(GridView DefectosEncontrados, bool ayuda)
    {
        btnsave.Enabled = false; //BOTON SALVAR BUNDLE REJECTED

        string IdBundle = Request.QueryString["IdBundle"].ToString();
        string IdOrder = Request.QueryString["IdOrder"].ToString();//no util
        int Id_Linea = Convert.ToInt16(Request.QueryString["id_linea"]);
        int Id_Seccion = Convert.ToInt16(Request.QueryString["id_seccion"]);

        string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

        int Id_Biohorario = 0;
        DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio, bihorario FROM Biohorario WHERE @input between rango1 and rango2");
        if (Bihorarios.Rows.Count > 0)
        {
            Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);
        }

        // VERIFICA SI EL OPERARIO ES DE LA LINEA
        int IdoperarioOP = 0;
        if (ayuda)
        {
            IdoperarioOP = int.Parse(hdfIdOperacionOp.Value);
        }
        else
        {
            DataTable idOperarioOpe = DataAccess.Get_DataTable(" select operop.id_operarioOpe"
                                                             + " from OperacionOperario operop"
                                                             + " join Operacion oper on operop.id_operacion = oper.id_operacion"
                                                             + " join Operario op on operop.id_operario = op.id_operario"
                                                             + " where op.codigo = '" + hdnCodigo.Value + "' and operop.id_seccion = "
                                                             + Id_Seccion + " and oper.descripcion_completa = '" + hdnOperacion.Value + "'");

            IdoperarioOP = Convert.ToInt16(idOperarioOpe.Rows[0][0]);
        }

        DataTable dt_Bundle = DataAccess.Get_DataTable("select Quantity from Bundle where Id_Bundle=" + IdBundle);
        int Cantidad_Bundle = Convert.ToInt16(dt_Bundle.Rows[0][0]);
        DataTable dt_IdMuestreo = DataAccess.Get_DataTable("declare @input float =" + Cantidad_Bundle + " SELECT id_muestreo FROM Muestreo WHERE @input between rango_incial and rango_final and nivel_insp = (Select levelaql from AQL)");
        int Id_Muestreo = Convert.ToInt16(dt_IdMuestreo.Rows[0][0]);

        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
        string userId = mu.ProviderUserKey.ToString();

        DataTable dt_Inspector = DataAccess.Get_DataTable("select si.idseccionInspector from  seccionInspector si  join Inspector i on si.idInspector=i.id_inspector where si.idseccion=" + Id_Seccion + " and i.UserId='" + userId + "'");
        int idseccionInspector = Convert.ToInt16(dt_Inspector.Rows[0][0]);

        for (int i = 0; i <= DefectosEncontrados.Rows.Count - 1; i++)
        {
            CheckBox check = (CheckBox)DefectosEncontrados.Rows[i].FindControl("chkdefectos");
            if (check.Checked == true)
            {
                Label lbliddef = (Label)DefectosEncontrados.Rows[i].FindControl("labelIddefecto");
                int IdAreaxDef = Convert.ToInt16(lbliddef.Text);
                TextBox txtcantidad = (TextBox)DefectosEncontrados.Rows[i].Cells[1].FindControl("txtcantidad");

                if (txtcantidad.Text != "")
                {
                    try
                    {
                        OrderDetailDA.SaveBundlesRejected(Convert.ToInt32(IdBundle), Convert.ToInt16(Id_Biohorario), IdAreaxDef, IdoperarioOP, idseccionInspector, Id_Muestreo, Convert.ToDateTime(lbldaterechazo.Text), Convert.ToString(lblhorarechazo.Text), Convert.ToInt16(txtcantidad.Text), ayuda);
                    }
                    catch (Exception ex)
                    {
                        string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
                        return;
                    }
                }
                else
                {
                    string message = "alert('Por favor ingrese la cantidad!');";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
                    return;
                }

            }
        }

        Load_Data();
        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
        this.AsignarDefectos.ShowOnPageLoad = false;
    }

    /// <summary>
    /// evento 4.2 guarda registro rechazado de ayuda
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSaveAyuda_Click(object sender, EventArgs e)
    {
        try
        {
            guardaRechazo(DefectosEncontradosC, true);
        }
        catch (Exception)
        {

            throw;
        }
    }

    /// <summary>
    /// evento 5 reparar bulto 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button1_Click(object sender, EventArgs e)
    {
        lbllineaval.Text = "L" + lineaval.Text + " " + lblseccionval.Text;
        Load_ControlBultos();
        this.RechazoBultos.ShowOnPageLoad = true;
    }

    /// <summary>
    /// evento 6 carga defectos asginados anteriormente
    /// </summary>
    public void Load_ControlBultos()
    {
        int Id_Seccion = Convert.ToInt16(Request.QueryString["id_seccion"]);
        string Id_Bundle = Request.QueryString["IdBundle"];
        string IdOrder = Request.QueryString["IdOrder"];


        string hora_reparado = DateTime.Now.ToShortTimeString();
        string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));
        DateTime fecha_actual = DateTime.Now;
        string fecha_recibido = fecha_actual.ToString("yyyy-MM-dd");

        string query1 = " select row_number() OVER (ORDER BY Ope.nombre) AS id, B.Bld as Bulto, p.POrder as NumeroCorte,"
                      + " (CONVERT(VARCHAR, BR.FechaRecibido, 103) + ' ' + BR.HoraRecibido) as FechaRecibido,"
                      + " def.Iddefecto as Id_defectos, ope.nombre as Nombre, br.cantidad_defectos as Defectos"
                      + " from BundleRejected br"
                      + " join Bundle b on br.Id_Bundle = b.Id_Bundle"
                      + " join POrder p on b.Id_Order = p.Id_Order"
                      + " join OperacionOperario op on br.id_operarioOpe = op.id_operarioOpe"
                      + " join Operario ope on op.id_operario = ope.id_operario"
                      + " join tbDefectos def on br.Id_DefxArea = def.idDefecto"
                      + " where br.Id_Bundle = " + Convert.ToString(Id_Bundle) + "  and op.id_seccion = " + Id_Seccion;

        DataTable dt_rechazos = DataAccess.Get_DataTable(query1);

        lblDate.Text = fecha_recibido + " " + hora_reparado;

        if (dt_rechazos.Rows.Count > 0)
        {
            lblbundleid.Text = Convert.ToString(dt_rechazos.Rows[0][1]);
            lblcorte.Text = Convert.ToString(dt_rechazos.Rows[0][2]);
            lblmensaje.Visible = false;
            Button1.Visible = true;
            ControlBultos.Visible = true;
            ControlBultos.DataSource = dt_rechazos;
            ControlBultos.DataBind();
        }
        else
        {
            Button1.Visible = false;
            ControlBultos.Visible = false;
            lblmensaje.Visible = true;
            lblmensaje.Text = "No se encontraron Bultos Rechazados!";
            return;
        }

        for (int i = 0; i <= ControlBultos.Rows.Count - 1; i++)
        {
            //for (int j = 0; j <= ControlBultos.Columns.Count - 1; j++)
            //{
            //    if (j == 3)
            //    {
            DropDownList ddlDefectos = (DropDownList)ControlBultos.Rows[i].Cells[3].FindControl("ddlDefectos");
            ddlDefectos.SelectedValue = Convert.ToString(dt_rechazos.Rows[i].ItemArray[4]);
            //    }

            //}
        }

    }

    /// <summary>
    /// evento 7 Guarda Bulto reparado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnsave1_Click(object sender, EventArgs e)
    {
        Button1.Enabled = false;
        DateTime fecha_actual = DateTime.Now;
        string fecha_recibido = fecha_actual.ToString("yyyy-MM-dd");
        string sqlfecha_actual = fecha_recibido;

        string IdBundle = Request.QueryString["IdBundle"].ToString();
        string IdOrder = Request.QueryString["IdOrder"].ToString();
        int Id_Linea = Convert.ToInt16(Request.QueryString["id_linea"]);
        int Id_Seccion = Convert.ToInt16(Request.QueryString["id_seccion"]);

        int Id_Biohorario = 0;
        string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

        DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio, bihorario FROM Biohorario WHERE @input between rango1 and rango2");
        if (Bihorarios.Rows.Count > 0)
        {
            Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);
            lblbio.Text = "Biohorario " + Convert.ToString(Bihorarios.Rows[0][1]);
        }

        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
        string userId = mu.ProviderUserKey.ToString();

        DataTable dt_Bundle = DataAccess.Get_DataTable("select Quantity from Bundle where Id_Bundle=" + IdBundle);
        int Cantidad_Bundle = Convert.ToInt16(dt_Bundle.Rows[0][0]);
        DataTable dt_IdMuestreo = DataAccess.Get_DataTable("declare @input float =" + Cantidad_Bundle + " SELECT id_muestreo FROM Muestreo WHERE @input between rango_incial and rango_final and nivel_insp = (Select levelaql from AQL)");
        int Id_Muestreo = Convert.ToInt16(dt_IdMuestreo.Rows[0][0]);

        for (int i = 0; i <= ControlBultos.Rows.Count - 1; i++)
        {

            try
            {
                OrderDetailDA.SaveBundlesRepair(Convert.ToInt32(IdBundle), Id_Biohorario, Id_Seccion, userId, Id_Muestreo, Convert.ToDateTime(fecha_recibido), Convert.ToString(date_now));
                Response.Redirect("BundlesTrasero.aspx?IdBundle=" + Convert.ToInt32(IdBundle) + "&id_order=" + Convert.ToInt32(IdOrder) + "&id_seccion=" + Id_Seccion + "&id_linea=" + Id_Linea);
                this.RechazoBultos.ShowOnPageLoad = false;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
            }

        }


    }

    /// <summary>
    /// redireccona a una nueva buskeda
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkNew_Click(object sender, EventArgs e)
    {
        int Id_Seccion = Convert.ToInt16(Request.QueryString["id_seccion"]);
        int Id_Linea = Convert.ToInt16(Request.QueryString["id_linea"]);
        Response.Redirect("SearchIL.aspx?idsec=" + Id_Seccion + "&id_linea=" + Id_Linea);
    }

    #region no usadas
    protected void DefectosEncontrados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DefectosEncontradosC.PageIndex = e.NewPageIndex;
        Load_Data();
    }

    protected void grv_insp_linea_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblqy = (Label)e.Row.FindControl("lblmuestreo");
            int qty = Int32.Parse(lblqy.Text);
            total = total + qty;

            TextBox lblqdefect = (TextBox)e.Row.FindControl("txtdefectos");
            int qty1 = Int32.Parse(lblqdefect.Text);
            totaldef = qty1 + totaldef;

            porcentaje = Convert.ToDouble(totaldef) / Convert.ToDouble(total);
            porcentaje = porcentaje * 100;
        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotalqty = (Label)e.Row.FindControl("lblTotalqty");
            lblTotalqty.Text = total.ToString();

            Label lblTotalDef = (Label)e.Row.FindControl("lblTotaldefect");
            lblTotalDef.Text = totaldef.ToString();

            Label lblTPorDef = (Label)e.Row.FindControl("lblTPorDef");
            porcentaje = System.Math.Round(porcentaje, 2);
            lblTPorDef.Text = Convert.ToString(porcentaje) + "%";
        }
    }

    #endregion

    /// <summary>
    /// redirecciona a la hoja de corte
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        string id_order = Request.QueryString["IdOrder"].ToString();
        string id_seccion = Request.QueryString["id_seccion"].ToString();
        string id_linea = Request.QueryString["id_linea"].ToString();
        Response.Redirect("BundlesTrasero.aspx?id_order=" + id_order + "&id_seccion=" + id_seccion + "&id_linea=" + id_linea);
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        string id_order = Request.QueryString["IdOrder"].ToString();
        string id_seccion = Request.QueryString["id_seccion"].ToString();
        string id_linea = Request.QueryString["id_linea"].ToString();
        Response.Redirect("BundlesTrasero.aspx?id_order=" + id_order + "&id_seccion=" + id_seccion + "&id_linea=" + id_linea);
    }


    /// <summary>
    /// guarda defectos no asignados a operarios
    /// </summary>
    /// <param name="cantidad"></param>
    /// <param name="idarea"></param>
    /// <param name="iddefecto"></param>
    /// <param name="idbulto"></param>
    /// <param name="idseccion"></param>
    /// <param name="idbiho"></param>
    /// <returns></returns>
    [WebMethod]
    public static string GuardarRechazo(int cantidad, int idarea, int iddefecto, int idbulto,int idseccion,int idbiho)
    {
        try
        {

            int i = 0;
            if (idbulto != 0)
            {
                string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

                int Id_Biohorario = 0;
                DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio, bihorario FROM Biohorario WHERE @input between rango1 and rango2");
                if (Bihorarios.Rows.Count > 0)
                {
                    Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);
                }

                MembershipUser mu = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                string userId = mu.ProviderUserKey.ToString();

                DataTable dt_Inspector = DataAccess.Get_DataTable("select si.idseccionInspector from  seccionInspector si  join Inspector i on si.idInspector=i.id_inspector where si.idseccion=" + idseccion + " and i.UserId='" + userId + "'");
                int idseccionInspector = Convert.ToInt16(dt_Inspector.Rows[0][0]);

                //OrderDetailDA.SaveBundlesRejected(idbulto, Convert.ToInt16(Id_Biohorario), IdAreaxDef, IdoperarioOP, idseccionInspector, Id_Muestreo, Convert.ToDateTime(lbldaterechazo.Text), Convert.ToString(lblhorarechazo.Text), Convert.ToInt16(txtcantidad.Text), ayuda);

                string queryUpdate = "insert AreaDefectoRechazo (idbundle,idbio,iddefecto,idarea,idseccionInsp,fechaRecibido,Cantidad,idseccion) values(" + idbulto + "," + Id_Biohorario + "," + iddefecto + ","
                                      + idarea + "," + idseccionInspector + ",'" + DateTime.Now.ToString() + "'," + cantidad + ","+ idseccion +")";

                SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

                cn.Open();
                SqlCommand cmd = new SqlCommand(queryUpdate, cn);
                i = cmd.ExecuteNonQuery();
                cn.Close();

            }

            if (i == 0)
            {
                return "false";
            }

            return "true";
        }
        catch (Exception)
        {
            return "false";
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ardef> getarea(string pre)
    {
        try
        {
            List<ardef> list = new List<ardef>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top(10) a.Id_Area,a.Nombre from AreaDefecto a where a.Nombre like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ardef obj = new ardef();

                obj.Id_Area = int.Parse(dt.Rows[i][0].ToString());
                obj.Nombre = dt.Rows[i][1].ToString();
               
                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ardef> getdefecto(string pre)
    {
        try
        {
            List<ardef> list = new List<ardef>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
           
            SqlCommand cmd = new SqlCommand("select top(10) d.id_defectos,d.nombre from Defectos d where d.nombre like '%" + pre + "%' and d.Categoria='T' order by d.nombre asc", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ardef obj = new ardef();

                obj.Id_Defecto = int.Parse(dt.Rows[i][0].ToString());
                obj.Nombre = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class ardef
    {
        public int Id_Area { get; set; }
        public string Nombre { get; set; }
        public int Id_Defecto { get; set; }
    }




}