﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InfoLinea : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.User.Identity.IsAuthenticated == true)
        {
            if (Page.User.IsInRole("PackList") || Page.User.IsInRole("PackAdmon"))
            {

                if (!IsPostBack)
                {
                    hdnusuario.Value = Page.User.Identity.Name.ToString();
					
					string query = " select Estado from tbBloqueo where UserName = 'adminsnr' ";

                    DataTable dt = DataAccess.Get_DataTable(query);

                    string est = dt.Rows[0][0].ToString();

                    if (est == "False")
                    {
                        FormsAuthentication.SignOut();
                        Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
                    }
                }
				
				if (txtPorder.Text != string.Empty)
                    Ver();
               
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }
        else
        {
            Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre, string user)
    { 
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select top 10 p.Id_Order,p.POrder,s.Style" 
                                           +" from POrder p join Style s on p.Id_Style = s.Id_Style"
                                           +" where p.POrder like '%"+ pre +"%' and p.Id_Linea in"
                                           +" (select e.idLinea from tbluserenvio u join tblUserEnvioLinea e on u.idUserEnvio = e.idUserEnvio where u.nameMemberShip = '"+ user +"')", cn);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][1].ToString();
                obj.style = dt.Rows[i][2].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public int idorder { get; set; }
        public string porder { get; set; }
        public string style { get; set; }
    }

    public class p
    {
        public string nom1 { get; set; }
        public string nom2 { get; set; }
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            if (hfidPorder.Value != string.Empty)
            {
                int idporder = int.Parse(hfidPorder.Value);
                string query = "select b.Nseq Seq,b.Id_Bundle,b.Size,b.Quantity,isnull(eb.acumulado, 0) as Acumulado,isnull(eb.acumulado, 0) - b.Quantity as Deficit from Bundle b left join tblEnviosbulto eb on b.Id_Bundle = eb.idbulto where b.Id_Order =" + idporder + "  and isnull(eb.acumulado,0)<b.Quantity" +
                                   " and isnull(eb.liberado,'false') != 'true' order by b.nseq asc";
                DataTable dt = DataAccess.Get_DataTable(query);

                gridbultos.DataSource = dt;
                gridbultos.DataBind();

                string queryBundle = "Select b.Size,sum(b.Quantity) as Quantity from POrder p join Bundle b on p.Id_Order = b.Id_Order where p.Id_Order = " + hfidPorder.Value + " group by b.Size order by b.Size asc";
                DataTable dt_Bundle = DataAccess.Get_DataTable(queryBundle);

                List<p> l = new List<p>();
                List<p> l1 = new List<p>();
                for (int i = 0; i < dt_Bundle.Rows.Count; i++)
                {
                    p obj = new p();
                    obj.nom2 = i.ToString();
                    obj.nom1 = dt_Bundle.Rows[i][0].ToString();
                    l.Add(obj);
                }

                DropDownList2.DataSource = l;
                DropDownList2.DataTextField = "nom1";
                DropDownList2.DataValueField = "nom2";
                DropDownList2.DataBind();
                DropDownList2.Items.Insert(0, "Select...");
            }
        }
        catch (Exception ex)
        {
            Console.Write(ex);
        }

    }

    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (DropDownList2.SelectedItem.Text == "Select...")
            {
                return;
            }
            if (DropDownList2.Items.Count > 0 && !string.IsNullOrEmpty(hfidPorder.Value))
            {
                int idporder = int.Parse(hfidPorder.Value);
                string size = DropDownList2.SelectedItem.Text;

                string queryBundle = "select b.Nseq Seq,b.Id_Bundle,b.Size,b.Quantity,isnull(eb.acumulado, 0) as Acumulado,isnull(eb.acumulado, 0) - b.Quantity as Deficit from Bundle b left join tblEnviosbulto eb on b.Id_Bundle = eb.idbulto where b.Id_Order =" + idporder + "  and b.size='" + size + "' and isnull(eb.acumulado,0)<b.Quantity" +
                                     " and isnull(eb.liberado,'false') != 'true'";
                DataTable dt = DataAccess.Get_DataTable(queryBundle);

                gridbultos.DataSource = dt;
                gridbultos.DataBind();


            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnguardar_Click(object sender, EventArgs e)
    {
        try
        {

            int cont = 0;
            int cont2 = 0;
                       
            foreach (GridViewRow item in gridbultos.Rows)
            {
                string resp = "0";
                var idbulto = (Label)item.FindControl("lblidbulto");
                var size = (Label)item.FindControl("lblsize");
                var quantity = (Label)item.FindControl("lblquantity");
                var unidades = (TextBox)item.FindControl("txtunidades");
                var acumulado = (Label)item.FindControl("lblacumulado");
                var deficit = (Label)item.FindControl("txtdeficit");
                // var chk = (CheckBox)item.FindControl("checkEnvio");

                if (unidades.Text != string.Empty)
                {
                    cont2++;
                    if (Context.User.IsInRole("PackAdmon"))
                    {
                        resp = OrderDetailDA.saveEnviosbultos(int.Parse(idbulto.Text), int.Parse(quantity.Text), int.Parse(unidades.Text), size.Text, Context.User.Identity.Name);
                    }
                    else if (Context.User.IsInRole("PackList"))
                    {
                       
                        resp = OrderDetailDA.saveEnviosbultos(int.Parse(idbulto.Text), int.Parse(quantity.Text), int.Parse(unidades.Text), size.Text, Context.User.Identity.Name);
                       
                    }

                    if (int.Parse(resp) > 0)
                    {
                        cont++;
                    }
                }


            }

            // }

            if (cont == cont2)
            {
                gridbultos.DataSource = null;
                gridbultos.DataBind();

                if (cont2 > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }

            }
            else
            {
                if (cont == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
                }

            }

        }
        catch (Exception ex)
        {
            string r = ex.Message;
            throw;
        }
    }

    protected void txtunidades_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox txtunit = (TextBox)sender;
            GridViewRow row = (GridViewRow)txtunit.NamingContainer;
            int pos = row.RowIndex;


            Label lblacu = (Label)row.FindControl("lblacumulado");
            Label txtdef = (Label)row.FindControl("txtdeficit");
            TextBox txtunitp = (TextBox)row.FindControl("txtunidades");
            Label lblquan = (Label)row.FindControl("lblquantity");

            if (string.IsNullOrEmpty(txtunit.Text))
            {
                return;
            }

            int acu = int.Parse(lblacu.Text);
            int def = int.Parse(txtdef.Text);
            int unit = int.Parse(txtunit.Text);
            int quan = int.Parse(lblquan.Text);


            if (acu + unit > quan)
            {
                lblacu.Text = quan.ToString();
                txtunitp.Text = (quan - acu).ToString();
                txtdef.Text = "0";

                txtdef.CssClass = "label label-success";

                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertexeso();", true);
            }
            else
            {
                var d = (acu + unit);
                txtdef.Text = (d - quan).ToString();
                lblacu.Text = d.ToString();

                if (d - quan == 0)
                {
                    txtdef.CssClass = "label label-success";
                    //tdef.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    txtdef.CssClass = "label label-danger";
                    // txtdef.ForeColor = System.Drawing.Color.Red;
                }
            }

            if ((pos + 1) < gridbultos.Rows.Count)
            {
                TextBox txtunitNext = (TextBox)gridbultos.Rows[pos + 1].FindControl("txtunidades");
                txtunitNext.Focus();

            }

            txtunitp.Enabled = false;


        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void gridbultos_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblacu = (Label)e.Row.FindControl("lblacumulado");
            TextBox txtunitp = (TextBox)e.Row.FindControl("txtunidades");


            Label txtdef = (Label)e.Row.FindControl("txtdeficit");
            Label lblquan = (Label)e.Row.FindControl("lblquantity");

            int acu = int.Parse(lblacu.Text);
            int unit = txtunitp.Text == "" ? 0 : int.Parse(txtunitp.Text);
            int quan = int.Parse(lblquan.Text);

            var d = (acu + unit);

            if (d - quan == 0)
            {
                txtdef.CssClass = "label label-success";
            }
            else
            {
                txtdef.CssClass = "label label-danger";
            }

            txtunitp.Enabled = true;

        }
    }



    protected void gridbultos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "reiniciar")
            {
                if (DropDownList2.Items.Count > 0 && !string.IsNullOrEmpty(hfidPorder.Value))
                {
                    int idporder = int.Parse(hfidPorder.Value);
                    string size = DropDownList2.SelectedItem.Text;

               
                    string queryBundle = "select b.Nseq Seq,b.Id_Bundle,b.Size,b.Quantity,isnull(eb.acumulado, 0) as Acumulado,isnull(eb.acumulado, 0) - b.Quantity as Deficit from Bundle b left join tblEnviosbulto eb on b.Id_Bundle = eb.idbulto where b.Id_Order =" + idporder + "  and isnull(eb.acumulado,0)<b.Quantity" +
                                   " and isnull(eb.liberado,'false') != 'true' order by b.nseq asc";

                    DataTable dt = DataAccess.Get_DataTable(queryBundle);

                    gridbultos.DataSource = dt;
                    gridbultos.DataBind();


                }

            }
            if (e.CommandName == "agregar")
            {
                GridViewRow item = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int indice = item.RowIndex;
                string idbulto = gridbultos.DataKeys[indice].Value.ToString();

              
                string resp = "0";

                var size = (Label)item.FindControl("lblsize");
                var quantity = (Label)item.FindControl("lblquantity");
                var unidades = (TextBox)item.FindControl("txtunidades");
                var acumulado = (Label)item.FindControl("lblacumulado");
                var deficit = (Label)item.FindControl("txtdeficit");

                if (unidades.Text != string.Empty)
                {
                    resp = OrderDetailDA.saveEnviosbultos(int.Parse(idbulto), int.Parse(quantity.Text), int.Parse(unidades.Text), size.Text, Context.User.Identity.Name);
                }

                if (int.Parse(resp) > 0)
                {
                    if (hfidPorder.Value != string.Empty)
                    {
                        int idporder = int.Parse(hfidPorder.Value);
                       
                        string query = "select b.Nseq Seq,b.Id_Bundle,b.Size,b.Quantity,isnull(eb.acumulado, 0) as Acumulado,isnull(eb.acumulado, 0) - b.Quantity as Deficit from Bundle b left join tblEnviosbulto eb on b.Id_Bundle = eb.idbulto where b.Id_Order =" + idporder + "  and isnull(eb.acumulado,0)<b.Quantity" +
                                 " and isnull(eb.liberado,'false') != 'true' order by b.nseq asc";
                        DataTable dt = DataAccess.Get_DataTable(query);

                        gridbultos.DataSource = dt;
                        gridbultos.DataBind();
                    }

                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
                }

            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnvertodos_Click(object sender, EventArgs e)
    {

        try
        {
            if (hfidPorder.Value != string.Empty)
            {
                int idporder = int.Parse(hfidPorder.Value);
                string query = "select b.Nseq Seq, b.Id_Bundle,b.Size,b.Quantity,isnull(eb.acumulado, 0) as Acumulado,isnull(eb.acumulado, 0) - b.Quantity as Deficit from Bundle b left join tblEnviosbulto eb on b.Id_Bundle = eb.idbulto where b.Id_Order =" + idporder + "  and isnull(eb.acumulado,0)<b.Quantity" +
                                   " and isnull(eb.liberado,'false') != 'true' order by b.NSeq asc";
                DataTable dt = DataAccess.Get_DataTable(query);

                gridbultos.DataSource = dt;
                gridbultos.DataBind();
            }
        }
        catch (Exception)
        {

        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("Infolinea.aspx");
    }
	
	void Limpiar()
    {
        txtunidades.Text = "";
        txtcoment.Text = "";
    }    

    protected void Button3_Click(object sender, EventArgs e)
    {
        try
        {
            //spdGuardarUnidadesIrregulares
            if (hfidPorder.Value != string.Empty)
            {
                int idporder = int.Parse(hfidPorder.Value);

                string ok = OrderDetailDA.guardarirregulares(idporder, Convert.ToInt16(txtunidades.Text), txtcoment.Text, Context.User.Identity.Name);

                if (ok.Equals("1"))
                {
                    string query = "select isnull(SUM(ii.cantidadIrregulares),0) Total from IngresoIrregulares ii join POrder p on p.Id_Order = ii.idCorte where p.Id_Order = " + hfidPorder.Value + " group by p.Id_Order, p.POrder";

                    DataTable dt = DataAccess.Get_DataTable(query);

                    string a = dt.Rows[0][0].ToString();

                    Limpiar();
					
					Ver();

                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitosoirreg('" + a + "','" + txtPorder.Text + "');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertError();", true);
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }
	
	void Ver()
    {
        string a;

        string query = " select isnull(SUM(ii.cantidadIrregulares),0) Total from IngresoIrregulares ii join POrder p on p.Id_Order = ii.idCorte" +
                       " where p.Id_Order = " + hfidPorder.Value + " group by p.Id_Order, p.POrder";

        DataTable dt = DataAccess.Get_DataTable(query);

        if (dt.Rows.Count > 0)
        {
            a = dt.Rows[0][0].ToString();
        }
        else
        {
            a = "0";
        }

        txt2.Text = "El corte " + txtPorder.Text + " tiene " + a + " unidades irregulares ingresadas";

        //lbl1.Text = "3";
    }


}