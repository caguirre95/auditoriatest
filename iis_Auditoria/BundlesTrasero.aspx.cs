﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using SistemaAuditores.DataAccess;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Net.Mail;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (User.Identity.IsAuthenticated == true)
            {
                string Id_Order, Id_Seccion,linea;

                Id_Order = (string)Request.QueryString["id_order"] ?? "NA";
                Id_Seccion = (string)Request.QueryString["id_seccion"] ?? "NA";
                linea = (string)Request.QueryString["id_linea"];

                if (Id_Order == "")
                    Id_Order = "0";

                Load_Data(Id_Order, Id_Seccion);

              //  Load_Grid_Asistencia(int.Parse(linea),int.Parse(Id_Seccion));          

            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    public void Load_Data(string Id_Order, string Id_Seccion)
    {

        DataTable dt = new DataTable();
        dt = OrderDetailDA.Get_OrderbyId(Id_Order);

        if (dt.Rows.Count > 0)
        {
            lblorder1.Text = Convert.ToString(dt.Rows[0][1]); //extraemos numero de Porder
            lblorder2.Text = lblorder1.Text;
            lblcustomer.Text = dt.Rows[0][5].ToString();

            txtporder.Text = dt.Rows[0][1].ToString();
            txtstyle.Text = dt.Rows[0][6].ToString();
            txtdescrip.Text = dt.Rows[0][2].ToString();
            txtcantidad.Text = dt.Rows[0][3].ToString();
            txtbultos.Text = dt.Rows[0][4].ToString();
        }
        else
        {
            lblnoRows.Text = "No se encontraron registros para esta busqueda!";
            return;
        }

        MembershipUser user = Membership.GetUser(Page.User.Identity.Name);
        string UserId = user.ProviderUserKey.ToString();

        DataTable dt_Inspector = DataAccess.Get_DataTable("select si.idseccionInspector from seccionInspector si join Inspector i on si.idInspector=i.id_inspector where i.UserId='" + UserId + "' and si.idseccion=" + Id_Seccion);
        string id_inspector = Convert.ToString(dt_Inspector.Rows[0][0]);

        DataTable dt_ODetails = new DataTable();
        dt_ODetails = OrderDetailDA.Get_OrderDetailbyIdOrder(Convert.ToInt32(Id_Order), Convert.ToInt16(Id_Seccion),id_inspector);//pendiente revisar despues p

        DataColumn Muestreo = new DataColumn("Muestreo", typeof(string));
        dt_ODetails.Columns.Add(Muestreo);

        for (int i = 0; i <= dt_ODetails.Rows.Count - 1; i++)
        {
           dt_ODetails.Rows[i][7] = dt_ODetails.Rows[i][6];// dt_IdMuestreo.Rows[0][0];
        }

        GridBultos.DataSource = dt_ODetails;
        GridBultos.DataBind();

        string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

        DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio, bihorario FROM Biohorario WHERE @input between rango1 and rango2");
        if (Bihorarios.Rows.Count > 0)
        {
            lblidbio.Text = Convert.ToString(Bihorarios.Rows[0][0]);
            lblbio.Text = "Biohorario " + Convert.ToString(Bihorarios.Rows[0][1]);
        }

    }

    protected void GridBultos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label idbulto = (Label)e.Row.FindControl("lblidbundle");
                string  Id_Seccion = (string)Request.QueryString["id_seccion"];

                string query = "Select * from BundleRejected br join OperacionOperario op on br.id_operarioOpe = op.id_operarioOpe  where Id_Bundle = " + idbulto.Text + " and op.id_seccion =" + Id_Seccion;
                DataTable bundle_rejected = DataAccess.Get_DataTable(query);

                query = "select idAreaDefectoRec from AreaDefectoRechazo where idbundle="+ idbulto.Text +" and idseccion=" + Id_Seccion;
                DataTable bundle_rejectedDefecto = DataAccess.Get_DataTable(query);

                LinkButton lk = (LinkButton)e.Row.FindControl("comando");
                LinkButton lk1 = (LinkButton)e.Row.FindControl("comandoRep");

                if (bundle_rejected.Rows.Count > 0 || bundle_rejectedDefecto.Rows.Count>0)
                {
                    RadioButtonList rbl_aprob1 = (RadioButtonList)e.Row.FindControl("RadioButtonList1");
                    rbl_aprob1.SelectedValue = "0";//Rechazado
                    lk.CssClass = "btn btn-danger";
                    lk1.Visible = true;
                }
                else
                {
                    lk1.Visible = false;
                    lk.CssClass = "btn btn-default";             
                }

            }
        }
        catch (Exception ex)
        {
            Console.Write(ex.Message);

        }

    }

    protected void btnGuardarTodos_Click(object sender, EventArgs e)
    {
        try
        {
            string Id_Order = (string)Request.QueryString["id_order"];
            int Id_Seccion = Convert.ToInt32(Request.QueryString["id_seccion"]);
            int Id_Linea = Convert.ToInt16(Request.QueryString["id_linea"]);

            if (Id_Order == "")
            {
                Response.Redirect("Secciones.aspx");
            }

            MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
            string userId = mu.ProviderUserKey.ToString();

            foreach (GridViewRow Row in GridBultos.Rows)
            {
                RadioButtonList rd = (RadioButtonList)Row.FindControl("RadioButtonList1");

                if (rd.SelectedValue!="0")
                {
                    Label Id_OrderDetail = (Label)Row.FindControl("lblidbundle");
                    int Id_Bundle = Convert.ToInt32(Id_OrderDetail.Text);
                    string lblQuantity = Row.Cells[5].Text;

                    string query1 = "Select * from BundleRejected br join OperacionOperario op on br.id_operarioOpe = op.id_operarioOpe  where Id_Bundle = " + Id_Bundle + " and op.id_seccion =" + Id_Seccion;
                    DataTable dt_rechazos = DataAccess.Get_DataTable(query1);

                    string query = "select idAreaDefectoRec from AreaDefectoRechazo where idbundle=" + Id_Bundle + " and idseccion=" + Id_Seccion;
                    DataTable bundle_rejectedDefecto = DataAccess.Get_DataTable(query);

                    if (dt_rechazos.Rows.Count > 0 || bundle_rejectedDefecto.Rows.Count > 0)
                    {
                        // ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "bultomalo();", true);
                    }
                    else
                    {
                        DateTime fechahora_registro = DateTime.Now;//FECHA DE REGISTRO
                        string hora_registro = DateTime.Now.ToShortTimeString();//HORA DE REGISTRO
                        int cantidad_defecto = 0;// CANTIDAD DE DEFECTOS

                        string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

                        DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio FROM Biohorario WHERE @input between rango1 and rango2");
                        int Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);

                        OrderDetailDA.SaveBundlesApproved(Id_Bundle, Id_Biohorario, Id_Seccion, userId, int.Parse(lblQuantity), Convert.ToDateTime(fechahora_registro), Convert.ToString(hora_registro), Convert.ToInt16(cantidad_defecto));
                    }

                }


            }

            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
            Load_Data(Id_Order,Id_Seccion.ToString());
        }
        catch (Exception)
        {
            string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);


        }

    }

    protected void GridBultos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string Id_Order = (string)Request.QueryString["id_order"] ?? "NA";
            int Id_Seccion = Convert.ToInt32(Request.QueryString["id_seccion"]);
            int Id_Linea = Convert.ToInt16(Request.QueryString["id_linea"]);

            if (Id_Order == "")
            {
                Response.Redirect("Secciones.aspx");
            }

            
            if (e.CommandName == "Editar")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int indice = row.RowIndex;
                string Id_Bundle = GridBultos.DataKeys[indice].Value.ToString();

                RadioButtonList rbl_aprob = (RadioButtonList)row.FindControl("RadioButtonList1");
                if (rbl_aprob.SelectedValue=="")
                {
                    return;
                }
                int status = Convert.ToInt32(rbl_aprob.SelectedValue);

                if (status == 0)
                {
                    Response.Redirect("InspeccionLineaT.aspx?IdBundle=" + Id_Bundle + "&IdOrder=" + Id_Order + "&id_seccion=" + Id_Seccion + "&id_linea=" + Id_Linea+"&style=" + txtstyle.Text + "&customer=" + lblcustomer.Text, false);
                }
                else
                {
                    try
                    {
                        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
                        string userId = mu.ProviderUserKey.ToString();

                        string quanti = row.Cells[5].Text;
                        DataTable dt_Muestreo = DataAccess.Get_DataTable("declare @input float =" + quanti + " SELECT * FROM Muestreo WHERE @input between rango_incial and rango_final and nivel_insp = (Select levelaql from AQL)");
                        int Id_Muestreo = Convert.ToInt16(dt_Muestreo.Rows[0][0]);

                        DateTime fechahora_registro = DateTime.Now;//FECHA DE REGISTRO
                        string hora_registro = DateTime.Now.ToShortTimeString();//HORA DE REGISTRO

                        string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

                        DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio FROM Biohorario WHERE @input between rango1 and rango2");
                        int Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);

                        string query = "Select * from BundleRejected br join OperacionOperario op on br.id_operarioOpe = op.id_operarioOpe  where Id_Bundle = " + Convert.ToString(Id_Bundle) + " and op.id_seccion =" + Id_Seccion;

                        DataTable dt_rechazos = DataAccess.Get_DataTable(query);

                        query = "select idAreaDefectoRec from AreaDefectoRechazo where idbundle=" + Id_Bundle + " and idseccion=" + Id_Seccion;
                        DataTable bundle_rejectedDefecto = DataAccess.Get_DataTable(query);

                        if (dt_rechazos.Rows.Count > 0 || bundle_rejectedDefecto.Rows.Count > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "bultomalo();", true);
                        }
                        else
                        {
                            OrderDetailDA.SaveBundlesApproved(int.Parse(Id_Bundle), Id_Biohorario, Id_Seccion, userId, Id_Muestreo, Convert.ToDateTime(fechahora_registro), Convert.ToString(hora_registro), 0);
                            Load_Data(Id_Order, Id_Seccion.ToString());
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);          
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
                    }
                }


            }
            if (e.CommandName == "Reparar")
            {
                try
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    string Id_Bundle = GridBultos.DataKeys[indice].Value.ToString();

                    MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
                    string userId = mu.ProviderUserKey.ToString();

                    string quanti = row.Cells[5].Text;
                    DataTable dt_Muestreo = DataAccess.Get_DataTable("declare @input float =" + quanti + " SELECT * FROM Muestreo WHERE @input between rango_incial and rango_final and nivel_insp = (Select levelaql from AQL)");
                    int Id_Muestreo = Convert.ToInt16(dt_Muestreo.Rows[0][0]);

                    DateTime fechahora_registro = DateTime.Now;//FECHA DE REGISTRO
                    string hora_registro = DateTime.Now.ToShortTimeString();//HORA DE REGISTRO

                    string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

                    DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio FROM Biohorario WHERE @input between rango1 and rango2");
                    int Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);

                    OrderDetailDA.SaveBundlesRepair(Convert.ToInt32(Id_Bundle), Id_Biohorario, Id_Seccion, userId,Id_Muestreo, Convert.ToDateTime(fechahora_registro), Convert.ToString(date_now));

                    Load_Data(Id_Order, Id_Seccion.ToString());
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                }
                catch (Exception ex)
                {
                    string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
                }

            }

        }
        catch (Exception)
        {

            throw;
        }
    }
  
    protected void lnkNew_Click(object sender, EventArgs e)
    {
        int Id_Seccion = Convert.ToInt16(Request.QueryString["id_seccion"]);
        int Id_Linea = Convert.ToInt16(Request.QueryString["id_linea"]);
        Response.Redirect("SearchIL.aspx?id_seccion=" + Id_Seccion + "&id_linea=" + Id_Linea);
    }

    //public void Load_Grid_Asistencia(int linea,int Id_Seccion)
    //{
    //    string query = " SELECT distinct row_number() OVER(ORDER BY Op.id_operario) AS id, Op.id_operario,oper.id_operarioOpe, Op.nombre as Nombre,op.activo as Estado"
    //                  + " FROM Operario Op join OperacionOperario Oper ON op.id_operario = Oper.id_operario"
    //                  +" join Seccion s on oper.id_seccion = s.id_seccion where s.Id_Linea = "+linea+" and s.Id_Seccion = "+Id_Seccion+" and Op.nombre Not like 'AYUDA'";

    //    DataTable dt_Asitencia = DataAccess.Get_DataTable(query);

    //    ControlAsistencia.DataSource = dt_Asitencia;
    //    ControlAsistencia.DataBind();

    //}

    //protected void btnguardar_Click(object sender, EventArgs e)
    //{
    //    int Id_Seccion = Convert.ToInt16(Request.QueryString["id_seccion"]);
    //    string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

    //    DataTable Bihorarios_update = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio FROM Biohorario WHERE @input between rango1 and rango2");

    //    DateTime fecha = DateTime.Now;
    //    string hora = DateTime.Now.ToShortTimeString();

    //    for (int i = 0; i <= ControlAsistencia.Rows.Count - 1; i++)
    //    {       
    //                CheckBox check = (CheckBox)ControlAsistencia.Rows[i].Cells[3].FindControl("check_asistencia");
    //                if (check.Checked == false)
    //                {
    //                    Label lblidoperario = (Label)ControlAsistencia.Rows[i].Cells[1].FindControl("lblidopOper");
    //                    OrderDetailDA.SaveAusencia(Convert.ToInt16(Bihorarios_update.Rows[0][0]), Convert.ToInt16(lblidoperario.Text), fecha, hora);                  
    //                }

    //    }

    //    string message = "alert('Registros Ingresados Exitosamente!');";
    //    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);

    //}

    //protected void btnhistorial_Click(object sender, EventArgs e)
    //{
    //    this.EditarAcciones.ShowOnPageLoad = true;
    //    int id_order = Convert.ToInt16(Request.QueryString["id_order"]);
    //    int id_seccion = Convert.ToInt16(Request.QueryString["id_seccion"]);

    //    string query = "select ba.Id_Bundle,bd.NSeq,ba.FechaAprobado, ba.HoraAprobado from BundleApproved  ba inner join Bundle bd on ba.Id_Bundle=bd.Id_Bundle  where bd.Id_Order = " + id_order + " and ba.idseccion =" + id_seccion;

    //    DataTable bundleApr = DataAccess.Get_DataTable(query);
    //    ASPxGridView1.DataSource = bundleApr;
    //    ASPxGridView1.DataBind();

    //    string query1 = " select br.Id_BundleRejected,bd.NSeq, ad.Nombre as Area,d.nombre as Defecto, op.nombre, br.FechaRecibido, br.HoraRecibido"
    //                   +" from BundleRejected br inner join Bundle bd on br.Id_Bundle = bd.Id_Bundle"
    //                   +" inner join DefectosxArea da on da.Id_DefxArea = br.Id_DefxArea"
    //                   +" inner join AreaDefecto ad on ad.Id_Area = da.Id_Area"
    //                   +" inner join Defectos d on d.id_defectos = da.Id_defectos"
    //                   +" inner join OperacionOperario oper on br.id_operarioOpe = oper.id_operarioOpe"
    //                   +" inner join Operario op on oper.id_operario = op.id_operario"
    //                   +" inner join Seccion s on oper.id_seccion = s.id_seccion"
    //                   +" inner join Operacion o on oper.id_operacion = o.id_operacion"
    //                   +" where bd.Id_Order = "+id_order+" and s.id_seccion ="+ id_seccion;

    //    DataTable bundleRej = DataAccess.Get_DataTable(query1);
    //    ASPxGridView2.DataSource = bundleRej;
    //    ASPxGridView2.DataBind();

    //}

    //protected void SomethingChanged(object sender, EventArgs e)
    //{
    //    //in this example this handler is used for both, Dropdownlist and RadiobuttonList
    //    var listControl = (ListControl)sender;
    //    var row = (RepeaterItem)listControl.NamingContainer;
    //    var item = listControl.SelectedItem;
    //    var btncontrol = (Button)row.FindControl("btnSave1");
    //    if (item.Text == "Rechazado")
    //    {
    //        btncontrol.Text = "Audit Now";
    //        btncontrol.CssClass = "belize-hole-flat-button";
    //    }
    //    else
    //    {
    //        btncontrol.Text = "Save";
    //        btncontrol.CssClass = "nephritis-flat-button";
    //    }

    //    //with FindControl on the row you could also find controls in other columns...
    //}
}
