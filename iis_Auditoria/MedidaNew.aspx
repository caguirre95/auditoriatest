﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MedidaNew.aspx.cs" Inherits="MedidaNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "MedidaNew.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    idstyle: item.idstyle,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);
                    $('#<%=txtstyle.ClientID%>').val(ui.item.style);
                    $('#<%=hdnidstyle.ClientID %>').val(ui.item.idstyle)

                    PoblarDropDown(ui.item.idstyle);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });

        $(function () {
            $('#<%=txttallas.ClientID%>').autocomplete({
                 source: function (request, response) {
                     var cod = document.getElementById("<%= hdnidstyle.ClientID %>").value;
                     $.ajax({
                         url: "MedidaNew.aspx/GetTallaXEstilo",
                         data: "{'pre' :'" + request.term + "','idestilo': '" + cod + "'}",
                         dataType: "json",
                         type: "POST",
                         contentType: "application/json; charset=utf-8",
                         success: function (data) {
                             response($.map(data.d, function (item) {
                                 return {
                                     porder: item.porder,
                                     idorder: item.idorder,
                                     json: item
                                 }
                             }))
                         },
                         error: function (XMLHttpRequest, textStatus, errorThrown) {
                             alert(textStatus);
                         }
                     });
                 },
                 focus: function (event, ui) {
                     $('#<%=txttallas.ClientID%>').val(ui.item.porder);

                     return false;
                 },
                 select: function (event, ui) {
                     document.getElementById("<%= drpTallas.ClientID %>").value = ui.item.idorder;
                    funcionchange();
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });

        //window.onload = function() {
        //    imprimirValor();
        //}

        //function imprimirValor(){
        //    var select = document.getElementById("feedingHay");
        //    var options=document.getElementsByTagName("option");
        //    console.log(select.value);
        //    console.log(options[select.value-1].innerHTML)
        //}
    </script>

    <script type="text/javascript">

        function PoblarDropDown(id) {
            $.ajax({
                type: "POST",
                url: "MedidaNew.aspx/GetTallasXIdStyle",
                data: "{ id: '" + id + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#<%=drpTallas.ClientID%>').empty().append($("<option></option>").val("0").html("Seleccione..."));
                    $.each(data.d, function (key, value) {
                        var option = $(document.createElement('option'));
                        option.html(value.porder);
                        option.val(value.idorder);

                        $('#<%=drpTallas.ClientID%>').append(option);
                    });

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus + ": " + XMLHttpRequest.responseText);

                }
            });
        }

        function funcionchange() {
            var cod = document.getElementById("<%= drpTallas.ClientID %>").value;
            creartabla(cod);
            limpiar2();
            //ar name = $('[id*=drpTallas] option:selected').text();
        }

        function creartabla(id) {
            console.log(id);
            $.ajax({
                type: "POST",
                url: "MedidaNew.aspx/GetEspecificaciones",
                data: "{ id: '" + id + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    $("#cuerpo").html("");
                    $.each(data.d, function (key, value) {
                        var tr = '<tr> <td style="display:none">' + value.idorder + '</td>' +
                            '<td >' + value.porder + '</td>' +
                            '<td style="display:none">' + value.value1 + '</td>' +
                            '<td style="display:none">' + value.value2 + '</td>' +
                            '<td style="display:none">' + value.value3 + '</td>' +
                            '<td style="display:none"></td>' +
                            '<td style="display:none"></td>' +
                            '<td> <input type="button" id="btnSelecccionar" onclick="selected()" value="Medir" class="btn btn-primary" /> </td>' +
                            '</tr>';

                        $("#cuerpo").append(tr)

                    });

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus + ": " + XMLHttpRequest.responseText);

                }
            });
        }

        function selected() {
            var rowin, table = document.getElementById("tablePMedidas");

            for (var i = 0; i < table.rows.length; i++) {

                table.rows[i].onclick = function () {

                    rowin = this.rowIndex;
                    console.log(rowin);

                    //idespecificacion
                    var v1 = table.rows[rowin].cells[0].innerHTML;
                    //punto de medida
                    var v2 = table.rows[rowin].cells[1].innerHTML;
                    //valor de medida
                    var v3 = table.rows[rowin].cells[2].innerHTML;
                    //tolerancia Maxima
                    var v4 = table.rows[rowin].cells[3].innerHTML;
                    //tolerancia Minima
                    var v5 = table.rows[rowin].cells[4].innerHTML;


                    var texto = document.getElementById("txtvalor");
                    texto.innerHTML = "0";

                    document.getElementById("lblseccionMedidaM").innerHTML = v2;
                    document.getElementById("lblValorM").innerHTML = v3;

                    document.getElementById("lblvalor1").innerHTML = v3;
                    document.getElementById("lblvalor2").innerHTML = v4;
                    document.getElementById("lblvalor3").innerHTML = v5;
                    $('#<%=hdrowIndex.ClientID %>').val(rowin);
                    // console.log(v1 +" "+ v2 + " " + v3 +" "+ v4 +" " +v5);
                    mostrarmodal();
                }
            }


        }

        function mostrarmodal() {
            $('#ModalCapturaInfo').modal('show');
        }

        function calculo() {

            //variables de controles
            var puntoMedidaM = document.getElementById("lblseccionMedidaM");
            var puntoMedida = document.getElementById("lblPuntoMedida");
            var status = document.getElementById("lblEstatus");
            var medidaIngresada = document.getElementById("lblMedidaIngresada");
            var maxminResto = document.getElementById("lblMaxMin");
            var leyenda = document.getElementById("leyendaMaxMin");

            //valor de tallas y tolerancias
            var medidaIngresadaM = document.getElementById("txtvalor").innerHTML;
            var tolMax = document.getElementById("lblvalor2").innerHTML;
            var tolMin = document.getElementById("lblvalor3").innerHTML;
            var ban = "";
            if (medidaIngresadaM != "0" && tolMax != "" && tolMin != "") {

                var n1 = parseFloat(medidaIngresadaM);
                var n2 = parseFloat(tolMax);
                var n3 = parseFloat(tolMin);

                puntoMedida.innerHTML = puntoMedidaM.innerHTML;
                medidaIngresada.innerHTML = medidaIngresadaM;

                if (n1 <= n2 && n1 >= n3) {

                    status.innerHTML = "In Tolerance";
                    maxminResto.innerHTML = "0";
                    status.className = "text-success";
                    ban = "In Spec";
                }
                else {

                    if (n1 > n2) {
                        var r = n1 - n2;
                        maxminResto.innerHTML = r.toFixed(3);
                        status.innerHTML = "Big";
                        leyenda.innerHTML = "Exceso";
                        status.className = "text-danger";
                        ban = "Big";
                    }
                    else if (n1 < n3) {
                        var r = n3 - n1;
                        leyenda.innerHTML = "Deficit";
                        maxminResto.innerHTML = r.toFixed(3);
                        status.innerHTML = "Small";
                        status.className = "text-primary";
                        ban = "Small";
                    }

                }

                var indide = document.getElementById("<%= hdrowIndex.ClientID %>").value;
                var table = document.getElementById("tablePMedidas");
                table.rows[indide].cells[5].innerHTML = medidaIngresadaM;
                table.rows[indide].cells[6].innerHTML = ban;
                table.rows[indide].style.backgroundColor = '#808080';

                $('#ModalCapturaInfo').modal('hide');

                var count = 0;
                var cantidad = table.rows.length;
                for (var i = 0; i < cantidad; i++) {

                    if (table.rows[i].cells[5].innerHTML != "") {
                        count++;
                    }
                }

                if (count === cantidad) {

                    guarda1Elemento();

                }
                else {

                    alertTime("'Medida Asignada!'");
                }

            }
            else {
                alertMaster('Algunos Campos Se Encuentran Vacios, Seleccione el Punto de Medida y Vuelva a Intentar  ', 'info');
            }
        }


        function guarda1Elemento() {

            var table = document.getElementById("tablePMedidas");
            var idporder = document.getElementById("<%= hfidPorder.ClientID %>").value;
            var idestilotalla = document.getElementById("<%= drpTallas.ClientID %>").value;
            var idestacion = document.getElementById("<%= hdnidestacion.ClientID %>").value;
            var cont = 0;

            ///verifica que la pieza este en espec
            for (var i = 0; i < table.rows.length; i++) {
                if (table.rows[i].cells[6].innerHTML != "In Spec") {
                    cont++;
                }
            }

            var estado = "";
            if (cont == 0) {
                estado = "true";
            }
            else {
                estado = "false";
            }

            var objMaster = "{'idestacion': '" + idestacion + "','idestilotalla': '" + idestilotalla + "','idporder': '" + idporder + "','estado': '" + estado + "'}";

            $.ajax({
                url: "MedidaNew.aspx/GuardarMaster",
                type: "POST",
                data: objMaster,
                //dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d != "") {
                        guardardatos(data.d);
                    }
                    else {
                        alertMaster('Error vuelva a intentar', 'error');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(textStatus)
                    alertMaster(textStatus, 'error');
                }
            });
        }

        function guardardatos(idresultado) {

            var table = document.getElementById("tablePMedidas");

            var lista = "{";
            for (var rowin = 0; rowin < table.rows.length; rowin++) {
                //idespecificacion
                var v1 = table.rows[rowin].cells[0].innerHTML;
                //valor speck
                var v3 = table.rows[rowin].cells[2].innerHTML;
                var v10 = parseFloat(v3);
                v10 = v10.toFixed(3);
                //valor medido
                var v6 = table.rows[rowin].cells[5].innerHTML;
                //estatusPM
                var v7 = table.rows[rowin].cells[6].innerHTML;

                var objDetail = "{'idespecificacion': '" + v1 + "','valormedido': '" + v6 + "','valorspeck': '" + v10 + "','status': '" + v7 + "','id': '" + idresultado + "'}";

                // listDetail = listDetail
                lista = lista + objDetail;

                $.ajax({
                    url: "MedidaNew.aspx/GuardarDetail",
                    type: "POST",
                    data: objDetail,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d != "") {
                            console.log(data.d);
                        }
                        else {
                            console.log(1);
                            //  alertMaster('Error vuelva a intentar', 'error');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log(2);
                        // alertMaster(textStatus, 'error');
                    }

                });

            }

            lista = lista + "}";

            var cod = document.getElementById("<%= drpTallas.ClientID %>").value;
            creartabla(cod);
            limpiar2();

            alertTime("'Pieza Agregada Correctamente!'");

        }

        function GuardarAntes() {
            var table = document.getElementById("tablePMedidas");
            var idporder = document.getElementById("<%= hfidPorder.ClientID %>").value;
            var idestilotalla = document.getElementById("<%= drpTallas.ClientID %>").value;
            var idestacion = document.getElementById("<%= hdnidestacion.ClientID %>").value;
            var cont = 0;

            for (var i = 0; i < table.rows.length; i++) {
                if (table.rows[i].cells[6].innerHTML != "In Spec" && table.rows[i].cells[5].innerHTML != "") {
                    cont++;
                }
            }

            var estado = "";
            if (cont == 0) {
                estado = "true";
            }
            else {
                estado = "false";
            }

            var objMaster = "{'idestacion': '" + idestacion + "','idestilotalla': '" + idestilotalla + "','idporder': '" + idporder + "','estado': '" + estado + "'}";

            $.ajax({
                url: "MedidaNew.aspx/GuardarMaster",
                type: "POST",
                data: objMaster,
                //dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d != "") {
                        guardardatosantes(data.d);
                    }
                    else {
                        alertMaster('Error vuelva a intentar', 'error');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(textStatus)
                    alertMaster(textStatus, 'error');
                }
            });

        }

        function guardardatosantes(idresultado) {

            var table = document.getElementById("tablePMedidas");

            var lista = "{";
            for (var rowin = 0; rowin < table.rows.length; rowin++) {

                if (table.rows[rowin].cells[5].innerHTML != "") {

                    //idespecificacion
                    var v1 = table.rows[rowin].cells[0].innerHTML;
                    //valor speck
                    var v3 = table.rows[rowin].cells[2].innerHTML;
                    var v10 = parseFloat(v3);
                    v10 = v10.toFixed(3);
                    //valor medido
                    var v6 = table.rows[rowin].cells[5].innerHTML;
                    //estatusPM
                    var v7 = table.rows[rowin].cells[6].innerHTML;

                    var objDetail = "{'idespecificacion': '" + v1 + "','valormedido': '" + v6 + "','valorspeck': '" + v10 + "','status': '" + v7 + "','id': '" + idresultado + "'}";

                    // listDetail = listDetail
                    lista = lista + objDetail;

                    $.ajax({
                        url: "MedidaNew.aspx/GuardarDetail",
                        type: "POST",
                        data: objDetail,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d != "") {
                                console.log(data.d);
                            }
                            else {
                                console.log(1);
                                //  alertMaster('Error vuelva a intentar', 'error');
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            console.log(2);
                            // alertMaster(textStatus, 'error');
                        }

                    });
                }
            }

            lista = lista + "}";

            var cod = document.getElementById("<%= drpTallas.ClientID %>").value;
            creartabla(cod);
            limpiar2();

            alertTime("'Pieza Agregada Correctamente!'");

        }

        function changeText(name) {

            //var row = this.parentNode.parentNode.parentNode;
            var texto = document.getElementById("txtvalor");

            var texto2 = texto.innerHTML;
            if (texto2 != "0" && isNaN(texto2) == false) {

                var num = eval(name);
                var n1 = parseFloat(texto2);
                console.log(n1);
                console.log(num);
                n1 = n1 + num;

                texto.innerHTML = n1;
            }
            else {
                swal({
                    title: 'La Caja de Texto esta en 0!',
                    type: 'info'
                });
                return;
            }

        }

        function changeTextConcat(name) {

            var texto = document.getElementById("txtvalor");

            var n = texto.innerHTML.includes(".");

            if (n) {
                if (name == ".") {
                    swal({
                        title: 'No puede ingreasar un punto decimal ya existe!',
                        type: 'info'
                    });
                    return;
                }
            }

            if ((isNaN(name) == false && name != "") || name == ".") {
                var valor = texto.innerHTML;
                if (valor == "0") {
                    valor = "";
                }
                console.log(valor);
                texto.innerHTML = valor.concat(name);
            }
            else if (name == "reset") {
                texto.innerHTML = "0";
                return;
            }

        }

        function alertMaster(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }

        function alertTime(texto) {
            swal({
                type: 'success',
                title: texto,
                timer: 1000,
                showConfirmButton: false
            });
        }

        function LimpiarFocus() {
            limpiar();
            limpiar2();
        }

        function limpiar() {
            $("#cuerpo").html("");
            $('#<%=drpTallas.ClientID%>').empty();

            $('#<%=txtPorder.ClientID%>').val('');
            $('#<%=hfidPorder.ClientID%>').val('');

            $('#<%=txtstyle.ClientID%>').val('');
            $('#<%=hdnidstyle.ClientID %>').val('');

            $('#<%=txttallas.ClientID %>').val('');

        }

        function limpiar3() {
            $("#cuerpo").html("");

            var texto = document.getElementById("<%= drpTallas.ClientID %>").value;

             if (texto != "") {
                 document.getElementById("<%= drpTallas.ClientID %>").value = 0;
             }


             $('#<%=txttallas.ClientID%>').val('');

            limpiar2();
        }

        function limpiar2() {

            document.getElementById("lblseccionMedidaM").innerHTML = "";
            document.getElementById("lblPuntoMedida").innerHTML = "";
            document.getElementById("lblEstatus").innerHTML = "";
            document.getElementById("lblMedidaIngresada").innerHTML = 0;
            document.getElementById("lblMaxMin").innerHTML = 0;
            document.getElementById("leyendaMaxMin").innerHTML = "Exceso";

            document.getElementById("txtvalor").innerHTML = "0";
            document.getElementById("lblvalor1").innerHTML = "0";
            document.getElementById("lblvalor2").innerHTML = "0";
            document.getElementById("lblvalor3").innerHTML = "0";

        }

    </script>

    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>

    <link href="StyleSheetAuto.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Measurement suggestion </strong></h3>
            </div>
            <div class="panel-body">

                <div id="contenedorMain" class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 quitarpadd-iz-der">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            Corte:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></span>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" required onfocus="LimpiarFocus()" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                            Tallas:
                                     <asp:DropDownList ID="drpTallas" CssClass="form-control" onchange="funcionchange()" runat="server"></asp:DropDownList>

                            Buscar Tallas
                             <div class="input-group">
                                 <span class="input-group-btn">
                                     <span class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></span>
                                 </span>
                                 <div class="ui-widget" style="text-align: left;">
                                     <asp:TextBox ID="txttallas" placeholder="Nombre Talla" required onfocus="limpiar3()" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                 </div>
                             </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            Estilo:
                            <asp:TextBox runat="server" CssClass="form-control" placeholder="Estilo" Font-Size="14px" ID="txtstyle" />
                            <asp:HiddenField ID="hfidPorder" runat="server" />
                            <asp:HiddenField ID="hdnidstyle" runat="server" />
                            <asp:HiddenField ID="hdrowIndex" runat="server" />
                            <asp:HiddenField ID="hdnfieltest" runat="server" />
                            <asp:HiddenField ID="hdnidestacion" runat="server" />

                            <input type="button" value='Guardar Medidas' name='btnGuardar' onclick="GuardarAntes()" class="btn btn-success " style="margin-top: 1.2em" />
                        </div>

                        <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pad-0_marr-15">
                                <article class="form-group">
                                    <h4 class="bg-primary text-center">
                                        <label class="">Medida Valor de Media</label>
                                    </h4>
                                    <h3 class="text-muted text-center">
                                        <label id="lblvalor1" class="">0</label>
                                    </h3>
                                </article>

                                <article class="form-group">
                                    <h4 class="bg-primary text-center">
                                        <label class="">Medida Valor Maximo</label>
                                    </h4>
                                    <h3 class="text-muted text-center">
                                        <label id="lblvalor2" class="">0</label>
                                    </h3>
                                </article>
                                <article class="form-group">
                                    <h4 class="bg-primary text-center">
                                        <label class="">Medida Valor Minimo</label>

                                    </h4>
                                    <h3 class="text-muted text-center">
                                        <label id="lblvalor3">0</label>
                                    </h3>
                                </article>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pad-0_marr-15">
                                <article class="form-group">
                                    <h4 class="bg-info text-center">
                                        <label>Medida ingresada</label>
                                    </h4>
                                    <h3 class="text-muted text-center">
                                        <label id="lblMedidaIngresada">0</label>
                                    </h3>
                                </article>
                                <article class="form-group">
                                    <h4 class="bg-info text-center">
                                        <label id="leyendaMaxMin">Exceso</label>
                                    </h4>
                                    <h3 class="text-muted text-center">
                                        <label id="lblMaxMin" class="">0</label>
                                    </h3>
                                </article>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pad-0_marr-15">
                                <article class="form-group">
                                    <h4 class="bg-success text-center">
                                        <label>Punto Medida</label>
                                    </h4>
                                    <h3 class="text-muted text-center">
                                        <label id="lblPuntoMedida"></label>
                                    </h3>
                                </article>
                                <article class="form-group">
                                    <h4 class="bg-success text-center">
                                        <label>Status de Medida</label>
                                    </h4>
                                    <h3 class="text-muted text-center">
                                        <label id="lblEstatus"></label>
                                    </h3>
                                </article>
                            </div>
                        </section>


                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Secciones de Medida
                            </div>
                            <div class="panel-body">
                                <table id="tablePMedidas" class="table table-responsive table-striped">
                                    <tbody id="cuerpo">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>


    <aside class="modal fade fullscreen-modal" id="ModalCapturaInfo">
        <div class="modal-dialog">
            <div class="modal-content">

                <article class="modal-header" style="padding-bottom: 3px">
                    <button id="cerrar" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>

                    <h4 style="margin: 5px; padding: 5px;">Ingreso de Medidas</h4>
                    <div class="col-lg-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label class="label label-warning" style="font-size: large">Punto de Medida</label>
                                <label id="lblseccionMedidaM" class="classLabel"></label>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label class="label label-warning" style="font-size: large">Valor de Punto Medida</label>
                                <label id="lblValorM" class="classLabel"></label>
                            </div>
                        </div>
                    </div>
                </article>

                <article class="modal-body" style="padding-top: 5px">

                    <%--  <div class="panel panel-body" style="padding-left: 0; padding-right: 0">--%>
                    <div class="row">

                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">

                            <label id="txtvalor" class="classLabel">0</label>

                            <%-- <input type="text" id="txtvalor" class="form-control" />--%>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-7 col-xs-7">
                            <input type="button" value='Aceptar' onclick="calculo()" class="btn btn-info withB" />
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0 !important; padding: 0 !important">

                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="margin: 0 !important; padding: 0 !important">

                                <div class="col-lg-12">
                                    <input type="button" value='7' name='7' onclick="changeTextConcat(this.name)" class="btn btn-success withB3" />
                                    <input type="button" value='8' name='8' onclick="changeTextConcat(this.name)" class="btn btn-success withB3" />
                                    <input type="button" value='9' name='9' onclick="changeTextConcat(this.name)" class="btn btn-success withB3" />
                                </div>
                                <div class="col-lg-12">
                                    <input type="button" value='4' name='4' onclick="changeTextConcat(this.name)" class="btn btn-success withB3" />
                                    <input type="button" value='5' name='5' onclick="changeTextConcat(this.name)" class="btn btn-success withB3" />
                                    <input type="button" value='6' name='6' onclick="changeTextConcat(this.name)" class="btn btn-success withB3" />
                                </div>
                                <div class="col-lg-12">
                                    <input type="button" value='1' name='1' onclick="changeTextConcat(this.name)" class="btn btn-success withB3" />
                                    <input type="button" value='2' name='2' onclick="changeTextConcat(this.name)" class="btn btn-success withB3" />
                                    <input type="button" value='3' name='3' onclick="changeTextConcat(this.name)" class="btn btn-success withB3" />
                                </div>
                                <div class="col-lg-12">
                                    <input type="button" value='0' name='0' onclick="changeTextConcat(this.name)" class="btn btn-success withB3" />
                                    <input type="button" value='.' name='.' onclick="changeTextConcat(this.name)" class="btn btn-success withB3" />
                                    <input type="button" value='C' name='reset' onclick="changeTextConcat(this.name)" class="btn btn-warning withB3" />
                                </div>

                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-7 col-xs-7" style="margin: 0 !important; padding: 0 !important">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="button" value='1/2' name='1/2' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='3/8' name='3/8' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='3/16' name='3/16' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='11/16' name='11/16' onclick="changeText(this.name)" class="btn btn-success withB" />
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="button" value='1/4' name='1/4' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='5/8' name='5/8' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='5/16' name='5/16' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='13/16' name='13/16' onclick="changeText(this.name)" class="btn btn-success withB" />
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="button" value='3/4' name='3/4' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='7/8' name='7/8' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='7/16' name='7/16' onclick="changeText(this.name)" class="btn btn-success  withB" />
                                    <input type="button" value='15/16' name='15/16' onclick="changeText(this.name)" class="btn btn-success  withB" />
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <input type="button" value='1/8' name='1/8' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='1/16' name='1/16' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='9/16' name='9/16' onclick="changeText(this.name)" class="btn btn-success withB" />
                                </div>

                            </div>

                        </div>
                    </div>
                </article>
            </div>
        </div>
    </aside>




    <div class="modal fade fullscreen-modal" tabindex="-1" role="dialog" id="ventanapro">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h1>Correccion de Medidas</h1>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanelproveedor" runat="server">
                        <ContentTemplate>
                            <div style="color: #006699; font-size: 14px">

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Seleccione el bulto:</label>
                                    <asp:DropDownList ID="DropDownListbultos" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Seleccione la Seccion de Medida:</label>

                                </div>

                                <div style="text-align: center">
                                    <asp:Label ID="msg" runat="server"></asp:Label>
                                </div>
                                <hr />

                                <div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

