﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReporteMuestra/Muestra.master" AutoEventWireup="true" CodeFile="SeccionMuestra.aspx.cs" Inherits="ReporteMuestra_SeccionMuestra" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtstyle.ClientID%>').autocomplete({
                source: function (request, response) {

                    var ids = $('#<%=cli.ClientID%>').val();

                    $.ajax({
                        url: "SeccionMuestra.aspx/GetStyle",
                        data: "{'pref' :'" + request.term + "' , 'ids':'" + ids + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    estilo: item.estilo,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtstyle.ClientID%>').val(ui.item.estilo);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

            })

        });

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

    <link href="../css/font-awesome.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <br />
        <div class="panel panel-primary">
            <div class="panel-heading">Reporte de Muestras</div>
            <div class="panel-body">

                <nav class="navbar navbar-default" style="margin-bottom: 5px !important; margin-top: 10px !important">
                    <ol class="breadcrumb" style="margin-bottom: 8px !important">
                        <li><a href="../MedidaMuestra.aspx">Medidas Muestra</a></li>
                        <li class="active">Reporte</li>
                    </ol>
                </nav>
                <br />

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                Cliente:
                        <asp:DropDownList ID="cli" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Select...</asp:ListItem>
                            <asp:ListItem Value="11">CARHARTT</asp:ListItem>
                            <asp:ListItem Value="14">DENNIS</asp:ListItem>
                            <asp:ListItem Value="17">DICKIES</asp:ListItem>
                            <asp:ListItem Value="28">UNIFIRST</asp:ListItem>
                            <asp:ListItem Value="42">ARIAT</asp:ListItem>
                            <asp:ListItem Value="65">VF</asp:ListItem>
                            <asp:ListItem Value="68">TIMBERLAND</asp:ListItem>
                            <asp:ListItem Value="72">SPG</asp:ListItem>
                            <asp:ListItem Value="85">BOOT BARN</asp:ListItem>
                            <asp:ListItem Value="88">CODY JAMES</asp:ListItem>
                            <asp:ListItem Value="92">FECHHEIMER</asp:ListItem>
                            <asp:ListItem Value="95">NORTH FACE</asp:ListItem>
                        </asp:DropDownList>

                                Style:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" CssClass="btn btn-default" ID="LinkButton2" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtstyle" placeholder="Style" class="textboxAuto text-info form-control" Font-Size="14px" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                            <asp:HiddenField ID="hdstyle" runat="server" />
                                        </div>
                                    </div>
                            </div>
                        </div>

                        <br />
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <%--<div class="panel-heading">Select search filter</div>--%>
                            <div class="panel-body">
                                Start Date
                            <div class="input-group">

                                <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                <asp:TextBox ID="txtfecha1" CssClass="form-control" runat="server"></asp:TextBox>

                            </div>
                                End Date
                          
                                <div class="input-group">
                                    <i class="input-group-addon fa fa-calendar-o" aria-hidden="true"></i>
                                    <asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div style="margin-bottom: 10px">
                                    <asp:Button Text="Generate Report" CssClass="btn btn-primary form-control" ID="Button1" runat="server" OnClick="Button1_Click" />
                                </div>
                                <div style="margin-bottom: 10px">
                                    <asp:Button ID="btnexport" CssClass="btn btn-success form-control" runat="server" Text="Export Excel" OnClick="btnexport_Click" />
                                </div>
                                <div style="margin-bottom: 10px">
                                    <asp:Button Text="Limpiar Formulario" CssClass=" btn btn-info form-control" ID="Button2" runat="server" OnClick="Button2_Click" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 15px">
                        <hr />
                        <dx:ASPxGridView ID="gridre" Theme="Metropolis" runat="server" Width="100%" AutoGenerateColumns="False" OnDataBinding="gridre_DataBinding" OnCustomCallback="gridre_CustomCallback" Settings-ShowColumnHeaders="true" OnHtmlRowPrepared="gridre_HtmlRowPrepared">
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="tiempo" Caption="Hours" Width="100px" />
                                <dx:GridViewDataTextColumn FieldName="Tol(+)"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Tol(-)"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="style" Caption="Style" />
                                <dx:GridViewDataTextColumn FieldName="NombreMedida" Caption="POM" />
                                <dx:GridViewDataTextColumn FieldName="spec" Caption="Spec" />
                                <dx:GridViewDataTextColumn FieldName="valor" Caption="Measurements" />
                                <dx:GridViewDataTextColumn Caption="Comment">
                                    <DataItemTemplate>
                                        <dx:ASPxLabel ID="lbltexto" runat="server" Text='<%# Eval("texto") %>'>
                                        </dx:ASPxLabel>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DiffSpec"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="OOT"></dx:GridViewDataTextColumn>
                            </Columns>
                            <Settings VerticalScrollBarMode="Visible" VerticalScrollableHeight="350" />
                            <SettingsPager Mode="ShowAllRecords" />
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar" runat="server"></dx:ASPxGridViewExporter>
                    </div>


                </div>

            </div>
        </div>
    </div>

</asp:Content>

