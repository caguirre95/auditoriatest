﻿using DevExpress.Web;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReporteMuestra_SeccionMuestra : System.Web.UI.Page
{
    int cliente = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                txtfecha1.Text = DateTime.Now.ToShortDateString();
                txtfecha2.Text = DateTime.Now.ToShortDateString();

                DataTable dt = DataAccess.Get_DataTable("select idcliente from tbusuariosDash where NameUser ='" + User.Identity.Name.Trim() + "'");
                if (dt.Rows.Count > 0)
                {
                    cliente = Convert.ToInt16(dt.Rows[0][0]);
                }

                Session["muestra"] = null;

                datos();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<estilosClass> GetStyle(string pref, int ids)
    {
        try
        {
            List<estilosClass> list = new List<estilosClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);
            string sql = "";
            cn.Open();
            if (ids.ToString() != "0")
            {
                sql = "SELECT distinct top 10  Style,Id_Style FROM Style where Style like '%" + pref + "%' and Id_Cliente=" + ids;
            }
            else
            {
                sql = "SELECT distinct top 10  Style,Id_Style FROM Style where Style like '%" + pref + "%'";
            }
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                estilosClass obj = new estilosClass();

                obj.estilo = dt.Rows[i][0].ToString();

                obj.id = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class estilosClass
    {
        public string estilo { get; set; }
        public string id { get; set; }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ReporteMuestra/SeccionMuestra.aspx");
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            dts();
        }
        catch (Exception)
        {

            throw;
        }
    }

    void datos()
    {
        try
        {
            DataTable dt = new DataTable();

            int estacion = 2;

            dt = OrderDetailDA.RepDashboard5Muestra(estacion);

            Session["muestra"] = dt;

            gridre.DataBind();
        }
        catch (Exception)
        {
            throw;
        }
    }

    void dts()
    {
        try
        {
            DataTable dt2 = new DataTable();

            int estacion = 2;

            if (txtstyle.Text != "" && cli.SelectedValue == "0")
            {
                dt2 = OrderDetailDA.RepDashboard5StyleMuestra(estacion, txtstyle.Text, Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));
            }
            else if (txtstyle.Text == "" && cli.SelectedValue != "0")
            {
                dt2 = OrderDetailDA.RepDashboard5ClienteMuestra(estacion, Convert.ToInt16(cli.SelectedValue), Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));
            }
            else if (txtstyle.Text != "" && cli.SelectedValue != "0")
            {
                dt2 = OrderDetailDA.RepDashboard5StyleClienteMuestra(estacion, Convert.ToInt16(cli.SelectedValue), txtstyle.Text, Convert.ToDateTime(txtfecha1.Text), Convert.ToDateTime(txtfecha2.Text));
            }

            Session["muestra"] = dt2;

            gridre.DataBind();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        try
        {
            gridre.DataBind();

            exportar.GridViewID = "gridre";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = exportar;

            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreatePageForEachLink();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();
                options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                options.SheetName = "Reporte de Muestra";
                options.DocumentOptions.Author = "Rocedes Team Developer";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=Muestra.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            ps.Dispose();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void gridre_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;

        ASPxLabel label = gridre.FindRowCellTemplateControl(e.VisibleIndex, null, "lbltexto") as ASPxLabel;

        if (label.Text.TrimEnd() == "In Spec")
            label.ForeColor = System.Drawing.Color.Green;
        else if (label.Text.TrimEnd() == "Small")
            label.ForeColor = System.Drawing.Color.Blue;
        else if (label.Text.TrimEnd() == "Big")
            label.ForeColor = System.Drawing.Color.Red;
    }

    protected void gridre_DataBinding(object sender, EventArgs e)
    {
        gridre.DataSource = Session["muestra"];
    }

    protected void gridre_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridre.DataBind();
    }
}