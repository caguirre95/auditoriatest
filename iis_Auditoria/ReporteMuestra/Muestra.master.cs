﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReporteMuestra_Muestra : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/Login.aspx", false);
        }
        catch (Exception)
        {

        }
    }

    protected void prc_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/ReporteMuestra/SeccionMuestra.aspx");
    }
}
