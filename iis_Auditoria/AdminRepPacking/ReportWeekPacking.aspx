﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportWeekPacking.aspx.cs" Inherits="AdminRepPacking_ReportWeekPacking" %>


<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Week packing Report</strong></div>
            <div class="panel-body">
                <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <strong>Week</strong>
                    <asp:DropDownList ID="drpSemana" CssClass="form-control" runat="server" Width="200px" />
                    <br />
                    <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control" Text="Generar" Width="200px" OnClick="btngenerar_Click" />
                    
                    <asp:Button ID="btnExport" OnClick="btnExport_Click" CssClass="btn btn-success form-control a" runat="server" Text="Export Excel" Width="200px" />
                    <hr />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow-x: auto">
                    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" Theme="Metropolis" runat="server" OnDataBinding="grid_DataBinding" OnLoad="grid_Load"
                        Width="100%" AutoGenerateColumns="False" OnCustomCallback="grid_CustomCallback">
                        <Columns>
                            <dx:GridViewBandColumn Caption="week" HeaderStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <dx:ASPxLabel ID="ASPxLabelweek" runat="server" Text=""></dx:ASPxLabel>
                                </HeaderTemplate>
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="Semana" />
                                    <dx:GridViewDataTextColumn FieldName="Corte" />
                                    <dx:GridViewDataTextColumn FieldName="Estilo" />
                                    <dx:GridViewDataTextColumn FieldName="Descripcion" />
                                    <dx:GridViewDataTextColumn FieldName="Color" />
                                    <dx:GridViewDataTextColumn FieldName="Unidades" />
                                    <dx:GridViewDataTextColumn FieldName="Cajas" />
                                    <dx:GridViewDataTextColumn FieldName="Primera" />
                                    <dx:GridViewDataTextColumn FieldName="PrimeraPer" Caption="% Primera"/>
                                    <dx:GridViewDataTextColumn FieldName="Segunda" />
                                    <dx:GridViewDataTextColumn FieldName="SegundaPer" Caption="% Segundas" />
                                    <dx:GridViewDataTextColumn FieldName="Empaquado" />
                                    <dx:GridViewDataTextColumn FieldName="EmpaquePer" Caption="% Empaque" />
                                    <dx:GridViewDataTextColumn FieldName="Deficit" />
                                  
                                </Columns>
                            </dx:GridViewBandColumn>
                        </Columns>
                        <Settings ShowFooter="true" ShowGroupFooter="VisibleAlways" />
                        <SettingsBehavior AutoExpandAllGroups="true" />
                        <SettingsPager Mode="ShowAllRecords" />
                       <%-- <GroupSummary>
                            <dx:ASPxSummaryItem FieldName="Quantity" DisplayFormat="{0}" ShowInGroupFooterColumn="Quantity" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Monday" DisplayFormat="{0}" ShowInGroupFooterColumn="Monday" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Tuesday" DisplayFormat="{0}" ShowInGroupFooterColumn="Tuesday" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Wednesday" DisplayFormat="{0}" ShowInGroupFooterColumn="Wednesday" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Thursday" DisplayFormat="{0}" ShowInGroupFooterColumn="Thursday" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Friday" DisplayFormat="{0}" ShowInGroupFooterColumn="Friday" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Saturday" DisplayFormat="{0}" ShowInGroupFooterColumn="Saturday" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="OpenCut" DisplayFormat="{0}" ShowInGroupFooterColumn="OpenCut" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="inventario" DisplayFormat="{0}" ShowInGroupFooterColumn="inventario" SummaryType="Sum" />
                        </GroupSummary>--%>
                        <TotalSummary>
                           
                            <dx:ASPxSummaryItem FieldName="Corte" DisplayFormat="{0}" SummaryType="Count" />
                            <dx:ASPxSummaryItem FieldName="Unidades" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Cajas" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Primera" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Friday" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Segunda" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Empaquado" DisplayFormat="{0}" SummaryType="Sum" />
                          
                        </TotalSummary>
                    </dx:ASPxGridView>
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

