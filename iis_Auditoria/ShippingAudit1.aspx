﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ShippingAudit1.aspx.cs" Inherits="ShippingAudit1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ShippingAudit1.aspx/GetPorderCom",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    idcorte: item.idcorte,
                                    Corte: item.Corte,
                                    Cantidad: item.Cantidad,
                                    Estilo: item.Estilo,
                                    Cliente: item.Cliente,
                                    Color: item.Color,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.Corte);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcliente.ClientID%>').val(ui.item.Cliente);
                    $('#<%=txtcorte.ClientID%>').val(ui.item.Corte);
                    $('#<%=HiddenFieldidcorte.ClientID%>').val(ui.item.idcorte);
                    $('#<%=txtestilo.ClientID%>').val(ui.item.Estilo);
                    $('#<%=txtcantidad.ClientID%>').val(ui.item.Cantidad);
                    $('#<%=txtcolor.ClientID%>').val(ui.item.Color);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                .append("<a class='text-info ' style='padding-left:30px;' >" + item.Corte + "</a>").appendTo(ul);
            };
        });
    </script>

    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <style>
        .padd {
            padding-top: 10px;
        }
    </style>
    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }
    </script>

     <script type="text/javascript">
        function alertsucess() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }

        function alertError() {
            swal({
                title:'Error!',
                text: "Ha ocurrido un error durante la ejecucion!",
                type: 'error'
            });
        }

        function alertwarning() {
            swal({
                title:'Alerta!',
                text: "Debe seleccionar rechazo ó Aceptado",
                type: 'warning'
            });
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2>
        <asp:Label ID="lbl" runat="server" Text="Shipping Audit"></asp:Label></h2>
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <div class="container">
        <div class="col-lg-12">
            <div class="panel panel-primary" style="margin-top:10px;">
                <div class="panel-heading">Auditoria Embarque</div>
                <div class="panel-body">

                    <div class="col-lg-12">

                   
                    <div class="col-md-6 padd">
                        <asp:Label ID="Label2" runat="server" Text="Cliente:"></asp:Label>
                        <asp:TextBox ID="txtcliente" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                    </div>

                    <div class="col-md-6 padd">
                        <asp:Label ID="Label3" runat="server" Text="Firma:"></asp:Label>
                        <asp:TextBox ID="txtfirma" runat="server" Font-Size="Small" CssClass="form-control" TextMode="Password"></asp:TextBox>
                    </div>

                    <div class="col-md-6 padd">
                        <asp:Label ID="Label1" runat="server" Text="Responsable: "></asp:Label>
                        <asp:TextBox ID="txtresponsable" CssClass="form-control" Font-Size="Small" Text="Candido Solis" Enabled="false" runat="server"></asp:TextBox>

                    </div>
                    <div class="col-md-6 padd">
                        <asp:Label ID="Label4" runat="server" Text="Fecha:"></asp:Label>
                        <asp:Label ID="lbfecha" runat="server" CssClass="form-control" Font-Size="Small" Text=""></asp:Label>
                    </div>


                   <%-- <div class="col-md-6 padd">
                        <asp:Label ID="Label6" runat="server" Text="Auditor: "></asp:Label>
                        <asp:TextBox ID="txtauditor" required CssClass="form-control" runat="server"></asp:TextBox>
                    </div>--%>
                </div>

        <div class="col-lg-12 padd">
            <div class="col-lg-6 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Informacion del Corte</div>
                    <div class="panel-body">
                        <div class="col-lg-12 padd">
                            <div class="col-lg-3">Corte#</div>
                            <div class="col-lg-9">
                                <asp:HiddenField ID="HiddenFieldidcorte" runat="server" />
                                <asp:TextBox ID="txtcorte" required Font-Size="Small" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-12 padd">
                            <div class="col-lg-3">Estilo#</div>
                            <div class="col-lg-9">
                                <asp:TextBox ID="txtestilo" CssClass="form-control" Font-Size="Small" required runat="server" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-12 padd">
                            <div class="col-lg-3">Color#</div>
                            <div class="col-lg-9">
                                <asp:TextBox ID="txtcolor" CssClass="form-control" Font-Size="Small" runat="server" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-12 padd">
                            <div class="col-lg-3">Cantidad#</div>
                            <div class="col-lg-9">
                                <asp:TextBox ID="txtcantidad" CssClass="form-control" required runat="server" Font-Size="Small" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-12 padd" style="text-align: center">
                            <strong>Plan de Muestreo</strong>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" Width="100%" RepeatDirection="Horizontal" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Value="2.5" Text="2.5 AQL"></asp:ListItem>
                                        <asp:ListItem Value="4.0" Text="4.0 AQL"></asp:ListItem>
                                        <asp:ListItem Value="6.5" Text="6.5 AQL"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Resultados de Calidad</div>
                    <div class="panel-body">

                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <div class="col-lg-12 padd">
                                    <div class="col-lg-6"># Unidades Auditadas</div>
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtunidades" required Font-Size="Small" CssClass="form-control" onkeypress="return num(event);" Enabled="false" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-12 padd">
                                    <div class="col-lg-6"># de mayor defecto</div>
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtdefectos" required Font-Size="Small" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-12 padd" style="margin-bottom: 20px;">
                                    <div class="col-lg-6">% Defectuoso</div>
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtdefectuosoPer" required Font-Size="Small" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                           

                        <div class="col-lg-12 padd" style="text-align: center">
                            <asp:RadioButtonList runat="server" ID="RadioButtonList2"  Enabled="false" RepeatDirection="Horizontal" Width="100%">
                                <asp:ListItem Text="ACEPTADO" Value="true"></asp:ListItem>
                                <asp:ListItem Text="RECHAZADO" Value="false"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                                 </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="col-lg-12 " style="">
                            <div class="col-lg-2 padd">
                                <strong>DQATS</strong>
                            </div>
                            <div class="col-lg-4 padd">
                                <asp:TextBox ID="txtDQATS" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 padd">
            <p>
                <asp:Button ID="btnGuardar" OnClick="btnGuardar_Click" CssClass="btn btn-primary" runat="server" Text="Guardar Registro" />
                <asp:LinkButton ID="btnnuevo" OnClick="btnnuevo_Click" CssClass="btn btn-success" runat="server" Text="Crear Nuevo Registro" />
            </p>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gridDefectos" runat="server" CssClass="table table-hover table-striped" ShowFooter="true" AutoGenerateColumns="false" OnRowDataBound="gridDefectos_RowDataBound" GridLines="None" Width="100%">
                        <Columns>
                            <asp:TemplateField Visible="True">
                                <HeaderTemplate>
                                    Codigo 
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblcodigo" runat="server" Text=''></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Tipo Defecto:
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:DropDownList ID="DropDownListTipoDefecto" Font-Size="Small" OnSelectedIndexChanged="DropDownListTipoDefecto_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Defecto Mayor:
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdefmayor" Font-Size="Small" CssClass="form-control" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Total:
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtTotal1" Font-Size="Small" OnTextChanged="txtTotal1_TextChanged" onkeypress="return num(event);" CssClass="form-control" AutoPostBack="true" runat="server"></asp:TextBox>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lbltotal" Font-Size="Small" runat="server" Text="Total: 0"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Defecto Menor:
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdefmenor" Font-Size="Small" onkeypress="return num(event);" CssClass="form-control" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Total
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txttotal2" Font-Size="Small" onkeypress="return num(event);" CssClass="form-control" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>

                     </div>

            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

