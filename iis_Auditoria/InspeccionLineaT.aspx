﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InspeccionLineaT.aspx.cs" Inherits="InspeccionLineaT" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   <%-- <script type="text/javascript" src="scripts/footable.min.js"></script>--%>
    <script type="text/javascript">
        function successalert() {
            swal({
                title: 'Registro Ingresado Correctamente!',
                type: 'success'
            });
        }
    </script>
    <script type="text/javascript">
        //function pageLoad() { // this gets fired when the UpdatePanel.Update() completes
        //    ReBindMyStuff();
        //}
        //function ReBindMyStuff() { // create the rebinding logic in here
        //    $('[id*=Repeater]').footable();
        //}
    </script>

    <script type="text/javascript">
        function ShowWindow() {
            AsignarDefectos.Show();
        }
        function HideWindow() {
            AsignarDefectos.Hide();
        }
        function Check_Click(objRef) {
            //Get the Row based on checkbox
            var row = objRef.parentNode.parentNode.parentNode.parentNode;

            if (objRef.checked) {
                //If checked change color to Aqua
                row.style.backgroundColor = "#0090CB";
                row.style.color = "white";
            }
            else {
                //If not checked change back to original color
                if (row.rowIndex % 2 == 0) {
                    //Alternating Row Color
                    row.style.backgroundColor = "white";
                    row.style.color = "black";
                }
                else {
                    row.style.backgroundColor = "white";
                    row.style.color = "black";
                }
            }
        }

        function Check_Click2(objRef) {
            //Get the Row based on checkbox
            var row = objRef.parentNode.parentNode.parentNode;

            if (objRef.checked) {
                //If checked change color to Aqua
                row.style.backgroundColor = "#0090CB";
                row.style.color = "white";
            }
            else {
                //If not checked change back to original color
                if (row.rowIndex % 2 == 0) {
                    //Alternating Row Color
                    row.style.backgroundColor = "white";
                    row.style.color = "black";
                }
                else {
                    row.style.backgroundColor = "white";
                    row.style.color = "black";
                }
            }
        }
    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {
            var num = document.getElementById("<%= drplinea.ClientID %>").value;
            $('#<%=txtoperario.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "BultoABultoReject.aspx/GetOperario",
                        data: "{'pre' :'" + request.term + "','idlinea':'" + num + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    operario: item.operario,
                                    idoperacionOpe: item.idoperacionOpe,
                                    operacion: item.operacion,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtoperario.ClientID%>').val(ui.item.operario);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtoperario.ClientID%>').val(ui.item.operario);
                    $('#<%=hdfIdOperacionOp.ClientID%>').val(ui.item.idoperacionOpe);
                    $('#<%=txtoperacion.ClientID%>').val(ui.item.operacion);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.operario + "</a>").appendTo(ul);
            };
        });

    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {

            $('#<%=txtarea.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "InspeccionLineaT.aspx/GetArea",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    Id_Area: item.Id_Area,
                                    Nombre: item.Nombre,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtarea.ClientID%>').val(ui.item.Nombre);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=hdnIdArea.ClientID%>').val(ui.item.Id_Area);
                    $('#<%=txtarea.ClientID%>').val(ui.item.Nombre);
                    //var divElem = document.getElementById('divElem');
                    //divElem.style.display = "block";

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.Nombre + "</a>").appendTo(ul);
            };
        });

    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {

            $('#<%=txtdefecto.ClientID%>').autocomplete({
                source: function (request, response) {
                    //var divElem = document.getElementById('divElem');
                    //divElem.style.display = "block";
                    $.ajax({
                        url: "InspeccionLineaT.aspx/getdefecto",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    Id_Defecto: item.Id_Defecto,
                                    Nombre: item.Nombre,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtdefecto.ClientID%>').val(ui.item.Nombre);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=hdnidDefecto.ClientID%>').val(ui.item.Id_Defecto);
                    $('#<%=txtdefecto.ClientID%>').val(ui.item.Nombre);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.Nombre + "</a>").appendTo(ul);
            };
        });

    </script>

    <script>
        $(document).ready(function () {

            $('#<%=btnguardaDefectoXarea.ClientID%>').click(function (e) {
                e.preventDefault();
                var idbulto = 0;
                var idseccion = 0;

                var divElem = document.getElementById('divElem');

                var query = window.location.search.substring(1);
                var vars = query.substr(query.indexOf("?") + 1).split("&");
               // var idbiho = $('#<%=lblbio.ClientID%>').html().substr(11);
                // alert(idbiho);
                for (var i = 0; i < vars.length; i++) {
                    var pair = vars[i].split("=");
                    if (pair[0] == "IdBundle") {
                        idbulto = pair[1];
                    }
                    if (pair[0] == "id_seccion") {
                        idseccion = pair[1];
                    }

                }
                
                var obj = {
                    cantidad: document.getElementById("<%= txtCantidadDefecto.ClientID%>").value,
                    idarea: document.getElementById("<%= hdnIdArea.ClientID %>").value,
                    iddefecto: document.getElementById("<%= hdnidDefecto.ClientID %>").value,
                    idbulto: idbulto,
                    idseccion: idseccion,
                    idbiho: $('#<%=lblbio.ClientID%>').html().substr(11),
                };

                // alert(obj.idbulto);
                if (obj.iddefecto != "" && isNaN(obj.iddefecto) == false && obj.cantidad != "" && isNaN(obj.cantidad) == false && obj.idarea != "" && isNaN(obj.idarea) == false) {
                    var actionData = "{'cantidad': '" + obj.cantidad + "','idarea': '" + obj.idarea + "','iddefecto': '" + obj.iddefecto + "','idbulto': '" + obj.idbulto + "','idseccion': '" + obj.idseccion + "','idbiho': '" + obj.idbiho + "'}";
                    //   alert("ddddd");
                    $.ajax({
                        url: "InspeccionLineaT.aspx/GuardarRechazo",
                        type: "POST",
                        data: actionData,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "true") {

                                divElem.innerHTML = " <strong> Guardado correctamente </strong>";
                                divElem.className = "alert alert-success";
                                divElem.style.display = "block";

                                limpiartexbox();
                            }
                            else {
                                divElem.innerHTML = " <strong> Error en insert </strong>";
                                divElem.className = "alert alert-warning";
                                divElem.style.display = "block";
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            // alert(status);
                            divElem.innerHTML = " <strong>'" + textStatus + "'</strong>";
                            divElem.className = "alert alert-danger";
                            divElem.style.display = "block";
                        }

                    })
                }
                else {
                    // alert("dlfk");
                    divElem.innerHTML = " <strong> Llenar campos correctamente </strong>";
                    divElem.className = "alert alert-info";
                    divElem.style.display = "block";
                }
            });

        });


        function limpiartexbox() {
            document.getElementById("<% =txtCantidadDefecto.ClientID%>").value = "";
            document.getElementById("<%= txtdefecto.ClientID%>").value = "";
            document.getElementById("<%= txtarea.ClientID%>").value = "";
            document.getElementById("<%= hdnidDefecto.ClientID%>").value = "";
            document.getElementById("<%= hdnIdArea.ClientID%>").value = "";

        }
    </script>

    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2>
        <asp:Label ID="lbl" runat="server" Text="In Line Audit"></asp:Label></h2>
    <br />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="container-fluid">
        <div class="col-lg-12 centrar">
            <asp:ImageButton ID="ImageButton2" runat="server" CssClass="marB" ImageUrl="~/img/system_search.png" OnClick="lnkNew_Click" />
            <asp:LinkButton ID="lnkNew" runat="server" OnClick="lnkNew_Click">New Search</asp:LinkButton>

            <asp:ImageButton ID="ImageButton1" runat="server" CssClass="marB" ImageUrl="~/img/report.png" OnClick="ImageButton1_Click" />
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Hoja de Corte</asp:LinkButton>
        </div>
    </div>
    <div class="container-fluid info_ord">
        <div class="container  " style="padding-bottom: 5px;">

            <div class="col-xs-4" style="padding-top: 5px;">
                <div class="col-xs-12 centrar">
                    <asp:Label ID="lblcustom" runat="server" Text="Customer:"></asp:Label>
                    <asp:Label ID="lblcustomerval" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-12 centrar">
                    <asp:Label ID="Label1" runat="server" Text="Style:"></asp:Label>
                    <asp:Label ID="lblstylesval" runat="server" Text=""></asp:Label>
                </div>
            </div>

            <div class="col-xs-4" style="padding-top: 5px;">
                <div class="col-xs-12 centrar">
                    <asp:Label ID="Fecha" runat="server" Text="Fecha:"></asp:Label>
                    <asp:Label ID="lblfechaval" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-12 centrar">
                    <asp:Label ID="Label3" runat="server" Text="Modulo:"></asp:Label>

                </div>
            </div>

            <div class="col-xs-4" style="padding-top: 5px;">
                <div class="col-xs-12 centrar">
                    <asp:Label ID="Label5" runat="server" Text="Linea/Sección:"></asp:Label>
                    <asp:Label ID="lineaval" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblseccionval" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-12 centrar">
                    <asp:Label ID="Label7" runat="server" Text="Inspector:"></asp:Label>
                    <asp:Label ID="lblinspectorval" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
    </div>

    <br />

    <div class="col-md-8 col-md-offset-2">
        <div class="col-lg-4 col-lg-offset-4">
            <asp:Button ID="Button11" Text=" Reparar Bulto " OnClick="Button1_Click" CssClass="btn btn-success form-control marB" runat="server" />
           <%-- <a class="btn btn-default form-control marB" data-toggle="modal" data-target="#myModal"><span class="icon-plus"></span>Operario Ayuda</a>--%>
            <a class="btn btn-default form-control marB" data-toggle="modal" data-target="#myModalDef"><span class="icon-plus"></span>Asignar Defectos de Corte</a>

            <style>
                .marB {
                    margin-top: 5px;
                    margin-bottom: 5px;
                }
            </style>
        </div>
    </div>

    <div class="col-lg-12 centrar" style="height: 35px">
        <h4>
            <asp:Label ID="lblbio" runat="server" Text="" CssClass="label label-primary"></asp:Label>
        </h4>
    </div>

    <asp:GridView ID="grv_insp_linea" runat="server" ShowFooter="true" CssClass="table table-hover table-striped" GridLines="None" Width="100%"
        AutoGenerateColumns="False" OnRowDataBound="grv_insp_linea_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="No">
                <ItemTemplate>
                    <asp:Label ID="lblno" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Codigo de Empleado">
                <ItemTemplate>
                    <asp:Label ID="lblcodigo" runat="server" Text='<%# Eval("Codigo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nombre del Operario">
                <ItemTemplate>
                    <asp:Label ID="lbloperario" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Operacion">
                <ItemTemplate>
                    <asp:Label ID="lbloperacion" runat="server" Text='<%# Eval("Operacion") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Muestreo" HeaderStyle-CssClass="muestreo_col" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblmuestreo" runat="server" Text='<%# Eval("Muestreo") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <div>
                        <asp:Label ID="lblsample" Text="TTLMuestreo:" runat="server" />
                        <asp:Label ID="lblTotalqty" runat="server" />
                    </div>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Defectos">
                <ItemTemplate>
                    <asp:TextBox ID="txtdefectos" runat="server" Enabled="false" Font-Size="Medium" CssClass="form-control" MaxLength="3" Text='<%# Eval("CantidadDefectos") %>'></asp:TextBox>
                    <asp:Label runat="server" ID="lblYourLabel" ForeColor="red" Font-Size="Small" />
                </ItemTemplate>
                <FooterTemplate>
                    <div>
                        <asp:Label ID="lblTtdef" Text="TTDEF:" runat="server" />
                        <asp:Label ID="lblTotaldefect" runat="server" />
                    </div>
                </FooterTemplate>
            </asp:TemplateField>
            <%--  <asp:TemplateField HeaderText="PxP" HeaderStyle-CssClass="muestreo_col" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:RadioButtonList ID="rbl_PxP" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="✓" Value="0"></asp:ListItem>
                        <asp:ListItem Text="X" Value="1"></asp:ListItem>
                    </asp:RadioButtonList>
                </ItemTemplate>
               
            </asp:TemplateField>--%>
            <%--  <asp:TemplateField HeaderText="AUD" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:CheckBox ID="chk_Aud" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="R-AUD" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:CheckBox ID="chk_raud" runat="server" />
                </ItemTemplate>
                <FooterTemplate>
                    <div>
                        <asp:Label ID="lblPorDef" Text="%DEF:" runat="server" />
                        <asp:Label ID="lblTPorDef" runat="server" />
                    </div>
                </FooterTemplate>
            </asp:TemplateField>
            <%-- <asp:TemplateField HeaderText="H" HeaderStyle-CssClass="muestreo_col" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:RadioButtonList ID="rbl_H" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="✓" Value="0"></asp:ListItem>
                        <asp:ListItem Text="X" Value="1"></asp:ListItem>
                    </asp:RadioButtonList>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnrechazar" runat="server" Text="Rechazar" Visible="false" CssClass="sun-flower-button1" />
                    <asp:Button ID="ASPxButton8" runat="server" Text="Rechazar" OnClick="ASPxButton8_Click" CssClass="btn btn-danger" />
                    <%-- <dx:ASPxButton ID="ASPxButton8" runat="server" OnClick="ASPxButton8_Click"
                        Text="Rechazar" CssClass="sun-flower-button1">
                    </dx:ASPxButton>--%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>

    </asp:GridView>

    <asp:HiddenField runat="server" ID="hdnCodigo" />
    <asp:HiddenField runat="server" ID="hdnOperacion" />
    <br />

    <dx:ASPxPopupControl ID="AsignarDefectos" runat="server" EnableTheming="True" ClientInstanceName="AsignarDefectos"
        HeaderText="Defectos Encontrados" Modal="True" Theme="MetropolisBlue" Width="750px"
        AllowDragging="True" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" CloseAction="CloseButton"
        ScrollBars="Vertical" MaxHeight="650px">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxPanel ID="ASPxPanel1" runat="server">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                            <center>
                             <table>
               <tr>
               <td>  
                   <asp:Label ID="lblsuperv" runat="server" Text="Supervisor:" style="font-size:18px;font-weight: bold;"></asp:Label>
                   <asp:Label ID="lblsupervalue" runat="server" Text="" style="font-size:18px;font-weight: bold;"></asp:Label>
               </td>
               <td style="text-align: right;">
               <asp:Label ID="lblFechahora" runat="server" Text="Fecha:" style="font-size:18px;font-weight: bold;"></asp:Label>
               <asp:Label ID="lbldaterechazo" runat="server" Text="" style="font-size:18px;"></asp:Label>
               <asp:Label ID="lblhorarechazo" runat="server" Text="" style="font-size:18px;"></asp:Label>
               </td>
               </tr>
               <tr>
               <td style="text-align: left;">
                   <asp:Label ID="Label20" runat="server" Text="Operario:" style="font-size:18px;font-weight: bold;"></asp:Label>
                   <asp:Label ID="lbloperario" runat="server" style="display: inline-block; font-size:18px;color:Red;"></asp:Label>
                   </td>
               <td style="text-align: right;">
                   <asp:Label ID="Label2" runat="server" Text="Linea:" style="display: inline-block; width: 120px; font-size:18px;font-weight: bold;"></asp:Label>
                   <asp:Label ID="lbllinearechazo" runat="server" style="display: inline-block; font-size:18px;"></asp:Label>
                   </td>
               </tr>                  
               <tr>
               <td colspan="2">
                    <div style="height:450px; overflow-y: scroll;margin-top:5px;margin-bottom:5px">
                          <asp:GridView ID="DefectosEncontradosC" runat="server" CssClass="table table-hover table-striped" GridLines="None" AutoGenerateColumns="false" Width="100%">
                       <Columns>                      
                           <asp:TemplateField>
                               <ItemTemplate>
                               <center>
                                   <asp:CheckBox ID="chkdefectos" Width="30px" runat="server" onclick="Check_Click(this)" />
                                   </center>
                               </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Defectos" HeaderStyle-CssClass="header_gv">
                               <ItemTemplate>
                                   <asp:Label ID="LabelDefect" runat="server" Text='<%# Eval("Nombre") %>' Font-Size="Large"></asp:Label>
                               </ItemTemplate>
                           </asp:TemplateField>                      
                           <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                           <ItemTemplate>
                                               <asp:TextBox ID="txtcantidad"  CssClass="form-control" runat="server" Font-Size="Large"></asp:TextBox>
                                           </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField Visible="false">
                               <ItemTemplate>
                                    <asp:Label ID="labelIddefecto" runat="server" Text='<%# Eval("id_defectos") %>' ></asp:Label>
                                   </ItemTemplate>
                                </asp:TemplateField>
                       </Columns>
                     <%--  <PagerStyle HorizontalAlign="Center" CssClass="paginador" Font-Bold="False" 
                           Font-Size="16px" Width="50px" />--%>
                   </asp:GridView>
</div>
                 
               </td>
               </tr>
                <tr>
                       <td colspan="2" style="text-align:center;">
                           <asp:Button ID="btnsave" runat="server" Text=" GUARDAR " CssClass="btn btn-default" Width="200" OnClick="btnsave_Click" />
                       </td>
                   </tr>
               </table>
                           </center>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>

    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="RechazoBultos" runat="server" EnableTheming="True" ClientInstanceName="AsignarDefectos"
        HeaderText="Control de Bultos Rechazados" Modal="True" Theme="MetropolisBlue"
        AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        Width="850px" CloseAction="CloseButton">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <dx:ASPxPanel ID="ASPxPanel2" runat="server">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent1" runat="server">
                            <table style="width: 100%">
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                        <b>
                                            <asp:Label ID="lbllinea" runat="server" Text="Linea:"></asp:Label></b>
                                        <asp:Label ID="lbllineaval" runat="server" Text=""></asp:Label>
                                        <b>
                                            <asp:Label ID="Label4" runat="server" Text="Serie:"></asp:Label></b>
                                        <asp:Label ID="lblbundleid" runat="server" Text=""></asp:Label>
                                        <b>
                                            <asp:Label ID="Label6" runat="server" Text="Corte:"></asp:Label></b>
                                        <asp:Label ID="lblcorte" runat="server" Text=""></asp:Label>
                                        <b>
                                            <asp:Label ID="lbl222" runat="server" Text="Fecha y Hora:"></asp:Label></b>
                                        <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div style="height: 450px; overflow-y: scroll; margin-top: 5px; margin-bottom: 5px">

                                            <asp:GridView ID="ControlBultos" runat="server" CssClass="table table-hover table-striped" AutoGenerateColumns="False" GridLines="None" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Nombre">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblnombre" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Nombre")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Defectos">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlDefectos" runat="server" CssClass="form-control" DataSourceID="SqlDataSource2" Enabled="false"
                                                                DataTextField="nombre" DataValueField="id_defectos">
                                                            </asp:DropDownList>
                                                            <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                                                                ConnectionString="<%$ ConnectionStrings:RocedesCS %>"
                                                                SelectCommand="select [id_defectos], [nombre] FROM [Defectos]"></asp:SqlDataSource>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Recibido por Supervisor">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblfecharecibido" Text='<%#DataBinder.Eval(Container.DataItem, "FechaRecibido")%>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Cantidad">
                                                        <ItemTemplate>
                                                            <asp:Label ID="cantdef" Text='<%#DataBinder.Eval(Container.DataItem, "Defectos")%>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>

                                            </asp:GridView>
                                        </div>
                                        <center>
                               <asp:Label ID="lblmensaje" runat="server" Text="" Visible="false"></asp:Label>
                               </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center;">
                                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-default" ValidationGroup="val01" Text=" SAVE " Width="200" OnClick="btnsave1_Click" />
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Asignar Defecto a Opeario Ayuda</h4>
                </div>
                <div class="modal-body">
                    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>--%>
                    <div class="form-group">
                        Linea:
                                <asp:DropDownList ID="drplinea" CssClass="form-control" runat="server"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        Operario:
                          <div class="ui-widget" style="text-align: left;">
                              <asp:TextBox ID="txtoperario" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                          </div>
                    </div>
                    <div class="form-group">
                        Operacion:
                                 <asp:TextBox ID="txtoperacion" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <%--  <asp:HiddenField ID="hdfIdOperario" runat="server" />--%>
                    <asp:HiddenField ID="hdfIdOperacionOp" runat="server" />
                    <br />
                    <div style="height: 500px; overflow-y: scroll">

                        <asp:GridView ID="GridViewdefectosC" runat="server" CssClass="table table-hover table-striped" GridLines="None" Width="100%" AutoGenerateColumns="False">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkdefectos" Width="30px" runat="server" onclick="Check_Click2(this)" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Defectos" HeaderStyle-CssClass="header_gv">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelDefect" runat="server" Text='<%# Eval("Nombre") %>' Font-Size="Large"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Seccion" ItemStyle-HorizontalAlign="Center" Visible="false" HeaderStyle-CssClass="header_gv">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIdDefecto" runat="server" Text='<%# Eval("id_defectos") %>' Font-Size="Large"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtcantidad" Width="100px" Height="30px" Style="color: black !important;" BackColor="LightYellow" runat="server" Font-Size="Large"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" CssClass="paginador" Font-Bold="False"
                                Font-Size="16px" Width="50px" />
                        </asp:GridView>
                    </div>

                    <asp:Button ID="btnSaveAyuda" runat="server" OnClick="btnSaveAyuda_Click" Text="SAVE " CssClass="btn btn-default center-block" />
                    <%-- </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
                <style>
                    .ui-autocomplete {
                        z-index: 4000;
                    }
                </style>
                <%-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>--%>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalDef" tabindex="-1" role="dialog" aria-labelledby="myModalLabeldef">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabeldef">Asignar Defecto de Area</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        Area:
                          <div class="ui-widget" style="text-align: left;">
                              <asp:HiddenField ID="hdnIdArea" runat="server" />
                              <asp:TextBox ID="txtarea" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                          </div>
                    </div>
                    <div class="form-group">
                        Defecto:
                         <asp:HiddenField ID="hdnidDefecto" runat="server" />
                        <div class="ui-widget" style="text-align: left;">
                            <asp:TextBox ID="txtdefecto" placeholder="Defecto" CssClass="form-control textboxAuto text-info" Font-Size="14px" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        Cantidad:
                                 <asp:TextBox ID="txtCantidadDefecto" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>


                    <br />
                  

                    <asp:Button ID="btnguardaDefectoXarea" runat="server" Text="SAVE " CssClass="btn btn-default center-block" />

                    <div role="alert" style="display: none" id="divElem"></div>

                </div>
                <style>
                    .ui-autocomplete {
                        z-index: 4000;
                    }
                </style>

            </div>
        </div>
    </div>


</asp:Content>
