﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdministrarEstilosYTallas.aspx.cs" Inherits="AdministrarEstilosYTallas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtestilo.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "AdministrarEstilosYTallas.aspx/GetEstilo",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    id: item.id,
                                    estilo: item.estilo,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtestilo.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtestilo.ClientID%>').val(ui.item.estilo);
                    $('#<%=hdnidestilo.ClientID%>').val(ui.item.id);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
            };
        });
    </script>

    <script>
        function limpiar() {
            $('#<%=txtestilo.ClientID%>').val('');
            $('#<%=hdnidestilo.ClientID%>').val('');
            $('#<%=hdnidestilogrid.ClientID%>').val('');

        }

        function cerrarmodal() {
            $('#modaltallas').modal('hide')
        }

        function guardarEstilo() {

            var estilo = document.getElementById("<%= txtnuevoestilo.ClientID %>").value;
            var idcliente = document.getElementById("<%= drpClientes.ClientID %>").value;
            var wash = document.getElementById("<%= chklavado.ClientID %>").checked;

            if (estilo != "" && idcliente > 0) {


                var objMaster = "{'estilo': '" + estilo + "','idcliente': '" + idcliente + "','wash': '" + wash + "'}";

                $.ajax({
                    url: "AdministrarEstilosYTallas.aspx/Guardarestilo",
                    type: "POST",
                    data: objMaster,
                    //dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "true") {
                            $('#mymodalestilo').modal('hide');
                            $('#<%=txtnuevoestilo.ClientID%>').val('');

                            swal({
                                title: 'Guardado Correctamente',
                                type: 'success'
                            });
                        }
                        else {
                            swal({
                                title: 'Error vuelva a intentar',
                                type: 'error'
                            });

                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log(textStatus)
                        swal({
                            title: textStatus,
                            type: 'error'
                        });

                    }
                });

            }
            else {
                swal({
                    title: 'Cargar los campos correctamente',
                    type: 'info'
                });
            }
        }

        function guardarTalla() {

            var talla = document.getElementById("<%= txtNuevatalla.ClientID %>").value;

            if (talla != "") {
                $.ajax({
                    url: "AdministrarEstilosYTallas.aspx/Guardartalla",
                    type: "POST",
                    data: "{'talla':'" + talla + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "true") {
                            $('#mymodaltalla').modal('hide');
                            $('#<%=txtNuevatalla.ClientID%>').val('');
                            swal({
                                title: 'Guardado Correctamente',
                                type: 'success'
                            });
                        }
                        else {
                            swal({
                                title: 'Error vuelva a intentar',
                                type: 'error'
                            });

                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log(textStatus)
                        swal({
                            title: textStatus,
                            type: 'error'
                        });

                    }
                });
            }
            else {
                swal({
                    title: 'Cargar los campos correctamente',
                    type: 'info'
                });
            }


        }

        function alertSeleccionarMedida(_title, _type) {
            swal({
                title: _title,
                type: _type,
                timer: 1000,
                showConfirmButton: false
                 
            });
        }
    </script>



    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>
    <link href="StyleSheetAuto.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-10">
                Style:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" OnClick="LinkButton1_Click" CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtestilo" placeholder="Style" onfocus="limpiar();" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdnidestilo" />
                                        </div>
                                    </div>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-10">

                <div class="form-inline pt">
                    <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#mymodalestilo">
                        Nuevo Estilo
                    </button>
                    <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#mymodaltalla">
                        Nueva Talla
                    </button>
                    <asp:Button ID="btn1" runat="server" CssClass="btn btn-primary" Text="Spec" OnClick="btn1_Click" />
                </div>

                <!-- Modal -->
                <div class="modal fade" id="mymodalestilo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Nuevo Estilo</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    Estilo
                                  <asp:TextBox runat="server" ID="txtnuevoestilo" CssClass="form-control" />
                                </div>
                                <div class="form-group">
                                    Cliente
                                  <asp:DropDownList runat="server" ID="drpClientes" CssClass="form-control" />
                                </div>
                                <div class="form-group">
                                    Lavado
                                  <asp:CheckBox ID="chklavado" Text="Estilo Lavado" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="button" value='Guardar Estilo' onclick="guardarEstilo()" class="btn btn-success " />
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="mymodaltalla" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Nueva Talla</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    Talla
                                  <asp:TextBox runat="server" ID="txtNuevatalla" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="button" value='Guardar Talla' onclick="guardarTalla()" class="btn btn-success " />
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <hr />

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h2>Estilos</h2>
                <div>
                    <asp:HiddenField runat="server" ID="hdnidestilogrid" />
                    <asp:GridView ID="GridEstilos" OnRowCommand="GridEstilos_RowCommand" OnRowDataBound="GridEstilos_RowDataBound" AllowPaging="true" PageSize="13"
                        OnPageIndexChanging="GridEstilos_PageIndexChanging" AutoGenerateColumns="false" DataKeyNames="Id_Style"
                        CssClass="table table-hover table-striped" GridLines="None" Width="100%" runat="server" HeaderStyle-CssClass="backcolorheadergrid">
                        <Columns>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblidbundle" runat="server" Text='<%#Bind("Id_Style") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Style" HeaderText="Estilo" ControlStyle-ForeColor="#0099cc" ItemStyle-Width="300px" />
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Button Text="Refrescar" ID="btnrefreshStyle" CssClass="btn btn-default" OnClick="btnrefreshStyle_Click" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton CommandName="verTallas" ID="comandoRep" CssClass="btn btn-primary" runat="server">
                                     <i class="glyphicon glyphicon-text-size" aria-hidden="true"></i> Ver Tallas
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="Primero" LastPageText="Ultimo" />
                        <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            <h4 style="text-align: center">Sin datos para mostrar</h4>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>

            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2>Tallas Asignadas Al Estilo:
                            <asp:Label Text="" ID="lblEstiloSeleccionado" CssClass="text-danger" runat="server" />
                        </h2>
                        <div>
                            <asp:GridView ID="GridTallasDetalle" OnRowCommand="GridTallasDetalle_RowCommand" AutoGenerateColumns="false" DataKeyNames="idestilotalla" HeaderStyle-CssClass="backcolorheadergrid2"
                                CssClass="table table-hover table-striped" GridLines="None" Width="100%" runat="server" AllowPaging="true" PageSize="5" OnPageIndexChanging="GridTallasDetalle_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblidbundle" runat="server" Text='<%#Bind("idestilotalla") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Talla" HeaderText="Size" ItemStyle-Width="150px" />
                                    <asp:TemplateField HeaderText=" Especificacion">
                                        <ItemTemplate>
                                            <asp:LinkButton CommandName="nuevo" ID="LinkButton2" CssClass="btn btn-primary" runat="server">
                                                <i class="  glyphicon glyphicon-plus " aria-hidden="true"></i> Nuevo
                                            </asp:LinkButton>
                                            <asp:LinkButton CommandName="speck" ID="speck" CssClass="btn btn-success" runat="server">
                                                <i class=" glyphicon glyphicon-list " aria-hidden="true"></i> Detalle
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="Primero" LastPageText="Ultimo" />
                                <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    <h4 style="text-align: center">El estilo no posee tallas asignadas</h4>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>

                        <!-- Small modal -->
                        <button type="button" class="btn btn-info form-control" data-toggle="modal" data-target=".bs-example-modal-sm">Asignar Tallas al Estilo</button>

                        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="modaltallas">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" style="margin: 10px 10px 5px 10px">
                                                <asp:Button CssClass="btn btn-primary" Text="Actualizar Vista" ID="btnactualizarvistatallas" OnClick="btnactualizarvistatallas_Click" runat="server" />
                                            </div>
                                            <div class="form-group" style="height: 450px; overflow-y: scroll; margin: 10px">
                                                <asp:GridView ID="GridViewtallas" AutoGenerateColumns="false" DataKeyNames="idtalla"
                                                    CssClass="table table-hover table-striped" GridLines="None" Width="100%" runat="server">
                                                    <Columns>
                                                        <asp:TemplateField Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblidtallas" runat="server" Text='<%#Bind("idtalla") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="talla" HeaderText="Punto de Medida" ItemStyle-Width="100px" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox Text="Seleccionado" ID="chkSeleccionado" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        <h4 style="text-align: center">No se encuentran Tallas</h4>
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </div>
                                            <div class="form-group" style="margin: auto 10px 5px 10px">
                                                <asp:Button CssClass="btn btn-primary" Text="Guardar Datos" ID="btnAsignarTallas" OnClick="btnAsignarTallas_Click" runat="server" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2>Speck de la Talla:
                            <asp:Label Text="" ID="lbltallaSeleccionada" CssClass="text-info" runat="server" />
                        </h2>
                        <div>
                            <asp:GridView ID="gridespeck" OnRowCommand="gridespeck_RowCommand" AutoGenerateColumns="false" DataKeyNames="idEspecificacion"
                                CssClass="table table-hover table-striped" GridLines="None" Width="100%" runat="server">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblidespecificacion" runat="server" Text='<%#Bind("idEspecificacion") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="NombreMedida" HeaderText="Punto de Medida" ItemStyle-Width="100px" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtvalor" ModeType="Number"  CssClass="form-control" runat="server" Text='<%#Bind("Valor") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtvalor1" ModeType="Number" CssClass="form-control" runat="server" Text='<%#Bind("TolMax") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Button ID="btnActualizarTodos" OnClick="btnActualizarTodos_Click" CssClass="btn btn-primary" runat="server" Text="Actualizar" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtvalor2" ModeType="Number"  CssClass="form-control" runat="server" Text='<%#Bind("Tolmin") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <h4 style="text-align: center">Debe asignar punto de medida y valor a la talla</h4>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="border-left: 1px solid #cbcbcb">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Nueva Especificacion</h3>
                        <h3><span class="btn btn-info">Estilo: <span class="badge">
                            <asp:Label ID="lblestiloEspecificacion" runat="server" /></span>
                        </span>
                            <span class="btn btn-info">Talla: <span class="badge">
                                <asp:Label ID="lbltallaEspecificacion" runat="server" /></span>
                            </span>
                        </h3>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-inline">
                            <asp:HiddenField ID="hdnidestilotalla" runat="server" />
                        </div>
                        <div class="form-group">
                            Punto de Medida
                           <asp:DropDownList CssClass="form-control" runat="server" ID="drpPuntosMedidas" />
                        </div>
                        <div class="form-group">
                            Medida Talla
                            <asp:TextBox ID="txtvalorTalla" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            Tolerancia Maxima
                            <asp:TextBox ID="txtvalorMax" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            Tolerancia Minima
                            <asp:TextBox ID="txtvalorMin" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Button CssClass="btn btn-primary" Text="Guardar Datos" ID="guardarEspecificacion" OnClick="guardarEspecificacion_Click" runat="server" />
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</asp:Content>

