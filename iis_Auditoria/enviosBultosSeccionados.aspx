﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="enviosBultosSeccionados.aspx.cs" Inherits="enviosBultosSeccionados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    var user = document.getElementById("<%= HiddenFielduser.ClientID %>").value;
                    $.ajax({
                        url: "EnviosBultoXCorte.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "','user':'" + user + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);


                    if (ui.item.porder != "" && ui.item.idorder != "") {

                        //var currentURL = window.location.href + '&';
                        //alert(currentURL);

                        cadena = "enviosBultosSeccionados.aspx?idorder=" + ui.item.idorder + "&porder=" + ui.item.porder;

                        // alert(cadena);
                        window.history.replaceState('', '', cadena);
                    }

                    //var change = new RegExp('('+param+')=(.*)&', 'g'); 
                    //var newURL = currentURL.replace(change, '$1='+value+'&'); 

                    //if (getURLParameter(param) !== null){ 
                    //        try { 
                    //                window.history.replaceState('', '', newURL.slice(0, - 1) ); 
                    //        } catch (e) { 
                    //                console.log(e); 
                    //        } 
                    //} else { 
                    //        var currURL = window.location.href; 
                    //        if (currURL.indexOf("?") !== -1){ 
                    //                window.history.replaceState('', '', currentURL.slice(0, - 1) + '&' + param + '=' + value); 
                    //        } else { 
                    //                window.history.replaceState('', '', currentURL.slice(0, - 1) + '?' + param + '=' + value); 
                    //        } 
                    //} 

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

    <script>
        function openModal(e) {

            var obj = e.parentNode.parentNode;
            var objid = obj.cells[0].children[0];
            var objnum = obj.cells[7].children[0];

            var idenvio = objid.innerHTML;
            var acumulado = obj.cells[6].innerText;
            var enviocorte = objnum.value;

            //* document.getElementById("<>").innerHTML = idenvio;
            $('#<%=lblid.ClientID%>').val(idenvio);
            $('#<%=txtAcumuladoM.ClientID%>').val(acumulado);
            $('#<%=txtenvioCorteM.ClientID%>').val(enviocorte);


           

            mostrarmodal();
        }

        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function verificarDatos() {
            var idenvio = document.getElementById("<%= lblid.ClientID %>").value;
            var acumulado = document.getElementById("<%= txtAcumuladoM.ClientID %>").value;
            var enviocorte = document.getElementById("<%= txtenvioCorteM.ClientID %>").value;
            var envioDia = document.getElementById("<%= txtenviodiaM.ClientID %>").value;
            var acumuladoMod = document.getElementById("<%= txtunidadesenviarM.ClientID %>").value;

            if (idenvio != "" && isNaN(idenvio) == false && acumulado != "" && isNaN(acumulado) == false && enviocorte != "" && isNaN(enviocorte) == false
                && envioDia != "" && isNaN(envioDia) == false && acumulado != "" && isNaN(acumulado) == false && acumuladoMod != "" && isNaN(acumuladoMod) == false) {

                if (parseInt(acumulado) <= parseInt(acumuladoMod)) {

                    swal({
                        type: 'info',
                        title: 'Las Unidades a enviar deben ser menor que el acumulado'

                    });
                    
                    return false;

                }

            }
            else {
                swal({
                    type: 'info',
                    title: 'Llenar correctamente los campos'
                    
                });

             
                return false;
            }

            return true;
        }

        function mostrarmodal() {
            $('#ModalCapturaInfo').modal('show');

        }

        $(function () {
            $('#ModalCapturaInfo').on('shown.bs.modal', function (e) {
                $('#<%=txtunidadesenviarM.ClientID%>').focus();
                //  $('.focus').focus();
            })
        });
    </script>

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Algunos Bultos No Se Guardaron Correctamente !',
                type: 'info'
            });
        }

        function limpiar() {
            //alert();
            $('#<%=txtPorder.ClientID%>').val("");
            $('#<%=hfidPorder.ClientID%>').val("");
        }
    </script>

    <style>
        .mar_t {
            margin: 10px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>

    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="">
            <div class="panel panel-primary" style="margin-top: 10px">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Envios Seccionados </strong></h3>
                </div>
                <div class="panel-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="HiddenFielduser" runat="server" />


                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                Corte:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:HiddenField ID="hfidPorder" runat="server" />
                                            <asp:LinkButton Text="" OnClick="lnkMostrarBultosIncompletos_Click" CssClass="btn btn-default" ID="lnkMostrarBultosIncompletos" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" onfocus="limpiar();" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                <asp:LinkButton Text="" ID="lnkEnvio" OnClick="lnkEnvio_Click" CssClass="btn btn-info mar_t" runat="server"><i class="glyphicon glyphicon-arrow-left"> </i>Ir a envios </asp:LinkButton>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <%-- Envio Dia
                                    <asp:DropDownList ID="drpEnvioDia" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Select..." Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 Envio" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 Envio" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 Envio" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 Envio" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5 Envio" Value="5"></asp:ListItem>
                                    </asp:DropDownList>--%>


                                <%--   <asp:Button runat="server" ID="btnguardar" OnClick="btnguardar_Click" Text="Guardar Unidades" CssClass="btn btn-primary pull-right mar_t" />--%>

                                <%-- <asp:Button runat="server" ID="btnver" OnClick="btnver_Click" Text="Guardar Todos Seleccionados" CssClass="btn btn-primary pull-right mar_t" />--%>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            </div>
                            <style>
                                .w {
                                    width: 100%;
                                    min-width: 1000px;
                                }

                                .vis {
                                    display: none
                                }
                            </style>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="overflow-x: auto">
                                <%--   DataKeyNames="idEnvio" OnRowCommand="gridEnvios_RowCommand"--%>
                                <asp:GridView ID="gridEnvios" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" CssClass="table table-hover table-striped w" ShowFooter="true" AutoGenerateColumns="false" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField ControlStyle-CssClass="vis">
                                            <ItemTemplate>
                                                <asp:Label ID="lblidenvio" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("idEnvio") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Linea 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbllinea" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("Linea") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Porder
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPorder" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("POrder") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Secuencia
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblseq" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("NSeq") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Talla 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblsize" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Size") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Cantidad 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblquantity" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Quantity") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Acumulado
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblUni" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Cantidad") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Und. Envio
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                              <asp:TextBox runat="server" ID="txtenvio" Width="60px" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Envio") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Envio Corte
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtenvio" Width="60px" Font-Size="Medium" Enabled="false" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Envio") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnSelect" OnClientClick="openModal(this);" Text="Enviar" CssClass="btn btn-info" runat="server" />
                                                <%-- <asp:Button Text="Save" CommandName="enviar" CssClass="btn btn-default" runat="server" />
                                                <asp:CheckBox ID="checkEnvio" Text="Enviar" Checked="true" CssClass="btn btn-default" runat="server" />--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <aside class="modal fade " id="ModalCapturaInfo">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <article class="modal-header">
                                    <button id="cerrar" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>

                                    <h4 style="margin: 5px; padding: 5px;">Envio de bultos Seccionados</h4>

                                </article>
                                <article class="modal-body">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                Codigo de Modificacion
                                <asp:TextBox runat="server" CssClass="text-info form-control" ID="lblid" Enabled="false" />
                                            </div>
                                            <div class="form-group">
                                                Acumulado
                                <asp:TextBox runat="server" Enabled="false" CssClass="form-control" ID="txtAcumuladoM" />
                                            </div>
                                            <div class="form-group">
                                                Unidades a Enviar
                                <asp:TextBox runat="server" CssClass="form-control " onkeypress="return num(event);" ID="txtunidadesenviarM" />
                                            </div>
                                            <div class="form-group">
                                                Envio Corte
                                <asp:TextBox runat="server" CssClass="form-control" onkeypress="return num(event);" ID="txtenvioCorteM" />
                                            </div>
                                            <div class="form-group">
                                                Envio del Dia                                        
                                                <asp:TextBox runat="server" CssClass="form-control" onkeypress="return num(event);" ID="txtenviodiaM" />
                                            </div>
                                            <div class="form-group">
                                                Comentario de Validacion                                              
                                                <asp:TextBox runat="server" CssClass="form-control" MaxLength="50" TextMode="MultiLine" ID="txtjustificacionM" />
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="modal-footer">

                                    <asp:Button Text="Guardar Envio" ID="guardarEnvio" CssClass="btn btn-primary" OnClick="guardarEnvio_Click" OnClientClick="return verificarDatos()" runat="server" />
                                </article>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>

        </div>
    </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

