﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using SistemaAuditores.DataAccess;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using DevExpress.Web;


public partial class ShippingAudit : System.Web.UI.Page
{
    int total = 0, totaldef = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Load_Data();
        }
    }

    public void Load_Data()
    {
        lbfecha.Text = DateTime.Now.Date.ToShortDateString();
        DataTable shipping = new DataTable();
        DataColumn col = new DataColumn();
        col.ColumnName = "menu";
        shipping.Columns.Add(col);
        for (int i = 0; i <= 8; i++)
        {
            DataRow newrow = shipping.NewRow();
            newrow[0] = 0;
            shipping.Rows.Add(newrow);
        }
        GridView1.DataSource = shipping;
        GridView1.DataBind();
    }


    protected void Save_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow gvr in GridView1.Rows)
        {
            TextBox txtcodDef = gvr.FindControl("txtcodDef") as TextBox;
            ASPxComboBox txtTipDef = gvr.FindControl("txtTipDef") as ASPxComboBox;
            TextBox txtDefMay = gvr.FindControl("txtDefMay") as TextBox;
            TextBox txtDefMenor = gvr.FindControl("txtDefMenor") as TextBox;
            Label lblTotalqty = gvr.FindControl("lblTotalqty") as Label;
            Label lblTotalqtydef = gvr.FindControl("lblTotalqtydef") as Label;

            if ((txtcodDef.Text != null) && (txtTipDef.Text != null) & (txtDefMay.Text != null) & (lblTotalqty.Text != null) & (txtDefMenor.Text != null) & (lblTotalqtydef.Text != null))
            {
                ShippingAuditDA.SaveShippingAudit(1, 1, 1, Convert.ToInt32(txtDefMay.Text), Convert.ToInt32(txtDefMenor.Text), Convert.ToInt32(txtundAuditadas.Text),
                    Convert.ToInt32(txtmayorDefec.Text), Convert.ToInt32(CheckBoxList1.SelectedValue), lbfecha.Text);
            }
            else
            {
                string error = "alert('! Faltan campos')";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
                return;
            }
        }
    }
    protected void comboDef_SelectedIndexChanged(object sender, EventArgs e)
    {
        ASPxComboBox comboDef = (ASPxComboBox)sender;
        GridViewRow row = (GridViewRow)comboDef.NamingContainer;
        TextBox cod = (TextBox)row.FindControl("txtcodDef");
        cod.Text = comboDef.SelectedItem.Value.ToString();

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TextBox lblqy = (TextBox)e.Row.FindControl("txtDefMay");
            int qty = Int32.Parse(lblqy.Text);
            total = total + qty;

            TextBox lblqdefect = (TextBox)e.Row.FindControl("txtDefMenor");
            int qty1 = Int32.Parse(lblqdefect.Text);
            totaldef = qty1 + totaldef;
        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotalqty = (Label)e.Row.FindControl("lblTotalqty");
            lblTotalqty.Text = total.ToString();

            Label lblTotalDef = (Label)e.Row.FindControl("lblTotalqtydef");
            lblTotalDef.Text = totaldef.ToString();
        }
    }
}