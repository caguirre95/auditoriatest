﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SalidaSecadora : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            multiview.ActiveViewIndex = 0;
            CargaInfo();
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select top 15 p.POrderClient, MIN(s.Style) Estilo, SUM(p.Quantity) Totalcorte from POrder p join dbo.Style s on s.Id_Style = p.Id_Style"
                                          + " where s.washed = 1 and p.POrderClient like '%" + pre + "%' group by p.POrderClient order by LEN(p.POrderClient)", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porderclient = dt.Rows[i][0].ToString();
                obj.estilo = dt.Rows[i][1].ToString();
                obj.totalcorte = int.Parse(dt.Rows[i][2].ToString());

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porderclient { get; set; }
        public string estilo { get; set; }
        public int totalcorte { get; set; }
    }

    protected void btnagregar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtPorder.Text.Trim() != string.Empty)
            {
                string resp = OrderDetailDA.AgregarCorteSecado(txtPorder.Text.Trim(), hfestilo.Value, int.Parse(hftotalc.Value));

                if (resp == "OK")
                {
                    //ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }

                Limpiar();

                CargaInfo();
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnver_Click(object sender, EventArgs e)
    {
        try
        {
            CargaInfo();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnvercorte_Click(object sender, EventArgs e)
    {
        try
        {
            CargaInfoCorte();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    void CargaInfo()
    {
        try
        {
            DataTable dt;
            string user = HttpContext.Current.User.Identity.Name;

            //string queryCortes = " select p.POrderClient, MIN(s.Style) Estilo, SUM(p.Quantity) Totalcorte, ISNULL(ins.unidadesSecadas,0) Secadas from dbo.POrder p"
            //                   + " join dbo.Style s on s.Id_Style = p.Id_Style left join Intexdbo.intexSecado ins on ins.corteCompleto = p.POrderClient"
            //                   + " left join Intexdbo.intexPlanSecado ips on ips.corteCompleto = p.POrderClient"
            //                   + " where CONVERT(date, ips.fechaPlanSecado) = CONVERT(date, GETDATE()) and ins.estado != 'C'"
            //                   + " group by p.POrderClient, ins.unidadesSecadas order by ISNULL(ins.unidadesSecadas,0) desc";

            string queryCortes = " select a.POrderClient, a.Estilo, a.Totalcorte, b.TotalEnviado, a.Secadas from"
                               + " (select p.POrderClient, MIN(s.Style) Estilo, SUM(p.Quantity) Totalcorte, ISNULL(ins.unidadesSecadas, 0) Secadas from dbo.POrder p"
                               + " join dbo.Style s on s.Id_Style = p.Id_Style left join Intexdbo.intexSecado ins on ins.corteCompleto = p.POrderClient"
                               + " left join Intexdbo.intexPlanSecado ips on ips.corteCompleto = p.POrderClient where CONVERT(date, ips.fechaPlanSecado) = CONVERT(date, GETDATE()) and ins.estado != 'C'"
                               + " group by p.POrderClient, ins.unidadesSecadas) a join (select a.POrderClient, a.TotalEnviado from (select p.POrderClient, SUM(h.cantidadagregada) TotalEnviado"
                               + " from POrder p join Bundle b on b.Id_Order = p.Id_Order join tblEnviosbulto e on e.idBulto = b.Id_Bundle"
                               + " join tblhistoricoenvio h on h.idEnvio = e.idEnvio join Style s on s.Id_Style = p.Id_Style where isnull(h.confirmacionEnvio,0) != 0"
                               + " and CONVERT(date, h.fechaEnvio) <= CONVERT(date, GETDATE()) and s.washed = 1 Group by p.POrderClient) a) b on a.POrderClient = b.POrderClient order by a.Secadas desc";

            dt = DataAccess.Get_DataTable(queryCortes);

            gridEnvios.DataSource = dt;
            gridEnvios.DataBind();

        }
        catch (Exception)
        {
            throw;
        }
    }

    void CargaInfoCorte()
    {
        try
        {
            DataTable dt;
            string user = HttpContext.Current.User.Identity.Name;

            //string queryCortes = " select p.POrderClient, MIN(s.Style) Estilo, SUM(p.Quantity) Totalcorte, ISNULL(ins.unidadesSecadas,0) Secadas from dbo.POrder p"
            //                   + " join dbo.Style s on s.Id_Style = p.Id_Style left join Intexdbo.intexSecado ins on ins.corteCompleto = p.POrderClient"
            //                   + " left join Intexdbo.intexPlanSecado ips on ips.corteCompleto = p.POrderClient"
            //                   + " where p.POrderClient = '" + txtPorder.Text.Trim() + "' and ins.estado != 'C'"
            //                   + " group by p.POrderClient, ins.unidadesSecadas";

            //string queryCortes = " select a.POrderClient, a.Estilo, a.Totalcorte, b.TotalEnviado, a.Secadas from"
            //                   + " (select p.POrderClient, MIN(s.Style) Estilo, SUM(p.Quantity) Totalcorte, ISNULL(ins.unidadesSecadas, 0) Secadas from dbo.POrder p"
            //                   + " join dbo.Style s on s.Id_Style = p.Id_Style left join Intexdbo.intexSecado ins on ins.corteCompleto = p.POrderClient"
            //                   + " left join Intexdbo.intexPlanSecado ips on ips.corteCompleto = p.POrderClient where CONVERT(date, ips.fechaPlanSecado) = CONVERT(date, GETDATE()) and ins.estado != 'C'"
            //                   + " and p.POrderClient = '" + txtPorder.Text.Trim() + "' group by p.POrderClient, ins.unidadesSecadas) a join (select a.POrderClient, a.TotalEnviado from (select p.POrderClient, SUM(h.cantidadagregada) TotalEnviado"
            //                   + " from POrder p join Bundle b on b.Id_Order = p.Id_Order join tblEnviosbulto e on e.idBulto = b.Id_Bundle"
            //                   + " join tblhistoricoenvio h on h.idEnvio = e.idEnvio join Style s on s.Id_Style = p.Id_Style where isnull(h.confirmacionEnvio,0) != 0"
            //                   + " and CONVERT(date, h.fechaEnvio) <= CONVERT(date, GETDATE()) and s.washed = 1 and p.POrderClient = '" + txtPorder.Text.Trim() + "' Group by p.POrderClient) a) b on a.POrderClient = b.POrderClient order by a.Secadas desc";

            string queryCortes = " select a.POrderClient, a.Estilo, a.Totalcorte, ISNULL(b.TotalEnviado,0) TotalEnviado, a.Secadas from"
                               + " (select p.POrderClient, MIN(s.Style) Estilo, SUM(p.Quantity) Totalcorte, ISNULL(ins.unidadesSecadas, 0) Secadas from dbo.POrder p"
                               + " join dbo.Style s on s.Id_Style = p.Id_Style join Intexdbo.intexSecado ins on ins.corteCompleto = p.POrderClient where p.POrderClient = '" + txtPorder.Text.Trim() + "'"
                               + " group by p.POrderClient, ins.unidadesSecadas) a join (select a.POrderClient, a.TotalEnviado from (select p.POrderClient, SUM(h.cantidadagregada) TotalEnviado"
                               + " from POrder p join Bundle b on b.Id_Order = p.Id_Order join tblEnviosbulto e on e.idBulto = b.Id_Bundle join tblhistoricoenvio h on h.idEnvio = e.idEnvio"
                               + " join Style s on s.Id_Style = p.Id_Style where isnull(h.confirmacionEnvio,0) != 0 and CONVERT(date, h.fechaEnvio) <= CONVERT(date, GETDATE())"
                               + " and s.washed = 1 and p.POrderClient = '" + txtPorder.Text.Trim() + "' Group by p.POrderClient) a) b on a.POrderClient = b.POrderClient order by a.Secadas desc";

            dt = DataAccess.Get_DataTable(queryCortes);

            gridEnvios.DataSource = dt;
            gridEnvios.DataBind();

        }
        catch (Exception)
        {
            throw;
        }
    }

    void Limpiar()
    {
        txtPorder.Text = "";
        hfestilo.Value = "";
        hftotalc.Value = "";
    }

    protected void gridEnvios_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "AgregarCT")
            {
                GridViewRow item = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int indice = item.RowIndex;
                string corteCompleto = gridEnvios.DataKeys[indice].Value.ToString();

                var units = (TextBox)item.FindControl("txtunits");
                var estilo = (Label)item.FindControl("lblestilo");
                var cantidadRecibida = (Label)item.FindControl("lblcantidadrecb");

                if (units.Text != string.Empty)
                {
                    string resp = OrderDetailDA.AgregarCantidadBultoSecado(corteCompleto, int.Parse(units.Text), estilo.Text, int.Parse(cantidadRecibida.Text), Context.User.Identity.Name);

                    if (resp.Equals("Exito"))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave();", true);
                    }

                    CargaInfo();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
                }

            }
            if (e.CommandName == "AgregarCE")
            {
                GridViewRow item = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int indice = item.RowIndex;
                string corteCompleto = gridEnvios.DataKeys[indice].Value.ToString();

                var estilo = (Label)item.FindControl("lblestilo");
                var cantidadRecibida = (Label)item.FindControl("lblcantidadrecb");

                string resp = OrderDetailDA.AgregarCantidadBultoSecado(corteCompleto, 24, estilo.Text, int.Parse(cantidadRecibida.Text), Context.User.Identity.Name);

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                    CargaInfo();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave();", true);
                }
            }
        }
        catch (Exception ex)
        {
            //throw;
        }
    }

    protected void gridEnvios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gridEnvios.PageIndex = e.NewPageIndex;
        CargaInfo();
    }
}