﻿<%@ Page Title="Sistema de Auditores" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BultoxBulto.aspx.cs" Inherits="_InspeccionxBulto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v16.2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="scripts/footable.min.js"></script>
    <script type="text/javascript">
        function successalert() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
    </script>
    <script type="text/javascript">
        function warningalert() {
            swal({
                title: "¿Estas seguro?",
                text: "¿Deseas ingresar este registro?",
                type: "warning"
            })
        }
    </script>
    <script type="text/javascript">
        function pageLoad() { // this gets fired when the UpdatePanel.Update() completes
            ReBindMyStuff();
        }
        function ReBindMyStuff() { // create the rebinding logic in here
            $('[id*=Repeater]').footable();
        }
    </script>

    <%--  <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="font-awesome/fonts.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2>
        <asp:Label ID="lbl" runat="server" ForeColor="White" Text="Bundles Check Off"></asp:Label></h2>
    <br />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="margin-left: 10px;">
        <%-- <label style="">From Production Order:</label>--%>
        <label id="frprododr">
            <asp:Label ID="lblorder1" Visible="false" runat="server" Text=""></asp:Label></label>
        <%-- <label>To </label>--%>
        <%-- <label id="lbtoprod">Production Order:</label>--%>
        <label id="toprododr">
            <asp:Label ID="lblorder2" runat="server" Visible="false" Text=""></asp:Label></label>
        <%-- <strong>All Dates</strong>--%>
        <br />

    </div>

    <div class="container-fluid">
        <div class="col-lg-12 centrar">
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/img/system_search.png" PostBackUrl="~/Search.aspx?id_seccion=5" /><asp:LinkButton ID="lnkNew" runat="server" OnClick="lnkNew_Click">New Search</asp:LinkButton>

        </div>
    </div>

    <div class="container " style="padding-bottom: 5px;">
        <div class="col-lg-2">
            <strong>Customer:</strong>
            <asp:Label ID="lbclient" runat="server" CssClass="main">
                <asp:Label ID="lblcustomer" runat="server" Text=""></asp:Label></asp:Label>
        </div>

    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container-fluid info_ord">
        <div class="container " style="padding-bottom: 5px;">
            <div class="col-lg-2 col-lg-offset-1">
                <asp:Label ID="Label10" runat="server" Text="Corte#:"></asp:Label>
                <asp:TextBox ID="txtporder" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label28" runat="server" Text="Estilo"></asp:Label>
                <asp:TextBox ID="txtstyle" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label29" runat="server" Text="Descripcion"></asp:Label>
                <asp:TextBox ID="txtdescrip" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label30" runat="server" Text="Cantidad"></asp:Label>
                <asp:TextBox ID="txtcantidad" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label31" runat="server" Text="Bultos"></asp:Label>
                <asp:TextBox ID="txtbultos" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="Grid" runat="server">
            <asp:GridView ID="GridBultos" OnRowCommand="GridBultos_RowCommand" OnRowDataBound="GridBultos_RowDataBound" AutoGenerateColumns="false" DataKeyNames="Id_Bundle"
                CssClass="table table-hover table-striped" GridLines="None" Width="100%" runat="server">
                <Columns>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblidbundle" runat="server" Text='<%#Bind("Id_Bundle") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="NSeq" HeaderText="Seq" />
                    <asp:BoundField DataField="Bld" HeaderText="Bulto" />
                    <asp:BoundField DataField="Size" HeaderText="Talla" />
                    <asp:BoundField DataField="Color" HeaderText="Color" />
                    <asp:BoundField DataField="Quantity" HeaderText="Cantidad" />
                    <asp:BoundField DataField="Muestreo" HeaderText="Muestreo" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                <asp:ListItem Value="0" Text="Rechazado"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Aprobado"></asp:ListItem>
                            </asp:RadioButtonList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Button ID="btnGuardarTodos" OnClick="btnGuardarTodos_Click" CssClass="btn btn-primary" runat="server" Text="Aprobar Todos" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton CommandName="Editar" ID="comando" CssClass="btn btn-default"  runat="server">
                                     <i class="fa fa-check-circle marApRe" aria-hidden="true"></i>  
                                             
                                     <i class="fa fa-times-circle" aria-hidden="true"></i>
                            </asp:LinkButton>
                              <asp:LinkButton CommandName="Reparar" ID="comandoRep" CssClass="btn btn-success"   runat="server">
                                     <i class="glyphicon glyphicon-wrench" aria-hidden="true"></i> Reparar
                                </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <h4 style="text-align: center">Sin datos para mostrar</h4>
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <asp:Label ID="lblnoRows" runat="server" Text=""></asp:Label>
        </div>
    </div>

    <%-- <dx:ASPxPopupControl ID="ReporteLinea" runat="server" EnableTheming="True" ClientInstanceName="AsignarDefectos"
        HeaderText="Control de Auditoria x Bihorario" Modal="True" Theme="MetropolisBlue"
        AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        Width="800px" MaxHeight="600px" CloseAction="CloseButton">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <dx:ASPxPanel ID="ASPxPanel1" runat="server">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent2" runat="server">
                            <div class="col-md-12" style="margin-bottom: 20px;">
                                <center>
                                <div style="display:table-row;">
                                <div style="display:table-cell;">
                                <b>BiHorario:</b>&nbsp;&nbsp;
                                </div>
                                <div style="display:table-cell;">
                                <dx:ASPxComboBox ID="cbxbihorario" CssClass="ComboCenter" runat="server" 
                                    ValueType="System.String" TextField="bihorario" ValueField="id_bio"
                                    OnSelectedIndexChanged="cbxbihorario_SelectedIndexChanged" Theme="Moderno" 
                                        AutoPostBack="True">
                                </dx:ASPxComboBox>
                                </div>
                               </div>
                               </center>
                                <div class="col-md-6" style="text-align: left; margin-top: 20px;">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label4" runat="server" Text="AQL" Style="font-size: 18px; font-weight: bold;"></asp:Label>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 20px;">
                                        <asp:Label ID="Label6" runat="server" Text="Reporte De Auditoria Por Bulto" Style="font-size: 18px; font-weight: bold;"></asp:Label>
                                        <div class="col-md-6">
                                            <asp:Label ID="Label7" runat="server" Text="Aceptados: "></asp:Label>
                                            <asp:Label ID="Label8" runat="server" Text="Rechazados: "></asp:Label>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblAcep" runat="server" Width="100%"></asp:Label>
                                            <asp:Label ID="lblReje" runat="server" Width="100%"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <asp:Label ID="lblaudit_bdl_pzs" runat="server" Text="Total Bultos Auditados/Piezas:" Style="font-size: 18px; font-weight: bold;"></asp:Label>
                                        <asp:Label ID="lbltotbultau" runat="server" Style="font-size: 18px; font-weight: bold;"></asp:Label>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 20px;">
                                        <asp:Label ID="Label9" runat="server" Text="% Calidad:" Style="font-size: 18px; font-weight: bold;"></asp:Label>
                                        <asp:Label ID="lblcalidad" runat="server" Style="font-size: 18px; font-weight: bold;"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-6" style="margin-top: 20px;">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label16" runat="server" Text="OQL" Style="font-size: 18px; font-weight: bold;"></asp:Label>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 20px;">
                                        <asp:Label ID="Label17" runat="server" Text="Piezas Auditadas" Width="100%" Style="font-size: 18px; font-weight: bold;"></asp:Label>
                                        <div class="col-md-6">
                                            <asp:Label ID="Label18" runat="server" Text="Aceptados: "></asp:Label>
                                            <asp:Label ID="Label19" runat="server" Text="Rechazados: "></asp:Label>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblOQLacep" runat="server" Width="100%"></asp:Label>
                                            <asp:Label ID="lblOQLrec" runat="server" Width="100%"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <asp:Label ID="Label20" Text="Total de Piezas:" Style="font-weight: bold;" runat="server"></asp:Label>
                                        <asp:Label ID="lblOQLtotp" runat="server" Width="100%" Style="font-weight: bold; margin-bottom: 20px;"></asp:Label>
                                    </div>
                                    <div class="col-md-12">
                                        <asp:Label ID="Label21" Text="% Rechazo:" runat="server"></asp:Label>
                                        <asp:Label ID="lblOQLporRech" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-md-12">
                                        <asp:Label ID="Label22" Text="% Aceptación:" runat="server"></asp:Label>
                                        <asp:Label ID="lblOQLporAcep" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <center>
                                <div class="col-md-12" style="margin-top:20px;">
                                    <asp:Label ID="Label23" runat="server" Text="% DE SECCION" Style="font-size: 18px; font-weight: bold;"></asp:Label>
                                </div>
                                <div class="col-md-3" style="border-left:1px solid #ccc;border-top:1px solid #ccc;border-bottom:1px solid #ccc;">
                                    <asp:Label ID="Label24" runat="server" Text="TRASERO %:"></asp:Label>
                                    <asp:Label ID="lblSecTra" runat="server" Width="100%"></asp:Label>
                                </div>
                                <div class="col-md-3" style="border-left:1px solid #ccc;border-top:1px solid #ccc;border-bottom:1px solid #ccc;">
                                    <asp:Label ID="Label25" runat="server" Text="DELANTERO %:"></asp:Label>
                                    <asp:Label ID="lblSecDel" runat="server" Width="100%"></asp:Label>
                                </div>
                                <div class="col-md-3" style="border-left:1px solid #ccc;border-top:1px solid #ccc;border-bottom:1px solid #ccc;">
                                    <asp:Label ID="Label26" runat="server" Text="ENSAMBLE I %:"></asp:Label>
                                    <asp:Label ID="lblSecEn1" runat="server" Width="100%"></asp:Label>
                                </div>
                                <div class="col-md-3" style="border-left:1px solid #ccc;border-right:1px solid #ccc;border-top:1px solid #ccc;border-bottom:1px solid #ccc;">
                                    <asp:Label ID="Label27" runat="server" Text="ENSAMBLE II %:"></asp:Label>
                                    <asp:Label ID="lblSecEn2" runat="server" Width="100%"></asp:Label>
                                </div>
                                </center>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>--%>
</asp:Content>
