﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />    
    <script type="text/javascript" src="jquery-1.9.1.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

    <style type="text/css">
        .margenes {
            margin-top: 10%;
            padding-bottom: 10px;
            padding-top: 10px;
        }

        .cent {
            text-align: center;
        }

        .mar {
            margin-left: 5px !important;
            background-color: transparent;
            color: whitesmoke;
            border: none;
            border-bottom: 1px solid white;
            display: block;
            width: 100%;
            padding: 6px 12px;
            margin-top: 10px !important;
        }

            .mar:focus {
                outline: none;
            }

        .str {
            font-size: larger;
            padding-left: 5px;
        }

        .boxsh {
            background-color: rgba(0,0,0,0.4);
            -webkit-box-shadow: 0px 3px 34px -8px rgba(255,255,255,1);
            -moz-box-shadow: 0px 3px 34px -8px rgba(255,255,255,1);
            box-shadow: 0px 3px 34px -8px rgba(255,255,255,1);
            color: white;
        }

        body {
            background: #2069a1;
        }

        .w {
            width: 220px;
            margin: auto;
        }

        input:-webkit-autofill, textarea:-webkit-autofill, select:-webkit-autofill {
            background-color: transparent !important;
            color: black;
        }

        input:-webkit-autofill {
            background: #FFF !important;
        }
    </style>

</head>
<body class="body_login">
    <form id="form1" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-xs-10  col-sm-7 col-md-4 col-lg-4 col-xs-offset-1 col-sm-offset-3 col-md-offset-4 col-lg-offset-4 panel panel-default margenes boxsh">
                    <div class="col-lg-12">
                        <img src="img/log.png" alt="logo" width="180px" class="img-thumbnail center-block" />
                        <hr />
                    </div>

                    <div class="col-lg-12">
                        <asp:Login>
                        </asp:Login>
                        <asp:Login ID="UserLogin" 
                            runat="server" 
                            CssClass="" 
                            TitleText="Inicio de Sesión Control de Auditores" 
                            RenderOuterTable="true"
                            TitleTextStyle-CssClass="login-title" 
                            DisplayRememberMe="True" 
                            DestinationPageUrl="~/Default.aspx"
                            TextBoxStyle-CssClass=" mar"
                            LoginButtonStyle-CssClass="btn btn-default  w"
                            LoginButtonText="Iniciar sesión" 
                            RememberMeText="Recordarme"
                            LabelStyle-Font-Size="Medium"
                            CheckBoxStyle-CssClass="cent"
                            UserNameLabelText="Usuario:" 
                            PasswordLabelText="Contraseña:" 
                            FailureText="Inicio de sesión fallido, intente de nuevo"
                            PasswordRequiredErrorMessage="Favor especificar contraseña" 
                            UserNameRequiredErrorMessage="Favor especificar usuario"
                            Orientation="Vertical" 
                            FailureAction="RedirectToLoginPage">
                        </asp:Login>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
