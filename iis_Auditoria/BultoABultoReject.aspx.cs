﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using SistemaAuditores.DataAccess;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using DevExpress.Web;
using System.Net.Mail;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;

public partial class BultoABultoReject : System.Web.UI.Page
{
    int total = 0, totaldef = 0;
    double porcentaje = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!this.IsPostBack)
            {
                if (User.Identity.IsAuthenticated == true)
                {
                    int Id_Seccion = Convert.ToInt32(Request.QueryString["id_seccion"]);
                    cargacombo();
                    comboSeccion.SelectedValue =Id_Seccion.ToString();
                    Load_Data(Id_Seccion);
                }
                else
                {
                    // Response.Redirect("");
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    /// <summary>
    /// utilizado 1
    /// </summary>
    void cargacombo()
    {
        
        int Id_Linea = Convert.ToInt32(Request.QueryString["id_linea"]);

        DataTable dt_comboSeccion = DataAccess.Get_DataTable(" select S.id_seccion , M.Modulo from Modulo M  join Seccion S ON M.id_modulo=s.idModulo where S.id_linea=" + Id_Linea);

        comboSeccion.DataSource = dt_comboSeccion;
        comboSeccion.DataTextField = "Modulo";
        comboSeccion.DataValueField = "id_seccion";
        comboSeccion.DataBind();

        DataTable defectos_dt = new DataTable();
        defectos_dt = DataAccess.Get_DataTable(" select id_defectos, nombre from Defectos where Categoria='C'");

        GridViewdefectos.DataSource = defectos_dt;
        GridViewdefectos.DataBind();

        DataTable linea_dt = new DataTable();
        linea_dt = DataAccess.Get_DataTable(" select id_linea,numero"
                                          + " from linea l join (select l.id_planta from Linea l join planta p on l.id_planta = p.id_planta where l.id_linea = " + Id_Linea + ") as p"
                                          + " on l.id_planta = p.id_planta where l.estado = 'True' order by CONVERT(numeric, numero) asc");

        drplinea.DataSource = linea_dt;
        drplinea.DataTextField = "numero";
        drplinea.DataValueField = "id_linea";
        drplinea.DataBind();

      
    }

    /// <summary>
    /// utilizado 2
    /// </summary>
    /// <param name="Id_Seccion"></param>
    public void Load_Data(int Id_Seccion)
    {
        try
        {
            string Id_Order = (string)Request.QueryString["IdOrder"];
            if (Id_Order == "")
            {
                Response.Redirect("Secciones.aspx");
                return;
            }

            int Id_Linea = Convert.ToInt32(Request.QueryString["id_linea"]);
            lblcustomerval.Text = (string)Request.QueryString["customer"];
            lblstylesval.Text = (string)Request.QueryString["style"];
            string IdBundle = Request.QueryString["IdBundle"];
            lblfechaval.Text = DateTime.Now.ToShortDateString();

            MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
            string UserId = mu.ProviderUserKey.ToString();

            DataTable dt_Inspector = DataAccess.Get_DataTable("select nombre from Inspector where UserId='" + UserId + "'");
            string NombreInspector = Convert.ToString(dt_Inspector.Rows[0][0]);
            DataTable dt_Seccion = DataAccess.Get_DataTable("select m.Modulo from Seccion s join modulo m on s.idModulo=m.id_modulo  where id_seccion=" + Id_Seccion);
            string Seccion = Convert.ToString(dt_Seccion.Rows[0][0]);

            lblseccionval.Text = Seccion;
            lblinspectorval.Text = NombreInspector;

            DataTable dt_Linea = DataAccess.Get_DataTable("select l.numero,p.id_planta,p.descripcion from linea l join planta p on l.id_planta=p.id_planta where l.id_linea=" + Id_Linea);

            lineaval.Text = Convert.ToString(dt_Linea.Rows[0][0]);
            int Id_Planta = Convert.ToInt32(dt_Linea.Rows[0][1]);
            lblplantaval.Text = Convert.ToString(dt_Linea.Rows[0][2]);

            //DataTable dt_Supervisor = DataAccess.Get_DataTable("select nombre from Supervisor where Id_Planta= " + Id_Planta + " and id_linea= " + Id_Linea + " and id_seccion=" + Id_Seccion);
            //lblsupervalue.Text = Convert.ToString(dt_Supervisor.Rows[0][0]);

            string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));
            DateTime fecha_actual = DateTime.Now;
            string sqlfecha_actual = fecha_actual.ToString("yyyy-MM-dd");

            int Id_Biohorario = 0;
            DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio, bihorario FROM Biohorario WHERE @input between rango1 and rango2");
            if (Bihorarios.Rows.Count > 0)
            {
                Id_Biohorario = Convert.ToInt32(Bihorarios.Rows[0][0]);
                lblbio.Text = "Biohorario " + Convert.ToString(Bihorarios.Rows[0][1]);
            }


            DataTable gridInspecLinea = new DataTable();

            string query = "";

            query = " SELECT row_number() OVER (ORDER BY O.id_operario) AS id, O.codigo AS Codigo, O.nombre as Nombre, Op.descripcion_completa as Operacion,Oper.id_operarioOpe as idOpOp"
                   + " FROM Operario O"
                   + " inner join OperacionOperario Oper on O.id_operario = Oper.id_operario"
                   + " inner join  Operacion Op ON Oper.id_operacion = OP.id_operacion"
                   + " where OPER.id_seccion =" + Id_Seccion.ToString()
                   + " and Oper.estado='true'";// O.id_operario not in (select id_operarioOpe from Ausencia where id_biohorario = 4 and fecha = '2018-02-26')";



            gridInspecLinea = DataAccess.Get_DataTable(query);
            DataColumn Muestreo = new DataColumn("Muestreo", typeof(string));
            gridInspecLinea.Columns.Add(Muestreo);

            string Id_Bundle = Request.QueryString["IdBundle"];
            DataTable dt_Bundle = DataAccess.Get_DataTable("select Quantity from Bundle where Id_Bundle=" + Id_Bundle + " and Id_Order=" + Id_Order);
            int Cantidad_Bundle = Convert.ToInt32(dt_Bundle.Rows[0][0]);
            DataTable dt_Muestreo = DataAccess.Get_DataTable("declare @input float =" + Cantidad_Bundle + " SELECT sample_size FROM Muestreo WHERE @input between rango_incial and rango_final and nivel_insp = (Select levelaql from AQL)");
            int Muestra = 0;

            for (int i = 0; i <= gridInspecLinea.Rows.Count - 1; i++)
            {
                gridInspecLinea.Rows[i][5] = Muestra;
            }

            DataColumn CantidadDefectos = new DataColumn("CantidadDefectos", typeof(string));
            gridInspecLinea.Columns.Add(CantidadDefectos);
            DataTable dt_bundlerejected = new DataTable();
            

            for (int i = 0; i <= gridInspecLinea.Rows.Count - 1; i++)
            {
                string query_sum_defectos = "select sum(cantidad_defectos) as cantidad_defectos from BundleRejectedBulto where Id_Bundle=" + IdBundle + " and id_operarioOpe=" + Convert.ToString(gridInspecLinea.Rows[i][4]) + "   group by id_operarioOpe";
                //string query_sum_defectos = "select sum(cantidad_defectos) as cantidad_defectos from BundleRejectedBulto where Id_Bundle=" + IdBundle + " and id_operarioOpe=" + Convert.ToString(gridInspecLinea.Rows[i][4]) + " and id_bio=" + Id_Biohorario + "  group by id_operarioOpe";

                //string query_sum_defectos = "select sum(cantidad_defectos) as cantidad_defectos from BundleRejectedBulto where Id_Bundle=" + IdBundle + " and id_operarioOpe=" + Convert.ToString(gridInspecLinea.Rows[i][4]) + " and id_bio=" + Id_Biohorario + " and FechaRecibido='" + sqlfecha_actual + "' group by id_operarioOpe";

                dt_bundlerejected = DataAccess.Get_DataTable(query_sum_defectos);
                if (dt_bundlerejected.Rows.Count > 0)
                {
                    gridInspecLinea.Rows[i][6] = dt_bundlerejected.Rows[0][0];
                }
                else
                {
                    gridInspecLinea.Rows[i][6] = 0;
                }
            }
            grv_insp_linea.DataSource = gridInspecLinea;
            grv_insp_linea.DataBind();
        }
        catch (Exception)
        {
            throw;
        }

    }

    /// <summary>
    /// utilizado 3
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grv_insp_linea_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblqy = (Label)e.Row.FindControl("lblmuestreo");
            int qty = Int32.Parse(lblqy.Text);
            total = total + qty;

            TextBox lblqdefect = (TextBox)e.Row.FindControl("txtdefectos");
            int qty1 = Int32.Parse(lblqdefect.Text);
            totaldef = qty1 + totaldef;

            porcentaje = Convert.ToDouble(totaldef) / Convert.ToDouble(total);
            porcentaje = porcentaje * 100;
            porcentaje = Math.Round(porcentaje, 2);
        }

        //lblTotalqty.Text = total.ToString();
        //lblTotaldefect.Text = totaldef.ToString();
        //lblTPorDef.Text = Convert.ToString(porcentaje) + "%";
    }

    /// <summary>
    /// utilizado 4
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void comboSeccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Load_Data(int.Parse(comboSeccion.SelectedValue));
        }
        catch (Exception)
        {

            throw;
        }
    }

    /// <summary>
    /// utilizado 5
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ASPxButton8_Click(object sender, EventArgs e)
    {
        try
        {

            Button btn = (Button)sender;
            GridViewRow gv = (GridViewRow)(btn.Parent.Parent);
            Label lbloper = (Label)gv.FindControl("lbloperario");
            Label lblidopop = (Label)gv.FindControl("lblidopop");
            Load_ControlBultos(lbloper.Text, lblidopop.Text);

        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// carga grid defectos para asignar 6
    /// </summary>
    /// <param name="nomb"></param>
    public void Load_ControlBultos(string nomb, string idopop)
    {
        lbloper.Text = nomb;
        hdIdOperOper.Value = idopop;
        DataTable defectos_dt = new DataTable();
        defectos_dt = DataAccess.Get_DataTable("select id_defectos, nombre from Defectos where id_defectos>1");

        grvDefectosEncontrados.DataSource = defectos_dt;
        grvDefectosEncontrados.DataBind();

        Button1.Enabled = true;
        RechazoBultos.ShowOnPageLoad = true;
    }

    /// <summary>
    /// utilizado 7
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnsave_Click(object sender, EventArgs e)
    {    
          
        GuardarDefecto(grvDefectosEncontrados,false);
    }

    protected void btnSaveAyuda_Click(object sender, EventArgs e)
    {
        hdIdOperOper.Value = hdfIdOperacionOp.Value;
        GuardarDefecto(GridViewdefectos,true);
    }

    void GuardarDefecto(GridView DefectosEncontrados,bool ayuda)
    {
        try
        {
            Button1.Enabled = false; //BOTON SALVAR BUNDLE REJECTED
            string IdBundle = Request.QueryString["IdBundle"].ToString();
            string IdOrder = Request.QueryString["IdOrder"].ToString();
            int Id_Linea = Convert.ToInt32(Request.QueryString["id_linea"]);
            int Id_Seccion = Convert.ToInt32(Request.QueryString["id_seccion"]);

            MembershipUser user = Membership.GetUser(Page.User.Identity.Name);
            string UserId = user.ProviderUserKey.ToString();

            DataTable dt_Inspector = DataAccess.Get_DataTable("select si.idseccionInspector from seccionInspector si join Inspector i on si.idInspector=i.id_inspector where i.UserId='" + UserId + "' and si.idseccion=" + Id_Seccion);
            string id_inspector = Convert.ToString(dt_Inspector.Rows[0][0]);

            DataTable dt_Linea = DataAccess.Get_DataTable("select id_planta, numero from Linea where id_linea=" + Id_Linea);
            int Id_Planta = Convert.ToInt32(dt_Linea.Rows[0][0]);


            int Id_Biohorario = 0;
            string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));
            string recibido = DateTime.Now.ToShortTimeString();

            DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio FROM Biohorario WHERE @input between rango1 and rango2");
            if (Bihorarios.Rows.Count > 0)
            {
                Id_Biohorario = Convert.ToInt32(Bihorarios.Rows[0][0]);
            }

            DataTable dt_Bundle = DataAccess.Get_DataTable("select Quantity from Bundle where Id_Bundle=" + IdBundle + " and Id_Order=" + IdOrder);
            int Cantidad_Bundle = Convert.ToInt32(dt_Bundle.Rows[0][0]);
            DataTable dt_IdMuestreo = DataAccess.Get_DataTable("declare @input float =" + Cantidad_Bundle + " SELECT id_muestreo FROM Muestreo WHERE @input between rango_incial and rango_final and nivel_insp = (Select levelaql from AQL)");
            int Id_Muestreo = Convert.ToInt32(dt_IdMuestreo.Rows[0][0]);

            for (int i = 0; i < DefectosEncontrados.Rows.Count; i++)
            {
                CheckBox check = (CheckBox)DefectosEncontrados.Rows[i].Cells[0].FindControl("chkdefectos");

                if (check.Checked)
                {
                    TextBox txtcantidad = (TextBox)DefectosEncontrados.Rows[i].Cells[3].FindControl("txtcantidad");

                    if (txtcantidad.Text != "")
                    {
                        try
                        {
                            Label lblIdDefecto = (Label)DefectosEncontrados.Rows[i].Cells[2].FindControl("lblIdDefecto");
                            //agregar la ayuda//pendiente
                            OrderDetailDA.SaveBundlesRejectedBulto(Convert.ToInt32(IdBundle), Convert.ToInt16(Id_Biohorario), int.Parse(lblIdDefecto.Text), int.Parse(hdIdOperOper.Value), int.Parse(id_inspector), Id_Muestreo, Convert.ToDateTime(lblfechaval.Text), Convert.ToString(recibido), Convert.ToInt16(txtcantidad.Text),ayuda);
                            //while (i == DefectosEncontrados.Rows.Count - 1)
                            //{
                            //    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                            //}
                        }
                        catch (Exception ex)
                        {
                            string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
                            return;
                        }
                    }
                    else
                    {
                        // string message = "alert('Por favor ingrese la cantidad!');";
                        //  ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
                        // return;
                    }

                }
            }

            #region MyRegion


            /********************************************************************************************************************************/
            //    if (txtCantidad1.ClientVisible == true)
            //{
            //    if (txtCantidad1.Text != "")
            //    {
            //        DataTable dt_IdAreaDef = new DataTable();
            //        dt_IdAreaDef = DataAccess.Get_DataTable("select Id_DefxArea from DefectosxArea where Id_defectos=" + Convert.ToInt32(ComboDefectos1.SelectedItem.Value) + " and Id_Area=" + Convert.ToInt32(ComboArea1.SelectedItem.Value));
            //        int IdAreaxDef = Convert.ToInt32(dt_IdAreaDef.Rows[0][0]);
            //        //TextBox txtcantidad = (TextBox)DefectosEncontrados.Rows[i].Cells[4].FindControl("txtcantidad");
            //        //DropDownList ddlSeccion = (DropDownList)DefectosEncontrados.Rows[i].Cells[3].FindControl("ddlSeccion");

            //        try
            //        {
            //            OrderDetailDA.SaveBundlesRejectedBulto(Convert.ToInt32(IdBundle), Convert.ToInt32(IdOrder), Convert.ToInt32(Id_Biohorario), Convert.ToInt32(ComboSeccion1.SelectedItem.Value), Id_Linea, IdAreaxDef, Convert.ToInt32(id_inspector), IdSupervisor, Idoperario, Id_Muestreo, Convert.ToDateTime(lblfechaval.Text), Convert.ToString(recibido), Convert.ToInt32(txtCantidad1.Text));
            //            //while (i == DefectosEncontrados.Rows.Count - 1)
            //            //{ ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true); }
            //        }
            //        catch (Exception ex)
            //        {
            //            MailMessage mailObj = new MailMessage("soporte@rocedes.com.ni", "emedina@rocedes.com.ni", "Sistema Auditoria", ex.ToString());
            //            SmtpClient SMTPServer = new SmtpClient("rocedes.com.ni");
            //            SMTPServer.Send(mailObj);
            //            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "fieldalert();", true);
            //        return;
            //    }
            //}



            //if (txtCantidad2.ClientVisible == true)
            //{
            //    if (txtCantidad2.Text != "")
            //    {
            //        DataTable dt_IdAreaDef2 = new DataTable();
            //        dt_IdAreaDef2 = DataAccess.Get_DataTable("select Id_DefxArea from DefectosxArea where Id_defectos=" + Convert.ToInt32(ComboDefectos2.SelectedItem.Value) + " and Id_Area=" + Convert.ToInt32(ComboArea2.SelectedItem.Value));
            //        int IdAreaxDef2 = Convert.ToInt32(dt_IdAreaDef2.Rows[0][0]);
            //        //TextBox txtcantidad = (TextBox)DefectosEncontrados.Rows[i].Cells[4].FindControl("txtcantidad");
            //        //DropDownList ddlSeccion = (DropDownList)DefectosEncontrados.Rows[i].Cells[3].FindControl("ddlSeccion");

            //        try
            //        {
            //            OrderDetailDA.SaveBundlesRejectedBulto(Convert.ToInt32(IdBundle), Convert.ToInt32(IdOrder), Convert.ToInt32(Id_Biohorario), Convert.ToInt32(ComboSeccion2.SelectedItem.Value), Id_Linea, IdAreaxDef2, Convert.ToInt32(id_inspector), IdSupervisor, Idoperario, Id_Muestreo, Convert.ToDateTime(lblfechaval.Text), Convert.ToString(recibido), Convert.ToInt32(txtCantidad2.Text));
            //        }
            //        catch (Exception ex)
            //        {
            //            MailMessage mailObj = new MailMessage("soporte@rocedes.com.ni", "emedina@rocedes.com.ni", "Sistema Auditoria", ex.ToString());
            //            SmtpClient SMTPServer = new SmtpClient("rocedes.com.ni");
            //            SMTPServer.Send(mailObj);
            //            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "fieldalert();", true);
            //        return;
            //    }
            //}


            //if (txtCantidad3.ClientVisible == true)
            //{
            //    if (txtCantidad3.Text != "")
            //    {
            //        DataTable dt_IdAreaDef3 = new DataTable();
            //        dt_IdAreaDef3 = DataAccess.Get_DataTable("select Id_DefxArea from DefectosxArea where Id_defectos=" + Convert.ToInt32(ComboDefectos3.SelectedItem.Value) + " and Id_Area=" + Convert.ToInt32(ComboArea3.SelectedItem.Value));
            //        int IdAreaxDef3 = Convert.ToInt32(dt_IdAreaDef3.Rows[0][0]);
            //        //TextBox txtcantidad = (TextBox)DefectosEncontrados.Rows[i].Cells[4].FindControl("txtcantidad");
            //        //DropDownList ddlSeccion = (DropDownList)DefectosEncontrados.Rows[i].Cells[3].FindControl("ddlSeccion");        
            //        try
            //        {
            //            OrderDetailDA.SaveBundlesRejectedBulto(Convert.ToInt32(IdBundle), Convert.ToInt32(IdOrder), Convert.ToInt32(Id_Biohorario), Convert.ToInt32(ComboSeccion3.SelectedItem.Value), Id_Linea, IdAreaxDef3, Convert.ToInt32(id_inspector), IdSupervisor, Idoperario, Id_Muestreo, Convert.ToDateTime(lblfechaval.Text), Convert.ToString(recibido), Convert.ToInt32(txtCantidad3.Text));
            //        }
            //        catch (Exception ex)
            //        {
            //            MailMessage mailObj = new MailMessage("soporte@rocedes.com.ni", "emedina@rocedes.com.ni", "Sistema Auditoria", ex.ToString());
            //            SmtpClient SMTPServer = new SmtpClient("rocedes.com.ni");
            //            SMTPServer.Send(mailObj);
            //            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "fieldalert();", true);
            //        return;
            //    }
            //}



            //if (txtCantidad4.ClientVisible == true)
            //{
            //    if (txtCantidad4.Text != "")
            //    {
            //        DataTable dt_IdAreaDef4 = new DataTable();
            //        dt_IdAreaDef4 = DataAccess.Get_DataTable("select Id_DefxArea from DefectosxArea where Id_defectos=" + Convert.ToInt32(ComboDefectos4.SelectedItem.Value) + " and Id_Area=" + Convert.ToInt32(ComboArea4.SelectedItem.Value));
            //        int IdAreaxDef4 = Convert.ToInt32(dt_IdAreaDef4.Rows[0][0]);
            //        //TextBox txtcantidad = (TextBox)DefectosEncontrados.Rows[i].Cells[4].FindControl("txtcantidad");
            //        //DropDownList ddlSeccion = (DropDownList)DefectosEncontrados.Rows[i].Cells[3].FindControl("ddlSeccion");        
            //        try
            //        {
            //            OrderDetailDA.SaveBundlesRejectedBulto(Convert.ToInt32(IdBundle), Convert.ToInt32(IdOrder), Convert.ToInt32(Id_Biohorario), Convert.ToInt32(ComboSeccion4.SelectedItem.Value), Id_Linea, IdAreaxDef4, Convert.ToInt32(id_inspector), IdSupervisor, Idoperario, Id_Muestreo, Convert.ToDateTime(lblfechaval.Text), Convert.ToString(recibido), Convert.ToInt32(txtCantidad4.Text));
            //        }
            //        catch (Exception ex)
            //        {
            //            MailMessage mailObj = new MailMessage("soporte@rocedes.com.ni", "emedina@rocedes.com.ni", "Sistema Auditoria", ex.ToString());
            //            SmtpClient SMTPServer = new SmtpClient("rocedes.com.ni");
            //            SMTPServer.Send(mailObj);
            //            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "fieldalert();", true);
            //        return;
            //    }
            //}


            //if (txtCantidad5.ClientVisible == true)
            //{
            //    if (txtCantidad5.Text != "")
            //    {
            //        DataTable dt_IdAreaDef5 = new DataTable();
            //        dt_IdAreaDef5 = DataAccess.Get_DataTable("select Id_DefxArea from DefectosxArea where Id_defectos=" + Convert.ToInt32(ComboDefectos5.SelectedItem.Value) + " and Id_Area=" + Convert.ToInt32(ComboArea5.SelectedItem.Value));
            //        int IdAreaxDef5 = Convert.ToInt32(dt_IdAreaDef5.Rows[0][0]);
            //        //TextBox txtcantidad = (TextBox)DefectosEncontrados.Rows[i].Cells[4].FindControl("txtcantidad");
            //        //DropDownList ddlSeccion = (DropDownList)DefectosEncontrados.Rows[i].Cells[3].FindControl("ddlSeccion");        
            //        try
            //        {
            //            OrderDetailDA.SaveBundlesRejectedBulto(Convert.ToInt32(IdBundle), Convert.ToInt32(IdOrder), Convert.ToInt32(Id_Biohorario), Convert.ToInt32(ComboSeccion5.SelectedItem.Value), Id_Linea, IdAreaxDef5, Convert.ToInt32(id_inspector), IdSupervisor, Idoperario, Id_Muestreo, Convert.ToDateTime(lblfechaval.Text), Convert.ToString(recibido), Convert.ToInt32(txtCantidad5.Text));
            //        }
            //        catch (Exception ex)
            //        {
            //            MailMessage mailObj = new MailMessage("soporte@rocedes.com.ni", "emedina@rocedes.com.ni", "Sistema Auditoria", ex.ToString());
            //            SmtpClient SMTPServer = new SmtpClient("rocedes.com.ni");
            //            SMTPServer.Send(mailObj);
            //            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "fieldalert();", true);
            //        return;
            //    }
            //}
            #endregion
            Load_Data(int.Parse(comboSeccion.SelectedValue));
            this.RechazoBultos.ShowOnPageLoad = false;
        }
        catch (Exception)
        {

            throw;
        }
    }

    /// <summary>
    /// utilizado 8
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            lbllineaval.Text = "L" + lineaval.Text + " " + lblseccionval.Text;
            Load_ControlBultosDefectos();
            this.ASPxPopupControl1.ShowOnPageLoad = true;
        }
        catch (Exception)
        {

            throw;
        }
    }

    /// <summary>
    /// utilizado 9
    /// </summary>
    public void Load_ControlBultosDefectos()
    {
        int Id_Seccion = Convert.ToInt32(Request.QueryString["id_seccion"]);
        string Id_Bundle = Request.QueryString["IdBundle"];
        string IdOrder = Request.QueryString["IdOrder"];

        string hora_reparado = DateTime.Now.ToShortTimeString();
        string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));
        DateTime fecha_actual = DateTime.Now;
        string fecha_recibido = fecha_actual.ToString("yyyy-MM-dd");

        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
        string userId = mu.ProviderUserKey.ToString();

        DataTable id_sup = DataAccess.Get_DataTable("select si.idseccionInspector from seccionInspector si join Inspector i on si.idInspector=i.id_inspector where i.UserId='" + userId + "' and si.idseccion=" + Id_Seccion);
        DataTable dt_rechazos = OrderDetailDA.Get_BundleRejected_ALL_bio_Bulto(Id_Bundle, Convert.ToInt32(id_sup.Rows[0][0]));
        lblDate.Text = fecha_recibido + " " + hora_reparado;

        if (dt_rechazos.Rows.Count > 0)
        {
            lblbundleid.Text = Convert.ToString(dt_rechazos.Rows[0][1]);
            lblcorte.Text = Convert.ToString(dt_rechazos.Rows[0][2]);
            lblmensaje.Visible = false;
            Button1.Visible = true;
            ControlBultos.Visible = true;
            ControlBultos.DataSource = dt_rechazos;
            ControlBultos.DataBind();
        }
        else
        {
            Button2.Visible = false;
            ControlBultos.Visible = false;
            lblmensaje.Visible = true;
            lblmensaje.Text = "No se encontraron Bultos Rechazados!";
            return;
        }

        for (int i = 0; i <= ControlBultos.Rows.Count - 1; i++)
        {
            DropDownList ddlDefectos = (DropDownList)ControlBultos.Rows[i].FindControl("ddlDefectos");
            ddlDefectos.SelectedValue = Convert.ToString(dt_rechazos.Rows[i].ItemArray[4]);
        }


    }

    /// <summary>
    /// utilizado 10
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnsave1_Click(object sender, EventArgs e)
    {
        try
        {
            Button1.Enabled = false;
            DateTime fecha_actual = DateTime.Now;
            string fecha_recibido = fecha_actual.ToString("yyyy-MM-dd");
            string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

            string IdBundle = Request.QueryString["IdBundle"].ToString();
            string IdOrder = Request.QueryString["IdOrder"].ToString();
            int Id_Linea = Convert.ToInt32(Request.QueryString["id_linea"]);
            int Id_Seccion = Convert.ToInt32(Request.QueryString["id_seccion"]);

            int Id_Biohorario = 0;
            DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio FROM Biohorario WHERE @input between rango1 and rango2");
            if (Bihorarios.Rows.Count > 0)
            {
                Id_Biohorario = Convert.ToInt32(Bihorarios.Rows[0][0]);
            }

            MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
            string userId = mu.ProviderUserKey.ToString();

            DataTable dt_Bundle = DataAccess.Get_DataTable("select Quantity from Bundle where Id_Bundle=" + IdBundle);
            int Cantidad_Bundle = Convert.ToInt32(dt_Bundle.Rows[0][0]);
            DataTable dt_IdMuestreo = DataAccess.Get_DataTable("declare @input float =" + Cantidad_Bundle + " SELECT id_muestreo FROM Muestreo WHERE @input between rango_incial and rango_final and nivel_insp = (Select levelaql from AQL)");
            int Id_Muestreo = Convert.ToInt32(dt_IdMuestreo.Rows[0][0]);

            for (int i = 0; i <= ControlBultos.Rows.Count - 1; i++)
            {
                Label cantDefe = (Label)ControlBultos.Rows[i].FindControl("cantdef");

                try
                {
                    OrderDetailDA.SaveBundlesRepairBulto(Convert.ToInt32(IdBundle), Id_Biohorario, Id_Seccion, userId, Id_Muestreo, Convert.ToDateTime(fecha_recibido), Convert.ToString(date_now));
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                    this.RechazoBultos.ShowOnPageLoad = false;
                    Response.Redirect("BultoxBulto.aspx?IdBundle=" + Convert.ToInt32(IdBundle) + "&id_order=" + Convert.ToInt32(IdOrder) + "&id_seccion=" + Id_Seccion + "&id_linea=" + Id_Linea, false);
                    //this.RechazoBultos.ShowOnPageLoad = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert();", true);
                }

            }

        }
        catch (Exception)
        {

            throw;
        }
    }

    /// <summary>
    /// util
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void DefectosEncontrados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    Load_Data(int.Parse(comboSeccion.SelectedValue));
    //}
    /// <summary>
    /// util
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        DropDownList ddlSeccion = (DropDownList)e.Row.FindControl("ddlSeccion");
    //        int id_linea = Convert.ToInt32(Request.QueryString["id_linea"]);
    //        //return DataTable havinf department data
    //        DataTable dt = DataAccess.Get_DataTable("select S.id_seccion as id_seccion, M.Modulo as Modulo from Modulo M inner join Seccion S on M.id_seccion=S.id_seccion where S.id_linea=" + id_linea);
    //        ddlSeccion.DataSource = dt;
    //        ddlSeccion.DataBind();
    //        ddlSeccion.DataTextField = "Modulo";
    //        ddlSeccion.DataValueField = "id_seccion";
    //    }
    //}

    protected void lnkNew_Click(object sender, EventArgs e)
    {
        int Id_Seccion = Convert.ToInt32(Request.QueryString["id_seccion"]);
        int Id_Linea = Convert.ToInt32(Request.QueryString["id_linea"]);
        Response.Redirect("Search.aspx?id_seccion=" + Id_Seccion + "&id_linea=" + Id_Linea, false);
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        string id_order = Request.QueryString["IdOrder"].ToString();
        string id_seccion = Request.QueryString["id_seccion"].ToString();
        string id_linea = Request.QueryString["id_linea"].ToString();
        Response.Redirect("BultoxBulto.aspx?id_order=" + id_order + "&id_seccion=" + id_seccion + "&id_linea=" + id_linea);
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        string id_order = Request.QueryString["IdOrder"].ToString();
        string id_seccion = Request.QueryString["id_seccion"].ToString();
        string id_linea = Request.QueryString["id_linea"].ToString();
        Response.Redirect("BultoxBulto.aspx?id_order=" + id_order + "&id_seccion=" + id_seccion + "&id_linea=" + id_linea);
    }

    protected void ControlBultos_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    //protected void ASPxButton1_Click(object sender, EventArgs e)
    //{
    //    DataTable aql_aprov = new DataTable();
    //    DataTable aql_reject = new DataTable();
    //    int Id_Biohorario = 0;
    //    string hora_actual = DateTime.Now.Hour.ToString();
    //    string minutos_actual = DateTime.Now.Minute.ToString();
    //    string date_now = hora_actual + '.' + minutos_actual;
    //    DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio, bihorario FROM Biohorario WHERE @input between rango1 and rango2");
    //    if (Bihorarios.Rows.Count > 0)
    //    {
    //        Id_Biohorario = Convert.ToInt32(Bihorarios.Rows[0][0]);
    //    }


    //    DataTable Bio_auditados = DataAccess.Get_DataTable("SELECT id_bio, bihorario FROM Biohorario WHERE id_bio<=" + Id_Biohorario);
    //    cbxbihorario.DataSource = Bio_auditados;
    //    cbxbihorario.DataBind();
    //    cbxbihorario.TextField = "bihorario";
    //    cbxbihorario.ValueField = "id_bio";

    //    DateTime fecha_actual = DateTime.Now;
    //    string sqlfecha_actual = fecha_actual.ToString("yyyy-MM-dd");
    //    cbxbihorario.Text = Convert.ToString(Bihorarios.Rows[0][1]);
    //    int bio_seleccionado = Convert.ToInt32(cbxbihorario.Value);

    //    string query = "select BA.Id_Bundle, BA.Id_Order, B.Quantity from BundleApprovedBulto BA inner join Bundle B on BA.Id_Bundle=B.Id_Bundle where BA.id_bio=" + bio_seleccionado + " and BA.FechaAprobado='" + sqlfecha_actual + "'";
    //    string query2 = "select BR.Id_Bundle, BR.Id_Order, B.Quantity from BundleRejectedBulto BR inner join Bundle B on BR.Id_Bundle=B.Id_Bundle where BR.id_bio=" + bio_seleccionado + " and BR.FechaRecibido='" + sqlfecha_actual + "'";
    //    //Reporte de Auditoria por Bulto
    //    aql_aprov = DataAccess.Get_DataTable(query);
    //    int total_aprov = 0, total_rejec = 0;
    //    for (int i = 0; i <= aql_aprov.Rows.Count - 1; i++)
    //    {
    //        total_aprov = total_aprov + Convert.ToInt32(aql_aprov.Rows[i][2]);
    //    }
    //    aql_reject = DataAccess.Get_DataTable(query2);
    //    for (int j = 0; j <= aql_aprov.Rows.Count - 1; j++)
    //    {
    //        total_rejec = total_rejec + Convert.ToInt32(aql_aprov.Rows[j][2]);
    //    }
    //    lblAcep.Text = Convert.ToString(aql_aprov.Rows.Count);
    //    lblReje.Text = Convert.ToString(aql_reject.Rows.Count);
    //    //Total de Bultos Auditados
    //    string Id_Bundle = Request.QueryString["IdBundle"] ?? "";
    //    string query3 = "select Quantity from Bundle where Id_Bundle=" + Id_Bundle;
    //    DataTable aql_total = DataAccess.Get_DataTable(query3);
    //    int total = Convert.ToInt32(aql_aprov.Rows.Count) + Convert.ToInt32(aql_reject.Rows.Count);
    //    string piezas = Convert.ToString(total_aprov + total_rejec);
    //    lbltotbultau.Text = total + "/" + piezas;
    //    //%Calidad
    //    double calidad = 0;
    //    if (total > 0)
    //    {
    //        calidad = (Convert.ToInt32(aql_aprov.Rows.Count) * 100) / total;
    //    }
    //    calidad = (double)Math.Round(Convert.ToDecimal(calidad), 0);
    //    lblcalidad.Text = Convert.ToString(calidad) + "%";

    //    //OQL Piezas Auditadas

    //    string queryOQL = "select isnull(max(m.sample_size),0) as Muestreo from BundleApprovedBulto bd inner join Muestreo m on bd.id_muestreo=m.id_muestreo where bd.id_bio=" + bio_seleccionado + " and FechaAprobado='" + sqlfecha_actual + "'";
    //    DataTable oql = DataAccess.Get_DataTable(queryOQL);
    //    int oqlapr = Convert.ToInt32(aql_aprov.Rows.Count) * Convert.ToInt32(oql.Rows[0][0]);
    //    lblOQLacep.Text = Convert.ToString(oqlapr);
    //    string queryOQLRej = "select Id_Seccion,cantidad_defectos from BundleRejectedBulto where id_bio=" + bio_seleccionado + " and FechaRecibido='" + sqlfecha_actual + "'";

    //    DataTable oqlreje = DataAccess.Get_DataTable(queryOQLRej);
    //    int rejectoql = 0;
    //    int oqltra = 0, oqldel = 0, oqlen = 0, oqlen1 = 0;
    //    int id_linea = Convert.ToInt32(Request.QueryString["id_linea"]);
    //    DataTable secciones = DataAccess.Get_DataTable("select S.id_seccion, M.Modulo from Modulo M inner join Seccion S on M.id_seccion=S.id_seccion where S.id_linea=" + id_linea);

    //    for (int i = 0; i <= oqlreje.Rows.Count - 1; i++)
    //    {
    //        rejectoql = rejectoql + Convert.ToInt32(oqlreje.Rows[i][1]);
    //        if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[0][0]))//Trasero
    //        {
    //            oqltra = oqltra + Convert.ToInt32(oqlreje.Rows[i][1]);
    //        }
    //        if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[1][0]))//Delantero
    //        {
    //            oqldel = oqldel + Convert.ToInt32(oqlreje.Rows[i][1]);
    //        }
    //        if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[2][0]))//E1
    //        {
    //            oqlen = oqlen + Convert.ToInt32(oqlreje.Rows[i][1]);
    //        }
    //        if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[3][0]))//E2
    //        {
    //            oqlen1 = oqlen1 + Convert.ToInt32(oqlreje.Rows[i][1]);
    //        }
    //    }
    //    int TotalOql = Convert.ToInt32(oqlapr) + rejectoql;
    //    if (TotalOql > 0)
    //    {
    //        int porReject = (rejectoql * 100) / TotalOql;
    //        int porAcep = (Convert.ToInt32(oqlapr) * 100) / TotalOql;
    //        lblOQLporRech.Text = porReject + "%";
    //        lblOQLporAcep.Text = porAcep + "%";
    //        lblOQLrec.Text = Convert.ToString(rejectoql);
    //    }

    //    if (rejectoql > 0)
    //    {
    //        lblSecTra.Text = Convert.ToString(((oqltra * 100) / rejectoql)) + "%";
    //        lblSecDel.Text = Convert.ToString(((oqldel * 100) / rejectoql)) + "%";
    //        lblSecEn1.Text = Convert.ToString(((oqlen * 100) / rejectoql)) + "%";
    //        lblSecEn2.Text = Convert.ToString(((oqlen1 * 100) / rejectoql)) + "%";
    //    }

    //    ReporteLinea.ShowOnPageLoad = true;
    //}

    //public void Reporte_Bihorario()
    //{
    //    DataTable aql_aprov = new DataTable();
    //    DataTable aql_reject = new DataTable();
    //    int Id_Biohorario = 0;
    //    string hora_actual = DateTime.Now.Hour.ToString();
    //    string minutos_actual = DateTime.Now.Minute.ToString();
    //    string date_now = hora_actual + '.' + minutos_actual;
    //    DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio FROM Biohorario WHERE @input between rango1 and rango2");
    //    if (Bihorarios.Rows.Count > 0)
    //    {
    //        Id_Biohorario = Convert.ToInt32(Bihorarios.Rows[0][0]);
    //    }
    //    DataTable Bio_auditados = DataAccess.Get_DataTable("SELECT id_bio, bihorario FROM Biohorario WHERE id_bio<=" + Id_Biohorario);
    //    cbxbihorario.DataSource = Bio_auditados;
    //    cbxbihorario.DataBind();
    //    cbxbihorario.TextField = "bihorario";
    //    cbxbihorario.ValueField = "id_bio";

    //    DateTime fecha_actual = DateTime.Now;
    //    string sqlfecha_actual = fecha_actual.ToString("yyyy-MM-dd");
    //    int bio_seleccionado = Convert.ToInt32(cbxbihorario.SelectedItem.Value);

    //    string query = "select * from BundleApprovedBulto where id_bio=" + bio_seleccionado + " and FechaAprobado='" + sqlfecha_actual + "'";
    //    string query2 = "select * from BundleRejectedBulto where id_bio=" + bio_seleccionado + " and FechaRecibido='" + sqlfecha_actual + "'";
    //    //Reporte de Auditoria por Bulto
    //    aql_aprov = DataAccess.Get_DataTable(query);
    //    aql_reject = DataAccess.Get_DataTable(query2);
    //    lblAcep.Text = Convert.ToString(aql_aprov.Rows.Count);
    //    lblReje.Text = Convert.ToString(aql_reject.Rows.Count);
    //    //Total de Bultos Auditados
    //    string Id_Bundle = Request.QueryString["IdBundle"] ?? "";
    //    string query3 = "select Quantity from Bundle where Id_Bundle=" + Id_Bundle;
    //    DataTable aql_total = DataAccess.Get_DataTable(query3);
    //    int total = Convert.ToInt32(aql_aprov.Rows.Count) + Convert.ToInt32(aql_reject.Rows.Count);
    //    string piezas = Convert.ToString(total * Convert.ToInt32(aql_total.Rows[0][0]));
    //    lbltotbultau.Text = total + "/" + piezas;
    //    //%Calidad
    //    double calidad = 0;
    //    if (total > 0)
    //    {
    //        calidad = (Convert.ToInt32(aql_aprov.Rows.Count) * 100) / total;
    //    }
    //    calidad = (double)Math.Round(Convert.ToDecimal(calidad), 0);
    //    lblcalidad.Text = Convert.ToString(calidad) + "%";

    //    //OQL Piezas Auditadas

    //    string queryOQL = "select isnull(max(m.sample_size),0) as Muestreo from BundleApprovedBulto bd inner join Muestreo m on bd.id_muestreo=m.id_muestreo where bd.id_bio=" + bio_seleccionado + " and FechaAprobado='" + sqlfecha_actual + "'";
    //    DataTable oql = DataAccess.Get_DataTable(queryOQL);
    //    int oqlapr = Convert.ToInt32(aql_aprov.Rows.Count) * Convert.ToInt32(oql.Rows[0][0]);

    //    lblOQLacep.Text = Convert.ToString(oqlapr);
    //    string queryOQLRej = "select Id_Seccion,cantidad_defectos from BundleRejectedBulto where id_bio=" + bio_seleccionado + " and FechaRecibido='" + sqlfecha_actual + "'";

    //    DataTable oqlreje = DataAccess.Get_DataTable(queryOQLRej);
    //    decimal rejectoql = 0;
    //    decimal oqltra = 0, oqldel = 0, oqlen = 0, oqlen1 = 0;
    //    int id_linea = Convert.ToInt32(Request.QueryString["id_linea"]);
    //    DataTable secciones = DataAccess.Get_DataTable("select S.id_seccion, M.Modulo from Modulo M inner join Seccion S on M.id_seccion=S.id_seccion where S.id_linea=" + id_linea);

    //    for (int i = 0; i <= oqlreje.Rows.Count - 1; i++)
    //    {
    //        rejectoql = rejectoql + Convert.ToInt32(oqlreje.Rows[i][1]);
    //        if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[0][0]))//Trasero
    //        {
    //            oqltra = oqltra + Convert.ToInt32(oqlreje.Rows[i][1]);
    //        }
    //        if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[1][0]))//Delantero
    //        {
    //            oqldel = oqldel + Convert.ToInt32(oqlreje.Rows[i][1]);
    //        }
    //        if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[2][0]))//E1
    //        {
    //            oqlen = oqlen + Convert.ToInt32(oqlreje.Rows[i][1]);
    //        }
    //        if (Convert.ToString(oqlreje.Rows[i][0]) == Convert.ToString(secciones.Rows[3][0]))//E2
    //        {
    //            oqlen1 = oqlen1 + Convert.ToInt32(oqlreje.Rows[i][1]);
    //        }
    //    }
    //    decimal TotalOql = Convert.ToDecimal(oqlapr) + rejectoql;
    //    if (TotalOql > 0)
    //    {
    //        decimal porReject = Math.Round(((rejectoql * 100) / TotalOql), 0);
    //        decimal porAcep = Math.Round(((Convert.ToDecimal(oqlapr) * 100) / TotalOql), 0);
    //        lblOQLporRech.Text = porReject + "%";
    //        lblOQLporAcep.Text = porAcep + "%";
    //        lblOQLrec.Text = Convert.ToString(rejectoql);
    //    }

    //    if (rejectoql > 0)
    //    {
    //        lblSecTra.Text = Convert.ToString(((oqltra * 100) / rejectoql)) + "%";
    //        lblSecDel.Text = Convert.ToString(((oqldel * 100) / rejectoql)) + "%";
    //        lblSecEn1.Text = Convert.ToString(((oqlen * 100) / rejectoql)) + "%";
    //        lblSecEn2.Text = Convert.ToString(((oqlen1 * 100) / rejectoql)) + "%";
    //    }
    //    lblOQLtotp.Text = Convert.ToString(Convert.ToInt32(lblOQLacep.Text) + Convert.ToInt32(lblOQLrec.Text));
    //    ReporteLinea.ShowOnPageLoad = true;
    //}

    protected void cbxbihorario_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Reporte_Bihorario();
    }

    protected void ImageButton2_Click1(object sender, ImageClickEventArgs e)
    {
        int Id_Seccion = Convert.ToInt32(Request.QueryString["id_seccion"]);
        int Id_Linea = Convert.ToInt32(Request.QueryString["id_linea"]);
        Response.Redirect("Search.aspx?id_seccion=" + Id_Seccion + "&id_linea=" + Id_Linea, false);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Oper> GetOperario(string pre,int idlinea)
    {
        try
        {
            List<Oper> list = new List<Oper>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select Top(10) opOp.id_operarioOpe,op.nombre,oper.descripcion_completa from OperacionOperario opOp join Operario op on opOp.id_operario=op.id_operario"
                                           +" join Operacion oper on opOp.id_operacion=oper.id_operacion join Seccion s on opOp.id_seccion=s.id_seccion"
                                           +" join Linea l on s.id_linea = l.id_linea where op.nombre like '%" + pre + "%' and l.id_linea ="+ idlinea, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Oper obj = new Oper();

                obj.idoperacionOpe = int.Parse(dt.Rows[i][0].ToString());
                obj.operario = dt.Rows[i][1].ToString();
                obj.operacion = dt.Rows[i][2].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class Oper
    {
        public int idoperacionOpe { get; set; }
        public string operario { get; set; }
        public string operacion { get; set; }
    }

    /// <summary>
    /// guarda defectos no asignados a operarios
    /// </summary>
    /// <param name="cantidad"></param>
    /// <param name="idarea"></param>
    /// <param name="iddefecto"></param>
    /// <param name="idbulto"></param>
    /// <param name="idseccion"></param>
    /// <param name="idbiho"></param>
    /// <returns></returns>
    [WebMethod]
    public static string GuardarRechazo(int cantidad, int idarea, int iddefecto, int idbulto, int idseccion, int idbiho)
    {
        try
        {

            int i = 0;
            if (idbulto != 0)
            {
                string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

                int Id_Biohorario = 0;
                DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio, bihorario FROM Biohorario WHERE @input between rango1 and rango2");
                if (Bihorarios.Rows.Count > 0)
                {
                    Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);
                }

                MembershipUser mu = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                string userId = mu.ProviderUserKey.ToString();

                DataTable dt_Inspector = DataAccess.Get_DataTable("select si.idseccionInspector from  seccionInspector si  join Inspector i on si.idInspector=i.id_inspector where si.idseccion=" + idseccion + " and i.UserId='" + userId + "'");
                int idseccionInspector = Convert.ToInt16(dt_Inspector.Rows[0][0]);

                //OrderDetailDA.SaveBundlesRejected(idbulto, Convert.ToInt16(Id_Biohorario), IdAreaxDef, IdoperarioOP, idseccionInspector, Id_Muestreo, Convert.ToDateTime(lbldaterechazo.Text), Convert.ToString(lblhorarechazo.Text), Convert.ToInt16(txtcantidad.Text), ayuda);

                string queryUpdate = "insert  AreaDefectoRechazoBundleB (idbundle,idbio,iddefecto,idarea,idseccionInsp,fechaRecibido,Cantidad,idseccion) values(" + idbulto + "," + Id_Biohorario + "," + iddefecto + ","
                                      + idarea + "," + idseccionInspector + ",'" + DateTime.Now.ToString() + "'," + cantidad + "," + idseccion + ")";

                SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

                cn.Open();
                SqlCommand cmd = new SqlCommand(queryUpdate, cn);
                i = cmd.ExecuteNonQuery();
                cn.Close();

            }

            if (i == 0)
            {
                return "false";
            }

            return "true";
        }
        catch (Exception)
        {
            return "false";
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ardef> getarea(string pre)
    {
        try
        {
            List<ardef> list = new List<ardef>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top(10) a.Id_Area,a.Nombre from AreaDefecto a where a.Nombre like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ardef obj = new ardef();

                obj.Id_Area = int.Parse(dt.Rows[i][0].ToString());
                obj.Nombre = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ardef> getdefecto(string pre)
    {
        try
        {
            List<ardef> list = new List<ardef>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();

            SqlCommand cmd = new SqlCommand("select top(10) d.id_defectos,d.nombre from Defectos d where d.nombre like '%" + pre + "%' and d.Categoria='T' order by d.nombre asc", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ardef obj = new ardef();

                obj.Id_Defecto = int.Parse(dt.Rows[i][0].ToString());
                obj.Nombre = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class ardef
    {
        public int Id_Area { get; set; }
        public string Nombre { get; set; }
        public int Id_Defecto { get; set; }
    }


}