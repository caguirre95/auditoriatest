﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EnviosBultoExcepcion.aspx.cs" Inherits="EnviosBultoExcepcion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    var user = document.getElementById("<%= HiddenFielduser.ClientID %>").value;
                    $.ajax({
                        url: "EnviosBultoExcepcion.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "','user':'" + user + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);


                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }


    </script>

    <script>
        function key_up(objRef) {

            var row = objRef.parentNode.parentNode;

            if (row.hasChildNodes()) {
                var children = row.childNodes;

                alert(row.children[0].nodoName);

                for (var i = 0; i < children.length; i++) {
                    alert(children[i].childNodes[0].val);

                    // do something with each child as children[i]
                    // NOTE: List is live, adding or removing children will change the list
                }
            }
            //  txtunidades
            //  lblquantity
            //row.cell[0].text;
            //  if (objRef.checked) {
            //  var unidades = row.getElementById("<= txtunidades.ClientID %>").value;
            //  var quantity = row.getElementById("<= lblquantity.ClientID %>").value;
            //  var deficit = row.getElementById("<= txtdeficit.ClientID %>").value;

            //var num = document.getElementById("<%= HiddenFielduser.ClientID %>").value;

            row.style.backgroundColor = "#0090CB";
            // row.style.color = "white";
            //}
            //else {
            //    //If not checked change back to original color
            //    if (row.rowIndex % 2 == 0) {
            //        //Alternating Row Color
            //        row.style.backgroundColor = "white";
            //        row.style.color = "black";
            //    }
            //    else {
            //        row.style.backgroundColor = "white";
            //        row.style.color = "black";
            //    }
            //}
        }
    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }
    </script>

    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>

    <style>
        .mar_t {
            margin-top: 20px;
            margin-bottom: 10px;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="">
            <div class="panel panel-primary" style="margin-top: 10px">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Measurement suggestion </strong></h3>
                </div>
                <div class="panel-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="HiddenFielduser" runat="server" />


                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                Corte:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" OnClick="lnkMostrarBultosIncompletos_Click" CssClass="btn btn-default" ID="lnkMostrarBultosIncompletos" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                Bultos:
                                     <asp:DropDownList ID="DropDownList2" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div>
                            <%--  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    Bihorario
                                    <asp:DropDownList ID="drpBihorario" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Select..." Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 Bihorario" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 Bihorario" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 Bihorario" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 Bihorario" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5 Bihorario" Value="5"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>--%>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                Envio Dia
                                    <asp:DropDownList ID="drpEnvioDia" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Select..." Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 Envio" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 Envio" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 Envio" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 Envio" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5 Envio" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="6 Envio" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="7 Envio" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="8 Envio" Value="8"></asp:ListItem>
                                    </asp:DropDownList>

                                <asp:HiddenField ID="hfidPorder" runat="server" />
                                <%--   <asp:Button runat="server" ID="btnguardar" OnClick="btnguardar_Click" Text="Guardar Unidades" CssClass="btn btn-primary pull-right mar_t" />--%>
                                <asp:Button runat="server" ID="btnver" OnClick="btnver_Click" Text="Ver Todos" CssClass="btn btn-primary pull-right mar_t" />
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            </div>
                            <style>
                                .w {
                                    width: 100%;
                                    min-width: 1000px;
                                }
                            </style>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="overflow-x: auto">
                                <asp:GridView ID="gridEnvios" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" DataKeyNames="Id_Bundle" OnRowCommand="gridEnvios_RowCommand" CssClass="table table-hover table-striped w" ShowFooter="true" AutoGenerateColumns="false" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblidbundle" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("Id_Bundle") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Linea 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbllinea" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("Linea") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Porder
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPorder" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("POrder") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Talla 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblsize" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Size") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Cantidad 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblquantity" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Quantity") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Acumulado
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblUni" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Cantidad") %>'></asp:Label>
                                                <%--<asp:TextBox runat="server" ID="txtunidades" Enabled="false" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Cantidad") %>' />--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Envio Corte
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtenvio" Width="60px" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Envio") %>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <ItemTemplate>

                                                <asp:TextBox TextMode="MultiLine" runat="server" CssClass="form-control" ID="txtcomentario"></asp:TextBox>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button Text="Save" CommandName="enviar" CssClass="btn btn-default" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                            </div>



                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

