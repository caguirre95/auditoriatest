﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteBihorarioSecadoEnviado.aspx.cs" Inherits="Intex_ReporteBihorarioSecadoEnviado" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtdate.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <style>
        .he {
            height: 96px !important;
            width: 968px;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Bihorario de Cortes Entregados a Medida</strong></div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Controles</div>
                            <div class="panel-body">
                                <strong>Fecha</strong>
                                <asp:TextBox ID="txtdate" runat="server" CssClass="form-control"></asp:TextBox>
                                <hr />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Controles</div>
                            <div class="panel-body">
                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control MTop" Text="Generar" OnClick="btngenerar_Click" />

                                <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control MTop" Text="Pass to excel" OnClick="btnexcel_Click" />

                                <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-warning form-control MTop" Text="Limpiar" OnClick="btnlimpiar_Click" />

                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px">
                        <dx:ASPxGridView ID="grdsecadoenvio" runat="server" KeyFieldName="id" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="true" 
                            OnDataBinding="grdsecadoEnvio_DataBinding" OnCustomCallback="grdsecadoEnvio_CustomCallback">
                            <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                            <Columns>
                                <dx:GridViewBandColumn>
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Bihorario de Cortes Entregados a Medida" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="corteCompleto" Caption="Corte" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="estilo" Caption="Estilo" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="09:00" Caption="09:00" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="11:00" Caption="11:00" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="13:00" Caption="13:00" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="15:00" Caption="15:00" VisibleIndex="5">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="16:50" Caption="16:50" VisibleIndex="6">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="18:00" Caption="18:00" VisibleIndex="7">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="unidadesSecadasEnviadas" Caption="Unidades Entregadas a Medida" VisibleIndex="8">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Deficit" Caption="Balance" VisibleIndex="9">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" />
                            <Settings ShowFooter="true" VerticalScrollBarMode="Visible"  />
                            <SettingsPager PageSize="150" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="corteCompleto" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="09:00" SummaryType="Sum" DisplayFormat="Totales : {0}" />
                                <dx:ASPxSummaryItem FieldName="11:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="13:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="15:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="16:50" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="18:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="unidadesSecadasEnviadas" SummaryType="Sum" DisplayFormat="{0}" />                                
                            </TotalSummary>
                            <GroupSummary>
                                <dx:ASPxSummaryItem FieldName="corteCompleto" ShowInGroupFooterColumn="corteCompleto" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="09:00" ShowInGroupFooterColumn="09:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="11:00" ShowInGroupFooterColumn="11:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="13:00" ShowInGroupFooterColumn="13:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="15:00" ShowInGroupFooterColumn="15:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="16:50" ShowInGroupFooterColumn="16:50" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="18:00" ShowInGroupFooterColumn="18:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="unidadesSecadasEnviadas" ShowInGroupFooterColumn="unidadesSecadasEnviadas" SummaryType="Sum" DisplayFormat="{0}" />
                            </GroupSummary>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar" runat="server"></dx:ASPxGridViewExporter>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

