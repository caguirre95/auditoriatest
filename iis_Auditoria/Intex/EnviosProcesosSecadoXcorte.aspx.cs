﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Intex_EnviosProcesosSecadoXcorte : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.User.Identity.IsAuthenticated == true)
        {
            if (Page.User.IsInRole("PackAdmon"))
            {
                if (!IsPostBack)
                {
                    HiddenFielduser.Value = HttpContext.Current.User.Identity.Name;
                    var corteC = Request.QueryString["corteCompleto"];

                    if (!string.IsNullOrEmpty(corteC))
                    {
                        hdfcorte.Value = corteC.ToString();
                        VerInfo(corteC.ToString());
                    }
                }
            }
        }
        else
        {
            Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
        }
    }

    void VerInfo(string id)
    {
        try
        {
            if (!string.IsNullOrEmpty(id.ToString()))
            {
                string queryBundle = " select a.idSecadoDetalle, a.corteCompleto, a.Unidades, case when MAX(b.Envio) = 0 then 1 else max(b.Envio) + 1 end NumEnvio"
                                   + " from (select isd.idSecadoDetalle, ise.corteCompleto, isd.unidadesSecadas Unidades from Intexdbo.intexSecadoDetalle isd"
                                   + " join Intexdbo.intexSecado ise on ise.corteCompleto = isd.corteCompleto where ISNULL(isd.envioSecado, 0) = 0 and ise.corteCompleto = '" + id + "'"
                                   + " group by idSecadoDetalle, ise.corteCompleto, isd.unidadesSecadas) a join (select isd.corteCompleto, isnull(isd.numEnvioSecado,0) Envio"
                                   + " from Intexdbo.intexSecadoDetalle isd group by isd.corteCompleto, isd.numEnvioSecado) b on a.corteCompleto = b.corteCompleto"
                                   + " group by a.idSecadoDetalle, a.corteCompleto, a.Unidades";

                DataTable dt = DataAccess.Get_DataTable(queryBundle);

                gridEnviosSecado.DataSource = dt;
                gridEnviosSecado.DataBind();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    void Limpiar()
    {
        drpEnvioDia.SelectedValue = "0";
    }

    protected void gridEnviosSecado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (drpEnvioDia.SelectedValue != "0")
            {
                if (e.CommandName.Equals("enviar"))
                {
                    GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    string idsecdet = gridEnviosSecado.DataKeys[indice].Value.ToString();

                    int envioDia = int.Parse(drpEnvioDia.SelectedValue);

                    var corteCompleto = (Label)row.FindControl("lblcorte");
                    var txtenvio = (TextBox)row.FindControl("txtenvio");

                    string resp = OrderDetailDA.enviarProcesoSecadoXcorte(corteCompleto.Text, int.Parse(idsecdet), int.Parse(txtenvio.Text), envioDia, Context.User.Identity.Name);

                    if (resp == "Exito")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                        Limpiar();

                        var corteC = Request.QueryString["corteCompleto"];

                        if (!string.IsNullOrEmpty(corteC))
                        {
                            hdfcorte.Value = corteC;

                            VerInfo(corteC);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave();", true);
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnvertodos_Click(object sender, EventArgs e)
    {
        try
        {
            VerInfo(hdfcorte.Value);
            Limpiar();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnguardarSel_Click(object sender, EventArgs e)
    {
        try
        {
            string resp = "";
            if (drpEnvioDia.SelectedValue != "0")
            {
                int cont = 0;

                int envioDia = int.Parse(drpEnvioDia.SelectedValue);

                foreach (GridViewRow item in gridEnviosSecado.Rows)
                {
                    var chk = (CheckBox)item.FindControl("checkEnvio");
                    var idsecdet = (Label)item.FindControl("lblidsecdet");
                    var corteCompleto = (Label)item.FindControl("lblcorte");
                    var txtenvio = (TextBox)item.FindControl("txtenvio");

                    if (chk.Checked && idsecdet.Text != string.Empty && txtenvio.Text != string.Empty && corteCompleto.Text != string.Empty)
                    {
                        resp = OrderDetailDA.enviarProcesoSecadoXcorte(corteCompleto.Text, int.Parse(idsecdet.Text), int.Parse(txtenvio.Text), envioDia, Context.User.Identity.Name);

                        if (resp != "Exito")
                        {
                            cont++;
                        }
                    }
                    else
                    {
                        cont++;
                    }
                }

                if (cont == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                    Limpiar();

                    var corteC = Request.QueryString["corteCompleto"];

                    if (!string.IsNullOrEmpty(corteC))
                    {
                        hdfcorte.Value = corteC;

                        VerInfo(corteC);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave();", true);
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void lnkregresar_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("../Intex/EnviosProcesoSecado.aspx");
        }
        catch (Exception)
        {

            //throw;
        }
    }
}