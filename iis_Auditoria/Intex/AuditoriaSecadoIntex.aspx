﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AuditoriaSecadoIntex.aspx.cs" Inherits="Intex_AuditoriaSecadoIntex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                timer: 1000,
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Campos Vacios, llenelos Correctamente !',
                timer: 1000,
                type: 'info'
            });
        }
        function alertClave(r) {
            swal({
                title: 'Ocurrio un evento Inesperado, intentelo Nuevamente ! - ' + r,
                timer: 1000,
                type: 'warning'
            });
        }
        function ClaveCorrect() {
            swal({
                title: 'Clave Correcta, Accion Realizada !',
                type: 'info'
            });
        }

    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function alertMasterE(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }
    </script>

    <style>
        .mar_t {
            margin: 10px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="">
            <div class="panel panel-primary" style="margin-top: 10px;">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Auditoria Despues de Secado</strong></h3>
                </div>
                <div class="panel-body">

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:MultiView runat="server" ID="multiview">
                                <asp:View runat="server">

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Button ID="btnrec" runat="server" CssClass="form-control btn btn-info" Text="Actualizar Secado" OnClick="btnrec_Click" />
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <a href="#ventanaG" data-toggle="modal" class="btn btn-md btn-success form-control"><span class="glyphicon glyphicon-align-justify"></span>Piezas Por Estilo</a>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="overflow-x: auto">
                                        <hr />
                                        <style>
                                            .w {
                                                width: 100%;
                                                min-width: 700px;
                                            }
                                        </style>
                                        <asp:GridView ID="gridEnvios" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" DataKeyNames="corteCompleto" CssClass="table table-hover table-responsive table-striped"
                                            AutoGenerateColumns="false" GridLines="None" OnRowCommand="gridEnvios_RowCommand">
                                            <Columns>
                                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        CORTE
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPorder" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("corteCompleto") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        ESTILO 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblestilo" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("estilo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%--<asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        QUANTITY 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcantidad" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("unidadesCorteCompleto") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        UNIDADES SECADAS
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtunidades" Enabled="false" Font-Size="Medium" CssClass="form-control" Text='<%#Bind("TotalDia") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        UNIDADES AUDITADAS
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtunits" Enabled="false" Font-Size="Medium" CssClass="form-control" Text='<%#Bind("TotalAudit") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        UNIDAD AUDIT
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtauditoriaU" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" MaxLength="4" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        AUDITORIA
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chk1" runat="server" CssClass="form-control" Checked="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        NOTA
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtobservacion" AutoCompleteType="Disabled" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton Text="" ID="lnkenviotxt" CssClass="btn btn-success" runat="server" CommandName="Agregar" ToolTip="Guadar"><i class="glyphicon glyphicon-object-align-bottom"> </i> </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>

                                        <%-- </div>--%>
                                    </div>

                                </asp:View>
                                <asp:View runat="server">
                                </asp:View>
                            </asp:MultiView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>

    <aside class="modal fade" id="ventanaG">
        <div class="modal-dialog">
            <div class="modal-content">
                <article class="modal-header">
                    <button id="cerrarG" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                    <h4>Piezas Agreadas Por Estilo </h4>
                </article>
                <article class="modal-body">

                    <asp:Label ID="lbl1" Text="" runat="server" CssClass="form-control btn-warning"></asp:Label>
                    <br />
                    <asp:Label ID="lbl2" Text="" runat="server" CssClass="form-control btn-warning"></asp:Label>
                    <br />
                    <asp:Label ID="lbl3" Text="" runat="server" CssClass="form-control btn-warning"></asp:Label>

                </article>
            </div>
        </div>
    </aside>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

