﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Intex_EnviosProcesoSecado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.User.Identity.IsAuthenticated == true)
        {
            if (Page.User.IsInRole("PackAdmon"))
            {
                if (!IsPostBack)
                {
                    multiview.ActiveViewIndex = 0;
                    UnidadesSecadas();
                }
            }
        }
        else
        {
            Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
        }
    }

    void UnidadesSecadas()
    {
        try
        {
            if (txtPorder.Text != string.Empty)
            {
                DataTable dt;

                string querySec = " select a.corteCompleto, a.unidadesCorteCompleto, a.Unidades, case when MAX(b.Envio) = 0 then 1 else max(b.Envio) + 1 end NumEnvio"
                                + " from (select ise.corteCompleto, ise.unidadesCorteCompleto, SUM(isd.unidadesSecadas) Unidades from Intexdbo.intexSecadoDetalle isd"
                                + " join Intexdbo.intexSecado ise on ise.corteCompleto = isd.corteCompleto where ISNULL(isd.envioSecado, 0) = 0 and ise.corteCompleto = '" + txtPorder.Text.Trim() + "'"
                                + " group by ise.corteCompleto, ise.unidadesCorteCompleto"
                                + " ) a join (select isd.corteCompleto, isnull(isd.numEnvioSecado,0) Envio from Intexdbo.intexSecadoDetalle isd where isd.corteCompleto = '"+txtPorder.Text.Trim()+"'"
                                + " group by isd.corteCompleto, isd.numEnvioSecado"
                                + " ) b on a.corteCompleto = b.corteCompleto group by a.corteCompleto, a.unidadesCorteCompleto, a.Unidades";

                dt = DataAccess.Get_DataTable(querySec);

                gridEnviosSec.DataSource = dt;
                gridEnviosSec.DataBind();
            }
            else
            {
                DataTable dt;

                string querySec = " select a.corteCompleto, a.unidadesCorteCompleto, a.Unidades, case when MAX(b.Envio) = 0 then 1 else max(b.Envio) + 1 end NumEnvio"
                                + " from (select ise.corteCompleto, ise.unidadesCorteCompleto, SUM(isd.unidadesSecadas) Unidades from Intexdbo.intexSecadoDetalle isd"
                                + " join Intexdbo.intexSecado ise on ise.corteCompleto = isd.corteCompleto where ISNULL(isd.envioSecado, 0) = 0 group by ise.corteCompleto, ise.unidadesCorteCompleto"
                                + " ) a join (select isd.corteCompleto, isnull(isd.numEnvioSecado,0) Envio from Intexdbo.intexSecadoDetalle isd group by isd.corteCompleto, isd.numEnvioSecado"
                                + " ) b on a.corteCompleto = b.corteCompleto group by a.corteCompleto, a.unidadesCorteCompleto, a.Unidades";

                dt = DataAccess.Get_DataTable(querySec);

                gridEnviosSec.DataSource = dt;
                gridEnviosSec.DataBind();
            }
        }
        catch (Exception)
        {
            //throw;
        }
    }

    protected void btnVertodos_Click(object sender, EventArgs e)
    {
        try
        {
            Limpiar();
            UnidadesSecadas();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void gridEnviosSec_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gridEnviosSec.PageIndex = e.NewPageIndex;
        UnidadesSecadas();
    }

    void Limpiar()
    {
        drpEnvioDia.SelectedValue = "0";        
    }

    protected void btnguardarEnviar_Click(object sender, EventArgs e)
    {
        try
        {
            string resp = "";

            if (drpEnvioDia.SelectedValue != "0")
            {
                foreach (GridViewRow item in gridEnviosSec.Rows)
                {

                    var corteCompleto = (Label)item.FindControl("lblPorder");
                    var envioCorte = (TextBox)item.FindControl("txtenvio");
                    var chk = (CheckBox)item.FindControl("checkEnvio");

                    if (chk.Checked)
                    {
                        resp = OrderDetailDA.enviarProcesoSecado(corteCompleto.Text, int.Parse(envioCorte.Text), int.Parse(drpEnvioDia.SelectedValue), Context.User.Identity.Name);
                    }
                }

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto('" + resp + "');", true);
                }

                UnidadesSecadas();
                Limpiar();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave();", true);
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void lnkEnviosEC_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnkred = (LinkButton)sender;
            GridViewRow row = (GridViewRow)lnkred.NamingContainer;

            var lblporder = (Label)row.FindControl("lblPorder");

            if (lblporder.Text != string.Empty)
            {
                Response.Redirect("EnviosProcesosSecadoXcorte.aspx?corteCompleto=" + lblporder.Text);
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select top 15 corteCompleto from Intexdbo.intexSecado where corteCompleto like '%" + pre + "%' order by LEN(corteCompleto)", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porderclient = dt.Rows[i][0].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porderclient { get; set; }
        public string estilo { get; set; }
        public int totalcorte { get; set; }
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            UnidadesSecadas();
        }
        catch (Exception)
        {

            //throw;
        }
    }
}