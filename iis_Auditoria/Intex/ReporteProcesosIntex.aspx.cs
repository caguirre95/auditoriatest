﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Intex_ReporteProcesosIntex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtdate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["dtproc"] = null;
        }
    }

    void VerInfo()
    {
        var proc = OrderDetailDA.ReporteProcesosIntex(Convert.ToDateTime(txtdate.Text));
        Session["dtproc"] = proc;
        grdintexproc.DataSource = Session["dtproc"];
        grdintexproc.DataBind();
        grdintexproc.GroupBy(grdintexproc.Columns["Estilo"]);
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            VerInfo();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            exportar.GridViewID = "grdintexproc";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = exportar;


            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();

                //PdfExportOptions oppdf = new PdfExportOptions();

                options.ExportMode = XlsxExportMode.SingleFile;
                options.SheetName = "ProcesosIntex";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=ProcesosIntex.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }

            ps.Dispose();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void grdintexproc_DataBinding(object sender, EventArgs e)
    {
        grdintexproc.DataSource = Session["dtproc"];
    }

    protected void grdintexproc_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdintexproc.DataBind();
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Intex/ReporteProcesosIntex.aspx");
    }
}