﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Intex_ReparacionAuditoriaIntex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string us = Page.User.Identity.Name.ToString().ToLower().Trim();
            txtdate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtdate1.Text = DateTime.Now.ToString("yyyy-MM-dd");

            if (!string.Equals(us, "admonintex"))
            {
                FormsAuthentication.SignOut();
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select top 15 corteCompleto, estilo from Intexdbo.intexSecado where corteCompleto like '%" + pre + "%' order by LEN(corteCompleto)", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porderclient = dt.Rows[i][0].ToString();
                obj.estilo = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porderclient { get; set; }
        public string estilo { get; set; }
        public int totalcorte { get; set; }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Intex/ReparacionAuditoriaIntex.aspx");
    }

    void VerInfoAuditoria()
    {
        try
        {
            DataTable dt;

            string queryCortes = " select ROW_NUMBER() OVER (ORDER BY idAuditoriaSecado) id, idAuditoriaSecado, corteCompleto, estilo, unidadesAuditadas, fechaAuditoria from Intexdbo.intexSecadoAuditoria"
                               + " where corteCompleto = '" + txtPorder.Text.Trim() + "' order by fechaAuditoria";

            dt = DataAccess.Get_DataTable(queryCortes);

            gridAuditoria.DataSource = dt;

            gridAuditoria.DataBind();

        }
        catch (Exception ex)
        {
            //throw;
        }
    }

    void InfCorteAudit()
    {
        try
        {
            DataTable dt;

            string queryCortes = " select isa.corteCompleto, ise.unidadesCorteCompleto, isa.estilo, SUM(ISNULL(unidadesAuditadas,0)) UnidadesAuditadas,"
                               + " MAX(CONVERT(date,fechaAuditoria)) UltimaFechaAuditoria from Intexdbo.intexSecadoAuditoria isa join Intexdbo.intexSecado ise on isa.corteCompleto = ise.corteCompleto"
                               + " where isa.corteCompleto = '" + txtPorder.Text.Trim() + "' group by isa.corteCompleto, ise.unidadesCorteCompleto, isa.estilo";

            dt = DataAccess.Get_DataTable(queryCortes);

            gridAuditoriaInf.DataSource = dt;

            gridAuditoriaInf.DataBind();

        }
        catch (Exception ex)
        {
            //throw;
        }
    }

    protected void LtnMostrar_Click(object sender, EventArgs e)
    {
        try
        {
            VerInfoAuditoria();
            InfCorteAudit();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    void Limpiar()
    {
        txtunitsadd.Text = "";
        chkadd.Checked = false;
        txtdate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        txtcomenadd.Text = string.Empty;
    }

    protected void btnagregar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtPorder.Text != string.Empty && hdfestilo.Value != string.Empty && txtunitsadd.Text != string.Empty)
            {
                string resp = OrderDetailDA.AgregarCantidadAuditoriaSecadoReparado(txtPorder.Text.Trim(), hdfestilo.Value, int.Parse(txtunitsadd.Text),
                                                                                   chkadd.Checked, txtcomenadd.Text, Context.User.Identity.Name, Convert.ToDateTime(txtdate.Text));

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                    Limpiar();
                    VerInfoAuditoria();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave('" + resp + "');", true);
                    Limpiar();
                    VerInfoAuditoria();
                    InfCorteAudit();
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void gridAuditoria_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("Actualizar"))
            {
                GridViewRow item = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int indice = item.RowIndex;
                string id = gridAuditoria.DataKeys[indice].Value.ToString();

                var corteCompleto = (Label)item.FindControl("lblcorte");
                var units = (TextBox)item.FindControl("txtunits");

                if (units.Text != string.Empty)
                {
                    string resp = OrderDetailDA.RepararAuditoriaIntex(int.Parse(id), corteCompleto.Text, int.Parse(units.Text), Context.User.Identity.Name);

                    if (resp.Equals("Exito"))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                        VerInfoAuditoria();
                        InfCorteAudit();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave('" + resp + "');", true);
                    }
                }                
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
                }
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnActualizarAuditoria_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtPorder.Text.Trim()) && !string.IsNullOrEmpty(hdfestilo.Value))
            {
                string resp = "";

                for (int i = 0; i < gridAuditoria.Rows.Count; i++)
                {
                    var row = gridAuditoria.Rows[i];

                    int indice = row.RowIndex;

                    string id = gridAuditoria.DataKeys[indice].Value.ToString();

                    var chk = (CheckBox)row.FindControl("chk1");

                    if (chk.Checked)
                    {
                        var corteCompleto = (Label)row.FindControl("lblcorte");
                        var unidadesupd = (TextBox)row.FindControl("txtunits");

                        resp = OrderDetailDA.RepararAuditoriaIntex(int.Parse(id), corteCompleto.Text, int.Parse(unidadesupd.Text), Context.User.Identity.Name);
                    }
                }

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                    VerInfoAuditoria();
                    InfCorteAudit();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave('" + resp + "');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnupdFecha_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtPorder.Text.Trim()) && !string.IsNullOrEmpty(hdfestilo.Value))
            {
                string resp = "";

                for (int i = 0; i < gridAuditoria.Rows.Count; i++)
                {
                    var row = gridAuditoria.Rows[i];

                    int indice = row.RowIndex;

                    string id = gridAuditoria.DataKeys[indice].Value.ToString();

                    var chk = (CheckBox)row.FindControl("chk1");

                    if (chk.Checked)
                    {
                        var corteCompleto = (Label)row.FindControl("lblcorte");                        

                        resp = OrderDetailDA.RepararFechaAuditoriaIntex(int.Parse(id), corteCompleto.Text, Convert.ToDateTime(txtdate1.Text), Context.User.Identity.Name);
                    }
                }

                if (resp == string.Empty)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
                }

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                    VerInfoAuditoria();
                    InfCorteAudit();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave('" + resp + "');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }
}