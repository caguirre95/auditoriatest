﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EnviosProcesoSecado.aspx.cs" Inherits="Intex_EnviosProcesoSecado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Intex/EnviosProcesoSecado.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porderclient: item.porderclient,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porderclient);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porderclient);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porderclient + "</a>").appendTo(ul);
            };
        })
    </script>

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                timer: 500,
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function alertincompleto(r) {
            swal({
                title: 'No se guardo ningun registro ! ' + r,
                timer: 1000,
                type: 'info'
            });
        }
        function alertClave() {
            swal({
                title: 'Seleccione el Envio !',
                timer: 1000,
                type: 'warning'
            });
        }
        function ClaveCorrect() {
            swal({
                title: 'Clave Correcta, Accion Realizada !',
                type: 'info'
            });
        }

        function LimpiarFocus() {
            limpiar();
        }

        function limpiar() {

            $('#<%=txtPorder.ClientID%>').val('');
        }

    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function alertMasterE(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }
    </script>

    <style>
        .mar_t {
            margin: 20px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="">
            <div class="panel panel-primary" style="margin-top: 10px;">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Bihorario de Envio </strong></h3>
                </div>
                <div class="panel-body">

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        Corte
                        <asp:TextBox ID="txtPorder" runat="server" placeholder="Digite el Corte..." CssClass="form-control" AutoCompleteType="Disabled" onfocus="LimpiarFocus()"></asp:TextBox>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <asp:Button runat="server" ID="btnbuscar" Text="Buscar Corte" OnClick="btnbuscar_Click" CssClass="btn btn-info  mar_t form-control" />
                    </div>

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:MultiView runat="server" ID="multiview">
                                <asp:View runat="server">

                                    <asp:HiddenField ID="HiddenFielduser" runat="server" />




                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        Envio Dia
                                    <asp:DropDownList ID="drpEnvioDia" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Seleccionar Envio del Dia" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 Envio" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 Envio" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 Envio" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 Envio" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5 Envio" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="6 Envio" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="7 Envio" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="8 Envio" Value="8"></asp:ListItem>
                                    </asp:DropDownList>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Button runat="server" ID="btnVertodos" Text="Ver Todos >" OnClick="btnVertodos_Click" CssClass="btn btn-success  mar_t form-control" />

                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Button runat="server" ID="btnguardarEnviar" Text="Guardar Envio Secado" OnClick="btnguardarEnviar_Click" CssClass="btn btn-primary  mar_t form-control" />

                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="overflow-x: auto">
                                        <hr />
                                        <style>
                                            .w {
                                                width: 100%;
                                                min-width: 700px;
                                            }
                                        </style>
                                        <asp:GridView ID="gridEnviosSec" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" CssClass="table table-hover table-striped w" AutoGenerateColumns="false"
                                            GridLines="None" AllowPaging="true" OnPageIndexChanging="gridEnviosSec_PageIndexChanging" PageSize="10" PagerSettings-Mode="NumericFirstLast">
                                            <Columns>
                                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        CORTE
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPorder" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("corteCompleto") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        QUANTITY 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblquantity" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("unidadesCorteCompleto") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        UNIDADES
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtunidades" Enabled="false" Width="100px" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Unidades") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        ENVIO CORTE
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtenvio" Font-Size="Medium" CssClass="form-control" MaxLength="2" onkeypress="return num(event);" Text='<%#Bind("NumEnvio") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="checkEnvio" Text="Enviar" Checked="true" CssClass="btn btn-default" runat="server" />
                                                        <asp:LinkButton Text="" ID="lnkEnviosEC" CssClass="btn btn-success" OnClick="lnkEnviosEC_Click" runat="server"><i class="glyphicon glyphicon-indent-left"> </i> </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                        <%-- </div>--%>
                                    </div>

                                </asp:View>
                                <asp:View runat="server">
                                </asp:View>
                            </asp:MultiView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

