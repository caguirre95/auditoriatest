﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Intex_AuditoriaSecadoIntex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            multiview.ActiveViewIndex = 0;
            CargaInfo();
            Etiquetas();
        }
    }

    void CargaInfo()
    {
        try
        {
            DataTable dt;
            string user = HttpContext.Current.User.Identity.Name;

            string queryCortes = " select a.corteCompleto, a.estilo, a.unidadesCorteCompleto, b.TotalDia, ISNULL(c.TotalAudit,0) TotalAudit from ("
                               + " select ise.corteCompleto, ise.estilo, ise.unidadesCorteCompleto, ise.unidadesSecadas from Intexdbo.intexPlanSecado ipl"
                               + " join Intexdbo.intexSecado ise on ise.corteCompleto = ipl.corteCompleto join Intexdbo.intexSecadoDetalle isd on isd.corteCompleto = ise.corteCompleto"
                               + " where CONVERT(date,isd.fechaSecado) = CONVERT(date,GETDATE())"
                               + " group by ise.corteCompleto, ise.estilo, ise.unidadesCorteCompleto, ise.unidadesSecadas) a join"
                               + " (select ise.corteCompleto, SUM(isd.unidadesSecadas) TotalDia from Intexdbo.intexSecado ise"
                               + " join Intexdbo.intexSecadoDetalle isd on isd.corteCompleto = ise.corteCompleto where CONVERT(date, isd.fechaSecado) = CONVERT(date, GETDATE())"
                               + " group by ise.corteCompleto) b on a.corteCompleto = b.corteCompleto"
                               + " left join (select corteCompleto, sum(unidadesAuditadas) TotalAudit from Intexdbo.intexSecadoAuditoria where CONVERT(date, fechaAuditoria) = CONVERT(date, GETDATE())"
                               + " group by corteCompleto) c on a.corteCompleto = c.corteCompleto";

            dt = DataAccess.Get_DataTable(queryCortes);

            gridEnvios.DataSource = dt;
            gridEnvios.DataBind();

        }
        catch (Exception)
        {
            throw;
        }
    }

    void Etiquetas()
    {
        lbl1.Text = "Para el estilo" + " 104031 el bulto sera de 4 piezas.";
        lbl2.Text = "Para el estilo" + " 102987 el bulto sera de 8 piezas";
        lbl3.Text = "Para los demas estilos" + " el bulto sera de 24 piezas";

    }

    protected void gridEnvios_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Agregar") && e.CommandName.CompareTo("Agregar") == 0)
        {
            GridViewRow item = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

            int indice = item.RowIndex;
            string corteCompleto = gridEnvios.DataKeys[indice].Value.ToString();

            var estilo = (Label)item.FindControl("lblestilo");
            var unidadesSecadodia = (TextBox)item.FindControl("txtunidades");
            var unidadesAuditDia = (TextBox)item.FindControl("txtunits");
            var unidades = (TextBox)item.FindControl("txtauditoriaU");
            var chk = (CheckBox)item.FindControl("chk1");
            var observacion = (TextBox)item.FindControl("txtobservacion");

            if (unidades.Text != string.Empty)
            {
                string resp = OrderDetailDA.AgregarCantidadAuditoriaSecado(corteCompleto, estilo.Text, int.Parse(unidadesSecadodia.Text), int.Parse(unidadesAuditDia.Text), int.Parse(unidades.Text), Convert.ToBoolean(chk.Checked), observacion.Text, Context.User.Identity.Name);

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave('" + resp + "');", true);
                }
            }
            else
            {
                string resp = OrderDetailDA.AgregarCantidadAuditoriaSecado(corteCompleto, estilo.Text, int.Parse(unidadesSecadodia.Text), int.Parse(unidadesAuditDia.Text), 24, Convert.ToBoolean(chk.Checked), observacion.Text, Context.User.Identity.Name);

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave('" + resp + "');", true);
                }
            }

            CargaInfo();
        }
    }

    protected void btnrec_Click(object sender, EventArgs e)
    {
        try
        {
            CargaInfo();
        }
        catch (Exception)
        {

            //throw;
        }
    }
}