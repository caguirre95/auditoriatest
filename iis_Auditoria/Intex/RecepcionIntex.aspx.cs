﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Intex_RecepcionIntex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.User.Identity.IsAuthenticated == true)
        {
            if (Page.User.IsInRole("PackAdmon"))
            {
                if (!IsPostBack)
                {
                    multiview.ActiveViewIndex = 0;
                    CargaInfo();
                }
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }

        }
        else
        {
            Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
        }
    }

    void CargaInfo()
    {
        try
        {
            DataTable dt;
            string user = HttpContext.Current.User.Identity.Name;

            string queryCortes = " SELECT c1.Id_Order, c1.POrder, c1.Style, c1.Cantidad, c1.Quantity, c1.POrderClient FROM ("
                               + " SELECT p.Id_Order, p.POrder, p.POrderClient, s.Style, p.Quantity, SUM(he.cantidadagregada) AS Cantidad"
                               + " FROM dbo.POrder AS p INNER JOIN dbo.Bundle AS b ON p.Id_Order = b.Id_Order INNER JOIN dbo.tblEnviosbulto AS eb ON b.Id_Bundle = eb.idBulto"
                               + " INNER JOIN dbo.tblhistoricoenvio AS he ON eb.idEnvio = he.idEnvio INNER JOIN dbo.Style s on s.Id_Style = p.Id_Style"
                               + " WHERE (ISNULL(he.confirmacionEnvio, 'false') = 'true') AND (ISNULL(he.corteRecepcionado, 'false') = 'false') and s.washed = 1"
                               + " GROUP BY p.Id_Order, p.POrder, p.POrderClient, s.Style, p.Quantity) AS c1 GROUP BY c1.Id_Order, c1.POrderClient, c1.POrder, c1.Style, c1.Quantity, c1.Cantidad";

            dt = DataAccess.Get_DataTable(queryCortes);

            gridrecibir.DataSource = dt;
            gridrecibir.DataBind();            

        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnguardarEnviar_Click(object sender, EventArgs e)
    {
        try
        {
            int rows = 0;
            int cont = 0;            

            foreach (GridViewRow item in gridrecibir.Rows)
            {
                string resp = "";
                var chk = (CheckBox)item.FindControl("checkEnvio");
                var idorder = (Label)item.FindControl("lblidorder");
                var corte = (Label)item.FindControl("lblPorder");
                var estilo = (Label)item.FindControl("lblStyle");
                var unidadesEnviadas = (Label)item.FindControl("lblunitssent");
                var totalCorte = (Label)item.FindControl("lblquantity");
                var corteCompleto = (Label)item.FindControl("lblordencompleta");

                if (chk.Checked)
                {
                    rows++;
                    resp = OrderDetailDA.enviarRecepcionIntex(Convert.ToInt32(idorder.Text), corte.Text.ToString(), estilo.Text.ToString(),
                                                              Convert.ToInt32(unidadesEnviadas.Text), Convert.ToInt32(totalCorte.Text),
                                                              corteCompleto.Text.ToString(), Context.User.Identity.Name);
                }

                if (resp == "OK")
                {
                    cont++;
                }

            }

            if (rows > 0 && cont > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
            }

            CargaInfo();

        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void gridrecibir_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Agregar")
            {
                GridViewRow item = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int indice = item.RowIndex;

                int rows = 0;
                int cont = 0;

                string resp = "";
                var chk = (CheckBox)item.FindControl("checkEnvio");
                var idorder = (Label)item.FindControl("lblidorder");
                var corte = (Label)item.FindControl("lblPorder");
                var estilo = (Label)item.FindControl("lblStyle");
                var unidadesEnviadas = (Label)item.FindControl("lblunitssent");
                var totalCorte = (Label)item.FindControl("lblquantity");
                var corteCompleto = (Label)item.FindControl("lblordencompleta");

                if (chk.Checked)
                {
                    rows++;
                    resp = OrderDetailDA.enviarRecepcionIntex(Convert.ToInt32(idorder.Text), corte.Text.ToString(), estilo.Text.ToString(),
                                                              Convert.ToInt32(unidadesEnviadas.Text), Convert.ToInt32(totalCorte.Text),
                                                              corteCompleto.Text.ToString(), Context.User.Identity.Name);
                }

                if (resp == "OK")
                {
                    cont++;
                }

                if (rows > 0 && cont > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
                }

                CargaInfo();

            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }        

    protected void gridrecibir_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gridrecibir.PageIndex = e.NewPageIndex;
        CargaInfo();
    }
}