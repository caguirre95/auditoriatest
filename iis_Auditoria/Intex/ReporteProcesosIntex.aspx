﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteProcesosIntex.aspx.cs" Inherits="Intex_ReporteProcesosIntex" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtdate.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <style>
        .he {
            height: 96px !important;
            width: 968px;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Reporte Procesos Intex</strong></div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Controles</div>
                            <div class="panel-body">
                                <strong>Fecha</strong>
                                <asp:TextBox ID="txtdate" runat="server" CssClass="form-control"></asp:TextBox>
                                <hr />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Controles</div>
                            <div class="panel-body">
                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control MTop" Text="Generar" OnClick="btngenerar_Click" />

                                <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control MTop" Text="Pass to excel" OnClick="btnexcel_Click" />

                                <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-warning form-control MTop" Text="Limpiar" OnClick="btnlimpiar_Click" />

                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px">
                        <dx:ASPxGridView ID="grdintexproc" runat="server" KeyFieldName="id" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="true"
                            OnDataBinding="grdintexproc_DataBinding" OnCustomCallback="grdintexproc_CustomCallback">
                            <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                            <Columns>
                                <dx:GridViewBandColumn>
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="REPORTE PROCESOS INTEX" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="POrderClient" Caption="PO" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Totalcorte" Caption="UNIDADES CORTE" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="TotalEnviadoDesdePlanta" Caption="ENVIADO DE PLANTA A INTEX" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Secadas" Caption="UNIDADES SECADAS" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="DeficitEnviadoPlantas" Caption="DEFICIT ENVIADO PLANTAS" VisibleIndex="5">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="DeficitSecadoIntex" Caption="DEFICIT SECADO INTEX" VisibleIndex="6">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="UnidadesEnviadas" Caption="UNIDADES ENVIADAS A MEDIDA" VisibleIndex="7">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="DeficitEnvio" Caption="DEFICIT ENVIO A MEDIDA" VisibleIndex="8">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" />
                            <Settings ShowFooter="true" VerticalScrollBarMode="Visible"  />
                            <SettingsPager PageSize="150" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="POrderClient" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Totalcorte" SummaryType="Sum" DisplayFormat="Totales : {0}" />
                                <dx:ASPxSummaryItem FieldName="TotalEnviadoDesdePlanta" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Secadas" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="UnidadesEnviadas" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <GroupSummary>
                                <dx:ASPxSummaryItem FieldName="POrderClient" ShowInGroupFooterColumn="POrderClient" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Totalcorte" ShowInGroupFooterColumn="Totalcorte" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="TotalEnviadoDesdePlanta" ShowInGroupFooterColumn="TotalEnviadoDesdePlanta" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Secadas" ShowInGroupFooterColumn="Secadas" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="UnidadesEnviadas" ShowInGroupFooterColumn="UnidadesEnviadas" SummaryType="Sum" DisplayFormat="{0}" />
                            </GroupSummary>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar" runat="server"></dx:ASPxGridViewExporter>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

