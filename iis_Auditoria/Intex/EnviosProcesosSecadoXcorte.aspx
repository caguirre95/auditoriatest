﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EnviosProcesosSecadoXcorte.aspx.cs" Inherits="Intex_EnviosProcesosSecadoXcorte" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                timer: 500,
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'No se Realizo Ninguna Operacion !',
                timer: 500,
                type: 'info'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Algunos Bultos No Se Guardaron Correctamente !',
                type: 'info'
            });
        }

        function alertClave() {
            swal({
                title: 'Seleccione el Envio !',
                timer: 500,
                type: 'warning'
            });
        }

    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function alertMasterE(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }
    </script>

    <style>
        .mar_t {
            margin: 20px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>

    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="">
            <div class="panel panel-primary" style="margin-top: 10px">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Proceso Secado Por Corte </strong></h3>
                </div>
                <div class="panel-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="HiddenFielduser" runat="server" />
                            <asp:HiddenField ID="hdfcorte" runat="server" />

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <asp:LinkButton Text="" ID="lnkregresar" OnClick="lnkregresar_Click" CssClass="btn btn-warning mar_t form-control" runat="server"><i class="glyphicon glyphicon-arrow-left"> </i>Regresar </asp:LinkButton>

                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <asp:Button runat="server" ID="btnvertodos" OnClick="btnvertodos_Click" Text="Ver Todos" CssClass="btn btn-success pull-right mar_t form-control" />

                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                Envio Dia
                                    <asp:DropDownList ID="drpEnvioDia" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Seleccionar Envio del Dia" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 Envio" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 Envio" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 Envio" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 Envio" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5 Envio" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="6 Envio" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="7 Envio" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="8 Envio" Value="8"></asp:ListItem>
                                    </asp:DropDownList>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <asp:Button runat="server" ID="btnguardarSel" OnClick="btnguardarSel_Click" Text="Guardar Todos Seleccionados" CssClass="btn btn-primary pull-right mar_t form-control" />

                            </div>

                            <style>
                                .w {
                                    width: 100%;
                                    min-width: 1000px;
                                }

                                .vis {
                                    display: none
                                }
                            </style>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="overflow-x: auto">
                                <hr />
                                <asp:GridView ID="gridEnviosSecado" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" DataKeyNames="idSecadoDetalle" OnRowCommand="gridEnviosSecado_RowCommand" CssClass="table table-hover table-striped w" ShowFooter="true" AutoGenerateColumns="false" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField ControlStyle-CssClass="vis">
                                            <ItemTemplate>
                                                <asp:Label ID="lblidsecdet" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("idSecadoDetalle") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                CORTE 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblcorte" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("corteCompleto") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                UNIDADES
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblunidades" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("Unidades") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                ENVIO CORTE
                                            </HeaderTemplate>
                                            <ItemTemplate>                                                
                                                <asp:TextBox runat="server" ID="txtenvio" Width="60px" Font-Size="Medium" CssClass="form-control" MaxLength="2" onkeypress="return num(event);" Text='<%#Bind("NumEnvio") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>

                                                <asp:Button Text="Guardar" CommandName="enviar" CssClass="btn btn-success" runat="server" />
                                                <asp:CheckBox ID="checkEnvio" Text="Enviar" Checked="true" CssClass="btn btn-default" runat="server" />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>


                </div>
            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

