﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RecepcionIntex.aspx.cs" Inherits="Intex_RecepcionIntex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Algunos Registros No Se Guardaron Correctamente !',
                type: 'info'
            });
        }
        function alertClave() {
            swal({
                title: 'Clave Incorrecta !',
                type: 'warning'
            });
        }
        function ClaveCorrect() {
            swal({
                title: 'Clave Correcta, Accion Realizada !',
                type: 'info'
            });
        }

    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }

        function alertMasterE(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }
    </script>

    <style>
        .mar_t {
            margin: 10px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="">
            <div class="panel panel-primary" style="margin-top: 10px;">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Recepcion Intex</strong></h3>
                </div>
                <div class="panel-body">
                    <%--<asp:UpdatePanel runat="server">
                        <ContentTemplate>--%>
                    <asp:MultiView runat="server" ID="multiview">
                        <asp:View runat="server">

                            <asp:HiddenField ID="HiddenFielduser" runat="server" />

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <asp:Button runat="server" ID="btnguardarEnviar" Text="Guardar Recepcion Intex" OnClick="btnguardarEnviar_Click" CssClass="btn btn-info form-control mar_t borderR" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="overflow-x: auto">
                                <style>
                                    .w {
                                        width: 100%;
                                        min-width: 700px;
                                    }
                                </style>
                                <asp:GridView ID="gridrecibir" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" CssClass="table table-hover table-striped w"
                                    AutoGenerateColumns="false" GridLines="None" OnRowCommand="gridrecibir_RowCommand" OnPageIndexChanging="gridrecibir_PageIndexChanging" AllowPaging="true"
                                    PageSize="15">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <HeaderTemplate>
                                                Id 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblidorder" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("Id_Order") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Corte
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPorder" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("Porder") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Estilo
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblStyle" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("Style") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Unidades Enviadas 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblunitssent" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Cantidad") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="false" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Total Unidades 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblquantity" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Quantity") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField Visible="false">
                                            <HeaderTemplate>
                                                POrderClient 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblordencompleta" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("POrderClient") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="checkEnvio" Text="Recepción" Checked="true" CssClass="btn btn-default" runat="server" />
                                                <asp:LinkButton Text="" ID="lnkEnvio" CssClass="btn btn-info" CommandName="Agregar" runat="server"><i class="glyphicon glyphicon-check"> </i> </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" Position="Bottom" PageButtonCount="5" FirstPageText="Primera" LastPageText="Ultima" />
                                    <PagerStyle BackColor="LightBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                </asp:GridView>

                                <%-- </div>--%>
                            </div>

                        </asp:View>
                        <asp:View runat="server">
                        </asp:View>
                    </asp:MultiView>
                    <%-- </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

