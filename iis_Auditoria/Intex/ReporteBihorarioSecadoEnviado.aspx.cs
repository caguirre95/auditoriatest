﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Intex_ReporteBihorarioSecadoEnviado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtdate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["dtenvio"] = null;
        }
    }

    void VerInfo()
    {
        var proc = OrderDetailDA.ReporteBihorarioSecadoEnviado(Convert.ToDateTime(txtdate.Text));
        Session["dtenvio"] = proc;
        grdsecadoenvio.DataSource = Session["dtenvio"];
        grdsecadoenvio.DataBind();
        grdsecadoenvio.GroupBy(grdsecadoenvio.Columns["estilo"]);
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            VerInfo();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            exportar.GridViewID = "grdsecadoenvio";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = exportar;


            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();

                //PdfExportOptions oppdf = new PdfExportOptions();

                options.ExportMode = XlsxExportMode.SingleFile;
                options.SheetName = "SecadoEnvio";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=SecadoEnvio.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }

            ps.Dispose();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Intex/ReporteBihorarioSecadoEnviado.aspx");
    }

    protected void grdsecadoEnvio_DataBinding(object sender, EventArgs e)
    {
        grdsecadoenvio.DataSource = Session["dtenvio"];
    }

    protected void grdsecadoEnvio_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdsecadoenvio.DataBind();
    }
}