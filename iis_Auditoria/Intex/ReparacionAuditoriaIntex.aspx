﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReparacionAuditoriaIntex.aspx.cs" Inherits="Intex_ReparacionAuditoriaIntex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Intex/ReparacionAuditoriaIntex.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porderclient: item.porderclient,
                                    estilo: item.estilo,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porderclient);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porderclient);
                    $('#<%=hdfestilo.ClientID%>').val(ui.item.estilo);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porderclient + "</a>").appendTo(ul);
            };
        })
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtdate.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script>
        function alertDinamica(mes, tipo) {
            swal({
                title: mes,
                type: tipo
            });
        }

        function alertDinamica2(mes, text, tipo) {
            swal({
                title: mes,
                timer: 2000,
                text: text,
                type: tipo
            });
        }

        function alertExitoso() {
            swal({
                title: 'Exito!',
                timer: 1000,
                type: 'success'
            });
        }
        function alertExitosoElim() {
            swal({
                title: 'Exito, Registro Eliminado!',
                timer: 1000,
                type: 'info'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Campos Vacios, llenelos Correctamente !',
                timer: 1000,
                type: 'info'
            });
        }
        function alertClave(r) {
            swal({
                title: 'Ocurrio un evento, ',
                timer: 10000,
                text: r,
                type: 'info'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Campos Vacios, llenelos Correctamente !',
                timer: 1000,
                type: 'info'
            });
        }

        function limpiar(ee) {
            ee.value = '';
        }

        function limpiarauto() {
            $('#<%=txtPorder.ClientID%>').val('');
        }
    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }
    </script>

    <style>
        .m {
            margin-left: 20px;
        }

        .m1 {
            margin-top: 20px;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtdate.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtdate1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading">Reparacion Unidades Auditadas</div>
            <div class="panel-body">

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">Filtros</div>
                        <div class="panel-body">

                            <div class="form-group">
                                Corte:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" CssClass="btn btn-default" ID="LtnMostrar" OnClick="LtnMostrar_Click" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Digite el Corte" onfocus="limpiarauto();" required="true" AutoCompleteType="Disabled" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                            <asp:HiddenField ID="hdfestilo" runat="server" />
                                        </div>
                                    </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">Seleccione la Nueva Fecha de Ingreso de Unidades </div>
                        <div class="panel-body">
                            Fecha Para Actualizar
                            <asp:TextBox ID="txtdate1" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-info">
                        <div class="panel-heading">Comandos</div>
                        <div class="panel-body">
                            <asp:Button ID="btnActualizarAuditoria" runat="server" CssClass="btn btn-warning btn-block " Text="Actualizar Unidades" OnClick="btnActualizarAuditoria_Click" OnClientClick="javascript:return confirm('Esta seguro que desea Actualizar los Datos Seleccionados?');" />
                            <br />
                            <asp:Button ID="btnupdFecha" runat="server" CssClass="btn btn-success btn-block" Text="Actualizar Fecha de Ingreso" OnClick="btnupdFecha_Click" OnClientClick="javascript:return confirm('Esta seguro que desea Actualizar las fechas de los Datos Seleccionados?');" />
                            <br />
                            <%--<asp:Button ID="btnEliminarAuditoria" runat="server" CssClass="btn btn-danger btn-block" Text="Eliminar Corte Completo" OnClick="btnEliminarsecado_Click" OnClientClick="javascript:return confirm('Esta seguro que desea eliminar el Corte?');" />--%>
                            <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-info btn-block" Text="Limpiar" OnClick="btnlimpiar_Click" />
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">Agregar Unidades</div>
                        <div class="panel-body">
                            <div class="col-lg-3">
                                Unidades
                            <asp:TextBox ID="txtunitsadd" runat="server" CssClass="form-control" placeholder="Digite Unidades" MaxLength="4" onkeypress="return num(event);"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                Auditoria (Pass/Fail)
                            <asp:CheckBox ID="chkadd" runat="server" Checked="false" Text="" CssClass="form-control" />
                            </div>
                            <div class="col-lg-3">
                                Fecha
                                <asp:TextBox ID="txtdate" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-4">
                                Comentario
                            <asp:TextBox ID="txtcomenadd" runat="server" CssClass="form-control" placeholder="Digite Comentario" TextMode="MultiLine"></asp:TextBox>
                            </div>

                            <div class="col-lg-12">
                                <hr />
                                <asp:Button ID="btnagregar" runat="server" CssClass="btn btn-info btn-block form-control" Text="Agregar Unidades Auditadas" OnClick="btnagregar_Click" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">Información General del Corte</div>
                        <div class="panel-body">

                            <asp:GridView ID="gridAuditoriaInf" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" DataKeyNames="corteCompleto" CssClass="table-responsive table-hover table"
                                AutoGenerateColumns="false" GridLines="None">
                                <Columns>

                                    <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                        <HeaderTemplate>
                                            CORTE
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblcorte" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("corteCompleto") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                        <HeaderTemplate>
                                            UNIDADES DEL CORTE
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblunidades" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("unidadesCorteCompleto") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                        <HeaderTemplate>
                                            ESTILO 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblestilo" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("estilo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                        <HeaderTemplate>
                                            UNIDADES AUDITADAS 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblunitsaudit" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("UnidadesAuditadas") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            ULTIMA FECHA AUDITADO
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblfecha" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("UltimaFechaAuditoria","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <hr />
                    <asp:GridView ID="gridAuditoria" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" DataKeyNames="idAuditoriaSecado" CssClass="table-responsive table-hover table"
                        AutoGenerateColumns="false" GridLines="None" OnRowCommand="gridAuditoria_RowCommand">
                        <Columns>

                            <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                <HeaderTemplate>
                                    No.
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblrn" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField Visible="false" ItemStyle-CssClass="padd_t">
                                <HeaderTemplate>
                                    ID
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblid" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("idAuditoriaSecado") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                <HeaderTemplate>
                                    CORTE 
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblcorte" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("corteCompleto") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                <HeaderTemplate>
                                    ESTILO 
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblestilo" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("estilo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    UNIDADES AUDITADAS
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtunidades" Enabled="false" Width="100px" Font-Size="Medium" CssClass="form-control" Text='<%#Bind("unidadesAuditadas") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                <HeaderTemplate>
                                    FECHA INGRESO
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblfecha" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("fechaAuditoria","{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    ACTUALIZAR UNIDADES 
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtunits" Font-Size="Medium" CssClass="form-control" AutoCompleteType="Disabled" onkeypress="return num(event);" MaxLength="4" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton Text="" ID="lnkupd" CssClass="btn btn-info" runat="server" CommandName="Actualizar" ToolTip="Update"><i class="glyphicon glyphicon-indent-left"> </i> </asp:LinkButton>
                                    <asp:CheckBox ID="chk1" runat="server" Text="Actualizar" Checked="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

