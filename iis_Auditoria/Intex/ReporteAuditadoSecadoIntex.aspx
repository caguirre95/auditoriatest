﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteAuditadoSecadoIntex.aspx.cs" Inherits="Intex_ReporteAuditadoSecadoIntex" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraReports.v16.2.Web, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtdate.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <style>
        .he {
            height: 96px !important;
            width: 968px;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Reporte Bihorario</strong></div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Controles</div>
                            <div class="panel-body">
                                <strong>Fecha</strong>
                                <asp:TextBox ID="txtdate" runat="server" CssClass="form-control"></asp:TextBox>
                                <hr />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Controles</div>
                            <div class="panel-body">
                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control MTop" Text="Generar" OnClick="btngenerar_Click" />

                                <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control MTop" Text="Pass to excel" OnClick="btnexcel_Click" />

                                <asp:Button ID="btnpdf" runat="server" CssClass="btn btn-danger form-control MTop" Text="Pass to PDF" OnClick="btnpdf_Click" />
                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px">
                        <dx:ASPxGridView ID="grdbihoSec" runat="server" KeyFieldName="id" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="true"
                            OnDataBinding="grdbihoSec_DataBinding" OnCustomCallback="grdbihoSec_CustomCallback">
                            <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                            <Columns>
                                <dx:GridViewBandColumn>
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="INFORME DE ENTREGA - AREA DE SECADO" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="Por este medio certifico que las unidades recibidas del area de secado esta debidamente contado y auditado para su respectivo cobro.">
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn FieldName="estilo" Caption="STYLE" VisibleIndex="0">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="corteCompleto" Caption="PO" VisibleIndex="1">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="TotalDia" Caption="UNIDADES CONTADAS" VisibleIndex="2">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="TotalAudit" Caption="UNIDADES AUDITADAS" VisibleIndex="3">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="Porcentaje" Caption="PORCENTAJE" VisibleIndex="4">
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
                                                </dx:GridViewBandColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" />
                            <Settings ShowFooter="true" VerticalScrollBarMode="Visible" />
                            <SettingsPager PageSize="150" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="corteCompleto" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="TotalDia" SummaryType="Sum" DisplayFormat="Totales : {0}" />
                                <dx:ASPxSummaryItem FieldName="TotalAudit" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Variacion" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <GroupSummary>
                                <dx:ASPxSummaryItem FieldName="corteCompleto" ShowInGroupFooterColumn="corteCompleto" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="TotalDia" ShowInGroupFooterColumn="TotalDia" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="TotalAudit" ShowInGroupFooterColumn="TotalAudit" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Variacion" ShowInGroupFooterColumn="Variacion" SummaryType="Sum" DisplayFormat="{0}" />
                            </GroupSummary>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar" runat="server"></dx:ASPxGridViewExporter>
                    </div>


                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Report</h3>
                            </div>
                            <div class="panel-body">
                                <dx:ASPxWebDocumentViewer ID="ASPxWebDocumentViewer1" Visible="true" runat="server">
                                </dx:ASPxWebDocumentViewer>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>

