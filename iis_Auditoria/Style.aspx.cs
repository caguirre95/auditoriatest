﻿using DevExpress.Web;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Style : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (User.Identity.IsAuthenticated == true)
        {
            if (!IsPostBack)
            {

            }
            carga();
            style();
        }
        else
        {
            FormsAuthentication.SignOut();
            Response.Redirect("/Login.aspx", false);
        }
    }

    void style()
    {
        try
        {
            string s = " select Id_Style as id, Style, isnull(Price,0) as Price, isnull(Washed,0) as Washed from Style";
            DataTable st = DataAccess.Get_DataTable(s);
            cmbstyle.DataSource = st;
            cmbstyle.ValueField = "id";
            cmbstyle.TextField = "Style";
            cmbstyle.DataBind();
        }
        catch (Exception ex)
        {
            string e = ex.Message;
        }

    }

    void carga()
    {
        try
        {
            string s = " select Id_Style as id, Style, isnull(Price,0) as Price, isnull(Washed,0) as Washed from Style";
            DataTable st = DataAccess.Get_DataTable(s);
            gridStyle.DataSource = st;
            gridStyle.DataBind();
        }
        catch (Exception ex)
        {
            string e = ex.Message;
        }
    }

    protected void btnestadoact_Click(object sender, EventArgs e)
    {
        try
        {
            MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
            string userId = mu.ProviderUserKey.ToString();
            ASPxCheckBox chkstylew = new ASPxCheckBox();
            string ok = string.Empty;

            for (int i = 0; i <= gridStyle.VisibleRowCount; i++)
            {
                int id = Convert.ToInt32(gridStyle.GetRowValues(i, "id"));

                if (gridStyle.FindRowCellTemplateControl(i, (GridViewDataColumn)gridStyle.Columns["Wash"], "chkstylew") != null)
                {
                    chkstylew = (ASPxCheckBox)gridStyle.FindRowCellTemplateControl(i, (GridViewDataColumn)gridStyle.Columns["Wash"], "chkstylew");
                    ((IPostBackDataHandler)chkstylew).LoadPostData("chkstylew", Request.Form);

                    try
                    {
                        ok = DataAccess.updateStyleWash(id, chkstylew.Checked);
                        if (ok == "OK")
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki();", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki_bad();", true);
                        }
                    }
                    catch (Exception ex)
                    {
                        MailMessage mailObj = new MailMessage("pgarcia@rocedes.com.ni", "dmartinez@rocedes.com.ni", "Sistema Auditoria", ex.ToString() + "Usuario : " + mu.UserName);
                        SmtpClient SMTPServer = new SmtpClient("rocedes.com.ni");
                        SMTPServer.Send(mailObj);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            string el = ex.Message;
        }
    }

    void limpiar()
    {
        txtstylenew.Text = "";
        txtprice.Text = "";
        chkwash.Checked = false;
    }

    private bool IsNumeric(string num)
    {
        try
        {
            double x = Convert.ToDouble(num);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    protected void btnagregarnewline_Click(object sender, EventArgs e)
    {
        try
        {
            if (IsNumeric(txtprice.Text))
            {
                string v = " select Id_Style as id, Style, isnull(Price,0) as Price, Wash from Style where Style = '" + txtstylenew.Text + "'";/*https:152.231.39.178:4446*/
                DataTable ver = DataAccess.Get_DataTable(v);
                if (ver.Rows.Count < 1)
                {
                    DataAccess.SaveStyle(txtstylenew.Text, Convert.ToDecimal(txtprice.Text), chkwash.Checked);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki();", true);
                    limpiar();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki_bad1('" + txtstylenew.Text + "');", true);
                    limpiar();
                }
            }
            else
            {
                limpiar();
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki_bad2();", true);
            }
            //ScriptManager.RegisterStartupScript(this, GetType(), "Update", "gridStyle.Refresh();", true);
        }
        catch (Exception ex)
        {
            string a = ex.Message;
        }
    }

    //void lim()
    //{
    //    txtstyle.Text = "";
    //}

    //protected void btnStyle_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        //lblnm.Text = txtstyle.Text;
    //        string a = " select Id_Style as id from Style where Id_Style = " + Convert.ToInt32(txtstylenew.Text);
    //        DataTable av = DataAccess.Get_DataTable(a);
    //        int s = Convert.ToInt32(av.Rows[0][0]);
    //        //DataAccess.UpdateStyleName(Convert.ToInt32(s), txtstyle.Text);
    //        //lim();
    //        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki();", true);
    //    }
    //    catch (Exception ex)
    //    {
    //        string a = ex.Message;
    //    }
    //}

    protected void cmbstyle_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string s = " select Id_Style as id, Style, isnull(Price,0) as Price, Wash from Style where Id_Style = '" + cmbstyle.SelectedItem.Value + "'";
            DataTable d = DataAccess.Get_DataTable(s);
            txtstilo.Text = d.Rows[0][1].ToString();
            txtpriceedit.Text = d.Rows[0][2].ToString();
            checkboxstyle.Checked = Convert.ToBoolean(d.Rows[0][3]);
        }
        catch (Exception ex)
        {
            string a = ex.Message;
        }
    }

    void limpiar1()
    {
        cmbstyle.Text = "";
        txtstilo.Text = "";
        txtpriceedit.Text = "";
        checkboxstyle.Checked = false;
    }

    protected void btnedit_Click(object sender, EventArgs e)
    {
        try
        {
            var a = DataAccess.Update_Style_All(Convert.ToInt32(cmbstyle.SelectedItem.Value), txtstilo.Text, Convert.ToSingle(txtpriceedit.Text==string.Empty ? "0.0": txtpriceedit.Text), checkboxstyle.Checked);
            limpiar1();
            carga();
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki();", true);
        }
        catch (Exception ex)
        {
            string a = ex.Message;
        }
    }
}