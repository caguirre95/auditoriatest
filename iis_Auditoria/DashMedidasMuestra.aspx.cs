﻿using DevExpress.Web;
using DevExpress.XtraCharts;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DashMedidasMuestra : System.Web.UI.Page
{
    int cliente = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated == false)
                {
                    FormsAuthentication.SignOut();
                    Response.Redirect("Login.aspx", false);
                }
                
                cliente = Convert.ToInt16(Request.QueryString["idcliente"]);

                DataTable dt1 = DataAccess.Get_DataTable("select * from Cliente where Id_Cliente="+ cliente );

                lblClienteN.Text = "";
                if (dt1 != null)
                    lblClienteN.Text ="Packing " + dt1.Rows[0][1].ToString();

                DateTime date = DateTime.Now;

                CultureInfo norwCulture = CultureInfo.CreateSpecificCulture("es");

                System.Globalization.Calendar cal = norwCulture.Calendar;

                int weekNo = cal.GetWeekOfYear(date, norwCulture.DateTimeFormat.CalendarWeekRule, norwCulture.DateTimeFormat.FirstDayOfWeek);

                week.Value = weekNo.ToString();    
                
                Session["dt0"] = null;

                Session["dt"] = null;

                cargagrid();

                cargaMain();

                string url = HttpContext.Current.Request.Url.AbsoluteUri;

                Response.AppendHeader("Refresh", "50; URL=" + url);
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    void cargaMain()
    {
        string sql = " select isnull(sum(case estado when 0 then 1 else 0 end),0) as Failed,isnull(sum(case estado when 1 then 1 else 0 end),0) as Passed"
                   + " from tbCajasInspeccionMuestra ci join porder p on ci.idPorder=p.Id_Order  where p.Id_Cliente="+ cliente +"  and convert(date, fecha)= convert(date, GETDATE())";

        var dt = DataAccess.Get_DataTable(sql);

        if (dt.Rows.Count > 0)
        {
            Label1.Text = (Convert.ToInt32(dt.Rows[0][0].ToString()) + Convert.ToInt32(dt.Rows[0][1].ToString())).ToString();

            Label7.Text = dt.Rows[0][0].ToString();

            Label4.Text = dt.Rows[0][1].ToString();

            Label2.Text = "0 %";

            Label3.Text = "0 %";

            if (Label1.Text != "0")
            {
                Label2.Text =Math.Round((Convert.ToInt32(dt.Rows[0][1].ToString()) / Convert.ToDecimal(Label1.Text)) * 100, 2) + "%";

                Label3.Text =Math.Round((Convert.ToInt32(dt.Rows[0][0].ToString()) / Convert.ToDecimal(Label1.Text)) * 100, 2)+ "%";

                var bandera = Math.Round((Convert.ToInt32(dt.Rows[0][0].ToString()) / Convert.ToDecimal(Label1.Text)) * 100, 2);

                if (bandera > 4)
                {
                    lblCalculoAQL.ForeColor = Color.Red;

                    lblStatus.Text = "Warning";

                    lblStatus.ForeColor = Color.Red;
                }
                else
                {
                    lblCalculoAQL.ForeColor = Color.Green;

                    lblStatus.Text = "Good";

                    lblStatus.ForeColor = Color.Green;
                }
            }

        }
    }    

    protected void grid_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grid.DataBind();
    }

    protected void grid_DataBinding(object sender, EventArgs e)
    {
        grid.DataSource = Session["dt"];

        grid.ExpandAll();
    }

    protected void ASPxGridView1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView1.DataBind();
    }

    protected void ASPxGridView1_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView1.DataSource = Session["dt0"];
    }

    void cargagrid()
    {     
        DataTable dt = new DataTable();

        DataTable dt2 = new DataTable();

        int resp = 2;

        DateTime date = DateTime.Now;

        System.Globalization.CultureInfo norwCulture = System.Globalization.CultureInfo.CreateSpecificCulture("es");

        System.Globalization.Calendar cal = norwCulture.Calendar;

        int weekNo = cal.GetWeekOfYear(date, norwCulture.DateTimeFormat.CalendarWeekRule, norwCulture.DateTimeFormat.FirstDayOfWeek);

        int num1=0;

        int num2=0;

        dt2 = OrderDetailDA.RepDashboard4Muestra(resp, weekNo, cliente);

        dt = OrderDetailDA.RepDashboard1Muestra(resp, weekNo, cliente);        

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i][1].Equals("Passed"))
            {
                num1 = num1 + Convert.ToInt32(dt.Rows[i][2].ToString());
            }
            else if (dt.Rows[i][1].Equals("Failed"))
            {
                num2 = num2 + Convert.ToInt32(dt.Rows[i][2].ToString());
            }
        }

        WebChartControl1.DataSource = dt2;

        WebChartControl1.Titles.Clear();
      
        WebChartControl1.Titles.Add(new ChartTitle { Dock = ChartTitleDockStyle.Top, Text = "Current Week" });
          
        WebChartControl1.DataBind();
     
        WebChartControl2.DataSource = dt;

        WebChartControl2.Titles.Clear();
     
        WebChartControl2.Titles.Add(new ChartTitle { Dock = ChartTitleDockStyle.Top, Text = "Current Week" });

        WebChartControl2.Titles.Add(new ChartTitle { Dock = ChartTitleDockStyle.Left, TextColor = Color.Green, Text = "" + num1 + " | " + num2 });

        WebChartControl2.DataBind();

        dt2 = OrderDetailDA.RepDashboard4Muestra(resp, weekNo-1,cliente);
      
        dt = OrderDetailDA.RepDashboard1Muestra(resp, weekNo - 1, cliente);

        num1 = 0; num2 = 0;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i][1].Equals("Passed"))
            {
                num1 = num1 + Convert.ToInt32(dt.Rows[i][2].ToString());
            }
            else if (dt.Rows[i][1].Equals("Failed"))
            {
                num2 = num2 + Convert.ToInt32(dt.Rows[i][2].ToString());
            }
        }

        WebChartControl3.DataSource = dt2;

        WebChartControl3.Titles.Clear();   
        
        WebChartControl3.Titles.Add(new ChartTitle { Dock = ChartTitleDockStyle.Top, Text = "Last Week" }); 
        
        WebChartControl3.DataBind(); 
        
        WebChartControl4.DataSource = dt;

        WebChartControl4.Titles.Clear();     
        
        WebChartControl4.Titles.Add(new ChartTitle { Dock = ChartTitleDockStyle.Top, Text = "Last Week" });

        WebChartControl4.Titles.Add(new ChartTitle { Dock = ChartTitleDockStyle.Left, TextColor = Color.Green, Text = "" + num1 + " | " + num2 });

        WebChartControl4.DataBind();
       
        dt = OrderDetailDA.RepDashboard2Muestra(resp, cliente);

        Session["dt0"] = dt;
        
        dt = OrderDetailDA.RepDashboard3Muestra(resp, cliente);

        Session["dt"] = dt;

        ASPxGridView1.DataBind();

        grid.DataBind();

    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        cargagrid();

        cargaMain();
    }

    protected void ASPxGridView1_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;

        ASPxLabel label = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, null, "lbltexto") as ASPxLabel;

        if (label.Text.TrimEnd() == "In Spec")
            label.ForeColor = System.Drawing.Color.Green;
        else if (label.Text.TrimEnd() == "Small")
            label.ForeColor = System.Drawing.Color.Blue;
        else if (label.Text.TrimEnd() == "Big")
            label.ForeColor = System.Drawing.Color.Red;

    }
}