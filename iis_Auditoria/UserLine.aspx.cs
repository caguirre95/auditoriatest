﻿using SistemaAuditores.DataAccess;
using System;
using System.Data;
using System.Net.Mail;
using System.Web.UI;

public partial class UserLine : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.User.IsInRole("Administrator"))
        {
            userLine();
        }
        else
        {
            //FormsAuthentication.SignOut();
            Response.Redirect("/NotFound.aspx");
        }        
    }

    void userLine()
    {
        string usuario = Page.User.Identity.Name.ToString();
        
        if (usuario == "Roc6")
        {
            string us = " select um.userms as id,"
                      + " um.userName as Name"
                      + " from tblUserMedida um"
                      + " join Linea l on l.id_linea = um.linea"
                      + " where l.id_Planta = 3"
                      + " order by um.userName asc";
            DataTable user = DataAccess.Get_DataTable(us);
            cmbuser.DataSource = user;
            cmbuser.ValueField = "id";
            cmbuser.TextField = "Name";
            cmbuser.DataBind();

            string l = " select id_linea as id,"
                     + " numero as Linea"
                     + " from Linea"
                     + " where id_Planta = 3"
                     + " and numero not like '%-%'"
                     + " order by numero asc";
            DataTable lin = DataAccess.Get_DataTable(l);
            cmbline.DataSource = lin;
            cmbline.ValueField = "id";
            cmbline.TextField = "Linea";
            cmbline.DataBind();

            string grUs = " select um.userms as id, um.userName as Name, l.numero as Line"
                        + " from tblUserMedida um"
                        + " join Linea l on l.id_linea = um.linea"
                        + "  where l.id_planta = 3"
                        + " order by l.numero asc";
            DataTable grUser = DataAccess.Get_DataTable(grUs);
            grduser.DataSource = grUser;
            grduser.DataBind();
        }
        else if (usuario == "Planta03")
        {
            string us = " select um.userms as id,"
                      + " um.userName as Name"
                      + " from tblUserMedida um"
                      + " join Linea l on l.id_linea = um.linea"
                      + " where l.id_Planta = 2"
                      + " order by um.userName asc";
            DataTable user = DataAccess.Get_DataTable(us);
            cmbuser.DataSource = user;
            cmbuser.ValueField = "id";
            cmbuser.TextField = "Name";
            cmbuser.DataBind();

            string l = " select id_linea as id,"
                    + " numero as Linea"
                    + " from Linea"
                    + " where id_Planta = 2"
                    + " and numero not like '%-%'"
                    + " order by numero asc";
            DataTable lin = DataAccess.Get_DataTable(l);
            cmbline.DataSource = lin;
            cmbline.ValueField = "id";
            cmbline.TextField = "Linea";
            cmbline.DataBind();

            string grUs = " select um.userms as id, um.userName as Name, l.numero as Line"
                        + " from tblUserMedida um"
                        + " join Linea l on l.id_linea = um.linea"
                        + "  where l.id_planta = 2"
                        + " order by l.numero asc";
            DataTable grUser = DataAccess.Get_DataTable(grUs);
            grduser.DataSource = grUser;
            grduser.DataBind();
        }
        else if (usuario == "Planta1")
        {
            string us = " select um.userms as id,"
                      + " um.userName as Name"
                      + " from tblUserMedida um"
                      + " join Linea l on l.id_linea = um.linea"
                      + " where l.id_Planta = 1"
                      + " order by um.userName asc";
            DataTable user = DataAccess.Get_DataTable(us);
            cmbuser.DataSource = user;
            cmbuser.ValueField = "id";
            cmbuser.TextField = "Name";
            cmbuser.DataBind();

            string l = " select id_linea as id,"
                     + " numero as Linea"
                     + " from Linea"
                     + " where id_Planta = 1"
                     + " and numero not like '%-%'"
                     + " order by numero asc";
            DataTable lin = DataAccess.Get_DataTable(l);
            cmbline.DataSource = lin;
            cmbline.ValueField = "id";
            cmbline.TextField = "Linea";
            cmbline.DataBind();

            string grUs = " select um.userms as id, um.userName as Name, l.numero as Line"
                        + " from tblUserMedida um"
                        + " join Linea l on l.id_linea = um.linea"
                        + " where l.id_planta = 1"
                        + " order by l.numero asc";
            DataTable grUser = DataAccess.Get_DataTable(grUs);
            grduser.DataSource = grUser;
            grduser.DataBind();
        }
        else if (usuario == "Administrator")
        {
            string sql = "SELECT Id_planta, Descripcion FROM Planta WHERE estado = 1";
            DataTable planta = DataAccess.Get_DataTable(sql);
            cmbPlanta.DataSource = planta;
            cmbPlanta.ValueField = "Id_planta";
            cmbPlanta.TextField = "Descripcion";
            cmbPlanta.DataBind();

            int idPlanta = 0;
            string filtroPlanta = "";
            string filtroPlanta2 = "";            
            if (cmbPlanta.SelectedIndex != -1)
            {
                idPlanta = Convert.ToInt32(cmbPlanta.SelectedItem.Value);
                filtroPlanta = string.Format(" where l.id_Planta = {0}", idPlanta);
                filtroPlanta2 = string.Format(" where id_Planta = {0}", idPlanta);
                
            }

            string us = " select um.userms as id,"
                      + " um.userName as Name"
                      + " from tblUserMedida um"
                      + " join Linea l on l.id_linea = um.linea"
                      + filtroPlanta
                      + " order by um.userName asc";

            DataTable user = DataAccess.Get_DataTable(us);
            cmbuser.DataSource = user;
            cmbuser.ValueField = "id";
            cmbuser.TextField = "Name";
            cmbuser.DataBind();

            string l = " select id_linea as id,"
                     + " numero as Linea"
                     + " from Linea"
                     + filtroPlanta2
                     + " order by numero asc";
            DataTable lin = DataAccess.Get_DataTable(l);
            cmbline.DataSource = lin;
            cmbline.ValueField = "id";
            cmbline.TextField = "Linea";
            cmbline.DataBind();

            string grUs = " select um.userms as id, um.userName as Name, l.numero as Line"
                        + " from tblUserMedida um"
                        + " join Linea l on l.id_linea = um.linea"
                        + filtroPlanta
                        + " order by l.numero asc";
            DataTable grUser = DataAccess.Get_DataTable(grUs);
            grduser.DataSource = grUser;
            grduser.DataBind();
        }
    }

    void limpiar()
    {
        cmbuser.Text = "";
        cmbline.Text = "";
    }

    protected void btnGuadar_Click(object sender, EventArgs e)
    {
        try
        {
            if (cmbuser.Text == "" || cmbline.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "incorrecto();", true);
            }
            else
            {
                DataAccess.Update_UserLine(cmbuser.SelectedItem.Value.ToString(),
                                           Convert.ToInt32(cmbline.SelectedItem.Value));
                limpiar();
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "exito();", true);
            }
        }
        catch (Exception ex)
        {
            MailMessage mailObj = new MailMessage("pgarcia@rocedes.com.ni", "dmartinez@rocedes.com.ni", "Psr", ex.ToString());
            SmtpClient SMTPServer = new SmtpClient("rocedes.com.ni");
            SMTPServer.Send(mailObj);
        }
    }
}