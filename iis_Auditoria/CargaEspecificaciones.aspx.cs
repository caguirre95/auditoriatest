﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CargaEspecificaciones : System.Web.UI.Page
{
    class objMedidas
    {

        public int idMedida { get; set; }
        public string medida { get; set; }
        public decimal valor { get; set; }
    }

    class objtallas
    {

        public int idtalla { get; set; }
        public string talla { get; set; }
        public int valor { get; set; }


        public int idPOM { get; set; }
        public string puntomedida { get; set; }

        public int idmedidas { get; set; }
        public string medidas { get; set; }
        public decimal medidasdec { get; set; }

    }

    class objespecificaciones
    {
        public int idPOM { get; set; }
        public int idtalla { get; set; }
        public int idestilo { get; set; }
        public int rango { get; set; }
        public string Estilo { get; set; }
        public double valorspec { get; set; }
        public double valorMax { get; set; }
        public double valorMin { get; set; }
    }
 
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                cargaestilos();
            }

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            string file = Path.GetFileName(FileUpload1.FileName);

            if (string.IsNullOrEmpty(file))
            {
                string tipo = "warning";
                string ms = "Debe Seleccionar un archivo";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ms + "','" + tipo + "');", true);
                return;
            }
            string Name = Path.GetFileNameWithoutExtension(FileUpload1.FileName);

            var ruta = Server.MapPath("~/Documentos/" + file);

            if (drpEstilos.SelectedItem.Text != "Select...")
            {
                FileUpload1.SaveAs(ruta);

                funcionprincipal(ruta);
            }
            else
            {
                string tipo = "warning";
                string ms = "Debe Seleccionar El Estilo";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ms + "','" + tipo + "');", true);
            }

        }
        catch (Exception)
        {

            throw;
        }
    }

    void cargaestilos()
    {
        var dt1 = DataAccess.Get_DataTable("select Id_Style, Style from Style where Id_Cliente = 92 order by Style desc");

        drpEstilos.DataSource = dt1;
        drpEstilos.DataTextField = "Style";
        drpEstilos.DataValueField = "Id_Style";
        drpEstilos.DataBind();
        drpEstilos.Items.Insert(0, "Select...");

    }

    private void funcionprincipal(string ruta)
    {
        try
        {

            List<objMedidas> listaMedidas = new List<objMedidas>();

            var dtm = DataAccess.Get_DataTable("select * from tblMedidas order by valor desc");

            foreach (DataRow item in dtm.Rows)
            {
                var obj = new objMedidas
                {
                    idMedida = Convert.ToInt16(item[0].ToString()),
                    medida = item[1].ToString().Trim(),
                    valor = Convert.ToDecimal(item[2].ToString())
                };

                listaMedidas.Add(obj);

            }

            List<objtallas> lista = new List<objtallas>();
            List<objespecificaciones> listaFinal = new List<objespecificaciones>();
            #region    

            using (OleDbConnection conn = new OleDbConnection())
            {
                DataTable dt = new DataTable();

                string Import_FileName = ruta;
                string fileExtension = Path.GetExtension(Import_FileName);
                if (fileExtension == ".xls")
                    conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 8.0;HDR=NO;IMEX=1;'";
                if (fileExtension == ".xlsx")
                    conn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 12.0 Xml;HDR=NO;IMEX=1;'";
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertArchivo();", true);
                    return;
                }

                string cadena = "estilo";


                
                int idestilo = 0;

                idestilo = Convert.ToInt32(drpEstilos.SelectedValue);
                using (OleDbCommand comm = new OleDbCommand())
                {
                    comm.CommandText = "Select * from [" + cadena + "$]";
                    comm.Connection = conn;
                    dt = new DataTable();

                    using (OleDbDataAdapter da = new OleDbDataAdapter())
                    {
                        da.SelectCommand = comm;
                        da.Fill(dt);


                        //extraer tallas de formato
                        int columnas = 0;
                        for (int i = 3; i < dt.Columns.Count; i++)
                        {
                            if (!dt.Rows[3][i].ToString().Equals(""))
                            {
                                columnas = i;
                            }
                        }

                        for (int i = 3; i <= columnas; i++)
                        {
                            var size = dt.Rows[1][i].ToString().Replace(" ", "").Trim().ToUpper();
                            //var col = dt.Rows[0][i + 1].ToString();



                            //if (!string.IsNullOrEmpty(size) && string.IsNullOrEmpty(col) && i <= columnas)
                            //{
                            //    var idtalla = OrderDetailDA.SaveTallaParametroSalida(size.Replace("X","*"));

                            //    var obj = new objtallas();
                            //    obj.idtalla = idtalla;
                            //    obj.talla = size;
                            //    obj.valor = 1;

                            //    var obj1 = new objtallas();
                            //    obj1.idtalla = idtalla;
                            //    obj1.talla = size;
                            //    obj1.valor = 2;

                            //    lista.Add(obj);
                            //    lista.Add(obj1);

                            //    i++;

                            //}
                            //else 
                            //if (!string.IsNullOrEmpty(size) && i <= columnas)
                            //{
                                var idtalla = OrderDetailDA.SaveTallaParametroSalida(size.Replace("*","X"));

                                var obj = new objtallas();
                                obj.idtalla = idtalla;
                                obj.talla = size;
                                obj.valor = 1;

                                lista.Add(obj);

                            //}


                        }

                        string[] separators = { "," };


                        for (int i = 3; i < dt.Rows.Count; i++)
                        {

                            var puntomedida = dt.Rows[i][1].ToString().ToUpper().TrimEnd().TrimStart();

                            if(string.IsNullOrEmpty(puntomedida))
                            {
                                break;
                            }

                            var idpuntoM = OrderDetailDA.SavePuntoMedidaParametroSalida(puntomedida);
                            var tol = dt.Rows[i][2].ToString().Trim().Replace(" ","");

                            var band = tol.IndexOf(',');

                            var tolMX = 0.0;
                            var tolMN = 0.0;

                            if (band > 1)
                            {
                                var arr = tol.Split(separators, StringSplitOptions.RemoveEmptyEntries);

                                //var dt1 = DataAccess.Get_DataTable(" select * from tblMedidas where Medida ='" + arr[0].Replace(" ", "") + "'");
                                //var dt2 = DataAccess.Get_DataTable(" select * from tblMedidas where Medida ='" + arr[1].Replace(" ", "") + "'");

                                tolMX = Convert.ToDouble(listaMedidas.FirstOrDefault(x => x.medida.Equals(arr[0].Replace(" ", ""))).valor); //dt1.Rows[0][2].ToString()
                                tolMN = Convert.ToDouble(listaMedidas.FirstOrDefault(x => x.medida.Equals(arr[1].Replace(" ", ""))).valor);

                            }
                            else
                            {
                                var valor1 = tol.Replace("+/-", "+");
                                var valor2 = tol.Replace("+/-", "-");

                                //var dt1 = DataAccess.Get_DataTable(" select * from tblMedidas where Medida ='" + valor1.Replace(" ", "") + "'");
                                //var dt2 = DataAccess.Get_DataTable(" select * from tblMedidas where Medida ='" + valor2.Replace(" ", "") + "'");
                                tolMX = Convert.ToDouble(listaMedidas.FirstOrDefault(x => x.medida.Equals(valor1.Replace(" ", ""))).valor); //dt1.Rows[0][2].ToString()
                                tolMN = Convert.ToDouble(listaMedidas.FirstOrDefault(x => x.medida.Equals(valor2.Replace(" ", ""))).valor);


                                //tolMX = Convert.ToDouble(dt1.Rows[0][2].ToString());
                                //tolMN = Convert.ToDouble(dt2.Rows[0][2].ToString());
                            }


                            int contList = 0;// lista.Count();
                            for (int j = 3; j <= columnas; j++)
                            {
                                try
                                {
                                    var valor = dt.Rows[i][j].ToString().Trim() == "" ? "0" : dt.Rows[i][j].ToString().Trim();

                                    if (!valor.Equals("0"))
                                    {
                                        var valorReal = double.Parse(dt.Rows[i][j].ToString());

                                        var valorAddTolmax = valorReal + tolMX;
                                        var valorAddTolmn = valorReal - Math.Abs(tolMN);

                                        var obj3 = lista[contList];

                                        var objespc = new objespecificaciones
                                        {
                                            idPOM = idpuntoM,
                                            idtalla = obj3.idtalla,
                                            idestilo = idestilo,
                                            valorspec = valorReal,
                                            valorMax = valorAddTolmax,
                                            valorMin = valorAddTolmn,
                                            rango = obj3.valor,
                                            Estilo = cadena

                                        };

                                        listaFinal.Add(objespc);

                                        contList++;
                                    }
                                    else
                                        contList++;


                                }
                                catch (Exception ex)
                                {

                                    var r=contList;
                                    var resppp = ex.Message;
                                   // throw;
                                }
                                

                            }


                        }//fin ciclo for

                    }//fin using OleDbDataAdapter

                }//fin de using OleDbCommand

            }//fin de using OleDbConnection 

            #endregion

            int contador = 0;
            foreach (var item in listaFinal)
            {
                try
                {
                    var resp = OrderDetailDA.saveEspecificacionNew(item.idestilo, item.idtalla, item.idPOM, item.valorspec, item.valorMax, item.valorMin, Page.User.Identity.Name, item.rango);

                    if (resp!="OK")
                    {
                        contador++;
                    }
                }
                catch (Exception)
                {
                    contador++;
                   
                }
              

            }


            if (contador == 0)
            {
                cargaestilos();

                string tipo = "success";
                string mess = "Guardado exitosamente!";


                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
            }
            else
            {
                string tipo = "warning";
                string mess = "Ocurrio un error es posible que algunos regsitros no se guardaron";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
            }

        }
        catch (Exception ex)
        {
            var mes = ex.Message;

            string tipo = "warning";
        
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mes + "','" + tipo + "');", true);
        }

    }

}