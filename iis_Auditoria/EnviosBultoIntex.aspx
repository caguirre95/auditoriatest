﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EnviosBultoIntex.aspx.cs" Inherits="Intex_EnviosBultoIntex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/pagination.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="js/bootstrap.min.js"></script>--%>

    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>
    <link href="jquery-ui.css" rel="stylesheet" />
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Algunos Bultos No Se Guardaron Correctamente !',
                type: 'info'
            });
        }

    </script>

    <style>
        .mar_t {
            margin: 10px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="">
            <div class="panel panel-primary" style="margin-top: 10px;">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Bihorario de Envio </strong></h3>
                </div>
                <div class="panel-body">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:MultiView runat="server" ID="multiview">
                                <asp:View runat="server">

                                    <asp:HiddenField ID="HiddenFielduser" runat="server" />

                                    <%--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        Bihorario
                                    <asp:DropDownList ID="drpBihorario" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Select..." Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 Bihorario" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 Bihorario" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 Bihorario" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 Bihorario" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5 Bihorario" Value="5"></asp:ListItem>
                                    </asp:DropDownList>
                                    </div>--%>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        Envio Dia
                                    <asp:DropDownList ID="drpEnvioDia" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Select..." Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 Envio" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 Envio" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 Envio" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 Envio" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5 Envio" Value="5"></asp:ListItem>
                                    </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        Planta a Enviar
                                        <asp:DropDownList ID="drpplantasent" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">Select...</asp:ListItem>
                                            <asp:ListItem Value="5">Rocedes 2</asp:ListItem>
                                            <asp:ListItem Value="4">Rocedes 7</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <%--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <asp:Button runat="server" ID="btnguardar" OnClick="btnguardar_Click" Text="Guardar Bihorario" CssClass="btn btn-primary  mar_t" />

                                    </div>--%>

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <asp:Button runat="server" ID="btnbultosInco" OnClick="btnbultosInco_Click" Text="Bultos Incommpletos" CssClass="btn btn-default  mar_t" Visible="false" />
                                        <asp:Button runat="server" ID="Button1" OnClick="Button1_Click" Text="Bihorario" CssClass="btn btn-default  mar_t" Visible="false" />
                                        Linea
                                         <asp:DropDownList runat="server" ID="drpLinea" OnSelectedIndexChanged="drpLinea_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                         </asp:DropDownList>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <asp:Button runat="server" ID="btnguardarEnviar" OnClick="btnguardarEnviar_Click" Text="Guardar Envio" CssClass="btn btn-primary  mar_t" />

                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                        <asp:Button runat="server" ID="btnVertodos" OnClick="btnVertodos_Click" Text="Ver Todos " CssClass="btn btn-info  mar_t" />
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="overflow:scroll hidden">
                                        <asp:GridView ID="gridEnvios" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" CssClass="table table-hover table-striped " AutoGenerateColumns="false" GridLines="None" Width="100%">
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <HeaderTemplate>
                                                        Bulto 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblidorder" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("Id_Order") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        Linea 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbllinea" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("Linea") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        Porder
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPorder" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("Porder") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                    <HeaderTemplate>
                                                        Quantity 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblquantity" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Quantity") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        Unidades
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtunidades" Enabled="false" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Cantidad") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        Envio Corte
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtenvio" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Envio") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton Text="" ID="lnkEnvio" OnClick="lnkEnvio_Click" CssClass="btn btn-info" runat="server"><i class="glyphicon glyphicon-indent-left"> </i> </asp:LinkButton>
                                                        <asp:CheckBox ID="checkEnvio" Text="Enviar" Checked="true" CssClass="btn btn-default" runat="server" />
                                                        <asp:LinkButton Text="" ID="lnkseccionado" OnClick="lnkseccionado_Click" CssClass="btn btn-success" runat="server"><i class="glyphicon glyphicon-object-align-bottom"> </i> </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                        <%-- </div>--%>
                                    </div>

                                </asp:View>
                                <asp:View runat="server">

                                    <div class="col-lg-12">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            Bihorario
                                            <asp:DropDownList ID="drpbihorario2" CssClass="form-control" runat="server">
                                                <asp:ListItem Text="Select..." Value="0"></asp:ListItem>
                                                <asp:ListItem Text="1 Bihorario" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2 Bihorario" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3 Bihorario" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="4 Bihorario" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="5 Bihorario" Value="5"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            Linea
                                         <asp:DropDownList runat="server" ID="drplinea2" OnSelectedIndexChanged="drplinea2_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                         </asp:DropDownList>
                                        </div>

                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                            <asp:Button runat="server" ID="btnguardarIncompleto" OnClick="btnguardarIncompleto_Click" Text="Guardar Bihorario" CssClass="btn btn-primary  mar_t" />
                                            <asp:Button runat="server" ID="btnvertodos2" OnClick="btnvertodos2_Click" Text="Ver Todos " CssClass="btn btn-info  mar_t" />
                                            <asp:Button runat="server" ID="btnbultosComple" OnClick="btnbultosComple_Click" Text="Bultos Completos" CssClass="btn btn-default  mar_t" />
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                            <asp:GridView ID="gridenvioIncompleto" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" CssClass="table table-hover table-striped " ShowFooter="true" AutoGenerateColumns="false" GridLines="None" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField Visible="false">
                                                        <HeaderTemplate>
                                                            Bulto 
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblidorder" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("Id_Order") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                        <HeaderTemplate>
                                                            Linea 
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbllinea" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("Linea") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                        <HeaderTemplate>
                                                            Porder
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPorder" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("POrder") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                                        <HeaderTemplate>
                                                            Quantity 
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblquantity" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Quantity") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            Unidades
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" ID="txtunidades" Enabled="false" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Cantidad") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="checkEnvio" Text="Enviado" Checked="true" CssClass="form-control" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>

                                            <%-- </div>--%>
                                        </div>

                                    </div>

                                </asp:View>
                            </asp:MultiView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

