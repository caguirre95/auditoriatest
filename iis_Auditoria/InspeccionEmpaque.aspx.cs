﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Threading.Tasks;
using System.Drawing;

public partial class InspeccionEmpaque : System.Web.UI.Page
{
    class listas
    {
        public string id { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (User.Identity.IsAuthenticated == true)
            {
                Session["defectos"] = null;
                Session["posicion"] = null;

                string idorder, idauditoria;

                idorder = (string)Request.QueryString["idOrder"];
                idauditoria = (string)Request.QueryString["idAuditoria"];

                if (idorder == "")
                    idorder = "0";


                carga();
                cargagrid();
                Load_Data(idorder, idauditoria);
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    void carga()
    {
        DataTable dtDefecto = new DataTable();
        dtDefecto = DataAccess.Get_DataTable("select idDefecto as idDefecto,Defecto as Defecto from tbDefectos WHERE Tipo='Sewing' and Cliente=92 order by Defecto  asc");

        DataTable dtPosicion = new DataTable();
        dtPosicion = DataAccess.Get_DataTable("select idPosicion,Posicion as Codigo from tbPosicion where Estado = 1 order by Codigo asc");

        Session["defectos"] = dtDefecto;
        Session["posicion"] = dtPosicion;

    }

    void Load_Data(string idporder, string idauditoria)
    {

        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();

        dt = DataAccess.Get_DataTable(" select po.Id_Order,po.POrder,po.Description,po.Quantity,po.Bundles,c.Cliente,s.Style,min(b.Color)as color"
                                     + " from POrder po join Cliente c on po.Id_Cliente=c.Id_Cliente join Style s on po.Id_Style=s.Id_Style"
                                     + " join Bundle  b on b.Id_Order = po.Id_Order where po.Id_Order = " + idporder
                                     + " group by po.Id_Order, po.POrder, po.Description, po.Quantity, po.Bundles, c.Cliente, s.Style");

        dt1 = DataAccess.Get_DataTable("SELECT ISNULL(SUM(CantidadDefecto),0) Total FROM tbDefectoEmpaqueDet dt JOIN tbDefectoEmpaque d ON d.IdDefEmpaque = dt.IdDefEmpaque WHERE d.IdCorte = " + idporder);

        if (dt == null)
        {
            Response.Redirect("Default.aspx");
        }

        if (dt.Rows.Count > 0)
        {
            lblorder1.Text = Convert.ToString(dt.Rows[0][1]); //extraemos numero de Porder
            lblorder2.Text = lblorder1.Text;
            lblcustomer.Text = dt.Rows[0][5].ToString();

            txtporder.Text = dt.Rows[0][1].ToString();
            txtstyle.Text = dt.Rows[0][6].ToString();
            txtdescrip.Text = dt.Rows[0][2].ToString();
            txtcantidad.Text = dt.Rows[0][3].ToString();
            txtcolor.Text = dt.Rows[0][7].ToString();

            txtpiezas.Text = dt1.Rows[0][0].ToString();
        }
        else
        {
            var mess = "No se encontraron registros para esta busqueda!";
            string tipo = "info";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
            return;
        }


    }

    void cargagrid()
    {
        List<listas> lista = new List<listas>();

        for (int i = 1; i <= 15; i++)
        {
            var obj = new listas();
            obj.id = i.ToString();
            lista.Add(obj);
        }

        gridAddDefectos.DataSource = lista;
        gridAddDefectos.DataBind();
    }

    void limpiar()
    {
        cargagrid();
        //txtpiezas.Text = "";
    }

    protected void lnkNew_Click(object sender, EventArgs e)
    {
        string idauditoria = (string)Request.QueryString["idAuditoria"];
        int idlinea = Convert.ToInt16(Request.QueryString["idLinea"]);
        Response.Redirect("SearchIL.aspx?idAuditoria=" + idauditoria + "&idLinea=" + idlinea + "&idSeccion=0");
    }

    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string idauditoria = (string)Request.QueryString["idAuditoria"];
            int idlinea = Convert.ToInt16(Request.QueryString["idLinea"]);
            Response.Redirect("SearchIL.aspx?idAuditoria=" + idauditoria + "&idLinea=" + idlinea + "&idSeccion=0");
        }
        catch (Exception)
        {

        }
    }

    protected void gridAddDefectos_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddldef = (DropDownList)e.Row.FindControl("ddldefectos");
                DropDownList ddlpos = (DropDownList)e.Row.FindControl("ddlposicion");

                var dt1 = (DataTable)Session["defectos"];
                var dt2 = (DataTable)Session["posicion"];

                ddldef.DataSource = dt1;
                ddldef.DataTextField = "Defecto";
                ddldef.DataValueField = "idDefecto";
                ddldef.DataBind();
                ddldef.Items.Insert(0, "");

                ddlpos.DataSource = dt2;
                ddlpos.DataTextField = "Codigo";
                ddlpos.DataValueField = "idPosicion";
                ddlpos.DataBind();
                ddlpos.Items.Insert(0, "");

            }
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + ex.Message + "','" + tipo + "');", true);

        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            string respp = "";
            var idcorte = txtporder.Text.Trim() == "" ? "0" : txtporder.Text.Trim();

            var resp = OrderDetailDA.spdGuardarDefectoEmp(idcorte, 0, HttpContext.Current.User.Identity.Name);//Agregar Nombre Auditor

            if (resp > 0)
            {
                foreach (GridViewRow item in gridAddDefectos.Rows)
                {
                    DropDownList ddldef = (DropDownList)item.FindControl("ddldefectos");
                    DropDownList ddlpos = (DropDownList)item.FindControl("ddlposicion");
                    TextBox txtcant = (TextBox)item.FindControl("txtcantidaddef");

                    if (ddldef.SelectedItem.Text != "" && ddlpos.SelectedItem.Text != "")
                    {
                        var resp1 = OrderDetailDA.spdGuardarDefectoEmpDet(resp, Convert.ToInt32(ddldef.SelectedValue), Convert.ToInt32(ddlpos.SelectedValue), txtcant.Text == string.Empty ? 1 : Convert.ToInt16(txtcant.Text));

                        respp = resp1;
                    }
                }

                if (respp == "OK")
                {
                    string message = "Guardado Correctamente!!!";
                    //string message = "Guardado Correctamente!!!";
                    string tipo = "success";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
                    limpiar();
                    Load_Data(idcorte.ToString(), "7");
                }
                else
                {
                    string message = "Error al Guardar Datos!!!";
                    string tipo = "warning";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
                }
            }
        }
        catch (Exception ex)
        {
            string message = ex.Message;
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        try
        {
            limpiar();
        }
        catch (Exception)
        {

            throw;
        }
    }
}