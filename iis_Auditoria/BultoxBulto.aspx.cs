﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using SistemaAuditores.DataAccess;
using System.Web.Security;
using System.Web.UI.HtmlControls;

public partial class _InspeccionxBulto : System.Web.UI.Page
{
    string Id_Order, Id_Seccion;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (User.Identity.IsAuthenticated == true)
            {
                string Id_Order, Id_Seccion;

                Id_Order = (string)Request.QueryString["id_order"];
                Id_Seccion = (string)Request.QueryString["id_seccion"];

                if (Id_Order == "")
                    Id_Order = "0";

                Load_Data(Id_Order, Id_Seccion);

                
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

    }

    public void Load_Data(string idporder, string idseccion)
    {

        DataTable dt = new DataTable();
        dt = OrderDetailDA.Get_OrderbyId(idporder);

        if (dt == null)
        {
            Response.Redirect("Default.aspx");
        }

        if (dt.Rows.Count > 0)
        {
            lblorder1.Text = Convert.ToString(dt.Rows[0][1]); //extraemos numero de Porder
            lblorder2.Text = lblorder1.Text;
            lblcustomer.Text = dt.Rows[0][5].ToString();

            txtporder.Text = dt.Rows[0][1].ToString();
            txtstyle.Text = dt.Rows[0][6].ToString();
            txtdescrip.Text = dt.Rows[0][2].ToString();
            txtcantidad.Text = dt.Rows[0][3].ToString();
            txtbultos.Text = dt.Rows[0][4].ToString();
        }
        else
        {
            lblnoRows.Text = "No se encontraron registros para esta busqueda!";
            return;
        }

        MembershipUser user = Membership.GetUser(Page.User.Identity.Name);
        string UserId = user.ProviderUserKey.ToString();

        DataTable dt_Inspector = DataAccess.Get_DataTable("select si.idseccionInspector from seccionInspector si join Inspector i on si.idInspector=i.id_inspector where i.UserId='" + UserId + "' and si.idseccion=" + idseccion);
        string id_inspector = Convert.ToString(dt_Inspector.Rows[0][0]);

        DataTable dt_ODetails = new DataTable();
        dt_ODetails = OrderDetailDA.Get_OrderDetailbyIdOrderBulto(Convert.ToInt32(idporder), Convert.ToInt32(idseccion),id_inspector);

        DataColumn Muestreo = new DataColumn("Muestreo", typeof(string));
        dt_ODetails.Columns.Add(Muestreo);

        for (int i = 0; i <= dt_ODetails.Rows.Count - 1; i++)
        {
            dt_ODetails.Rows[i][7] = dt_ODetails.Rows[i][6];// dt_IdMuestreo.Rows[0][0];
        }

        GridBultos.DataSource = dt_ODetails;
        GridBultos.DataBind();

    }

    protected void GridBultos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label idbulto = (Label)e.Row.FindControl("lblidbundle");
                string Id_Seccion = (string)Request.QueryString["id_seccion"];
                string query = "Select * from BundleRejectedBulto br join seccionInspector si on br.idseccionInsp = si.idseccionInspector  where Id_Bundle = " + idbulto.Text + " and si.idseccion ="+ Id_Seccion;
               // string query = "Select * from BundleRejectedBundle br join OperacionOperario op on br.id_operarioOpe = op.id_operarioOpe  where Id_Bundle = " + idbulto.Text + " and op.id_seccion =" + Id_Seccion;
                DataTable bundle_rejected = DataAccess.Get_DataTable(query);

                query = "select idAreaDefectoRec from AreaDefectoRechazoBundleB where idbundle=" + idbulto.Text + " and idseccion=" + Id_Seccion;
                DataTable bundle_rejectedDefecto = DataAccess.Get_DataTable(query);

                LinkButton lk = (LinkButton)e.Row.FindControl("comando");
                LinkButton lk1 = (LinkButton)e.Row.FindControl("comandoRep");

                if (bundle_rejected.Rows.Count > 0 || bundle_rejectedDefecto.Rows.Count > 0)
                {
                    RadioButtonList rbl_aprob1 = (RadioButtonList)e.Row.FindControl("RadioButtonList1");
                    rbl_aprob1.SelectedValue = "0";//Rechazado
                    lk.CssClass = "btn btn-danger";
                    lk1.Visible = true;
                }
                else
                {
                    lk1.Visible = false;
                    lk.CssClass = "btn btn-default";
                }

            }
        }
        catch (Exception ex)
        {
            Console.Write(ex.Message);

        }

    }

    protected void GridBultos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string Id_Order = (string)Request.QueryString["id_order"];
            int Id_Seccion = Convert.ToInt32(Request.QueryString["id_seccion"]);
            int Id_Linea = Convert.ToInt16(Request.QueryString["id_linea"]);

            if (Id_Order == "")
            {
                Response.Redirect("Secciones.aspx");
            }

           
            if (e.CommandName == "Editar")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int indice = row.RowIndex;
                string Id_Bundle = GridBultos.DataKeys[indice].Value.ToString();

                RadioButtonList rbl_aprob = (RadioButtonList)row.FindControl("RadioButtonList1");
                int status = Convert.ToInt32(rbl_aprob.SelectedValue);

                if (status == 0)
                {
                    Response.Redirect("BultoABultoReject.aspx?IdBundle=" + Id_Bundle + "&IdOrder=" + Id_Order + "&id_seccion=" + Id_Seccion + "&id_linea=" + Id_Linea + "&style=" + txtstyle.Text + "&customer=" + lblcustomer.Text, false);
                }
                else
                {
                    try
                    {
                        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
                        string userId = mu.ProviderUserKey.ToString();

                        string quanti = row.Cells[5].Text;
                        DataTable dt_Muestreo = DataAccess.Get_DataTable("declare @input float =" + quanti + " SELECT * FROM Muestreo WHERE @input between rango_incial and rango_final and nivel_insp = (Select levelaql from AQL)");
                        int Id_Muestreo = Convert.ToInt16(dt_Muestreo.Rows[0][0]);

                        DateTime fechahora_registro = DateTime.Now;//FECHA DE REGISTRO
                        string hora_registro = DateTime.Now.ToShortTimeString();//HORA DE REGISTRO

                        string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

                        DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio FROM Biohorario WHERE @input between rango1 and rango2");
                        int Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);

                        string query = "Select * from BundleRejectedBulto br join seccionInspector si on br.idseccionInsp = si.idseccionInspector  where Id_Bundle = " + Convert.ToString(Id_Bundle) + " and si.idseccion =" + Id_Seccion;
                       // string query = "Select * from BundleRejectedBulto br join OperacionOperario op on br.id_operarioOpe = op.id_operarioOpe  where Id_Bundle = " + Convert.ToString(Id_Bundle) + " and op.id_seccion =" + Id_Seccion;
                        DataTable dt_rechazos = DataAccess.Get_DataTable(query);

                        if (dt_rechazos.Rows.Count > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "bultomalo();", true);
                        }
                        else
                        {
                            OrderDetailDA.SaveBundlesApprovedBulto(int.Parse(Id_Bundle), Id_Biohorario, Id_Seccion, userId, Id_Muestreo, Convert.ToDateTime(fechahora_registro), Convert.ToString(hora_registro), 0);
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                            Load_Data(Id_Order, Id_Seccion.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
                    }

                }

            }
            if (e.CommandName == "Reparar")
            {
                try
                {


                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    string Id_Bundle = GridBultos.DataKeys[indice].Value.ToString();

                    MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
                    string userId = mu.ProviderUserKey.ToString();

                    string quanti = row.Cells[5].Text;
                    DataTable dt_Muestreo = DataAccess.Get_DataTable("declare @input float =" + quanti + " SELECT * FROM Muestreo WHERE @input between rango_incial and rango_final and nivel_insp = (Select levelaql from AQL)");
                    int Id_Muestreo = Convert.ToInt16(dt_Muestreo.Rows[0][0]);

                    DateTime fecha_recibido = DateTime.Now;//FECHA DE REGISTRO
                    string hora_registro = DateTime.Now.ToShortTimeString();//HORA DE REGISTRO

                    string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

                    DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio FROM Biohorario WHERE @input between rango1 and rango2");
                    int Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);

                    OrderDetailDA.SaveBundlesRepairBulto(Convert.ToInt32(Id_Bundle), Id_Biohorario, Id_Seccion, userId,Id_Muestreo, Convert.ToDateTime(fecha_recibido), Convert.ToString(date_now));
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                    Load_Data(Id_Order, Id_Seccion.ToString());
                }
                catch (Exception ex)
                {
                    string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
                }
            }

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        //btnsave.Enabled = false;
        //foreach (GridViewRow row in grv_defectos.Rows)
        //{
        //    TextBox txt = row.FindControl("txtfirma") as TextBox;
        //    if (txt != null)
        //    {
        //        string value = txt.Text;
        //    }
        //    else
        //    {
        //        string error = "alert('Ingrese su firma!');";
        //        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
        //    }
        //}
        //this.AsignarDefectos.ShowOnPageLoad = false;
    }

    protected void lnkNew_Click(object sender, EventArgs e)
    {
        int id_seccion = Convert.ToInt16(Request.QueryString["id_seccion"]);
        int id_linea = Convert.ToInt16(Request.QueryString["id_linea"]);
        Response.Redirect("SearchIL.aspx?id_seccion=" + id_seccion + "&id_linea=" + id_linea);
    }

    protected void btnGuardarTodos_Click(object sender, EventArgs e)
    {
        try
        {
            Id_Order = (string)Request.QueryString["id_order"];
            int Id_Seccion = Convert.ToInt32(Request.QueryString["id_seccion"]);
            int Id_Linea = Convert.ToInt16(Request.QueryString["id_linea"]);

            if (Id_Order == "")
            {
                Response.Redirect("Secciones.aspx");
            }

            foreach (GridViewRow Row in GridBultos.Rows)
            {
                RadioButtonList rd = (RadioButtonList)Row.FindControl("RadioButtonList1");

                if (rd.SelectedValue == "1")
                {
                    Label Id_OrderDetail = (Label)Row.FindControl("lblOrderDetail");
                    int Id_Bundle = Convert.ToInt32(Id_OrderDetail.Text);

                    string query = "Select * from BundleRejectedBulto br join seccionInspector si on br.idseccionInsp = si.idseccionInspector  where Id_Bundle = "+ Convert.ToString(Id_Bundle) + " and si.idseccion =" + Id_Seccion;                   
                    //string query = "Select * from BundleRejectedBundle br join OperacionOperario op on br.id_operarioOpe = op.id_operarioOpe  where Id_Bundle = " + Id_Bundle + " and op.id_seccion =" + Id_Seccion;

                    DataTable dt_rechazos = DataAccess.Get_DataTable(query);

                    if (dt_rechazos.Rows.Count <= 0)
                    {
                        //  ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "bultomalo();", true);
                        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
                        string UserId = mu.ProviderUserKey.ToString();

                        Label lblQuantity = (Label)Row.FindControl("lblQuantity");
                        DataTable dt_Muestreo = DataAccess.Get_DataTable("declare @input float =" + lblQuantity.Text + " SELECT * FROM Muestreo WHERE @input between rango_incial and rango_final and nivel_insp = (Select levelaql from AQL)");
                        int Id_Muestreo = Convert.ToInt32(dt_Muestreo.Rows[0][0]);

                        DateTime fechahora_registro = DateTime.Now;//FECHA DE REGISTRO
                        string hora_registro = DateTime.Now.ToShortTimeString();//HORA DE REGISTRO
                        int cantidad_defecto = 0;// CANTIDAD DE DEFECTOS

                        string date_now = string.Format("{0}.{1}", DateTime.Now.Hour, DateTime.Now.Minute.ToString("00"));

                        DataTable Bihorarios = DataAccess.Get_DataTable("declare @input float =" + date_now + " SELECT id_bio FROM Biohorario WHERE @input between rango1 and rango2");
                        int Id_Biohorario = Convert.ToInt16(Bihorarios.Rows[0][0]);

                        OrderDetailDA.SaveBundlesApprovedBulto(Id_Bundle, Id_Biohorario, Id_Seccion, UserId, Id_Muestreo, Convert.ToDateTime(fechahora_registro), Convert.ToString(hora_registro), Convert.ToInt16(cantidad_defecto));
                    }
                }


            }

            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
            Load_Data(Id_Order, Id_Seccion.ToString());
        }
        catch (Exception)
        {
            string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);


        }

    }


}
