﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SearchIL.aspx.cs" Inherits="Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtpo.ClientID%>').autocomplete({
                source: function (request, response) {
                    var idl = $('#<%=HiddenFieldidAuditoria.ClientID%>').val();
                    $.ajax({
                        url: "SearchIL.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "','idl' :'" + idl + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    Id_Style: item.Id_Style,
                                    Id_Order: item.Id_Order,
                                    Id_Linea: item.Id_Linea,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtpo.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtpo.ClientID%>').val(ui.item.porder);
                    $('#<%=txtestilo.ClientID%>').val(ui.item.style);
                    $('#<%=HiddenFieldidStyle.ClientID%>').val(ui.item.Id_Style);
                    $('#<%=HiddenFieldidorder.ClientID%>').val(ui.item.Id_Order);
                    $('#<%=HiddenFieldidlinea.ClientID%>').val(ui.item.Id_Linea);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });


        function limpiar() {

            $('#<%=txtpo.ClientID%>').val('');
            $('#<%=txtestilo.ClientID%>').val('');
            $('#<%=HiddenFieldidStyle.ClientID%>').val('');
            $('#<%=HiddenFieldidorder.ClientID%>').val('');
            $('#<%=HiddenFieldidlinea.ClientID%>').val('');
        }
    </script>


    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script type="text/javascript" src="/scripts/jquery.blockUI.js"></script>

    <%-- <style>
        #ContentPlaceHolder2_txtestilo, #ContentPlaceHolder2_txtpo {
            text-transform: uppercase !important;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2 class="quitPadH">
        <asp:Label ID="lbl" runat="server" Text="Production Order Search"></asp:Label></h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <script type="text/javascript">
        function BlockUI(elementID) {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(function () {
                $("#" + elementID).block({
                    message: '<table><tr><td>' + '<img src="/img/please_wait.gif"/></td></tr></table>',
                    css: {},
                    overlayCSS: { backgroundColor: '#FFFFFF', opacity: 0.6, border: '1px solid #000000' }
                });
            });
            prm.add_endRequest(function () {
                $("#" + elementID).unblock();
            });
        }
        $(document).ready(function () {
            BlockUI("search_panel");
            $.blockUI.defaults.css = {};
        });
    </script>

    <div class="container">
        <div class="col-lg-6 col-lg-offset-3 Csearch_panel">
            <asp:UpdatePanel runat="server" ID="Panel">
                <ContentTemplate>
                    <div id="search_panel">
                        <asp:HiddenField ID="HiddenFieldidAuditoria" runat="server" />
                        <asp:HiddenField ID="HiddenFieldidStyle" runat="server" />
                        <asp:HiddenField ID="HiddenFieldidlinea" runat="server" />
                        <asp:HiddenField ID="HiddenFieldidorder" runat="server" />
                        <div class="col-lg-10 col-lg-offset-1 centrar">
                            <img src="img/searchS.png" width="150" alt="Alternate Text" />
                        </div>
                        <div class="col-lg-5 MTop">
                            <strong>
                                <asp:Label ID="lblpo" runat="server" Text="NUMERO DE ORDEN: "></asp:Label></strong>
                            <asp:TextBox ID="txtpo" onfocus="limpiar();" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtpo"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-5 MTop">
                            <strong>
                                <asp:Label ID="lblestilo" runat="server" Text="ESTILO #: "></asp:Label></strong>
                            <asp:TextBox ID="txtestilo" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="txtestilo"></asp:RequiredFieldValidator>

                        </div>
                        <div class="col-lg-2 MTop">
                            <asp:Button ID="btnbuscar" runat="server" CssClass="btn btn-primary MTop" Text="Search" OnClick="btnbuscar_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <div style="margin: 0 auto; width: 100%; text-align: center; font-size: 12px; color: Red;">
                <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>

</asp:Content>

