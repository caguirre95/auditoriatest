﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InspecProcesoSemaforo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (User.Identity.IsAuthenticated == true)
            {
                string idorder, idauditoria, idmodulo, linea;

                idorder = (string)Request.QueryString["idOrder"];

                idauditoria = (string)Request.QueryString["idAuditoria"];
                
                idmodulo = (string)Request.QueryString["idModulo"];

                linea= (string)Request.QueryString["idLinea"];

                var guid = Guid.NewGuid();

                lbllinea.Text = linea;

                hdnguid.Value = guid.ToString();
                Session["guid"] = guid.ToString();

                if (idorder == "")
                    idorder = "0";

                //carga informacion del corte, inspector y bultos de acuerdo al módulo (hoja de corte)
                carga();
                Load_Data(idorder, idauditoria, idmodulo);

            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

    }

    public class Oper
    {
        public string codOperario { get; set; }
        public string nombreOperario { get; set; }
        public string operacion { get; set; }
        public string operacionSh { get; set; }

    }

    private class listas
    {
        public string id { get; set; }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Oper> GetOperario(string pre)
    {
        try
        {
            List<Oper> list = new List<Oper>();

            DataTable dt = new DataTable();

         
            string query = "select empno,loc1,loc2, firstname + ' ' +lastname as nombreCompleto, badgeno  from employee where firstname like '" + pre.ToUpper() +"%'";

            dt = DataAccessPervasive.GetDataTable(query);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Oper obj = new Oper();

                obj.codOperario = dt.Rows[i][4].ToString();
                obj.nombreOperario = dt.Rows[i][3].ToString();
                
                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Oper> GetOperacion(string pre)
    {
        try
        {
            List<Oper> list = new List<Oper>();

            DataTable dt = new DataTable();

            string query = "select distinct top 10 DESCR,ctdescr from oper where descr like '%" + pre.ToUpper() +"%' order by char_length(descr) asc";

         
            dt = DataAccessPervasive.GetDataTable(query);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Oper obj = new Oper();

                obj.operacion = dt.Rows[i][0].ToString();
                obj.operacionSh = dt.Rows[i][1].ToString();
                
                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public void Load_Data(string idporder, string idauditoria, string idmodulo)
    {

        DataTable dt = new DataTable();
        dt = OrderDetailDA.Get_OrderbyId(idporder);

        if (dt == null)
        {
            Response.Redirect("Default.aspx");
        }

        if (dt.Rows.Count > 0)
        {
            lblorder1.Text = Convert.ToString(dt.Rows[0][1]); //extraemos numero de Porder
            lblorder2.Text = lblorder1.Text;
            lblcustomer.Text = dt.Rows[0][5].ToString();

            txtporder.Text = dt.Rows[0][1].ToString();
            txtstyle.Text = dt.Rows[0][6].ToString();
            txtdescrip.Text = dt.Rows[0][2].ToString();
            txtcantidad.Text = dt.Rows[0][3].ToString();
            txtbultos.Text = dt.Rows[0][4].ToString();
        }
        else
        {
            lblnoRows.Text = "No se encontraron registros para esta busqueda!";
            string tipo = "info";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + lblnoRows.Text + "','" + tipo + "');", true);
            return;
        }

        MembershipUser user = Membership.GetUser(Page.User.Identity.Name);
        string UserId = user.ProviderUserKey.ToString();

        DataTable dt_Inspector = DataAccess.Get_DataTable("select id_inspector from Inspector where UserId='" + UserId + "'");
        int idInspector = Convert.ToInt16(dt_Inspector.Rows[0][0]);

        DataTable dt_ODetails = new DataTable();
        dt_ODetails = OrderDetailDA.GetBultoByOrder(Convert.ToInt32(idporder));

        GridBultos.DataSource = dt_ODetails;
        GridBultos.DataBind();

    }


    /// <summary>
    /// detecta bultos rechazados de acuerdo al módulo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridBultos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label idbulto = (Label)e.Row.FindControl("lblidbundle");
                string idmodulo = (string)Request.QueryString["idModulo"];
                string query = "select idBBRechazado from tbInspecProcRechazado  where  idBulto = " + idbulto.Text + " and idModulo=" + idmodulo;

                DataTable bundle_rejected = DataAccess.Get_DataTable(query);

                LinkButton lk = (LinkButton)e.Row.FindControl("comando");
                LinkButton lk1 = (LinkButton)e.Row.FindControl("comandoRep");

                if (bundle_rejected.Rows.Count > 0)
                {
                    RadioButtonList rbl_aprob1 = (RadioButtonList)e.Row.FindControl("RadioButtonList1");
                    rbl_aprob1.SelectedValue = "0";//Rechazado
                    lk.CssClass = "btn btn-danger";
                    lk1.Visible = true;
                }
                else
                {
                    lk1.Visible = false;
                    lk.CssClass = "btn btn-default";
                }

            }
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
            return;

        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridBultos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string idorder = (string)Request.QueryString["idOrder"];
            string idauditoria = (string)Request.QueryString["idAuditoria"];
            int idlinea = Convert.ToInt16(Request.QueryString["idLinea"]);
            string idmodulo = (string)Request.QueryString["idModulo"];

            //pendiente de revision
            if (idorder == "")
            {
                Response.Redirect("Default.aspx");
                //  Response.Redirect("Secciones.aspx");
            }

            if (e.CommandName == "Editar")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int indice = row.RowIndex;
                string idbulto = GridBultos.DataKeys[indice].Value.ToString();

                RadioButtonList rbl_aprob = (RadioButtonList)row.FindControl("RadioButtonList1");
                int status = Convert.ToInt32(rbl_aprob.SelectedValue);

                if (status == 0)
                {
                    if (hdnCodigooperario.Value == string.Empty || txtoperacion.Text == string.Empty)
                    {
                        string mess = "Debe resgistrar operario y operación!";
                        string tipo = "info";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
                        return;

                    }

                    lbloperacion.Text = txtoperacion.Text;
                    lbloperario.Text = txtoperario.Text;

                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "myModal();", true);


                   /// Response.Redirect("InspecProcesoRechazo.aspx?idBulto=" + idbulto + "&idOrder=" + idorder + "&idAuditoria=" + idauditoria + "&idLinea=" + idlinea + "&idModulo=" + idmodulo + "&style=" + txtstyle.Text + "&customer=" + lblcustomer.Text + "&cut=" + lblorder1.Text, false);
                }
                else
                {
                    try
                    {
                        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
                        string userId = mu.ProviderUserKey.ToString();

                        string query = "select idBBRechazado from tbInspecProcRechazado ba where IdBulto= " + idbulto + " and idModulo=" + idmodulo + " and Operacion='" + txtoperacion.Text +"'"  ;             
                        DataTable dt_rechazos = DataAccess.Get_DataTable(query);

                        if (dt_rechazos.Rows.Count > 0)
                        {
                            string mess = "Por favor ingrese la cantidad o seleccione un defecto u operacion!";
                            string tipo = "info";
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
                            return;
                            //string mess = "El bulto esta rechazado debe repararlo!!!";
                            //string tipo = "info";
                            //ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
                        }
                        else
                        {
                            if (hdnCodigooperario.Value==string.Empty || txtoperacion.Text==string.Empty )
                            {
                                string mess = "Debe resgistrar operario y operación!";
                                string tipo = "info";
                                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
                                return;

                            }

                            DataTable dtporder = new DataTable();
                            dtporder = DataAccess.Get_DataTable("select b.Quantity from Bundle b  where b.Id_Bundle=" + idbulto);
                            int muestra = 0;
                            var quantity = int.Parse(dtporder.Rows[0][0].ToString());
                            if (quantity < 25)
                            {
                                muestra = 5;
                            }
                            else if (quantity > 24)
                            {
                                muestra = 8;
                            }

                            OrderDetailDA.SaveIPApprovedSem(int.Parse(idbulto), userId, idlinea, Convert.ToInt32(idmodulo), muestra,hdnCodigooperario.Value.Trim(),txtoperario.Text.Trim(),txtoperacion.Text.Trim());
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                            Load_Data(idorder, idauditoria, idmodulo);

                        }
                    }
                    catch (Exception ex)
                    {
                        string tipo = "info";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
                    }

                }

            }
            if (e.CommandName == "Reparar")
            {
                try
                {

                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    string Id_Bundle = GridBultos.DataKeys[indice].Value.ToString();

                    OrderDetailDA.SaveIPRepair(Convert.ToInt32(Id_Bundle), Convert.ToInt16(idmodulo));
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                    Load_Data(idorder, idauditoria, idmodulo);

                }
                catch (Exception ex)
                {
                    //  string mess = "Por favor ingrese la cantidad o seleccione un defecto u operacion!";
                    string tipo = "info";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
                }
            }

        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        //btnsave.Enabled = false;
        //foreach (GridViewRow row in grv_defectos.Rows)
        //{
        //    TextBox txt = row.FindControl("txtfirma") as TextBox;
        //    if (txt != null)
        //    {
        //        string value = txt.Text;
        //    }
        //    else
        //    {
        //        string error = "alert('Ingrese su firma!');";
        //        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
        //    }
        //}
        //this.AsignarDefectos.ShowOnPageLoad = false;
    }

    protected void lnkNew_Click(object sender, EventArgs e)
    {
        string idauditoria = (string)Request.QueryString["idAuditoria"];
        int idlinea = Convert.ToInt16(Request.QueryString["idLinea"]);
        string idmodulo = (string)Request.QueryString["idModulo"];
        Response.Redirect("SearchIL.aspx?idAuditoria=" + idauditoria + "&idLinea=0&idSeccion=" + idmodulo);
        //   Response.Redirect("SearchIL.aspx?idAuditoria=" + idauditoria + "&idLinea=" + idlinea);
    }

    protected void btnGuardarTodos_Click(object sender, EventArgs e)
    {
        try
        {
            string idorder = (string)Request.QueryString["idOrder"];
            string idauditoria = (string)Request.QueryString["idAuditoria"];
            int idlinea = Convert.ToInt16(Request.QueryString["idLinea"]);
            string idmodulo = (string)Request.QueryString["idModulo"];

            string resp = "true";
            int cont = 0;
            //revisar ***
            if (idorder == "")
            {
                //Response.Redirect("Secciones.aspx");
            }

            foreach (GridViewRow Row in GridBultos.Rows)
            {
                RadioButtonList rd = (RadioButtonList)Row.FindControl("RadioButtonList1");

                if (rd.SelectedValue == "1")
                {
                    Label Id_OrderDetail = (Label)Row.FindControl("lblidbundle");
                    int Id_Bundle = Convert.ToInt32(Id_OrderDetail.Text);

                    //string query = "select idBBRechazado from tbInspecProcRechazado ba where IdBulto= " + Convert.ToString(Id_Bundle);
                    string query = "select idBBRechazado from tbInspecProcRechazado ba where IdBulto= " + Id_Bundle + " and idModulo=" + idmodulo +" and Operacion='" + txtoperacion.Text + "'";

                    DataTable dt_rechazos = DataAccess.Get_DataTable(query);

                    if (dt_rechazos.Rows.Count <= 0)
                    {
                        //  ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "bultomalo();", true);
                        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
                        string UserId = mu.ProviderUserKey.ToString();


                        DataTable dtporder = new DataTable();
                        dtporder = DataAccess.Get_DataTable("select p.POrder,b.Quantity from Bundle b join POrder p on b.Id_Order=p.Id_Order where b.Id_Bundle=" + Id_Bundle);
                        int muestra = 0;
                        var quantity = int.Parse(dtporder.Rows[0][1].ToString());
                        if (quantity < 25)
                        {
                            muestra = 5;
                        }
                        else if (quantity > 24)
                        {
                            muestra = 8;
                        }

                        OrderDetailDA.SaveIPApprovedSem(Id_Bundle, UserId, idlinea, Convert.ToInt16(idmodulo), muestra,hdnCodigooperario.Value.Trim(),txtoperario.Text.Trim(),txtoperacion.Text.Trim());
                        cont++;
                    }
                    else
                    {
                        resp = "false";
                    }
                }
                else
                {
                    resp = "false";
                }

            }

            Load_Data(idorder, idauditoria, idmodulo);

            if (resp == "true")
            {
                string message = "Exito! " + " Aprobados: " + cont;
                string tipo = "success";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
            }
            else
            {
                string message = "Aprobados: " + cont + ", No ha sido posible la aprobacion de algunos bultos!";
                string tipo = "info";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
            }

        }
        catch (Exception)
        {
            string message = "Ha Ocurrido un Error, Reintente nuevamente!";
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);


        }

    }

    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string idauditoria = (string)Request.QueryString["idAuditoria"];
            int idlinea = Convert.ToInt16(Request.QueryString["idLinea"]);
            string idmodulo = (string)Request.QueryString["idModulo"];
            Response.Redirect("SearchIL.aspx?idAuditoria=" + idauditoria + "&idLinea=0&idSeccion=" + idmodulo);
        }
        catch (Exception)
        {


        }
    }

    /*****   guardar rechazo semaforo *********/

    /// <summary>
    /// evento 4 guarda registro rechazado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSaveDefectos_Click(object sender, EventArgs e)
    {
        try
        {
            int sumadefValidos = 0;

            for (int i = 0; i <= grvDefectos.Rows.Count - 1; i++)
            {
                TextBox txtcantidad = (TextBox)grvDefectos.Rows[i].FindControl("txtcantidad");
                DropDownList drpddef = (DropDownList)grvDefectos.Rows[i].FindControl("drpDefectos");

                if (txtcantidad.Text != string.Empty && Convert.ToInt16(drpddef.SelectedValue) > 0)
                {
                    sumadefValidos++;
                }
            }  

            if (sumadefValidos==0)
            {
                string message = "Debe seleccionar defectos y agregar unidades";
                string tipo = "error";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
                return;
            }
            else
               guardaRechazo(grvDefectos, false);
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "')", true);
        }
    }

    /// <summary>
    /// evento 4.. funcion guarda rechazo
    /// </summary>
    /// <param name="DefectosEncontrados"></param>
    /// <param name="ayuda"></param>

    void guardaRechazo(GridView DefectosEncontrados, bool ayuda)
    {
        string guid = (string)Session["guid"];
        if (hdnguid.Value == guid)
        {

            string idorder = (string)Request.QueryString["idOrder"];
            string idbulto = (string)Request.QueryString["idBulto"];
            string idauditoria = (string)Request.QueryString["idAuditoria"];
            string idlinea = (string)(Request.QueryString["idLinea"]);
            string idmodulo = (string)Request.QueryString["idModulo"];

          //  int idoperario, idoperacion, idopeop;

            MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
            string userId = mu.ProviderUserKey.ToString();

            DataTable dt_Inspector = DataAccess.Get_DataTable("select i.id_inspector from  Inspector i  where i.UserId='" + userId + "'");
            int idinspector = Convert.ToInt16(dt_Inspector.Rows[0][0]);

            DataTable dtporder = new DataTable();
            dtporder = DataAccess.Get_DataTable("select b.Quantity from Bundle b  where b.Id_Bundle=" + idbulto);
            int muestra = 0;
            var quantity = int.Parse(dtporder.Rows[0][0].ToString());

            muestra = quantity > 24 ? 8 : 5;
            
            var resp = OrderDetailDA.SaveIPRejectedSem(Convert.ToInt32(idbulto), idinspector, Convert.ToInt16(idlinea), Convert.ToInt16(idmodulo), muestra, guid,hdnCodigooperario.Value.Trim(),txtoperario.Text,txtoperacion.Text);// hdnguid.Value);

            int numAudi = 1;

            if (chkReauditoria.Checked && resp > 0)
            {
                DataTable dtnumAudi = DataAccess.Get_DataTable("select isnull(MAX(dt.NumAuditoria),0) from tbInspecProcRechazadoDet dt where dt.idBBRechazado=" + resp);
                numAudi = Convert.ToInt16(dtnumAudi.Rows[0][0]) + 1;
            }

            int cont = 0;

            for (int i = 0; i <= DefectosEncontrados.Rows.Count - 1; i++)
            {
                TextBox txtcantidad = (TextBox)DefectosEncontrados.Rows[i].FindControl("txtcantidad");
                DropDownList drpddef = (DropDownList)DefectosEncontrados.Rows[i].FindControl("drpDefectos");

                if (txtcantidad.Text != string.Empty &&  Convert.ToInt16(drpddef.SelectedValue)>0)
                {
                   
                    int iddefecto = Convert.ToInt16(drpddef.SelectedValue);


                    if (resp > 0)
                    {
                        //var r = OrderDetailDA.SaveIPRejectedDetail(resp, iddefecto, Convert.ToInt16(0), idopeop, idoperario, idoperacion, Convert.ToInt16(txtcantidad.Text), numAudi);
                        var r = OrderDetailDA.SaveIPRejectedDetailSem(resp, iddefecto, Convert.ToInt16(txtcantidad.Text), numAudi);
                        if (r != "OK")
                        {
                            cont++;
                        }

                    }

                }
            }


            if (cont == 0)
            {
                cargagrid();
                Load_Data(idlinea, idmodulo, idbulto);
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "hideModal();", true);

                var _guid = Guid.NewGuid();
                Session["guid"] = _guid.ToString();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "Popup", "Erroalert()", true);
            }


        }
    }


    void carga()
    {
        DataTable dtDefecto = new DataTable();
        dtDefecto = DataAccess.Get_DataTable("select idDefecto, Defecto from tbDefectos where Estado = 1 order by Defecto asc");
    
        Session["defectos"] = dtDefecto;
      
    }

    void cargagrid()
    {
        List<listas> lista = new List<listas>();

        for (int i = 1; i <= 10; i++)
        {
            var obj = new listas();
            obj.id = i.ToString();
            lista.Add(obj);
        }

        grvDefectos.DataSource = lista;
        grvDefectos.DataBind();
    }

    protected void grvDefectos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddldef = (DropDownList)e.Row.FindControl("drpDefectos");

                var dt1 = (DataTable)Session["defectos"];
               
                ddldef.DataSource = dt1;
                ddldef.DataTextField = "Defecto";
                ddldef.DataValueField = "idDefecto";
                ddldef.DataBind();
                ddldef.Items.Insert(0, "");             

            }
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + ex.Message + "','" + tipo + "');", true);

        }
    }


}