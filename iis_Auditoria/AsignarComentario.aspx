﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AsignarComentario.aspx.cs" Inherits="AsignarComentario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "AsignarComentario.aspx/GetPorderComment",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    comentario: item.comentario,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);
                    $('#<%=txtcomentario.ClientID%>').val(ui.item.comentario);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });

    </script>

    
    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>

<%--    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-ui-1.12.1.custom/jquery-ui.js"></script>--%>

<%--    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" />--%>

 <%--   <link href="css/pagination.css" rel="stylesheet" />--%>
 <%--   <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="js/bootstrap.min.js"></script>--%>

    <script type="text/javascript">
        function correctoG() {
            swal({
                title: '!Exito!',

                type: 'success'
            });
        }

        function errorG() {
            swal({
                title: '!Error!',

                type: 'error'
            });
        }

        function errorUser() {
            swal({
                title: '!Seleccione Correctamente el Corte!',

                type: 'Info'
            });
        }

        function restrincion() {
            swal({
                title: '!Utilice un Usuario Autorizado!',
                type: 'Info'
            });
        }



        $(document).ready(function () {

            $('#<%=btnGuadar.ClientID%>').click(function (e) {
                e.preventDefault();
                      var divElem = document.getElementById('msg');

                      var obj = {
                          comentario: document.getElementById("<%= txtcomentario.ClientID%>").value,
                          idorder: document.getElementById("<%= hfidPorder.ClientID %>").value,
                          porder: document.getElementById("<%= txtPorder.ClientID %>").value,
                      };

                      if (obj.porder != "" && obj.idorder!= "0") {
                          var actionData = "{'idorder': '" + obj.idorder + "','porder': '" + obj.porder + "','comentario': '" + obj.comentario + "'}";

                          $.ajax({
                              url: "AsignarComentario.aspx/GuardarComentario",
                              type: "POST",
                              data: actionData,
                              dataType: "json",
                              contentType: "application/json; charset=utf-8",
                              success: function (data) {
                                  if (data.d == "true") {
                                      divElem.innerHTML = " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> <strong> Guardado Correctamente </strong>";
                                      divElem.className = "alert alert-success alert-dismissible ";
                                      divElem.style.display = "block";

                                      limpiartexbox();
                                  }
                                  else {
                                      divElem.innerHTML = " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> <strong>La ejecucion no se realizo con exito  </strong>";
                                      divElem.className = "alert alert-info alert-dismissible ";
                                      divElem.style.display = "block";
                                  }
                              },
                              error: function (XMLHttpRequest, textStatus, errorThrown) {
                                  divElem.innerHTML = " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> <strong>  Error de Servidor al Guardar </strong>";
                                  divElem.className = "alert alert-warning alert-dismissible ";
                                  divElem.style.display = "block";
                              }

                          })
                      }
                      else {
                          divElem.innerHTML = " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> <strong> Debe Llenar correctamente los campos </strong>";
                          divElem.className = "alert alert-info alert-dismissible ";
                          divElem.style.display = "block";
                      }
                  });

        });


        function limpiartexbox() {
            document.getElementById("<% =txtPorder.ClientID%>").value = "";
            document.getElementById("<%= txtcomentario.ClientID%>").value = "";
            document.getElementById("<%= hfidPorder.ClientID%>").value = "0";
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:10px;">
            <div class="panel-heading"><strong>Asignacion de Comenterio</strong></div>
            <div class="panel-body">
                <div class="col-lg-12">

                    <div class="col-lg-4 col-lg-offset-4">
                        <div class="col-lg-12">
                            Corte:
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                            Comentario:
                                     <div class="input-group">
                                         <asp:TextBox ID="txtcomentario" TextMode="MultiLine" Height="200" Width="300" CssClass="form-control" runat="server"></asp:TextBox>
                                         <asp:HiddenField ID="hfidPorder" Value="0" runat="server" />
                                     </div>

                        </div>
                        <div class="col-lg-12" style="padding-top: 10px">

                            <div id='msg' style="display: none" role="alert">
                            </div>

                            <asp:Button ID="btnGuadar" runat="server" Text="Guardar" CssClass="btn btn-primary form-control " />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

