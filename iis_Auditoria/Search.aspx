﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtpo.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "SearchIL.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    //idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtpo.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtpo.ClientID%>').val(ui.item.porder);                    
                    $('#<%=txtestilo.ClientID%>').val(ui.item.style);
                    

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });      
    </script>

    <script src="jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script type="text/javascript" src="/scripts/jquery.blockUI.js"></script>   
   
<style>
#ContentPlaceHolder2_txtestilo, #ContentPlaceHolder2_txtpo
{
text-transform:uppercase;    
}
</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentTitulo" Runat="Server">
    <h2><asp:Label ID="lbl" runat="server" Text="Production Order Search"></asp:Label></h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <script type="text/javascript">
    function BlockUI(elementID) {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $("#" + elementID).block({ message: '<table><tr><td>' + '<img src="/img/please_wait.gif"/></td></tr></table>',
                css: {},
                overlayCSS: { backgroundColor: '#FFFFFF', opacity: 0.6, border: '1px solid #000000' }
            });
        });
        prm.add_endRequest(function () {
            $("#" + elementID).unblock();
        });
    }
    $(document).ready(function () {
        BlockUI("search_panel");
        $.blockUI.defaults.css = {};
    });
</script>
        <asp:UpdatePanel runat="server" id="Panel">
            <ContentTemplate>
                <div id="search_panel" class="box_search">
                    <div class="row">
                    <div class="izq">
                       <div id="po">
                        <strong><asp:Label ID="lblpo" runat="server" Text="NUMERO DE CORTE: "></asp:Label></strong>
                        </div> 
                       <div id="po_txt">
                        <asp:TextBox ID="txtpo"  runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtpo"></asp:RequiredFieldValidator>
                        </div> 
                    </div>                                                                                                                                                                                    
                    <div class="cent">
                            <div id="estilo">
                            <strong><asp:Label ID="lblestilo" runat="server" Text="ESTILO #: "></asp:Label></strong>
                            </div>
                            <div id="estilo_txt">
                            <asp:TextBox ID="txtestilo" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="txtestilo"></asp:RequiredFieldValidator>
                            </div>
                     </div>
                    <div class="der">
                    <div id="boton">
                    <asp:Button ID="btnbuscar" runat="server" CssClass="sun-flower-button" 
                            Text="BUSCAR" onclick="btnbuscar_Click" />
                    </div>
                    </div>    
                    </div>
                    </div>
             </ContentTemplate>
         </asp:UpdatePanel><br />
<div style="margin:0 auto;width:100%;text-align:center;font-size:12px;color:Red;">
<asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
</div>
</asp:Content>

