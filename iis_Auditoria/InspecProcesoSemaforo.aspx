﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InspecProcesoSemaforo.aspx.cs" Inherits="InspecProcesoSemaforo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v16.2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="scripts/footable.min.js"></script>

    <script type="text/javascript">

        function alertSeleccionarMedida(mess, tipo) {
            swal({
                title: "",
                text: mess,
                type: tipo
            });
        }

        function successalert() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }


    </script>

    <script type="text/javascript">

        function warningalert() {
            swal({
                title: "¿Estas seguro?",
                text: "¿Deseas ingresar este registro?",
                type: "warning"
            })
        }

    </script>

    <script type="text/javascript">
        function pageLoad() { // this gets fired when the UpdatePanel.Update() completes
            ReBindMyStuff();
        }

    </script>

    <script lang="javascript" type="text/javascript">

        function busquedaOperario() {
            $(function () {
                $('#<%=txtoperario.ClientID%>').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "InspecProcesoSemaforo.aspx/GetOperario",
                            data: "{'pre' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        nombreOperario: item.nombreOperario,
                                        codOperario: item.codOperario,
                                        //   operacion: item.operacion,
                                        json: item
                                    }
                                }))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $('#<%=txtoperario.ClientID%>').val(ui.item.nombreOperario);
                        return false;
                    },
                    select: function (event, ui) {
                        $('#<%=txtoperario.ClientID%>').val(ui.item.nombreOperario);
                        $('#<%=hdnCodigooperario.ClientID%>').val(ui.item.codOperario);

                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li  >")
                        .append("<a class='text-info ' style='padding-left:30px;' >" + item.nombreOperario + "</a>").appendTo(ul);
                };
            });
        }



        function busquedaOperacion() {
            $(function () {
                $('#<%=txtoperacion.ClientID%>').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "InspecProcesoSemaforo.aspx/GetOperacion",
                            data: "{'pre' :'" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        operacion: item.operacion,
                                        json: item
                                    }
                                }))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus);
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $('#<%=txtoperacion.ClientID%>').val(ui.item.operacion);
                        return false;
                    },
                    select: function (event, ui) {
                        $('#<%=txtoperacion.ClientID%>').val(ui.item.operacion);
                       
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li  >")
                        .append("<a class='text-info ' style='padding-left:30px;' >" + item.operacion + "</a>").appendTo(ul);
                };
            });
        }
    </script>

      <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <div style="margin-left: 10px;">
        <%-- <label style="">From Production Order:</label>--%>
        <label id="frprododr">
            <asp:Label ID="lblorder1" Visible="false" runat="server" Text=""></asp:Label></label>
        <%-- <label>To </label>--%>
        <%-- <label id="lbtoprod">Production Order:</label>--%>
        <label id="toprododr">
            <asp:Label ID="lblorder2" runat="server" Visible="false" Text=""></asp:Label></label>
        <%-- <strong>All Dates</strong>--%>
        <br />

    </div>

    <div class="container-fluid">
        <div class="col-lg-12 centrar">
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/img/system_search.png" OnClick="ImageButton2_Click" /><asp:LinkButton ID="lnkNew" runat="server" OnClick="lnkNew_Click">New Search</asp:LinkButton>

        </div>
    </div>

    <div class="container " style="padding-bottom: 5px;">
        <div class="col-lg-2">
            <strong>Customer:</strong>
            <asp:Label ID="lbclient" runat="server" CssClass="main">
                <asp:Label ID="lblcustomer" runat="server" Text=""></asp:Label></asp:Label>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container-fluid info_ord">
        <div class="container " style="padding-bottom: 5px;">
            <div class="col-lg-2 col-lg-offset-1">
                <asp:Label ID="Label10" runat="server" Text="Corte#:"></asp:Label>
                <asp:TextBox ID="txtporder" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label28" runat="server" Text="Estilo"></asp:Label>
                <asp:TextBox ID="txtstyle" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label29" runat="server" Text="Descripcion"></asp:Label>
                <asp:TextBox ID="txtdescrip" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label30" runat="server" Text="Cantidad"></asp:Label>
                <asp:TextBox ID="txtcantidad" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-lg-2">
                <asp:Label ID="Label31" runat="server" Text="Bultos"></asp:Label>
                <asp:TextBox ID="txtbultos" Font-Size="Small" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
        </div>
    </div>

    <div class="container">

        <div id="panelBusquedaOperario">
            <div class="form-group">
                operario Sin codigo:
                          <asp:CheckBox ID="ckdSinCodigo" runat="server" />
            </div>
            <div class="form-group">
                Operario:
                          <div class="ui-widget" style="text-align: left;">
                              <asp:TextBox ID="txtoperario" placeholder="Codigo Order" onkeypress="busquedaOperario()" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                          </div>
                <asp:HiddenField ID="hdnCodigooperario" runat="server" />
            </div>
             <div class="form-group">
                Operacion:
                          <div class="ui-widget" style="text-align: left;">
                              <asp:TextBox ID="txtoperacion" placeholder="Codigo Order" onkeypress="busquedaOperacion()" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                          </div>
               
            </div>
        </div>


        <div id="Grid" runat="server">
            <asp:GridView ID="GridBultos" OnRowCommand="GridBultos_RowCommand" OnRowDataBound="GridBultos_RowDataBound" AutoGenerateColumns="false" DataKeyNames="Id_Bundle"
                CssClass="table table-hover table-striped" GridLines="None" Width="100%" runat="server">
                <Columns>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblidbundle" runat="server" Text='<%#Bind("Id_Bundle") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="NSeq" HeaderText="Seq" />
                    <asp:BoundField DataField="Bld" HeaderText="Bulto" />
                    <asp:BoundField DataField="Size" HeaderText="Talla" />
                    <asp:BoundField DataField="Color" HeaderText="Color" />
                    <asp:BoundField DataField="Quantity" HeaderText="Cantidad" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                <asp:ListItem Value="0" Text="Rechazado"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Aprobado"></asp:ListItem>
                            </asp:RadioButtonList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Button ID="btnGuardarTodos" OnClick="btnGuardarTodos_Click" CssClass="btn btn-primary" runat="server" Text="Aprobar Todos" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton CommandName="Editar" ID="comando" CssClass="btn btn-default" runat="server">
                                     <i class="fa fa-check-circle marApRe" aria-hidden="true"></i>  
                                             
                                     <i class="fa fa-times-circle" aria-hidden="true"></i>
                            </asp:LinkButton>
                            <asp:LinkButton CommandName="Reparar" ID="comandoRep" CssClass="btn btn-success" runat="server">
                                     <i class="glyphicon glyphicon-wrench" aria-hidden="true"></i> Reparar
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <h4 style="text-align: center">Sin datos para mostrar</h4>
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <asp:Label ID="lblnoRows" runat="server" Text=""></asp:Label>
        </div>
    </div>


      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Asignar Defecto a Opeario</h4>
                </div>
                <div class="modal-body">
                    <asp:HiddenField runat="server" ID="hdnguid" />
                      <div class="form-group">
                        
                          Reauditoria:
                          <asp:CheckBox ID="chkReauditoria" Text="Activo" Checked="false" CssClass="form-control" runat="server" />       
                    </div>
                    <div class="form-group">
                        Linea:
                        <asp:Label runat="server" ID="lbllinea" ></asp:Label>       
                    </div>
                    <div class="form-group">
                        Operario:
                          <asp:Label runat="server" ID="lbloperario" ></asp:Label>       
                    </div>
                    <div class="form-group">
                        Operacion:
                              <asp:Label runat="server" ID="lbloperacion" ></asp:Label>         
                    </div>

                
                    <br />
                    <div style="height: 500px; overflow-y: scroll">

                        <asp:GridView ID="grvDefectos" runat="server" CssClass="table table-hover table-striped" OnRowDataBound="grvDefectos_RowDataBound" GridLines="None" Width="100%" AutoGenerateColumns="False">
                            <Columns>                             
                                <asp:TemplateField HeaderText="Defectos" HeaderStyle-CssClass="header_gv">
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" ID="drpDefectos">
                                        </asp:DropDownList>                                    
                                    </ItemTemplate>
                                </asp:TemplateField>
                               
                                <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                    <ItemTemplate>
                                         <asp:TextBox ID="txtcantidad" CssClass="form-control" runat="server" Type="Number" Font-Size="Large"></asp:TextBox>
                                      
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" CssClass="paginador" Font-Bold="False"
                                Font-Size="16px" Width="50px" />
                        </asp:GridView>
                    </div>

                    <asp:Button ID="btnSaveDefectos" runat="server" OnClick="btnSaveDefectos_Click" Text="GUARDAR " CssClass="btn btn-default center-block" />

                </div>

                <style>
                    .ui-autocomplete {
                        z-index: 4000;
                    }
                </style>

            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <h2>
        <asp:Label ID="lbl" runat="server" ForeColor="White" Text="Bundles Check Off"></asp:Label></h2>
    <br />
</asp:Content>

