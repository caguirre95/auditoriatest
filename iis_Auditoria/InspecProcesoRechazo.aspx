﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InspecProcesoRechazo.aspx.cs" Inherits="InspecProcesoRechazo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function successalert() {
            swal({
                title: 'Registro Ingresado Correctamente!',
                type: 'success'
            });
        }

        function Erroalert() {
            swal({
                title: 'Error Intente nuevamente!',
                type: 'error'
            });
        }

        function alertSeleccionarMedida(mes, tipo) {
            swal({
                title: mes,
                type: tipo
            });
        }
    </script>

    <script type="text/javascript">
        function ShowWindow() {
            AsignarDefectos.Show();
        }
        function HideWindow() {
            AsignarDefectos.Hide();
        }

        function Check_Click(objRef) {
            //Get the Row based on checkbox
            var row = objRef.parentNode.parentNode.parentNode.parentNode;

            if (objRef.checked) {
                //If checked change color to Aqua
                row.style.backgroundColor = "#0090CB";
                row.style.color = "white";
            }
            else {
                //If not checked change back to original color
                if (row.rowIndex % 2 == 0) {
                    //Alternating Row Color
                    row.style.backgroundColor = "white";
                    row.style.color = "black";
                }
                else {
                    row.style.backgroundColor = "white";
                    row.style.color = "black";
                }
            }
        }

        function Check_Click2(objRef) {
            //Get the Row based on checkbox
            var row = objRef.parentNode.parentNode.parentNode;

            if (objRef.checked) {
                //If checked change color to Aqua
                row.style.backgroundColor = "#0090CB";
                row.style.color = "white";
            }
            else {
                //If not checked change back to original color
                if (row.rowIndex % 2 == 0) {
                    //Alternating Row Color
                    row.style.backgroundColor = "white";
                    row.style.color = "black";
                }
                else {
                    row.style.backgroundColor = "white";
                    row.style.color = "black";
                }
            }
        }

        function showModal() {
            $('#ModalAsignarDefectos').modal('show');
        }
        function hideModal() {
            $('#ModalAsignarDefectos').modal('hide');
        }
    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {
            var num = document.getElementById("<%= drplinea.ClientID %>").value;
            $('#<%=txtoperario.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "InspecProcesoRechazo.aspx/GetOperario",
                        data: "{'pre' :'" + request.term + "','idlinea':'" + num + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    operario: item.operario,
                                    idoperacionOpe: item.idoperacionOpe,
                                 //   operacion: item.operacion,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtoperario.ClientID%>').val(ui.item.operario);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtoperario.ClientID%>').val(ui.item.operario);
                    $('#<%=hdnidoperario2.ClientID%>').val(ui.item.idoperacionOpe);
                   
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.operario + "</a>").appendTo(ul);
            };
        });

    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtoperacion.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "InspecProcesoRechazo.aspx/getoperacion",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    Id_Area: item.Id_Area,
                                    Nombre: item.Nombre,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtoperacion.ClientID%>').val(ui.item.Nombre);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=hdnidoperacion2.ClientID%>').val(ui.item.Id_Area);
                    $('#<%=txtoperacion.ClientID%>').val(ui.item.Nombre);
                   
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.Nombre + "</a>").appendTo(ul);
            };
        });

    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtarea.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "InspecProcesoRechazo.aspx/GetArea",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    Id_Area: item.Id_Area,
                                    Nombre: item.Nombre,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtarea.ClientID%>').val(ui.item.Nombre);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=hdnIdArea.ClientID%>').val(ui.item.Id_Area);
                    $('#<%=txtarea.ClientID%>').val(ui.item.Nombre);
                    //var divElem = document.getElementById('divElem');
                    //divElem.style.display = "block";

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.Nombre + "</a>").appendTo(ul);
            };
        });

    </script>

    <script lang="javascript" type="text/javascript">
        $(function () {

            $('#<%=txtdefecto.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "InspecProcesoRechazo.aspx/getdefecto",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    Id_Defecto: item.Id_Defecto,
                                    Nombre: item.Nombre,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtdefecto.ClientID%>').val(ui.item.Nombre);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=hdnidDefecto.ClientID%>').val(ui.item.Id_Defecto);
                    $('#<%=txtdefecto.ClientID%>').val(ui.item.Nombre);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.Nombre + "</a>").appendTo(ul);
            };
        });

    </script>

    <script>
        $(document).ready(function () {

            $('#<%=btnguardaDefectoXarea.ClientID%>').click(function (e) {
                e.preventDefault();
                var idbulto = 0;
                var idseccion = 0;

                var divElem = document.getElementById('divElem');

                var query = window.location.search.substring(1);
                var vars = query.substr(query.indexOf("?") + 1).split("&");
               // var idbiho = $('#<%=lblbio.ClientID%>').html().substr(11);
                // alert(idbiho);
                for (var i = 0; i < vars.length; i++) {
                    var pair = vars[i].split("=");
                    if (pair[0] == "IdBundle") {
                        idbulto = pair[1];
                    }
                    if (pair[0] == "id_seccion") {
                        idseccion = pair[1];
                    }

                }

                var obj = {
                    cantidad: document.getElementById("<%= txtCantidadDefecto.ClientID%>").value,
                    idarea: document.getElementById("<%= hdnIdArea.ClientID %>").value,
                    iddefecto: document.getElementById("<%= hdnidDefecto.ClientID %>").value,
                    idbulto: idbulto,
                    idseccion: idseccion,
                    idbiho: $('#<%=lblbio.ClientID%>').html().substr(11),
                };

                // alert(obj.idbulto);
                if (obj.iddefecto != "" && isNaN(obj.iddefecto) == false && obj.cantidad != "" && isNaN(obj.cantidad) == false && obj.idarea != "" && isNaN(obj.idarea) == false)
                {
                    var actionData = "{'cantidad': '" + obj.cantidad + "','idarea': '" + obj.idarea + "','iddefecto': '" + obj.iddefecto + "','idbulto': '" + obj.idbulto + "','idseccion': '" + obj.idseccion + "','idbiho': '" + obj.idbiho + "'}";
                    //   alert("ddddd");
                    $.ajax({
                        url: "InspeccionLineaT.aspx/GuardarRechazo",
                        type: "POST",
                        data: actionData,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "true") {

                                divElem.innerHTML = " <strong> Guardado correctamente </strong>";
                                divElem.className = "alert alert-success";
                                divElem.style.display = "block";

                                limpiartexbox();
                            }
                            else {
                                divElem.innerHTML = " <strong> Error en insert </strong>";
                                divElem.className = "alert alert-warning";
                                divElem.style.display = "block";
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            // alert(status);
                            divElem.innerHTML = " <strong>'" + textStatus + "'</strong>";
                            divElem.className = "alert alert-danger";
                            divElem.style.display = "block";
                        }

                    })
                }
                else {
                    // alert("dlfk");
                    divElem.innerHTML = " <strong> Llenar campos correctamente </strong>";
                    divElem.className = "alert alert-info";
                    divElem.style.display = "block";
                }
            });

        });


        function limpiartexbox() {
            document.getElementById("<% =txtCantidadDefecto.ClientID%>").value = "";
            document.getElementById("<%= txtdefecto.ClientID%>").value = "";
            document.getElementById("<%= txtarea.ClientID%>").value = "";
            document.getElementById("<%= hdnidDefecto.ClientID%>").value = "";
            document.getElementById("<%= hdnIdArea.ClientID%>").value = "";

        }
    </script>

    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2>
        <asp:Label ID="lbl" runat="server" Text="Inspeccion de Proceso"></asp:Label></h2>
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="container-fluid">
        <div class="col-lg-12 centrar">
            <asp:ImageButton ID="ImageButton2" runat="server" CssClass="marB" ImageUrl="~/img/system_search.png" OnClick="lnkNew_Click" />
            <asp:LinkButton ID="lnkNew" runat="server" OnClick="lnkNew_Click">New Search</asp:LinkButton>

            <asp:ImageButton ID="ImageButton1" runat="server" CssClass="marB" ImageUrl="~/img/report.png" OnClick="LinkButton1_Click" />
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Hoja de Corte</asp:LinkButton>
        </div>
    </div>
    <div class="container-fluid info_ord">
        <div class="container" style="padding-bottom: 5px;">

            <div class="col-xs-4" style="padding-top: 5px;">
                <div class="col-xs-12 centrar">
                    <asp:Label ID="lblcustom" runat="server" Text="Customer:"></asp:Label>
                    <asp:Label ID="lblcustomerval" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-12 centrar">
                    <asp:Label ID="Label1" runat="server" Text="Style:"></asp:Label>
                    <asp:Label ID="lblstylesval" runat="server" Text=""></asp:Label>
                </div>
            </div>

            <div class="col-xs-4" style="padding-top: 5px;">
                <div class="col-xs-12 centrar">
                    <asp:Label ID="Fecha" runat="server" Text="Fecha:"></asp:Label>
                    <asp:Label ID="lblfechaval" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-12 centrar">
                    <asp:Label ID="Label3" runat="server" Text="Corte:"></asp:Label>
                    <asp:Label ID="lblcut" runat="server" Text=""></asp:Label>
                </div>
            </div>

           
        </div>
    </div>

    <br />

    <div class="container">

        <div class="col-md-8 col-md-offset-2">
            <div class="col-lg-4 col-lg-offset-4">
                <asp:Button ID="Button11" Text=" Reparar Bulto " OnClick="Button1_Click" CssClass="btn btn-success form-control marB" runat="server" />
                 <a class="btn btn-default form-control marB" data-toggle="modal" data-target="#myModal"><span class="icon-plus"></span> Operario De Otra Linea</a>
                <%-- <a class="btn btn-default form-control marB" data-toggle="modal" data-target="#myModalDef"><span class="icon-plus"></span>Asignar Defectos de Corte</a>--%>

                <style>
                    .marB {
                        margin-top: 5px;
                        margin-bottom: 5px;
                    }
                </style>
            </div>
        </div>

        <div class="col-lg-12 ">
            <h4 style="visibility: hidden">
                <asp:Label ID="lblbio" runat="server" Text="" CssClass="label label-primary"></asp:Label>
            </h4>
            <div class="col-lg-6">
                <div class="form-group">
                    Reauditoria
                    <asp:CheckBox ID="CheckBox1" Text="Activo" Checked="false" CssClass="form-control" runat="server" />
                </div>
            </div>
            <div class="col-lg-6">
                Cantidad de Muestra
                  <asp:TextBox ID="txtMuestreo" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
        </div>

        <div class="col-lg-12">
            <asp:GridView ID="grv_insp_linea" runat="server" ShowFooter="true" CssClass="table table-hover table-striped" GridLines="None" Width="100%"
                AutoGenerateColumns="False" OnRowDataBound="grv_insp_linea_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="No">
                        <ItemTemplate>
                            <asp:Label ID="lblno" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Codigo de Empleado">
                        <ItemTemplate>
                            <asp:Label ID="lblcodigo" runat="server" Text='<%# Eval("Codigo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre del Operario">
                        <ItemTemplate>
                            <asp:Label ID="lbloperario" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Operacion">
                        <ItemTemplate>
                            <asp:Label ID="lbloperacion" runat="server" Text='<%# Eval("Operacion") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="idoperacionoperario" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lbloperacionOperario" runat="server" Text='<%# Eval("idoperarioOpe") %>'></asp:Label>
                             <asp:Label ID="lblidoperacion" runat="server" Text='<%# Eval("id_operacion") %>'></asp:Label>
                             <asp:Label ID="lblidoperario" runat="server" Text='<%# Eval("id_operario") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Defectos">
                        <ItemTemplate>
                            <asp:TextBox ID="txtdefectos" runat="server" Enabled="false" Font-Size="Medium" CssClass="form-control" MaxLength="3" Text='<%# Eval("CantidadDefectos") %>'></asp:TextBox>
                            <asp:Label runat="server" ID="lblYourLabel" ForeColor="red" Font-Size="Small" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <div>
                                <asp:Label ID="lblTtdef" Text="TTDEF:" runat="server" />
                                <asp:Label ID="lblTotaldefect" runat="server" />
                            </div>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>

                            <asp:Button ID="ASPxButton8" runat="server" Text="Rechazar" OnClick="ASPxButton8_Click" CssClass="btn btn-danger" />

                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        </div>


    </div>

    <asp:HiddenField runat="server" ID="hdnCodigo" />
    <asp:HiddenField runat="server" ID="hdnOperacion" />
    <asp:HiddenField runat="server" ID="hdnidoperacionope" />
    <asp:HiddenField runat="server" ID="hdnguid" />
    <asp:HiddenField runat="server" ID="hdnidoperacion" />
    <asp:HiddenField runat="server" ID="hdnidoperario" />
    <br />


    <div class="modal fade" id="ModalAsignarDefectos" tabindex="-1" role="dialog" aria-labelledby="ModalAsignarDefectosLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalAsignarDefectosLabel">Asignar Defecto </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:Label ID="Label20" runat="server" Text="Operario: " Style="font-size: 18px; font-weight: bold;"></asp:Label>
                        <asp:Label ID="lbloperario" runat="server" Style="display: inline-block; font-size: 18px; color: Red;"></asp:Label>
                    </div>
                    <div class="form-group">
                        <div style="height: 450px; overflow-y: scroll; margin-top: 5px; margin-bottom: 5px">
                            <asp:GridView ID="DefectosEncontradosC" runat="server" CssClass="table table-hover table-striped" GridLines="None" AutoGenerateColumns="false" Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <center>
                                   <asp:CheckBox ID="chkdefectos" Width="30px" runat="server" onclick="Check_Click(this)" />
                                   </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Defectos" HeaderStyle-CssClass="header_gv">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelDefect" runat="server" Text='<%# Eval("Defecto") %>' Font-Size="Large"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtcantidad" CssClass="form-control" runat="server" Type="Number" Font-Size="Large"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="labelIddefecto" runat="server" Text='<%# Eval("idDefecto") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnsave" runat="server" Text=" GUARDAR " CssClass="btn btn-default" Width="200" OnClick="btnsave_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Asignar Defecto a Opeario Ayuda</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        Linea:
                                <asp:DropDownList ID="drplinea" CssClass="form-control" runat="server"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        Operario:
                          <div class="ui-widget" style="text-align: left;">
                              <asp:TextBox ID="txtoperario" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                          </div>
                    </div>
                    <div class="form-group">
                        Operacion:
                                 <asp:TextBox ID="txtoperacion" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                    <asp:HiddenField ID="hdnidoperario2" runat="server" />
                    <asp:HiddenField ID="hdnidoperacion2" runat="server" />
                    <br />
                    <div style="height: 500px; overflow-y: scroll">

                        <asp:GridView ID="GridViewdefectosC" runat="server" CssClass="table table-hover table-striped" GridLines="None" Width="100%" AutoGenerateColumns="False">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkdefectos" Width="30px" runat="server" onclick="Check_Click2(this)" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Defectos" HeaderStyle-CssClass="header_gv">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelDefect" runat="server" Text='<%# Eval("Defecto") %>' Font-Size="Large"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Seccion" ItemStyle-HorizontalAlign="Center" Visible="false" HeaderStyle-CssClass="header_gv">
                                    <ItemTemplate>
                                        <asp:Label ID="labelIddefecto" runat="server" Text='<%# Eval("idDefecto") %>' Font-Size="Large"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                    <ItemTemplate>
                                         <asp:TextBox ID="txtcantidad" CssClass="form-control" runat="server" Type="Number" Font-Size="Large"></asp:TextBox>
                                      
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" CssClass="paginador" Font-Bold="False"
                                Font-Size="16px" Width="50px" />
                        </asp:GridView>
                    </div>

                    <asp:Button ID="btnSaveAyuda" runat="server" OnClick="btnSaveAyuda_Click" Text="SAVE " CssClass="btn btn-default center-block" />

                </div>
                <style>
                    .ui-autocomplete {
                        z-index: 4000;
                    }
                </style>

            </div>
        </div>
    </div>

    <div class="modal fade" id="Defectosdecorte" tabindex="-1" role="dialog" aria-labelledby="myModalLabeldef">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabeldef">Asignar Defecto de Area</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        Area:
                          <div class="ui-widget" style="text-align: left;">
                              <asp:HiddenField ID="hdnIdArea" runat="server" />
                              <asp:TextBox ID="txtarea" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                          </div>
                    </div>
                    <div class="form-group">
                        Defecto:
                         <asp:HiddenField ID="hdnidDefecto" runat="server" />
                        <div class="ui-widget" style="text-align: left;">
                            <asp:TextBox ID="txtdefecto" placeholder="Defecto" CssClass="form-control textboxAuto text-info" Font-Size="14px" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        Cantidad:
                                 <asp:TextBox ID="txtCantidadDefecto" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                    <br />

                    <asp:Button ID="btnguardaDefectoXarea" runat="server" Text="SAVE " CssClass="btn btn-default center-block" />

                    <div role="alert" style="display: none" id="divElem"></div>

                </div>
                <style>
                    .ui-autocomplete {
                        z-index: 4000;
                    }
                </style>

            </div>
        </div>
    </div>

</asp:Content>

