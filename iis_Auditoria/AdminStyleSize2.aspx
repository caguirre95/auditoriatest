﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdminStyleSize2.aspx.cs" Inherits="AdminStyleSize2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtestilo.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "AdministrarEstilosYTallas.aspx/GetEstilo",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    id: item.id,
                                    estilo: item.estilo,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtestilo.ClientID%>').val(ui.item.estilo);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtestilo.ClientID%>').val(ui.item.estilo);
                    $('#<%=hdnidestilo.ClientID%>').val(ui.item.id);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.estilo + "</a>").appendTo(ul);
            };
        });
    </script>

    <%--<script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txttallas.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "AdminStyleSize2.aspx/GetTalla",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    id: item.id,
                                    talla: item.talla,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txttallas.ClientID%>').val(ui.item.talla);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txttallas.ClientID%>').val(ui.item.talla);
                    $('#<%=hdftallas.ClientID%>').val(ui.item.id);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.talla + "</a>").appendTo(ul);
            };
        });
    </script>--%>

    <%--<script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtpm.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "AdminStyleSize2.aspx/GetPuntoMedida",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    idp: item.idp,
                                    nombre: item.nombre,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtpm.ClientID%>').val(ui.item.nombre);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtpm.ClientID%>').val(ui.item.nombre);
                    $('#<%=hdfpm.ClientID%>').val(ui.item.idp);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.nombre + "</a>").appendTo(ul);
            };
        });
    </script>--%>

    <script>      

        function limpiar() {
            $('#<%=txtestilo.ClientID%>').val('');
            $('#<%=hdnidestilo.ClientID%>').val('');
            $('#<%=drptallas.ClientID%>').empty();
        }

        function cerrarmodal() {
            $('#modaltallas').modal('hide')
        }

        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 44) {
                if (unicode < 48 || unicode > 57) //if not a number
                { return false } //disable key press    
            }
        }

        function abrirmod(e) {    
            console.log(e);
            var texto = document.getElementById(e.id);
            texto.innerHTML = 1;
            $(e.id).val('Hola');
            $('#ModalPrimero').modal('show')
        }

        function abrirmod2(e) {
            console.log(e);
            $('#ModalSegundo').modal('show')
        }        

    </script>

    <script type="text/javascript">
        function exito() {
            swal({
                title: 'Informacion!',
                text: 'Exito al ingresar los datos',
                type: 'success'
            });
        }

        function alertSeleccionarMedida(_title, _type) {
            swal({
                title: _title,
                type: _type,
                timer: 1000,
                showConfirmButton: false

            });
        }
    </script>



    <style>
        .mb {
            margin-top: 20px;
        }

        .mar_t {
            margin: 10px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }
    </style>

    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>
    <link href="StyleSheetAuto.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <br />
        <div class="panel panel-info">
            <div class="panel-heading">Spec</div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        Style
                        <div class="input-group">
                            <span class="ui-widget" style="text-align: left;">
                                <asp:TextBox ID="txtestilo" placeholder="Style" onfocus="limpiar();" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="hdnidestilo" />
                            </span>
                            <div class="input-group-btn">
                                <asp:LinkButton Text="" OnClick="LinkButton1_Click" CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                            </div>
                        </div>
                        <asp:Button ID="btnspc" runat="server" CssClass="btn btn-primary form-control mb" Text="Ver Spec" OnClick="btnspc_Click" />
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        Tallas  
                        <asp:DropDownList ID="drptallas" runat="server" CssClass="form-control"></asp:DropDownList>
                        <asp:Button ID="btnspec" runat="server" CssClass="btn btn-success form-control mb" Text="Limpiar" OnClick="btnspec_Click" />
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="margin-top: 18px">
                        <button type="button" class="btn btn-info form-control" data-toggle="modal" data-target=".bs-example-modal-sm">Asignar Puntos M.</button>

                        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="modaltallas">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" style="margin: 10px 10px 5px 10px">
                                                <asp:Button CssClass="btn btn-primary" Text="Actualizar Vista" ID="btnupdpm" OnClick="btnupdpm_Click" runat="server" />
                                            </div>
                                            <div class="form-group" style="height: 450px; overflow-y: scroll; margin: 10px">
                                                <asp:GridView ID="GridViewtallas" AutoGenerateColumns="false" DataKeyNames="idPuntosM" CssClass="table table-hover table-striped" GridLines="None" Width="100%" runat="server">
                                                    <Columns>
                                                        <asp:TemplateField Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblidpm" runat="server" Text='<%#Bind("idPuntosM") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="NombreMedida" HeaderText="Punto de Medida" ItemStyle-Width="400px" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:CheckBox Text="Seleccionado" ID="chkSeleccionado" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        <h4 style="text-align: center">No se encuentran Puntos de Medida</h4>
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </div>
                                            <div class="form-group" style="margin: auto 10px 5px 10px">
                                                <asp:Button CssClass="btn btn-primary" Text="Guardar Datos" ID="Btnasigpm" OnClick="Btnasigpm_Click" runat="server" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="margin-top: 18px">
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <hr />
                        <asp:GridView ID="grdsp" runat="server" AutoGenerateColumns="false" DataKeyNames="idEspecificacion" CssClass="table table-hover table-striped" GridLines="None" Width="100%">
                            <Columns>
                                <asp:TemplateField Visible="false">
                                    <HeaderTemplate>
                                        id 
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblidesp" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("idEspecificacion") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                    <HeaderTemplate>
                                        Estilo 
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblestilo" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("Style") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                    <HeaderTemplate>
                                        Talla 
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbltalla" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("Talla") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                    <HeaderTemplate>
                                        Punto Medida 
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblpm" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("NombreMedida") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Valor">
                                    <ItemTemplate>
                                        <%-- <div class="input-group">
                                            <span class="ui-widget" style="text-align: left;">--%>
                                        <asp:TextBox ID="txtvalor" runat="server" CssClass="form-control" Font-Size="Medium" onkeypress="return num(event);" Text='<%#Bind("Valor") %>'></asp:TextBox>
                                        <%--</span>
                                            <div class="input-group-btn">--%>
                                        <%--<asp:Button ID="btn1" runat="server" CssClass="btn btn-primary" Text="+" />
                                            </div>
                                        </div>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tolerancia Maxima">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txttolmax" runat="server" CssClass="form-control" Font-Size="Medium" Text='<%#Bind("TolMax") %>'></asp:TextBox>
                                       <%-- onfocus="abrirmod(this)"--%>
                                        <%--onkeypress="return num(event);"--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tolerancia Minima">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txttolmin" runat="server" CssClass="form-control" Font-Size="Medium" Text='<%#Bind("Tolmin") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <br />
                        <asp:Button ID="btnespecif" runat="server" Text="Agregar Especificación" CssClass="btn btn-info form-control" OnClick="btnespecif_Click" />
                    </div>

                </div>

                <%--<br />
                    Punto de Medida
                    <asp:TextBox ID="txtpm" runat="server" CssClass="form-control" placeholder="Punto de Medida" onfocus="limpiar2();"></asp:TextBox>
                    <asp:HiddenField ID="hdfpm" runat="server" />--%>
                <%--<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        Punto de Medida
                        <asp:TextBox ID="txtpm" runat="server" CssClass="form-control" placeholder="Punto de Medida" onfocus="limpiar2();"></asp:TextBox>
                        <asp:HiddenField ID="hdfpm" runat="server" />
                    </div>--%>
            </div>
        </div>
    </div>

    <aside class="modal fade fullscreen-modal" id="ModalPrimero">
        <div class="modal-dialog">
            <div class="modal-content">

                <article class="modal-header" style="padding-bottom: 3px">
                    <button id="cerrar" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                </article>

                <article class="modal-body" style="padding-top: 5px">

                    <%-- <div class="panel panel-body" style="padding-left: 0; padding-right: 0">--%>
                    <div class="row">

                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">

                            <label id="txtvalor" class="classLabel">0</label>

                            <%--<input type="text" id="txtvalor" class="form-control" />--%>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <input type="button" value='Aceptar' onclick="calculo()" class="btn btn-info withB" />
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0 !important; padding: 0 !important">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-21" style="margin: 0 !important; padding: 0 !important">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="button" value='1' name='1' onclick="changeText(this.name)" class="btn btn-primary withB" />
                                    <input type="button" value='1/2' name='1/2' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='3/8' name='3/8' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='3/16' name='3/16' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='11/16' name='11/16' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='1/4' name='1/4' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='5/8' name='5/8' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='5/16' name='5/16' onclick="changeText(this.name)" class="btn btn-success withB" />                                    
                                </div>                               
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="button" value='13/16' name='13/16' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='3/4' name='3/4' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='7/8' name='7/8' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='7/16' name='7/16' onclick="changeText(this.name)" class="btn btn-success  withB" />
                                    <input type="button" value='15/16' name='15/16' onclick="changeText(this.name)" class="btn btn-success  withB" />
                                    <input type="button" value='1/8' name='1/8' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='1/16' name='1/16' onclick="changeText(this.name)" class="btn btn-success withB" />
                                    <input type="button" value='9/16' name='9/16' onclick="changeText(this.name)" class="btn btn-success withB" />
                                </div>                               

                            </div>  

                        </div>
                    </div>
                </article>
            </div>
        </div>
    </aside>

    <aside class="modal fade fullscreen-modal" id="ModalSegundo">
        <div class="modal-dialog">
            <div class="modal-content">

                <article class="modal-header" style="padding-bottom: 3px">
                    <button id="cerrar1" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>
                </article>

                <article class="modal-body" style="padding-top: 5px">

                     <%-- <div class="panel panel-body" style="padding-left: 0; padding-right: 0">--%>
                    <div class="row">

                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">

                            <label id="txtvalor1" class="classLabel">0</label>

                            <%--<input type="text" id="txtvalor" class="form-control" />--%>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <input type="button" value='Aceptar' onclick="calculo()" class="btn btn-info withB form-control" />
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0 !important; padding: 0 !important">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-21" style="margin: 0 !important; padding: 0 !important">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="button" value='1' name='1' onclick="changeText(this.name)" class="btn btn-primary withB" />
                                    <input type="button" value='1/2' name='1/2' onclick="changeText(this.name)" class="btn btn-warning withB" />
                                    <input type="button" value='3/8' name='3/8' onclick="changeText(this.name)" class="btn btn-warning withB" />
                                    <input type="button" value='3/16' name='3/16' onclick="changeText(this.name)" class="btn btn-warning withB" />
                                    <input type="button" value='11/16' name='11/16' onclick="changeText(this.name)" class="btn btn-warning withB" />
                                    <input type="button" value='1/4' name='1/4' onclick="changeText(this.name)" class="btn btn-warning withB" />
                                    <input type="button" value='5/8' name='5/8' onclick="changeText(this.name)" class="btn btn-warning withB" />
                                    <input type="button" value='5/16' name='5/16' onclick="changeText(this.name)" class="btn btn-warning withB" />                                    
                                </div>                               
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="button" value='13/16' name='13/16' onclick="changeText(this.name)" class="btn btn-warning withB" />
                                    <input type="button" value='3/4' name='3/4' onclick="changeText(this.name)" class="btn btn-warning withB" />
                                    <input type="button" value='7/8' name='7/8' onclick="changeText(this.name)" class="btn btn-warning withB" />
                                    <input type="button" value='7/16' name='7/16' onclick="changeText(this.name)" class="btn btn-warning  withB" />
                                    <input type="button" value='15/16' name='15/16' onclick="changeText(this.name)" class="btn btn-warning  withB" />
                                    <input type="button" value='1/8' name='1/8' onclick="changeText(this.name)" class="btn btn-warning withB" />
                                    <input type="button" value='1/16' name='1/16' onclick="changeText(this.name)" class="btn btn-warning withB" />
                                    <input type="button" value='9/16' name='9/16' onclick="changeText(this.name)" class="btn btn-warning withB" />
                                </div>                               

                            </div>

                        </div>
                    </div>
                </article>
            </div>
        </div>
    </aside>

</asp:Content>

