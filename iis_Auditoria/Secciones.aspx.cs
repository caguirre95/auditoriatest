﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using SistemaAuditores.DataAccess;
using System.Web.Security;
using System.Web.UI.HtmlControls;

public partial class SelectAudit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string idauditoria = (string)Request.QueryString["idAuditoria"];
            string idinspector = (string)Request.QueryString["idInspector"];
           
            HiddenFieldidAuditoria.Value = idauditoria;

            acceso(idinspector);
        }
    }

    void acceso(string idinspector)
    {
        tr.Visible = false;
        de.Visible = false;
        e1.Visible = false;
        e2.Visible = false;
        en.Visible = false;
        fe.Visible = false;
        pf.Visible = false;
        pfr.Visible = false;
        pre.Visible = false;

        DataTable dt_Inspector = DataAccess.Get_DataTable("select s.id_seccion,s.id_linea,md.Id_Modulo from Inspector I"
                                                        + " join seccionInspector SI on I.id_inspector = SI.idInspector"
                                                        + " join Seccion S on SI.idseccion = S.id_seccion"
                                                        + " join Modulo Md on S.idModulo = Md.id_modulo"
                                                        + " where I.id_inspector = " + idinspector);

        if (dt_Inspector.Rows.Count == 1)
        {
            int idseccion = Convert.ToInt16(dt_Inspector.Rows[0][0]);//mod2020
            HiddenFieldidSeccion.Value = idseccion.ToString();

            Response.Redirect("SearchIL.aspx?idAuditoria=" + HiddenFieldidAuditoria.Value + "&idLinea=0&idSeccion=" + idseccion);

        }
        else if (dt_Inspector.Rows.Count > 1)
        {

            for (int i = 0; i < dt_Inspector.Rows.Count; i++)
            {
                int idaudi = int.Parse(dt_Inspector.Rows[i][2].ToString());

                switch (idaudi)
                {
                    case 1:
                        tr.Visible = true;
                        break;

                    case 2:
                        de.Visible = true;
                        break;

                    case 3:
                        e1.Visible = true;
                        break;

                    case 4:
                        e2.Visible = true;
                        break;
                    case 5:
                        pre.Visible = true;
                        break;
                    case 6:
                        en.Visible = true;
                        break;

                    case 11:
                        fe.Visible = true;
                        break;

                    case 12:
                        pf.Visible = true;
                        break;

                    case 13:
                        pfr.Visible = true;
                        break;

                    default:
                        break;
                }

            }

        }
        else
        {
            //MESS.Visible = true;
            //string message = "alert('Acceso Denegado!');";
            //ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        }

    }
   
    void redSerch(string parm)
    {
       // string inspectorUN = Page.User.Identity.Name;
      //  DataTable dt_UserName = DataAccess.Get_DataTable("select UserId from aspnet_Users where UserName='" + inspectorUN + "'");

       // if (dt_UserName.Rows.Count > 0)
       // {
            string idinspector = (string)Request.QueryString["idInspector"];
           // string UserId = Convert.ToString(dt_UserName.Rows[0][0]);

            DataTable dt_Inspector = DataAccess.Get_DataTable(" select s.id_seccion,s.id_linea,m.Id_Modulo from Inspector I"
                                                             + " join seccionInspector secc on i.id_inspector=secc.idInspector"
                                                             + " join Seccion s on secc.idseccion=s.id_seccion"
                                                             + " join Modulo m on s.idModulo=m.id_modulo"
                                                             + " where I.id_inspector='" + idinspector + "' and M.Modulo = '" + parm + "'");

            if (dt_Inspector.Rows.Count > 0)
            {
                int idseccion = Convert.ToInt16(dt_Inspector.Rows[0][0]);
           
                Response.Redirect("SearchIL.aspx?idAuditoria=" + HiddenFieldidAuditoria.Value + "&idLinea=0&idSeccion=" + idseccion);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "result();", true);
            }

        //}
    }

    #region NO usadas



    protected void imgPre_Click(object sender, ImageClickEventArgs e)
    {
        string inspectorUN = Page.User.Identity.Name;
        DataTable dt_UserName = DataAccess.Get_DataTable("select UserId from aspnet_Users where UserName='" + inspectorUN + "'");
        string UserId = Convert.ToString(dt_UserName.Rows[0][0]);
        DataTable dt_Inspector = DataAccess.Get_DataTable("select I.id_seccion, I.id_linea from Inspector I inner join Seccion S on S.id_seccion=I.id_seccion"
                          + " inner join Modulo M on S.id_seccion = M.id_seccion "
                          + " where I.UserId='" + UserId + "'"
                          + " and M.Modulo='Preliminares'");
        if (dt_Inspector.Rows.Count > 0)
        {
            int id_seccion = Convert.ToInt16(dt_Inspector.Rows[0][0]);
            int id_linea = Convert.ToInt16(dt_Inspector.Rows[0][1]);
            Response.Redirect("SearchIL.aspx?idsec=" + id_seccion + "&id_linea=" + id_linea);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "result();", true);
        }
    }

    protected void lnkPre_Click(object sender, EventArgs e)
    {
        string inspectorUN = Page.User.Identity.Name;
        DataTable dt_UserName = DataAccess.Get_DataTable("select UserId from aspnet_Users where UserName='" + inspectorUN + "'");
        string UserId = Convert.ToString(dt_UserName.Rows[0][0]);
        DataTable dt_Inspector = DataAccess.Get_DataTable("select I.id_seccion, I.id_linea from Inspector I inner join Seccion S on S.id_seccion=I.id_seccion"
                          + " inner join Modulo M on S.id_seccion = M.id_seccion "
                          + " where I.UserId='" + UserId + "'"
                          + " and M.Modulo='Preliminares'");
        if (dt_Inspector.Rows.Count > 0)
        {
            int id_seccion = Convert.ToInt16(dt_Inspector.Rows[0][0]);
            int id_linea = Convert.ToInt16(dt_Inspector.Rows[0][1]);
            Response.Redirect("SearchIL.aspx?idsec=" + id_seccion + "&id_linea=" + id_linea);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "result();", true);
        }
    }
    #endregion

    protected void lnksec1_Click(object sender, EventArgs e)
    {
        redSerch("Trasero");
    }

    protected void lnksec2_Click(object sender, EventArgs e)
    {
        redSerch("Delantero");
    }

    protected void lnksec3_Click(object sender, EventArgs e)
    {
        redSerch("Ensamble I");
    }

    protected void LNKSEC4_Click(object sender, EventArgs e)
    {
        redSerch("Ensamble II");
    }

    protected void LNKSEC5_Click(object sender, EventArgs e)
    {
        redSerch("Ensamble");
    }

    protected void LNKSEC6_Click(object sender, EventArgs e)
    {
        redSerch("Frente y Espalda");
    }

    protected void LNKSEC7_Click(object sender, EventArgs e)
    {
        redSerch("Preliminar Frente");
    }

    protected void LNKSEC8_Click(object sender, EventArgs e)
    {
        redSerch("Preliminar Forro");
    }

    protected void LNKSEC9_Click(object sender, EventArgs e)
    {
        redSerch("Preliminares");
    }

    //



}