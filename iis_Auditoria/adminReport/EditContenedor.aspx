﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EditContenedor.aspx.cs" Inherits="adminReport_EditContenedor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <script lang="javascript" type="text/javascript">

        $(function () {
            $('#<%=txtcut.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcut.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcut.ClientID%>').val(ui.item.porder);
                    $('#<%=hdfcut.ClientID%>').val(ui.item.idorder);
                   <%-- $('#<%=txtStyle.ClientID%>').val(ui.item.idorder);--%>
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        })
    </script>

    <script lang="javascript" type="text/javascript">

        $(function () {
            $('#<%=txtcontenedor.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "EditContenedor.aspx/GetContenedor",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    Contenedor: item.Contenedor,
                                    Id_contenedor: item.Id_contenedor,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcontenedor.ClientID%>').val(ui.item.Contenedor);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcontenedor.ClientID%>').val(ui.item.Contenedor);
                    $('#<%=hdfcontenedor.ClientID%>').val(ui.item.Id_contenedor);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.Contenedor + "</a>").appendTo(ul);
            };
        })
    </script>

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function Eliminar() {
            swal({
                title: 'Exito!',
                text: 'El corte ha sido eliminado del contenedor.',
                type: 'success'
            });
        }
        function Actualizar() {
            swal({
                title: 'Exito!',
                text: 'Corte agregado al Contenedor.',
                type: 'success'
            });
        }
        function cerrarCon(cont) {
            swal({
                title: 'Información!',
                text: 'El Contenedor ' + cont + ' ha sido Cerrardo.',
                type: 'success'
            });
        }
        function AbrirCon(cont) {
            swal({
                title: 'Información!',
                text: 'El Contenedor ' + cont + ' ha sido Abierto.',
                type: 'info'
            });
        }
        function cerrarConVerf() {
            swal({
                title: 'Info!',
                text: 'Contenedor Cerrardo, no puede agregar mas cortes.',
                type: 'warning'
            });
        }
    </script>

    <script type="text/javascript">
        function verificarCut(corte, contenedor) {
            swal({
                title: '!Información!',
                text: 'El Corte : ' + corte + ' ,esta Asignado al Contenedor : ' + contenedor,
                type: 'success'
            });
        }
    </script>

    <style>
        .sp {
            margin-top: 20px;
        }

        .es {
            background: #F77E05;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading"><strong>Editar Contenedor</strong></div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <strong>Contenedor</strong>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <asp:LinkButton Text="" OnClick="LinkButton1_Click" CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                            </span>
                            <div class="ui-widget" style="text-align: left;">
                                <asp:TextBox ID="txtcontenedor" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Contenedor"></asp:TextBox>
                                <asp:HiddenField ID="hdfcontenedor" runat="server" />
                            </div>
                        </div>
                        <asp:Button ID="btnmodifi" runat="server" CssClass="btn btn-info form-control sp" Text="Modificar Contenedor" OnClick="btnmodifi_Click" />

                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <strong>Corte</strong>
                        <asp:TextBox ID="txtcut" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Corte"></asp:TextBox>
                        <asp:HiddenField ID="hdfcut" runat="server" />
                        <%--<asp:TextBox ID="txtStyle" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Estilo" Enabled="false"></asp:TextBox>--%>
                        <asp:Button ID="btncerrar" runat="server" CssClass="btn btn-danger form-control sp es" Text="Cerrar / Abrir - Contenedor" OnClick="btncerrar_Click" />
                    </div>

                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 sp">
                        <asp:Button ID="btnagregar" runat="server" CssClass="btn btn-info form-control" Text="Agregar Corte" OnClick="btnagregar_Click" />
                    </div>

                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 sp">
                        <asp:Button ID="btnlimp" runat="server" CssClass="btn btn-success form-control" Text="Limpiar" OnClick="btnlimp_Click" />
                    </div>

                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 sp">
                        <asp:Button ID="btnregresar" runat="server" CssClass="btn btn-warning form-control" Text="Regresar" OnClick="btnregresar_Click" />
                    </div>

                    <div class="col-lg-6 sp">
                        <div class="panel panel-default">
                            <div class="panel-heading">Comandos Eliminar</div>
                            <div class="panel-body">
                                <div class="col-lg-6">
                                    <asp:Button ID="btn1elim" runat="server" CssClass="btn btn-danger form-control" Text="Eliminar Todos los Cortes" OnClick="btn1elim_Click" />
                                </div>
                                <div class="col-lg-6">
                                    <asp:Button ID="btnelimCheck" runat="server" CssClass="btn btn-danger form-control" Text="Eliminar Cortes Check" OnClick="btnelimCheck_Click" />
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sp" style="padding: 0">
                        <hr />
                        <div class="badge marcontrol-bottom">
                            <div class="input-group">
                                Cortes Agregados 
                            <asp:Label ID="lblcnt" runat="server" Text="" CssClass="badge"></asp:Label>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sp">
                        <asp:GridView ID="grdcortes" runat="server" CssClass="table table-hover table-striped" HeaderStyle-BackColor="#337ab7"
                            HeaderStyle-ForeColor="White" Width="100%" GridLines="None" DataKeyNames="Id_Order" AutoGenerateColumns="False" OnRowCommand="grdcortes_RowCommand">
                            <Columns>
                                <asp:BoundField ControlStyle-CssClass="label label-success" DataField="Id_Order" HeaderText="Id"></asp:BoundField>
                                <asp:BoundField ControlStyle-CssClass="label label-success" DataField="POrder" HeaderText="Corte"></asp:BoundField>
                                <asp:TemplateField>
                                    <HeaderTemplate>Check Cortes a Eliminar</HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chk1" runat="server" Text="Eliminar" Checked="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton3" CssClass="btn btn-danger" CommandName="Eliminar" runat="server" ToolTip="Eliminar Registro" OnClientClick="javascript:return confirm('Esta seguro que desea eliminar el Corte de Contenedor?');"><span class="glyphicon glyphicon-remove"></span></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                            </Columns>
                        </asp:GridView>
                    </div>

                </div>
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
