﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_ReporteIntexInv : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
            Session["dtinv"] = null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select top 15 p.POrderClient, MIN(s.Style) Estilo, SUM(p.Quantity) Totalcorte from POrder p join dbo.Style s on s.Id_Style = p.Id_Style"
                                          + " where s.washed = 1 and p.POrderClient like '%" + pre + "%' group by p.POrderClient order by LEN(p.POrderClient)", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porderclient = dt.Rows[i][0].ToString();
                obj.estilo = dt.Rows[i][1].ToString();
                obj.totalcorte = int.Parse(dt.Rows[i][2].ToString());

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porderclient { get; set; }
        public string estilo { get; set; }
        public int totalcorte { get; set; }
    }

    void VerInfo()
    {
        if (!string.IsNullOrEmpty(txtcorte.Text))
        {
            var proc = OrderDetailDA.ReporteInventarioIntexProcPorCorte(txtcorte.Text.Trim());
            Session["dtinv"] = proc;
            grdinvintex.DataSource = Session["dtinv"];
            grdinvintex.DataBind();
        }
        else
        {
            var proc = OrderDetailDA.ReporteInventarioIntexProc();
            Session["dtinv"] = proc;
            grdinvintex.DataSource = Session["dtinv"];
            grdinvintex.DataBind();
        }        
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            VerInfo();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            exportar.GridViewID = "grdinvintex";

            DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

            DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
            link1.Component = exportar;


            DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
            compositeLink.Links.AddRange(new object[] { link1 });

            compositeLink.CreateDocument();

            using (MemoryStream stream = new MemoryStream())
            {
                XlsxExportOptions options = new XlsxExportOptions();                

                options.ExportMode = XlsxExportMode.SingleFile;
                options.SheetName = "InventarioIntex";
                compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/xlsx");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", "attachment; filename=InventarioIntex.xlsx");
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }

            ps.Dispose();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReporteIntexInv.aspx");
    }

    protected void grdinvintex_DataBinding(object sender, EventArgs e)
    {
        grdinvintex.DataSource = Session["dtinv"];
    }

    protected void grdinvintex_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdinvintex.DataBind();
    }
}