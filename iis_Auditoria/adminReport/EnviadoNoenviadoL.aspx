﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EnviadoNoenviadoL.aspx.cs" Inherits="adminReport_EnviadoNoenviadoL" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=fecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });

        function vacio1() {
            swal({
                title: 'Por Favor!',
                text: " Seleccione el tipo de reporte.",
                type: 'info'
            });
        }
    </script>

    <script type="text/javascript">       

        function exito() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }

        $(document).ready(function () {
            $(function () {
                $('#<%=fecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <style>
        .mar {
            margin-bottom: 10px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Cortes Abiertos-Cerrados</strong></div>
            <div class="panel-body">
                <%--<asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>--%>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <br />

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Selección</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="rd1" runat="server">
                                    <asp:ListItem Value="1" Text="Filtro"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Filtro - Fechas"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Inventario - O/C"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Filtros</div>
                            <div class="panel-body">
                                <strong>Selected Type</strong>
                                <asp:DropDownList ID="drptype" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">Select...</asp:ListItem>
                                    <asp:ListItem Value="1">Open Cut</asp:ListItem>
                                    <asp:ListItem Value="2">Close Cut</asp:ListItem>
                                    <asp:ListItem Value="3">All Cut</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <asp:Panel ID="pan1" runat="server">
                            <div class="panel panel-default">
                                <div class="panel-heading">Fechas</div>
                                <div class="panel-body">
                                    <strong>Fecha Inicio</strong>
                                    <asp:TextBox ID="fecha1" runat="server" AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>

                                    <strong>Fecha Final</strong>
                                    <asp:TextBox ID="fecha2" runat="server" AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>

                                </div>
                            </div>
                        </asp:Panel>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <br />
                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control" Text="Generar" OnClick="btngenerar_Click" />

                                <div>
                                    <br />
                                    <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control" Text="Pass to excel" OnClick="btnexcel_Click" Visible="true" />
                                </div>

                                <div>
                                    <br />
                                    <asp:Button ID="btnl" runat="server" CssClass="btn btn-primary form-control" Text="Limpiar" OnClick="btnl_Click" Visible="true" />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <dx:ASPxGridView ID="gridsent" runat="server" Width="100%" Theme="Metropolis" KeyFieldName="Corte" SettingsBehavior-AllowSort="false" OnDataBinding="gridsent_DataBinding" OnCustomCallback="gridsent_CustomCallback">
                        <SettingsSearchPanel Visible="true" ShowClearButton="true" />
                        <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="Corte" Caption="PO" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Estilo" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Quantity" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Color" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Contadas" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Deficit" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="FechaUltimoEnvio" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Enviadas" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="NoEnviadas" Caption="No Enviadas" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowFooter="true" ShowGroupFooter="VisibleAlways" VerticalScrollBarMode="Visible" />
                        <SettingsPager Mode="ShowAllRecords" />
                        <SettingsPager PageSize="100" />
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="Corte" SummaryType="Count" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="Quantity" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="Contadas" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="Deficit" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="Enviadas" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="NoEnviadas" SummaryType="Sum" DisplayFormat="{0}" />
                        </TotalSummary>
                    </dx:ASPxGridView>
                    <dx:ASPxGridViewExporter ID="export" runat="server"></dx:ASPxGridViewExporter>
                    <hr />
                    <asp:Panel ID="panel1" runat="server" Visible="false">
                        <dx:ASPxGridView ID="grdInv" runat="server" Width="100%" Theme="Metropolis" KeyFieldName="Corte" SettingsBehavior-AllowSort="false" OnDataBinding="grdInv_DataBinding" OnCustomCallback="grdInv_CustomCallback">
                            <SettingsSearchPanel Visible="true" ShowClearButton="true" />
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="Corte" Caption="PO" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Estilo" VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Quantity" VisibleIndex="3">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Color" VisibleIndex="4">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="InventarioIntex" Caption="Inventario Intex" VisibleIndex="5">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowFooter="true" ShowGroupFooter="VisibleAlways" VerticalScrollBarMode="Visible" />
                            <SettingsPager Mode="ShowAllRecords" />
                            <SettingsPager PageSize="100" />
                            <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="EnvioDePlantasAIntex" SummaryType="Sum" DisplayFormat="{0}" />                            
                        </TotalSummary>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar1" runat="server"></dx:ASPxGridViewExporter>
                    </asp:Panel>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
