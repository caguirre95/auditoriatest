﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportWeekIntex.aspx.cs" Inherits="Intex_ReportWeekIntex" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:10px;">
            <div class="panel-heading">Select search filter</div>
            <div class="panel-body">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    Week
                      <asp:DropDownList ID="drpSemana" CssClass="form-control" OnSelectedIndexChanged="drpSemana_SelectedIndexChanged" AutoPostBack="true" runat="server" />
                    <br />
                    <asp:Button ID="btnExport" OnClick="btnExport_Click" CssClass="btn btn-success form-control" runat="server" Text="Export Excel" />
                    <hr />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow-x: auto">
                    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" Theme="Metropolis" runat="server" OnDataBinding="grid_DataBinding" OnLoad="grid_Load"
                        Width="100%" AutoGenerateColumns="False" OnCustomCallback="grid_CustomCallback">
                        <Columns>
                            <dx:GridViewBandColumn Caption="week" HeaderStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <dx:ASPxLabel ID="ASPxLabelweek" runat="server" Text=""></dx:ASPxLabel>
                                </HeaderTemplate>
                                <Columns>
                                    <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="Cancelacion De Cortes">
                                        <Columns>
                                            <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="ccc">
                                                <HeaderTemplate>
                                                    <dx:ASPxLabel ID="ASPxLabelfecha" runat="server" Text=""></dx:ASPxLabel>
                                                </HeaderTemplate>
                                                <Columns>
                                                    <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Left" Caption="Responsable: ">
                                                        <Columns>
                                                            <dx:GridViewDataDateColumn FieldName="Linea" GroupIndex="0" Settings-GroupInterval="Value" />
                                                            <dx:GridViewDataDateColumn FieldName="Cut" />
                                                            <dx:GridViewDataDateColumn FieldName="Style">
                                                                <FooterTemplate>
                                                                    Total                                         
                                                                </FooterTemplate>
                                                            </dx:GridViewDataDateColumn>
                                                            <dx:GridViewDataTextColumn FieldName="Quantity" />
                                                            <dx:GridViewDataTextColumn FieldName="Monday" />
                                                        </Columns>
                                                    </dx:GridViewBandColumn>
                                                    <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Left" Caption="Firma">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn FieldName="Tuesday" />
                                                            <dx:GridViewDataTextColumn FieldName="Wednesday" />
                                                            <dx:GridViewDataTextColumn FieldName="Thursday" />
                                                        </Columns>
                                                    </dx:GridViewBandColumn>
                                                </Columns>
                                            </dx:GridViewBandColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="ROC 05 REG CAL 0031">
                                        <Columns>
                                            <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="Version: 00 ">
                                                <Columns>
                                                    <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Left" Caption="fff">
                                                        <HeaderTemplate>
                                                            <dx:ASPxLabel ID="ASPxLabelfechaLong" runat="server" Text=""></dx:ASPxLabel>
                                                        </HeaderTemplate>
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn FieldName="Friday" />
                                                            <dx:GridViewDataTextColumn FieldName="Saturday" />
                                                            <dx:GridViewDataTextColumn FieldName="OpenCut" />
                                                            <dx:GridViewDataTextColumn FieldName="inventario" />
                                                        </Columns>
                                                    </dx:GridViewBandColumn>
                                                </Columns>
                                            </dx:GridViewBandColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                </Columns>
                            </dx:GridViewBandColumn>

                        </Columns>
                        <Settings ShowFooter="true" ShowGroupFooter="VisibleAlways" />
                        <SettingsBehavior AutoExpandAllGroups="true" />
                        <SettingsPager Mode="ShowAllRecords" />
                        <GroupSummary>
                            <dx:ASPxSummaryItem FieldName="Quantity" DisplayFormat="{0}" ShowInGroupFooterColumn="Quantity" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Monday" DisplayFormat="{0}" ShowInGroupFooterColumn="Monday" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Tuesday" DisplayFormat="{0}" ShowInGroupFooterColumn="Tuesday" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Wednesday" DisplayFormat="{0}" ShowInGroupFooterColumn="Wednesday" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Thursday" DisplayFormat="{0}" ShowInGroupFooterColumn="Thursday" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Friday" DisplayFormat="{0}" ShowInGroupFooterColumn="Friday" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Saturday" DisplayFormat="{0}" ShowInGroupFooterColumn="Saturday" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="OpenCut" DisplayFormat="{0}" ShowInGroupFooterColumn="OpenCut" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="inventario" DisplayFormat="{0}" ShowInGroupFooterColumn="inventario" SummaryType="Sum" />
                        </GroupSummary>
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="Quantity" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Monday" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Tuesday" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Wednesday" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Thursday" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Friday" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Saturday" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="OpenCut" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="inventario" DisplayFormat="{0}" SummaryType="Sum" />
                        </TotalSummary>


                    </dx:ASPxGridView>
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
