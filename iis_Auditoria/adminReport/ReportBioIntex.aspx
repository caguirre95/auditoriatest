﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportBioIntex.aspx.cs" Inherits="Intex_ReportBioIntex" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script>
        function vacio(fecha) {
            swal({
                title: 'Por Favor!',
                text: "No hay datos para mostrar en esta fecha : " + fecha,
                type: 'info'
            });
        }

        function vacio1() {
            swal({
                title: 'Por Favor!',
                text: " Ingrese los campos de manera correcta",
                type: 'info'
            });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtdate.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"                    
                })
            })
        });

        function exito() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
    </script>

  <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top:10px;">
            <div class="panel-heading"><strong>Reporte Bihorario</strong></div>
            <div class="panel-body">

                <div class="col-lg-12">
                    <%--<strong>Opciones</strong>
                    <asp:DropDownList ID="drpop" runat="server" CssClass="form-control" Width="200px">
                        <asp:ListItem Value="0">Select...</asp:ListItem>
                        <asp:ListItem Value="1">Bihorario</asp:ListItem>                        
                    </asp:DropDownList>--%>
                    <strong>Fecha</strong>
                    <asp:TextBox ID="txtdate" placeholder="Date" runat="server" CssClass="form-control" Width="200px" AutoCompleteType="Disabled">
                    </asp:TextBox>
                    <br />
                    <div style="margin-top: 5px;">
                        <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info" Text="Generar" Width="200px" OnClick="btngenerar_Click" />
                        <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success" Text="Pass to excel" Width="200px" OnClick="btnexcel_Click" />
                    </div>
                    <div style="margin-top:20px">
                        <dx:ASPxGridView ID="grdbiho" runat="server" KeyFieldName="id" Width="100%" Theme="Metropolis" OnCustomUnboundColumnData="grdbiho_CustomUnboundColumnData" SettingsBehavior-AllowSort="true"
                            OnDataBinding="grdbiho_DataBinding" OnCustomCallback="grdbiho_CustomCallback">
                            <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                            <Columns>

                                <dx:GridViewBandColumn Caption="CONTROL INTERNO DE ENTREGAS DE LINEA A LAVANDERIA O EMPAQUE" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Responsable:">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Line" Caption="Line" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Quantity" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="POrder" Caption="Cut" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Color" Caption="Color" VisibleIndex ="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="09:00am" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Firma:">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="11:00am" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="01:00pm" VisibleIndex="5">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="03:00pm" VisibleIndex="6">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="04:50pm" VisibleIndex="7">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="06:50pm" VisibleIndex="8">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>

                                    </Columns>
                                </dx:GridViewBandColumn>

                                <dx:GridViewBandColumn Caption="ROC 01 REG PRO 0020" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Fecha de Creacion: Junio de 2017" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="TotalBihorario" UnboundType="Integer" VisibleIndex="9">
                                                    <PropertiesTextEdit DisplayFormatString="{0}" />
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Version: 01" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Deficit" VisibleIndex="10">                                                    
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>

                                    </Columns>
                                </dx:GridViewBandColumn>

                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                            <Settings ShowFooter="true" />
                            <SettingsPager PageSize="100" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="POrder" SummaryType="Count" DisplayFormat="{0}" />
                                <%--<dx:ASPxSummaryItem FieldName="09:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="11:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="01:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="03:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="04:50pm" SummaryType="Sum" DisplayFormat="{0}" />--%>
                                <dx:ASPxSummaryItem FieldName="TotalBihorario" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <GroupSummary>
                                <dx:ASPxSummaryItem FieldName="POrder" ShowInGroupFooterColumn="POrder" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="09:00am" ShowInGroupFooterColumn="09:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="11:00am" ShowInGroupFooterColumn="11:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="01:00pm" ShowInGroupFooterColumn="01:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="03:00pm" ShowInGroupFooterColumn="03:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="04:50pm" ShowInGroupFooterColumn="04:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="06:50pm" ShowInGroupFooterColumn="06:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="TotalBihorario" ShowInGroupFooterColumn="TotalBihorario" SummaryType="Sum" DisplayFormat="{0}" />
                            </GroupSummary>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar" runat="server"></dx:ASPxGridViewExporter>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>

