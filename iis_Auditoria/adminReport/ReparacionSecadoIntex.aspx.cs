﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_ReparacionSecadoIntex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string us = Page.User.Identity.Name.ToString().ToLower().Trim();

            if (!string.Equals(us, "admonintex"))
            {
                FormsAuthentication.SignOut();
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
    }

    protected void LtnMostrar_Click(object sender, EventArgs e)
    {
        try
        {
            VerinfoCorteRep();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    void VerinfoCorteRep()
    {
        try
        {
            DataTable dt;

            string queryCortes = " select ROW_NUMBER() OVER (ORDER BY idSecadoDetalle) id, idSecadoDetalle, corteCompleto, unidadesSecadas, CONVERT(date, fechaSecado) Fecha from Intexdbo.intexSecadoDetalle"
                               + " where corteCompleto = '" + txtPorder.Text.Trim() + "'";

            dt = DataAccess.Get_DataTable(queryCortes);

            gridEnvios.DataSource = dt;

            gridEnvios.DataBind();

        }
        catch (Exception)
        {
            //throw;
        }
    }

    protected void btnActualizarSecado_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtPorder.Text.Trim()))
            {
                string resp = "";

                for (int i = 0; i < gridEnvios.Rows.Count; i++)
                {
                    var row = gridEnvios.Rows[i];

                    int indice = row.RowIndex;

                    string id = gridEnvios.DataKeys[indice].Value.ToString();

                    var chk = (CheckBox)row.FindControl("chk1");

                    if (chk.Checked)
                    {                        
                        var corteCompleto = (Label)row.FindControl("lblcorte");
                        string param = "E";

                        resp = OrderDetailDA.RepararBultoSecadoIntex(int.Parse(id), corteCompleto.Text, 1, Context.User.Identity.Name, param);
                    }
                }

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitosoElim();", true);
                    VerinfoCorteRep();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave('" + resp + "');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnEliminarsecado_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtPorder.Text.Trim()))
            {
                string param = "T";
                string resp = OrderDetailDA.RepararBultoSecadoIntex(0, txtPorder.Text.Trim(), 1, Context.User.Identity.Name, param);

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitosoElim();", true);
                    VerinfoCorteRep();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave('" + resp + "');", true);
                }
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/Reparacionsecadointex.aspx");
    }

    protected void gridEnvios_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("Actualizar"))
            {
                GridViewRow item = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int indice = item.RowIndex;
                string id = gridEnvios.DataKeys[indice].Value.ToString();

                var corteCompleto = (Label)item.FindControl("lblcorte");
                var units = (TextBox)item.FindControl("txtunits");
                string param = "A";

                if (units.Text != string.Empty)
                {
                    string resp = OrderDetailDA.RepararBultoSecadoIntex(int.Parse(id), corteCompleto.Text, int.Parse(units.Text), Context.User.Identity.Name, param);

                    if (resp.Equals("Exito"))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);

                        VerinfoCorteRep();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave('" + resp + "');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
                }

            }
            else if (e.CommandName.Equals("Eliminar"))
            {
                GridViewRow item = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int indice = item.RowIndex;
                string id = gridEnvios.DataKeys[indice].Value.ToString();

                var corteCompleto = (Label)item.FindControl("lblcorte");
                var units = (TextBox)item.FindControl("txtunits");
                string param = "E";

                string resp = OrderDetailDA.RepararBultoSecadoIntex(int.Parse(id), corteCompleto.Text, 1, Context.User.Identity.Name, param);

                if (resp.Equals("Exito"))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitosoElim();", true);

                    VerinfoCorteRep();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertClave('" + resp + "');", true);
                }
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }
}