﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HistInPlant.aspx.cs" Inherits="adminReport_HistInPlant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link rel="stylesheet" type="text/css" href="../sweetalert-master/dist/sweetalert.css" />
    <script src="../sweetalert-master/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }

         function selec() {
            swal({
                title: 'Error, seleccione un comentario !',                
                type: 'warning'
            });
        }
    </script>

    <style>
        .mar_t {
            margin: 10px 10px 10px auto;
        }

        .padd_t {
            padding-top: 18px !important;
        }

        .mr {
            margin-bottom: 15px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading"><strong>In Plant</strong></div>
            <div class="panel-body">
                 
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">                     
                    <asp:UpdatePanel ID="upd" runat="server">
                        <ContentTemplate>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <strong>Linea</strong>  
                                <asp:DropDownList ID="drpline" runat="server" CssClass="form-control" OnSelectedIndexChanged="drpline_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        <strong>Comentario</strong> 
                                        <asp:DropDownList ID="drpcom" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">Select...</asp:ListItem>
                                            <asp:ListItem Value="1">Calidad</asp:ListItem>
                                            <asp:ListItem Value="2">Problemas</asp:ListItem>
                                            <asp:ListItem Value="3">Otros</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <asp:Button ID="btngrd" runat="server" Text="Save In Plant" CssClass="btn btn-primary form-control mr" OnClick="btngrd_Click" />
                                <asp:Button ID="btnlim" runat="server" Text="Ver Todo" CssClass="btn btn-info form-control mr" OnClick="btnlim_Click" />
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <asp:GridView ID="grdhist" runat="server" CssClass="table table-hover table-striped" AutoGenerateColumns="false" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <HeaderTemplate>
                                                Id 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblidorder" runat="server" Font-Size="Medium" CssClass="mar_t" Text='<%#Bind("Id_Order") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Linea 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbllinea" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("Linea") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Porder
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPorder" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("Porder") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" ItemStyle-CssClass="padd_t">
                                            <HeaderTemplate>
                                                Quantity 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblquantity" runat="server" Font-Size="Medium" CssClass="label label-success" Text='<%#Bind("Quantity") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                No Enviadas
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtunidades" Enabled="false" Width="100px" Font-Size="Medium" CssClass="form-control" onkeypress="return num(event);" Text='<%#Bind("Cantidad") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                Comentario
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtcom" Font-Size="Medium" CssClass="form-control" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" Text="Enviar" Checked="true" CssClass="btn btn-default" runat="server" />
                                                <asp:LinkButton Text="" ID="lnkg" OnClick="lnkg_Click" CssClass="btn btn-info" runat="server" ><i class="glyphicon glyphicon-upload"> </i> </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>

            </div>
        </div>

    </div>

</asp:Content>

