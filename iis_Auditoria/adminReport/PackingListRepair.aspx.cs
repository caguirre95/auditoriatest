﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_PackingListRepair : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                string us = Page.User.Identity.Name.ToString().ToLower().Trim();

                if (!string.Equals(us, "adminsnr"))
                {
                    FormsAuthentication.SignOut();
                    Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
                }
            }
        }
        catch (Exception ex)
        {

            string tipo = "warning";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + ex.Message + "','" + tipo + "');", true);
        }
    }

    DataTable ListarEnvios(int corte, string fecha, int envio, string fechaconteo)
    {

        var date1 = Convert.ToDateTime(fecha);
        var date2 = Convert.ToDateTime(fechaconteo);

        var dt = OrderDetailDA.ListarCorteaCorreccion(corte, date1, envio, date2);

        return dt;
    }

    void cargagrid(int corte, string fecha, int envio, string fechaconteo)
    {
        var dt = ListarEnvios(corte, fecha, envio, fechaconteo);

        dt.DefaultView.Sort = "fechaenvio ASC,enviodia ASC";

        GridView1.DataSource = dt;
        GridView1.DataBind();
    }



    public class porderClass
    {
        public int idorder { get; set; }
        public string porder { get; set; }
        public string style { get; set; }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select top 20 p.Id_Order,p.POrder,s.Style"
                                           + " from POrder p join Style s on p.Id_Style = s.Id_Style"
                                           + " where p.POrder like '%" + pre + "%'", cn);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][1].ToString();
                obj.style = dt.Rows[i][2].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    void limpiar()
    {
        txtenvioCorteNuevo.Text = string.Empty;
        txtenviodiaNuevo.Text = string.Empty;
        txtfechaenvioNuevo.Text = string.Empty;
        txtfechaconteoNuevo.Text = string.Empty;
    }

    protected void LtnMostrar_Click(object sender, EventArgs e)
    {
        try
        {

            if (!string.IsNullOrEmpty(hdnidcorte.Value))
            {
                int corte = int.Parse(hdnidcorte.Value);
                int envio = 0;
                string fecha = "01-01-1990";
                string fechaconteo = "01-01-1990";

                if (!string.IsNullOrEmpty(txtenviodia.Text))
                    envio = Convert.ToInt16(txtenviodia.Text);

                if (!string.IsNullOrEmpty(txtfechaenvio.Text))
                    fecha = txtfechaenvio.Text;

                if (!string.IsNullOrEmpty(txtfechaconteo.Text))
                    fechaconteo = txtfechaconteo.Text;


                cargagrid(corte, fecha, envio, fechaconteo);

            }
            else
            {
                string tipo = "info";
                string mess = "Debe ingresar un corte!!!";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + mess + "','" + tipo + "');", true);
            }

        }
        catch (Exception ex)
        {

            string tipo = "warning";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + ex.Message + "','" + tipo + "');", true);
        }
    }


    protected void lbkOrdenarPorSeq_Click(object sender, EventArgs e)
    {
        try
        {

            if (!string.IsNullOrEmpty(hdnidcorte.Value))
            {
                int corte = int.Parse(hdnidcorte.Value);
                int envio = 0;
                string fecha = "01-01-1990";
                string fechaconteo = "01-01-1990";

                if (!string.IsNullOrEmpty(txtenviodia.Text))
                    envio = Convert.ToInt16(txtenviodia.Text);

                if (!string.IsNullOrEmpty(txtfechaenvio.Text))
                    fecha = txtfechaenvio.Text;

                if (!string.IsNullOrEmpty(txtfechaconteo.Text))
                    fechaconteo = txtfechaconteo.Text;

                var dt = ListarEnvios(corte, fecha, envio, fechaconteo);

                dt.DefaultView.Sort = "nseq ASC";

                GridView1.DataSource = dt;
                GridView1.DataBind();
            }

        }
        catch (Exception ex)
        {

            string tipo = "warning";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + ex.Message + "','" + tipo + "');", true);
        }
    }


    protected void lbkOrdenarPorfechaEnvio_Click(object sender, EventArgs e)
    {
        try
        {

            if (!string.IsNullOrEmpty(hdnidcorte.Value))
            {
                int corte = int.Parse(hdnidcorte.Value);
                int envio = 0;
                string fecha = "01-01-1990";
                string fechaconteo = "01-01-1990";

                if (!string.IsNullOrEmpty(txtenviodia.Text))
                    envio = Convert.ToInt16(txtenviodia.Text);

                if (!string.IsNullOrEmpty(txtfechaenvio.Text))
                    fecha = txtfechaenvio.Text;

                if (!string.IsNullOrEmpty(txtfechaconteo.Text))
                    fechaconteo = txtfechaconteo.Text;

                cargagrid(corte, fecha, envio, fechaconteo);
            }

        }
        catch (Exception ex)
        {

            string tipo = "warning";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + ex.Message + "','" + tipo + "');", true);
        }
    }


    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

            var resp = "";

            if (e.CommandName == "actualizar")
            {

                //filtro de busqueda
                string fecha1 = string.IsNullOrEmpty(txtfechaenvio.Text) == true ? "01-01-1990" : txtfechaenvio.Text;
                string fecha2 = string.IsNullOrEmpty(txtfechaconteo.Text) == true ? "01-01-1990" : txtfechaconteo.Text;
                int enviodia = string.IsNullOrEmpty(txtenviodia.Text) == true ? 0 : Convert.ToInt16(txtenviodia.Text);

                GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int corte = int.Parse(hdnidcorte.Value);

                var lblidhistorico = (Label)row.FindControl("lblidhistorico");
                var lblidenvio = (Label)row.FindControl("lblidenvio");
                var txtfechaConteo = (TextBox)row.FindControl("txtfechaConteo");
                var txtcantidadAgregada = (TextBox)row.FindControl("txtcantidadAgregada");
                var txtenvioCorteN = (TextBox)row.FindControl("txtenviocorte");
                var txtenvioDiaN = (TextBox)row.FindControl("txtenviodia");
                var txtfechaEnvioN = (TextBox)row.FindControl("txtfechaenvio");

                int enviocorteN = string.IsNullOrEmpty(txtenvioCorteN.Text) == true ? 0 : Convert.ToInt16(txtenvioCorteN.Text);
                int enviodiaN = string.IsNullOrEmpty(txtenvioDiaN.Text) == true ? 0 : Convert.ToInt16(txtenvioDiaN.Text);
                string fechaEnvioN = string.IsNullOrEmpty(txtfechaEnvioN.Text) == true ? "01-01-1990" : txtfechaEnvioN.Text.Substring(0, 10);

                string fechaconteoN = string.IsNullOrEmpty(txtfechaConteo.Text) == true ? "01-01-1990" : txtfechaConteo.Text.Substring(0, 10);
                int contadoN = string.IsNullOrEmpty(txtcantidadAgregada.Text) == true ? 0 : Convert.ToInt16(txtcantidadAgregada.Text);

                if ((!fechaEnvioN.Equals("01-01-1990") || enviodiaN > 0) && enviocorteN == 0)
                {
                    txtenvioDiaN.Text = "";
                    txtfechaEnvioN.Text = "";

                    enviodiaN = 0;
                    fechaEnvioN = "01-01-1990";
                }
               
            
                DateTime date1 = Convert.ToDateTime(fechaconteoN);
                DateTime date2 = Convert.ToDateTime(fechaEnvioN);
                if (date1.Date > DateTime.Now.Date || date2.Date > DateTime.Now.Date)
                {
                    cargagrid(corte, fecha1, enviodia, fecha2);

                    string tipo1 = "warning";
                    string mess1 = "Las fechas no pueden ser mayor a la fecha actual!!!";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + mess1 + "','" + tipo1 + "');", true);

                    return;
                }
                else
                {
                    resp = OrderDetailDA.CorreccionFechaEnvio(int.Parse(lblidhistorico.Text), int.Parse(lblidenvio.Text), fechaEnvioN, enviocorteN, enviodiaN, fechaconteoN, contadoN, corte);
                }

                cargagrid(corte, fecha1, enviodia, fecha2);

                if (resp.Equals("Exito"))
                    resp = "Cambios Guardados!!!";
                
                string tipo = "success";
              //  string mess = "Cambios Guardados!!!";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + resp + "','" + tipo + "');", true);

            }
        }
        catch (Exception ex)
        {
            string tipo = "warning";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + ex.Message + "','" + tipo + "');", true);
        }

    }


    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        try
        {
          
            int enviodiaN = string.IsNullOrEmpty(txtenviodiaNuevo.Text) == true ? 0 : Convert.ToInt16(txtenviodiaNuevo.Text);
            string fechaN = string.IsNullOrEmpty(txtfechaenvioNuevo.Text) == true ? "01-01-1990" : txtfechaenvioNuevo.Text;
            string fechaconteoN = string.IsNullOrEmpty(txtfechaconteoNuevo.Text) == true ? "01-01-1990" : txtfechaconteoNuevo.Text;


            if (!fechaconteoN.Equals("01-01-1990") || !fechaN.Equals("01-01-1990"))
            {
                int corte = int.Parse(hdnidcorte.Value);

           
                if ((!fechaN.Equals("01-01-1990") && enviodiaN == 0) || (fechaN.Equals("01-01-1990") && enviodiaN > 0))
                {
                    string tipo = "info";
                    string mess = "para Actualizar fecha envio o envio del dia ambos campos deben contener datos!!!";
                    string info = "Validacion";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica2('" + info + "','"+ mess + "','" + tipo + "');", true);

                    return;
                }

                DateTime date1 = Convert.ToDateTime(fechaconteoN);
                DateTime date2 = Convert.ToDateTime(fechaN);
                if (date1.Date > DateTime.Now.Date || date2.Date > DateTime.Now.Date)
                {
                    string tipo1 = "warning";
                    string mess1 = "Las fechas no pueden ser mayor a la fecha actual!!!";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + mess1 + "','" + tipo1 + "');", true);

                    return;
                }

                int cont = 0,Anulados=0;
                for (int i = 0; i < GridView1.Rows.Count; i++)
                {
                    var row = (GridViewRow)GridView1.Rows[i];

                    var chk = (CheckBox)row.FindControl("CheckBox");

                    if (chk.Checked)
                    {
                        cont++;
                        var lblidhistorico = (Label)row.FindControl("lblidhistorico");
                        var lblidenvio = (Label)row.FindControl("lblidenvio");
                        var txtenvioCorteN = (TextBox)row.FindControl("txtenviocorte");

                        var enviocorteN = string.IsNullOrEmpty(txtenvioCorteN.Text) == true ? 0 : Convert.ToInt16(txtenvioCorteN.Text);

                        if ((!fechaN.Equals("01-01-1990") ) && enviocorteN == 0)
                        {
                            Anulados++;
                        }
                        else
                        {
                            OrderDetailDA.CorreccionFechaEnvio(int.Parse(lblidhistorico.Text), int.Parse(lblidenvio.Text), fechaN, enviocorteN, enviodiaN, fechaconteoN, 0, corte);
                        }

                       

                    }
                }

                if (cont > 0)
                {

                    string fecha = string.IsNullOrEmpty(txtfechaenvio.Text) == true ? "01-01-1990" : txtfechaenvio.Text;
                    int enviodia = string.IsNullOrEmpty(txtenviodia.Text) == true ? 0 : Convert.ToInt16(txtenviodia.Text);
                    string fechaconteo = string.IsNullOrEmpty(txtfechaconteo.Text) == true ? "01-01-1990" : txtfechaconteo.Text;

                    cargagrid(corte, fecha, enviodia, fechaconteo);

                    limpiar();

                    if (Anulados>0)
                    {
                        string tipo = "success";
                        string mess = "No se actualizaron algunos registros por intentar actualizar fechas de envio a registros que no han sido enviados";
                        string info = "Cambios Guardados!!!";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica2('" + info + "','" + mess + "','" + tipo + "');", true);
                    
                    }
                    else
                    {
                        string tipo = "success";
                        string mess = "Cambios Guardados!!!";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + mess + "','" + tipo + "');", true);
                    }

                  

                }
                else
                {
                    string tipo = "info";
                    string mess = "Debe Chequear los registros que seran actualizados!!!";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + mess + "','" + tipo + "');", true);
                }
            }

           



        }
        catch (Exception ex)
        {

            string tipo = "warning";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + ex.Message + "','" + tipo + "');", true);
        }

    }


    protected void btnAnularEnvio_Click(object sender, EventArgs e)
    {
        try
        {
            int corte = int.Parse(hdnidcorte.Value);

            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                var row = (GridViewRow)GridView1.Rows[i];

                var chk = (CheckBox)row.FindControl("CheckBox");

                if (chk.Checked)
                {

                    var lblidhistorico = (Label)row.FindControl("lblidhistorico");
                    var lblidenvio = (Label)row.FindControl("lblidenvio");

                    OrderDetailDA.AnularEnvio(int.Parse(lblidhistorico.Text), int.Parse(lblidenvio.Text),corte);

                }

            }

            string fecha = string.IsNullOrEmpty(txtfechaenvio.Text) == true ? "01-01-1990" : txtfechaenvio.Text;
            int enviodia = string.IsNullOrEmpty(txtenviodiaNuevo.Text) == true ? 0 : Convert.ToInt16(txtenviodiaNuevo.Text);
            string fechaconteo = string.IsNullOrEmpty(txtfechaconteo.Text) == true ? "01-01-1990" : txtfechaconteo.Text;


            cargagrid(corte, fecha, enviodia, fechaconteo);

            limpiar();

            string tipo = "success";
            string mess = "Cambios Guardados!!!";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + mess + "','" + tipo + "');", true);


        }
        catch (Exception ex)
        {

            string tipo = "warning";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + ex.Message + "','" + tipo + "');", true);
        }
    }


    protected void btnEliminarConteo_Click(object sender, EventArgs e)
    {
        try
        {

            if (!string.IsNullOrEmpty(hdnidcorte.Value))
            {
                int corte = int.Parse(hdnidcorte.Value);

                for (int i = 0; i < GridView1.Rows.Count; i++)
                {
                    var row = (GridViewRow)GridView1.Rows[i];

                    var chk = (CheckBox)row.FindControl("CheckBox");

                    if (chk.Checked)
                    {

                        var lblidhistorico = (Label)row.FindControl("lblidhistorico");
                        var lblidenvio = (Label)row.FindControl("lblidenvio");

                        OrderDetailDA.EliminarConteoEnvio(int.Parse(lblidhistorico.Text), int.Parse(lblidenvio.Text),corte);

                    }

                }

                string fecha = string.IsNullOrEmpty(txtfechaenvio.Text) == true ? "01-01-1990" : txtfechaenvio.Text;
                int enviodia = string.IsNullOrEmpty(txtenviodia.Text) == true ? 0 : Convert.ToInt16(txtenviodia.Text);
                string fechaconteo = string.IsNullOrEmpty(txtfechaconteo.Text) == true ? "01-01-1990" : txtfechaconteo.Text;

               // int corte = int.Parse(hdnidcorte.Value);

                cargagrid(corte, fecha, enviodia, fechaconteo);

                limpiar();

                string tipo = "success";
                string mess = "Cambios Guardados!!!";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + mess + "','" + tipo + "');", true);

            }
            else
            {
                string tipo = "info";
                string mess = "Llenar los campos correctamente!!!";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + mess + "','" + tipo + "');", true);
            }

        }
        catch (Exception ex)
        {

            string tipo = "warning";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + ex.Message + "','" + tipo + "');", true);
        }
    }


    protected void CheckBoxtodos_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            var todos = (CheckBox)GridView1.HeaderRow.FindControl("CheckBoxtodos");

            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                var row = (GridViewRow)GridView1.Rows[i];

                var chk = (CheckBox)row.FindControl("CheckBox");

                if (todos.Checked)
                {
                    chk.Checked = true;
                }
                else
                {
                    chk.Checked = false;
                }

            }
        }
        catch (Exception ex)
        {

            string tipo = "warning";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + ex.Message + "','" + tipo + "');", true);
        }
    }


    protected void CheckBox_CheckedChanged(object sender, EventArgs e)
    {
        try
        {

            int cont = 0;

            int filas = GridView1.Rows.Count;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                var chkA = (CheckBox)GridView1.Rows[i].FindControl("CheckBox");

                if (chkA.Checked)
                {
                    cont++;
                }
            }

            var todos = (CheckBox)GridView1.HeaderRow.FindControl("CheckBoxtodos");

            todos.Checked = cont == filas ? true : false;

        }
        catch (Exception ex)
        {
            string tipo = "warning";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertDinamica('" + ex.Message + "','" + tipo + "');", true);
        }
    }

}