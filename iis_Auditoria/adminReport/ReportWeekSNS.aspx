﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportWeekSNS.aspx.cs" Inherits="adminReport_ReportWeekSNS" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .a {
            margin-top: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Week Report</strong></div>
            <div class="panel-body">

                <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">

                    <strong>Week</strong>
                    <asp:DropDownList ID="drpSemana" CssClass="form-control" runat="server" Width="200px" />
                    <br />
                    <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control" Text="Generar" Width="200px" OnClick="btngenerar_Click" />
                   
                    <asp:Button ID="btnExport" OnClick="btnExport_Click" CssClass="btn btn-success form-control a" runat="server" Text="Export Excel" Width="200px" />
                    <hr />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow-x: auto">
                    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" Theme="Metropolis" runat="server" OnDataBinding="grid_DataBinding" 
                        Width="100%" AutoGenerateColumns="False" OnCustomCallback="grid_CustomCallback">
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="descripcion" Caption="Plant"  GroupIndex="0" />
                            <dx:GridViewDataTextColumn FieldName="Cliente" Caption="Client"  />
                            <dx:GridViewDataTextColumn FieldName="Linea" Caption="Line" />
                            <dx:GridViewDataTextColumn FieldName="Style" Caption="Style"  />
                            <dx:GridViewDataTextColumn FieldName="Cut" Caption="Cut"  />
                            <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Quantity"  />
                            <dx:GridViewDataTextColumn FieldName="Sewn_pairs" Caption="Sewn Pairs"  />
                            <dx:GridViewDataTextColumn FieldName="Line_Dif" Caption="Line Dif" />
                            <dx:GridViewDataTextColumn FieldName="WEEK_SEWN" Caption="Week Sewn"  />
                            <dx:GridViewDataTextColumn FieldName="SEWN_ACCUM" Caption="Sewn Accum"  />
                            <dx:GridViewDataTextColumn FieldName="START_ACCUM" Caption="Start Accum"  />

                            <dx:GridViewBandColumn Caption="Monday" HeaderStyle-HorizontalAlign="Left">                              
                                <Columns>
                                     <dx:GridViewDataTextColumn FieldName="SewM" Caption="Sew"  />
                                     <dx:GridViewDataTextColumn FieldName="SentM" Caption="Sent"  />
                                     <dx:GridViewDataTextColumn FieldName="NSentM" Caption="No Sent"  />
                                </Columns>
                            </dx:GridViewBandColumn>

                             <dx:GridViewBandColumn Caption="Tuesday" HeaderStyle-HorizontalAlign="Left">                              
                                <Columns>
                                     <dx:GridViewDataTextColumn FieldName="SewT" Caption="Sew" />
                                     <dx:GridViewDataTextColumn FieldName="SentT" Caption="Sent"  />
                                     <dx:GridViewDataTextColumn FieldName="NSentT" Caption="No Sent"  />
                                </Columns>
                            </dx:GridViewBandColumn>

                             <dx:GridViewBandColumn Caption="Wednesday" HeaderStyle-HorizontalAlign="Left">                              
                                <Columns>
                                     <dx:GridViewDataTextColumn FieldName="SewW" Caption="Sew" />
                                     <dx:GridViewDataTextColumn FieldName="SentW" Caption="Sent"  />
                                     <dx:GridViewDataTextColumn FieldName="NSentW" Caption="No Sent" />
                                </Columns>
                            </dx:GridViewBandColumn>

                             <dx:GridViewBandColumn Caption="Thursday" HeaderStyle-HorizontalAlign="Left">                              
                                <Columns>
                                     <dx:GridViewDataTextColumn FieldName="SewTH" Caption="Sew"  />
                                     <dx:GridViewDataTextColumn FieldName="SentTH" Caption="Sent"  />
                                     <dx:GridViewDataTextColumn FieldName="NSentTH" Caption="No Sent" />
                                </Columns>
                            </dx:GridViewBandColumn>

                             <dx:GridViewBandColumn Caption="Friday" HeaderStyle-HorizontalAlign="Left">                              
                                <Columns>
                                     <dx:GridViewDataTextColumn FieldName="SewF" Caption="Sew"  />
                                     <dx:GridViewDataTextColumn FieldName="SentF" Caption="Sent" />
                                     <dx:GridViewDataTextColumn FieldName="NSentF" Caption="No Sent" />
                                </Columns>
                            </dx:GridViewBandColumn>

                             <dx:GridViewBandColumn Caption="Saturday" HeaderStyle-HorizontalAlign="Left">                              
                                <Columns>
                                     <dx:GridViewDataTextColumn FieldName="SewS" Caption="Sew"  />
                                     <dx:GridViewDataTextColumn FieldName="SentS" Caption="Sent"  />
                                     <dx:GridViewDataTextColumn FieldName="NSentS" Caption="No Sent" />
                                </Columns>
                            </dx:GridViewBandColumn>

                            <dx:GridViewDataTextColumn FieldName="Received" Caption="Recived" />
                            <dx:GridViewDataTextColumn FieldName="BALANCE" Caption="Balance"  />
                            <dx:GridViewDataTextColumn FieldName="ReceivedP"  Caption="Received %" />

                        </Columns> 
                        <Settings ShowFooter="true" ShowGroupFooter="VisibleAlways" VerticalScrollBarMode="Visible" HorizontalScrollBarMode="Visible"  />
                        <SettingsBehavior AutoExpandAllGroups="true" />
                        <SettingsPager Mode="ShowAllRecords" />
                        <GroupSummary>
                            <dx:ASPxSummaryItem FieldName="Quantity" DisplayFormat="{0}" ShowInGroupFooterColumn="Quantity" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Sewn_pairs" DisplayFormat="{0}" ShowInGroupFooterColumn="Sewn_pairs" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Line_Dif" DisplayFormat="{0}" ShowInGroupFooterColumn="Line_Dif" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="WEEK_SEWN" DisplayFormat="{0}" ShowInGroupFooterColumn="WEEK_SEWN" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SEWN_ACCUM" DisplayFormat="{0}" ShowInGroupFooterColumn="SEWN_ACCUM" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="START_ACCUM" DisplayFormat="{0}" ShowInGroupFooterColumn="START_ACCUM" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SewM" DisplayFormat="{0}" ShowInGroupFooterColumn="SewM" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SentM" DisplayFormat="{0}" ShowInGroupFooterColumn="SentM" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="NSentM" DisplayFormat="{0}" ShowInGroupFooterColumn="NSentM" SummaryType="Sum" />
                             <dx:ASPxSummaryItem FieldName="SewT" DisplayFormat="{0}" ShowInGroupFooterColumn="SewT" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SentT" DisplayFormat="{0}" ShowInGroupFooterColumn="SentT" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="NSentT" DisplayFormat="{0}" ShowInGroupFooterColumn="NSentT" SummaryType="Sum" />

                             <dx:ASPxSummaryItem FieldName="SewW" DisplayFormat="{0}" ShowInGroupFooterColumn="SewW" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SentW" DisplayFormat="{0}" ShowInGroupFooterColumn="SentW" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="NSentW" DisplayFormat="{0}" ShowInGroupFooterColumn="NSentW" SummaryType="Sum" />

                             <dx:ASPxSummaryItem FieldName="SewTH" DisplayFormat="{0}" ShowInGroupFooterColumn="SewTH" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SentTH" DisplayFormat="{0}" ShowInGroupFooterColumn="SentTH" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="NSentTH" DisplayFormat="{0}" ShowInGroupFooterColumn="NSentTH" SummaryType="Sum" />

                             <dx:ASPxSummaryItem FieldName="SewF" DisplayFormat="{0}" ShowInGroupFooterColumn="SewF" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SentF" DisplayFormat="{0}" ShowInGroupFooterColumn="SentF" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="NSentF" DisplayFormat="{0}" ShowInGroupFooterColumn="NSentF" SummaryType="Sum" />

                             <dx:ASPxSummaryItem FieldName="SewS" DisplayFormat="{0}" ShowInGroupFooterColumn="SewS" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SentS" DisplayFormat="{0}" ShowInGroupFooterColumn="SentS" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="NSentS" DisplayFormat="{0}" ShowInGroupFooterColumn="NSentS" SummaryType="Sum" />
                            
                        </GroupSummary>
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="Quantity" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Sewn_pairs" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Line_Dif" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="WEEK_SEWN" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SEWN_ACCUM" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="START_ACCUM" DisplayFormat="{0}" SummaryType="Sum" />
                           
                            <dx:ASPxSummaryItem FieldName="SewM" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SentM" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="NSentM" DisplayFormat="{0}" SummaryType="Sum" />

                             <dx:ASPxSummaryItem FieldName="SewT" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SentT" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="NSentT" DisplayFormat="{0}" SummaryType="Sum" />

                             <dx:ASPxSummaryItem FieldName="SewW" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SentW" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="NSentW" DisplayFormat="{0}" SummaryType="Sum" />

                             <dx:ASPxSummaryItem FieldName="SewTH" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SentTH" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="NSentTH" DisplayFormat="{0}" SummaryType="Sum" />

                             <dx:ASPxSummaryItem FieldName="SewF" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SentF" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="NSentF" DisplayFormat="{0}" SummaryType="Sum" />

                             <dx:ASPxSummaryItem FieldName="SewS" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="SentS" DisplayFormat="{0}" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="NSentS" DisplayFormat="{0}" SummaryType="Sum" />

                             
                        </TotalSummary>

                    </dx:ASPxGridView>
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"></dx:ASPxGridViewExporter>
                </div>
            </div>

        </div>

    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

