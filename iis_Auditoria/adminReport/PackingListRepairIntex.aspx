﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PackingListRepairIntex.aspx.cs" Inherits="adminReport_PackingListRepairIntex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {

                    $.ajax({
                        url: "PackingListRepairIntex.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hdnidcorte.ClientID%>').val(ui.item.idorder);

                    //   $('#myModal').modal('toggle')

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });

        $(document).ready(function () {

            $(function () {
                $('#<%=txtfechaenvio.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

                $('#<%=txtfechaconteo.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })

                $('#<%=txtfechaconteoNuevo.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })
                 $('#<%=txtfechaenvioNuevo.ClientID%>').datepicker({
                    dateFormat: "dd/mm/yy"
                })
                

            })
        })


    </script>

    <script>
        function alertDinamica(mes, tipo) {
            swal({
                title: mes,
                type: tipo
            });
        }

        function alertDinamica2(mes,text, tipo) {
            swal({
                title: mes,
                text: text,
                type: tipo
            });
        }

        function limpiar(ee) {
            ee.value = '';
        }

        function limpiarauto() {
            $('#<%=txtPorder.ClientID%>').val('');
            $('#<%=hdnidcorte.ClientID%>').val('');
            $('#<%=txtfechaenvio.ClientID%>').val('');
            $('#<%=txtenviodia.ClientID%>').val('');
            $('#<%=txtfechaenvioNuevo.ClientID%>').val('');
            $('#<%=txtenviodiaNuevo.ClientID%>').val('');
            $('#<%=txtenvioCorteNuevo.ClientID%>').val('');
        }
    </script>

    <%--  <script>
        function key_up(objRef) {

            var row = objRef.parentNode.parentNode;

            if (row.hasChildNodes()) {
                var children = row.childNodes;

                alert(row.children[0].nodoName);

                for (var i = 0; i < children.length; i++) {
                    alert(children[i].childNodes[0].val);
                }
            }
        }
    </script>--%>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }
    </script>


    <script src="../jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <script src="../jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <link href="../jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <style>
        .mar_t {
            margin-top: 20px;
            margin-bottom: 10px;
            margin-left: 5px;
            margin-right: 5px;
        }

        .padd_t {
            padding-top: 18px !important;   
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
     <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Reparacion de Envios</strong></div>
            <div class="panel-body">
                <%-- <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>--%>

                <br />
                <div class="col-lg-4">

                    <div class="panel panel-default">
                        <div class="panel-heading">Filtros</div>
                        <div class="panel-body">
                            <asp:HiddenField ID="hdnidcorte" runat="server" />
                            <div class="form-group">
                                Corte:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" CssClass="btn btn-default" ID="LtnMostrar" OnClick="LtnMostrar_Click" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" onfocus="limpiarauto();" required="true" AutoCompleteType="Disabled" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                            </div>
                             <div class="form-group">
                                <label>Fecha Conteo</label>
                                <asp:TextBox ID="txtfechaconteo" onfocus="limpiar(this);" autocomplete="off" AutoCompleteType="None" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Fecha Envio</label>
                                <asp:TextBox ID="txtfechaenvio" onfocus="limpiar(this);" autocomplete="off" AutoCompleteType="None" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Envio Dia</label>
                                <asp:TextBox ID="txtenviodia" onfocus="limpiar(this);" CssClass="form-control" onkeypress="return num(event);" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="panel panel-info">
                        <div class="panel-heading">Comando de Reparación</div>
                        <div class="panel-body form-inline">
                            <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-success btn-block " Text="Actualizar" OnClick="btnActualizar_Click"  />
                            <asp:Button ID="btnAnularEnvio" runat="server" CssClass="btn btn-warning btn-block" Text="Anular Envio" OnClick="btnAnularEnvio_Click" />
                            <asp:Button ID="btnEliminarConteo" runat="server" CssClass="btn btn-danger btn-block" Text="Eliminar Conteo de Corte" OnClick="btnEliminarConteo_Click" />
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Nuevos Datos</div>
                        <div class="panel-body">
                             <div class="form-group">
                                <label>Fecha conteo</label>
                                <asp:TextBox ID="txtfechaconteoNuevo" autocomplete="off" onfocus="limpiar(this);" AutoCompleteType="None" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Fecha envio</label>
                                <asp:TextBox ID="txtfechaenvioNuevo" autocomplete="off" onfocus="limpiar(this);" AutoCompleteType="None" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Envio Dia</label>
                                <asp:TextBox ID="txtenviodiaNuevo" onkeypress="return num(event);" onfocus="limpiar(this);" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                
                                <asp:TextBox ID="txtenvioCorteNuevo" Enabled="false" Visible="false" onkeypress="return num(event);" onfocus="limpiar(this);" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="margin-top: 20px">

                    <asp:GridView ID="GridView1" runat="server" CssClass="table-responsive table-hover table" GridLines="None" AutoGenerateColumns="False" Width="100%" OnRowCommand="GridView1_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Corte" HeaderStyle-CssClass="header_gv">
                                <ItemTemplate>
                                    <asp:Label ID="lblporder" runat="server" CssClass="label label-default" Text='<%# Eval("Porder") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cantidad" HeaderStyle-CssClass="header_gv">
                                <ItemTemplate>
                                    <asp:Label ID="lblquantity" runat="server" CssClass="label label-default" Text='<%# Eval("quantity") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Talla" HeaderStyle-CssClass="header_gv">
                                <ItemTemplate>
                                    <asp:Label ID="lbltalla" runat="server" CssClass="label label-default" Text='<%# Eval("Size") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbkOrdenarPorSeq" CssClass="btn btn-xs btn-info" OnClick="lbkOrdenarPorSeq_Click" runat="server">Sequencia ↓</asp:LinkButton>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblnseq" runat="server" CssClass="label label-default" Text='<%# Eval("nseq") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha Conteo" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtfechaConteo" CssClass="form-control"  runat="server"   Text='<%# Bind("fechaIngresoCantidad","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Agregada" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtcantidadAgregada" Width="50" CssClass="form-control" runat="server" Text='<%# Bind("cantidadagregada") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Envio Corte" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtenviocorte" Width="50" CssClass="form-control" Enabled="false" runat="server" Text='<%# Bind("numenviocorte") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Envio Dia" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtenviodia" CssClass="form-control" Width="50" runat="server" Text='<%# Bind("enviodia") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="fechanvio" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="header_gv">
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbkOrdenarPorfechaEnvio" CssClass="btn btn-xs btn-info" OnClick="lbkOrdenarPorfechaEnvio_Click" runat="server">Fecha Envio ↓</asp:LinkButton>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtfechaenvio" CssClass="form-control" runat="server" Text='<%# Bind("fechaenvio","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="CheckBoxtodos" Text="Todos" AutoPostBack="true" OnCheckedChanged="CheckBoxtodos_CheckedChanged" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox" OnCheckedChanged="CheckBox_CheckedChanged" AutoPostBack="true" runat="server" />
                                    <asp:Button ID="btnActualizar" CommandName="actualizar" CssClass="btn btn-primary" runat="server" Text="Actualizar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblidhistorico" runat="server" Text='<%# Eval("idhistorico") %>'></asp:Label>
                                    <asp:Label ID="lblidenvio" runat="server" Text='<%# Eval("idenvio") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                </div>


            </div>
        </div>
    </div>
</asp:Content>

