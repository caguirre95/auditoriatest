﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReportBioProd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtdate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["dt"] = null;
        }
    }

    void bioIng()
    {
        try
        {
            if (drplanta.SelectedValue == "5")
            {
                var bihor1 = OrderDetailDA.Bihorario_allplantsProd(Convert.ToDateTime(txtdate.Text));

                if (bihor1.Rows.Count > 0)
                {
                    Session["dt"] = bihor1;
                    grdbiho.DataSource = Session["dt"];
                    grdbiho.DataBind();
                    grdbiho.GroupBy(grdbiho.Columns["Planta"]);
                    grdbiho.GroupBy(grdbiho.Columns["Line"]);
                }
            }
            else
            {
                var bihor1 = OrderDetailDA.BihorarioProd(Convert.ToDateTime(txtdate.Text), Convert.ToInt32(drplanta.SelectedValue));

                if (bihor1.Rows.Count > 0)
                {
                    Session["dt"] = bihor1;
                    grdbiho.DataSource = Session["dt"];
                    grdbiho.DataBind();
                    grdbiho.GroupBy(grdbiho.Columns["Line"]);
                }
            }
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void grdbiho_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "TotalBihorario")
        {
            int a1 = Convert.ToInt32(e.GetListSourceFieldValue("09:00am"));
            int a2 = Convert.ToInt32(e.GetListSourceFieldValue("11:00am"));
            int a3 = Convert.ToInt32(e.GetListSourceFieldValue("01:00pm"));
            int a4 = Convert.ToInt32(e.GetListSourceFieldValue("03:00pm"));
            int a5 = Convert.ToInt32(e.GetListSourceFieldValue("04:50pm"));
            int a6 = Convert.ToInt32(e.GetListSourceFieldValue("06:50pm"));                     
            e.Value = a1 + a2 + a3 + a4 + a5 + a6;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {

            bioIng();

            exportar.GridViewID = "grdbiho";
            exportar.FileName = "Reporte de Bihorario " + DateTime.Now;
            exportar.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            bioIng();
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void grdbiho_DataBinding(object sender, EventArgs e)
    {
        grdbiho.DataSource = Session["dt"];
    }

    protected void grdbiho_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdbiho.DataBind();
    }

    protected void btnsent_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Correo.aspx");
    }

    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportBioProd.aspx");
    }
}