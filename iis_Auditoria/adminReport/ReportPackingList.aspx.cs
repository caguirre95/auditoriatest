﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_ReportPackingList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.User.IsInRole("PackAdmon") || Page.User.IsInRole("Administrator") || Page.User.IsInRole("Reports"))
        {

        }
        else
        {
            Response.Redirect("../NotFound.aspx");
        }
    }

    //protected void imgpackwash_Click(object sender, ImageClickEventArgs e)
    //{
    //    Response.Redirect("/adminReport/PackingList.aspx");
    //}

    protected void lnkpackwash_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/PackingList.aspx");
    }

    protected void lnkpacknowash_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/PackingListNoWash.aspx");
    }

    //protected void imgpacknowash_Click(object sender, ImageClickEventArgs e)
    //{
    //    Response.Redirect("/adminReport/PackingListNoWash.aspx");
    //}

    protected void lnkplgeneral_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/PLgeneral.aspx");
    }
}