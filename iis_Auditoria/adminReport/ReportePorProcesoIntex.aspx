﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportePorProcesoIntex.aspx.cs" Inherits="adminReport_ReportePorProcesoIntex" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Procesos En Intex</div>
                <div class="panel-body">
                    <dx:ASPxGridView ID="grdproc" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdproc_DataBinding" OnCustomCallback="grdproc_CustomCallback">
                        <Columns>
                            <dx:GridViewBandColumn Caption="Reporte de Procesos" HeaderStyle-HorizontalAlign="Center">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="orden" Caption="Corte" CellStyle-BackColor="#00ccff">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="estilo" Caption="Estilo">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="color" Caption="Color">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="unidadesCorte" Caption="Unidades">
                                    </dx:GridViewDataTextColumn>                                    
                                    <dx:GridViewDataTextColumn FieldName="Deficit" Caption="Deficit">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Liberadas" Caption="Contadas">
                                    </dx:GridViewDataTextColumn>         
                                    <dx:GridViewDataTextColumn FieldName="Medida" Caption="Medidas">
                                    </dx:GridViewDataTextColumn> 
                                    <dx:GridViewDataTextColumn FieldName="ReprocesoLavado" Caption="Reproceso Lavado">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="ReprocesoPlancha" Caption=" Reproceso Plancha">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="ReprocesoMedida" Caption=" Reproceso Medida">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="HeatTransfer" Caption="Heat Transfer">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="EnProcesos" Caption="En Proceso">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Primera" Caption="Primera">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Irregulares" Caption="Irregulares">
                                    </dx:GridViewDataTextColumn>                                    
                                    <dx:GridViewDataTextColumn FieldName="SinProcesos" Caption="Deficit en Procesos">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:GridViewBandColumn>
                        </Columns>
                        <Settings VerticalScrollBarMode="Visible" HorizontalScrollBarMode="Visible" />
                    </dx:ASPxGridView>
                    <hr />  
                    <asp:Button ID="btnexport" runat="server" Text="Exportar" CssClass="btn btn-success form-control" OnClick="btnexport_Click" />
                    <dx:ASPxGridViewExporter ID="export" runat="server"></dx:ASPxGridViewExporter>
                </div>
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

