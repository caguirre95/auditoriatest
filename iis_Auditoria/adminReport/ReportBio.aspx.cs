﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReportBio : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtdate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtdate2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Carga();
            Session["dt"] = null;
        }
    }

    void Carga()
    {
        string queryCliente = "select Id_Cliente, Cliente from Cliente";
        DataTable qc = DataAccess.Get_DataTable(queryCliente);
        drpcliente.DataSource = qc;
        drpcliente.DataTextField = "Cliente";
        drpcliente.DataValueField = "Id_Cliente";
        drpcliente.DataBind();
        drpcliente.Items.Insert(0, "Select...");
    }

    void bioIng()
    {
        try
        {
            string id = rd1.SelectedValue;

            switch (id)
            {
                case "1":

                    if (drplanta.SelectedValue == "5")
                    {
                        var bihor1 = OrderDetailDA.Bihorario_allplants(Convert.ToDateTime(txtdate.Text));

                        if (bihor1.Rows.Count > 0)
                        {
                            #region
                            //DataColumn exc = new DataColumn("Exceso", typeof(string));
                            //bihor1.Columns.Add(exc);

                            //var ex = OrderDetailDA.BihorarioExceso(Convert.ToDateTime(txtdate.Text));

                            //for (int i = 0; i < ex.Rows.Count; i++)
                            //{
                            //    for (int j = 0; j < bihor1.Rows.Count; j++)
                            //    {
                            //        if (ex.Rows[i][0].ToString().Equals(bihor1.Rows[j][3].ToString()))
                            //        {
                            //            bihor1.Rows[j][11] = ex.Rows[i][1];
                            //        }
                            //    }
                            //}
                            #endregion

                            Session["dt"] = bihor1;
                            grdbiho.DataSource = Session["dt"];
                            grdbiho.DataBind();
                            grdbiho.GroupBy(grdbiho.Columns["Planta"]);
                            grdbiho.GroupBy(grdbiho.Columns["Line"]);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio('" + txtdate.Text + "');", true);
                            Session["dt"] = null;
                            grdbiho.DataSource = Session["dt"];
                            grdbiho.DataBind();
                        }
                    }
                    else
                    {
                        var bihor1 = OrderDetailDA.Bihorario(Convert.ToDateTime(txtdate.Text), Convert.ToInt32(drplanta.SelectedValue));

                        if (bihor1.Rows.Count > 0)
                        {
                            Session["dt"] = bihor1;
                            grdbiho.DataSource = Session["dt"];
                            grdbiho.DataBind();
                            grdbiho.GroupBy(grdbiho.Columns["Line"]);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio('" + txtdate.Text + "');", true);
                            Session["dt"] = null;
                            grdbiho.DataSource = Session["dt"];
                            grdbiho.DataBind();
                        }
                    }

                    break;

                case "2":

                    var bihorCliente = OrderDetailDA.BihorarioCliente(Convert.ToDateTime(txtdate.Text), Convert.ToDateTime(txtdate2.Text), int.Parse(drpcliente.SelectedValue));

                    if (bihorCliente.Rows.Count > 0)
                    {
                        Session["dt"] = bihorCliente;
                        grdbiho.DataSource = Session["dt"];
                        grdbiho.DataBind();
                        grdbiho.GroupBy(grdbiho.Columns["Planta"]);
                        grdbiho.GroupBy(grdbiho.Columns["Line"]);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio('" + txtdate.Text + "');", true);
                        Session["dt"] = null;
                        grdbiho.DataSource = Session["dt"];
                        grdbiho.DataBind();
                    }

                    break;

                case "3":

                    var bihorFechas = OrderDetailDA.BihorarioEntreFechas(Convert.ToDateTime(txtdate.Text), Convert.ToDateTime(txtdate2.Text));

                    if (bihorFechas.Rows.Count > 0)
                    {
                        Session["dt"] = bihorFechas;
                        grdbiho.DataSource = Session["dt"];
                        grdbiho.DataBind();
                        grdbiho.GroupBy(grdbiho.Columns["Planta"]);
                        grdbiho.GroupBy(grdbiho.Columns["Line"]);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio('" + txtdate.Text + "');", true);
                        Session["dt"] = null;
                        grdbiho.DataSource = Session["dt"];
                        grdbiho.DataBind();
                    }

                    break;

                default: break;
            }
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void grdbiho_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "TotalBihorario")
        {
            int a1 = Convert.ToInt32(e.GetListSourceFieldValue("09:00am"));
            int a2 = Convert.ToInt32(e.GetListSourceFieldValue("11:00am"));
            int a3 = Convert.ToInt32(e.GetListSourceFieldValue("01:00pm"));
            int a4 = Convert.ToInt32(e.GetListSourceFieldValue("03:00pm"));
            int a5 = Convert.ToInt32(e.GetListSourceFieldValue("04:50pm"));
            int a6 = Convert.ToInt32(e.GetListSourceFieldValue("06:50pm"));
            //decimal price = (decimal)e.GetListSourceFieldValue("UnitPrice");            
            e.Value = a1 + a2 + a3 + a4 + a5 + a6;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio1();", true);
            }
            else
            {
                bioIng();
            }
            exportar.GridViewID = "grdbiho";
            exportar.FileName = "Reporte de Bihorario " + DateTime.Now;
            exportar.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio1();", true);
            }
            else
            {
                bioIng();
            }
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void grdbiho_DataBinding(object sender, EventArgs e)
    {
        try
        {
            if (Session["dt"] != null)
            {
                grdbiho.DataSource = Session["dt"];
                //grdbiho.GroupBy(grdbiho.Columns["Planta"]);
                //grdbiho.GroupBy(grdbiho.Columns["Line"]);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio('" + txtdate.Text + "');", true);
            }
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void grdbiho_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdbiho.DataBind();
    }

    protected void btnsent_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Correo.aspx");
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportBio.aspx");
    }
}