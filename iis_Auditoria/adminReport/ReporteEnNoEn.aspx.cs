﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class adminReport_ReporteEnNoEn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            fecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["dt"] = null;
            cargacombo();
        }
    }

    void cargacombo()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta, descripcion FROM Planta order by descripcion asc";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");

        string sqlcli = "select Id_Cliente, Cliente from Cliente where Estado = 1";

        DataTable dtcli = DataAccess.Get_DataTable(sqlcli);

        drpcli.DataSource = dtcli;
        drpcli.DataTextField = "Cliente";
        drpcli.DataValueField = "Id_Cliente";
        drpcli.DataBind();
        drpcli.Items.Insert(0, "Select...");
    }

    protected void rd1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string id = rd1.SelectedValue;

            switch (id)
            {
                case "1"://Planta
                    panelp.Visible = true;
                    panell.Visible = false;
                    panelc.Visible = false;                    
                    panelcl.Visible = false;
                    panelfec.Visible = false;
                    break;

                case "2"://Linea
                    panelp.Visible = false;
                    panell.Visible = true;
                    panelc.Visible = false;                   
                    panelcl.Visible = false;
                    panelfec.Visible = false;
                    break;

                case "3"://Corte
                    panelp.Visible = false;
                    panell.Visible = false;
                    panelc.Visible = true;                    
                    panelcl.Visible = false;
                    panelfec.Visible = false;
                    break;

                case "4"://Fecha
                    panelp.Visible = false;
                    panell.Visible = false;
                    panelc.Visible = false;                    
                    panelcl.Visible = false;
                    panelfec.Visible = true;
                    break;

                case "5"://Cliente
                    panelp.Visible = false;
                    panell.Visible = false;
                    panelc.Visible = false;                    
                    panelcl.Visible = true;
                    panelfec.Visible = false;
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    void carga()
    {
        string r = rd1.SelectedValue;

        switch (r)
        {
            case "1":
                var a = OrderDetailDA.Contado_Envi_NoEnvi_plan(Convert.ToInt16(DropDownListPlant.SelectedValue));
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                gridsent.GroupBy(gridsent.Columns["Planta"]);
                gridsent.GroupBy(gridsent.Columns["Linea"]);
                break;

            case "2":
                var b = OrderDetailDA.Contado_Envi_NoEnvi_Linea(Convert.ToInt16(DropDownListLine.SelectedValue));
                Session["dt"] = b;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                gridsent.GroupBy(gridsent.Columns["Planta"]);
                gridsent.GroupBy(gridsent.Columns["Linea"]);
                break;

            case "3":
                var c = OrderDetailDA.Contado_Envi_NoEnvi_Corte(Convert.ToInt32(hdnidporder.Value));
                Session["dt"] = c;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                gridsent.GroupBy(gridsent.Columns["Planta"]);
                gridsent.GroupBy(gridsent.Columns["Linea"]);
                break;

            case "4":
                var d = OrderDetailDA.Contado_Envi_NoEnvi(Convert.ToDateTime(fecha1.Text), Convert.ToDateTime(fecha2.Text));
                Session["dt"] = d;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                gridsent.GroupBy(gridsent.Columns["Planta"]);
                gridsent.GroupBy(gridsent.Columns["Linea"]);
                break;

            case "5":
                var e = OrderDetailDA.Contado_Envi_NoEnvi_cliente(Convert.ToInt16(drpcli.SelectedValue));
                Session["dt"] = e;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                gridsent.GroupBy(gridsent.Columns["Planta"]);
                gridsent.GroupBy(gridsent.Columns["Linea"]);
                break;

            default:
                break;

        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            carga();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            export.GridViewID = "gridsent";
            export.FileName = "Reporte Send Not Send " + DateTime.Now;
            export.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void gridsent_DataBinding(object sender, EventArgs e)
    {
        try
        {
            gridsent.DataSource = Session["dt"];
            gridsent.GroupBy(gridsent.Columns["Planta"]);
            gridsent.GroupBy(gridsent.Columns["Linea"]);
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void gridsent_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridsent.DataBind();
    }

    protected void btnlim_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReporteEnNoEn.aspx");
    }
}