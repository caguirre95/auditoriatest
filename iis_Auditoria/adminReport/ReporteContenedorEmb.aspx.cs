﻿using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_ReporteContenedorEmb : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["ContenedorR"] = null;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ContenedorEmb> GetContenedor(string pre)
    {
        try
        {
            List<ContenedorEmb> list = new List<ContenedorEmb>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top 10 Id_Contenedor, Contenedor from tbContenedorEmb where Contenedor like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ContenedorEmb obj = new ContenedorEmb();

                obj.Id_contenedor = int.Parse(dt.Rows[i][0].ToString());
                obj.Contenedor = dt.Rows[i][1].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class ContenedorEmb
    {
        public int Id_contenedor { get; set; }
        public string Contenedor { get; set; }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdfcontenedor.Value != "" && txtcontenedor.Text != "")
            {                
                data();
            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;
        }
    }

    void data()
    {
        Session["ContenedorR"] = null;

        var dt = OrderDetailDA.Reporte_ContenedorEmbarque(Convert.ToInt32(hdfcontenedor.Value));

        Session["ContenedorR"] = dt;

        grdrptCont.DataBind();

        grdrptCont.ExpandAll();
    }

    void DataCortesCompletos()
    {
        Session["ContenedorR"] = null;

        var dt = OrderDetailDA.Reporte_ContenedorEmbarqueCortesCompletos(Convert.ToInt32(hdfcontenedor.Value));

        Session["ContenedorR"] = dt;

        grdrptCont.DataBind();

        grdrptCont.ExpandAll();
    }

    protected void grdrptCont_DataBinding(object sender, EventArgs e)
    {
        grdrptCont.DataSource = Session["ContenedorR"];
        grdrptCont.GroupBy(grdrptCont.Columns["Contenedor"]);
    }

    protected void grdrptCont_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdrptCont.DataBind();
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {

        System.Globalization.CultureInfo norwCulture = System.Globalization.CultureInfo.CreateSpecificCulture("es");
        System.Globalization.Calendar cal = norwCulture.Calendar;
        int x = cal.GetWeekOfYear(DateTime.Now, norwCulture.DateTimeFormat.CalendarWeekRule, norwCulture.DateTimeFormat.FirstDayOfWeek);

        exportar1.GridViewID = "grdrptCont";

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link1.Component = exportar1;


        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link1 });

        compositeLink.CreateDocument();

        using (MemoryStream stream = new MemoryStream())
        {
            XlsxExportOptions options = new XlsxExportOptions();

            options.ExportMode = XlsxExportMode.SingleFile;
            options.SheetName = "Week: " + x;
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=Summary.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        ps.Dispose();

    }

    protected void btnlimp_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReporteContenedorEmb.aspx");
    }

    protected void btncortecompl_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdfcontenedor.Value != "" && txtcontenedor.Text != "")
            {
                DataCortesCompletos();
            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;            
        }
    }
}