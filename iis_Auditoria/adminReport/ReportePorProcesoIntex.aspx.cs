﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_ReportePorProcesoIntex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["proc"] = null;
            Reporte();
        }
    }

    void Reporte()
    {
        try
        {
            var consulta = new DataTable();
            string sqlquery = "";

            sqlquery = " select a.orden, a.estilo, a.unidadesCorte, a.color, a.Liberadas, (a.Liberadas - a.unidadesCorte) Deficit, b.EnProcesos,"
                     + " a.Liberadas - b.EnProcesos 'SinProcesos', c.primera Primera, c.irregular Irregulares, h.Medida, e.ReprocesoLavado, f.ReprocesoPlancha, g.ReprocesoMedida, d.HeatTransfer from ("
                     + " select orden, estilo, unidadesCorte, color, SUM(unidad) Liberadas from InventarioMedidas im join inventarioDetalleUnidades idu on idu.corte = im.orden "
                     + " group by orden, estilo, unidadesCorte, color) a join (select corte, SUM(unidad) 'EnProcesos' from inventarioDetalleUnidades where ISNULL(idMedidaDL, 0) != 0 group by corte"
                     + " ) b on a.orden = b.corte join (select corte, primera, irregular from (select corte, estadoCalidad, COUNT(estadoCalidad) Estado from inventarioDetalleUnidades"
                     + " where ISNULL(idMedidaDL, 0) != 0 group by corte, estadoCalidad) as st pivot(SUM(Estado) for estadoCalidad in ([primera],[irregular])) as pvt) c on a.orden = c.corte join"
                     + " (select corte, SUM(unidad) HeatTransfer from inventarioDetalleUnidades where ISNULL(idMedidaDL, 0) != 0 and procesoActual = 'heat' group by corte) d on a.orden = d.corte join"
                     + " (select corte, SUM(unidad) ReprocesoLavado from inventarioDetalleUnidades where ISNULL(idMedidaDL, 0) != 0 and procesoActual = 'replav' group by corte) e on a.orden = e.corte join"
                     + " (select corte, SUM(unidad) ReprocesoPlancha from inventarioDetalleUnidades where ISNULL(idMedidaDL, 0) != 0 and procesoActual = 'reppla' group by corte) f on a.orden = f.corte join"
                     + " (select corte, SUM(unidad) ReprocesoMedida from inventarioDetalleUnidades where ISNULL(idMedidaDL, 0) != 0 and(procesoActual = 'esperaHeat' or procesoActual = 'repmed2') group by corte"
                     + " ) g on a.orden = g.corte join (select corte, SUM(unidad) Medida from inventarioDetalleUnidades where ISNULL(idMedidaDL, 0) != 0 and procesoActual = 'med' group by corte"
                     + " ) h on a.orden = h.corte";

            consulta = DataAccess.Get_DataTable(sqlquery);
            Session["proc"] = consulta;
            grdproc.DataSource = Session["proc"];
            grdproc.DataBind();
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        export.GridViewID = "grdproc";        

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link3 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link3.Component = export;        

        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link3 });

        compositeLink.CreatePageForEachLink();

        using (MemoryStream stream = new MemoryStream())
        {
            XlsxExportOptions options = new XlsxExportOptions();
            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
            options.SheetName = "ProcesoIntex";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=ProcIntex" + ".xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }
        ps.Dispose();
    }

    protected void grdproc_DataBinding(object sender, EventArgs e)
    {
        grdproc.DataSource = Session["proc"];
    }

    protected void grdproc_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdproc.DataBind();
    }
}