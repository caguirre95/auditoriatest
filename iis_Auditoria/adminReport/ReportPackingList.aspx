﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportPackingList.aspx.cs" Inherits="adminReport_ReportPackingList" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <style type="text/css">
        .box_effect {
            height: 250px;
            width: 180px;
            padding-top: 15px;
            text-align: center;
            border-width: 160px;
            margin: auto;
            margin-bottom: 15px !important;
            opacity: 0.8;
            border: 1px solid rgba(0,0,0,0.2);
        }

        .te {
            text-decoration: none;
        }

        .centrar {
            text-align: center;
        }

        .mt {
            margin-top: 10px;
        }


        .mb {
            margin-bottom: 10px;
        }

        .te:hover {
            color: white !important;
            text-decoration: none;
            outline-style: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container mt">

        <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control mb"></asp:SiteMapPath>

        <div class="col-lg-12">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4  ">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="lnkpackwash_Click" CssClass="te">
                 <div>                        
                            <img src="/img/Wash.png" alt="Inspection" height="128" width="128" class="mt" />
                        </div>
                

                <div style="margin-top:20px">
                            <span style="font-weight: bold;">Packing List Wash </span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4  ">
                <div class="box_effect">

                    <asp:LinkButton ID="LinkButton2" runat="server" OnClick="lnkpacknowash_Click" CssClass="te">
                <div>                        
                            <img src="/img/NoWash.png" alt="Inspection"  height="128" width="128" class="mt" />
                        </div>
                <div style="margin-top:20px">
                            <span style="font-weight: bold;">Packing List No Wash </span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4  ">
                <div class="box_effect">

                    <asp:LinkButton ID="LinkButton3" runat="server" OnClick="lnkplgeneral_Click" CssClass="te">
                <div>                        
                            <img src="/img/biho.png" alt="Inspection"  height="128" width="128" class="mt" />
                        </div>
                <div style="margin-top:20px">
                            <span style="font-weight: bold;">Packing List General </span>
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
        </div>

    </div>

</asp:Content>

