﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContenedorEmb.aspx.cs" Inherits="adminReport_ContenedorEmb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <script lang="javascript" type="text/javascript">

        $(function () {
            $('#<%=txtcut.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcut.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcut.ClientID%>').val(ui.item.porder);
                    $('#<%=hdfcut.ClientID%>').val(ui.item.idorder);
                   <%-- $('#<%=txtStyle.ClientID%>').val(ui.item.idorder);--%>
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        })
    </script>

    <script lang="javascript" type="text/javascript">
       function verificar() {
            var codOrden = $('#<%=txtcont.ClientID%>').val();
            //  alert(codOrden.length)
            if (codOrden.length > 4) {
                $.ajax({
                    url: "ContenedorEmb.aspx/VerificarCod",
                    type: "POST",
                    data: "{'codOrden' :'" + codOrden + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "true") {
                            divElem.innerHTML = " <strong> El codigo de Contenedor ya se encuentra </strong>";
                            divElem.className = "alert alert-success";
                            divElem.style.display = "block";

                        } else if (data.d == "false") {
                            divElem.innerHTML = "";
                            divElem.className = "";
                            divElem.style.display = "none";
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        divElem.innerHTML = "  <strong>  Error de Servidor al Guardar </strong>";
                        divElem.className = "alert alert-warning  ";
                        divElem.style.display = "block";
                    }
                })
            }
            else {
                divElem.innerHTML = "";
                divElem.className = "";
                divElem.style.display = "none";
            }
        }
    </script>

    <script type="text/javascript">
        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
    </script>

    <script type="text/javascript">
        function verificarCut(corte, contenedor) {
            swal({
                title: '!Información!',
                text: 'El Corte : ' + corte + ' ,esta Asignado al Contenedor : ' + contenedor,
                type: 'success'
            });
        }

         function verificarContene(contenedor) {
            swal({
                title: '!Información!',
                text: 'El Contenedor : ' + contenedor +' ya existe en la base de datos',
                type: 'success'
            });
        }
    </script>

    <style>
        .sp {
            margin-top: 20px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading"><strong>Crear Contenedor</strong></div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <strong>Contenedor</strong>
                        <asp:TextBox ID="txtcont" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Digite Contenedor" AutoComplete="off"></asp:TextBox>
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <strong>Corte</strong>
                        <asp:TextBox ID="txtcut" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Digite Corte"></asp:TextBox>
                        <asp:HiddenField ID="hdfcut" runat="server" />
                        <%--<asp:TextBox ID="txtStyle" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Estilo" Enabled="false"></asp:TextBox>--%>
                    </div>

                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 sp">
                        <asp:Button ID="btnagregar" runat="server" CssClass="btn btn-info form-control" Text="Agregar Corte" OnClick="btnagregar_Click" />
                    </div>

                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 sp">
                        <asp:Button ID="btnlimp" runat="server" CssClass="btn btn-success form-control" Text="Limpiar Pantalla" OnClick="btnlimp_Click" />
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sp">
                        <asp:Button ID="btnsave" runat="server" CssClass="btn btn-primary" Text="Guardar Contenedor" OnClick="btnsave_Click" />

                        <asp:Button ID="btneditar" runat="server" CssClass="btn btn-warning" Text="Editar Contenedor" OnClick="btneditar_Click" />
                        <hr />
                        <div class="badge marcontrol-bottom">
                            <div class="input-group">
                                Cortes Agregados 
                            <asp:Label ID="lblcnt" runat="server" Text="" CssClass="badge"></asp:Label>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sp">
                        <asp:GridView ID="grdcortes" runat="server" CssClass="table table-hover table-striped" HeaderStyle-BackColor="#337ab7"
                            HeaderStyle-ForeColor="White" Width="100%" GridLines="None" DataKeyNames="Id_Order" AutoGenerateColumns="False" OnRowCommand="grdcortes_RowCommand">
                            <Columns>
                                <asp:BoundField ControlStyle-CssClass="label label-success" DataField="Id_Order" HeaderText="Id"></asp:BoundField>
                                <asp:BoundField ControlStyle-CssClass="label label-success" DataField="POrder" HeaderText="Corte"></asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton3" CssClass="btn btn-danger" CommandName="Eliminar" runat="server" ToolTip="Eliminar Registro"><span class="glyphicon glyphicon-remove"></span></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>

                </div>
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

