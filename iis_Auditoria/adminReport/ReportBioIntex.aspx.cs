﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Intex_ReportBioIntex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtdate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["a"] = null;
        }
    }

    void bioIng()
    {
        try
        {
            var bihor1 = OrderDetailDA.BihorarioIntex(Convert.ToDateTime(txtdate.Text));           

            if (bihor1.Rows.Count > 0)
            {
                #region
                //DataColumn def = new DataColumn("Deficit", typeof(string));
                //bihor1.Columns.Add(def);
                //var df = OrderDetailDA.BihorarioDeficitIntex(Convert.ToDateTime(txtdate.Text));
                //#region
                ////string d = " select p.POrder,"
                ////         + " (isnull(sum(he.cantidadagregada),0) - p.Quantity) as Deficit"
                ////         + " from tblhistoricoenvioIntex he"
                ////         + " join tblEnviosbultoIntex eb on eb.idEnvio = he.idEnvio"
                ////         + " left join Bundle b on b.Id_Bundle = eb.idBulto"
                ////         + " join POrder p on p.Id_Order = b.Id_Order"
                ////         + " join Linea l on l.id_linea = p.Id_Linea"
                ////         + " where isnull(he.idBihorario,0) != 0"
                ////         + " group by p.POrder,l.numero, p.Quantity";
                ////DataTable df = DataAccess.Get_DataTable(d);
                //#endregion
                //for (int i = 0; i < df.Rows.Count; i++)
                //{
                //    for (int j = 0; j < bihor1.Rows.Count; j++)
                //    {
                //        if (df.Rows[i][0].ToString().Equals(bihor1.Rows[j][2].ToString()))
                //        {
                //            bihor1.Rows[j][9] = df.Rows[i][1];
                //        }
                //    }
                //}
                #endregion
                Session["a"] = bihor1;
                grdbiho.DataSource = Session["a"];
                grdbiho.DataBind();
                grdbiho.GroupBy(grdbiho.Columns["Line"]);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio('" + txtdate.Text + "');", true);
                Session["a"] = null;
                grdbiho.DataSource = Session["a"];
                grdbiho.DataBind();
            }
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void grdbiho_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "TotalBihorario")
        {
            int a1 = Convert.ToInt32(e.GetListSourceFieldValue("09:00am"));
            int a2 = Convert.ToInt32(e.GetListSourceFieldValue("11:00am"));
            int a3 = Convert.ToInt32(e.GetListSourceFieldValue("01:00pm"));
            int a4 = Convert.ToInt32(e.GetListSourceFieldValue("03:00pm"));
            int a5 = Convert.ToInt32(e.GetListSourceFieldValue("04:50pm"));
            int a6 = Convert.ToInt32(e.GetListSourceFieldValue("06:50pm"));
            e.Value = a1 + a2 + a3 + a4 + a5 + a6;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio1();", true);
            }
            else
            {
                bioIng();
            }
            exportar.GridViewID = "grdbiho";
            exportar.FileName = "Reporte de Bihorario Intex " + DateTime.Now;
            exportar.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }

    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio1();", true);
            }
            else
            {
                bioIng();
            }
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void grdbiho_DataBinding(object sender, EventArgs e)
    {
        try
        {
            grdbiho.DataSource = Session["a"];
            grdbiho.GroupBy(grdbiho.Columns["Line"]);
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void grdbiho_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdbiho.DataBind();
    }
}