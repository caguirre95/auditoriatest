﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteIntexInv.aspx.cs" Inherits="adminReport_ReporteIntexInv" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../adminReport/ReporteIntexInv.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porderclient: item.porderclient,
                                    estilo: item.estilo,
                                    totalcorte: item.totalcorte,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porderclient);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porderclient);                    
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porderclient + "</a>").appendTo(ul);
            };
        })
    </script>

     <script type="text/javascript">  

        function LimpiarFocus() {
            limpiar();
        }

        function limpiar() {

            $('#<%=txtcorte.ClientID%>').val('');          

        }

    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Reporte Procesos Intex</strong></div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                   
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Controles</div>
                            <div class="panel-body">
                                <span style="font-weight:bold">CORTE</span>
                                <asp:TextBox ID="txtcorte" runat="server" onfocus="LimpiarFocus()" placeholder="Ingrese el Corte..." AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>

                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control MTop" Text="Generar" OnClick="btngenerar_Click" />

                                <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control MTop" Text="Pass to excel" OnClick="btnexcel_Click" />

                                <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-warning form-control MTop" Text="Limpiar" OnClick="btnlimpiar_Click" />

                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <hr />
                        <dx:ASPxGridView ID="grdinvintex" runat="server" KeyFieldName="id" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="true"
                            OnDataBinding="grdinvintex_DataBinding" OnCustomCallback="grdinvintex_CustomCallback">
                            <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" /> 
                            <SettingsSearchPanel Visible="true" />
                            <Columns>
                                <dx:GridViewBandColumn>
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="INVENTARIO INTEX" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="id" Caption="Id" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Corte" Caption="PO" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Estilo" Caption="Estilo" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Quantity" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Color" Caption="Color" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="EnviodePlanta" Caption="Enviado de Plantas" VisibleIndex="5">
                                                </dx:GridViewDataTextColumn>                                                                                                
                                                <dx:GridViewDataTextColumn FieldName="unidadesSecadas" Caption="Unidades Secadas" VisibleIndex="9">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="deficitSecado" Caption="Deficit Secado" VisibleIndex="10">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="EstatusSecado" Caption="Estatus" VisibleIndex="11">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="EnviosecadoMedida" Caption="Unidades Enviadas a Medida" VisibleIndex="12">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="SalidaIntexAEmpaque" Caption="Unidades Enviadas a Empaque" VisibleIndex="13">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="InventarioIntex" Caption="Inventario" VisibleIndex="14">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" />
                            <Settings ShowFooter="true" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
                            <SettingsPager PageSize="150" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="Corte" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="InventarioIntex" SummaryType="Sum" DisplayFormat="Totales : {0}" />
                                <dx:ASPxSummaryItem FieldName="unidadesSecadas" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="EnviosecadoMedida" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="SalidaIntexAEmpaque" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <GroupSummary>
                                <dx:ASPxSummaryItem FieldName="Corte" ShowInGroupFooterColumn="Corte" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="InventarioIntex" ShowInGroupFooterColumn="InventarioIntex" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="unidadesSecadas" ShowInGroupFooterColumn="unidadesSecadas" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="EnviosecadoMedida" ShowInGroupFooterColumn="EnviosecadoMedida" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="SalidaIntexAEmpaque" ShowInGroupFooterColumn="SalidaIntexAEmpaque" SummaryType="Sum" DisplayFormat="{0}" />
                            </GroupSummary>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar" runat="server"></dx:ASPxGridViewExporter>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

