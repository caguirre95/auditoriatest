﻿using DevExpress.Web;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Intex_ReportWeekIntex : System.Web.UI.Page
{
    class ClassArrm
    {
        public int id { get; set; }
        public string semana { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Session["dt"] = null;

                int x = System.Globalization.CultureInfo.CurrentUICulture.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstDay, DateTime.Now.DayOfWeek);

                var list = new List<ClassArrm>();
                for (int i = 1; i <= x; i++)
                {
                    ClassArrm obj = new ClassArrm();
                    obj.id = i;
                    obj.semana = "Semana " + i;
                    list.Add(obj);
                }

                drpSemana.DataSource = list;
                drpSemana.DataTextField = "semana";
                drpSemana.DataValueField = "id";
                drpSemana.DataBind();

                drpSemana.SelectedValue = (x).ToString();

                getgrid();


            }
        }
        catch (Exception)
        {

            throw;
        }
    }
    protected void grid_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grid.DataBind();
        ASPxLabel l1 = (ASPxLabel)grid.FindHeaderTemplateControl((GridViewBandColumn)grid.Columns["ccc"], "ASPxLabelfecha");
        ASPxLabel l3 = (ASPxLabel)grid.FindHeaderTemplateControl((GridViewBandColumn)grid.Columns["week"], "ASPxLabelweek");
        l3.Text = DateTime.Now.ToString("yyyy-mm-dd");
        l1.Text = DateTime.Now.ToString("yyyy-mm-dd");
    }
    protected void grid_DataBinding(object sender, EventArgs e)
    {
        grid.DataSource = Session["dt"];


        ASPxLabel l2 = (ASPxLabel)grid.FindHeaderTemplateControl((GridViewBandColumn)grid.Columns["fff"], "ASPxLabelfechaLong");
        //ASPxLabel l3 = (ASPxLabel)grid.FindHeaderTemplateControl((GridViewBandColumn)grid.Columns["week"], "ASPxLabelweek");

        //l1.Text = "Fecha de Creaciòn: " + DateTime.Now.ToString("y");
        l2.Text = DateTime.Now.ToLongDateString();
        //l3.Text = "Week: " + drpSemana.SelectedValue;
    }
    protected void drpSemana_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (drpSemana.Items.Count > 0)
            {
                getgrid();

            }
        }
        catch (Exception)
        {

            throw;
        }

    }
    void getgrid()
    {
        var dt = OrderDetailDA.getreportBioEnvioIntex(int.Parse(drpSemana.SelectedValue));

        Session["dt"] = dt;
        grid.DataBind();

    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        //ASPxGridViewExporter1.GridViewID = "grid";
        //ASPxGridViewExporter1.FileName = "Cancelacion de Cortes ";
        //ASPxGridViewExporter1.WriteXlsxToResponse();


        var dt = OrderDetailDA.getreportBioEnvioIntex(int.Parse(drpSemana.SelectedValue));

        Session["dt"] = dt;
        grid.DataBind();

        //ASPxLabel l1 = (ASPxLabel)grid.FindHeaderTemplateControl((GridViewBandColumn)grid.Columns["ccc"], "ASPxLabelfecha");
        //ASPxLabel l2 = (ASPxLabel)grid.FindHeaderTemplateControl((GridViewBandColumn)grid.Columns["fff"], "ASPxLabelfechaLong");
        //ASPxLabel l3 = (ASPxLabel)grid.FindHeaderTemplateControl((GridViewBandColumn)grid.Columns["week"], "ASPxLabelweek");

        //l1.Text = "Fecha de Creaciòn: " + DateTime.Now.ToString("y");
        //l2.Text = DateTime.Now.ToLongDateString();
        //l3.Text = "Week: " + drpSemana.SelectedValue;

        ASPxGridViewExporter1.GridViewID = "grid";

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link1.Component = ASPxGridViewExporter1;


        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link1 });

        compositeLink.CreatePageForEachLink();

        using (MemoryStream stream = new MemoryStream())
        {
            //XlsExportOptionsEx xlsExportOptionsEx = new XlsExportOptionsEx();

            //XlsExportOptionsEx exportOptions = new XlsExportOptionsEx();
            //exportOptions.CustomizeSheetHeader += options_CustomizeSheetHeader;

            //return GridViewExtension.ExportToXls(GridViewHelper.ExportGridViewSettings, model, exportOptions);

            XlsxExportOptions options = new XlsxExportOptions();

            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
            options.SheetName = "Cancelacion de Corte";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=Cancelacion de Cortes.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }
        ps.Dispose();
    }

    protected void grid_Load(object sender, EventArgs e)
    {

        ASPxLabel l1 = (ASPxLabel)grid.FindHeaderTemplateControl((GridViewBandColumn)grid.Columns["ccc"], "ASPxLabelfecha");
        ASPxLabel l2 = (ASPxLabel)grid.FindHeaderTemplateControl((GridViewBandColumn)grid.Columns["fff"], "ASPxLabelfechaLong");
        ASPxLabel l3 = (ASPxLabel)grid.FindHeaderTemplateControl((GridViewBandColumn)grid.Columns["week"], "ASPxLabelweek");

        l1.Text = "Fecha de Creaciòn: " + DateTime.Now.ToString("y");
        l2.Text = DateTime.Now.ToLongDateString();
        l3.Text = "Week: " + drpSemana.SelectedValue;
    }


}