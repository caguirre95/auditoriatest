﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReparacionSecadoIntex.aspx.cs" Inherits="adminReport_ReparacionSecadoIntex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../SalidaSecadora.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porderclient: item.porderclient,
                                    estilo: item.estilo,
                                    totalcorte: item.totalcorte,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porderclient);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porderclient);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porderclient + "</a>").appendTo(ul);
            };
        })
    </script>

    <script>
        function alertDinamica(mes, tipo) {
            swal({
                title: mes,
                type: tipo
            });
        }

        function alertDinamica2(mes, text, tipo) {
            swal({
                title: mes,
                text: text,
                type: tipo
            });
        }

        function alertExitoso() {
            swal({
                title: 'Exito!',
                timer: 1000,
                type: 'success'
            });
        }
        function alertExitosoElim() {
            swal({
                title: 'Exito, Registro Eliminado!',
                timer: 1000,
                type: 'info'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }
        function alertincompleto() {
            swal({
                title: 'Campos Vacios, llenelos Correctamente !',
                timer: 1000,
                type: 'info'
            });
        }
        function alertClave(r) {
            swal({
                title: 'Ocurrio un evento Inesperado, intentelo Nuevamente ! ' + r,
                timer: 5000,
                type: 'warning'
            });
        }        

        function limpiar(ee) {
            ee.value = '';
        }

        function limpiarauto() {
            $('#<%=txtPorder.ClientID%>').val('');
        }
    </script>

    <script type="text/javascript">
        function num(e) {
            evt = e ? e : event;
            tcl = (window.Event) ? evt.which : evt.keyCode;
            if ((tcl < 48 || tcl > 57) && (tcl != 8 && tcl != 0 && tcl != 46)) {
                return false;
            }
            return true;
        }
    </script>

    <style>
        .m {
            margin-left:20px;
        }
    </style>

    <script src="../jquery-ui-1.12.1.custom/external/jquery/jquery.js"></script>
    <script src="../jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <link href="../jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading">Reparacion Secado</div>
            <div class="panel-body">

                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">Filtros</div>
                        <div class="panel-body">

                            <div class="form-group">
                                Corte:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" CssClass="btn btn-default" ID="LtnMostrar" OnClick="LtnMostrar_Click" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Corte..." onfocus="limpiarauto();" required="true" AutoCompleteType="Disabled" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-info">
                        <div class="panel-heading">Comando</div>
                        <div class="panel-body">
                            <asp:Button ID="btnActualizarSecado" runat="server" CssClass="btn btn-warning btn-block " Text="Eliminar Bultos Check" OnClick="btnActualizarSecado_Click" OnClientClick="javascript:return confirm('Esta seguro que desea eliminar los Bultos Seleccionados?');" />
                            <br />
                            <asp:Button ID="btnEliminarsecado" runat="server" CssClass="btn btn-danger btn-block" Text="Eliminar Corte Completo" OnClick="btnEliminarsecado_Click" OnClientClick="javascript:return confirm('Esta seguro que desea eliminar el Corte?');" />
                            <br />
                            <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-info btn-block" Text="Limpiar" OnClick="btnlimpiar_Click" />
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <hr />
                    <asp:GridView ID="gridEnvios" runat="server" HeaderStyle-BackColor="#337ab7" HeaderStyle-ForeColor="White" DataKeyNames="idSecadoDetalle" CssClass="table-responsive table-hover table"
                        AutoGenerateColumns="false" GridLines="None" OnRowCommand="gridEnvios_RowCommand">
                        <Columns>

                            <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                <HeaderTemplate>
                                    No.
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblrn" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField Visible="false" ItemStyle-CssClass="padd_t">
                                <HeaderTemplate>
                                    ID
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblid" runat="server" Font-Size="Medium" CssClass="label label-primary" Text='<%#Bind("idSecadoDetalle") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                <HeaderTemplate>
                                    CORTE 
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblcorte" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("corteCompleto") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    UNIDADES SECADAS
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtunidades" Enabled="false" Width="100px" Font-Size="Medium" CssClass="form-control" Text='<%#Bind("unidadesSecadas") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField Visible="true" ItemStyle-CssClass="padd_t">
                                <HeaderTemplate>
                                    FECHA INGRESO
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblcantidad" runat="server" Font-Size="Medium" CssClass="label label-default" Text='<%#Bind("Fecha","{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    ACTUALIZAR UNIDADES 
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtunits" Font-Size="Medium" CssClass="form-control" AutoCompleteType="Disabled" onkeypress="return num(event);" MaxLength="4" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton Text="" ID="lnkupd" CssClass="btn btn-info" runat="server" CommandName="Actualizar" ToolTip="Update"><i class="glyphicon glyphicon-indent-left"> </i> </asp:LinkButton>                                    
                                    <asp:CheckBox ID="chk1" runat="server" Text="Eliminar" Checked="false" />
                                    <asp:LinkButton Text="" ID="lnkdel" CssClass="btn btn-danger m" runat="server" CommandName="Eliminar" ToolTip="Delete" OnClientClick="javascript:return confirm('Esta seguro que desea eliminar el bulto contado?');"><i class="glyphicon glyphicon-object-align-bottom"> </i> </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

