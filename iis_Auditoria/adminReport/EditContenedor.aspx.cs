﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_EditContenedor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string user = Page.User.Identity.Name.ToLower();

            if (user == "adminsnr")
            {
                Session["Contenedor1"] = null;
            }
            else
            {
                FormsAuthentication.SignOut();
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
    }

    protected void btnagregar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtcontenedor.Text != "" && txtcut.Text != "" && hdfcut.Value != "" && hdfcontenedor.Value != "")
            {
                string query1 = "select Estado from tbContenedorEmb where Id_Contenedor = " + hdfcontenedor.Value;

                DataTable dt1 = DataAccess.Get_DataTable(query1);

                string estado = dt1.Rows[0][0].ToString();

                if (estado.ToLower() == "false")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "cerrarConVerf();", true);
                    txtcut.Text = "";
                    hdfcut.Value = "";
                }
                else
                {
                    string query = "select p.POrder, ce.Contenedor from POrder p join tb_ContenedorPO cp on cp.Id_Order = p.Id_Order join tbContenedorEmb ce on ce.Id_Contenedor = cp.Id_Contenedor where p.Id_Order = " + hdfcut.Value;

                    DataTable dt = DataAccess.Get_DataTable(query);

                    if (dt.Rows.Count <= 0)
                    {
                        List<POCont> lista = new List<POCont>();

                        if (Session["Contenedor1"] != null)
                        {
                            lista = (List<POCont>)Session["Contenedor1"];
                        }

                        POCont obj = new POCont();

                        obj.Id_Order = Convert.ToInt32(hdfcut.Value);
                        obj.POrder = txtcut.Text;

                        var seleccion = lista.Where(a => a.Id_Order == obj.Id_Order).Select(a => a).Where(at => at.POrder == obj.POrder).ToList();

                        if (seleccion.Count > 0)
                        {
                            Limpiar();
                        }
                        else
                        {
                            lista.Add(obj);
                            lblcnt.Text = lista.Count.ToString();
                            Session["Contenedor1"] = lista;
                            Cargargrid();
                            string ok = OrderDetailDA.GuardarDetalleCont(Convert.ToInt32(hdfcontenedor.Value), Convert.ToInt32(hdfcut.Value));
                            if (ok == "OK")
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "Actualizar();", true);
                            }
                            Limpiar();
                        }
                    }
                    else
                    {
                        string corte = dt.Rows[0][0].ToString();
                        string contenedor = dt.Rows[0][1].ToString();
                        txtcut.Text = "";
                        hdfcut.Value = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "verificarCut('" + corte + "','" + contenedor + "');", true);
                    }
                }
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtcontenedor.Text != "" && hdfcontenedor.Value != "")
            {
                string query = "select p.Id_Order, p.POrder from POrder p join tb_ContenedorPO cp on cp.Id_Order = p.Id_Order where cp.Id_Contenedor = " + hdfcontenedor.Value + " order by p.POrder";

                DataTable dt = DataAccess.Get_DataTable(query);

                List<POCont> cortesCont = new List<POCont>();

                cortesCont = (from DataRow dr in dt.Rows
                              select new POCont()
                              {
                                  Id_Order = Convert.ToInt32(dr["Id_Order"]),
                                  POrder = dr["POrder"].ToString()
                              }).ToList();

                Session["Contenedor1"] = cortesCont;

                grdcortes.DataSource = Session["Contenedor1"];

                grdcortes.DataBind();

                lblcnt.Text = grdcortes.Rows.Count.ToString();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ContenedorEmb> GetContenedor(string pre)
    {
        try
        {
            List<ContenedorEmb> list = new List<ContenedorEmb>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top 10 Id_Contenedor, Contenedor from tbContenedorEmb where Contenedor like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ContenedorEmb obj = new ContenedorEmb();

                obj.Id_contenedor = int.Parse(dt.Rows[i][0].ToString());
                obj.Contenedor = dt.Rows[i][1].ToString();

                list.Add(obj);
            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class POCont
    {
        public int Id_Order { get; set; }
        public string POrder { get; set; }
    }

    public class ContenedorEmb
    {
        public int Id_contenedor { get; set; }
        public string Contenedor { get; set; }
    }

    void Cargargrid()
    {
        grdcortes.DataSource = Session["Contenedor1"];
        grdcortes.DataBind();
    }

    void Limpiar()
    {
        txtcut.Text = "";
        hdfcut.Value = "";
    }

    void Limpiar2()
    {
        Session["Contenedor"] = null;
        grdcortes.DataSource = Session["Contenedor1"];
        grdcortes.DataBind();
        txtcontenedor.Text = "";
        hdfcontenedor.Value = "";
        txtcut.Text = "";
        hdfcut.Value = "";
        lblcnt.Text = "";
    }

    protected void btnlimp_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/EditContenedor.aspx");
    }

    protected void grdcortes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Eliminar")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                GridViewRow row1 = grdcortes.SelectedRow;

                int indice = row.RowIndex;

                string corteC = row.Cells[0].Text;

                var list = (List<POCont>)Session["Contenedor1"];

                list.RemoveAt(indice);

                if (corteC != "")
                {
                    string ok = OrderDetailDA.EliminarCorteContenedor(Convert.ToInt32(hdfcontenedor.Value), Convert.ToInt32(corteC), Page.User.Identity.Name.ToLower());

                    if (ok == "OK")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "Eliminar();", true);
                    }
                }

                if (list.Count > 0)
                {
                    lblcnt.Text = list.Count.ToString();
                    Session["Contenedor1"] = list;
                    Cargargrid();
                }
                else
                {
                    lblcnt.Text = "";
                    Session["Contenedor1"] = null;
                    Cargargrid();
                }
            }
        }
        catch (Exception ex)
        {
            string s = ex.Message;
            //throw;
        }
    }

    protected void btncerrar_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdfcontenedor.Value != "")
            {
                int ok = OrderDetailDA.CerrarContenedor(Convert.ToInt32(hdfcontenedor.Value));
                string cont = txtcontenedor.Text;

                if (ok == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "cerrarCon('" + cont + "');", true);
                    txtcontenedor.Text = "";
                    hdfcontenedor.Value = "";
                }
                else if (ok == 1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "AbrirCon('" + cont + "');", true);
                    txtcontenedor.Text = "";
                    hdfcontenedor.Value = "";
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
                }
            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;
        }
    }

    protected void btnmodifi_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtcontenedor.Text != "" && hdfcontenedor.Value != "")
            {
                string ok = OrderDetailDA.GuardarNameCont(Convert.ToInt32(hdfcontenedor.Value), txtcontenedor.Text);

                if (ok == "OK")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                    txtcontenedor.Text = "";
                    hdfcontenedor.Value = "";
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
                }
            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;
        }
    }

    protected void btnregresar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ContenedorEmb.aspx");
    }

    protected void btn1elim_Click(object sender, EventArgs e)
    {
        try
        {
            //Eliminar Todos los Cortes
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnelimCheck_Click(object sender, EventArgs e)
    {
        try
        {
            //Eliminar Todos los Cortes que esten checkiados dentro del grid 
        }
        catch (Exception)
        {

            //throw;
        }
    }
}