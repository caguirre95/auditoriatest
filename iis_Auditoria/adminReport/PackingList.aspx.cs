﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PackingList : System.Web.UI.Page
{

    string a = "---";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["dtpk"] = null;
            fecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }
    }

    void pack()
    {
        try
        {
            if (drpenviodia.SelectedValue == "0")
            {
                //var pl1 = OrderDetailDA.PackingList(Convert.ToDateTime(fecha1.Text), Convert.ToInt32(drplanta.SelectedValue));
                //DataColumn fec = new DataColumn("SentDate", typeof(string));
                //pl1.Columns.Add(fec);
                //for (int i = 0; i < pl1.Rows.Count; i++)
                //{
                //    string f = pl1.Rows[i][5].ToString().Substring(0, 10);
                //    pl1.Rows[i][12] = f;
                //    if (f == "01/01/1900")
                //    {
                //        pl1.Rows[i][12] = a;
                //    }
                //}
                //Session["dtpk"] = pl1;
                //gridpack.DataBind();
            }
            else
            {
                var pl1 = OrderDetailDA.PackingListXEnvio(Convert.ToDateTime(fecha1.Text), Convert.ToInt32(drplanta.SelectedValue), Convert.ToInt32(drpenviodia.SelectedValue));
                //DataColumn fec = new DataColumn("SentDate", typeof(string));
                //pl1.Columns.Add(fec);
                //for (int i = 0; i < pl1.Rows.Count; i++)
                //{
                //    string f = pl1.Rows[i][5].ToString().Substring(0, 10);
                //    pl1.Rows[i][12] = f;
                //    if (f == "01/01/1900")
                //    {
                //        pl1.Rows[i][12] = a;
                //    }
                //}
                Session["dtpk"] = pl1;
                gridpack.DataBind();
            }
        }
        catch (Exception ex)
        {
            string b = ex.Message;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        pack();
    }

    protected void gridpack_DataBinding(object sender, EventArgs e)
    {
        try
        {
            gridpack.DataSource = Session["dtpk"];
            gridpack.GroupBy(gridpack.Columns["Linea"]);
        }
        catch (Exception ex)
        {
            string j = ex.Message;
        }
    }

    protected void gridpack_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridpack.DataBind();
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        pack();
        exportar1.GridViewID = "gridpack";
        exportar1.FileName = "Packing List " + DateTime.Now;
        exportar1.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/PackingList.aspx");
    }
}