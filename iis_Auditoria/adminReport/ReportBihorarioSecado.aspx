﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportBihorarioSecado.aspx.cs" Inherits="adminReport_ReportBihorarioSecado" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script>
        function vacio(fecha) {
            swal({
                title: 'Por Favor!',
                text: "No hay datos para mostrar en esta fecha : " + fecha,
                type: 'info'
            });
        }

        function vacio1() {
            swal({
                title: 'Por Favor!',
                text: " Ingrese los campos de manera correcta",
                type: 'info'
            });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtdate.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });

        function exito() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Reporte Bihorario</strong></div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Controles</div>
                            <div class="panel-body">
                                <strong>Fecha</strong>
                                <asp:TextBox ID="txtdate" runat="server" CssClass="form-control"></asp:TextBox>
                                <hr />
                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control" Text="Generar" OnClick="btngenerar_Click" />
                                <hr />
                                <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control" Text="Pass to excel" OnClick="btnexcel_Click" />
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px">
                        <dx:ASPxGridView ID="grdbihoSec" runat="server" KeyFieldName="id" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="true"
                            OnDataBinding="grdbihoSec_DataBinding" OnCustomCallback="grdbihoSec_CustomCallback">
                            <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                            <Columns>

                                <dx:GridViewBandColumn Caption="BIHORARIO SECADO INTEX" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Responsable:">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="corteCompleto" Caption="CORTE" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="unidadesCorteCompleto" Caption="UNIDADES" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="estilo" Caption="ESTILO" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="09:00" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Firma:">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="11:00" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="13:00" VisibleIndex="5">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="15:00" VisibleIndex="6">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="16:50" VisibleIndex="7">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="18:00" VisibleIndex="8">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="unidadesSecadas" Caption="UNIDADES SECADAS" VisibleIndex="9">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>

                                    </Columns>
                                </dx:GridViewBandColumn>

                                <dx:GridViewBandColumn Caption="ROC 01 REG PRO 0020" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Version: 01" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="deficitSecado" Caption="DEFICIT" VisibleIndex="10">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Estatus" VisibleIndex="11">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>

                                    </Columns>
                                </dx:GridViewBandColumn>

                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                            <Settings ShowFooter="true" HorizontalScrollBarMode="Visible" />
                            <SettingsPager PageSize="50" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="corteCompleto" SummaryType="Count" DisplayFormat="{0}" />                                
                                <dx:ASPxSummaryItem FieldName="unidadesSecadas" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <GroupSummary>
                                <dx:ASPxSummaryItem FieldName="corteCompleto" ShowInGroupFooterColumn="POrder" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="09:00" ShowInGroupFooterColumn="09:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="11:00" ShowInGroupFooterColumn="11:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="13:00" ShowInGroupFooterColumn="13:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="15:00" ShowInGroupFooterColumn="15:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="16:50" ShowInGroupFooterColumn="16:50" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="18:00" ShowInGroupFooterColumn="18:00" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="unidadesSecadas" ShowInGroupFooterColumn="Exceso" SummaryType="Sum" DisplayFormat="{0}" />                                
                            </GroupSummary>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar" runat="server"></dx:ASPxGridViewExporter>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>

