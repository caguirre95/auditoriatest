﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportHistInPlant.aspx.cs" Inherits="adminReport_ReportHistInPlant" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <script lang="javascript" type="text/javascript">        

        $(document).ready(function () {

            $(function () {
                $('#<%=fecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });

        $(document).ready(function () {

            $(function () {
                $('#<%=fecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });

    </script>

    <style>
        .mar {
            margin-bottom: 10px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading">Reporte Inventario</div>
            <div class="panel-body">
                 <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control  mar"></asp:SiteMapPath>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <asp:UpdatePanel ID="upd1" runat="server">
                        <ContentTemplate>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Filtros</div>
                                    <div class="panel-body">
                                        <asp:RadioButtonList ID="rd1" runat="server" OnSelectedIndexChanged="rd1_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="1" Text="Planta"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Linea"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="Cliente"></asp:ListItem>
                                            <asp:ListItem Value="5" Text="Fecha"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Opciones</div>
                                    <div class="panel-body">
                                        <asp:Panel ID="planta" runat="server">
                                            <strong>Planta</strong>
                                            <asp:DropDownList ID="DropDownListPlant" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </asp:Panel>
                                        <asp:Panel ID="Linea" runat="server">
                                            <strong>Linea</strong>
                                            <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </asp:Panel>
                                        <asp:Panel ID="Cliente" runat="server">
                                            <strong>Cliente</strong>
                                            <asp:DropDownList ID="drpcli" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </asp:Panel>
                                        <asp:Panel ID="vacio" runat="server" Visible="false">
                                            <strong>Sin Opciones</strong>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Fechas</div>
                            <div class="panel-body">
                                <strong>Fecha Inicio</strong>
                                <asp:TextBox ID="fecha1" runat="server" AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>

                                <strong>Fecha Final</strong>
                                <asp:TextBox ID="fecha2" runat="server" AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Generar</div>
                            <div class="panel-body">
                                <br />
                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control" Text="Generar" OnClick="btngenerar_Click" />
                                <div>
                                    <br />
                                    <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control" Text="Pass to excel" OnClick="btnexcel_Click" />
                                </div>
                                <div>
                                    <br />
                                    <asp:Button ID="btnlim" runat="server" CssClass="btn btn-primary form-control" Text="Limpiar Formulario" OnClick="btnlim_Click" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <br />
                    <dx:ASPxGridView ID="grdc" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="grdc_DataBinding" OnCustomCallback="grdc_CustomCallback">
                        <SettingsSearchPanel Visible="true" ShowClearButton="true" />
                        <SettingsBehavior AutoExpandAllGroups="true" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="Planta" VisibleIndex="0"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Linea" VisibleIndex="1"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Estilo" VisibleIndex="2"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Cantidad" VisibleIndex="3"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Porder" VisibleIndex="4"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="unidades" Caption="InPlant" VisibleIndex="5"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Comentario" VisibleIndex="6" Caption="Motivo" CellStyle-BackColor="SkyBlue"></dx:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowFooter="true" ShowGroupFooter="VisibleAlways" VerticalScrollBarMode="Visible" />
                        <SettingsPager Mode="ShowAllRecords" />
                        <SettingsPager PageSize="100" />
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="Porder" SummaryType="Count" DisplayFormat="Total de Cortes : {0}" />
                            <dx:ASPxSummaryItem FieldName="unidades" SummaryType="Sum" DisplayFormat="Retenidas en Planta: {0}" />
                        </TotalSummary>
                    </dx:ASPxGridView>
                    <dx:ASPxGridViewExporter ID="export" runat="server"></dx:ASPxGridViewExporter>
                </div>

            </div>
        </div>

    </div>

</asp:Content>

