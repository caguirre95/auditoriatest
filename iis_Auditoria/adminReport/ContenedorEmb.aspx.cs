﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_ContenedorEmb : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string user = Page.User.Identity.Name.ToLower().Trim();

            if (user == "adminsnr")
            {
                Session["Contenedor"] = null;
            }
            else
            {
                FormsAuthentication.SignOut();
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
    }

    protected void btnagregar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtcont.Text != "" && txtcut.Text != "" && hdfcut.Value != "")
            {
                string query = "select p.POrder, ce.Contenedor from POrder p join tb_ContenedorPO cp on cp.Id_Order = p.Id_Order join tbContenedorEmb ce on ce.Id_Contenedor = cp.Id_Contenedor where p.Id_Order = " + hdfcut.Value;

                DataTable dt = DataAccess.Get_DataTable(query);

                if (dt.Rows.Count <= 0)
                {
                    List<POCont> lista = new List<POCont>();

                    if (Session["Contenedor"] != null)
                    {
                        lista = (List<POCont>)Session["Contenedor"];
                    }

                    POCont obj = new POCont();

                    obj.Id_Order = Convert.ToInt32(hdfcut.Value);
                    obj.POrder = txtcut.Text;
                    //obj.CodigoEstilo = txtStyle.Text;

                    var seleccion = lista.Where(a => a.Id_Order == obj.Id_Order).Select(a => a).Where(at => at.POrder == obj.POrder).ToList();

                    if (seleccion.Count > 0)
                    {
                        //ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "bad7();", true);
                        Limpiar();
                    }
                    else
                    {
                        lista.Add(obj);
                        lblcnt.Text = lista.Count.ToString();
                        Session["Contenedor"] = lista;
                        Cargargrid();
                        Limpiar();
                    }
                }
                else
                {
                    string corte = dt.Rows[0][0].ToString();
                    string contenedor = dt.Rows[0][1].ToString();
                    txtcut.Text = "";
                    hdfcut.Value = "";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "verificarCut('" + corte + "','" + contenedor + "');", true);
                }

            }
            else
            {

            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    //[WebMethod]
    //public static string VerificarCod(string codOrden)
    //{
    //    try
    //    {
    //        var i = SolicitudCompra.listarorden(codOrden);

    //        if (i.Count() > 0)
    //        {
    //            return "true";
    //        }
    //        else
    //        {
    //            return "false";
    //        }
    //    }
    //    catch (Exception e)
    //    {
    //        return "Error" + e.Message;
    //    }
    //}

    public class POCont
    {
        public int Id_Order { get; set; }
        public string POrder { get; set; }
        public string CodigoEstilo { get; set; }
    }

    void Cargargrid()
    {
        grdcortes.DataSource = Session["Contenedor"];
        grdcortes.DataBind();
    }

    void Limpiar()
    {
        txtcut.Text = "";
        hdfcut.Value = "";
    }

    void Limpiar2()
    {
        Session["Contenedor"] = null;
        grdcortes.DataSource = Session["Contenedor"];
        grdcortes.DataBind();
        txtcont.Text = "";
        txtcut.Text = "";
        hdfcut.Value = "";
        lblcnt.Text = "";
    }

    protected void btnlimp_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ContenedorEmb.aspx");
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtcont.Text != "")
            {
                string query = "select Id_Contenedor, Contenedor from tbContenedorEmb where Contenedor = '" + txtcont.Text.Trim() + "'";

                DataTable dt = DataAccess.Get_DataTable(query);

                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "verificarContene('" + txtcont.Text + "');", true);
                    txtcont.Text = "";
                }
                else
                {
                    int resp = OrderDetailDA.GuardarContenedor(txtcont.Text, Context.User.Identity.Name);

                    if (resp != 0)
                    {
                        foreach (GridViewRow item in grdcortes.Rows)
                        {
                            int idorder = Convert.ToInt32(item.Cells[0].Text);

                            string ok = OrderDetailDA.GuardarDetalleCont(resp, idorder);

                            if (ok == "OK")
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
                            }
                        }
                    }

                    Limpiar2();
                }
            }
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            //throw;
        }
    }

    protected void grdcortes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Eliminar")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int indice = row.RowIndex;
                var list = (List<POCont>)Session["Contenedor"];
                list.RemoveAt(indice);
                if (list.Count > 0)
                {
                    lblcnt.Text = list.Count.ToString();
                    Session["Contenedor"] = list;
                    Cargargrid();
                }
                else
                {
                    lblcnt.Text = "";
                    Session["Contenedor"] = null;
                    Cargargrid();
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btneditar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/EditContenedor.aspx");
    }
}