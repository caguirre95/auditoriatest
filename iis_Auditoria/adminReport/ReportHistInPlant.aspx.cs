﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_ReportHistInPlant : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            fecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["dt"] = null;
            cargacombo();
        }
    }

    void cargacombo()
    {
        string sqline = " select c1.id_linea, case when(convert(numeric(3, 1), c1.numero) % convert(numeric, c1.numero)) = 0.1 then CONCAT(convert(numeric, c1.numero),'A') else c1.numero end as numero from("
                       + " select l.id_linea, l.numero from Linea l where l.Estado = 1) as c1"
                       + " order by CONVERT(float, c1.numero) asc";

        DataTable dt_Line = DataAccess.Get_DataTable(sqline);

        DropDownListLine.DataSource = dt_Line;
        DropDownListLine.DataTextField = "numero";
        DropDownListLine.DataValueField = "id_linea";
        DropDownListLine.DataBind();
        DropDownListLine.Items.Insert(0, "Select...");

        string sqlplant = "SELECT id_planta, descripcion FROM Planta order by descripcion asc";

        DataTable dt_plant = DataAccess.Get_DataTable(sqlplant);

        DropDownListPlant.DataSource = dt_plant;
        DropDownListPlant.DataTextField = "descripcion";
        DropDownListPlant.DataValueField = "id_planta";
        DropDownListPlant.DataBind();
        DropDownListPlant.Items.Insert(0, "Select...");

        string sqlcli = "select Id_Cliente, Cliente from Cliente where Estado = 1";

        DataTable dtcli = DataAccess.Get_DataTable(sqlcli);

        drpcli.DataSource = dtcli;
        drpcli.DataTextField = "Cliente";
        drpcli.DataValueField = "Id_Cliente";
        drpcli.DataBind();
        drpcli.Items.Insert(0, "Select...");
    }

    protected void rd1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string id = rd1.SelectedValue;

            switch (id)
            {
                case "1"://Planta
                    planta.Visible = true;
                    Linea.Visible = false;
                    Cliente.Visible = false;
                    vacio.Visible = false;
                    break;

                case "2"://Linea
                    planta.Visible = false;
                    Linea.Visible = true;
                    Cliente.Visible = false;
                    vacio.Visible = false;
                    break;

                case "3"://Cliente
                    planta.Visible = false;
                    Linea.Visible = false;
                    Cliente.Visible = true;
                    vacio.Visible = false;
                    break;

                case "5"://Fechas
                    planta.Visible = false;
                    Linea.Visible = false;
                    Cliente.Visible = false;
                    vacio.Visible = true;
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    void generar()
    {
        string r = rd1.SelectedValue;

        switch (r)
        {
            case "1"://Planta
                var a = OrderDetailDA.Reporte_Invplanta(Convert.ToInt16(DropDownListPlant.SelectedValue), Convert.ToDateTime(fecha1.Text), Convert.ToDateTime(fecha2.Text));
                Session["dt"] = a;
                grdc.DataSource = Session["dt"];
                grdc.DataBind();
                grdc.GroupBy(grdc.Columns["Planta"]);
                grdc.GroupBy(grdc.Columns["Linea"]);
                break;

            case "2"://Linea
                var b = OrderDetailDA.Reporte_Invlinea(Convert.ToInt16(DropDownListLine.SelectedValue), Convert.ToDateTime(fecha1.Text), Convert.ToDateTime(fecha2.Text));
                Session["dt"] = b;
                grdc.DataSource = Session["dt"];
                grdc.DataBind();
                grdc.GroupBy(grdc.Columns["Planta"]);
                grdc.GroupBy(grdc.Columns["Linea"]);
                break;

            case "3"://Cliente
                var c = OrderDetailDA.Reporte_InvCliente(Convert.ToInt16(drpcli.SelectedValue), Convert.ToDateTime(fecha1.Text), Convert.ToDateTime(fecha2.Text));
                Session["dt"] = c;
                grdc.DataSource = Session["dt"];
                grdc.DataBind();
                grdc.GroupBy(grdc.Columns["Planta"]);
                grdc.GroupBy(grdc.Columns["Linea"]);
                break;

            case "5"://Fecha
                var d = OrderDetailDA.Reporte_InvFecha(Convert.ToDateTime(fecha1.Text), Convert.ToDateTime(fecha2.Text));
                Session["dt"] = d;
                grdc.DataSource = Session["dt"];
                grdc.DataBind();
                grdc.GroupBy(grdc.Columns["Planta"]);
                grdc.GroupBy(grdc.Columns["Linea"]);
                break;

            default:
                break;

        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            generar();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            export.GridViewID = "grdc";
            export.FileName = "Reporte Inventario en Planta " + DateTime.Now;
            export.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnlim_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportHistInPlant.aspx");
    }

    protected void grdc_DataBinding(object sender, EventArgs e)
    {
        try
        {
            grdc.DataSource = Session["dt"];
            grdc.GroupBy(grdc.Columns["Planta"]);
            grdc.GroupBy(grdc.Columns["Linea"]);
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void grdc_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdc.DataBind();
    }
}