﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_HistInPlant : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            conteo();
            extraerlinea();
        }
    }

    void extraerlinea()
    {
        try
        {
            string user = HttpContext.Current.User.Identity.Name.ToUpper();
            string query = " select l.id_linea,'Linea : ' + l.numero numero"
                          + " from linea l"
                          + " where numero not like '%-%' and numero not like '%B%' and id_planta = (select Id_Planta from UserPlanta u where u.nombreUsuario = '" + user + "')"
                          + " order by convert(float, numero) asc";

            DataTable dt = DataAccess.Get_DataTable(query);

            drpline.DataSource = dt;
            drpline.DataTextField = "numero";
            drpline.DataValueField = "id_linea";
            drpline.DataBind();
            drpline.Items.Insert(0, "Select...");
        }
        catch (Exception)
        {
            throw;
        }
    }

    void conteo()
    {
        try
        {
            DataTable dt;
            string user = HttpContext.Current.User.Identity.Name;

            string queryBundle = " select * from vwlistacortesenplanta where nombreUsuario = '" + user + "'";

            dt = DataAccess.Get_DataTable(queryBundle);

            grdhist.DataSource = dt;
            grdhist.DataBind();

        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void drpline_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (drpline.SelectedItem.Text != "Select...")
            {
                string user = HttpContext.Current.User.Identity.Name;

                string queryBundle = " select * from vwlistacortesenplanta where id_linea = " + drpline.SelectedValue + " and nombreUsuario = '" + user + "'";
                DataTable dt = DataAccess.Get_DataTable(queryBundle);

                grdhist.DataSource = dt;
                grdhist.DataBind();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnlim_Click(object sender, EventArgs e)
    {
        try
        {
            conteo();
            extraerlinea();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void lnkg_Click(object sender, EventArgs e)
    {
        try
        {
            int rows = 0;
            int cont = 0;
            foreach (GridViewRow item in grdhist.Rows)
            {
                string resp = "";
                var lblidorder = (Label)item.FindControl("lblidorder");
                var chkd = (CheckBox)item.FindControl("chk");
                var txtuni = (TextBox)item.FindControl("txtunidades");
                var txtc = (TextBox)item.FindControl("txtcom");

                if (chkd.Checked && txtc.Text != "")
                {
                    rows++;
                    resp = OrderDetailDA.enviarguardarComenIndi(int.Parse(lblidorder.Text), int.Parse(txtuni.Text), txtc.Text, Context.User.Identity.Name);
                }

                if (resp == "OK")
                {
                    cont++;
                    break;
                }
            }

            if (rows == cont)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
            }

            conteo();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btngrd_Click(object sender, EventArgs e)
    {
        try
        {
            if (drpcom.SelectedItem.Text != "Select...")
            {
                int rows = 0;// gridEnvios.Rows.Count;
                int cont = 0;
                foreach (GridViewRow item in grdhist.Rows)
                {
                    string resp = "";
                    var lblidorder = (Label)item.FindControl("lblidorder");
                    var chkd = (CheckBox)item.FindControl("chk");
                    var txtuni = (TextBox)item.FindControl("txtunidades");

                    if (chkd.Checked)
                    {
                        rows++;
                        resp = OrderDetailDA.enviarguardarComen(int.Parse(lblidorder.Text), int.Parse(txtuni.Text), drpcom.SelectedItem.ToString(), Context.User.Identity.Name);
                    }

                    if (resp == "OK")
                    {
                        cont++;
                    }
                }

                if (rows == cont)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
                }

                conteo();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "selec();", true);
            }
        }
        catch (Exception)
        {
            throw;
        }
    }
}