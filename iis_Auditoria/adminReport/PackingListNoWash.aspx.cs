﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_PackingListNoWash : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string a = "---";

        if (!IsPostBack)
        {
            Session["dtpk"] = null;
            fecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }
    }

    void pack()
    {
        try
        {
            if (drpenviodia.SelectedValue == "0")
            {
                //var pl1 = OrderDetailDA.PackingListnowash(Convert.ToDateTime(fecha1.Text), Convert.ToInt32(drplanta.SelectedValue));
                //DataColumn fec = new DataColumn("SentDate", typeof(string));
                //pl1.Columns.Add(fec);
                //for (int i = 0; i < pl1.Rows.Count; i++)
                //{
                //    string f = pl1.Rows[i][5].ToString().Substring(0, 10);
                //    pl1.Rows[i][12] = f;
                //    if (f == "01/01/1900")
                //    {
                //        pl1.Rows[i][12] = "---";
                //    }
                //}
                //Session["dtpk"] = pl1;
                //gridpacknowash.DataBind();
            }
            else
            {
                var pl1 = OrderDetailDA.PackingListnowashenvio(Convert.ToDateTime(fecha1.Text), Convert.ToInt32(drplanta.SelectedValue), Convert.ToInt16(drpenviodia.SelectedValue));
                //DataColumn fec = new DataColumn("SentDate", typeof(string));
                //pl1.Columns.Add(fec);
                //for (int i = 0; i < pl1.Rows.Count; i++)
                //{
                //    string f = pl1.Rows[i][5].ToString().Substring(0, 10);
                //    pl1.Rows[i][12] = f;
                //    if (f == "01/01/1900")
                //    {
                //        pl1.Rows[i][12] = "---";
                //    } 
                //}
                Session["dtpk"] = pl1;
                gridpacknowash.DataBind();
            }

            #region
            //if (fecha1.Text == "")
            //{
            //    var pl1 = OrderDetailDA.PackingListnowash(Convert.ToDateTime(fecha1.Text));
            //    DataColumn fec = new DataColumn("SentDate", typeof(string));
            //    pl1.Columns.Add(fec);
            //    for (int i = 0; i < pl1.Rows.Count; i++)
            //    {
            //        string f = pl1.Rows[i][5].ToString().Substring(0, 10);
            //        pl1.Rows[i][12] = f;
            //        if (f == "01/01/1900")
            //        {
            //            pl1.Rows[i][12] = a;
            //        }
            //    }
            //    Session["dtpk"] = pl1;
            //    gridpacknowash.DataBind();
            //}
            //else
            //{
            //    var pl1 = OrderDetailDA.PackingListXEnvionowash(Convert.ToDateTime(fecha1.Text));
            //    DataColumn fec = new DataColumn("SentDate", typeof(string));
            //    pl1.Columns.Add(fec);
            //    for (int i = 0; i < pl1.Rows.Count; i++)
            //    {
            //        string f = pl1.Rows[i][5].ToString().Substring(0, 10);
            //        pl1.Rows[i][12] = f;
            //        if (f == "01/01/1900")
            //        {
            //            pl1.Rows[i][12] = a;
            //        }
            //    }
            //    Session["dtpk"] = pl1;
            //    gridpacknowash.DataBind();
            //}
            #endregion
        }
        catch (Exception ex)
        {
            string b = ex.Message;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        pack();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/PackingListNoWash.aspx");
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        pack();
        exportar1.GridViewID = "gridpacknowash";
        exportar1.FileName = "Packing List " + DateTime.Now;
        exportar1.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
    }

    protected void gridpacknowash_DataBinding(object sender, EventArgs e)
    {
        try
        {
            gridpacknowash.DataSource = Session["dtpk"];
            gridpacknowash.GroupBy(gridpacknowash.Columns["Linea"]);

        }
        catch (Exception ex)
        {
            string j = ex.Message;
        }
    }

    protected void gridpacknowash_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridpacknowash.DataBind();
    }
}