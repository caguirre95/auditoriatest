﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteEnNoEn.aspx.cs" Inherits="adminReport_ReporteEnNoEn" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <script lang="javascript" type="text/javascript">
        $(document).ready(function () {

            $(function () {
                $('#<%=fecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });



        $(document).ready(function () {

            $(function () {
                $('#<%=fecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script lang="javascript" type="text/javascript">

        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../Medida.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hdnidporder.ClientID%>').val(ui.item.idorder);
                    // $('#<=txtstyle.ClientID%>').val(ui.item.style);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        })


    </script>

    <style>
        .mar {
            margin-bottom: 10px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading"><strong>Reporte Bihorario</strong></div>
            <div class="panel-body">
                <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <br />

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Filtros</div>
                            <div class="panel-body">
                                <asp:RadioButtonList ID="rd1" runat="server" OnSelectedIndexChanged="rd1_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="1">Planta</asp:ListItem>
                                    <asp:ListItem Value="2">Linea</asp:ListItem>
                                    <asp:ListItem Value="3">Corte</asp:ListItem>
                                    <asp:ListItem Value="4">Fecha</asp:ListItem>
                                    <asp:ListItem Value="5">Cliente</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">

                                <asp:Panel ID="panelp" runat="server">
                                    <strong>Planta</strong>
                                    <asp:DropDownList ID="DropDownListPlant" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </asp:Panel>

                                <asp:Panel ID="panell" runat="server">
                                    <strong>Linea</strong>
                                    <asp:DropDownList ID="DropDownListLine" CssClass="form-control" runat="server"></asp:DropDownList>
                                </asp:Panel>

                                <asp:Panel ID="panelc" runat="server">
                                    <strong>Corte</strong>
                                    <asp:TextBox ID="txtPorder" placeholder="Codigo Order" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdnidporder" />
                                </asp:Panel>

                                <asp:Panel ID="panelcl" runat="server">
                                    <strong>Cliente</strong>
                                    <asp:DropDownList ID="drpcli" runat="server" CssClass="form-control"></asp:DropDownList>
                                </asp:Panel>

                                <asp:Panel ID="panelfec" runat="server">
                                    <strong>Fecha Inicio</strong>
                                <asp:TextBox ID="fecha1" runat="server" AutoCompleteType="Disabled" CssClass="form-control" onkeypress="auto1()"></asp:TextBox>

                                <strong>Fecha Final</strong>
                                <asp:TextBox ID="fecha2" runat="server" AutoCompleteType="Disabled" CssClass="form-control" onkeypress="auto2()"></asp:TextBox>
                                </asp:Panel>                                

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Generar</div>
                            <div class="panel-body">
                                <br />
                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control" Text="Generar" OnClick="btngenerar_Click" />
                                <div>
                                    <br />
                                    <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control" Text="Pass to excel" OnClick="btnexcel_Click" />
                                </div>
                                <div>
                                    <br />
                                    <asp:Button ID="btnlim" runat="server" CssClass="btn btn-primary form-control" Text="Limpiar Formulario" OnClick="btnlim_Click" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <br />
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <dx:ASPxGridView ID="gridsent" runat="server" Width="100%" Theme="Metropolis" OnDataBinding="gridsent_DataBinding" OnCustomCallback="gridsent_CustomCallback">
                        <SettingsSearchPanel Visible="true" ShowClearButton="true" />
                        <SettingsBehavior AutoExpandAllGroups="true" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="Planta" VisibleIndex="0"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Linea" VisibleIndex="1"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Estilo" VisibleIndex="2"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Cantidad" VisibleIndex="3"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Porder" VisibleIndex="4"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Color" VisibleIndex ="4"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Contadas" VisibleIndex="5"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Deficit" VisibleIndex="6"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Enviadas" VisibleIndex="9"></dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="NoEnviadas" VisibleIndex="10"></dx:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowFooter="true" ShowGroupFooter="VisibleAlways" VerticalScrollBarMode="Visible" />
                        <SettingsPager Mode="ShowAllRecords" />
                        <SettingsPager PageSize="100" />
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="Porder" SummaryType="Count" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="Contadas" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="Enviadas" SummaryType="Sum" DisplayFormat="{0}" />
                            <dx:ASPxSummaryItem FieldName="NoEnviadas" SummaryType="Sum" DisplayFormat="{0}" />
                        </TotalSummary>
                    </dx:ASPxGridView>
                    <dx:ASPxGridViewExporter ID="export" runat="server"></dx:ASPxGridViewExporter>
                </div>

            </div>
        </div>
    </div>

</asp:Content>

