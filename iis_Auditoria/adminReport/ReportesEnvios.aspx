﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportesEnvios.aspx.cs" Inherits="adminReport_ReportesEnvios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   
    <style type="text/css">
        
        .box_effect {
            height: 250px;
            width: 180px;
            padding-top: 15px;
            text-align: center;
            padding: 5px;
            border-width: 160px;
            margin: auto;
            opacity: 0.8;
            border: 1px solid rgba(0,0,0,0.2);
        }

        .te {
            text-decoration: none;
        }

        .im {
            z-index: -1;
        }

        .mt {
            margin-left: 10px;
            margin-top: 15px;
        }
        .mb{
            margin-bottom:10px;
        }

        .te:hover {
            color: orangered !important;
            text-decoration: none;
            outline-style: none;
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="container" style="padding-top: 5px">

        <div class="col-lg-12 mb">
            <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>
        </div>

        <div class="row ">

            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkBihorario" runat="server" CssClass="te" OnClick="lnkBihorario_Click">
                    <div>                        
                        <img src="../img/by_section.png" alt="Inspection" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">BIOHORARIO</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="lnkPackL" runat="server" CssClass="te" OnClick="lnkPackL_Click">
                    <div>                        
                        <img src="../img/by_section.png" alt="Inspection" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;">PACKING LIST</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>



            <div class="col-lg-4 ">
                <div class="box_effect">
                    <asp:LinkButton ID="linkWeek" runat="server" CssClass="te" OnClick="linkWeek_Click">
                    <div>                        
                        <img src="../img/by_section.png" alt="Inspection" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;" class="text-uppercase">week REPORT</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>



        </div>

        <div class="row">
             <div class="col-lg-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkCancelados" runat="server" CssClass="te" OnClick="LinkCancelados_Click">
                    <div>                        
                        <img src="../img/by_section.png" alt="Inspection" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;" class="text-uppercase">Reporte Lavanderia</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>

             <div class="col-lg-4 col-lg-offset-4">
                <div class="box_effect">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="te" OnClick="LinkButton1_Click">
                    <div>                        
                        <img src="../img/by_section.png" alt="Inspection" class="mt" />
                    </div>
                    <div style="margin-top:20px">
                        <span style="font-weight: bold;" class="text-uppercase">Reporte Historial Corte</span>
                    </div>
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

