﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PackingListNoWash.aspx.cs" Inherits="adminReport_PackingListNoWash" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script>
        $(document).ready(function () {
            $(function () {
                $('#<%=fecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <style>
        .mr {
            margin-bottom: 10px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Packing List No Wash</strong></div>
            <div class="panel-body">
                <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>
                <div class="col-lg-12">
                    <br />
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Filtros</div>
                            <div class="panel-body">
                                <strong>Planta</strong>
                                <asp:DropDownList ID="drplanta" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">Select...</asp:ListItem>
                                    <asp:ListItem Value="1">Planta 1</asp:ListItem>
                                    <asp:ListItem Value="2">Planta 3</asp:ListItem>
                                    <asp:ListItem Value="3">Planta 6</asp:ListItem>
                                    <asp:ListItem Value="4">Planta 7</asp:ListItem>
                                </asp:DropDownList>
                                <strong>Fecha</strong>
                                <asp:TextBox ID="fecha1" runat="server" CssClass="form-control"></asp:TextBox>
                                <strong>Envio del día</strong>
                                <asp:DropDownList ID="drpenviodia" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">Select...</asp:ListItem>
                                    <asp:ListItem Value="1">Envio 1</asp:ListItem>
                                    <asp:ListItem Value="2">Envio 2</asp:ListItem>
                                    <asp:ListItem Value="3">Envio 3</asp:ListItem>
                                    <asp:ListItem Value="4">Envio 4</asp:ListItem>
                                    <asp:ListItem Value="5">Envio 5</asp:ListItem>
                                    <asp:ListItem Value="6">Envio 6</asp:ListItem>
                                    <asp:ListItem Value="7">Envio 7</asp:ListItem>
                                    <asp:ListItem Value="8">Envio 8</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <div style="margin-top: 5px;">

                                    <div class="mr">
                                        <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control" Text="Generar" OnClick="btngenerar_Click" />
                                    </div>
                                    <div class="mr">
                                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-info form-control" Text="Limpiar" OnClick="Button1_Click" />
                                    </div>
                                    <div class="mr">
                                        <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control" Text="Pass to excel" OnClick="btnexcel_Click" />
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    </div>

                    <br />

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px; overflow-x: auto">
                        <dx:ASPxGridView ID="gridpacknowash" runat="server" KeyFieldName="id" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnDataBinding="gridpacknowash_DataBinding" OnCustomCallback="gridpacknowash_CustomCallback">
                            <SettingsBehavior AutoExpandAllGroups="true" />
                            <Columns>

                                <dx:GridViewBandColumn Caption="CONTROL OF SHIPMENTS AND DEFICIT IN PACKING" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Responsable:">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Descripcion" Caption="Descripcion" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Linea" Caption="Line" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Cut" Caption="Cut" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Color" Caption="color" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Firma">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Style" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Quantity" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>
                                                <%-- <dx:GridViewDataTextColumn FieldName="FechaEnvio" VisibleIndex="5" PropertiesTextEdit-DisplayFormatString="yyyy-MM-dd" Visible="false">
                                                </dx:GridViewDataTextColumn>--%>
                                                <dx:GridViewDataTextColumn FieldName="SentDate" Caption="Fecha Envio" VisibleIndex="5">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Fecha de creación: Junio de 2017" HeaderStyle-HorizontalAlign="Center">
                                           <Columns>
                                                <dx:GridViewDataTextColumn FieldName="EnvioAnterior" VisibleIndex="6">
                                                </dx:GridViewDataTextColumn>
                                                 <dx:GridViewDataTextColumn FieldName="EnvioAnteriorEx" Caption="Envio Anterior Exceso" VisibleIndex="6">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="EnviadasLavar" VisibleIndex="7">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Exceso" VisibleIndex="8" Visible="false">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Bundle" VisibleIndex="8">
                                                </dx:GridViewDataTextColumn>                                                
                                            </Columns>
                                        </dx:GridViewBandColumn>

                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewBandColumn Caption="ROC 01 REG PRO 0021" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Version 01" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Deficit" VisibleIndex="9">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Status" VisibleIndex="10">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Envios" VisibleIndex="11">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>

                                    </Columns>
                                </dx:GridViewBandColumn>

                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                            <Settings ShowFooter="true" />
                            <SettingsPager PageSize="100" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="Cut" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Cantidad" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="EnviadasLavar" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Bundle" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar1" runat="server"></dx:ASPxGridViewExporter>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>

