﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_PLgeneral : System.Web.UI.Page
{
    string a = "---";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["dtpk"] = null;
            fecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }
    }

    void pack()
    {
        try
        {
            if (fecha1.Text != "" && drpla.SelectedValue != "2" && drpplant.SelectedValue != "0")
            {
                var pl2 = OrderDetailDA.PackingListgnralLa_NoLa(Convert.ToDateTime(fecha1.Text), Convert.ToInt16(drpla.SelectedValue), Convert.ToInt16(drpplant.SelectedValue));
                Session["dtpk"] = pl2;
                gridpack.DataBind();
            }
            else if (fecha1.Text != "" && drpla.SelectedValue != "2" && drpplant.SelectedValue == "0")
            {
                //var pl2 = OrderDetailDA.PackingListgnralLa_NoLa2(Convert.ToDateTime(fecha1.Text), Convert.ToInt16(drpla.SelectedValue));
                //Session["dtpk"] = pl2;
                //gridpack.DataBind();

                string sqlquery = " SELECT p.Describir AS Descripcion, MIN(b.Color) Color, pl.descripcion AS Planta, l.numero Linea, p.id_order id, p.POrder Cut,"
                                + " s.Style, p.Quantity, SUM(he.cantidadagregada) AS Enviadas, ISNULL(SUM(et.cantidadExceso), 0) AS Exceso, MAX(he.numEnvioCorte)  AS ultimoenvio"
                                + " FROM dbo.POrder AS p JOIN Style s ON p.Id_Style = s.Id_Style JOIN dbo.Linea AS l ON  p.Id_Linea = l.id_linea JOIN planta pl ON  l.id_planta = pl.Id_Planta"
                                + " JOIN dbo.Bundle AS b ON  p.Id_Order = b.Id_Order JOIN dbo.tblEnviosbulto AS eb ON  b.Id_Bundle = eb.idBulto JOIN dbo.tblhistoricoenvio AS he ON  eb.idEnvio = he.idEnvio"
                                + " LEFT JOIN (SELECT idCorte, cantidadExceso, fechaEnvioExceso FROM   dbo.tblExcesoTallaCorte WHERE  ISNULL(CONVERT(NVARCHAR, CONVERT(DATE, fechaEnvioExceso)), '') != ''"
                                + " ) AS et ON et.idCorte = p.Id_Order WHERE CONVERT(DATE, he.fechaEnvio) = '" + fecha1.Text + "' AND s.washed = " + drpla.SelectedValue + ""
                                + " GROUP BY pl.descripcion, l.numero, p.Id_Order, p.Describir, p.POrder, s.Style, p.Quantity";

                string sqlquery2 = " SELECT b1.Id_Order, SUM(b1.Cant) Enviadas FROM (SELECT b.Id_Order, SUM(he.cantidadagregada) Cant FROM   porder p JOIN Style s ON  p.Id_Style = s.Id_Style"
                                 + " JOIN Bundle b ON  p.Id_Order = b.id_order JOIN tblEnviosbulto eb ON  b.Id_Bundle = eb.idBulto JOIN tblhistoricoenvio he ON  eb.idEnvio = he.idEnvio"
                                 + " WHERE  CONVERT(DATE, he.fechaEnvio) <= '" + fecha1.Text + "' AND s.washed = " + drpla.SelectedValue + " GROUP BY b.id_order) AS b1 GROUP BY b1.Id_Order";

                string sqlquery3 = " SELECT p.Id_Order, MAX(CONVERT(DATE, he.fechaEnvio)) AS ultimafecha FROM dbo.POrder AS p JOIN dbo.Style AS s ON  s.Id_Style = p.Id_Style"
                                 + " JOIN dbo.Bundle AS b ON  p.Id_Order = b.Id_Order JOIN dbo.tblEnviosbulto AS eb ON  b.Id_Bundle = eb.idBulto JOIN dbo.tblhistoricoenvio AS he ON  eb.idEnvio = he.idEnvio"
                                 + " WHERE CONVERT(DATE, he.fechaEnvio) <= '" + fecha1.Text + "' AND s.washed = " + drpla.SelectedValue + " GROUP BY p.id_order";

                DataTable consulta = DataAccess.Get_DataTable(sqlquery);
                DataTable consulta2 = DataAccess.Get_DataTable(sqlquery2);
                DataTable consulta3 = DataAccess.Get_DataTable(sqlquery3);

                var pl2 = (
                from p in consulta.AsEnumerable()
                join pc in consulta2.AsEnumerable() on p.Field<int>("id") equals pc.Field<int>("Id_Order")
                join pc1 in consulta3.AsEnumerable() on p.Field<int>("id") equals pc1.Field<int>("Id_Order")
                select new
                {
                    Descripcion = p.Field<string>("Descripcion"),
                    Color = p.Field<string>("Color"),
                    Planta = p.Field<string>("Planta"),
                    Linea = p.Field<string>("Linea"),
                    id = p.Field<int>("id"),
                    Cut = p.Field<string>("Cut"),
                    Style = p.Field<string>("Style"),
                    Quantity = p.Field<int>("Quantity"),
                    Enviadas = p.Field<int>("Enviadas"),
                    ultimoenvio = p.Field<int>("ultimoenvio"),
                    ultimafechaEn = pc1.Field<DateTime>("ultimafecha"),
                    Exceso = p.Field<int>("Exceso"),
                    DeficitEnvio = pc.Field<int>("Enviadas") - p.Field<int>("Quantity"),
                    Estatus = pc.Field<int>("Enviadas") - p.Field<int>("Quantity") == 0 ? "Cancelado/Envios" : "Abierto/Envios",
                }).ToList();

                Session["dtpk"] = pl2;
                gridpack.DataBind();

            }
            else if (fecha1.Text != "" && drpla.SelectedValue == "2" && drpplant.SelectedValue != "0")
            {
                var pl1 = OrderDetailDA.PackingListgnralLa_NoLa3(Convert.ToDateTime(fecha1.Text), Convert.ToInt16(drpplant.SelectedValue));
                #region
                //DataColumn fec = new DataColumn("SentDate", typeof(string));
                //pl1.Columns.Add(fec);
                //for (int i = 0; i < pl1.Rows.Count; i++)
                //{
                //    string f = pl1.Rows[i][6].ToString().Substring(0, 10);
                //    pl1.Rows[i][13] = f;
                //    if (f == "01/01/1900")
                //    {
                //        pl1.Rows[i][13] = a;
                //    }
                //}
                #endregion
                Session["dtpk"] = pl1;
                gridpack.DataBind();
            }
            else if (fecha1.Text != "" && drpla.SelectedValue == "2" && drpplant.SelectedValue == "0")
            {
                //var pl1 = OrderDetailDA.PackingListgnral(Convert.ToDateTime(fecha1.Text));
                #region
                //DataColumn fec = new DataColumn("SentDate", typeof(string));
                //pl1.Columns.Add(fec);
                //for (int i = 0; i < pl1.Rows.Count; i++)
                //{
                //    string f = pl1.Rows[i][6].ToString().Substring(0, 10);
                //    pl1.Rows[i][13] = f;
                //    if (f == "01/01/1900")
                //    {
                //        pl1.Rows[i][13] = a;
                //    }
                //}
                #endregion
                //Session["dtpk"] = pl1;
                //gridpack.DataBind();

                string sqlquery = " select p.Describir as Descripcion, min(b.Color) Color, pl.descripcion as Planta, l.numero Linea, p.id_order id,p.POrder Cut,s.Style,p.Quantity,"
                                + " sum(he.cantidadagregada) as Enviadas, ISNULL(sum(et.cantidadExceso), 0) as Exceso, max(he.numEnvioCorte) as ultimoenvio FROM dbo.POrder AS p join"
                                + " Style s on p.Id_Style = s.Id_Style join dbo.Linea AS l ON p.Id_Linea = l.id_linea join planta pl on l.id_planta = pl.Id_Planta join"
                                + " dbo.Bundle AS b ON p.Id_Order = b.Id_Order join dbo.tblEnviosbulto AS eb ON b.Id_Bundle = eb.idBulto join dbo.tblhistoricoenvio AS he ON eb.idEnvio = he.idEnvio left join"
                                + " (select idCorte, cantidadExceso, fechaEnvioExceso from dbo.tblExcesoTallaCorte where ISNULL(CONVERT(nvarchar, CONVERT(date, fechaEnvioExceso)),'') != '') AS et ON et.idCorte = p.Id_Order"
                                + " where CONVERT(date, he.fechaEnvio)= '" + fecha1.Text + "' group by pl.descripcion,l.numero,p.Id_Order,p.Describir,p.POrder,s.Style,p.Quantity";

                string sqlquery2 = " select b1.Id_Order, SUM(b1.Cant) Enviadas from (select b.Id_Order, SUM(he.cantidadagregada) Cant from porder p join Bundle b on p.Id_Order = b.id_order join"
                                 + " tblEnviosbulto eb on b.Id_Bundle = eb.idBulto join tblhistoricoenvio he on eb.idEnvio = he.idEnvio where CONVERT(date, he.fechaEnvio) <= '" + fecha1.Text + "'"
                                 + " group by b.id_order) as b1 group by b1.Id_Order ";

                string sqlquery3 = " select p.Id_Order,MAX(convert(date,he.fechaEnvio))as ultimafecha FROM dbo.POrder AS p INNER JOIN dbo.Bundle AS b ON p.Id_Order = b.Id_Order INNER JOIN"
                                 + " dbo.tblEnviosbulto AS eb ON b.Id_Bundle = eb.idBulto INNER JOIN dbo.tblhistoricoenvio AS he ON eb.idEnvio = he.idEnvio"
                                 + " where CONVERT(date, he.fechaEnvio)<= '" + fecha1.Text + "' group by p.id_order";

                DataTable consulta = DataAccess.Get_DataTable(sqlquery);
                DataTable consulta2 = DataAccess.Get_DataTable(sqlquery2);
                DataTable consulta3 = DataAccess.Get_DataTable(sqlquery3);

                var pl1 = (
                from p in consulta.AsEnumerable()
                join pc in consulta2.AsEnumerable() on p.Field<int>("id") equals pc.Field<int>("Id_Order")
                join pc1 in consulta3.AsEnumerable() on p.Field<int>("id") equals pc1.Field<int>("Id_Order")
                select new
                {
                    Descripcion = p.Field<string>("Descripcion"),
                    Color = p.Field<string>("Color"),
                    Planta = p.Field<string>("Planta"),
                    Linea = p.Field<string>("Linea"),
                    id = p.Field<int>("id"),
                    Cut = p.Field<string>("Cut"),
                    Style = p.Field<string>("Style"),
                    Quantity = p.Field<int>("Quantity"),
                    Enviadas = p.Field<int>("Enviadas"),
                    ultimoenvio = p.Field<int>("ultimoenvio"),
                    ultimafechaEn = pc1.Field<DateTime>("ultimafecha"),
                    Exceso = p.Field<int>("Exceso"),
                    DeficitEnvio = pc.Field<int>("Enviadas") - p.Field<int>("Quantity"),
                    Estatus = pc.Field<int>("Enviadas") - p.Field<int>("Quantity") == 0 ? "Cancelado/Envios" : "Abierto/Envios",
                }).ToList();

                Session["dtpk"] = pl1;
                gridpack.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        pack();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/PLgeneral.aspx");
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        exportar1.GridViewID = "gridpack";
        exportar1.FileName = "Packing List " + DateTime.Now;
        exportar1.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
    }

    protected void gridpack_DataBinding(object sender, EventArgs e)
    {
        try
        {
            gridpack.DataSource = Session["dtpk"];
            gridpack.GroupBy(gridpack.Columns["Planta"]);
            gridpack.GroupBy(gridpack.Columns["Linea"]);
        }
        catch (Exception ex)
        {
            string j = ex.Message;
        }
    }

    protected void gridpack_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridpack.DataBind();
    }
}