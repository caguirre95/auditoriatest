﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteContenedorEmb.aspx.cs" Inherits="adminReport_ReporteContenedorEmb" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <script lang="javascript" type="text/javascript">

        $(function () {
            $('#<%=txtcontenedor.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "ReporteContenedorEmb.aspx/GetContenedor",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    Contenedor: item.Contenedor,
                                    Id_contenedor: item.Id_contenedor,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcontenedor.ClientID%>').val(ui.item.Contenedor);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcontenedor.ClientID%>').val(ui.item.Contenedor);
                    $('#<%=hdfcontenedor.ClientID%>').val(ui.item.Id_contenedor);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.Contenedor + "</a>").appendTo(ul);
            };
        })
    </script>

    <style>
        .sp {
            margin-top: 20px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <br />
    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading"><strong>Editar Contenedor</strong></div>
            <div class="panel-body">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <strong>Contenedor</strong>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <asp:LinkButton Text="" OnClick="LinkButton1_Click" CssClass="btn btn-default" ID="LinkButton1" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                            </span>
                            <div class="ui-widget" style="text-align: left;">
                                <asp:TextBox ID="txtcontenedor" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Contenedor"></asp:TextBox>
                                <asp:HiddenField ID="hdfcontenedor" runat="server" />
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 sp">
                        <asp:Button ID="btnexport" runat="server" CssClass="btn btn-success form-control" Text="Exportar" OnClick="btnexport_Click" />
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 sp">
                        <asp:Button ID="btnlimp" runat="server" CssClass="btn btn-info form-control" Text="Limpiar" OnClick="btnlimp_Click" />
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sp">
                        <asp:Button ID="btncortecompl" runat="server" CssClass="btn btn-primary" Text="Ver Reporte, Cortes Completos" OnClick="btncortecompl_Click" />
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sp">
                        <dx:ASPxGridView ID="grdrptCont" runat="server" KeyFieldName="Id_Order" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnDataBinding="grdrptCont_DataBinding" OnCustomCallback="grdrptCont_CustomCallback">
                            <Columns>
                                <dx:GridViewBandColumn Caption="Reporte de Contenedores" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="Contenedor" Caption="Contenedor" VisibleIndex="0">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Id_Order" Visible="false" Caption="id" VisibleIndex="0">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Linea" Caption="Line" VisibleIndex="1">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="id" Caption="Nº" VisibleIndex="2">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Style" Caption="Style" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Describir" Caption="Descripción" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Cut" Caption="Cut #" VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Prs Issued" VisibleIndex="5">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Partes_Confeccionadas" Caption="Sewn pairs " VisibleIndex="6">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Line_Dfi" Caption="Line Dfi" VisibleIndex="7">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Contado_No_Enviado_Planta" Caption="Out of Sew - Not Sent" VisibleIndex="8">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Fecha_Envio_Intex" Caption="Date to send to intex" VisibleIndex="9">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="En_Intex" Caption="At Laundry" VisibleIndex="10">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Fecha_Recibido_Empaque" Caption="Last Date Received" VisibleIndex="11">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Piezas_Recibidas_en_Empaque" Caption="Total Received pairs" VisibleIndex="12">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Recibido" Caption="Received %" VisibleIndex="13">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                            <Settings ShowFooter="true" HorizontalScrollBarMode="Visible" />
                            <SettingsPager PageSize="100" />                           
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="Cut" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Quantity" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Partes_Confeccionadas" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Line_Dfi" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Contado_No_Enviado_Planta" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="En_Intex" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Piezas_Recibidas_en_Empaque" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <%--<GroupSummary>
                                <dx:ASPxSummaryItem FieldName="Cut" ShowInGroupFooterColumn="Cut" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Quantity" ShowInGroupFooterColumn="Quantity" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Partes_Confeccionadas" ShowInGroupFooterColumn="Partes_Confeccionadas" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Line_Dfi" ShowInGroupFooterColumn="Line_Dfi" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Contado_No_Enviado_Planta" ShowInGroupFooterColumn="Contado_No_Enviado_Planta" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="En_Intex" ShowInGroupFooterColumn="En_Intex" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Piezas_Recibidas_en_Empaque" ShowInGroupFooterColumn="Piezas_Recibidas_en_Empaque" SummaryType="Sum" DisplayFormat="{0}" />                                
                            </GroupSummary>--%>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar1" runat="server"></dx:ASPxGridViewExporter>
                    </div>

                </div>
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

