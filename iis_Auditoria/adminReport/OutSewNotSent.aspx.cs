﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_OutSewNotSent : System.Web.UI.Page
{
    string a = "---";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            fecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["dt"] = null;
        }
    }

    void carga()
    {
        try
        {
            if (drptype.SelectedValue == "0")
            {
                Session["dt"] = null;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio1();", true);
            }
            else if (drptype.SelectedValue == "1")
            {
                var a = OrderDetailDA.sent_not_sent_Open_Cut(Convert.ToInt32(drplanta.SelectedValue), Convert.ToDateTime(fecha1.Text), Convert.ToDateTime(fecha2.Text));                
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                gridsent.GroupBy(gridsent.Columns["Linea"]);
            }
            else if (drptype.SelectedValue == "2")
            {
                var a = OrderDetailDA.sent_not_sent_close_Cut(Convert.ToInt32(drplanta.SelectedValue), Convert.ToDateTime(fecha1.Text), Convert.ToDateTime(fecha2.Text));                
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                gridsent.GroupBy(gridsent.Columns["Linea"]);
            }
            else if (drptype.SelectedValue == "3")
            {
                var a = OrderDetailDA.sent_not_sent(Convert.ToInt32(drplanta.SelectedValue), Convert.ToDateTime(fecha1.Text), Convert.ToDateTime(fecha2.Text));                
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                gridsent.GroupBy(gridsent.Columns["Linea"]);
            }
            else
            {
                Session["dt"] = null;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
            }
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    void carga2()
    {
        try
        {
            if (drptype.SelectedValue == "0")
            {
                Session["dt"] = null;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio1();", true);
            }
            else if (drptype.SelectedValue == "1")
            {
                var a = OrderDetailDA.sent_not_sent_Open_Cut2(Convert.ToInt32(drplanta.SelectedValue));                
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                gridsent.GroupBy(gridsent.Columns["Linea"]);
            }
            else if (drptype.SelectedValue == "2")
            {
                var a = OrderDetailDA.sent_not_sent_close_Cut2(Convert.ToInt32(drplanta.SelectedValue));                
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                gridsent.GroupBy(gridsent.Columns["Linea"]);
            }
            else if (drptype.SelectedValue == "3")
            {
                var a = OrderDetailDA.sent_not_sent2(Convert.ToInt32(drplanta.SelectedValue));                
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                gridsent.GroupBy(gridsent.Columns["Linea"]);
            }
            else
            {
                Session["dt"] = null;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            string r = rd1.SelectedValue;

            switch (r)
            {
                case "1":
                    carga2();
                    break;

                case "2":
                    carga();
                    break;

                default: break;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {
            export.GridViewID = "gridsent";
            export.FileName = "Reporte Send Not Send " + DateTime.Now;
            export.WriteXlsxToResponse(new XlsxExportOptionsEx { ExportType = ExportType.WYSIWYG });
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void gridsent_DataBinding(object sender, EventArgs e)
    {
        try
        {
            gridsent.DataSource = Session["dt"];
            gridsent.GroupBy(gridsent.Columns["Linea"]);
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void gridsent_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridsent.DataBind();
    }
}