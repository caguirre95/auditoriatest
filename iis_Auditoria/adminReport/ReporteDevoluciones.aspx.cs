﻿using DevExpress.Web;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_ReporteDevoluciones : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["devolucion"] = null;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {

        //exporterDev.GridViewID = "grdDev";      

        //DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

        //DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        //link1.Component = exporterDev;       

        //DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        //compositeLink.Links.AddRange(new object[] { link1 });

        //compositeLink.CreatePageForEachLink();

        //using (MemoryStream stream = new MemoryStream())
        //{

        //    XlsxExportOptions options = new XlsxExportOptions();

        //    options.ExportMode = XlsxExportMode.SingleFilePageByPage;
        //    options.SheetName = "Historial de Devoluciones";
        //    compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
        //    Response.Clear();
        //    Response.Buffer = false;
        //    Response.AppendHeader("Content-Type", "application/xlsx");
        //    Response.AppendHeader("Content-Transfer-Encoding", "binary");
        //    Response.AppendHeader("Content-Disposition", "attachment; filename=Historial Devoluciones.xlsx");
        //    Response.BinaryWrite(stream.ToArray());
        //    Response.End();
        //}

        //ps.Dispose();

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {

        if (HiddenFieldCortesId.Value != "")
        {

            Session["devolucion"] = null;
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            string json = "[" + HiddenFieldCortesId.Value + "]";

            var result = json_serializer.Deserialize<List<order>>(json);


            var r = result.Select(x => x.idorder).ToArray();

            var cadena = string.Join(",", r);

            //Console.WriteLine(cadena);

            var dt = OrderDetailDA.ReportDevolucionDeCorte(cadena);
            HiddenFieldCortesId.Value = "";
            Session["devolucion"] = dt;
            grdDev.DataBind();
            grdDev.ExpandAll();
            grdDev.DetailRows.ExpandAllRows();
        }
    }

    protected void grdDev_DataBinding(object sender, EventArgs e)
    {
        grdDev.DataSource = Session["devolucion"];
        grdDev.ExpandAll();
        grdDev.DetailRows.ExpandAllRows();
    }

    protected void grdDev_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdDev.DataBind();        
    }

    protected void grdRepar_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        int id = (int)detailGrid.GetMasterRowKeyValue();

        var dt = OrderDetailDA.ReportHistDevReparXidCorte(id);

        detailGrid.DataSource = dt;
    }

    protected void grdRepro_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        int id = (int)detailGrid.GetMasterRowKeyValue();

        var dt = OrderDetailDA.ReportHistDevReproXidCorte(id);

        detailGrid.DataSource = dt;
    }

    protected void grdDetRepar_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        int id = (int)detailGrid.GetMasterRowKeyValue();

        var dt = OrderDetailDA.ReportHistDevReparXidCorteDetalle(id);

        detailGrid.DataSource = dt;
    }

    protected void grdDetRepro_BeforePerformDataSelect(object sender, EventArgs e)
    {
        ASPxGridView detailGrid = (ASPxGridView)sender;
        int id = (int)detailGrid.GetMasterRowKeyValue();

        var dt = OrderDetailDA.ReportHistDevReproXidCorteDetalle(id);

        detailGrid.DataSource = dt;
    }

    public class order
    {
        public int idorder { get; set; }
        public string porder { get; set; }
    }

    protected void exportarbtn_Click(object sender, EventArgs e)
    {
        exp1.GridViewID = "grdDev";

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link1.Component = exp1;

        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link1 });

        compositeLink.CreatePageForEachLink();

        using (MemoryStream stream = new MemoryStream())
        {

            XlsxExportOptions options = new XlsxExportOptions();

            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
            options.SheetName = "HistorialdeCorte";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=Historial de Corte.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        ps.Dispose();
    }
}