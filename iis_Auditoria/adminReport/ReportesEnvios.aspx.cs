﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_ReportesEnvios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void linkWeek_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportWeek.aspx");
    }

    protected void lnkPackL_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportPackingList.aspx");
    }

    protected void lnkBihorario_Click(object sender, EventArgs e)
    {

        Response.Redirect("/adminReport/ReportBio.aspx");
    }

    protected void LinkCancelados_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/OutSewNotSent.aspx");
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/HistorialCorte.aspx");
    }
}