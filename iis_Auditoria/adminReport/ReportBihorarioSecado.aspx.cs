﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_ReportBihorarioSecado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtdate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["dtbs"] = null;
        }
    }

    void GenerarBihoSecado()
    {
        try
        {
            var bihor1 = OrderDetailDA.BihorarioSecadoIntex(Convert.ToDateTime(txtdate.Text));
            Session["dtbs"] = bihor1;
            grdbihoSec.DataSource = Session["dtbs"];
            grdbihoSec.DataBind();
            grdbihoSec.GroupBy(grdbihoSec.Columns["estilo"]);
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            GenerarBihoSecado();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {        

        exportar.GridViewID = "grdbihoSec";

        DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

        DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
        link1.Component = exportar;


        DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
        compositeLink.Links.AddRange(new object[] { link1 });

        compositeLink.CreateDocument();

        using (MemoryStream stream = new MemoryStream())
        {
            XlsxExportOptions options = new XlsxExportOptions();

            options.ExportMode = XlsxExportMode.SingleFile;
            options.SheetName = "BihorarioSecado";
            compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", "application/xlsx");
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", "attachment; filename=BihorarioSecado.xlsx");
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        ps.Dispose();
    }

    protected void grdbihoSec_DataBinding(object sender, EventArgs e)
    {
        if (Session["dtbs"] != null)
        {
            grdbihoSec.DataSource = Session["dtbs"];            
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio('" + txtdate.Text + "');", true);
        }
    }

    protected void grdbihoSec_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdbihoSec.DataBind();
    }
}