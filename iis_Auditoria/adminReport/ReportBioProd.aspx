﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportBioProd.aspx.cs" Inherits="ReportBioProd" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script>
        function vacio(fecha) {
            swal({
                title: 'Por Favor!',
                text: "No hay datos para mostrar en esta fecha : " + fecha,
                type: 'info'
            });
        }

        function vacio1() {
            swal({
                title: 'Por Favor!',
                text: " Ingrese los campos de manera correcta",
                type: 'info'
            });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtdate.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });

        function exito() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
    </script>

    <style>
        .m {
            margin-top:10px;
        }
    </style>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Reporte Bihorario Produccion</strong></div>
            <div class="panel-body">                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <div class="panel panel-default">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <strong>Planta</strong>
                                <asp:DropDownList ID="drplanta" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">Select...</asp:ListItem>
                                    <asp:ListItem Value="1">Planta 1</asp:ListItem>
                                    <asp:ListItem Value="2">Planta 3</asp:ListItem>
                                    <asp:ListItem Value="3">Planta 6</asp:ListItem>
                                    <asp:ListItem Value="4">Planta 7</asp:ListItem>
                                    <asp:ListItem Value="5">All Plants</asp:ListItem>
                                </asp:DropDownList>
                                
                                <strong>Fecha</strong>
                                <asp:TextBox ID="txtdate" runat="server" CssClass="form-control">
                                </asp:TextBox>
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <div class="panel panel-default">
                            <div class="panel-heading">Comandos</div>
                            <div class="panel-body">
                                <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info form-control" Text="Generar" OnClick="btngenerar_Click" />
                                <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success form-control m" Text="Pass to excel" OnClick="btnexcel_Click" />
                                <asp:Button ID="btnLimpiar" runat="server" CssClass="btn btn-primary form-control m" Text="Limpiar" OnClick="btnLimpiar_Click" />
                                <asp:Button ID="btnsent" runat="server" CssClass="btn btn-default form-control m" Text="Sent to Message" OnClick="btnsent_Click" Visible="false" />
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <dx:ASPxGridView ID="grdbiho" runat="server" KeyFieldName="id" Width="100%" Theme="Metropolis" OnCustomUnboundColumnData="grdbiho_CustomUnboundColumnData" 
                            SettingsBehavior-AllowSort="true" OnDataBinding="grdbiho_DataBinding" OnCustomCallback="grdbiho_CustomCallback">
                            <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                            <Columns>

                                <dx:GridViewBandColumn Caption="CONTROL INTERNO DE ENTREGAS DE LINEA A CALIDAD" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Responsable:">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Planta" Caption="Planta" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Line" Caption="Line" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>                                                
                                                <dx:GridViewDataTextColumn FieldName="Quantity" Caption="Quantity" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="POrder" Caption="Cut" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="09:00am" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Firma:">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="11:00am" VisibleIndex="5">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="01:00pm" VisibleIndex="6">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="03:00pm" VisibleIndex="7">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="04:50pm" VisibleIndex="8">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="06:50pm" VisibleIndex="9">
                                                </dx:GridViewDataTextColumn>                                                
                                            </Columns>
                                        </dx:GridViewBandColumn>

                                    </Columns>
                                </dx:GridViewBandColumn>

                                <dx:GridViewBandColumn Caption="ROC 01 REG PRO 0020" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Fecha de Creacion: Junio de 2017" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="TotalBihorario" UnboundType="Integer" VisibleIndex="11">
                                                    <PropertiesTextEdit DisplayFormatString="{0}" />
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Version: 01" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Deficit" VisibleIndex="10">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>

                                    </Columns>
                                </dx:GridViewBandColumn>

                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                            <Settings ShowFooter="true" HorizontalScrollBarMode="Visible" />
                            <SettingsPager PageSize="50" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="POrder" SummaryType="Count" DisplayFormat="{0}" />                                
                                <dx:ASPxSummaryItem FieldName="TotalBihorario" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <GroupSummary>
                                <dx:ASPxSummaryItem FieldName="POrder" ShowInGroupFooterColumn="POrder" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="09:00am" ShowInGroupFooterColumn="09:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="11:00am" ShowInGroupFooterColumn="11:00am" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="01:00pm" ShowInGroupFooterColumn="01:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="03:00pm" ShowInGroupFooterColumn="03:00pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="04:50pm" ShowInGroupFooterColumn="04:50pm" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="06:50pm" ShowInGroupFooterColumn="06:50pm" SummaryType="Sum" DisplayFormat="{0}" />                                
                                <dx:ASPxSummaryItem FieldName="TotalBihorario" ShowInGroupFooterColumn="TotalBihorario" SummaryType="Sum" DisplayFormat="{0}" />
                            </GroupSummary>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar" runat="server"></dx:ASPxGridViewExporter>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

