﻿using DevExpress.Export;
using DevExpress.XtraPrinting;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_EnviadoNoenviadoL : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fecha1.Text = DateTime.Now.ToString("yyyy-MM-dd");
            fecha2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            Session["dt"] = null;
            Session["inv"] = null;
        }
    }

    protected void btngenerar_Click(object sender, EventArgs e)
    {
        try
        {
            string r = rd1.SelectedValue;

            switch (r)
            {
                case "1":
                    carga2();
                    panel1.Visible = false;
                    break;

                case "2":
                    carga();
                    panel1.Visible = false;
                    break;

                case "3":
                    carga3();
                    break;

                default: break;
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    void carga()
    {
        try
        {
            if (drptype.SelectedValue == "0")
            {
                Session["dt"] = null;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio1();", true);
            }
            else if (drptype.SelectedValue == "1")
            {
                var a = OrderDetailDA.CorteAbiertoIntex(Convert.ToDateTime(fecha1.Text), Convert.ToDateTime(fecha2.Text));
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();                
            }
            else if (drptype.SelectedValue == "2")
            {
                var a = OrderDetailDA.CorteCerradoIntex(Convert.ToDateTime(fecha1.Text), Convert.ToDateTime(fecha2.Text));
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();                
            }
            else if (drptype.SelectedValue == "3")
            {
                var a = OrderDetailDA.GeneralIntex(Convert.ToDateTime(fecha1.Text), Convert.ToDateTime(fecha2.Text));
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();                
            }
            else
            {
                Session["dt"] = null;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
            }
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    void carga2()
    {
        try
        {
            if (drptype.SelectedValue == "0")
            {
                Session["dt"] = null;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "vacio1();", true);
            }
            else if (drptype.SelectedValue == "1")
            {
                var a = OrderDetailDA.CorteAbiertoIntex2();
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();                
            }
            else if (drptype.SelectedValue == "2")
            {
                var a = OrderDetailDA.CorteCerradoIntex2();
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();                
            }
            else if (drptype.SelectedValue == "3")
            {
                var a = OrderDetailDA.GeneralIntex2();
                Session["dt"] = a;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();                
            }
            else
            {
                Session["dt"] = null;
                gridsent.DataSource = Session["dt"];
                gridsent.DataBind();
            }
        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    void carga3()
    {
        try
        {
            panel1.Visible = true;

            var a = OrderDetailDA.CorteAbiertoIntex2();
            Session["dt"] = a;
            gridsent.DataSource = Session["dt"];
            gridsent.DataBind();

            var b = OrderDetailDA.InventarioIntex();
            Session["inv"] = b;
            grdInv.DataSource = Session["inv"];
            grdInv.DataBind();

        }
        catch (Exception ex)
        {
            string m = ex.Message;
        }
    }

    protected void btnexcel_Click(object sender, EventArgs e)
    {
        try
        {

            string r = rd1.SelectedValue;

            switch (r)
            {
                case "1":
                case "2":
                    export.GridViewID = "gridsent";
                    exportar1.GridViewID = "grdInv";

                    DevExpress.XtraPrinting.PrintingSystemBase ps = new DevExpress.XtraPrinting.PrintingSystemBase();

                    DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link1 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps);
                    link1.Component = export;                   

                    DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps);
                    compositeLink.Links.AddRange(new object[] { link1 });

                    compositeLink.CreatePageForEachLink();

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XlsxExportOptions options = new XlsxExportOptions();
                        options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        options.SheetName = "Reporte Intex";
                        compositeLink.PrintingSystemBase.ExportToXlsx(stream, options);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/xlsx");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", "attachment; filename=Intex" + ".xlsx");
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }
                    ps.Dispose();
                    break;

                case "3":
                    export.GridViewID = "gridsent";
                    exportar1.GridViewID = "grdInv";

                    DevExpress.XtraPrinting.PrintingSystemBase ps1 = new DevExpress.XtraPrinting.PrintingSystemBase();

                    DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link11 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps1);
                    link11.Component = export;

                    DevExpress.XtraPrintingLinks.PrintableComponentLinkBase link2 = new DevExpress.XtraPrintingLinks.PrintableComponentLinkBase(ps1);
                    link2.Component = exportar1;

                    DevExpress.XtraPrintingLinks.CompositeLinkBase compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLinkBase(ps1);
                    compositeLink1.Links.AddRange(new object[] { link11, link2 });

                    compositeLink1.CreatePageForEachLink();

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XlsxExportOptions options = new XlsxExportOptions();
                        options.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        options.SheetName = "Reporte Intex";
                        compositeLink1.PrintingSystemBase.ExportToXlsx(stream, options);
                        Response.Clear();
                        Response.Buffer = false;
                        Response.AppendHeader("Content-Type", "application/xlsx");
                        Response.AppendHeader("Content-Transfer-Encoding", "binary");
                        Response.AppendHeader("Content-Disposition", "attachment; filename=Intex" + ".xlsx");
                        Response.BinaryWrite(stream.ToArray());
                        Response.End();
                    }
                    ps1.Dispose();

                    break;

                default: break;
            }            
        }
        catch (Exception)
        {
            //throw;
        }
    }

    protected void gridsent_DataBinding(object sender, EventArgs e)
    {
        gridsent.DataSource = Session["dt"];
    }

    protected void gridsent_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridsent.DataBind();
    }

    protected void grdInv_DataBinding(object sender, EventArgs e)
    {
        grdInv.DataSource = Session["inv"];
    }

    protected void grdInv_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grdInv.DataBind();
    }

    protected void btnl_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/EnviadoNoenviadoL.aspx");
    }
}