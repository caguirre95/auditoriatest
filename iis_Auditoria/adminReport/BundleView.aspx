﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BundleView.aspx.cs" Inherits="adminReport_BundleView" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    var user = document.getElementById("<%= HiddenFielduser.ClientID %>").value;
                    $.ajax({
                        url: "BundleView.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "','user':'" + user + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,

                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);


                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });
    </script>

 <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <style>
        .mar {
            margin-top: 19px;
        }
		
		 .m{
            padding:0px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <div class="">
            <div class="panel panel-primary " style="margin-top:10px;">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Measurement suggestion </strong></h3>
                </div>
                <div class="panel-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="HiddenFielduser" runat="server" />

                            <div class="col-lg-12 m">
                                <div class="col-lg-4">
                                    Corte:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <asp:LinkButton Text="" OnClick="ViewBundle_Click" CssClass="btn btn-default" ID="ViewBundle" runat="server"><i class="fa fa-search" aria-hidden="true"></i> </asp:LinkButton>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" required="true" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-info mar" OnClick="btnlimpiar_Click" Text="Limpiar" Width="150px" />
                                </div>

                                <div class="col-lg-4">
                                    <asp:HiddenField ID="hfidPorder" runat="server" />
                                </div>
                            </div>
                            <%--  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    Bihorario
                                    <asp:DropDownList ID="drpBihorario" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Select..." Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 Bihorario" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 Bihorario" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 Bihorario" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 Bihorario" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5 Bihorario" Value="5"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    Envio Dia
                                    <asp:DropDownList ID="drpEnvioDia" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="Select..." Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 Envio" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 Envio" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 Envio" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 Envio" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5 Envio" Value="5"></asp:ListItem>
                                    </asp:DropDownList>
                                </div> --%>
                            <div class="col-lg-12 mar">
                                <dx:ASPxGridView ID="gridViewbld" runat="server" KeyFieldName="id" Width="100%" Theme="Metropolis" OnDataBinding="gridViewbld_DataBinding" OnCustomCallback="gridViewbld_CustomCallback">
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="Bundle" VisibleIndex="0">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Quantity" VisibleIndex="2">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Talla" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Acumuluado" VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Usuario" VisibleIndex="5">
                                        </dx:GridViewDataTextColumn>
                                        <%-- <dx:GridViewDataTextColumn FieldName="Bihorario" VisibleIndex="6">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FechaHora" VisibleIndex="7">
                                        </dx:GridViewDataTextColumn>--%>
                                    </Columns>
                                    <Settings ShowGroupFooter="VisibleAlways" />
                                    <Settings ShowFooter="true" />
                                    <SettingsPager PageSize="100" />
                                    <TotalSummary>
                                        <dx:ASPxSummaryItem FieldName="Bundle" SummaryType="Count" DisplayFormat="{0}" />
                                        <dx:ASPxSummaryItem FieldName="Acumuluado" SummaryType="Sum" DisplayFormat="{0}" />
                                    </TotalSummary>
                                    <GroupSummary>
                                    </GroupSummary>
                                </dx:ASPxGridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

