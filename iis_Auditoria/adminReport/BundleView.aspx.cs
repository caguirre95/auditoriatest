﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class adminReport_BundleView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.User.Identity.IsAuthenticated == true)
            {
                if (Page.User.IsInRole("PackAdmon"))
                {
                    HiddenFielduser.Value = HttpContext.Current.User.Identity.Name;
                    Session["bld"] = null;
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre, string user)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();
            //string a = HttpContext.Current.User.Identity.Name;
            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select top 10 p.Id_Order,p.POrder,s.Style"
                                          + " from POrder p join Style s on p.Id_Style = s.Id_Style"
                                          + " where p.POrder like '%" + pre + "%' and p.Id_Planta in"
                                          + " (select u.Id_Planta from UserPlanta u where u.nameMemberShip = '" + user + "')", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][1].ToString();
                obj.style = dt.Rows[i][2].ToString();

                list.Add(obj);
            }
            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public int idorder { get; set; }
        public string porder { get; set; }
        public string style { get; set; }
    }

    void ver()
    {
        try
        {
            if (hfidPorder.Value != "")
            {
                 string vista = " select b.Bld as Bundle, b.Quantity as Quantity, b.Size as Talla, e.acumulado as Acumuluado, h.usuarioIngresoCanti as Usuario"
                             + " from tblhistoricoenvio h"
                             + " left join tblEnviosbulto e on h.idEnvio = e.idEnvio"
                             + " join Bundle b on b.Id_Bundle = e.idBulto"
                             + " join POrder p on p.Id_Order = b.Id_Order"                            
                             + " where p.Id_Order = " + hfidPorder.Value
                             + " group by b.Bld, b.Quantity, b.Size, e.acumulado, h.usuarioIngresoCanti"
                             + " order by b.Bld asc";
                DataTable vistabld = DataAccess.Get_DataTable(vista);
                Session["bld"] = vistabld;
                gridViewbld.DataSource = Session["bld"];
                gridViewbld.DataBind();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void ViewBundle_Click(object sender, EventArgs e)
    {
        try
        {
            ver();
        }
        catch (Exception ex)
        {
            string mesnaje = ex.Message;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/BundleView.aspx");
    }

    protected void gridViewbld_DataBinding(object sender, EventArgs e)
    {
        gridViewbld.DataSource = Session["bld"];
    }

    protected void gridViewbld_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridViewbld.DataBind();
    }
}