﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PLgeneral.aspx.cs" Inherits="adminReport_PLgeneral" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script>
        function vacio() {
            swal({
                title: 'Por Favor!',
                text: "Seleccione una opcion!!",
                type: 'info'
            });
        }
    </script>

    <script type="text/javascript">       

        function exito() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }

        $(document).ready(function () {
            $(function () {
                $('#<%=fecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>
    <link href="../jquery-ui.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <style>
        .mr {
            margin-bottom: 10px;
            padding-bottom: 10px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <div class="container">
        <div class="panel panel-primary" style="margin-top: 10px;">
            <div class="panel-heading"><strong>Packing List</strong></div>
            <div class="panel-body">

                <asp:SiteMapPath ID="map1" runat="server" PathSeparator=" > " RenderCurrentNodeAsLink="false" CssClass="form-control"></asp:SiteMapPath>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <strong>Fecha</strong>
                    <asp:TextBox ID="fecha1" runat="server" Width="200px" CssClass="form-control"></asp:TextBox>
                    <strong>Lavado - No Lavado</strong>
                    <asp:DropDownList ID="drpla" runat="server" Width="200px" CssClass="form-control">
                        <asp:ListItem Value="2" Text="Select..."></asp:ListItem>
                        <asp:ListItem Value="0" Text="No Lavado"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Lavado"></asp:ListItem>
                    </asp:DropDownList>
                    <strong>Planta</strong>
                    <asp:DropDownList ID="drpplant" runat="server" Width="200px" CssClass="form-control">
                        <asp:ListItem Value="0">Select...</asp:ListItem>
                        <asp:ListItem Value="1">Rocedes 1</asp:ListItem>
                        <asp:ListItem Value="2">Rocedes 3</asp:ListItem>
                        <asp:ListItem Value="3">Rocedes 6</asp:ListItem>
                        <asp:ListItem Value="4">Rocedes 7</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <div style="margin-top: 5px;">
                        <asp:Button ID="btngenerar" runat="server" CssClass="btn btn-info mr" Text="Generar" Width="200px" OnClick="btngenerar_Click" />
                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-info mr" Text="Limpiar" Width="200px" OnClick="Button1_Click" />
                        <asp:Button ID="btnexcel" runat="server" CssClass="btn btn-success mr" Text="Pass to excel" Width="200px" OnClick="btnexcel_Click" />
                    </div>
                    <div style="margin-top: 20px">
                        <dx:ASPxGridView ID="gridpack" runat="server" KeyFieldName="id" Width="100%" Theme="Metropolis" SettingsBehavior-AllowSort="false" OnDataBinding="gridpack_DataBinding" OnCustomCallback="gridpack_CustomCallback">
                            <SettingsBehavior AllowSelectSingleRowOnly="true" AutoExpandAllGroups="true" />
                            <Columns>

                                <dx:GridViewBandColumn Caption="CONTROL OF SHIPMENTS AND DEFICIT IN LAUNDRY" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Responsable:">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Descripcion" Caption="Descripcion" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Color" Caption="Color" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Planta" Caption="Planta" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Linea" Caption="Line" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="id" Caption="id" Visible="false">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Cut" Caption="Cut" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Firma">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Style" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>                                                
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Fecha de creación: Junio de 2017" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Quantity" VisibleIndex="5">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Enviadas" VisibleIndex="6">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>

                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewBandColumn Caption="ROC 01 REG PRO 0021" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewBandColumn Caption="Version 01" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="ultimoenvio" Caption="Ultimo Envio" VisibleIndex="7">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ultimafechaEn" Caption="Ultima Fecha Envio" VisibleIndex="8" PropertiesTextEdit-DisplayFormatString="dd/MM/yyyy">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Exceso" VisibleIndex="8" Visible="false">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="DeficitEnvio" Caption="Deficit Envio" VisibleIndex="9">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Estatus" Caption="Estatus" VisibleIndex="10">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>

                                    </Columns>
                                </dx:GridViewBandColumn>

                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" ShowGroupPanel="true" />
                            <Settings ShowFooter="true" VerticalScrollBarMode="Visible" />
                            <SettingsPager PageSize="100" />
                            <TotalSummary>
                                <dx:ASPxSummaryItem FieldName="Cut" SummaryType="Count" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Quantity" SummaryType="Sum" DisplayFormat="{0}" />
                                <dx:ASPxSummaryItem FieldName="Enviadas" SummaryType="Sum" DisplayFormat="{0}" />
                            </TotalSummary>
                            <GroupSummary>
                                <dx:ASPxSummaryItem FieldName="Enviadas" ShowInGroupFooterColumn="Enviadas" SummaryType="Count" DisplayFormat="{0}" />
                            </GroupSummary>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="exportar1" runat="server"></dx:ASPxGridViewExporter>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>

