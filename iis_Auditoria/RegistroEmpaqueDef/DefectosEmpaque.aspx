﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DefectosEmpaque.aspx.cs" Inherits="RegistroEmpaqueDef_DefectosEmpaque" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link href="../jquery-ui.css" rel="stylesheet" />
    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>

    <script>
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "DefectosEmpaque.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    cantidad: item.cantidad,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porder);
                    $('#<%=txtestilo.ClientID%>').val(ui.item.style);
                    $('#<%=txtcantidad.ClientID%>').val(ui.item.cantidad);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });

    </script>

    <script type="text/javascript">
        function alertSeleccionarMedida(mess, tipo) {
            swal({
                title: "",
                text: mess,
                type: tipo
            });
        }
        function successalert() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
    </script>
    <script type="text/javascript">
        function warningalert() {
            swal({
                title: "¿Estas seguro?",
                text: "¿Deseas ingresar este registro?",
                type: "warning"
            })
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <br />
        <div class="col-lg-12">

            <div class="panel panel-primary">
                <div class="panel-heading">Registro de Defectos Empaque</div>
                <div class="panel-body">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <div class="col-lg-4">
                                Corte
                                <asp:TextBox ID="txtcorte" runat="server" CssClass="form-control" AutoCompleteType="Disabled" OnTextChanged="txtcorte_TextChanged" AutoPostBack="true"></asp:TextBox>
                                Estilo
                                <asp:TextBox ID="txtestilo" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                Cantidad 
                                <asp:TextBox ID="txtcantidad" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>

                            <div class="col-lg-4">
                                Auditor
                                <asp:TextBox ID="txtaudit" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                Fecha
                                <asp:TextBox ID="txtfecha" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                Cantidad Defecto
                                <asp:TextBox ID="txtcantdef" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="col-lg-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <asp:Button ID="btnlimpiar" runat="server" CssClass="btn btn-success btn-block" Text="Limpiar" OnClick="btnlimpiar_Click" />
                                <br />
                                <asp:Button ID="btndef" runat="server" CssClass="btn btn-warning btn-block" Text="Reporte Defectos" OnClick="btndef_Click" />
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <hr />
                        <div class="panel panel-default">
                            <div class="panel-heading">Ingreso de Informacion</div>
                            <div class="panel-body">

                                <div class="col-lg-8">

                                    <div id="Grid" runat="server">
                                        <asp:GridView ID="gridAddDefectos" runat="server" CssClass="table table-hover table-striped" GridLines="None" Width="100%" AutoGenerateColumns="False" OnRowDataBound="gridAddDefectos_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Defecto">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddldefectos" CssClass="form-control" runat="server"></asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Operación">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlposicion" CssClass="form-control" runat="server"></asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cantidad">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtcantidaddef" CssClass="form-control" runat="server" Enabled="false" Text="1"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblno" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>

                                </div>

                                <div class="col-lg-4">

                                    <div class="panel panel-info">
                                        <div class="panel-heading">Opciones</div>
                                        <div class="panel-body">
                                            <asp:Button ID="btnguardar" runat="server" CssClass="btn btn-primary btn-block" Text="Guardar" OnClick="btnguardar_Click" />
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>


                </div>
            </div>

        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

