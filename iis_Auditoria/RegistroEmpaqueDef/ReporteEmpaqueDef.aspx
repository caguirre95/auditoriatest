﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReporteEmpaqueDef.aspx.cs" Inherits="RegistroEmpaqueDef_ReporteEmpaqueDef" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link href="../jquery-ui.css" rel="stylesheet" />
    <script src="../jquery-1.9.1.js"></script>
    <script src="../jquery-ui.js"></script>

    <script>
        $(function () {
            $('#<%=txtcorte.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "DefectosEmpaque.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    cantidad: item.cantidad,
                                    style: item.style,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtcorte.ClientID%>').val(ui.item.porder);
                    $('#<%=txtestilo.ClientID%>').val(ui.item.style);
                    $('#<%=txtcantidad.ClientID%>').val(ui.item.cantidad);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#<%=txtfecha1.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
            $(function () {
                $('#<%=txtfecha2.ClientID%>').datepicker({
                    dateFormat: "yy-mm-dd"
                })
            })
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <br />

    <div class="container">

        <div class="col-lg-12">

            <div class="panel panel-primary">
                <div class="panel-heading">Reporte Defecto Empaque</div>
                <div class="panel-body">

                    <div class="col-lg-3">
                        <div class="panel panel-info">
                            <div class="panel-heading">Elegir</div>
                            <div class="panel-body">
                                <asp:RadioButtonList runat="server" ID="rdb1">
                                    <asp:ListItem Value="1">Corte</asp:ListItem>
                                    <asp:ListItem Value="2">Fecha</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        Corte
                        <asp:TextBox ID="txtcorte" runat="server" CssClass="form-control" placeholder="Digite el Corte..." AutoCompleteType="Disabled"></asp:TextBox>
                        Estilo
                        <asp:TextBox ID="txtestilo" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        Cantidad 
                        <asp:TextBox ID="txtcantidad" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                    </div>

                    <div class="col-lg-3">
                        Fecha Inicial
                        <dx1:ASPxDateEdit runat="server" ID="txtfecha1" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                        Fecha Final
                        <dx1:ASPxDateEdit runat="server" ID="txtfecha2" Theme="Moderno" CalendarProperties-ButtonStyle-CssClass="form-control" Width="100%"></dx1:ASPxDateEdit>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel panel-info">
                            <div class="panel-heading">Opciones</div>
                            <div class="panel-body">
                                <asp:Button ID="btnbuscar" runat="server" Text="Buscar >" CssClass="btn btn-success btn-block" OnClick="btnbuscar_Click" />
                                <br />
                                <asp:Button ID="btnlimpiar" runat="server" Text="Limpiar" CssClass="btn btn-info btn-block" OnClick="btnlimpiar_Click" />
                                <br />
                                <asp:Button ID="btnregresar" runat="server" Text="Regresar" CssClass="btn btn-warning btn-block" OnClick="btnregresar_Click" />
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <hr />
                        <dx1:ASPxGridView ID="grddfem" runat="server" AutoGenerateColumns="false" Width="100%" Theme="Metropolis" KeyFieldName="IdCorte"
                                          SettingsBehavior-AllowSort="false" OnDataBinding="grddfem_DataBinding" OnCustomCallback="grddfem_CustomCallback">
                            <Columns>
                                <dx1:GridViewBandColumn Caption="Reporte de Defectos de Empaque">
                                    <Columns>
                                        <dx1:GridViewDataTextColumn FieldName="Corte"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Defecto"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Posicion"></dx1:GridViewDataTextColumn>
                                        <dx1:GridViewDataTextColumn FieldName="Total"></dx1:GridViewDataTextColumn>                                        
                                    </Columns>
                                </dx1:GridViewBandColumn>
                            </Columns>
                            <Settings VerticalScrollableHeight="300" VerticalScrollBarMode="Visible" />
                            <SettingsPager PageSize="500">
                                <PageSizeItemSettings Visible="true" ShowAllItem="true" />
                            </SettingsPager>
                            <TotalSummary>
                                <dx1:ASPxSummaryItem FieldName="Total" SummaryType="Sum" DisplayFormat="p" />
                            </TotalSummary>
                            <Styles>
                                <Header HorizontalAlign="Center" />
                                <Row HorizontalAlign="Center"></Row>
                            </Styles>
                        </dx1:ASPxGridView>
                    </div>

                </div>


            </div>



        </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

