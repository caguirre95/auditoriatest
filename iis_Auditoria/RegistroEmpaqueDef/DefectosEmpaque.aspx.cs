﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroEmpaqueDef_DefectosEmpaque : System.Web.UI.Page
{
    class listas
    {
        public string id { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["defectos"] = null;
            Session["posicion"] = null;

            txtaudit.Text = ExtraeNombre(User.Identity.Name.ToString());

            txtfecha.Text = DateTime.Now.ToShortDateString();

            carga();
            cargagrid();
        }
    }

    void carga()
    {
        DataTable dtDefecto = new DataTable();
        dtDefecto = DataAccess.Get_DataTable("select id_defectos as idDefecto,nombre as Defecto from Defectos WHERE Categoria='C' and Cliente=92 order by nombre  asc");

        DataTable dtPosicion = new DataTable();
        dtPosicion = DataAccess.Get_DataTable("select idPosicion,Posicion as Codigo from tbPosicion where Estado = 1 order by Codigo asc");

        Session["defectos"] = dtDefecto;
        Session["posicion"] = dtPosicion;

    }

    void cargagrid()
    {
        List<listas> lista = new List<listas>();

        for (int i = 1; i <= 10; i++)
        {
            var obj = new listas();
            obj.id = i.ToString();
            lista.Add(obj);
        }

        gridAddDefectos.DataSource = lista;
        gridAddDefectos.DataBind();
    }

    protected void gridAddDefectos_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddldef = (DropDownList)e.Row.FindControl("ddldefectos");
                DropDownList ddlpos = (DropDownList)e.Row.FindControl("ddlposicion");

                var dt1 = (DataTable)Session["defectos"];
                var dt2 = (DataTable)Session["posicion"];

                ddldef.DataSource = dt1;
                ddldef.DataTextField = "Defecto";
                ddldef.DataValueField = "idDefecto";
                ddldef.DataBind();
                ddldef.Items.Insert(0, "");

                ddlpos.DataSource = dt2;
                ddlpos.DataTextField = "Codigo";
                ddlpos.DataValueField = "idPosicion";
                ddlpos.DataBind();
                ddlpos.Items.Insert(0, "");

            }
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alerta('" + ex.Message + "','" + tipo + "');", true);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre)
    {
        try
        {

            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);
            SqlCommand cmd = new SqlCommand();
            cn.Open();

            cmd.Connection = cn;
            cmd.CommandText = "spdConstainPorder";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            cmd.Parameters.Add("@corte", SqlDbType.NVarChar);
            cmd.Parameters[cmd.Parameters.Count - 1].Value = pre;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porder = dt.Rows[i][0].ToString();
                obj.style = dt.Rows[i][2].ToString();
                obj.cantidad = int.Parse(dt.Rows[i][1].ToString());

                //obj.totaldef = Convert.ToUInt16(dt1.Rows[0][0].ToString());

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porder { get; set; }
        public string style { get; set; }
        public int cantidad { get; set; }
    }

    string ExtraeNombre(string userms)
    {
        var dt = DataAccess.Get_DataTable("select * from tbUsuarioSegunda where userms='" + userms + "'");

        if (dt.Rows.Count > 0)
        {
            return dt.Rows[0][1].ToString();
        }
        else
        {
            return userms;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../RegistroEmpaqueDef/DefectosEmpaque.aspx");
    }

    protected void btnguardar_Click(object sender, EventArgs e)
    {
        try
        {
            string respp = "";
            var idcorte = txtcorte.Text.Trim() == "" ? "0" : txtcorte.Text.Trim();

            var resp = OrderDetailDA.spdGuardarDefectoEmp(idcorte, 0, HttpContext.Current.User.Identity.Name);//Agregar Nombre Auditor

            if (resp > 0)
            {
                foreach (GridViewRow item in gridAddDefectos.Rows)
                {
                    DropDownList ddldef = (DropDownList)item.FindControl("ddldefectos");
                    DropDownList ddlpos = (DropDownList)item.FindControl("ddlposicion");
                    TextBox txtcant = (TextBox)item.FindControl("txtcantidaddef");

                    if (ddldef.SelectedItem.Text != "" && ddlpos.SelectedItem.Text != "")
                    {
                        var resp1 = OrderDetailDA.spdGuardarDefectoEmpDet(resp, Convert.ToInt32(ddldef.SelectedValue), Convert.ToInt32(ddlpos.SelectedValue), txtcant.Text == string.Empty ? 1 : Convert.ToInt16(txtcant.Text));

                        respp = resp1;
                    }
                }

                if (respp == "OK")
                {
                    string message = "Guardado Correctamente!!!";
                    string tipo = "success";
                    Limpiar();
                    Calculardef();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
                }
                else
                {
                    string message = "Error al Guardar Datos!!!";
                    string tipo = "warning";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
                }
            }
        }
        catch (Exception ex)
        {
            string message = ex.Message;
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + message + "','" + tipo + "');", true);
        }
    }

    void Limpiar()
    {        
        cargagrid();
    }

    protected void btndef_Click(object sender, EventArgs e)
    {
        Response.Redirect("/RegistroEmpaqueDef/ReporteEmpaqueDef.aspx");
    }

    protected void txtcorte_TextChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable dt1 = new DataTable();

            dt1 = DataAccess.Get_DataTable("SELECT ISNULL(SUM(CantidadDefecto),0) Total FROM tbDefectoEmpaqueDet dt JOIN tbDefectoEmpaque d ON d.IdDefEmpaque = dt.IdDefEmpaque WHERE d.IdCorte = '" + txtcorte.Text + "'");

            txtcantdef.Text = dt1.Rows[0][0].ToString();
        }
        catch (Exception)
        {
            throw;
        }
    }

    void Calculardef()
    {
        DataTable dt1 = new DataTable();

        dt1 = DataAccess.Get_DataTable("SELECT ISNULL(SUM(CantidadDefecto),0) Total FROM tbDefectoEmpaqueDet dt JOIN tbDefectoEmpaque d ON d.IdDefEmpaque = dt.IdDefEmpaque WHERE d.IdCorte = '" + txtcorte.Text + "'");

        txtcantdef.Text = dt1.Rows[0][0].ToString();
    }
}