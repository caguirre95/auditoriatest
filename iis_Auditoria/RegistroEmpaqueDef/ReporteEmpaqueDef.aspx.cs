﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroEmpaqueDef_ReporteEmpaqueDef : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfecha1.Text = DateTime.Now.ToShortDateString();
            txtfecha2.Text = DateTime.Now.ToShortDateString();
            Session["RDE"] = null;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/RegistroEmpaqueDef/ReporteEmpaqueDef.aspx");
    }

    protected void btnregresar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../RegistroEmpaqueDef/DefectosEmpaque.aspx");
    }

    void Corte()
    {
        if (txtcorte.Text != "")
        {
            DataTable dt1 = new DataTable();

            string query = " Select de.IdCorte Corte, d.Defecto Defecto, po.Posicion Posicion, SUM(det.CantidadDefecto) Total from tbDefectoEmpaque de"
                         + " join tbDefectoEmpaqueDet det on det.IdDefEmpaque = de.IdDefEmpaque join tbDefectos d on d.idDefecto = det.IdDefecto"
                         + " join tbPosicion po on po.idPosicion = det.IdPosicion where de.IdCorte = '" + txtcorte.Text.Trim() + "' group by de.IdCorte, d.Defecto, po.Posicion";

            dt1 = DataAccess.Get_DataTable(query);

            Session["RDE"] = dt1;
            
            grddfem.DataBind();
        }
    }

    void Fecha()
    {
        if (txtfecha1.Text != "" && txtfecha2.Text != "")
        {
            DataTable dt1 = new DataTable();

            string query = " Select de.IdCorte Corte, d.Defecto Defecto, po.Posicion Posicion, SUM(det.CantidadDefecto) Total from tbDefectoEmpaque de"
                         + " join tbDefectoEmpaqueDet det on det.IdDefEmpaque = de.IdDefEmpaque join tbDefectos d on d.idDefecto = det.IdDefecto"
                         + " join tbPosicion po on po.idPosicion = det.IdPosicion where CONVERT(date, det.Fecha) between '" + txtfecha1.Text + "' and '" + txtfecha2.Text + "' group by de.IdCorte, d.Defecto, po.Posicion";

            dt1 = DataAccess.Get_DataTable(query);

            Session["RDE"] = dt1;
            
            grddfem.DataBind();
        }
    }

    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            string id = rdb1.SelectedValue;

            switch (id)
            {
                case "1":
                    Corte();
                    break;

                case "2":
                    Fecha();
                    break;

                default:
                    break;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void grddfem_DataBinding(object sender, EventArgs e)
    {
        grddfem.DataSource = Session["RDE"]; 
    }

    protected void grddfem_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        grddfem.DataBind();
    }
}