﻿using DevExpress.Web;
using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Line : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cliente();
            l();
            rocedes();
        }

        
    }

    void cliente()
    {
        string cli = "  select c.Id_Cliente as id , c.Cliente as Cliente from Cliente c";
        DataTable cl = DataAccess.Get_DataTable(cli);

        drpcliente.DataSource = cl;
        drpcliente.DataTextField = "Cliente";
        drpcliente.DataValueField = "id";
        drpcliente.DataBind();
        drpcliente.Items.Insert(0, "Select...");

        drpc.DataSource = cl;
        drpc.TextField = "Cliente";
        drpc.ValueField = "id";
        drpc.DataBind();

        combocliente.DataSource = cl;
        combocliente.TextField = "Cliente";
        combocliente.ValueField = "id";
        combocliente.DataBind();
    }

    void l()
    {
        string cli = " select l.id_linea as id, l.numero as Linea, l.estado as Estado from Linea l where idcliente = 11 order by l.numero asc";
        DataTable cl = DataAccess.Get_DataTable(cli);

        dr.DataSource = cl;
        dr.TextField = "Linea";
        dr.ValueField = "id";
        dr.DataBind();       
    }   

    void cargalinea()
    {
        if (drpcliente.SelectedItem.Value == "Select..." || Convert.ToInt32(drpcliente.SelectedItem.Value) == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "gridcampo1();", true);
        }
        else
        {
            string carga_l = " select l.id_linea as id, l.numero as Linea, l.estado as Estado from Linea l where idcliente = " + drpcliente.SelectedItem.Value;
            DataTable linea = DataAccess.Get_DataTable(carga_l);

            gridlinea.DataSource = linea;
        }       
    }

    void rocedes()
    {
        string roc = " select p.id_planta as id, p.descripcion as Planta from  Planta p";
        DataTable roced = DataAccess.Get_DataTable(roc);

        planta.DataSource = roced;
        planta.TextField = "Planta";
        planta.ValueField = "id";
        planta.DataBind();
    }

    protected void drpcliente_SelectedIndexChanged(object sender, EventArgs e)
    {
        gridlinea.DataBind();
    }

    protected void grid_linea_DataBinding(object sender, EventArgs e)
    {
        cargalinea();
    }

    protected void grid_linea_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridlinea.DataBind();
    }

    protected void btnestadoact_Click(object sender, EventArgs e)
    {
        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
        string userId = mu.ProviderUserKey.ToString();
        ASPxCheckBox chklineact = new ASPxCheckBox();
        string ok = string.Empty;

        for (int i = 0; i <= gridlinea.VisibleRowCount; i++)
        {
            int id = Convert.ToInt32(gridlinea.GetRowValues(i, "id"));

            if (gridlinea.FindRowCellTemplateControl(i, (GridViewDataColumn)gridlinea.Columns["Estado"], "chklineact") != null)
            {
                chklineact = (ASPxCheckBox)gridlinea.FindRowCellTemplateControl(i, (GridViewDataColumn)gridlinea.Columns["Estado"], "chklineact");
                ((IPostBackDataHandler)chklineact).LoadPostData("chklineact", Request.Form);

                try
                {
                    ok = DataAccess.UpdateEstadoLinea(id,chklineact.Checked);
                    if (ok == "OK")
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki_bad();", true);
                    }
                }
                catch (Exception ex)
                {
                    MailMessage mailObj = new MailMessage("soporte@rocedes.com.ni", "dmartinez@rocedes.com.ni", "Sistema Auditoria", ex.ToString() + "Usuario : " + mu.UserName);
                    SmtpClient SMTPServer = new SmtpClient("rocedes.com.ni");
                    SMTPServer.Send(mailObj);
                }
            }
        }
    }  

    protected void btnaceptaredit_Click(object sender, EventArgs e)
    {
        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
        string userId = mu.ProviderUserKey.ToString();

        try
        {
            if (Convert.ToInt32(dr.SelectedItem.Value) <= 0 || Convert.ToInt32(drpc.SelectedItem.Value) <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "gridcampo2();", true);
                txtnewline.Text = "";
                drpc.Text = "";
                dr.Text = "";
            }
            else
            {
                //string a = " select id_linea from Linea where id_linea = " + dr.SelectedItem.Value;
                //DataTable ab = DataAccess.Get_DataTable(a);
                //int id = Convert.ToInt32(ab.Rows[0][0]);
                DataAccess.UpdateLinea(Convert.ToInt32(dr.SelectedItem.Value), txtnewline.Text, Convert.ToInt32(drpc.SelectedItem.Value), lineact.Checked);
                txtnewline.Text = "";
                drpc.Text = "";
                dr.Text = "";                
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki();", true);
            }           
        }
        catch (Exception ex)
        {
            MailMessage mailObj = new MailMessage("soporte@rocedes.com.ni", "dmartinez@rocedes.com.ni", "Sistema Auditoria", ex.ToString() + "Usuario : " + mu.UserName);
            SmtpClient SMTPServer = new SmtpClient("rocedes.com.ni");
            SMTPServer.Send(mailObj);
        }
    }

    protected void btnagregarnewline_Click(object sender, EventArgs e)
    {
        MembershipUser mu = Membership.GetUser(Page.User.Identity.Name);
        string userId = mu.ProviderUserKey.ToString();
        try
        {
            if (Convert.ToInt32(planta.SelectedItem.Value) == 0 || Convert.ToInt32(combocliente.SelectedItem.Value) == 0 || planta.Text == "" || combocliente.Text.Equals(string.Empty) || txtnew.Text.Equals(string.Empty))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "gridcampo3();", true);
                txtnew.Text = "";
                planta.Text = "";
                combocliente.Text = "";
                chkactnewline.Checked = false;
            }
            else
            {
                int l = DataAccess.SaveLinea(Convert.ToInt32(planta.SelectedItem.Value), txtnew.Text, Convert.ToInt32(combocliente.SelectedItem.Value), chkactnewline.Checked);
                for (int i = 1; i <= 6; i++)
                {
                    DataAccess.SaveSeccion(l, i);
                }
                txtnew.Text = "";
                planta.Text = "";
                combocliente.Text = "";
                chkactnewline.Checked = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "okioki();", true);
            }            
        }
        catch(Exception ex)
        {
            MailMessage mailObj = new MailMessage("soporte@rocedes.com.ni", "dmartinez@rocedes.com.ni", "Sistema Auditoria", ex.ToString() + "Usuario : " + mu.UserName);
            SmtpClient SMTPServer = new SmtpClient("rocedes.com.ni");
            SMTPServer.Send(mailObj);
        }
    }
}