﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Intex_EnviosBultoIntex : System.Web.UI.Page
{
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/adminReport/ReportBioIntex.aspx");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.User.Identity.IsAuthenticated == true)
        {
            if (Page.User.IsInRole("PackAdmon"))
            {
                if (!IsPostBack)
                {
                    multiview.ActiveViewIndex = 0;
                    cantidadLiberada();
                    extraerlinea();
                }
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }
        else
        {
            Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
        }
    }

    void cantidadLiberada()
    {
        try
        {
            string queryBundle = "select * from vwcantidadliberadoIntex";
            DataTable dt = DataAccess.Get_DataTable(queryBundle);

            gridEnvios.DataSource = dt;
            gridEnvios.DataBind();
        }
        catch (Exception)
        {
            throw;
        }
    }

    void cantidadincompleta()
    {
        try
        {
            string queryBundle = "select * from vwgetporderIncompletosIntex";
            DataTable dt = DataAccess.Get_DataTable(queryBundle);

            gridenvioIncompleto.DataSource = dt;
            gridenvioIncompleto.DataBind();
        }
        catch (Exception)
        {
            throw;
        }
    }

    void extraerlinea()
    {
        try
        {
            string query = "select l.id_linea,l.numero from linea l	where numero not like '%-%'	order by convert(decimal, numero) asc";

            DataTable dt = DataAccess.Get_DataTable(query);

            drpLinea.DataSource = dt;
            drpLinea.DataTextField = "numero";
            drpLinea.DataValueField = "id_linea";
            drpLinea.DataBind();
            drpLinea.Items.Insert(0, "Select...");

            drplinea2.DataSource = dt;
            drplinea2.DataTextField = "numero";
            drplinea2.DataValueField = "id_linea";
            drplinea2.DataBind();
            drplinea2.Items.Insert(0, "Select...");
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnguardarEnviar_Click(object sender, EventArgs e)
    {
        try
        {
            if (drpEnvioDia.SelectedItem.Text != "Select..." && drpplantasent.SelectedItem.Text != "Select...")
            {
                int rows = 0;// gridEnvios.Rows.Count;
                int cont = 0;
                foreach (GridViewRow item in gridEnvios.Rows)
                {
                    string resp = "";
                    string resp1 = "";
                    var lblidorder = (Label)item.FindControl("lblidorder");
                    var chk = (CheckBox)item.FindControl("checkEnvio");
                    var txtenvio = (TextBox)item.FindControl("txtenvio");

                    if (chk.Checked)
                    {
                        rows++;
                        resp = OrderDetailDA.enviarguardarBihorarioIntex(int.Parse(lblidorder.Text),
                                                                         //int.Parse(drpBihorario.SelectedValue), 
                                                                         int.Parse(txtenvio.Text), int.Parse(drpEnvioDia.SelectedValue), Context.User.Identity.Name);
                        resp1 = OrderDetailDA.UpdatePorderAfterIntex(Convert.ToInt32(lblidorder.Text), Convert.ToInt32(drpplantasent.SelectedValue));
                    }

                    if (resp == "OK" && resp1 == "OK")
                    {
                        cont++;
                    }
                }

                if (rows == cont)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
                }

                cantidadLiberada();
                limpiar();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    void limpiar()
    {
        // gridEnvios.DataSource = null;
        // gridEnvios.DataBind();        
        drpEnvioDia.SelectedIndex = 0;
        drpplantasent.SelectedIndex = 0;
    }

    void limpiar2()
    {
        // gridEnvios.DataSource = null;
        // gridEnvios.DataBind();
        drpbihorario2.SelectedIndex = 0;
    }

    protected void drpLinea_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (drpLinea.SelectedItem.Text != "Select...")
            {
                string queryBundle = "select * from vwcantidadliberadoIntex where id_linea=" + drpLinea.SelectedValue;
                DataTable dt = DataAccess.Get_DataTable(queryBundle);

                gridEnvios.DataSource = dt;
                gridEnvios.DataBind();
            }
        }
        catch (Exception)
        {
            throw;
        }

    }

    protected void btnVertodos_Click(object sender, EventArgs e)
    {
        try
        {
            cantidadLiberada();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void lnkEnvio_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnkred = (LinkButton)sender;
            GridViewRow row = (GridViewRow)lnkred.NamingContainer;

            var lblidorder = (Label)row.FindControl("lblidorder");
            var lblporder = (Label)row.FindControl("lblPorder");

            if (lblidorder.Text != string.Empty && lblporder.Text != string.Empty)
            {
                Response.Redirect("EnviosBultoXCorteIntex.aspx?idorder=" + lblidorder.Text + "&porder=" + lblporder.Text);
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void lnkseccionado_Click(object sender, EventArgs e)
    {

        try
        {
            LinkButton lnkred = (LinkButton)sender;
            GridViewRow row = (GridViewRow)lnkred.NamingContainer;

            var lblidorder = (Label)row.FindControl("lblidorder");
            var lblporder = (Label)row.FindControl("lblPorder");

            if (lblidorder.Text != string.Empty && lblporder.Text != string.Empty)
            {
                Response.Redirect("enviosBultosSeccionadosIntex.aspx?idorder=" + lblidorder.Text + "&porder=" + lblporder.Text);
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnguardarIncompleto_Click(object sender, EventArgs e)//Falta este metodo
    {
        try
        {
            if (drpbihorario2.SelectedItem.Text != "Select...")
            {
                int rows = 0;// gridEnvios.Rows.Count;
                int cont = 0;
                foreach (GridViewRow item in gridenvioIncompleto.Rows)
                {
                    string resp = "";
                    var lblidorder = (Label)item.FindControl("lblidorder");
                    var chk = (CheckBox)item.FindControl("checkEnvio");

                    if (chk.Checked)
                    {
                        rows++;
                        resp = OrderDetailDA.guardarBihorarioPOincoIntex(int.Parse(lblidorder.Text), int.Parse(drpbihorario2.SelectedValue), Context.User.Identity.Name);
                    }

                    if (resp == "OK")
                    {
                        cont++;
                    }
                }

                if (rows == cont)
                {

                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertincompleto();", true);
                }

                cantidadLiberada();
                limpiar();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnvertodos2_Click(object sender, EventArgs e)
    {
        try
        {
            cantidadincompleta();
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void drplinea2_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (drplinea2.SelectedItem.Text != "Select...")
            {
                string queryBundle = "select * from vwgetporderIncompletosIntex where id_linea=" + drplinea2.SelectedValue;
                DataTable dt = DataAccess.Get_DataTable(queryBundle);

                gridenvioIncompleto.DataSource = dt;
                gridenvioIncompleto.DataBind();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnbultosComple_Click(object sender, EventArgs e)
    {
        multiview.ActiveViewIndex = 0;
        cantidadLiberada();
    }

    protected void btnbultosInco_Click(object sender, EventArgs e)
    {
        try
        {
            multiview.ActiveViewIndex = 1;
            cantidadincompleta();
        }
        catch (Exception)
        {
            throw;
        }
    }
}