﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistroExceso : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Page.User.Identity.IsAuthenticated == true)
            {
               // if (Page.User.IsInRole("PackList") || Page.User.IsInRole("PackAdmon"))
                //{

                    if (!IsPostBack)
                    {
                        hdnusuario.Value = Page.User.Identity.Name.ToString();
                    }

                /*}
                else
                {
                    Response.Redirect("/Default.aspx");
                }*/
            }
            else
            {
                Response.Redirect("/Login.aspx?ReturnUrl=%2fDefault.aspx");
            }
        }
        catch (Exception)
        {

            
        }
    }

    void limpiar()
    {
        txtPorder.Text = string.Empty;
        txtstyle.Text = string.Empty;
        txttalla.Text = string.Empty;
        txtunidades.Text = string.Empty;
        txtcomentario.Text = string.Empty;
        drpEnvio.SelectedIndex = 0;
        idporderAuxiliar.Value = string.Empty;
        lblmensajepo.Text = string.Empty;
        hfidPorder.Value = string.Empty;

        gridbultos.DataSource = null;
        gridbultos.DataBind();
    }

    void limpiar2()
    {
       
       
        txttalla.Text = string.Empty;
        txtunidades.Text = string.Empty;
        txtcomentario.Text = string.Empty;
        drpEnvio.SelectedIndex = 0;
     
      
     
    }


    private readonly Random _random = new Random();

    public int RandomNumber(int min, int max)
    {
        return _random.Next(min, max);
    }

    public string RandomString(int size, bool lowerCase = false)
    {
        var builder = new StringBuilder(size);
    
        char offset = lowerCase ? 'a' : 'A';
        const int lettersOffset = 26; // A...Z or a..z: length=26  

        for (var i = 0; i < size; i++)
        {
            var @char = (char)_random.Next(offset, offset + lettersOffset);
            builder.Append(@char);
        }

        return lowerCase ? builder.ToString().ToLower() : builder.ToString();
    }

    public string RandomCodigo()
    {
        var passwordBuilder = new StringBuilder();


        passwordBuilder.Append(RandomString(2));
  
        passwordBuilder.Append(RandomString(6, true));
      
        passwordBuilder.Append(RandomNumber(1000, 9999));

       
        return passwordBuilder.ToString();
    }

    protected void btnguardar_Click(object sender, EventArgs e)
    {
        try
        {
            string mess = "";
            string tipo = "info";
            if (txtPorder.Text == string.Empty || txtstyle.Text == string.Empty ||
               txttalla.Text == string.Empty || txtunidades.Text == string.Empty ||
               hfidPorder.Value == string.Empty)
            {

                tipo = "info";
                mess = "Algunos Campos Vacios";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertMasterE('" + mess + "','" + tipo + "');", true);
                return;
            }

            int unidades = 0;
            try
            {
                unidades = Convert.ToInt16(txtunidades.Text);
            }
            catch (Exception)
            {
                tipo = "info";
                mess = "El Valor de cantiad debe ser numerico";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertMasterE('" + mess + "','" + tipo + "');", true);
                return;
            }
          


            var cod = RandomCodigo();
            var resp =OrderDetailDA.GuardarExcesoTallaCorte(int.Parse(hfidPorder.Value),txttalla.Text.ToUpper().TrimEnd(),unidades, hdnusuario.Value,int.Parse(drpEnvio.SelectedValue),txtcomentario.Text,cod);

            if (resp=="OK")
            {
                limpiar2();
              
                tipo = "success";
                mess = "Guardado Correctamente";
                verlista();

            }
            else if(resp=="DP")
            {
                tipo = "info";
                mess = "Esta intentando guardar mas de una vez el mismo registro";
            }
            else if(resp=="ERR")
            {
                tipo = "error";
                mess = "Ha ocurrido un error";
            }
           
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertMasterE('" + mess + "','" + tipo + "');", true);
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertMasterE('" + ex.Message + "','" + tipo + "');", true);
        }
    }

    void verlista()
    {
        if (hfidPorder.Value == string.Empty)
        {
            return;
        }

        var data = OrderDetailDA.GetExcesoTallaCorteXidCorte(int.Parse(hfidPorder.Value));

        gridbultos.DataSource = data;
        gridbultos.DataBind();
    }

    protected void btnvertodos_Click(object sender, EventArgs e)
    {
        try
        {
            verlista();

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnlimpiar_Click(object sender, EventArgs e)
    {
        try
        {
            limpiar();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void gridbultos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
           var  tipo = "";
           var  mess = "";

            if (e.CommandName == "agregar")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int indice = row.RowIndex;
                string idexceso = gridbultos.DataKeys[indice].Value.ToString();

                string resp = "";

                var unidades = (TextBox)row.FindControl("txtcantidad");
                var enviodia = int.Parse(drpEnvio.SelectedValue);

                if (unidades.Text.Trim() != string.Empty )
                {
                    var cod = RandomCodigo();
                    resp = OrderDetailDA.ActualizarExcesoTallaCorte(int.Parse(idexceso),Convert.ToInt32(unidades.Text), Context.User.Identity.Name,enviodia,cod);
                }

                if (resp.Equals("OK"))
                {
                    if (hfidPorder.Value != string.Empty)
                    {
                        int idporder = int.Parse(hfidPorder.Value);

                        var data = OrderDetailDA.GetExcesoTallaCorteXidCorte(idporder);

                        gridbultos.DataSource = data;
                        gridbultos.DataBind();
                    }

                    tipo = "success";
                    mess = "Guardado correctamente";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertMasterE('" + mess + "','" + tipo + "');", true);
                }
                else if(resp.Equals("MY"))
                {
                    tipo = "info";
                    mess = "Las unidades sobrepasan la cantidad asignada al guardar el exceso";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertMasterE('" + mess + "','" + tipo + "');", true);
                }
                else if (resp.Equals("EV"))
                {
                    tipo = "info";
                    mess = "No selecciono el envio del dia";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertMasterE('" + mess + "','" + tipo + "');", true);
                }
                else if (resp.Equals("ERR"))
                {
                    tipo = "error";
                    mess = "Ha ocurrido un error";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertMasterE('" + mess + "','" + tipo + "');", true);
                }
                //else
                //{
                //    tipo = "error";
                //    mess = "Ha ocurrido un error";
                //    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertMasterE('" + mess + "','" + tipo + "');", true);
                //}
            }
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertMasterE('" + ex.Message + "','" + tipo + "');", true);
        }
    }
}