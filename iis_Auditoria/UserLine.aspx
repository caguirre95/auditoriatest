﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserLine.aspx.cs" Inherits="UserLine" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function incorrecto() {
            swal({
                title: 'Por Favor!',
                text: 'Ingrese los datos correctamente...',
                type: 'info'
            });
        }

        function exito() {
            swal({
                title: 'Exito!',
                text: 'Registro Ingresado Correctamente..',
                type: 'success'
            });
        }
    </script>
    <style>
        .mg{
            margin-top:15px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:10px;">
            <div class="panel-heading"><strong>User Line</strong></div>
            <div class="panel-body">
                <div class="col-lg-12">

                    <div class="col-lg-4 col-lg-offset-4">
                        <div class="col-lg-12">
                            <span style="font-weight: bold">Planta</span>
                            <dx:ASPxComboBox ID="cmbPlanta" runat="server" NullText="Planta" Width="300px" CssClass="form-control" AutoPostBack="True"></dx:ASPxComboBox>
                            <br />
                            <span style="font-weight: bold">Usuario</span>
                            <dx:ASPxComboBox ID="cmbuser" runat="server" NullText="User" Width="300px" CssClass="form-control"></dx:ASPxComboBox>
                            <br />
                            <span style="font-weight: bold">Linea</span>
                            <dx:ASPxComboBox ID="cmbline" runat="server" NullText="Line" Width="300px" CssClass="form-control"></dx:ASPxComboBox>
                        </div>
                        <div class="col-lg-12" style="padding-top: 15px">
                            <asp:Button ID="btnGuadar" runat="server" Text="Guardar" CssClass="btn btn-primary form-control " OnClick="btnGuadar_Click" />
                        </div>
                    </div>

                    <div class="mg">
                        <dx:ASPxGridView ID="grduser" runat="server" AutoGenerateColumns="false" KeyFieldName="id" Width="100%" Theme="Material">
                            <Columns>
                                <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0" Visible="false">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="id" Caption="Nombre de Usuario" ReadOnly="True" Visible="true" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Line" VisibleIndex="3">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowGroupFooter="VisibleAlways" />
                            <SettingsPager PageSize="5" />
                            <SettingsSearchPanel Visible="true" />
                        </dx:ASPxGridView>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

