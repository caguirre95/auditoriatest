﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Correo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private string tempPath = @"~/uploads/temp";

    protected void cmdAddFile_Click(object sender, EventArgs e)
    {

        FileUpload f = fUpload;

        // No se hace nada si no hay fichero
        if (!f.HasFile)
            return;

        // Se crea un Item para el ListBox
        //  - Value: Nombre del fichero
        //  - Text : Texto para mostrar
        ListItem item = new ListItem();
        item.Value = f.FileName;
        item.Text = f.FileName +
                    " (" + f.FileContent.Length.ToString("N0") +
                    " bytes).";

        // Se sube el fichero a la carpeta temporal
        f.SaveAs(Server.MapPath(Path.Combine(tempPath, item.Value)));

        // Se deja el nombre del fichero en el ListBox
        lstFiles.Items.Add(item);
    }

    protected void cmdDelFile_Click(object sender, EventArgs e)
    {
        ListBox lb = lstFiles;
        // Se comprueba que exista algún item seleccionado
        if (lb.SelectedItem == null)
            return;

        // Se elimina el fichero seleccionado
        borraEntrada(lb.SelectedItem.Value);
    }

    /// <summary>
    /// Elimina el fichero de la carpeta temporal y del ListBox.
    /// </summary>
    /// <param name="fileName"></param>
    private void borraEntrada(string fileName)
    {
        string fichero = Server.MapPath(Path.Combine(tempPath, fileName));
        File.Delete(fichero);

        ListItem l = lstFiles.Items.FindByValue(fileName);
        if (l != null)
            lstFiles.Items.Remove(l);
    }

    private void enviaCorreo()
    {
        using (System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage())
        {
            string resultado;

            MailAddress from = new MailAddress("dmartinez@rocedes.com.ni");
            message.From = from;
           
            // Dirección de destino
            message.To.Add("pgarcia@rocedes.com.ni");
            message.To.Add("dmartinez@rocedes.com.ni");
            //message.To.Add("soporte@rocedes.com.ni");
            //message.To.Add("lima@rocedes.com.ni");
            //message.To.Add("esanchez@rocedes.com.ni");            
            //message.To.Add("lescerna1974@gmail.com");            
            // Asunto 
            message.Subject = "Dato Final Roc6 "+ DateTime.Now.ToString();
            // Mensaje 
            message.Body = "Ver archivo Adjunto";

            // Se recuperan los ficheros
            foreach (ListItem l in lstFiles.Items)
            {
                // Lectura del nombre del fichero
                string fichero = Server.MapPath(Path.Combine(tempPath, l.Value));

                // Adjuntado del fichero a la colección Attachments
                message.Attachments.Add(new System.Net.Mail.Attachment(fichero));
            }

            // Se envía el mensaje y se informa al usuario
            System.Net.Mail.SmtpClient smpt = new System.Net.Mail.SmtpClient("rocedes.com.ni");
            string mensaje = string.Empty;
            try
            {
                smpt.Send(message);
                mensaje = "Correo enviado con éxito";
            }
            catch (Exception ex)
            {
                mensaje = "Ocurrió un error: " + ex.Message;
            }
            resultado = mensaje;
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "oki('" + resultado + "');", true);
        }

        // Se borran los ficheros de la carpeta temporal
        while (lstFiles.Items.Count > 0)
        {
            borraEntrada(lstFiles.Items[0].Value);
        }
    }

    protected void btn_Click(object sender, EventArgs e)
    {
        enviaCorreo();
    }
}