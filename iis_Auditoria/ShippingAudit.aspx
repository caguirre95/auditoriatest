﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ShippingAudit.aspx.cs" Inherits="ShippingAudit" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
    <h2>
        <asp:Label ID="lbl" runat="server" Text="Shipping Audit"></asp:Label></h2>
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
        <div class="col-lg-12" style="background-color: #4D7496; height: 50px; margin-top: 20px; font-size: 16px;text-align:center;">
            <div class="col-md-4" style="margin-top:10px;color: #fff; ">
                <asp:Label ID="Label1" runat="server" Text="Responsable: "></asp:Label>
                <asp:Label ID="lblresponsable" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-4" style="margin-top:10px;">
                <asp:Label ID="Label3" runat="server" ForeColor="white" Text="Firma"></asp:Label>
                <asp:TextBox ID="txtfirma" runat="server" Height="30px" TextMode="Password" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFV1" runat="server" ErrorMessage="*Requerido" ForeColor="white" Font-Size="12px" ControlToValidate="txtfirma"></asp:RequiredFieldValidator>
            </div>
            <div class="col-md-4" style="margin-top:10px;color: #fff; ">
                <asp:Label ID="Label4" runat="server" Text="Fecha"></asp:Label>
                <asp:Label ID="lbfecha" runat="server" Text=""></asp:Label>
            </div>
        </div>
    <section id="Principal">
        <div class="container">
                <div class="col-lg-12" style="text-align:center;margin-top: 15px;">
            <div class="col-md-6">
                 <div style="display:table;width:80%">
                    <div style="display:table-row;">
                    <div class="celdas_left">
                         <asp:Label ID="Label5" runat="server" Width="125px" Text="Nombre del Cliente:"></asp:Label>
                    </div>
                    <div class="celdas_right">
                    <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Height="30px" Width="170px"></dx:ASPxTextBox>
                    </div>
                    </div>  
                    <div style="display:table-row;">
                    <div class="celdas_left">
                          <asp:Label ID="Label6" runat="server" Text="Nombre del Auditor:"></asp:Label>
                    </div>
                    <div class="celdas_right">
                    <dx:ASPxTextBox ID="ASPxTextBox2" runat="server" Height="30px" Width="170px"></dx:ASPxTextBox>
                    </div>
                    </div>
                    <div style="display:table-row;">
                    <div class="celdas_left">
                          <asp:Label ID="Label7" runat="server" Text="Descripcion:"></asp:Label>
                    </div>
                    <div class="celdas_right">
                          <dx:ASPxTextBox ID="ASPxTextBox3" runat="server" Height="30px" Width="170px"></dx:ASPxTextBox>
                    </div>
                    </div>  
                    <div style="display:table-row;">
                    <div class="celdas_left celda_bottom">
                           <asp:Label ID="Label8" runat="server" Text="Envio #: "></asp:Label>
                    </div>
                    <div class="celdas_right celda_bottom">
                           <asp:TextBox ID="txtEnvio" runat="server" Height="30px" Width="170px"></asp:TextBox>
                    </div>
                    </div>  
                </div>
            </div>
        </div>

         <div class="col-lg-12" style="text-align:center;margin-top: 5px;">
            <div class="col-md-6" style="margin-top: 5px;">
                <div style="display:table;width:80%">
                    <div class="celdas_left celdas_right" style="display:table-caption;text-align:center;"> 
                    <asp:Label ID="Label14" runat="server" Text="Informacion de Corte"></asp:Label>      
                    </div>
                    <div style="display:table-row;text-align:right;">
                    <div class="celdas_left">
                        <asp:Label ID="Label9" runat="server" Width="125px" Text="Corte #: "></asp:Label>
                    </div>
                    <div class="celdas_right">
                    <dx:ASPxTextBox ID="txtCorte" runat="server" Height="30px" Width="170px"></dx:ASPxTextBox>
                    </div>
                    </div>  
                    <div style="display:table-row;text-align:right;">
                        <div class="celdas_left">                        
                        <asp:Label ID="Label10" runat="server" Text="Estilo #: "></asp:Label>
                        </div>
                        <div class="celdas_right">
                        <dx:ASPxTextBox ID="txtEstilo" runat="server" Height="30px" Width="170px"></dx:ASPxTextBox>
                        </div>
                    </div>
                    <div style="display:table-row;text-align:right;">
                        <div class="celdas_left">
                        <asp:Label ID="Label11" runat="server" Text="Color #: "></asp:Label>
                        </div>
                        <div class="celdas_right">
                        <dx:ASPxTextBox ID="txtColor" runat="server" Height="30px" Width="170px"></dx:ASPxTextBox>
                        </div>
                    </div>
                    <div style="display:table-row;text-align:right;">
                        <div class="celdas_left celda_bottom">
                        <asp:Label ID="Label12" runat="server" Text="Cantidad: "></asp:Label>
                        </div>
                        <div class="celdas_right celda_bottom">
                        <dx:ASPxTextBox ID="txtCantidad" runat="server" Height="30px" Width="170px"></dx:ASPxTextBox>
                        </div>
                    </div>
                  </div>
                  <div style="display:table;width:80%">
                         <div class="celda_middle" style="display:table-caption;text-align:center;">
                                <asp:Label ID="Label13" runat="server" Text="Plan de Muestreo"></asp:Label>
                            </div>
                         <div style="display:table-row;text-align:center;">
                            <asp:CheckBoxList ID="listplanMuestreo" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="2.5 AQL"></asp:ListItem>
                            <asp:ListItem Text="4.0 AQL"></asp:ListItem>
                            <asp:ListItem Text="6.5 AQL"></asp:ListItem>
                            </asp:CheckBoxList>
                        </div>  
                     </div>
                </div>

            <div class="col-md-6" style="margin-top: 5px;margin-bottom:20px;">
              <div style="display:table;width:80%">
                    <div class="celdas_left celdas_right" style="display:table-caption;text-align:center;"> 
                     <asp:Label ID="Label15" runat="server" Text="Resultados de Calidad"></asp:Label>    
                    </div>
                    <div style="display:table-row;text-align:right;">
                    <div class="celdas_left">
                        <asp:Label ID="Label16" runat="server" Text="#Unidaddes Auditadas"></asp:Label>
                    </div>
                    <div class="celdas_right">
                    <dx:ASPxTextBox ID="txtundAuditadas" runat="server" Height="30px" Width="170px"></dx:ASPxTextBox>
                    </div>
                    </div> 
                    <div style="display:table-row;text-align:right;">
                    <div class="celdas_left">
                        <asp:Label ID="Label17" runat="server" Text="#de mayor defecto"></asp:Label>
                    </div>
                    <div class="celdas_right">
                    <dx:ASPxTextBox ID="txtmayorDefec" runat="server" Height="30px" Width="170px"></dx:ASPxTextBox>
                    </div>
                    </div>  
                    <div style="display:table-row;text-align:right;">
                    <div class="celdas_left">
                        <asp:Label ID="Label18" runat="server" Text="% Defectuoso"></asp:Label>
                    </div>
                    <div class="celdas_right">
                    <dx:ASPxTextBox ID="txtporcentDefec" runat="server" Height="30px" Width="170px"></dx:ASPxTextBox>
                    </div>
                    </div> 
                    <div style="display:table-row;">
                    <div  class="celda_bottom" style="text-align:right;">
                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Aceptado" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Rechazado" Value="1"></asp:ListItem>
                    </asp:CheckBoxList>  
                    </div>
                    </div>
               </div>
            </div>
            <asp:GridView ID="GridView1" style="margin-top:50px;" OnRowDataBound="GridView1_RowDataBound" runat="server" AutoGenerateColumns="False" 
                ShowFooter="true" Width="80%">
                <Columns>
                    <asp:TemplateField HeaderText="Codigo Defecto">
                        <ItemTemplate>
                            <asp:TextBox ID="txtcodDef" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tipo de Defecto">
                        <ItemTemplate>
                            <dx:ASPxComboBox ValueField="codigo" ID="comboDef" DataSourceID="SqlDataSource1" TextField="nombre"  runat="server"
                                ValueType="System.String" AutoPostBack="true" OnSelectedIndexChanged="comboDef_SelectedIndexChanged">
                            </dx:ASPxComboBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Defecto Mayor">
                        <ItemTemplate>
                            <asp:TextBox ID="txtDefMay" runat="server" Text="0"></asp:TextBox>
                        </ItemTemplate>
                        <FooterTemplate>
                            <div style="text-align: right;">
                                <asp:Label ID="lblsample" Text="Total:" runat="server" />
                                <asp:Label ID="lblTotalqty" runat="server" />
                            </div>
                        </FooterTemplate>
                    </asp:TemplateField>                    
                    <asp:TemplateField HeaderText="Defecto Menor">
                        <ItemTemplate>
                            <asp:TextBox ID="txtDefMenor" runat="server" Text="0"></asp:TextBox>
                        </ItemTemplate>
                        <FooterTemplate>
                            <div style="text-align: right;">
                                <asp:Label ID="lblsampledef" Text="Total:" runat="server" />
                                <asp:Label ID="lblTotalqtydef" runat="server" />
                            </div>
                        </FooterTemplate>
                    </asp:TemplateField>                    
                </Columns>
            </asp:GridView>

             <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:RocedesCS %>"
                SelectCommand="select codigo,nombre from Defectos"></asp:SqlDataSource>
            <dx:ASPxButton ID="Save" runat="server" Text="Save" OnClick="Save_Click">
            </dx:ASPxButton>
         </div>
          </div>
          </section>
          
</asp:Content>

