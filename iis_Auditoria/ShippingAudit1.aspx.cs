﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ShippingAudit1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnnuevo.Visible = false;
            btnGuardar.Visible = true;
            cargagirddef();
        }
    }

    void cargagirddef()
    {
        lbfecha.Text = DateTime.Now.Date.ToShortDateString();
        DataTable shipping = new DataTable();
        DataColumn col = new DataColumn();
        col.ColumnName = "menu";
        shipping.Columns.Add(col);
        for (int i = 0; i <= 10; i++)
        {
            DataRow newrow = shipping.NewRow();
            newrow[0] = 0;
            shipping.Rows.Add(newrow);
        }

        gridDefectos.DataSource = shipping;
        gridDefectos.DataBind();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorderCom(string pre)
    {
        try
        {
            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(" select top(10) po.Id_Order,po.POrder,po.Quantity,s.Style,c.Cliente,b.Color from POrder po join Style s on po.Id_Style=s.Id_Style join Cliente c on po.Id_Cliente=c.Id_Cliente join Bundle b on po.Id_Order=b.Id_Bundle"
                                            + " where po.POrder like  '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.idcorte = int.Parse(dt.Rows[i][0].ToString());
                obj.Corte = dt.Rows[i][1].ToString();
                obj.Cantidad = dt.Rows[i][2].ToString();
                obj.Estilo = dt.Rows[i][3].ToString();
                obj.Cliente = dt.Rows[i][4].ToString();
                obj.Color = dt.Rows[i][5].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public int idcorte { get; set; }
        public string Corte { get; set; }
        public string Estilo { get; set; }
        public string Cantidad { get; set; }
        public string Color { get; set; }
        public string Cliente { get; set; }

    }

    protected void gridDefectos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string sql = "select  codigo, nombre from defectos where nombre not like 'Seleccione%' order by nombre asc";


            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();


            DropDownList drp = (DropDownList)e.Row.FindControl("DropDownListTipoDefecto");

            drp.DataSource = dt;
            drp.DataTextField = "nombre";
            drp.DataValueField = "codigo";
            drp.DataBind();
            drp.Items.Insert(0, "Seleccione...");

        }

    }

    protected void DropDownListTipoDefecto_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList comboDef = (DropDownList)sender;
        GridViewRow row = (GridViewRow)comboDef.NamingContainer;
        Label cod = (Label)row.FindControl("lblcodigo");
        cod.Text = comboDef.SelectedItem.Value.ToString();
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            string res = "";
            bool r = false;
            int cont = 0;

            foreach (ListItem item in RadioButtonList2.Items)
            {
                if (item.Selected == true)
                {
                    res = item.Value;
                    r = true;
                }

            }

            if (r)
            {

                DataTable dt_UserName = DataAccess.Get_DataTable("select UserId from aspnet_Users where UserName='" + HttpContext.Current.User.Identity.Name + "'");

                string UserId = Convert.ToString(dt_UserName.Rows[0][0]);
                string responsable = txtresponsable.Text.ToLower();
                int idporder = int.Parse(HiddenFieldidcorte.Value);
                string plan = RadioButtonList1.SelectedItem.Text;
                int unidadesAudi = int.Parse(txtunidades.Text);
                int numadef = int.Parse(txtdefectos.Text);
                float defectuoso = float.Parse(txtdefectuosoPer.Text);
                bool approved = Convert.ToBoolean(RadioButtonList2.SelectedItem.Value);


                string resp = OrderDetailDA.SaveEmbarque(idporder, UserId, plan, unidadesAudi, numadef, defectuoso, approved, DateTime.Now, responsable);

                if (resp == "OK")
                {
                    dt_UserName = DataAccess.Get_DataTable("select id_shippingAudit from tbShippingAudit sh where sh.id_order =" + idporder);

                    foreach (GridViewRow item in gridDefectos.Rows)
                    {
                        int idshippingAudi = int.Parse(dt_UserName.Rows[0][0].ToString());
                        DropDownList drp = (DropDownList)item.FindControl("DropDownListTipoDefecto");
                        TextBox txtcant = (TextBox)item.FindControl("txtTotal1");
                        TextBox txtdefMy = (TextBox)item.FindControl("txtdefmayor");
                        TextBox txtdefMn = (TextBox)item.FindControl("txtdefmenor");
                        TextBox txttot = (TextBox)item.FindControl("txttotal2");

                        int tcant = txtcant.Text == "" ? 0 : int.Parse(txtcant.Text);
                        int tdefmy = txtdefMy.Text == "" ? 0 : int.Parse(txtdefMy.Text);
                        int tdefmn = txtdefMn.Text == "" ? 0 : int.Parse(txtdefMn.Text);
                        int tota = txttot.Text == "" ? 0 : int.Parse(txttot.Text);

                        Label lbl = (Label)item.FindControl("lblcodigo");

                        if (lbl.Text != "")
                        {
                            string resp1 = OrderDetailDA.SaveDetalleEMbarque(idshippingAudi, drp.SelectedItem.Value, tcant, tdefmy, tdefmn, tota);
                            if (resp1 != "OK")
                                cont++;
                        }
                    }

                }
            }
            else
            {
                cont = -1;
            }

            if (cont == 0)
            {
               
                limpiar();
                btnGuardar.Visible = false;
                btnnuevo.Visible = true;
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertsucess();", true);
            }
            else if (cont == -1)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertwarning();", true);
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertError();", true);
                
            }

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void txtTotal1_TextChanged(object sender, EventArgs e)
    {
        try
        {
            int num = 0;

            foreach (GridViewRow item in gridDefectos.Rows)
            {
                TextBox txtcant = (TextBox)item.FindControl("txtTotal1");
                TextBox txtdefMy = (TextBox)item.FindControl("txtdefmayor");
                TextBox txtdefMn = (TextBox)item.FindControl("txtdefmenor");
                TextBox txttot = (TextBox)item.FindControl("txttotal2");
                Label lbl = (Label)item.FindControl("lblcodigo");

                if (lbl.Text != "" && txtcant.Text != "")
                {
                    num = num + Convert.ToInt32(txtcant.Text);
                }
            }

            Label lbltotal = (Label)gridDefectos.FooterRow.FindControl("lbltotal");

            lbltotal.Text = " Total: " + num.ToString();
            txtdefectos.Text = num.ToString();

            string resp = "";
            bool r = false;

            foreach (ListItem item in RadioButtonList1.Items)
            {
                if (item.Selected == true)
                {
                    resp = item.Value;
                    r = true;
                }

            }

            if (r)
            {
                if (txtdefectos.Text != string.Empty && txtcantidad.Text != string.Empty )
                {
                    DataTable dt_Valores = DataAccess.Get_DataTable("select Acep,reje,ToInspec from tblmuestralevel where AQL='" + resp + "' and valMin<=" + txtcantidad.Text + " and valMax>=" + txtcantidad.Text);

                    txtunidades.Text = dt_Valores.Rows[0][2].ToString();
                    txtdefectuosoPer.Text = ((float.Parse(txtdefectos.Text) / float.Parse(txtunidades.Text)) * 100).ToString();

                    if (int.Parse(dt_Valores.Rows[0][0].ToString()) >= int.Parse(txtdefectos.Text))
                    {
                        RadioButtonList2.SelectedIndex = 0;
                    }
                    else
                    {

                        RadioButtonList2.SelectedIndex = 1;
                    }
                }
            }



        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int num = 0;

            foreach (GridViewRow item in gridDefectos.Rows)
            {
                TextBox txtcant = (TextBox)item.FindControl("txtTotal1");
                TextBox txtdefMy = (TextBox)item.FindControl("txtdefmayor");
                TextBox txtdefMn = (TextBox)item.FindControl("txtdefmenor");
                TextBox txttot = (TextBox)item.FindControl("txttotal2");
                Label lbl = (Label)item.FindControl("lblcodigo");

                if (lbl.Text != "" && txtcant.Text != "")
                {
                    num = num + Convert.ToInt32(txtcant.Text);
                }
            }

            Label lbltotal = (Label)gridDefectos.FooterRow.FindControl("lbltotal");

            lbltotal.Text = " Total: " + num.ToString();
            txtdefectos.Text = num.ToString();

            string resp = "";
            bool r = false;

            foreach (ListItem item in RadioButtonList1.Items)
            {
                if (item.Selected == true)
                {
                    resp = item.Value;
                    r = true;
                }

            }

            if (r)
            {
                if (txtdefectos.Text != string.Empty && txtcantidad.Text != string.Empty)
                {
                    DataTable dt_Valores = DataAccess.Get_DataTable("select Acep,reje,ToInspec from tblmuestralevel where AQL='" + resp + "' and valMin<=" + txtcantidad.Text + " and valMax>=" + txtcantidad.Text);

                    txtunidades.Text = dt_Valores.Rows[0][2].ToString();
                    txtdefectuosoPer.Text = ( (float.Parse(txtdefectos.Text) / float.Parse(txtunidades.Text)) * 100).ToString();

                    if (int.Parse(dt_Valores.Rows[0][0].ToString()) >= int.Parse(txtdefectos.Text))
                    {
                       
                        RadioButtonList2.SelectedIndex = 0;
                        
                    }
                    else
                    {
                       
                        RadioButtonList2.SelectedIndex = 1;
                    }
                }

            }

        }
        catch (Exception)
        {
            throw;
        }
    }


    void limpiar()
    {
        gridDefectos.DataSource = null;
        gridDefectos.DataBind();

        txtcliente.Text = "";
        txtcolor.Text = "";
        txtcorte.Text = "";
        txtdefectos.Text = "";
        txtdefectuosoPer.Text = "";
        txtestilo.Text = "";
        txtunidades.Text = "";
        txtcantidad.Text = "";
    }

    protected void btnnuevo_Click(object sender, EventArgs e)
    {
        Response.Redirect("/ShippingAudit1.aspx");
    }


}