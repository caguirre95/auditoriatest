﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using SistemaAuditores.DataAccess;

public partial class EditOperario : System.Web.UI.Page
{
    List<object> fieldValues = new List<object>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.EditarOperario.ShowOnPageLoad = false;
        }
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        Load_Data();
    }

    public void Load_Data() 
    {
        DataTable dt = new DataTable();
        DataColumn cod = new DataColumn("Codigo", typeof(string));
        DataColumn nomb = new DataColumn("Nombre", typeof(string));
        DataColumn id = new DataColumn("Id", typeof(string));
        //DataColumn sec = new DataColumn("Seccion", typeof(string));
        dt.Columns.Add(cod);
        dt.Columns.Add(nomb);
        dt.Columns.Add(id);

        fieldValues = ASPxGridView1.GetSelectedFieldValues(new string[] { "codigo", "nombre", "Id_Seccion", "id_operario" });
        foreach (object[] item in fieldValues)
        {
            DataRow row = dt.NewRow();
            row["Codigo"] = item[0].ToString();
            row["Nombre"] = item[1].ToString();
            //row["Id"] = item[3].ToString();
            dt.Rows.Add(row);
        }
        GridView1.DataSource = dt;
        GridView1.DataBind();
        GridView1.Columns[4].Visible = false;        
        int i =0;
        foreach (object[] item in fieldValues)
        {
            DataTable sec = DataAccess.Get_DataTable("select Modulo from Modulo where id_seccion=" + item[2].ToString());
            Label tsec = (Label)GridView1.Rows[i].Cells[2].FindControl("textSecAct");
            tsec.Text = sec.Rows[0][0].ToString();
            Label tid = (Label)GridView1.Rows[i].Cells[4].FindControl("id");
            tid.Text = item[3].ToString();
            i++;
        }      
        i=0;
        if (GridView1.Rows.Count > 0)
        {
            this.EditarOperario.ShowOnPageLoad = true;
        }
        else
        {
            string message = "alert('Seleccione un Operario!');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        }            
    }

    protected void btnpop_click(object sender, EventArgs e)
    {
        btnpop.Enabled = false;
        try
        {
            for (int i = 0; i <= GridView1.Rows.Count - 1; i++)
            {
                DropDownList dest = (DropDownList)GridView1.Rows[i].Cells[3].FindControl("drop_sec_dest");
                Label tid = (Label)GridView1.Rows[i].Cells[4].FindControl("id");
                DataAccess.Update_Oper(Convert.ToInt16(tid.Text), Convert.ToInt16(dest.SelectedValue));
            }
        }
        catch (Exception ex)
        {
            string message = "alert('Ha Ocurrido un Error, Reintente nuevamente!');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", message, true);
        }
        this.EditarOperario.ShowOnPageLoad = false;
        ASPxGridView1.DataBind();
    }
}