﻿<%@ Page Title="Roster" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CargaRoster.aspx.cs" Inherits="Roster_CargaRoster" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link href="../bootstrap-fileinput-master/css/fileinput.css" rel="stylesheet" />
    <link href="../bootstrap-fileinput-master/css/fileinput.min.css" rel="stylesheet" />
    <script src="../bootstrap-fileinput-master/js/fileinput.js"></script>
    <script src="../bootstrap-fileinput-master/js/fileinput.min.js"></script>

    <script type="text/javascript">

        function alertExitoso() {
            swal({
                title: 'Exito!',
                type: 'success'
            });
        }
        function alertErr() {
            swal({
                title: 'Error !',
                type: 'danger'
            });
        }  

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading"><strong>Cargar Roster</strong></div>
            <div class="panel-body">
                <div class="row">

                    <div class="col-md-8 col-lg-6" style="margin-bottom: 10px">                       
                        <asp:FileUpload ID="file" runat="server" name="input2[]" class="file" multiple="" data-show-upload="false" data-show-caption="true" />
                    </div>
                    <div class="col-md-2 col-lg-3" style="margin-bottom: 10px">
                        <asp:Button ID="btncargar" runat="server" Text="Cargar Archivo" CssClass="btn btn-info form-control" OnClick="btncargar_Click" />
                    </div>
                    <div class="col-md-2 col-lg-3" style="margin-bottom: 10px">
                        <asp:Button ID="btnc" runat="server" Text="Guardar Archivo" CssClass="btn btn-primary form-control" OnClick="btnc_Click" />
                    </div>

                </div>

                <div class="row">
                    <hr />
                    <div class="col-md-12 col-lg-12">
                        <dx:aspxgridview id="grd" runat="server" autogeneratecolumns="false" width="100%" theme="Metropolis" settingsbehavior-allowsort="false" ondatabinding="grd_DataBinding" oncustomcallback="grd_CustomCallback">
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="Linea" CellStyle-HorizontalAlign="Justify" Caption="Line"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="ModuloS" CellStyle-HorizontalAlign="Justify" Caption="Seccion"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="CodigoEmp" CellStyle-HorizontalAlign="Justify" Caption="Codigo Empleado"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="CodigoBarra" CellStyle-HorizontalAlign="Justify" Caption="Codigo Barra"></dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DescripcionOper" CellStyle-HorizontalAlign="Justify" Caption="Operacion"></dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsPager></SettingsPager>
                        </dx:aspxgridview>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

