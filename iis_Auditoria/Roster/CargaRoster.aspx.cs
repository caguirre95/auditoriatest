﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Roster_CargaRoster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["dataRost"] = null;
        }
    }

    protected void btncargar_Click(object sender, EventArgs e)
    {
        try
        {
            List<Roster> lista = new List<Roster>();

            DataTable dt = new DataTable();

            if (file.PostedFile.ContentType == "application/vnd.ms-excel" || file.PostedFile.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                string filename = Path.Combine(Server.MapPath("~/Documentos/"), Guid.NewGuid().ToString() + Path.GetExtension(file.PostedFile.FileName));

                file.PostedFile.SaveAs(filename);

                string conString = "";

                string ext = Path.GetExtension(file.PostedFile.FileName);

                if (ext.ToLower() == ".xls")
                {
                    conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + ";Extended Properties=\"Excel 8.0;HDR=YES;IMEX=2\"";
                }
                else if (ext.ToLower() == ".xlsx")
                {
                    conString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filename + ";Extended Properties=\"Excel 12.0;HDR=YES;IMEX=2\"";
                }

                string query = "";

                OleDbConnection con = new OleDbConnection(conString);

                if (con.State == System.Data.ConnectionState.Closed)
                {
                    con.Open();

                    string Hoja1 = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();

                    query = "select [Linea], [ModuloS], [CodigoEmp], [CodigoBarra], [DescripcionOper] from [" + Hoja1 + "]";
                }

                OleDbCommand cmd = new OleDbCommand(query, con);

                OleDbDataAdapter da = new OleDbDataAdapter(cmd);

                DataSet ds = new DataSet();

                da.Fill(ds);

                da.Dispose();

                con.Close();

                con.Dispose();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Roster obj = new Roster();

                    if (dr["Linea"] != DBNull.Value || dr["ModuloS"] != DBNull.Value || dr["CodigoEmp"] != DBNull.Value || dr["CodigoBarra"] != DBNull.Value || dr["DescripcionOper"] != DBNull.Value)
                    {
                        obj.Linea = Convert.ToInt16(dr["Linea"].ToString());

                        obj.ModuloS = dr["ModuloS"].ToString();

                        obj.CodigoEmp = dr["CodigoEmp"].ToString();

                        obj.CodigoBarra = dr["CodigoBarra"].ToString();

                        obj.DescripcionOper = dr["DescripcionOper"].ToString();

                        lista.Add(obj);
                    }
                }

                Session["dataRost"] = lista;

                grd.DataSource = Session["dataRost"];

                grd.DataBind();

                File.Delete(filename);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "verificar2();", true);
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnc_Click(object sender, EventArgs e)
    {
        try
        {
            List<Roster> list = new List<Roster>();

            if (Session["dataRost"] != null)
            {
                list = (List<Roster>)Session["dataRost"];

                var resp = DatosRoster.guardarRoster(list);

                if (resp == "Exito")
                {
                    limpiar1();

                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertExitoso();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertErr();", true);
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    void limpiar1()
    {
        Session["dataRost"] = null;
        grd.DataSource = Session["dataRost"];
        grd.DataBind();
    }

    protected void grd_DataBinding(object sender, EventArgs e)
    {
        try
        {
            grd.DataSource = Session["dataRost"];
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void grd_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        try
        {
            grd.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
    }
}