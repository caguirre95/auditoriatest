﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MedidasNewIntexReproMPl.aspx.cs" Inherits="MedidasNewIntexReproMPl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script lang="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtPorder.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "MedidasNewIntexReproMPl.aspx/GetPorder",
                        data: "{'pre' :'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    porder: item.porder,
                                    idorder: item.idorder,
                                    style: item.style,
                                    idstyle: item.idstyle,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);

                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtPorder.ClientID%>').val(ui.item.porder);
                    $('#<%=hfidPorder.ClientID%>').val(ui.item.idorder);
                    $('#<%=txtstyle.ClientID%>').val(ui.item.style);
                    $('#<%=hdnidstyle.ClientID %>').val(ui.item.idstyle)

                    $('#ventanacolor').modal('show');
                    PoblarDropDown(ui.item.porder);
                    creartabla(ui.item.idstyle);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li  >")
                    .append("<a class='text-info ' style='padding-left:30px;' >" + item.porder + "</a>").appendTo(ul);
            };
        });


        function ocultarVentanaColor() {
            $('#<%=txtcolor.ClientID%>').val('');
            $('#ventanacolor').modal('hide');
        }


        function selectColor() {
            var select = document.getElementById("<%= drpColor.ClientID %>").value;

            if (select === "0") {
                $('#<%=txtcolor.ClientID%>').val('');
            }
            else {
                var color = $('[id*=drpColor] option:selected').text();
                $('#<%=txtcolor.ClientID%>').val(color);
            }

            $('#ventanacolor').modal('hide');

            $('#<%=drpColor.ClientID%>').val('0');
        }

    </script>

    <script type="text/javascript">

        function PoblarDropDown(id) {
            $.ajax({
                type: "POST",
                url: "MedidasNewIntexReproMPl.aspx/GetTallasXIdCorte",
                data: "{ id: '" + id + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#<%=drpTallas.ClientID%>').empty().append($("<option></option>").val("0").html("Seleccione..."));
                    $.each(data.d, function (key, value) {
                        var option = $(document.createElement('option'));
                        option.html(value.porder);
                        option.val(value.idorder);

                        $('#<%=drpTallas.ClientID%>').append(option);
                    });

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus + ": " + XMLHttpRequest.responseText);

                }
            });
        }

        function creartabla(id) {
            console.log(id);
            $.ajax({
                type: "POST",
                url: "MedidasNewIntexReproMPl.aspx/GetEspecificaciones",
                data: "{ id: '" + id + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    $("#cuerpo").html("");
                    $.each(data.d, function (key, value) {
                        var tr = '<tr> <td style="display:none">' + value.idorder + '</td>' +
                            '<td >' + value.porder + '</td>' +
                            '<td style="display:none">' + value.idpmedida + '</td>' +
                            '<td style="display:none">' + value.value2 + '</td>' +
                            '<td style="display:none">' + value.value3 + '</td>' +
                            '<td style="display:none"></td>' +
                            '<td style="display:none"></td>' +
                            '<td style="display:none"></td>' +
                            '<td style="display:none"></td>' +
                            '<td> <input type="button" id="btnSelecccionar" onclick="selected()" value="Medir" class="btn btn-primary" /> </td>' +
                            '</tr>';

                        $("#cuerpo").append(tr)

                    });

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus + ": " + XMLHttpRequest.responseText);

                }
            });
        }

        function funcionchange() {

            var talla = $('[id*=drpTallas] option:selected').text();
            var idporder = document.getElementById("<%= hfidPorder.ClientID %>").value;

            $.ajax({
                type: "POST",
                url: "MedidasNewIntexReproMPl.aspx/GetTotalCorteXTalla",
                data: "{ idporder: '" + idporder + "','talla':'" + talla + "'}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d != "") {
                        console.log(data.d);
                        document.getElementById("lblunidades").innerHTML = data.d;
                    }
                    else {
                        document.getElementById("lblunidades").innerHTML = "0";
                        console.log(1);
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus + ": " + XMLHttpRequest.responseText);

                }
            });

            limpiar2();
            var idestilo = document.getElementById("<%= hdnidstyle.ClientID %>").value;
            creartabla(idestilo);
        }

        function selected() {

            var talla = $('[id*=drpTallas] option:selected').text();

            if (talla != 'Seleccione...') {
                var rowin, table = document.getElementById("tablePMedidas");

                for (var i = 0; i < table.rows.length; i++) {

                    table.rows[i].onclick = function () {

                        rowin = this.rowIndex;
                        //console.log(rowin);

                        //punto de medida
                        var v2 = table.rows[rowin].cells[1].innerHTML;
                        //tolerancia Maxima
                        var v4 = table.rows[rowin].cells[3].innerHTML;
                        //tolerancia Minima
                        var v5 = table.rows[rowin].cells[4].innerHTML;

                        // var texto = document.getElementById("txtvalor");
                        // texto.innerHTML = "0";

                        document.getElementById("lblseccionMedidaM").innerHTML = v2;
                        // document.getElementById("lblValorM").innerHTML = v3;

                        document.getElementById("lbltolmax").innerHTML = v4;
                        document.getElementById("lbltolmin").innerHTML = v5;
                        //  document.getElementById("lblvalor3").innerHTML = v5;
                        $('#<%=hdrowIndex.ClientID %>').val(rowin);
                        // console.log(v1 +" "+ v2 + " " + v3 +" "+ v4 +" " +v5);
                        mostrarmodal();
                    }
                }
            }
            else {
                alertMaster('Debe seleccionar la talla!!!, ', 'info');
            }


        }

        function mostrarmodal() {
            $('#ModalCapturaInfo').modal('show');
        }



        function changeText(name) {

            var talla = $('[id*=drpTallas] option:selected').text();

            if (talla != 'Seleccione...') {

                var numerico = 0.0;
                var bspace = name.indexOf(" ");

                //obtiene la diferencia de especifcacion con la medida real 
                if (bspace > 1) {
                    var valorentero = eval(name.slice(0, bspace));
                    var decimal = eval(name.slice(bspace + 1, name.length + 1));
                    numerico = valorentero + decimal;
                }
                else {
                    numerico = eval(name);
                }


                //console.log(numerico);
                //variables de controles
                var puntoMedidaM = document.getElementById("lblseccionMedidaM");
                var puntoMedida = document.getElementById("lblPuntoMedida");
                var MedidaIngresada = document.getElementById("lblMedidaIngresada");
                //estado de medida
                var status = document.getElementById("lblEstatus");
                MedidaIngresada.innerHTML = name;

                var tolMax = document.getElementById("lbltolmax").innerHTML;
                var tolMin = document.getElementById("lbltolmin").innerHTML;

                var n1 = parseFloat(numerico);
                var n2 = parseFloat(tolMax);
                var n3 = parseFloat(tolMin);

                puntoMedida.innerHTML = puntoMedidaM.innerHTML;

                var ban = "";
                var ban2 = "";
                if (n1 == 0) { //<= n2 && n1 >= n3

                    status.innerHTML = "In Tolerance";
                    // maxminResto.innerHTML = "0";
                    status.className = "text-success";
                    ban = "In Spec";

                    ban2 = name == "0" ? "In Spec" : name == "+0" ? "Tol Max" : "Tol Min";

                }
                else {

                    if (n1 > 0) {
                        // var r = n1 - n2;
                        // maxminResto.innerHTML = r.toFixed(3);
                        status.innerHTML = "Big";
                        // leyenda.innerHTML = "Exceso";
                        status.className = "text-danger";
                        ban = "Big";
                        ban2 = "Big"
                    }
                    else if (n1 < 0) {
                        // var r = n3 - n1;
                        // leyenda.innerHTML = "Deficit";
                        // maxminResto.innerHTML = r.toFixed(3);
                        status.innerHTML = "Small";
                        status.className = "text-primary";
                        ban = "Small";
                        ban2 = "Small";
                    }

                }

                var indide = document.getElementById("<%= hdrowIndex.ClientID %>").value;
                var table = document.getElementById("tablePMedidas");
                table.rows[indide].cells[5].innerHTML = numerico;
                table.rows[indide].cells[6].innerHTML = ban;
                table.rows[indide].cells[7].innerHTML = name;
                table.rows[indide].cells[8].innerHTML = ban2;
                table.rows[indide].style.backgroundColor = '#808080';

                $('#ModalCapturaInfo').modal('hide');

                var count = 0;
                var cantidad = table.rows.length;
                for (var i = 0; i < cantidad; i++) {

                    if (table.rows[i].cells[5].innerHTML != "") {
                        count++;
                    }
                }

                if (count === 2) {

                    guarda1Elemento();

                }
                else {

                    alertTime("'Medida Asignada!'");
                }

            }
            else {
                alertMaster('Debe seleccionar la talla!!!, ', 'info');
            }
        }

        function alertMaster(_title, _type) {
            swal({
                title: _title,
                type: _type
            });
        }

        function alertTime(texto) {
            swal({
                type: 'success',
                title: texto,
                timer: 1000,
                showConfirmButton: false
            });
        }


        function LimpiarFocus() {
            limpiar();
            limpiar2();
        }

        function limpiar() {
            $("#cuerpo").html("");
            $('#<%=drpTallas.ClientID%>').empty();

            document.getElementById("lblunidades").innerHTML = "";

            $('#<%=txtPorder.ClientID%>').val('');
            $('#<%=hfidPorder.ClientID%>').val('');

            $('#<%=txtstyle.ClientID%>').val('');
            $('#<%=hdnidstyle.ClientID %>').val('');
            $('#<%=txtcolor.ClientID %>').val('');

        }

        function limpiar2() {

            document.getElementById("lblseccionMedidaM").innerHTML = "";
            document.getElementById("lblPuntoMedida").innerHTML = "";
            document.getElementById("lblEstatus").innerHTML = "";
            document.getElementById("lblMedidaIngresada").innerHTML = "";

            document.getElementById("lbltolmax").innerHTML = "0";
            document.getElementById("lbltolmin").innerHTML = "0";

        }

        function reiniciarTabla() {
            var idestilo = document.getElementById("<%= hdnidstyle.ClientID %>").value;
            creartabla(idestilo);

            limpiar2();
        }

    </script>

    <script>
        function guarda1Elemento() {

            var drp = document.getElementById("<%= drpTallas.ClientID %>");

            var table = document.getElementById("tablePMedidas");
            var idporder = document.getElementById("<%= hfidPorder.ClientID %>").value;
            var talla = drp.options[drp.selectedIndex].text;
            var unidades = document.getElementById("<%= drpTallas.ClientID %>").value;
            var idestilo = document.getElementById("<%= hdnidstyle.ClientID %>").value;
            var usuario = document.getElementById("<%= hdnusuario.ClientID %>").value;
            var color = document.getElementById("<%= txtcolor.ClientID %>").value;
            var cont = 0;

            ///verifica que la pieza este en espec
            for (var i = 0; i < table.rows.length; i++) {
                if (table.rows[i].cells[6].innerHTML != "In Spec" && table.rows[i].cells[5].innerHTML != "") {
                    cont++;
                }
            }

            var estado = "";
            if (cont == 0) {
                estado = "true";
            }
            else {
                estado = "false";
            }


            //creando  string con formato Xml para enviar detalle de medidas
            var xmlString = "<Medidas> "
            for (var rowin = 0; rowin < table.rows.length; rowin++) {

                if (table.rows[rowin].cells[5].innerHTML != "") {

                    var idpuntomedida = table.rows[rowin].cells[2].innerHTML;
                    var medidaFlotante = table.rows[rowin].cells[5].innerHTML;
                    var medidaTexto = table.rows[rowin].cells[7].innerHTML;
                    var estado1 = table.rows[rowin].cells[6].innerHTML;
                    var estado2 = table.rows[rowin].cells[8].innerHTML;


                    let nodo = "       <Detalle idPuntoMedida=\"" + idpuntomedida + "\" medidaFlotante=\"" + medidaFlotante + "\" medidaTexto=\"" + medidaTexto + "\" estado=\"" + estado1 + "\"  estado2=\"" + estado2 + "\" />";
                    // console.log(nodo);
                    xmlString = xmlString.concat(nodo);

                }

            }

            xmlString = xmlString.concat("</Medidas>");
           // console.log(xmlString);
            //fin de codigo creacion string Xml

            var objMaster = "{'idPorder': '" + idporder + "','idEstilo': '" + idestilo + "','size': '" + talla + "','estado': '" + estado + "','usuario':'" + usuario + "','unidadesXtalla':'" + unidades + "','color':'" + color + "','xmlstring':'" + xmlString + "'}";

            $.ajax({
                url: "MedidasNewIntexReproMPl.aspx/GuardarMasterIntex",
                type: "POST",
                data: objMaster,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d != "") {
                        if (data.d > 0) {

                            console.log(data.d);
                            document.getElementById("lblunidades").innerHTML = data.d;

                            var cod = document.getElementById("<%= hdnidstyle.ClientID %>").value;
                            creartabla(cod);
                            limpiar2();

                            alertTime("'Pieza Agregada Correctamente!'");
                        }
                        else if (data.d == 0) {
                            alertTime("'Llego al limite de unidades permitidas para esta talla!'");
                        }
                    }
                    else {
                        alertMaster('Error vuelva a intentar', 'error');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(textStatus)
                    alertMaster(textStatus, 'error');
                }
            });
        }

        function guardardatos(idresultado) {

            var table = document.getElementById("tablePMedidas");

            //var lista = "{";
            if (idresultado > 0) {

                for (var rowin = 0; rowin < table.rows.length; rowin++) {

                    var idpuntomedida = table.rows[rowin].cells[2].innerHTML;
                    var medidaFlotante = table.rows[rowin].cells[5].innerHTML;
                    var medidaTexto = table.rows[rowin].cells[7].innerHTML;
                    var estado = table.rows[rowin].cells[6].innerHTML;
                    var estado2 = table.rows[rowin].cells[8].innerHTML;
                    var objDetail = "{'idPuntoMedida': '" + idpuntomedida + "','medidaFlotante': '" + medidaFlotante + "','medidaTexto': '" + medidaTexto + "','estado': '" + estado + "','estado2': '" + estado2 + "','id': '" + idresultado + "'}";

                    // listDetail = listDetail
                    //   lista = lista + objDetail;

                    $.ajax({
                        url: "MedidasNewIntexReproMPl.aspx/GuardarDetailIntex",
                        type: "POST",
                        data: objDetail,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d != "") {
                                console.log(data.d);
                                document.getElementById("lblunidades").innerHTML = data.d;

                            }
                            else {
                                console.log(1);
                                //  alertMaster('Error vuelva a intentar', 'error');
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            console.log(2);
                            // alertMaster(textStatus, 'error');
                        }

                    });

                }

                // lista = lista + "}";

                var cod = document.getElementById("<%= hdnidstyle.ClientID %>").value;
                creartabla(cod);
                limpiar2();

                alertTime("'Pieza Agregada Correctamente!'");

            }
            else if (idresultado == 0) {
                alertTime("'Llego al limite de unidades permitidas para esta talla!'");
            }
            else {
                alertTime("'Ha ocurrido un error, Intente Nuevamente!!!'");
            }

        }


    </script>




    <link href="jquery-ui.css" rel="stylesheet" />
    <script src="jquery-1.9.1.js"></script>
    <script src="jquery-ui.js"></script>

    <link href="StyleSheetAuto.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Measurement suggestion </strong></h3>
            </div>
            <div class="panel-body">

                <div id="contenedorMain" class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 quitarpadd-iz-der">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            Corte:
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></span>
                                        </span>
                                        <div class="ui-widget" style="text-align: left;">
                                            <asp:TextBox ID="txtPorder" placeholder="Codigo Order" required onfocus="LimpiarFocus()" class="textboxAuto text-info form-control" Font-Size="14px" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                            Tallas:
                                     <asp:DropDownList ID="drpTallas" onchange="funcionchange()" CssClass="form-control" runat="server"></asp:DropDownList>

                            Color:
                            <asp:TextBox runat="server" CssClass="form-control" placeholder="color" Font-Size="14px" ID="txtcolor" Enabled="false" />
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            Estilo:
                            <asp:TextBox runat="server" CssClass="form-control" placeholder="Estilo" Font-Size="14px" ID="txtstyle" />
                            <asp:HiddenField ID="hfidPorder" runat="server" />
                            <asp:HiddenField ID="hdnidstyle" runat="server" />
                            <asp:HiddenField ID="hdrowIndex" runat="server" />
                            <asp:HiddenField ID="hdnfieltest" runat="server" />
                            <asp:HiddenField ID="hdnusuario" runat="server" />


                            <div class="btn btn-primary" style="margin-top: 20px">
                                Unidades Medidas:
                                            <label class="badge" id="lblunidades" />
                            </div>

                        </div>

                        <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pad-0_marr-15">

                                <article class="form-group">
                                    <h4 class="bg-primary text-center">
                                        <label>Status de Medida</label>

                                    </h4>
                                    <h3 class="text-muted text-center">
                                        <label id="lblEstatus"></label>
                                    </h3>
                                </article>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pad-0_marr-15">
                                <article class="form-group">
                                    <h4 class="bg-info text-center">
                                        <label>Medida ingresada</label>
                                    </h4>
                                    <h3 class="text-muted text-center">
                                        <label id="lblMedidaIngresada"></label>
                                    </h3>
                                </article>

                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pad-0_marr-15">
                                <article class="form-group">
                                    <h4 class="bg-success text-center">
                                        <label>Punto de Medida</label>
                                    </h4>
                                    <h3 class="text-muted text-center">
                                        <label id="lblPuntoMedida"></label>
                                    </h3>
                                </article>

                            </div>
                        </section>


                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Secciones de Medida
                            </div>
                            <div class="panel-body">
                                <table id="tablePMedidas" class="table table-responsive table-striped">
                                    <tbody id="cuerpo">
                                    </tbody>
                                </table>

                                <blockquote>
                                    <input type="button" value='Limpiar Medidas' onclick="reiniciarTabla()" class="btn btn-info" />
                                    <p>La función del comando es borrar la medida de un punto seleccionado por equivocación</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>


    <aside class="modal fade fullscreen-modal" id="ModalCapturaInfo">
        <div class="modal-dialog">
            <div class="modal-content">

                <article class="modal-header" style="padding-bottom: 3px">
                    <button id="cerrar" class="close" data-dismiss="modal" aria-hidden="true" type="button">&times;</button>

                    <h4 style="margin: 5px; padding: 5px;">Ingreso de Medidas</h4>
                    <h4 style="margin: 5px; padding: 5px;">
                        <label class="label label-warning" style="font-size: large">Punto de Medida</label>
                        <label id="lblseccionMedidaM" class="classLabel"></label>
                        <label id="lbltolmax" class="classLabel" style="visibility: collapse"></label>
                        <label id="lbltolmin" class="classLabel" style="visibility: collapse"></label>
                    </h4>

                </article>

                <article class="modal-body" style="padding-top: 5px">

                    <div class="row">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0 !important; padding: 0 !important">
                            <input type="button" value='+2' name='+2' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='+1 7/8' name='+1 7/8' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='+1 3/4' name='+1 3/4' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='+1 5/8' name='+1 5/8' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='+1 1/2' name='+1 1/2' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='+1 3/8' name='+1 3/8' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='+1 1/4' name='+1 1/4' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='+1 1/8' name='+1 1/8' onclick="changeText(this.name)" class="btn btn-danger withB" />
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0 !important; padding: 0 !important">
                            <input type="button" value='+1' name='+1' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='+7/8' name='+7/8' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='+3/4' name='+3/4' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='+5/8' name='+5/8' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='+1/2' name='+1/2' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='+3/8' name='+3/8' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='+1/4' name='+1/4' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='+1/8' name='+1/8' onclick="changeText(this.name)" class="btn btn-success withB3" />
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0 !important; padding: 0 !important">
                            <input type="button" value='0' name='0' onclick="changeText(this.name)" class="btn btn-default withB" />
                            <input type="button" value='+0' name='+0' onclick="changeText(this.name)" class="btn btn-info withB" />
                            <input type="button" value='-0' name='-0' onclick="changeText(this.name)" class="btn btn-warning withB" />
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0 !important; padding: 0 !important">
                            <input type="button" value='-1/8' name='-1/8' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='-1/4' name='-1/4' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='-3/8' name='-3/8' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='-1/2' name='-1/2' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='-5/8' name='-5/8' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='-3/4' name='-3/4' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='-7/8' name='-7/8' onclick="changeText(this.name)" class="btn btn-success withB3" />
                            <input type="button" value='-1' name='-1' onclick="changeText(this.name)" class="btn btn-success withB3" />
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0 !important; padding: 0 !important">
                            <input type="button" value='-1 1/8' name='-1 1/8' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='-1 1/4' name='-1 1/4' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='-1 3/8' name='-1 3/8' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='-1 1/2' name='-1 1/2' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='-1 5/8' name='-1 5/8' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='-1 3/4' name='-1 3/4' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='-1 7/8' name='-1 7/8' onclick="changeText(this.name)" class="btn btn-danger withB" />
                            <input type="button" value='-2' name='-2' onclick="changeText(this.name)" class="btn btn-danger withB" />
                        </div>


                    </div>
                </article>
            </div>
        </div>
    </aside>




    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="ventanacolor">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h1>Asignar Color</h1>
                </div>
                <div class="modal-body">
                    <div style="color: #006699; font-size: 14px">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label"></label>
                            <asp:DropDownList ID="drpColor" CssClass="form-control" onchange="selectColor()" runat="server">
                                <asp:ListItem Text="Select..." Value="0" />
                                <asp:ListItem Text="968" Value="1" />
                                <asp:ListItem Text="980" Value="2" />
                                <asp:ListItem Text="STW" Value="3" />
                                <asp:ListItem Text="DST" Value="4" />
                                <asp:ListItem Text="DPS" Value="5" />
                                <asp:ListItem Text="DKH" Value="6" />
                            </asp:DropDownList>

                            <input type="button" value='Omitir' onclick="ocultarVentanaColor()" class="btn btn-success withB" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

