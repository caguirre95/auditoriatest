﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using SistemaAuditores.DataAccess;

public class Handler : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            if (context.User.Identity.IsAuthenticated == true)
            {
                if (HttpContext.Current.User.IsInRole("PackList"))
                {
                    string user = HttpContext.Current.User.Identity.Name;
                    string query = " select modulo from tblUserEnvio where nameMemberShip = '" + user + "'";
                    DataTable acceso = DataAccess.Get_DataTable(query);
                    string a = acceso.Rows[0][0].ToString();

                    if (a == "Linea")
                    {
                        context.Response.Redirect("/InfoLinea.aspx", false);
                    }
                    else if (a == "Intex")
                    {
                        context.Response.Redirect("/InfoLineaIntex.aspx", false);
                    }
                }
                else if (HttpContext.Current.User.IsInRole("PackAdmon"))
                {

                    string user = HttpContext.Current.User.Identity.Name.ToLower();
                    string query = " select modulo from UserPlanta where LOWER(LowerName) = '" + user + "'";
                    DataTable acceso = DataAccess.Get_DataTable(query);
                    if (acceso.Rows.Count > 0)
                    {
                        if (acceso.Rows[0][0].ToString().Equals("Linea"))
                        {
                            context.Response.Redirect("/EnviosBulto.aspx", false);
                        }
                        else if (acceso.Rows[0][0].ToString().Equals("Intex"))
                        {
                            context.Response.Redirect("/EnviosBultoIntex.aspx", false);
                        }
                        else
                        {
                            try
                            {
                                if (user.Equals("danilo") || user.Equals("malvarado") || user.Equals("jvalenzuela"))                            
                                     context.Response.Redirect("/ReporteEmpaque/ReporteEmpacado.aspx", false);
                                else                        
                                context.Response.Redirect("/adminReport/ReportesEnvios.aspx", false);
                            }
                            catch (Exception ex)
                            {
                                string message = ex.Message;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            string resp = ex.Message;
        }
    }



    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}