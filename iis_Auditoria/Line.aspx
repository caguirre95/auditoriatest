﻿<%@ Page Title="Admon" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Line.aspx.cs" Inherits="Line" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v16.2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">  

    <script type="text/javascript">

        function oki() {
            swal({
                title: 'Exito!',
                text: "Registro Actualizado Correctamente",
                type: 'success'
            });
        }

        function oki_bad() {
            swal({
                title: 'Ups!',
                text: "No se Actualizo el Registro",
                type: 'warning'
            });
        }

        function OnGetSelectedFieldValues1(selectedValues) {
            if (selectedValues.length == 0) return;
            for (i = 0; i < selectedValues.length; i++) {

                for (j = 0; j < selectedValues[i].length; j++) {
                    idline.Set('Id', selectedValues[i][0]);
                    txtlineedit.SetText(selectedValues[i][1]);
                }
                editarline.Show();
            }
        };

        function gridcampo1() {
            swal({
                title: 'Por Favor!',
                text: "Seleccione Cliente!!",
                type: 'info'
            });
        }

        function gridcampo2() {
            swal({
                title: 'Por Favor!',
                text: "Seleccione Linea a Actualizar y Seleccione un Cliente!!",
                type: 'info'
            });
        }

        function gridcampo3() {
            swal({
                title: 'Por Favor!',
                text: "Ingrese Planta y Cliente de la Linea!!",
                type: 'info'
            });
        }

        function okioki() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }
        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <style>
        .mar {
            margin-top: 10px;
        }
    </style>

    <div class="container mar">
        <div class="panel panel-primary">
            <div class="panel-heading"><strong>Line</strong></div>
            <div class="panel-body">

                <asp:UpdatePanel ID="panelup" runat="server">
                    <ContentTemplate>

                        <div>
                            Client :
                    <asp:DropDownList ID="drpcliente" runat="server" CssClass="form-control" Width="180px" AutoPostBack="true" OnSelectedIndexChanged="drpcliente_SelectedIndexChanged"></asp:DropDownList>
                        </div>

                        <div style="margin-top: 30px">
                            <dx:ASPxGridView ID="gridlinea" runat="server" KeyFieldName="id" Width="100%" Theme="Material" OnDataBinding="grid_linea_DataBinding" OnCustomCallback="grid_linea_CustomCallback" ClientInstanceName="gridlinea">
                                <SettingsSearchPanel Visible="true" />
                                <SettingsBehavior AllowSelectSingleRowOnly="true" />
                                <Columns>
                                    <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="true" VisibleIndex="0" Caption="Id">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="id" ReadOnly="true" Visible="false" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Linea" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Estado" Caption="Estado" VisibleIndex="2">
                                        <DataItemTemplate>
                                            <dx:ASPxCheckBox ID="chklineact" runat="server" Theme="Material" Text="Estado" Checked='<%# Bind("Estado") %>' ValueChecked="true" ValueUnchecked="false" Enabled="true"></dx:ASPxCheckBox>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings ShowGroupFooter="VisibleAlways" />
                                <SettingsPager PageSize="5" />
                            </dx:ASPxGridView>
                        </div>

                        <div style="margin-top: 30px">
                            <div class="col-lg-2">
                                <%-- <dx:ASPxButton ID="btnaddline" runat="server" CssClass="btn btn-primary" Width="150px" Text="Add Line" ClientInstanceName="btnaddline" ClientVisible="true"></dx:ASPxButton>--%>
                                <button id="btnaddLine" runat="server" class="btn btn-primary" data-toggle="modal" data-target="#ventanamodal" style="width: 150px; height: 40px">Add Line</button>
                            </div>
                            <div class="col-lg-2">
                                <button id="btnedit" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="width: 150px; height: 40px">Edit Line</button>
                            </div>
                            <div class="col-lg-2">
                                <dx:ASPxButton ID="btnestadoact" runat="server" CssClass="btn btn-primary" Width="150px" Text="Update State" OnClick="btnestadoact_Click" ClientInstanceName="btnestadoact" ClientVisible="true"></dx:ASPxButton>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
    </div>

    <%--Ventana Modal Prueba--%>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Line</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div style="padding:25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Linea</span>
                                </div>
                                <div class="col-lg-9">                                    
                                    <dx:ASPxComboBox ID="dr" runat="server" NullText="Select..." Theme="Material" ClientInstanceName="dr"></dx:ASPxComboBox>
                                </div>
                            </div>
                            <br />
                            <div style="padding:25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Cliente</span>
                                </div>
                                <div class="col-lg-9">                                   
                                    <dx:ASPxComboBox ID="drpc" runat="server" NullText="Select..." Theme="Material" ClientInstanceName="drpc"></dx:ASPxComboBox>
                                </div>
                            </div>
                            <br />
                            <div style="padding:25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Nuevo Numero</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxTextBox ID="txtnewline" runat="server" NullText="New Line..." Theme="Material" Width="180px" ClientInstanceName="txtnewline" Enabled="true" MaxLength="5">
                                        <ClientSideEvents KeyUp="function(s, e) {
                                                        s.SetText(s.GetText().toUpperCase());
                                                        }" />
                                    </dx:ASPxTextBox>
                                </div>
                            </div>
                            <br />
                            <div style="padding:25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Activa</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxCheckBox ID="lineact" runat="server" ValueChecked="true" ValueUnchecked="false" Theme="Material"></dx:ASPxCheckBox>
                                </div>
                            </div>
                            <div style="padding:25px">
                                <div class="col-lg-12" style="text-align: center">
                                    <dx:ASPxButton ID="btnaceptaredit" runat="server" Theme="Material" Text="Save Edit Line" Width="150px" OnClick="btnaceptaredit_Click"></dx:ASPxButton>
                                </div>
                            </div>
                            <br />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>               
            </div>
        </div>
    </div>

    <div class="modal fade" id="ventanamodal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Line</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Planta</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxComboBox ID="planta" runat="server" NullText="Select..." Theme="Material" ClientInstanceName="planta" Width="180px"></dx:ASPxComboBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Cliente</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxComboBox ID="combocliente" runat="server" NullText="Select..." Theme="Material" ClientInstanceName="combocliente" Width="180px"></dx:ASPxComboBox>
                                </div>
                            </div>

                            <div style="padding: 25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Linea</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPxTextBox ID="txtnew" runat="server" NullText="Add..." Theme="Material" ClientInstanceName="txtnew" Width="180px" MaxLength="5"></dx:ASPxTextBox>
                                </div>
                            </div>

                            <div style="padding:25px">
                                <div class="col-lg-3" style="text-align: -webkit-right; font-weight: bold;">
                                    <span>Active</span>
                                </div>
                                <div class="col-lg-9">
                                    <dx:ASPXCheckBox ID="chkactnewline" runat="server" ValueChecked="true" ValueUnchecked="false" Theme="Material"></dx:ASPXCheckBox>
                                </div>
                            </div>

                            <div style="padding:25px">
                                <div class="col-lg-12" style="text-align:center;"> 
                                    <dx:ASPxButton ID="btnagregarnewline" runat="server" Theme="Material" Text="Save" Width="150px" OnClick="btnagregarnewline_Click"></dx:ASPxButton>
                                </div>
                            </div>
                            <br />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>               
            </div>
        </div>
    </div>
</asp:Content>

