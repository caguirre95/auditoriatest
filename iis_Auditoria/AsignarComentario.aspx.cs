﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AsignarComentario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (Page.User.IsInRole("Administrator"))
            {
               
            }
            else
            {
                //FormsAuthentication.SignOut();
                Response.Redirect("/NotFound.aspx");
            }
        }
        catch (Exception)
        {

            throw;
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<POClass> GetPorderComment(string pre)
    {
        try
        {
            List<POClass> list = new List<POClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("  select top 10 p.Id_Order,p.POrder,p.Comments from POrder p where p.POrder like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                POClass obj = new POClass();

                obj.idorder = int.Parse(dt.Rows[i][0].ToString());
                obj.porder = dt.Rows[i][1].ToString();
                obj.comentario = dt.Rows[i][2].ToString();

                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    //  [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GuardarComentario(int idorder, string porder, string comentario)
    {
        try
        {
            int i=0;
            if (idorder!=0)
            {
                string queryUpdate = "update POrder set Comments='"+ comentario +"' where Id_Order="+ idorder;
                SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

                cn.Open();
                SqlCommand cmd = new SqlCommand(queryUpdate,cn);
                i=cmd.ExecuteNonQuery();
                cn.Close();

            }

            if (i==0)
            {
                return "false";
            }

            return "true";
        }
        catch (Exception)
        {
            return "false";
        }

    }



    public class POClass
    {
        public int idorder { get; set; }
        public string porder { get; set; }
        public string comentario { get; set; }
    }

}