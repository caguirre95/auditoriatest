﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminStyleSize2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CargaTalla();
            cargaPuntoM();
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<tallaClass> GetTalla(string pre)
    {
        try
        {
            List<tallaClass> list = new List<tallaClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top(30) idTalla, RTRIM(Talla) Talla from tbTalla where Talla like '%" + pre + "%' and Estado = 1", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                tallaClass obj = new tallaClass();

                obj.id = int.Parse(dt.Rows[i][0].ToString());
                obj.talla = dt.Rows[i][1].ToString();


                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }



    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<PMClass> GetPuntoMedida(string pre)
    {
        try
        {
            List<PMClass> list = new List<PMClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select top(17) idPuntosM, RTRIM(NombreMedida) NombreMedida from tblPuntosMedida where NombreMedida like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                PMClass obj = new PMClass();

                obj.idp = int.Parse(dt.Rows[i][0].ToString());
                obj.nombre = dt.Rows[i][1].ToString();

                list.Add(obj);
            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class tallaClass
    {
        public int id { get; set; }
        public string talla { get; set; }
    }

    public class PMClass
    {
        public int idp { get; set; }
        public string nombre { get; set; }
    }

    void CargaTalla()
    {
        try
        {
            if (hdnidestilo.Value != string.Empty)
            {
                var dt = DataAccess.Get_DataTable(" select top(30) t.idTalla, RTRIM(t.Talla) Talla from tbEstiloTalla et join tbTalla t on t.idTalla = et.idTalla join Style s on s.Id_Style = et.idEstilo" +
                                              " where s.Id_Style = " + hdnidestilo.Value + " and t.Estado = 1");
                drptallas.DataSource = dt;
                drptallas.DataValueField = "idTalla";
                drptallas.DataTextField = "Talla";
                drptallas.DataBind();
                drptallas.Items.Insert(0, "Select...");
            }
            else
            {
                drptallas.DataSource = null;
                drptallas.DataBind();
            }

        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        CargaTalla();
    }

    void cargaPuntoM()
    {
        try
        {
            var dt = DataAccess.Get_DataTable("select idPuntosM, NombreMedida from tblPuntosMedida order by NombreMedida");

            GridViewtallas.DataSource = dt;
            GridViewtallas.DataBind();

        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnspc_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtestilo.Text != string.Empty && hdnidestilo.Value != string.Empty && (drptallas.SelectedValue != "" && drptallas.SelectedValue != "Select..."))
            {
                var dt = DataAccess.Get_DataTable(" select es.idEspecificacion, s.Style, t.Talla, pm.NombreMedida, es.Valor, es.TolMax, es.Tolmin from tbEspecificacion es"
                                                + " join tbEstiloTalla et on et.idEstiloTalla = es.idEstiloTalla"
                                                + " join Style s on s.Id_Style = et.idEstilo"
                                                + " join tbTalla t on t.idTalla = et.idTalla"
                                                + " join tblPuntosMedida pm on pm.idPuntosM = es.idPuntoDeMedida"
                                                + " where ISNULL(es.Valor,-1) = -1 and s.Id_Style = " + hdnidestilo.Value + " and t.idTalla = " + drptallas.SelectedValue
                                                + " order by pm.NombreMedida");
                grdsp.DataSource = dt;
                grdsp.DataBind();
            }
            else if (txtestilo.Text != string.Empty && hdnidestilo.Value != string.Empty && (drptallas.SelectedValue != "" || drptallas.SelectedValue != "Select..."))
            {
                var dt = DataAccess.Get_DataTable(" select es.idEspecificacion, s.Style, t.Talla, pm.NombreMedida, es.Valor, es.TolMax, es.Tolmin from tbEspecificacion es"
                                               + " join tbEstiloTalla et on et.idEstiloTalla = es.idEstiloTalla"
                                               + " join Style s on s.Id_Style = et.idEstilo"
                                               + " join tbTalla t on t.idTalla = et.idTalla"
                                               + " join tblPuntosMedida pm on pm.idPuntosM = es.idPuntoDeMedida"
                                               + " where ISNULL(es.Valor,-1) = -1 and s.Id_Style = " + hdnidestilo.Value 
                                               + " order by pm.NombreMedida");
                grdsp.DataSource = dt;
                grdsp.DataBind();
            }
            else
            {
                string tipo = "warning";
                string mess = "Ingrese los datos correctos por favor!";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void btnupdpm_Click(object sender, EventArgs e)
    {
        try
        {
            cargaPuntoM();
        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void Btnasigpm_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdnidestilo.Value != string.Empty && (drptallas.SelectedValue != "" && drptallas.SelectedValue != "Select..."))
            {
                int idestilo = Convert.ToInt32(hdnidestilo.Value);
                int idtalla = Convert.ToInt32(drptallas.SelectedValue);
                int cont = 0;

                for (int i = 0; i < GridViewtallas.Rows.Count; i++)
                {
                    var row = (GridViewRow)GridViewtallas.Rows[i];

                    var chk = (CheckBox)row.FindControl("chkSeleccionado");
                    var lblidpm = (Label)row.FindControl("lblidpm");

                    if (chk.Checked)
                    {
                        var idpm = Convert.ToInt32(lblidpm.Text);

                        var resp = OrderDetailDA.saveAsignacionPuntoM(idestilo, idtalla, idpm);

                        if (resp != "OK")
                        {
                            cont++;
                        }
                    }
                }


                if (cont == 0)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "invocarfuncion", "cerrarmodal()", true);

                    string tipo = "success";
                    string mess = "Guardado exitosamente!";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);

                    cargaPuntoM();

                    Limpiar();
                }
                else
                {
                    string tipo = "error";
                    string mess = "verificar algunos puntos de medida no se agregaron al estilo";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);

                    Limpiar();
                }

            }
            else
            {
                string tipo = "warning";
                string mess = "Debe seleccionar un Estilo y una Talla para agregar Puntos de medida";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

    void Limpiar()
    {
        txtestilo.Text = "";
        hdnidestilo.Value = "";
        CargaTalla();
    }

    protected void btnespecif_Click(object sender, EventArgs e)
    {
        try
        {
            if (grdsp.Rows.Count > 0)
            {
                int cont = 0;

                foreach (GridViewRow item in grdsp.Rows)
                {
                    string resp = "";
                    var lblidesp = (Label)item.FindControl("lblidesp");
                    var txtvalor = (TextBox)item.FindControl("txtvalor");
                    var txttolmax = (TextBox)item.FindControl("txttolmax");
                    var txttolmin = (TextBox)item.FindControl("txttolmin");

                    if (txtvalor.Text != string.Empty && txttolmax.Text != string.Empty && txttolmin.Text != string.Empty)
                    {
                        var idespecificacion = Convert.ToInt32(lblidesp.Text);
                        var valor = Math.Round(float.Parse(txtvalor.Text), 2);
                        var valorMax = Math.Round(float.Parse(txtvalor.Text), 2) + Math.Round(float.Parse(txttolmax.Text), 2);
                        var valorMin = Math.Round(float.Parse(txtvalor.Text), 2) - Math.Round(float.Parse(txttolmin.Text), 2);

                        resp = OrderDetailDA.updateEspecificacion(idespecificacion, valor, valorMax, valorMin, Page.User.Identity.Name);

                        if (resp == "OK")
                        {
                            cont++;
                        }
                    }

                    if (cont > 0)
                    {
                        string tipo = "success";
                        string mess = "Guardado exitosamente!";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);

                        grdsp.DataSource = null;
                        grdsp.DataBind();

                        Limpiar();
                    }
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnspec_Click(object sender, EventArgs e)
    {
        Response.Redirect("/AdminStyleSize2.aspx");
    }
}