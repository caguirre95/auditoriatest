﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using SistemaAuditores.DataAccess;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using DevExpress.Web;

public partial class InspeccionLinea : System.Web.UI.Page
{
    int total = 0, totaldef=0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
           if (User.Identity.IsAuthenticated == true)
            {
                DataTable dt = new DataTable();
                string Id_Order = (string)Request.QueryString["id_order"] ?? "NA";
                if (Id_Order == "")
                {
                    Id_Order = "0";
                }
                //dt = POrderDA.Get_OrderbyId(Id_Order);
                //lblorder1.Text = Convert.ToString(dt.Rows[0][3]);
                //lblorder2.Text = lblorder1.Text;
                //DataTable Customerdt = new DataTable();
                //string sql = "select Cliente from Cliente where Id_Cliente=" + dt.Rows[0][1];
                //Customerdt = DataAccess.Get_DataTable(sql);
                //lblcustomer.Text = Convert.ToString(Customerdt.Rows[0][0]);
                //Customer.Text = Convert.ToString(Customerdt.Rows[0][0]);
                Customer.Text = "DICKIES";
                lblinspector.Text = Page.User.Identity.Name;
                lblfecha.Text = DateTime.Now.Date.ToShortDateString();

                Load_Data();
            }
        }
    }

  /*  protected void btndefects_Click(object sender, EventArgs e)
    {
        int startIndex = ASPxGridView1.PageIndex * ASPxGridView1.SettingsPager.PageSize;
        int endIndex = Math.Min(ASPxGridView1.VisibleRowCount, startIndex + ASPxGridView1.SettingsPager.PageSize);
        Label lbloper1 = new Label();
        for (int i = startIndex; i < endIndex; i++)
        {
            if (ASPxGridView1.Columns[1].VisibleIndex == i)
            {
                lbloper1 = (Label)ASPxGridView1.FindRowCellTemplateControl(i, (GridViewDataColumn)ASPxGridView1.Columns[1], string.Format("lbloperario_{0}", i));
                lbloperario.Text = lbloper1.Text;
            }
        }
        this.AsignarDefectos.ShowOnPageLoad = true;
       
        //ASPxGridView grid = ((ASPxGridView)btn_defects.FindControl("lbloperario");
        
        
        //Label lbloper1 = (Label)grid.FindControl("lbloperario");
        
            //lbloper1.Text;        
    }*/

   /* protected void lbloperario_Init(object sender, EventArgs e)
    {
        Label lbloperario = (Label)sender;

        GridViewDataItemTemplateContainer templateContainer = (GridViewDataItemTemplateContainer)lbloperario.NamingContainer;
        lbloperario.ID = string.Format("lbloperario_{0}", templateContainer.VisibleIndex);

        //lbloperario.ClientInstanceName = string.Format("cmb_{0}", templateContainer.VisibleIndex);
    }*/

    public void Load_Data()
    {
        DataTable defectos_dt = new DataTable();
        defectos_dt = DataAccess.Get_DataTable("select id_defectos, nombre from Defectos");

        /*for (int i = 0; i <= defectos_dt.Rows.Count - 1; i++)
        {
            Label box1 = (Label)GridView1.Rows[i].Cells[1].FindControl("LabelDefect");
            box1.Text = Convert.ToString(defectos_dt.Rows[i][2]);
        } */
        GridView1.DataSource = defectos_dt;
        GridView1.DataBind();

        DataTable gridtest = new DataTable();
        gridtest = DataAccess.Get_DataTable("select * from TestOperario where id>1");
        grv_insp_linea.DataSource = gridtest; 
        grv_insp_linea.DataBind();
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void lnkNew_Click(object sender, EventArgs e)
    {

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        Load_Data(); 
        
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        this.AsignarDefectos.ShowOnPageLoad = false;
    }
    protected void ASPxButton8_Click(object sender, EventArgs e)
    {
        ASPxButton btn = (ASPxButton)sender;
        GridViewRow gv = (GridViewRow)(btn.Parent.Parent);
        Label lbloper = (Label)gv.FindControl("lbloperario");
        lbloperario.Text = lbloper.Text;
        this.AsignarDefectos.ShowOnPageLoad = true;
                
        Load_Data();
    }
    protected void grv_insp_linea_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblqy = (Label)e.Row.FindControl("lblmuestreo");
            int qty = Int32.Parse(lblqy.Text);
            total = total + qty;

            TextBox lblqdefect = (TextBox)e.Row.FindControl("txtdefectos");
            int qty1 = Int32.Parse(lblqdefect.Text);
            totaldef = qty1 + totaldef;
        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotalqty = (Label)e.Row.FindControl("lblTotalqty");
            lblTotalqty.Text = total.ToString();

            Label lblTotalDef = (Label)e.Row.FindControl("lblTotaldefect");
            lblTotalDef.Text = totaldef.ToString();
        }
    }

}