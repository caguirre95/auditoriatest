﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using SistemaAuditores.DataAccess;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Security;

public partial class Search : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtpo.Focus();
            string idauditoria = (string)Request.QueryString["idAuditoria"];
            //string id_linea = (string)Request.QueryString["id_linea"] ?? "NA";
            HiddenFieldidAuditoria.Value = idauditoria;
        }
    }
    protected void btnbuscar_Click(object sender, EventArgs e)
    {
        try
        {
            //string id_seccion = (string)Request.QueryString["id_seccion"] ?? "NA";
            string idmodulo = (string)Request.QueryString["idSeccion"];

            if (HiddenFieldidlinea.Value != "")
            {

                if (HiddenFieldidorder.Value != "")
                {
                    if (HiddenFieldidAuditoria.Value == "1")
                        Response.Redirect("InspecProcesoSemaforo.aspx?idOrder=" + HiddenFieldidorder.Value + "&idAuditoria=" + HiddenFieldidAuditoria.Value + "&idLinea=" + Convert.ToInt32(HiddenFieldidlinea.Value) + "&idModulo=" + idmodulo, false);
                    else if (HiddenFieldidAuditoria.Value == "2")
                        Response.Redirect("BultoaBultoAprovados.aspx?idOrder=" + HiddenFieldidorder.Value + "&idAuditoria=" + HiddenFieldidAuditoria.Value + "&idLinea=" + Convert.ToInt32(HiddenFieldidlinea.Value), false);
                    else if (HiddenFieldidAuditoria.Value == "3")
                        Response.Redirect("InspecInternaAprobados.aspx?idOrder=" + HiddenFieldidorder.Value + "&idAuditoria=" + HiddenFieldidAuditoria.Value + "&idLinea=" + Convert.ToInt32(HiddenFieldidlinea.Value), false);
                    else if (HiddenFieldidAuditoria.Value == "6")
                        Response.Redirect("InspecEmbarque.aspx?idOrder=" + HiddenFieldidorder.Value + "&idAuditoria=" + HiddenFieldidAuditoria.Value + "&idLinea=" + Convert.ToInt32(HiddenFieldidlinea.Value), false);
                    else if (HiddenFieldidAuditoria.Value == "7")
                        Response.Redirect("InspeccionEmpaque.aspx?idOrder=" + HiddenFieldidorder.Value + "&idAuditoria=" + HiddenFieldidAuditoria.Value + "&idLinea=" + Convert.ToInt32(HiddenFieldidlinea.Value), false);
                    else if (HiddenFieldidAuditoria.Value == "8")
                        Response.Redirect("InspecFinal.aspx?idOrder=" + HiddenFieldidorder.Value + "&idAuditoria=" + HiddenFieldidAuditoria.Value + "&idLinea=" + Convert.ToInt32(HiddenFieldidlinea.Value), false);
                    else if (HiddenFieldidAuditoria.Value == "9")
                        Response.Redirect("BultoaBultoInspFinal.aspx?idOrder=" + HiddenFieldidorder.Value + "&idAuditoria=" + HiddenFieldidAuditoria.Value + "&idLinea=" + Convert.ToInt32(HiddenFieldidlinea.Value), false);
                    else if (HiddenFieldidAuditoria.Value == "11")
                        Response.Redirect("InspecEmbarqueIntex.aspx?idOrder=" + HiddenFieldidorder.Value + "&idAuditoria=" + HiddenFieldidAuditoria.Value + "&idLinea=" + Convert.ToInt32(HiddenFieldidlinea.Value), false);

                }
                else
                {
                    limpiar();
                    string error = "alert('No se encontro esta Orden, Intente Nuevamente!')";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);

                }
            }
            else
            {
                string error = "alert('Debe Asignar El Corte a Una Linea, Intente Nuevamente luego de asignarlo a una Linea!')";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);

            }
        }
        catch (Exception ex)
        {
            string error = "alert('Error 300 " + ex.Message + "');";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "alert", error, true);
        }
    }

    void limpiar()
    {
        txtestilo.Text = string.Empty;
        txtpo.Text = string.Empty;
        HiddenFieldidStyle.Value = "";
        HiddenFieldidlinea.Value = "";
        HiddenFieldidorder.Value = "";
        txtpo.Focus();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<porderClass> GetPorder(string pre, string idl)
    {
        try
        {
            MembershipUser mu = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            string userId = mu.ProviderUserKey.ToString();

            string query = "";
            if (idl.Equals("1") || idl.Equals("2") || idl.Equals("8"))
            {
                query = " select top 20 p.POrder,s.Style,p.Id_Order,p.Id_Style,p.Id_Linea2 "
                      + " from POrder p"
                      + " join Style s on p.Id_Style = s.Id_Style"
                      + " join (select l.id_linea from Linea l"
                      + " join tbInspectorLinea il on l.id_linea = il.idLinea"
                      + " join Inspector i on il.idInspector = i.id_inspector"
                      + " where i.UserId = '" + userId + "')as c1 on p.Id_Linea2 = c1.id_linea"
                      + " where p.POrder like '%" + pre + "%' order by LEN(p.POrder)";
            }
            else if (idl.Equals("11"))
            {
                query = " select top 20 p.POrder,s.Style,p.Id_Order,p.Id_Style,p.Id_Linea2 from POrder p join Style s on p.Id_Style = s.Id_Style and s.washed = 1"
                      + " where p.POrder like '%" + pre + "%' order by LEN(p.POrderClient)";
            }
            else
            {
                query = " select top 20 p.POrder,s.Style,p.Id_Order,p.Id_Style,p.Id_Linea2 from POrder p join Style s on p.Id_Style=s.Id_Style where p.POrder like '%" + pre + "%'";
            }

            List<porderClass> list = new List<porderClass>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand(query, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                porderClass obj = new porderClass();

                obj.porder = dt.Rows[i][0].ToString();
                obj.style = dt.Rows[i][1].ToString();
                obj.Id_Order = dt.Rows[i][2].ToString();
                obj.Id_Style = dt.Rows[i][3].ToString();
                obj.Id_Linea = dt.Rows[i][4].ToString();
                list.Add(obj);
            }

            return list;
        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    public class porderClass
    {
        public string porder { get; set; }
        public string style { get; set; }
        public string Id_Order { get; set; }
        public string Id_Style { get; set; }
        public string Id_Linea { get; set; }
    }
}