﻿using SistemaAuditores.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdministrarEstilosYTallas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargaestilosAll();
            cargaPuntosMedida();
            cargaTallas();
            cargaClientes();
        }
    }

    void cargaestilosAll()
    {
        var dt = DataAccess.Get_DataTable("select Id_Style,Style from Style ");

        GridEstilos.DataSource = dt;
        GridEstilos.DataBind();
    }

    void cargaPuntosMedida()
    {
        try
        {
            var dt = DataAccess.Get_DataTable("select idPuntosM,NombreMedida from tblPuntosMedida order by NombreMedida");

            drpPuntosMedidas.DataSource = dt;
            drpPuntosMedidas.DataValueField = "idPuntosM";
            drpPuntosMedidas.DataTextField = "NombreMedida";
            drpPuntosMedidas.DataBind();
            drpPuntosMedidas.Items.Insert(0, "Selec...");
        }
        catch (Exception)
        {

            //throw;
        }
    }

    void cargaTallas()
    {
        try
        {
            var dt = DataAccess.Get_DataTable("select idtalla,talla from tbtalla order by LEN(talla)desc,Talla ");

            GridViewtallas.DataSource = dt;
            GridViewtallas.DataBind();

        }
        catch (Exception)
        {

            //throw;
        }
    }

    void cargaClientes()
    {
        try
        {
            var dt = DataAccess.Get_DataTable("select Id_Cliente,Cliente from cliente where Estado=1");

            drpClientes.DataSource = dt;
            drpClientes.DataValueField = "Id_Cliente";
            drpClientes.DataTextField = "Cliente";
            drpClientes.DataBind();
            drpClientes.Items.Insert(0, "Selec...");

        }
        catch (Exception)
        {

            //throw;
        }
    }

    protected void GridEstilos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "verTallas")
            {
                try
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    string Idstyle = GridEstilos.DataKeys[indice].Value.ToString();
                    hdnidestilogrid.Value = Idstyle;
                    var dt = DataAccess.Get_DataTable("select st.idestilotalla,t.Talla from tbTalla t join tbEstiloTalla st on t.idTalla=st.idTalla join Style s on st.idEstilo=s.Id_Style where s.Id_Style=" + Idstyle +" order by t.Talla asc");

                    var Estilo = row.Cells[1].Text;
                    lblEstiloSeleccionado.Text = Estilo;

                    GridTallasDetalle.DataSource = dt;
                    GridTallasDetalle.DataBind();

                    limpiarspeck();
                    LimspiarcajasNuevo();

                }
                catch (Exception ex)
                {

                    string tipo = "info";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
                }
            }

        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
        }
    }

    protected void GridEstilos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    Label idbulto = (Label)e.Row.FindControl("lblidbundle");

            //    string query = "select idBBRechazado from tbBultoaBultoRechazado  where  idBulto = " + idbulto.Text;

            //    DataTable bundle_rejected = DataAccess.Get_DataTable(query);

            //    LinkButton lk = (LinkButton)e.Row.FindControl("comando");
            //    LinkButton lk1 = (LinkButton)e.Row.FindControl("comandoRep");

            //    if (bundle_rejected.Rows.Count > 0)
            //    {
            //        RadioButtonList rbl_aprob1 = (RadioButtonList)e.Row.FindControl("RadioButtonList1");
            //        rbl_aprob1.SelectedValue = "0";//Rechazado
            //        lk.CssClass = "btn btn-danger";
            //        lk1.Visible = true;
            //    }
            //    else
            //    {
            //        lk1.Visible = false;
            //        lk.CssClass = "btn btn-default";
            //    }

            //}
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
            return;

        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            var id = hdnidestilo.Value;

            if (id != string.Empty)
            {
                var dt = DataAccess.Get_DataTable("select Id_Style,Style from Style where Id_Style=" + id);

                GridEstilos.DataSource = dt;
                GridEstilos.DataBind();

                hdnidestilo.Value = "";
                txtestilo.Text = "";
                hdnidestilogrid.Value = "";

                limpiarspeck();
                limpiartallas();
                LimspiarcajasNuevo();


            }
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
        }
    }

    protected void GridTallasDetalle_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "speck")
            {
                try
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    string idestilotalla = GridTallasDetalle.DataKeys[indice].Value.ToString();

                    var talla = row.Cells[1].Text;
                    lbltallaSeleccionada.Text = talla;

                    var dt = DataAccess.Get_DataTable("select e.idEspecificacion, p.NombreMedida, e.Valor, e.TolMax, e.Tolmin from tbEspecificacion e join tblPuntosMedida p on e.idPuntoDeMedida = p.idPuntosM  where idEstiloTalla =" + idestilotalla);

                    gridespeck.DataSource = dt;
                    gridespeck.DataBind();


                }
                catch (Exception ex)
                {

                    string tipo = "info";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
                }
            }
            if (e.CommandName == "nuevo")
            {
                try
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int indice = row.RowIndex;
                    string idestilotalla = GridTallasDetalle.DataKeys[indice].Value.ToString();

                    var talla = row.Cells[1].Text;
                    lbltallaSeleccionada.Text = talla;

                    //  lblEstilo.Text =lblEstiloSeleccionado.Text;
                    hdnidestilotalla.Value = idestilotalla;
                    // lbltalla.Text =talla;

                    lblestiloEspecificacion.Text = lblEstiloSeleccionado.Text;
                    lbltallaEspecificacion.Text = talla;
                }
                catch (Exception ex)
                {

                    string tipo = "info";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
                }

            }
        }
        catch (Exception ex)
        {
            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
        }
    }

    protected void GridEstilos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridEstilos.PageIndex = e.NewPageIndex;
            cargaestilosAll();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void gridespeck_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }

    protected void GridTallasDetalle_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridTallasDetalle.PageIndex = e.NewPageIndex;

            var dt = DataAccess.Get_DataTable("select st.idestilotalla,t.Talla from tbTalla t join tbEstiloTalla st on t.idTalla=st.idTalla join Style s on st.idEstilo=s.Id_Style where s.Id_Style=" + hdnidestilogrid.Value + " order by t.Talla asc");

            GridTallasDetalle.DataSource = dt;
            GridTallasDetalle.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void guardarEspecificacion_Click(object sender, EventArgs e)
    {
        try
        {
            var idpuntomedida = Convert.ToInt32(drpPuntosMedidas.SelectedValue);
            var idtallaestilo = Convert.ToInt32(hdnidestilotalla.Value);
            var valor = Math.Round(float.Parse(txtvalorTalla.Text), 2);
            var valorMax = Math.Round(float.Parse(txtvalorMax.Text), 2);
            var valorMin = Math.Round(float.Parse(txtvalorMin.Text), 2);

            var resp = OrderDetailDA.saveEspecificacion(idtallaestilo, idpuntomedida, valor, valorMax, valorMin, Page.User.Identity.Name);

            if (resp == "OK")
            {
                drpPuntosMedidas.Focus();
                txtvalorTalla.Text = string.Empty;
                txtvalorMax.Text = string.Empty;
                txtvalorMin.Text = string.Empty;

                string tipo = "success";
                string mess = "Guardado exitosamente!";


                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
            }
            else
            {
                string tipo = "warning";
                string mess = "Intente Nuevamente";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
            }
        }
        catch (Exception ex)
        {

            string tipo = "error";
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + ex.Message + "','" + tipo + "');", true);
        }

    }

    protected void btnrefreshStyle_Click(object sender, EventArgs e)
    {
        cargaestilosAll();
    }

    public class estil
    {
        public int id { get; set; }
        public string estilo { get; set; }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<estil> GetEstilo(string pre)
    {
        try
        {
            List<estil> list = new List<estil>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);

            cn.Open();
            SqlCommand cmd = new SqlCommand("select Top(10) Id_Style,Style from style where Style like '%" + pre + "%'", cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                estil obj = new estil();

                obj.id = int.Parse(dt.Rows[i][0].ToString());
                obj.estilo = dt.Rows[i][1].ToString();


                list.Add(obj);

            }

            return list;

        }
        catch (Exception ex)
        {
            Console.Write(ex);
            return null;
        }
    }

    [WebMethod]
    public static string Guardarestilo(string estilo, int idcliente, bool wash)
    {
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);
            SqlCommand comm = new SqlCommand();

            cn.Open();
            comm.Connection = cn;
            comm.CommandText = "spdSavestyle";
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Clear();

            comm.Parameters.Add("@Style", SqlDbType.VarChar);
            comm.Parameters[comm.Parameters.Count - 1].Value = estilo;

            comm.Parameters.Add("@idCliente", SqlDbType.Int);
            comm.Parameters[comm.Parameters.Count - 1].Value = idcliente;

            comm.Parameters.Add("@Wash", SqlDbType.Bit);
            comm.Parameters[comm.Parameters.Count - 1].Value = wash;

            comm.ExecuteNonQuery();
            cn.Close();

            return "true";

        }
        catch (Exception ex)
        {
            string mes = ex.Message;
            return "false";
        }

    }

    [WebMethod]
    public static string Guardartalla(string talla)
    {
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RocedesCS"].ConnectionString);
            SqlCommand comm = new SqlCommand();

            cn.Open();
            comm.Connection = cn;
            comm.CommandText = "spdSaveTalla";
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Clear();

            comm.Parameters.Add("@talla", SqlDbType.VarChar);
            comm.Parameters[comm.Parameters.Count - 1].Value = talla.Trim();

            comm.ExecuteNonQuery();
            cn.Close();



            return "true";

        }
        catch (Exception ex)
        {
            string mes = ex.Message;
            return "false";
        }

    }

    protected void btnAsignarTallas_Click(object sender, EventArgs e)
    {

        try
        {
            if (hdnidestilogrid.Value != string.Empty)
            {
                int idestilo = Convert.ToInt32(hdnidestilogrid.Value);
                int cont = 0;

                for (int i = 0; i < GridViewtallas.Rows.Count; i++)
                {
                    var row = (GridViewRow)GridViewtallas.Rows[i];

                    var chk = (CheckBox)row.FindControl("chkSeleccionado");
                    var lblidtallas = (Label)row.FindControl("lblidtallas");

                    if (chk.Checked)
                    {
                        var idtalla = Convert.ToInt32(lblidtallas.Text);

                        var resp = OrderDetailDA.saveAsignacionTallas(idestilo, idtalla);

                        if (resp != "OK")
                        {
                            cont++;
                        }
                    }
                }


                if (cont == 0)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "invocarfuncion", "cerrarmodal()", true);

                    string tipo = "success";
                    string mess = "Guardado exitosamente!";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);

                    cargaTallas();

                   var Idstyle= hdnidestilogrid.Value;
                   var dt = DataAccess.Get_DataTable("select st.idestilotalla,t.Talla from tbTalla t join tbEstiloTalla st on t.idTalla=st.idTalla join Style s on st.idEstilo=s.Id_Style where s.Id_Style=" + Idstyle + " order by t.Talla asc");


                    GridTallasDetalle.DataSource = dt;
                    GridTallasDetalle.DataBind();

                    limpiarspeck();
                    LimspiarcajasNuevo();
                }
                else
                {
                    string tipo = "error";
                    string mess = "verificar algunas tallas no se agregaron al estilo";
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
                }

            }
            else
            {
                string tipo = "warning";
                string mess = "Debe seleccionar el estilo de la tabla!";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnactualizarvistatallas_Click(object sender, EventArgs e)
    {
        try
        {
            cargaTallas();
        }
        catch (Exception)
        {


        }
    }


    void LimspiarcajasNuevo()
    {
        lblestiloEspecificacion.Text = string.Empty;
        lbltallaEspecificacion.Text = string.Empty;
        hdnidestilotalla.Value = string.Empty;
        drpPuntosMedidas.SelectedIndex = 0;
        txtvalorTalla.Text = string.Empty;
        txtvalorMax.Text = string.Empty;
        txtvalorMin.Text = string.Empty;
    }

    void limpiarspeck()
    {
        lbltallaSeleccionada.Text = string.Empty;
        gridespeck.DataSource = null;
        gridespeck.DataBind();

    }

    void limpiartallas()
    {
        lblEstiloSeleccionado.Text = string.Empty;
        GridTallasDetalle.DataSource = null;
        GridTallasDetalle.DataBind();
    }


    protected void btnActualizarTodos_Click(object sender, EventArgs e)
    {
        try
        {

            int cont = 0;

            for (int i = 0; i < gridespeck.Rows.Count; i++)
            {
                var row = gridespeck.Rows[i];


                var lblidespecificacion = (Label)row.FindControl("lblidespecificacion");
                var txtval = (TextBox)row.FindControl("txtvalor");
                var txtvalMax = (TextBox)row.FindControl("txtvalor1");
                var txtvalMin = (TextBox)row.FindControl("txtvalor2");

                if (txtval.Text!=string.Empty && txtvalMax.Text != string.Empty && txtvalMin.Text != string.Empty)
                {
                    var idespecificacion = Convert.ToInt32(lblidespecificacion.Text);
                    var valor = Math.Round(float.Parse(txtval.Text), 2);
                    var valorMax = Math.Round(float.Parse(txtvalMax.Text), 2);
                    var valorMin = Math.Round(float.Parse(txtvalMin.Text), 2);

                    var resp = OrderDetailDA.updateEspecificacion(idespecificacion,valor,valorMax,valorMin,Page.User.Identity.Name );

                    if (resp != "OK")
                    {
                        cont++;
                    }
                }
            }


            if (cont == 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "invocarfuncion", "cerrarmodal()", true);

                string tipo = "success";
                string mess = "Guardado exitosamente!";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);


            }
            else
            {
                string tipo = "error";
                string mess = "verificar algunas tallas no se agregaron al estilo";
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alertSeleccionarMedida('" + mess + "','" + tipo + "');", true);
            }


        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btn1_Click(object sender, EventArgs e)
    {
        Response.Redirect("/AdminStyleSize2.aspx");
    }
}