﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OperarioOperacion.aspx.cs" Inherits="OperarioOperacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript" src="scripts/footable.min.js"></script>

    <script type="text/javascript">

        function alertSeleccionarMedida(mess, tipo) {
            swal({
                title: "",
                text: mess,
                type: tipo
            });
        }

        function successalert() {
            swal({
                title: 'Exito!',
                text: "Registro Ingresado Correctamente",
                type: 'success'
            });
        }


    </script>

    <script type="text/javascript">

        function warningalert() {
            swal({
                title: "¿Estas seguro?",
                text: "¿Deseas ingresar este registro?",
                type: "warning"
            })
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentTitulo" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="col-lg-6">
                        <asp:Label ID="lbloperario" runat="server" Text="Cargar Operario"></asp:Label>
                        <asp:Button ID="btnoperario" runat="server" CssClass="form-control btn btn-info" Text="Operario" OnClick="btnoperario_Click" />
                    </div>
                    <div class="col-lg-6">
                        <asp:Label ID="Label1" runat="server" Text="Cargar Operacion"></asp:Label>
                        <asp:Button ID="Button1" runat="server" CssClass="form-control btn btn-info" Text="Operacion" OnClick="Button1_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

